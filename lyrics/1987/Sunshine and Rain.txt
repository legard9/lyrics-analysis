Hangul

[Microdot]
내 목걸이가 너무 빛나서
내 차도 비싸고
All Black이라서 너무 신났어
내 신도 색이 빠져
새로 샀어 탐나서
가격대도 물론 봤지
계산하고 나서
원했던 것들이
결론 나의 것이 됐네
하루가 너무 짧게
느껴지는 삶이 됐네
시간이 부족해
더 살까 고민했지 Rolex
Time Travel이 필요해
빈지노 I called him
＂Ayo 형
Tropical Night을 찾는데 어디로?＂
＂Ah, Let's time travel
준비됐어? Okay, Here we go＂
Hold up 아직 할 말이 몇 마디가
남아 떠나기 일러 중요한 이야기야
Listen. Be careful
What you wish for
Be fearful what you
Choose love and ignore
후회하지 않길 바래
나는 Rolex를 사지 않았지
새 신들은 숨어있고
목걸이는 보이지 않아 불빛 없인

[Microdot]
모두 Stop 합죽이가 됩시다 합!
너네들을 위해 내가 적은 편지니까
듣고 있다면 손을 좌우로 흔들어봐
듣고 있다면 다 같이 외쳐
Hallelujah
할렐루야 할렐루야 할렐루야 Amen
할렐루야 할렐루야 할렐루야 Amen
할렐루야 할렐루야 할렐루야 Amen
할렐루야 할렐루야 할렐루야

[Microdot]
Listen young world
Your our future generation
너네들이 우리에 미래를 당장
그려 나갈 준비 되어있어?
걱정들은 어서 내려
이 순간만 즐겨보게
휴대폰들 멀리 두고
연필과 종이를 들어보세
난 돈도 많이 벌어봤고
온 세계를 다 돌아봤지
크게 펼친 무댈 서고
거대한 꿈을 이뤄놨지
'불가능은 없다'란 말도
다 현실로 증명했으니
내가 뱉는 모든 말들은
It's for you all for the taking
Listen young world
너네 앞길이 어두워 보일 땐
세상이 던지는 유혹에
흔들리지 않길 기도를 올릴게
많은 애들이 배운 것 없이
지름길만 찾고 다닌대
그들 사이에 너가 제일
빛나는 미래를 펼치길 바랄게

Hallelujah
내 고생길은 24에 끝나서
Hallelujah
주님이 그어주신 길을 잃지 않아서
Hallelujah
For our freedom, so so spectacular
Hallelujah, I'm not the one
For I am just a messenger

[Microdot]
모두 Stop 합죽이가 됩시다 합!
너네들을 위해 내가 적은 편지니까
듣고 있다면 손을 좌우로 흔들어봐
듣고 있다면 다 같이 외쳐
Hallelujah
할렐루야 할렐루야 할렐루야 Amen
할렐루야 할렐루야 할렐루야 Amen
할렐루야 할렐루야 할렐루야 Amen
할렐루야 할렐루야 할렐루야

[YDG]
내가 이미 얻었다 함도 아니요
온전히 이루었다 함도 아니라
오직 내가 그리스도 예수께
잡힌바 된 그것을
잡으려고 좇아가노라
주의 얼굴을 봐
주의 체온을 느껴
주의 달린 십자가
주의 이름 높여
홀로 영광 받아 마땅한
세상 죄를 감당한
우리 다시 만나자 요단강 건너
최고의 가치
자가치유능력 회개기도
말씀에 하라 하셨으니
난 그저 따르지
네 뜻대로 마옵시고
주 뜻대로 하소서

We will stand together and pray
Through the sunshine and rain
Everybody get together and say
Hallelujah Hallelujah Amen
We will stand together and pray
Through the sunshine and rain
Everybody get together and say
Hallelujah Hallelujah Amen

Hallelujah Hallelujah Amen
Hallelujah Hallelujah Amen
Hallelujah Hallelujah Amen
Hallelujah Hallelujah Amen

할렐루야 할렐루야 할렐루야 Amen
할렐루야 할렐루야 할렐루야 Amen
할렐루야 할렐루야 할렐루야 Amen
할렐루야 할렐루야 할렐루야
X2

[YDG]
할렐루야 아멘

Romanization

[Microdot]
Nae moggeol-iga neomu bichnaseo
Nae chado bissago
All Blackilaseo neomu sinnass-eo
Nae sindo saeg-i ppajyeo
Saelo sass-eo tamnaseo
Gagyeogdaedo mullon bwassji
Gyesanhago naseo
Wonhaessdeon geosdeul-i
Gyeollon naui geos-i dwaessne
Haluga neomu jjalbge
Neukkyeojineun salm-i dwaessne
Sigan-i bujoghae
Deo salkka gominhaessji Rolex
Time Travel-i pil-yohae
Binjino I called him
＂Ayo hyeong
Tropical Nighteul chajneunde eodilo?＂
＂Ah, Let's time travel
Junbidwaess-eo? Okay, Here we go＂
Hold up ajig hal mal-i myeoch madiga
Nam-a tteonagi illeo jung-yohan iyagiya
Listen. Be careful
What you wish for
Be fearful what you
Choose love and ignore
Huhoehaji anhgil balae
Naneun Rolexleul saji anh-assji
Sae sindeul-eun sum-eoissgo
Moggeol-ineun boiji anh-a bulbich eobs-in

[Microdot]
Modu Stop habjug-iga doebsida hab!
Neonedeul-eul wihae naega jeog-eun pyeonjinikka
Deudgo issdamyeon son-eul jwaulo heundeul-eobwa
Deudgo issdamyeon da gat-i oechyeo
Hallelujah
Hallelluya hallelluya hallelluya Amen
Hallelluya hallelluya hallelluya Amen
Hallelluya hallelluya hallelluya Amen
Hallelluya hallelluya hallelluya

[Microdot]
Listen young world
Your our future generation
Neonedeul-i ulie milaeleul dangjang
Geulyeo nagal junbi doeeoiss-eo?
Geogjeongdeul-eun eoseo naelyeo
I sunganman jeulgyeoboge
Hyudaepondeul meolli dugo
Yeonpilgwa jong-ileul deul-eobose
Nan dondo manh-i beol-eobwassgo
On segyeleul da dol-abwassji
Keuge pyeolchin mudael seogo
Geodaehan kkum-eul ilwonwassji
'bulganeung-eun eobsda'lan maldo
Da hyeonsillo jeungmyeonghaess-euni
Naega baetneun modeun maldeul-eun
It's for you all for the taking
Listen young world
Neone apgil-i eoduwo boil ttaen
Sesang-i deonjineun yuhog-e
Heundeulliji anhgil gidoleul ollilge
Manh-eun aedeul-i baeun geos eobs-i
Jileumgilman chajgo danindae
Geudeul saie neoga jeil
Bichnaneun milaeleul pyeolchigil balalge

Hallelujah
Nae gosaeng-gil-eun 24e kkeutnaseo
Hallelujah
Junim-i geueojusin gil-eul ilhji anh-aseo
Hallelujah
For our freedom, so so spectacular
Hallelujah, I'm not the one
For I am just a messenger

[Microdot]
Modu Stop habjug-iga doebsida hab!
Neonedeul-eul wihae naega jeog-eun pyeonjinikka
Deudgo issdamyeon son-eul jwaulo heundeul-eobwa
Deudgo issdamyeon da gat-i oechyeo
Hallelujah
Hallelluya hallelluya hallelluya Amen
Hallelluya hallelluya hallelluya Amen
Hallelluya hallelluya hallelluya Amen
Hallelluya hallelluya hallelluya

[YDG]
Naega imi eod-eossda hamdo aniyo
Onjeonhi ilueossda hamdo anila
Ojig naega geuliseudo yesukke
Jabhinba doen geugeos-eul
Jab-eulyeogo joch-aganola
Juui eolgul-eul bwa
Juui che-on-eul neukkyeo
Juui dallin sibjaga
Juui ileum nop-yeo
Hollo yeong-gwang bad-a mattanghan
Sesang joeleul gamdanghan
Uli dasi mannaja yodangang geonneo
Choegoui gachi
Jagachiyuneunglyeog hoegaegido
Malsseum-e hala hasyeoss-euni
Nan geujeo ttaleuji
Ne tteusdaelo maobsigo
Ju tteusdaelo hasoseo

We will stand together and pray
Through the sunshine and rain
Everybody get together and say
Hallelujah Hallelujah Amen
We will stand together and pray
Through the sunshine and rain
Everybody get together and say
Hallelujah Hallelujah Amen

Hallelujah Hallelujah Amen
Hallelujah Hallelujah Amen
Hallelujah Hallelujah Amen
Hallelujah Hallelujah Amen

Hallelluya hallelluya hallelluya Amen
Hallelluya hallelluya hallelluya Amen
Hallelluya hallelluya hallelluya Amen
Hallelluya hallelluya hallelluya
X2

[YDG]
Hallelluya amen