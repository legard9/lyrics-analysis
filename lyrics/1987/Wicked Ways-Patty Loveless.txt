Well it's four in the morning
Rain is a pourin'
When you stumble in drunk again
Soaked to the bone
When you find your way home
Hey baby, where you been?
Had a few with the boys
Made a little noise
That's what you said to me
I shake my head
And help you up to bed
And wonder when I'll ever break free
From the hell you raised
When you gonna turn
From your wicked ways?

I couldn't sleep, from worrying
And it's keepin' me quite awake at night
Tossin and a turnin' prayin' and a yearnin' for the day
You'll see the light
Fightin' and a puttin'
Lyin' and a hurtin' me
More than I ever planned
I used to be sad
But now I'm just mad
And I've taken more than
I will stand
From the hell you raised
When you gonna turn
From your wicked ways?

Ahh..no more grievin'
Baby I'm leavin'
I'm tired of the endless pain
Stayin' 'round here
With your women and your beer
Is slowly drivin' me insane

Oh I couldn't sleep
And worry it's a keepin' me
Wide awake at night
Tossin' and a turnin'
Prayin' and a yearnin'
For the day you'll see the light
Gave it my best shot
Found out you're not
Really even willin' to try
What more can I do
I'm done puttin' up with you
And now I'm gonna say goodbye
To the hell you raise
When you gonna turn
From your wicked ways?

Oooh well I couldn't sleep
And worry it's keepin' me
Wide awake at night
Tossin' and a turnin'
Prayin' and a yearnin'
For the day you'll see the light
Gave it my best shot
Found out you're not
Really even willin' to try
What more can I do
I'm done puttin' up with you
Now I'm gonna say goodbye
To the hell you raise
When you gonna turn
From your wicked ways?

Oh the hell you raise
When you gonna turn
From your wicked ways?