In Afghanistan, yelda is the first night of the month of Jadi, the first
night of winter, and the longest night of the year. As was the tradition, Hassan
and I used to stay up late, our feet tucked under the kursi, while Ali tossed
apple skin into the stove and told us ancient tales of sultans and thieves to
pass that longest of nights. It was from Ali that I learned the lore of yelda,
that bedeviled moths flung themselves at candle flames, and wolves climbed
mountains looking for the sun. Ali swore that if you ate water melon the night
of yelda, you wouldn’t get thirsty the coming summer.
When I was older, I read in my poetry books that yelda was the starless night
tormented lovers kept vigil, enduring the endless dark, waiting for the sun to
rise and bring with it their loved one. After I met Soraya Taheri, every night
of the week became a yelda for me. And when Sunday mornings came, I rose from
bed, Soraya Taheri’s brown-eyed face already in my head. In Baba’s bus, I
counted the miles until I’d see her sitting barefoot, arranging cardboard boxes
of yellowed encyclopedias, her heels white against the asphalt, silver bracelets
jingling around her slender wrists. I’d think of the shadow her hair cast on the
ground when it slid off her back and hung down like a velvet curtain. Soraya.
Swap Meet Princess. The morning sun to my yelda.
I invented excuses to stroll down the aisle--which Baba acknowledged with a
playful smirk--and pass the Taheris’ stand. I would wave at the general,
perpetually dressed in his shiny overpressed gray suit, and he would wave back.
Sometimes he’d get up from his director’s chair and we’d make small talk about
my writing, the war, the day’s bargains. And I’d have to will my eyes not to
peel away, not to wander to where Soraya sat reading a paperback. The general
and I would say our good-byes and I’d try not to slouch as I walked away.
Sometimes she sat alone, the general off to some other row to socialize, and I
would walk by, pretending not to know her, but dying to. Sometimes she was there
with a portly middle-aged woman with pale skin and dyed red hair. I promised
myself that I would talk to her before the summer was over, but schools
reopened, the leaves reddened, yellowed, and fell, the rains of winter swept in
and wakened Baba’s joints, baby leaves sprouted once more, and I still hadn’t
had the heart, the dil, to even look her in the eye.
The spring quarter ended in late May 1985. I aced all of my general education
classes, which was a minor miracle given how I’d sit in lectures and think of
the soft hook of Soraya’s nose.
Then, one sweltering Sunday that summer, Baba and I were at the flea market,
sitting at our booth, fanning our faces with news papers. Despite the sun
bearing down like a branding iron, the market was crowded that day and sales had
been strong--it was only 12:30 but we’d already made $160. I got up, stretched,
and asked Baba if he wanted a Coke. He said he’d love one.
“Be careful, Amir,” he said as I began to walk. “Of what, Baba?”
“I am not an ahmaq, so don’t play stupid with me.”
“I don’t know what you’re talking about.”
“Remember this,” Baba said, pointing at me, “The man is a Pashtun to the root.
He has nang and namoos.” Nang. Namoos. Honor and pride. The tenets of Pashtun
men. Especially when it came to the chastity of a wife. Or a daughter.
“I’m only going to get us drinks.”
“Just don’t embarrass me, that’s all I ask.”
“I won’t. God, Baba.”
Baba lit a cigarette and started fanning himself again.