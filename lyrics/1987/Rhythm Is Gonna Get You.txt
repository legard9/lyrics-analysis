[Intro: Black Steel]
Ummm-hmmm-hm
Who-nanananana
Hmmmmm mmmm
Ummm-hmmm-hm-yeah
Ummm-hmmm-hm-yeah
Ummm-hmmmmmm
They're justified and they're ancient
And they like to roam the land
They're justified and they're ancient
I hope you understand
They don't want to upset the apple cart
And they don't want to cause any harm
But if you don't like what they're going to do
You better not stop them cause they're coming through
(fade-out)

[Post-Intro: Rob Tyner]
And right now... right now... right now it's time to...
Kick out the JAMS!

[Verse 1: M.C. Bello]
The notes'll flow, yo -- for the words I speak
Rap is weak so I teach and I reach
A positive vibe, a way of life is how I'm livin'
So get hype to the rhythm
KLF is the crew, ya hear -- yeah
Design a rhyme I just won't fear
Back to react, enough is enough
Let me ask you a question -- What Time Is Love?
What time is love?
What time is love?
What time is love?

[Refrain: Katie Kissoon, P.P. Arnold & Lawanda McFarland]
Mu mu
Mu mu
Mu mu
Mu mu
I wanna see you sweat
I wanna see you sweat
I wanna see you sweat

[Verse 2: M.C. Bello]
Take a chance to advance you might alucinate
Exagerate I devastate
We had to pass the Pyramid Blaster
The JAMs are here, is what you being at to
I make you, shake you, break you, take to
Right in the Path, what they call the Mu Mu
KLF - What, we can't rock? Bring the Break
What Time is Love?
What Time is Love?
What Time is Love?

[Refrain: Katie Kissoon, P.P. Arnold & Lawanda McFarland]
Mu mu
Mu mu
Mu mu
Mu mu
I wanna see you sweat
I wanna see you sweat
Okay, gonna get the countdown - 4, 3, 2, 1, fire!
I wanna see you

[Verse 3: M.C. Bello]
The final chapter -- prophetic, poetic
When I'm done, this calls for anesthetic
Get to, step to, let an MC
Come in effect with Kingboy D
A wannabe, gonnabe -- ol'time sucka
You know the time, I never stutter
A feat, a dream, a-yeah seem bright
Yeah, pass the mic -- What Time Is Love?
What time is love?
What time is love?
What time is love?

[Refrain: Katie Kissoon, P.P. Arnold & Lawanda McFarland]
Mu mu
Mu mu
Mu mu
Mu mu
I wanna see you sweat
I wanna see you sweat
I wanna see you sweat
I wanna see you sweat
I wanna see you sweat
I wanna see you sweat
I wanna see you
I wanna see you
I wanna see you sweat
I wanna see you sweat
I wanna see you sweat