[Verse 1:]
It's been so very long
Since you held me in your arms this way
And all this time that you've been gone
I've thought about you each and every day
So now you've returned
And you want to try our love again
But frankly I'm concerned
That you will hurt
(Hurt me)
Like you did then
What you must do
To prove your love is true
Is give me a guarantee
That you'll love me through and through

[Chorus:]
Promise me
That this time love will stay
(Love will stay)
Promise me
(Promise me)
That you'll never go away
(Never go away)
Promise me
That this time love won't end
(Love won't end)
Promise me
That I will never cry again

[Verse 2:]
It's been so very long
Since you held me in your arms so tight
And all this time that you've been gone
I've thought about you each and every night
So now you've returned
And you want to try our love again
But frankly I'm concerned
That you will hurt (Hurt me)
Like you did then
What you must do
To prove your love is true
Is give me a guarantee
That you'll love me through and through

[Repeat Chorus:]

[Bridge:]
Will you promise me
Will you promise, promise
Will you promise me
Will you promise, promise
Will you promise me
Will you promise, promise
Will you promise me
Will you promise, promise

[Repeat Chorus x2:]