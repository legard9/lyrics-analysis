[Verse 1]
Kneel down you sinners to streetwise religion
Greed's been crowned the new king
Hollywood dream teens, yesterday's trash queens
Save the blessings for the final ring, amen

[Chorus]
Take a ride on the wild side
Wild side

[Verse 2]
I carry my crucifix under my death list
Forward my mail to me in hell
Liars and the martyrs lost faith in the Father
Long lost is the wishing well

[Chorus]
Take a ride on the wild side
Wild side

[Verse 3]
Fallen angels so fast to kill
Thy Kingdom come on the wild side
Our Father, who ain't in heaven
Be Thy name on the wild side
Holy Mary, Mother, may I
Pray for us on the wild side?
Wild side, wild side

[Verse 4]
Name dropping no names, glamorize cocaine
Puppets with strings of gold
East LA at midnight, papa won't be home tonight
Found dead with his best friend's wife

[Chorus]
Take a ride on the wild side
Wild side
Take a ride on the wild side
Wild side

[Verse 5]
Gang fights, fatal strikes
We lie on the wild side
No escape, murder rape
Doing time on the wild side
A baby cries, a cop dies
A days pay on the wild side

[Outro]
Wild side, wild side
Tragic life on the wild side
Wild side, wild side
Kicking ass on the wild side
No going back on the wild side