[Spoken Intro: Jim & Billy]
*Morse Code repeats in the background*

[Jim:] 
This is K-A-O-S. You and I are listening to KAOS in Los Angeles. Let's go to the telephones now and take a request

[Billy:] 
Hello, I'm Billy

[Jim:] Yes?

[Billy:] 
I hear radio waves in my head

[Jim:] You hear radio waves in your head?

[Jim:] Uh, is there a request that you have tonight for KAOS?

[Hook]
Radio waves, radio waves
He hears radio waves, radio waves

[Verse 1]
The atmosphere is thin and cold
The yellow sun is getting old
The ozone overflows with radio waves
AM, FM, weather and news
Our leaders had a frank exchange of views
Are you confused? Radio waves

[Hook]
Radio waves, radio waves
AM radio waves, FM radio waves
Radio waves 
(Radio)
Mind-numbing radio waves 
(Radio)
Fish-stunning radio waves 
(Radio)
Radio waves

[Verse 2]
Magic Billy in his wheel chair
Is picking up all this stuff in the air
Billy is face to face with outer space
Messages from distant stars
The local police calling all cars! Radio waves

[Hook]
Hear them radio waves 
(Radio)
, radio waves 
(Radio)
Jesus saves radio, radio waves
Radio waves 
(Radio waves)
AM radio waves 
(Radio waves)
, FM radio waves 
(Radio waves)
All them radio waves 
(Radio waves)

[Instrumental Bridge]

[Outro]
(Radio waves)
 Radio waves
(Radio waves)
 Radio waves
(Radio waves)
 He hears radio waves
(Radio waves)
 Radio waves
(Radio waves)
(Radio waves)
 Radio waves
(Radio waves)
(Radio waves)
 Hopeful radio waves
(Radio waves)
(Radio waves)
 Dopeful radio waves
(Radio waves)
(Radio waves)
 Russian radio waves
(Radio waves)
(Radio waves)
 Prussian radio waves
Eastern radio waves
Western radio waves
Testing radio waves, one, two, one, two
Radio waves
(Radio waves)
 Getting through to you
Morse code radio waves
Tobacco road radio waves
South to Paloma radio waves
Oklahoma City radio waves
Sitting pretty radio waves
Nitty-gritty radio waves, radio waves
Radio waves, ciao!