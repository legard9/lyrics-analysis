I know you think
I'm no match for you, baby
You like making it
Rough on me, don't you?
My, my, my (ha, ha)

Back to the shack, oh, oh yeah
There ain't nothing suits me
Better than that

If time has taught me anything
You've got to learn to be the ball
And I can't keep from laughing at it all
I'm going now

I'm going all the way
Sooner or later gotta love somebody
I don't care how long it takes

Like a shot to the heart
I've got news for you
I may not look so smart
But I'm nobody's fool

On a roll
That's enough for me, baby
Taking it slow
It's too much for me to analyze
Still all right heading
Into the rhythm of the summer night

Winds of wild insanity
Blow with me tonight
Saddle up all you cowboys
Gonna ride
I'm going now

I'm going all the way
Sooner or later gotta love somebody
Don't care how long it takes

You can turn up the heat
But I'm playing it cool
I know it's hard to believe
I am nobody's fool

I'm going all the way
(All the way)
Sooner or later gotta love somebody
(I don't care)
Don't care how long it takes

You might think that
I'm not going to school on you
Baby, like it or not
I ain't nobody's fool

My illumination
May come as some surprise
You may try to deny it
You're amazed when the clown
Slaps you in the face
Leaves you dumbfounded
There in his wake, yeah

I'm going all the way, yeah
Sooner or later gotta love somebody
Don't care how long it takes

Like a shot to the heart
I've got news for you
Baby, I'm not so smart
But I'm nobody's fool

I'm going all the way, oh yeah
I'm going all the way
(Nobody's fool, baby)
Don't care how long it takes
(I don't care)
Listen to the band
I'm going all the way
(Who's fooling who?)