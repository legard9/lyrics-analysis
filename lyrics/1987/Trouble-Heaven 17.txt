I'm in a place where nothing ever happens
I'll try one day to walk away
But for now I'll hand around
I am a fool without exception

I break every rule
But I never break away

(Trouble) - It's walking my way
I feel it moving closer day after day
(Trouble) - I'm talking to you

It's getting hot in here but what can I do

Sometime, someplace I'll make it happen
Yours is the face, to help me set this time ablaze

I've always thought there's no protection
You'll walk through the door
And I'll never want for more

(Trouble) - Again and again

It's not so simple and I never know when
(Trouble) - For me and for you
The trouble's double now we're fighting for two

We'll take this place we'll make it happen
We'll leave them behind

We'll leave them all to change their minds
The trouble is with the solution
One's not so strong, but with two of us we can't go wrong

(Trouble) - I'm talking to you

It's getting hot in here but what can I do
(Trouble) - Again and again

It's not so simple and I never know when
(Trouble) - I'm talking to you

It's getting hot in here but what can I do