A million young poets screaming out their words
To a world full of people just living to be heard
Future generations, riding on the highways that we built
I hope they have a better understanding

(Check it out) Going to work on Monday
(Check it out) Got yourself a family
(Check it out) All utility bills have been paid
You can't tell your best buddy that you love him

(So check it out) Where does our time go
(Check it out) Got a brand new house in escrow
(Check it out) Sleeping with your back to your loved one
This is all that we've learned about happiness

(Check it out) Forgot to say hello to my neighbors
(Check it out) Sometimes I question my own behavior
(Check it out) Talking about the girls that we've seen on the sly
Just to tell our souls we're still the young lions

(So check it out) Getting too drunk on Saturdays
(Check it out) Playing football with the kids on Sundays
(Check it out) Soaring with the eagles all week long
And this is all that we've learned about living
This is all that we've learned about living

Check it out
Check it out
Check it out
Check it out

A million young poets screaming out their words
Maybe someday those words will be heard
By future generations riding on the highways that we built
Maybe they'll have a better understanding
(Check it out) Hope they'll have a better understanding
(Check it out) Maybe they'll have a better understanding
(Check it out) Maybe they'll have a better understanding
(Check it out) Hope they have a better understanding

Check it out
Check it out
Check it out
Check it out
Check it out
Check it out