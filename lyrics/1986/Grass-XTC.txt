[Verse 1]
Laying on the grass, my heart it flares like fire
The way you slap my face just fills me with desire

[Floating away 1]
You play hard to get
'Cause you're teacher's pet
But when the boats have gone
We'll take a tumble, excuse for a fumble

[Verse 2]
Shocked me too the things we used to do on grass
If you fancy, we can buy an ice-cream cone
Your mate has gone, she didn't want to be alone

[Floating away 2]
I will pounce on you
Just us and the cuckoos
You are helpless now
Over and over we flatten the clover

[Verse 3]
Shocked me too the things we used to do on grass
It would shock you too the things we used to do on grass

[Hook]
Grass
On grass

[Outro]
Things we did on grass