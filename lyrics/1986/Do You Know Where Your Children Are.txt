One morning I sleepwalked to the store and bought a paper
On the front page, it said, "wake up stop living in a dream"
Then it hit me like I was playing rugby
With the Thanksgiving Day Parade, hip hop hooray
Put it in a glass of water, something to help it go down
I'm into nothing like a tiny little town
In the middle of nowhere
Do you know where your children are tonight?
I hope they're saying no to dope like Nancy Reagan preached
Cause in time, they'll find their records blemished
And they've lost all of their teeth
And one day they'll discover another lover in their beds
And jump right of their Keds
Put it in a glass of water, something to help it go down
I'm into nothing like a tiny little town
In the middle of nowhere
Do you know where your children are tonight?