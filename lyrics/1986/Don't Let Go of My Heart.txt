English Translation
Did you love me lady
Anyway, I was so lonely
Ready now, let’s run together
Let’s go at the sound of the gunshot
Run as fast as you can
Goes around around
I wanna control my pace
But I’m always chasing after my heart
I think I’m already tired
I wanna take a break
Don’t just sprint one part
And then pass the baton
Perfectly run the entire course
Break me down
Running and running without a break
Break me down
My heart is running out
I’m running out of breath
I’m tired as if I’m running baby
Feels like my heart will explode
I’m trying to catch you, who is getting farther away (down down)
Follow me to the finish line of destiny
Where are we? It’s our fantasy world
I’ve been running the same laps
Running out of breath in my polo and jeans
I can’t hide my feelings for you anyway
I wonder why it just has to be you
Can’t I hold your hand? When I be your man
I hate seeing myself get smaller
Like dominos, I want to narrow our distance
I can’t fall behind or just pass by
The sweat we shed together
Flies away as tears
I can start to see the end
I’m completing it
Break me down
Running and running without a break
Break me down
My heart is running out
I’m running out of breath
I’m tired as if I’m running baby
Feels like my heart will explode
I’m trying to catch you, who is getting farther away (down down)
Just hold on
Faster than anyone else
Was you and I, why
Why do I wanna stop?
If I endure a little more, will I have victory?
Just one more step, don’t fall down
Don’t let go of my hand, running out of breath
I’m tired, as if I’ll explode, baby
Don’t let go of my hand, of my heart
I’m trying to catch you, who is getting farther away (down down)
So many things have changed
You pretend they haven’t
But now I need oxygen
I hope your last will be me
Let me be there
Hangul
Did you love me lady
Anyway, I was so lonely
Ready now, 달리기 함께
시작해 총소리 땅!
들리면 전력질주 해
Goes around around
내 페이스를 조절하고 싶은데
늘 앞서가는 나의 마음 따라가
벌써 나는 지쳤나 봐
쉬어가고 싶어져
한 번에 쏟아
버리고 바톤터치 하지 마
정해진 코스에
한 부분, 완주해
Break me down
(후우우우우)
쉴 틈 없이 뛰고 뛰어
Break me down
(후우우우우)
내 심장은 running out
가슴이 터질듯해 숨이 차올라
뛰는 것처럼 힘이 들어 baby
가슴이 터질듯해 내 맘 이렇게
멀어지는 너를 잡고 있어 (down down)
Follow me, 운명의 결승 line
여긴 어딘가? 너와 나의 fantasy world
똑같은 길만 대체 몇 바퀴
숨이 차 목 폴라티와 청바지
어차피 널 향한 내 맘은 감출 수 없어
왜 대체 너 말곤 안 되는지도 I wonder
네 손 잡을 순 없나 when I be your man
작아져 가는 모습은 싫어 도미노 같이
더 좁히고 싶은 이 간격
난 뒤로 뒤처지거나 그냥은 못 지나쳐
함께 흘렸던
땀들은 눈물로 날리고
끝이 보이기
시작해, 완주해
Break me down
(후우우우우)
쉴 틈 없이 뛰고 뛰어
Break me down
(후우우우우)
내 심장은 running out
가슴이 터질듯해 숨이 차올라
뛰는 것처럼 힘이 들어 baby
가슴이 터질듯해 내 맘 이렇게
멀어지는 너를 잡고 있어 (down down)
Just hold on 누구보다 빨랐던
너와 나 why
멈추고 싶은 건지
조금만 더 버티면 나 승리하게 될까
한 걸음만, 쓰러지지 마
내 손을 놓치지 마 숨이 차올라
터질 것처럼 힘이 들어 baby
내 손을 놓치지 마 내 맘 이렇게
멀어지는 너를 잡고 있어 (down down)
변했어 너무 많은 게 변했어
여전히 넌 아닌 척해도 이젠
거친 심호흡이 필요해져
부디 마지막은 나이길 let me be there
Romanization

[Bit-to]
Did you love me lady
Anyway, I was so lonely

[Hwanhee]
Ready now, dalligi hamkke
Shijakhae chongsori ttang!

[Kogyeol]
Deullimyeon jeonryeokjilju hae
Goes around around

[Jinhoo]
Nae peiseureul jojeolhago shipeunde
Neul apseoganeun naye maeum ttaraga

[Xiao]
Beolsseo naneun jichyeotna bwa
Swieogago shipeojyeo

[Sunyoul]
Han beone ssoda

[Gyujin]
Beorigo batonteochi haji ma

[Sunyoul]
Jeonghaejin koseue

[Gyujin]
Han bubun, wanjuhae

[Xiao]
Break me down
(huuuuu)

[Hwanhee]
Swil teum eopshi ttwigo ttwieo

[Xiao]
Break me down
(huuuuu)

[Jinhoo]
Nae shimjangeun running out

[Hwanhee]
Gaseumi teojildeuthae sumi chaolla
Ttwineun geotcheoreom himi deureo baby

[Sunyoul]
Gaseumi teojildeuthae nae mam ireoke
Meoreojineun neoreul jabgo isseo [Kuhn] (down down)

[Kogyeol]
Follow me, unmyeonge gyeolseung line

[Jinhoo]
Yeogin eodinga? neowa naye fantasy world

[Kuhn]
Ttokgateun gilman daeche myeot bakwi
Sumi cha mok pollatiwa cheongbaji
Eochapi neol hyanghan nae mameun gamchul su eopseo
Wae daeche neo malgon an doeneunjido I wonder

[Bit-to]
Ne son jabeul sun eomna when I be your man
Jagajyeo ganeun moseubeun shireo domino gachi
Deo jophigo shipeun i gangyeong
Nan dwiro dwicheojigeona geunyangeun mot jinachyeo

[Sunyoul]
Hamkke heullyeotdeon

[Gyujin]
Ttamdeureun nunmullo nalligo

[Sunyoul]
Kkeuchi boigi

[Gyujin]
Shijakhae, wanjuhae

[Xiao]
Break me down
(huuuuu)

[Hwanhee]
Swil teum eopshi ttwigo ttwieo

[Xiao]
Break me down
(huuuuu)

[Jinhoo]
Nae shimjangeun running out

[Hwanhee]
Gaseumi teojildeuthae sumi chaolla
Ttwineun geotcheoreom himi deureo baby

[Sunyoul]
Gaseumi teojildeuthae nae mam ireoke
Meoreojineun neoreul jabgo isseo [Kuhn] (down down)

[Hwanhee]
Just hold on nuguboda ppallatdeon
Neowa na why

[Kogyeol]
Meomchugo shipeun geonji

[Sunyoul]
Jogeumman deo beotimyeon na seungrihage doelkka

[Jinhoo]
Han georeumman, sseureojiji ma

[Sunyoul]
Nae soneul nochiji ma sumi chaolla
Teojil geotcheoreom himi deureo baby

[Hwanhee]
Nae soneul nochiji ma nae mam ireoke
Meoreojineun neoreul jabgo isseo [Kuhn] (down down)

[Wei]
Byeonhaesseo neomu maneun ge byeonhaesseo
Yeojeonhi neon anin cheokhaedo ijen
Geochin shimhoheubi piryohaejyeo
Budi majimageun naigil let me be there