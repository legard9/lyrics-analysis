<<<<<<< HEAD
                             
Chapter 1: Christmas Present
     
My name is Nancy Myfanwy Bright. My father liked the name      Nancy and I was called Myfanwy after my mother. I’m ninety-two      years of age and I’ve lived quietly in this cottage behind Bright’s      Shoes in Sticklepond all my life, so I don’t really know why you      want to record my memories for your archive, because it isn’t      going to be very interesting, is it, dear?
          Do help yourself to a slice of bara brith – it’s a sort of fruit loaf      made to my mother’s recipe. There’s another kind they call funeral      cake in the part of Wales Mother’s family came from, because it      was always served to the mourners after an interment. I’ve told      Tansy – that’s my great-niece – that she should do that when I pop      my clogs, too. I’ve taught her all Mother’s old recipes …
          Now, where were we?
                                                                  Middlemoss Living Archive
                                                                  Recordings: Nancy Bright.
As I drove out of London and headed north for Christmas my heart lifted with each passing mile. It always did, because West Lancashire – and, more specifically, the village of Sticklepond – was always going to feel like home to me. You can take the girl out of Lancashire, but you can’t take the Lancashire out of the girl …
     I would have moved back there like a flash, if it weren’t that my fiancé, Justin, was an orthopaedic consultant whose work was in London, not to mention his being so firmly tied to his widowed mother’s apron strings that he spent more time with Mummy in Tunbridge Wells than he did with me. And even when he wasn’t with Mummy Dearest, I still came second to his latest passion – golf.
     Justin’s mother was only one of the many things weighing on my mind – the sharp, pointy tip of the iceberg, you might say. She’d be staying at the flat in London while I was away and I knew from past experience that by the time I got back she would have thoroughly purged my unwanted presence from it by dumping all my possessions into the boxroom I used as a studio to write and illustrate my popular 
Slipper Monkey
 children’s books.
     I’d tried so hard to get on with her, but I was never going to be good enough for her beloved little boy. In fact, I once overheard her refer to me as ‘that bit of hippie trash you picked up on the plane back from India’, and though it’s true that Justin and I met after I was unexpectedly upgraded to the seat next to his in Business Class, I’m a couple of decades too young to have been any kind of hippie!
     I suppose many people did still go to India to ‘find themselves’, whatever they mean by that. In my case I’d gone to find my father. Now, he 
was
 an old hippie, if you like …
Still, at least I’d 
tried
 with Justin’s mother, which is more than he did on his one and only visit to Aunt Nan in Sticklepond, when he’d made it abundantly clear that he thought anything north of Watford was a barbaric region to be avoided at all costs, full of howling wolves, black puddings and men in flat caps with whippets.
     He did condescendingly describe Aunt Nan’s ancient stone cottage. set in a stone-flagged courtyard just off the High Street, its front room given over to a tiny shoe shop, as ‘quaint’. But then, that was before Aunt Nan made him sleep downstairs on the sofa in the parlour. I told him she disapproved of cohabitation before marriage so strongly that he was lucky she hadn’t taken a room for him at the Green Man next door, but he failed to see the funny side.
     Still, you can see why we’d spent our Christmases apart during our long engagement, not to mention many weekends too, what with him in Tunbridge Wells with Mummy (and a convenient golf course) and me heading home at least once a month – and more often than that, as Aunt Nan got frailer …
     Aunt Nan was actually my great-aunt, aged ninety-two, and as she kept reminding me, wouldn’t be around for ever. She’d brought me up and I adored her, so obviously I wanted to spend as much time with her as I could, but I also wanted her to see me married and with a family of my own, and so did she. And if I didn’t get a shift on, that last option would be closed to me for ever, another thing weighing on my mind.
     I knew it could be more difficult to get pregnant after thirty-five, so without telling Justin I’d booked myself into a clinic for a fertility MOT and the result had been a real wake-up call. The indication was that I had 
some
 eggs left, but probably not that many, so I needed to reach out and snatch the opportunity to have children before it vanished … if it hadn’t already.
     When Justin and I had first got engaged we were full of plans to marry and start a family, yet there we were, almost six years down the line, and he seemed to have lost interest in doing either. In fact, I could see that he was totally different from the man I fell in love with, though the change had happened so slowly I just hadn’t noticed. Perhaps it’s like that with all relationships and it takes a sudden shock to make you step back and take a good clear look at what’s been happening.
     I mainly blamed Mummy Dearest for poisoning Justin’s mind against me, dripping poisonous criticisms into his ear the whole time, though she hadn’t been so bad the first year – or maybe I’d been so in love I simply hadn’t registered it.
     Justin and I were such opposites, yet until the golf mania took hold, we used to love exploring the London parks together, and before he became such a skinflint, we used to go to a lot of musical theatre productions, too. When I first found out about Justin’s secret passion (we must have seen 
We Will Rock You
 five or six times!) I found it very endearing …
     As the radio cheered me on my way north with a succession of Christmas pop songs, I knew that when I got back to London we would need to do some 
serious
 talking.
Aunt Nan’s mind seemed to have been running along the same lines as mine, because she decided it was time for us to have a little heart-to-heart chat the very day after I arrived.
     My best friend, Bella, was looking after the shop and Aunt Nan had spent the first part of the morning shut away in the parlour with Cheryl Noakes, the archivist who was recording her memoirs for the Middlemoss Living Archive scheme. This seemed to perk up my aunt no end, despite awaking bittersweet memories, like the loss of her fiancé during the war.
     I’d shown Cheryl out and returned to collect the tray of coffee cups and any stray crumbs from the iced fairy cakes that she might have overlooked, when Aunt Nan said suddenly, ‘What will you do with the shop when I’m gone, lovey?’
     She was still sitting in her comfortable shabby armchair, a gaily coloured Afghan rug over her knees (she believed overheated houses were unhealthy, so the central heating, which I’d insisted she had put in, was always turned down really low), crocheting another doily for my already full-to-bursting bottom drawer.
     With a pang I realised how little room her once-plump frame took up in the chair now. When had she suddenly become so small and pale? And her curls, which had been as dark as her eyes, just like mine, were now purest silver …
     ‘Shouldn’t you leave it to Immy, Aunt Nan?’
     ‘No,’ she said uncompromisingly. ‘Your mother hates the place and she’s got more money than sense already, the flibbertigibbet! Anyway, she seems to be sticking with this last husband and making her home in America now.’
     ‘That’s true! Marrying a Californian plastic surgeon seems to have fulfilled all her wildest dreams.’
     Aunt Nan snorted. ‘She’s probably more plastic by now than a Barbie doll!’
     ‘Her face 
was
 starting to look a bit strange in that last picture she emailed me,’ I admitted. ‘All pulled up at the corners of her eyes, so they slanted like a cat’s. I hope she doesn’t overdo it. I didn’t realise you could have your knees lifted, did you? But she says you can and your knees show your age.’
     ‘She shouldn’t be showing her knees to anyone at her age. But there, that’s Imogen all over, shallow as a puddle from being a child. Except that she’s the spitting image of her mother, you’d think there wasn’t a scrap of Bright blood in her …’
     She paused, as if at some painful recollection, and then said firmly, ‘No, I’m passing on the shop and cottage to you, because you’re a true Bright and you come back every chance you get, like a homing pigeon.’
     ‘I do love the place, but I come back because I love you, too,’ I said, a few tears welling, ‘and I can’t bear to think of you gone.’
     ‘You great daft ha’porth,’ she said fondly. ‘You need to be practical about these things, because I’m ninety-two and I’ll be ready to go soon, like it or not!’
     ‘But do we have to talk about it now?’
     ‘Yes.’ She nodded her head in a very decided manner, her silver curls bobbing. ‘I’m not flaming immortal, you know! I’ll soon be shuffling off this mortal coil, as I told the vicar last time he called.’
     ‘Oh, Raffy Sinclair’s 
gorgeous!’
 I sighed, distracted by this mention of our new ex-rock star vicar.
     ‘He’s also very much married to Chloe Lyon that has the Chocolate Wishes shop, and they’ve got a baby now,’ Aunt Nan told me severely.
     ‘I know, and even if he wasn’t married, he’d still be way out of my league!’
     No one is out of your league, Tansy,’ she said. ‘The vicar’s a decent, kind man, for all his looks, and often pops in for a chat. And that Seth Greenwood from up at Winter’s End, he’s another who’s been good to me this last couple of years: I haven’t had to lift a hand in the garden other than to pick the herbs from my knot garden, and he or one of the gardeners from the hall keeps that trim and tidy, and looking a treat.’
     ‘Seth’s another big, attractive man, like the vicar: you’re a magnet for them!’ I teased.
     ‘I was at school with his father, Rufus, and I’ve known Hebe Winter for ever – has a hand in everything that goes on in Sticklepond, she does, despite her niece inheriting the hall.’
     ‘And marrying Seth. In fact, marrying the head gardener seems to be becoming a Winter tradition, doesn’t it?’
     ‘He and Sophy have got a baby too. There’s so many little ’uns around now, I’m starting to think they’re putting something in the water.’
     I felt a sudden, sharp, anguished pang, because when you’re desperate to have a baby, practically everyone else seems to have one, or be expecting one.
     But Nan had switched back to her original track. ‘I don’t suppose you’ll want to keep the shop open. Goodness knows, it’s been more of a hobby to me than a business the last few years, and I’d have had to close if Providence hadn’t sent Bella back to the village, looking for a job. The Lord moves in mysterious ways.’
     ‘He certainly does,’ I agreed, though I wasn’t sure that losing both her partner and her home in one fell swoop, and then being forced to move into the cramped annexe of her parents’ house with her five-year-old daughter, Tia, was something Bella saw in the light of Providence. But it had been a huge relief to me when she started working in the shop, because she could keep an eye on Aunt Nan for me too.
     ‘There’s been a Bright’s Shoes here since the first Bright set up as a cobbler and clog-maker way back, so I feel a bit sad that it’ll end with me. But there it is,’ Aunt Nan said. ‘Perhaps you and Justin could use the cottage as a holiday home – assuming you ever get round to marrying, that is, because I wouldn’t like to think of any immoral goings-on under this roof!’
     ‘Having the cottage as my very own bolthole in the north would be 
wonderful,’
 I agreed, ‘but I really don’t want to see Bright’s Shoes close down! Do you remember when you used to take me with you to the shoe warehouses in Manchester in the school holidays? You’d be searching for special shoes for some customer, or taking bridesmaids’ satin slippers to be dyed to match their dresses …’
     I could still recall the heady smell of leather in the warehouses and then the treat of tea in one of the big stores before we came back on the train. Not many shopkeepers nowadays would go all that way just to find the exact shoes one customer wanted, but then again, nowadays anyone but my aunt Nan would be tracking them down on the internet. That, together with vintage clothes fairs, was how I was amassing an ever-expanding collection of wedding shoes – or vintage shoes so pretty they ought to be wedding shoes. I was collecting them just for fun, but I only wished I had somewhere to display them all.
     ‘When you were a little girl you wanted to run the shop when you grew up and find the right Cinderella shoes, as you called them, for every bride.’
     ‘I remember that, and though I’m still not so interested in the wellies, school plimsolls and sensible-shoe side, I do love the way you’ve expanded the wedding shoe selection. I’ve wondered about the possibility of having a shop that specialises in bridal shoes.’
     ‘Would there be enough custom? It’s only been a sideline,’ Aunt Nan said doubtfully. ‘You don’t get much passing trade here either, being tucked away down Salubrious Passage, as we are.’
     ‘Oh, yes, because people will travel to a specialist shop once they know you’re there. I could advertise on the internet, and my shop would stock some genuine vintage bridal shoes as well as vintage-styled ones, so that would be a fairly unusual selling point,’ I enthused.
     ‘That would be different,’ Aunt Nan agreed. ‘But wouldn’t you have the bread-and-butter lines still, like purses and polish and shoelaces?’
     ‘No, not unless I could find shoe-shaped purses! In fact, I could sell all kinds of shoe-shaped things – jewellery, stationery, wedding favours, whatever I could find,’ I said thoughtfully, ‘because I’d be mad not to tap into the tourist trade too, wouldn’t I? I mean, the village has become a hotspot between Easter and autumn, since the discovery of that Shakespeare manuscript up at Winter’s End. The gardens are a draw too, now Seth has finished restoring the knot gardens on the terraces, and then you get the arty lot who want to see Ottie Winter’s sculpture in the garden and maybe even a glimpse of the great artist herself!’
     Aunt Nan nodded. ‘Yes, that’s very true. And when they’ve been to Winter’s End, they usually come into the village, what with the Witchcraft Museum and then the craft galleries and teashops and the pubs. The Green Man still does most of the catering for lunches and dinners, but Florrie’s installed a coffee machine in the snug at the Falling Star and puts out a sign, and she says they get quite a bit of passing trade. You’d be amazed what people are prepared to pay for a cup of coffee with a bit of froth on it.’
     Florrie Snowball was Aunt Nan’s greatest friend and, although the same age, showed no signs of flagging. Aunt Nan said this was because she’d sold her soul to the devil, involved as she was in some kind of occult group run by the proprietor of the Witchcraft Museum, Gregory Lyon, but it doesn’t seem to have affected their friendship.
     ‘I’m sure I could make a go of it!’ I said, starting to feel excited. Until all these plans had suddenly come pouring out, I hadn’t realised just how much I’d been thinking about it.
     Aunt Nan brought me back to earth with a bump. ‘But, Tansy, if you marry Justin, then you’ll make your home in London, won’t you?’
     ‘He could get a job up here,’ I suggested, though I sounded unconvincing even to myself. Justin could be transferred to a Lancashire hospital, but I was sure he wouldn’t want to. And even if he did want that, Mummy Dearest would have something to say about it!
     ‘I can’t see Justin doing that,’ Aunt Nan said.
     ‘Even if he won’t, Bella could manage the shop for me and I could divide my time between London and Sticklepond,’ I suggested, though suddenly I really, 
really
 wanted to do it myself! ‘Anyway, we needn’t think about that now, because you’re not going to leave me for years yet, and until then, Bella can run things just the way they’ve always been.’
     ‘I keep telling you I’m on the way out, and you’re not listening, you daft lump,’ my aunt said crossly. ‘After that rheumatic fever I had at eleven they said I wouldn’t make old bones, but they were wrong about that! But now I’m wearing out. One day soon, my cogs will stop turning altogether and I’ll be ready to meet my Maker. I’d hoped to see you married and with a family by then, though.’
     ‘Yes, me too, and it’s what Justin seemed to want when we got engaged … yet we haven’t even tied the knot yet!’
     ‘That’s what comes of living with a man before the ring’s on your finger,’ Aunt Nan said severely. ‘They’ve no reason to wed you, then.’
     ‘Things have changed, Aunt Nan – and I 
do
 have a ring on my finger.’ I twiddled my solitaire diamond.
     ‘Things haven’t changed for the better, and if he wants a family he should realise that time’s passing and you’re thirty-six – starting to cut it close.’
     ‘I know, though time has slipped by so quickly that I’ve only just woken up to the fact.’
     ‘I don’t know why you didn’t marry long since.’
     ‘Neither do I, though Justin does seem to have a thing about my weight. I thought he was joking when he said he’d set the wedding date when I was a size eight, but no, he was entirely serious! Only my diets always seem to fail, and then I put a few more pounds on after each attempt.’
     ‘He should leave well alone, then,’ she said tartly. ‘You’re a small, dark Bright, like me, and we plumpen as we get older. And, a woman’s meant to have a bit of padding, not be a rack of ribs.’
     ‘It’s not just my weight, but everything about me that seems to irritate him now. I think his mother keeps stirring him up and making him so critical. For instance, he used to say the way I dressed was eccentric and cute, but now he seems to want me to look like all his friends’ wives and girlfriends.’
     ‘There’s nowt wrong with the way you look,’ Aunt Nan said loyally, though even my close friends are prone to comment occasionally on the eccentricity of my style. ‘He can’t remodel you like an old coat to suit himself, he needs to love you for what you are.’
     ‘If he 
does
 still love me! He 
says
 he does, but is that the real me, or some kind of Stepford Wife vision he wants me to turn into?’ I sighed. ‘No, I’ve been drifting with the tide for too long and after Christmas I’m going to find out one way or the other!’
     ‘You do that,’ Aunt Nan agreed, ‘because there are lots of other fish in the sea if you want to throw him back.’
     I wasn’t too sure about that. I’d only ever loved two men in my life (if you count my first brief encounter as one of them) so the stock of my particular kind of fish was obviously already dangerously depleted.
     ‘If I want to have children, I’ve left it a bit late to start again with someone else,’ I said sadly, ‘and although Justin’s earning a good salary he’s turned into a total skinflint and says we can’t afford to have children yet – they’re way too expensive – but then, I expect he thinks our children would have a nanny and go to a private school, like he did, and of course I wouldn’t want that.’
     ‘He doesn’t seem much of a man to me at all,’ Aunt Nan said disparagingly. ‘But I’m not the one in love with him.’
     ‘He has his moments,’ I said, thinking of past surprises, like tickets to see a favourite musical, romantic weekends in Paris, or the trip to Venice he booked on the Orient Express, which gave me full rein to raid the dressing-up box …
=======
                             Chapter 1: Christmas Present

     My name is Nancy Myfanwy Bright. My father liked the name      Nancy and I was called Myfanwy after my mother. I’m ninety-two      years of age and I’ve lived quietly in this cottage behind Bright’s      Shoes in Sticklepond all my life, so I don’t really know why you      want to record my memories for your archive, because it isn’t      going to be very interesting, is it, dear?

          Do help yourself to a slice of bara brith – it’s a sort of fruit loaf      made to my mother’s recipe. There’s another kind they call funeral      cake in the part of Wales Mother’s family came from, because it      was always served to the mourners after an interment. I’ve told      Tansy – that’s my great-niece – that she should do that when I pop      my clogs, too. I’ve taught her all Mother’s old recipes …

          Now, where were we?

                                                                  Middlemoss Living Archive
                                                                  Recordings: Nancy Bright.

As I drove out of London and headed north for Christmas my heart lifted with each passing mile. It always did, because West Lancashire – and, more specifically, the village of Sticklepond – was always going to feel like home to me. You can take the girl out of Lancashire, but you can’t take the Lancashire out of the girl …

     I would have moved back there like a flash, if it weren’t that my fiancé, Justin, was an orthopaedic consultant whose work was in London, not to mention his being so firmly tied to his widowed mother’s apron strings that he spent more time with Mummy in Tunbridge Wells than he did with me. And even when he wasn’t with Mummy Dearest, I still came second to his latest passion – golf.

     Justin’s mother was only one of the many things weighing on my mind – the sharp, pointy tip of the iceberg, you might say. She’d be staying at the flat in London while I was away and I knew from past experience that by the time I got back she would have thoroughly purged my unwanted presence from it by dumping all my possessions into the boxroom I used as a studio to write and illustrate my popular Slipper Monkey children’s books.

     I’d tried so hard to get on with her, but I was never going to be good enough for her beloved little boy. In fact, I once overheard her refer to me as ‘that bit of hippie trash you picked up on the plane back from India’, and though it’s true that Justin and I met after I was unexpectedly upgraded to the seat next to his in Business Class, I’m a couple of decades too young to have been any kind of hippie!

     I suppose many people did still go to India to ‘find themselves’, whatever they mean by that. In my case I’d gone to find my father. Now, he was an old hippie, if you like …

Still, at least I’d tried with Justin’s mother, which is more than he did on his one and only visit to Aunt Nan in Sticklepond, when he’d made it abundantly clear that he thought anything north of Watford was a barbaric region to be avoided at all costs, full of howling wolves, black puddings and men in flat caps with whippets.

     He did condescendingly describe Aunt Nan’s ancient stone cottage. set in a stone-flagged courtyard just off the High Street, its front room given over to a tiny shoe shop, as ‘quaint’. But then, that was before Aunt Nan made him sleep downstairs on the sofa in the parlour. I told him she disapproved of cohabitation before marriage so strongly that he was lucky she hadn’t taken a room for him at the Green Man next door, but he failed to see the funny side.

     Still, you can see why we’d spent our Christmases apart during our long engagement, not to mention many weekends too, what with him in Tunbridge Wells with Mummy (and a convenient golf course) and me heading home at least once a month – and more often than that, as Aunt Nan got frailer …

     Aunt Nan was actually my great-aunt, aged ninety-two, and as she kept reminding me, wouldn’t be around for ever. She’d brought me up and I adored her, so obviously I wanted to spend as much time with her as I could, but I also wanted her to see me married and with a family of my own, and so did she. And if I didn’t get a shift on, that last option would be closed to me for ever, another thing weighing on my mind.

     I knew it could be more difficult to get pregnant after thirty-five, so without telling Justin I’d booked myself into a clinic for a fertility MOT and the result had been a real wake-up call. The indication was that I had some eggs left, but probably not that many, so I needed to reach out and snatch the opportunity to have children before it vanished … if it hadn’t already.

     When Justin and I had first got engaged we were full of plans to marry and start a family, yet there we were, almost six years down the line, and he seemed to have lost interest in doing either. In fact, I could see that he was totally different from the man I fell in love with, though the change had happened so slowly I just hadn’t noticed. Perhaps it’s like that with all relationships and it takes a sudden shock to make you step back and take a good clear look at what’s been happening.

     I mainly blamed Mummy Dearest for poisoning Justin’s mind against me, dripping poisonous criticisms into his ear the whole time, though she hadn’t been so bad the first year – or maybe I’d been so in love I simply hadn’t registered it.

     Justin and I were such opposites, yet until the golf mania took hold, we used to love exploring the London parks together, and before he became such a skinflint, we used to go to a lot of musical theatre productions, too. When I first found out about Justin’s secret passion (we must have seen We Will Rock You five or six times!) I found it very endearing …

     As the radio cheered me on my way north with a succession of Christmas pop songs, I knew that when I got back to London we would need to do some serious talking.

Aunt Nan’s mind seemed to have been running along the same lines as mine, because she decided it was time for us to have a little heart-to-heart chat the very day after I arrived.

     My best friend, Bella, was looking after the shop and Aunt Nan had spent the first part of the morning shut away in the parlour with Cheryl Noakes, the archivist who was recording her memoirs for the Middlemoss Living Archive scheme. This seemed to perk up my aunt no end, despite awaking bittersweet memories, like the loss of her fiancé during the war.

     I’d shown Cheryl out and returned to collect the tray of coffee cups and any stray crumbs from the iced fairy cakes that she might have overlooked, when Aunt Nan said suddenly, ‘What will you do with the shop when I’m gone, lovey?’

     She was still sitting in her comfortable shabby armchair, a gaily coloured Afghan rug over her knees (she believed overheated houses were unhealthy, so the central heating, which I’d insisted she had put in, was always turned down really low), crocheting another doily for my already full-to-bursting bottom drawer.

     With a pang I realised how little room her once-plump frame took up in the chair now. When had she suddenly become so small and pale? And her curls, which had been as dark as her eyes, just like mine, were now purest silver …

     ‘Shouldn’t you leave it to Immy, Aunt Nan?’

     ‘No,’ she said uncompromisingly. ‘Your mother hates the place and she’s got more money than sense already, the flibbertigibbet! Anyway, she seems to be sticking with this last husband and making her home in America now.’

     ‘That’s true! Marrying a Californian plastic surgeon seems to have fulfilled all her wildest dreams.’

     Aunt Nan snorted. ‘She’s probably more plastic by now than a Barbie doll!’

     ‘Her face was starting to look a bit strange in that last picture she emailed me,’ I admitted. ‘All pulled up at the corners of her eyes, so they slanted like a cat’s. I hope she doesn’t overdo it. I didn’t realise you could have your knees lifted, did you? But she says you can and your knees show your age.’

     ‘She shouldn’t be showing her knees to anyone at her age. But there, that’s Imogen all over, shallow as a puddle from being a child. Except that she’s the spitting image of her mother, you’d think there wasn’t a scrap of Bright blood in her …’

     She paused, as if at some painful recollection, and then said firmly, ‘No, I’m passing on the shop and cottage to you, because you’re a true Bright and you come back every chance you get, like a homing pigeon.’

     ‘I do love the place, but I come back because I love you, too,’ I said, a few tears welling, ‘and I can’t bear to think of you gone.’

     ‘You great daft ha’porth,’ she said fondly. ‘You need to be practical about these things, because I’m ninety-two and I’ll be ready to go soon, like it or not!’

     ‘But do we have to talk about it now?’

     ‘Yes.’ She nodded her head in a very decided manner, her silver curls bobbing. ‘I’m not flaming immortal, you know! I’ll soon be shuffling off this mortal coil, as I told the vicar last time he called.’

     ‘Oh, Raffy Sinclair’s gorgeous!’ I sighed, distracted by this mention of our new ex-rock star vicar.

     ‘He’s also very much married to Chloe Lyon that has the Chocolate Wishes shop, and they’ve got a baby now,’ Aunt Nan told me severely.

     ‘I know, and even if he wasn’t married, he’d still be way out of my league!’

     No one is out of your league, Tansy,’ she said. ‘The vicar’s a decent, kind man, for all his looks, and often pops in for a chat. And that Seth Greenwood from up at Winter’s End, he’s another who’s been good to me this last couple of years: I haven’t had to lift a hand in the garden other than to pick the herbs from my knot garden, and he or one of the gardeners from the hall keeps that trim and tidy, and looking a treat.’

     ‘Seth’s another big, attractive man, like the vicar: you’re a magnet for them!’ I teased.

     ‘I was at school with his father, Rufus, and I’ve known Hebe Winter for ever – has a hand in everything that goes on in Sticklepond, she does, despite her niece inheriting the hall.’

     ‘And marrying Seth. In fact, marrying the head gardener seems to be becoming a Winter tradition, doesn’t it?’

     ‘He and Sophy have got a baby too. There’s so many little ’uns around now, I’m starting to think they’re putting something in the water.’

     I felt a sudden, sharp, anguished pang, because when you’re desperate to have a baby, practically everyone else seems to have one, or be expecting one.

     But Nan had switched back to her original track. ‘I don’t suppose you’ll want to keep the shop open. Goodness knows, it’s been more of a hobby to me than a business the last few years, and I’d have had to close if Providence hadn’t sent Bella back to the village, looking for a job. The Lord moves in mysterious ways.’

     ‘He certainly does,’ I agreed, though I wasn’t sure that losing both her partner and her home in one fell swoop, and then being forced to move into the cramped annexe of her parents’ house with her five-year-old daughter, Tia, was something Bella saw in the light of Providence. But it had been a huge relief to me when she started working in the shop, because she could keep an eye on Aunt Nan for me too.

     ‘There’s been a Bright’s Shoes here since the first Bright set up as a cobbler and clog-maker way back, so I feel a bit sad that it’ll end with me. But there it is,’ Aunt Nan said. ‘Perhaps you and Justin could use the cottage as a holiday home – assuming you ever get round to marrying, that is, because I wouldn’t like to think of any immoral goings-on under this roof!’

     ‘Having the cottage as my very own bolthole in the north would be wonderful,’ I agreed, ‘but I really don’t want to see Bright’s Shoes close down! Do you remember when you used to take me with you to the shoe warehouses in Manchester in the school holidays? You’d be searching for special shoes for some customer, or taking bridesmaids’ satin slippers to be dyed to match their dresses …’

     I could still recall the heady smell of leather in the warehouses and then the treat of tea in one of the big stores before we came back on the train. Not many shopkeepers nowadays would go all that way just to find the exact shoes one customer wanted, but then again, nowadays anyone but my aunt Nan would be tracking them down on the internet. That, together with vintage clothes fairs, was how I was amassing an ever-expanding collection of wedding shoes – or vintage shoes so pretty they ought to be wedding shoes. I was collecting them just for fun, but I only wished I had somewhere to display them all.

     ‘When you were a little girl you wanted to run the shop when you grew up and find the right Cinderella shoes, as you called them, for every bride.’

     ‘I remember that, and though I’m still not so interested in the wellies, school plimsolls and sensible-shoe side, I do love the way you’ve expanded the wedding shoe selection. I’ve wondered about the possibility of having a shop that specialises in bridal shoes.’

     ‘Would there be enough custom? It’s only been a sideline,’ Aunt Nan said doubtfully. ‘You don’t get much passing trade here either, being tucked away down Salubrious Passage, as we are.’

     ‘Oh, yes, because people will travel to a specialist shop once they know you’re there. I could advertise on the internet, and my shop would stock some genuine vintage bridal shoes as well as vintage-styled ones, so that would be a fairly unusual selling point,’ I enthused.

     ‘That would be different,’ Aunt Nan agreed. ‘But wouldn’t you have the bread-and-butter lines still, like purses and polish and shoelaces?’

     ‘No, not unless I could find shoe-shaped purses! In fact, I could sell all kinds of shoe-shaped things – jewellery, stationery, wedding favours, whatever I could find,’ I said thoughtfully, ‘because I’d be mad not to tap into the tourist trade too, wouldn’t I? I mean, the village has become a hotspot between Easter and autumn, since the discovery of that Shakespeare manuscript up at Winter’s End. The gardens are a draw too, now Seth has finished restoring the knot gardens on the terraces, and then you get the arty lot who want to see Ottie Winter’s sculpture in the garden and maybe even a glimpse of the great artist herself!’

     Aunt Nan nodded. ‘Yes, that’s very true. And when they’ve been to Winter’s End, they usually come into the village, what with the Witchcraft Museum and then the craft galleries and teashops and the pubs. The Green Man still does most of the catering for lunches and dinners, but Florrie’s installed a coffee machine in the snug at the Falling Star and puts out a sign, and she says they get quite a bit of passing trade. You’d be amazed what people are prepared to pay for a cup of coffee with a bit of froth on it.’

     Florrie Snowball was Aunt Nan’s greatest friend and, although the same age, showed no signs of flagging. Aunt Nan said this was because she’d sold her soul to the devil, involved as she was in some kind of occult group run by the proprietor of the Witchcraft Museum, Gregory Lyon, but it doesn’t seem to have affected their friendship.

     ‘I’m sure I could make a go of it!’ I said, starting to feel excited. Until all these plans had suddenly come pouring out, I hadn’t realised just how much I’d been thinking about it.

     Aunt Nan brought me back to earth with a bump. ‘But, Tansy, if you marry Justin, then you’ll make your home in London, won’t you?’

     ‘He could get a job up here,’ I suggested, though I sounded unconvincing even to myself. Justin could be transferred to a Lancashire hospital, but I was sure he wouldn’t want to. And even if he did want that, Mummy Dearest would have something to say about it!

     ‘I can’t see Justin doing that,’ Aunt Nan said.

     ‘Even if he won’t, Bella could manage the shop for me and I could divide my time between London and Sticklepond,’ I suggested, though suddenly I really, really wanted to do it myself! ‘Anyway, we needn’t think about that now, because you’re not going to leave me for years yet, and until then, Bella can run things just the way they’ve always been.’

     ‘I keep telling you I’m on the way out, and you’re not listening, you daft lump,’ my aunt said crossly. ‘After that rheumatic fever I had at eleven they said I wouldn’t make old bones, but they were wrong about that! But now I’m wearing out. One day soon, my cogs will stop turning altogether and I’ll be ready to meet my Maker. I’d hoped to see you married and with a family by then, though.’

     ‘Yes, me too, and it’s what Justin seemed to want when we got engaged … yet we haven’t even tied the knot yet!’

     ‘That’s what comes of living with a man before the ring’s on your finger,’ Aunt Nan said severely. ‘They’ve no reason to wed you, then.’

     ‘Things have changed, Aunt Nan – and I do have a ring on my finger.’ I twiddled my solitaire diamond.

     ‘Things haven’t changed for the better, and if he wants a family he should realise that time’s passing and you’re thirty-six – starting to cut it close.’

     ‘I know, though time has slipped by so quickly that I’ve only just woken up to the fact.’

     ‘I don’t know why you didn’t marry long since.’

     ‘Neither do I, though Justin does seem to have a thing about my weight. I thought he was joking when he said he’d set the wedding date when I was a size eight, but no, he was entirely serious! Only my diets always seem to fail, and then I put a few more pounds on after each attempt.’

     ‘He should leave well alone, then,’ she said tartly. ‘You’re a small, dark Bright, like me, and we plumpen as we get older. And, a woman’s meant to have a bit of padding, not be a rack of ribs.’

     ‘It’s not just my weight, but everything about me that seems to irritate him now. I think his mother keeps stirring him up and making him so critical. For instance, he used to say the way I dressed was eccentric and cute, but now he seems to want me to look like all his friends’ wives and girlfriends.’

     ‘There’s nowt wrong with the way you look,’ Aunt Nan said loyally, though even my close friends are prone to comment occasionally on the eccentricity of my style. ‘He can’t remodel you like an old coat to suit himself, he needs to love you for what you are.’

     ‘If he does still love me! He says he does, but is that the real me, or some kind of Stepford Wife vision he wants me to turn into?’ I sighed. ‘No, I’ve been drifting with the tide for too long and after Christmas I’m going to find out one way or the other!’

     ‘You do that,’ Aunt Nan agreed, ‘because there are lots of other fish in the sea if you want to throw him back.’

     I wasn’t too sure about that. I’d only ever loved two men in my life (if you count my first brief encounter as one of them) so the stock of my particular kind of fish was obviously already dangerously depleted.

     ‘If I want to have children, I’ve left it a bit late to start again with someone else,’ I said sadly, ‘and although Justin’s earning a good salary he’s turned into a total skinflint and says we can’t afford to have children yet – they’re way too expensive – but then, I expect he thinks our children would have a nanny and go to a private school, like he did, and of course I wouldn’t want that.’

     ‘He doesn’t seem much of a man to me at all,’ Aunt Nan said disparagingly. ‘But I’m not the one in love with him.’

     ‘He has his moments,’ I said, thinking of past surprises, like tickets to see a favourite musical, romantic weekends in Paris, or the trip to Venice he booked on the Orient Express, which gave me full rein to raid the dressing-up box …

>>>>>>> master
     But all that was in the first heady year or so after we fell in love. Then the romance slowly tailed off … How was it that I hadn’t noticed when the music stopped playing?