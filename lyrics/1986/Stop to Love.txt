[Intro: MOON]
Can I come in

[Verse 1]
Neoboda aleumdaun yeojadeul around
Geuleonde naneun gwansim hana eobs-eo
Geunyang gidaligo iss-eullae ne yeonlag
Apdwiga hanado maj-ji anhneun mal-ieodo
Gwalho yeolgo eoseolpeun ping-gyeneun gwalho dadgo
Nae mal-i maj-janh-a, ne ma-eum-i nal wonhae
Eotteon iyudeunji sang-gwan-eobs-eo gyesog deo hae
Neoegeneun jinjja an twing-gil geoya
Naneun anya gita

[Pre-Hook]
Dasi yag-eul han al poppin'
Jam-e mos deul-eo du al-eul pop-poppin'
Geuttae geu sigan-e naneun gadhin ge hwagsilhae
Se beonjjae I gotta stop it
Neoman iss-eossdamyeon haegyeoldoeji
Chueogdeullo dasi rehabbin’

[Hook]
Naneun addicted to your love
Naneun addicted to your love
Naneun addicted to your love
Addict, naneun addict
Naneun addicted to your love
Addict, naneun addict
Naneun addicted to your love

[Verse 2]
Neoboda aleumdaun yeoja two
Du beonjjae malgo salam su
Sigan-eun saebyeog dulhago 20, eum, bunjjeum
Mannassdan ge aniya
Jeongmallo ttan ge anila
Munja myeoch tong-i daya, TMI
And I'm watchin’ the clock
Dasi ne saeng-gag-i nagi ttaemun-e

[Pre-Hook]
Dasi yag-eul han al poppin'
Jam-e mos deul-eo du al-eul pop-poppin'
Geuttae geu sigan-e naneun gadhin ge hwagsilhae
Se beonjjae I gotta stop it
Neoman iss-eossdamyeon haegyeoldoeji
Chueogdeullo dasi rehabbin’

[Hook]
Naneun addicted to your love
Naneun addicted to your love
Naneun addicted to your love
Addict, naneun addict
Naneun addicted to your love
Addict, naneun addict
Naneun addicted to your love

[Verse 3]
At first time
Geulae, geuttae, first time
Dasi geuttae cheoeum nal mannass-eul ttae deoun nal
Neon imi nae geosman gat-assneunde jeongmal
We felt that way
Seolma jigeum neodo
Geuttaeui seomgwang gat-assdeon chalnaleul
Neukkyeossdamyeon ama
Naega ttag geogi jjeol-eo sal-a

[Hook]
Naneun addicted to your love
Naneun addicted to your love
Naneun addicted to your love
Addict, naneun addict
Naneun addicted to your love
Addict, naneun addict
Naneun addicted to your love