Ulysses did not share his memories with her. Instead, he put his story into words, images, moving images, and composed a symphony. Let her, if she would, imagine what it was like: he was too fierce and too honorable a man to inflict what he had suffered on her.

The Renunciants had sailed from Sol to Canopus by launching laser. The laser was cut off when Earth died. Without the laser light to tack against, without external sources of energy, the fleet was forced to burn whole ships into reaction mass, lest they overshoot their destination. The larger and less-human thinking machines coolly volunteered for suicide; martyrs, and there was no storage space to save them.

From Earth, there was no last emergency narrowcast of noumenal information, no warning cry. An examination of the embedded messages sent in the final seconds of the laser stream revealed only routine comments. Then—silence.

At Canopus, less than a light-century from Earth, the Diaspora paused for many centuries, sophoforming certain planets found there, and cannibalizing their immense vessels to shipwright many smaller ones. This was the birth of Ulysses, who was dispatched toward Eta Carina.

Off into the long darkness he went. Mostly, he dreamed: even computers must run routines in their subsystems to do error-checking and-correcting, or exercise their minds to keep themselves sane. Understanding the mechanics behind thought had not alleviated man from the limitations of thought.

And his dreams were all of war: he saw the Earth on fire, smelled burned flesh, heard the screams of orphans, and the thinner wails of babies clenched in a dead mother’s arms, seeking to suckle and finding no milk at the lifeless breast. In those dreams he saw the Swans: figures in faceless silver faceplates, under elfin coronets of nodding spindles and plumes, robed in peacock-hued fabrics, wearing gauntlets crusted and begemmed with sophotechnic circuitry and thought-ports.

Once he woke. He was passing near one of those sunless bodies, something larger than Uranus, a globe of silicarbons paved in dark methane ice, which were surprisingly common in interstellar space. It had its own panoply of rings and little moons. The lifeless world dwindled beyond instrument range, and was gone. As the discoverer, he had the privilege of naming it: he called the rogue world Elpenor.

That was all. There was nothing else to look at. Again he hibernated.

When he reached the Eta Carina system, he cannibalized his empty engines and ceased to be a ship. He ate the nearby planetoids and put on weight and became a world with a wide orbit.

He was a watery world, covered with oceans from pole to pole. Storing water above his decks solved certain radiation problems, and allowed him to retire an expensive artificial Van Allen belt. More for decoration than anything else, he used his oceans as aquariums, bringing forth dolphins and whales and other extinct species out of his digital genetic archives. Cetaceans played and sported under skies of fire, for even at one thousand AU’s, distant Eta Carina A and B were monsters, variable stars with strangely pulsating cores.

Sending out remotes, he gathered the rich material from the nebulae, microengineered and dumped it (in the form of a billion tons of hungry nanomachine assemblers) on some unsuspecting ice giant of a world, and from its hulk constructed a broadcast antenna. Oh, how he wished for telepathy or tachyons or some way to outwit the limits of the spacetime: but the universe had only provided itself with exactly one electromagnetic spectrum, and more exotic ways of transmitting information did not operate at macroscopic scales. Ulysses could build nothing fundamentally different, merely larger, than what Marconi had built, back in the days of the Second Mental Structure. He built an antenna and radioed his findings to Canopus, over seven thousand light-years away.

His report said, in brief, that no one in his right mind would want to live anywhere near Eta Carina.

The sun was wavering near that tipping point where outward nuclear pressure from fusion could no longer equal the inward pressure of gravity. It was a powder-keg of a system, a Vesuvius waiting to blow. The size and instability of the main star, and its iron core of stellar ash staggering ever nearer to critical mass, suggested that when it collapsed and exploded, it would not be a nova, but a hypernova, such as have been seen in distant galaxies, the origin of exceptionally bright gamma-ray bursts.

The Diaspora at Canopus debated the options. Xi Puppis, Miaplacidus, the cluster at M93, were closer and more stable. The star HD70642 was known to have a Neptune-size world inhabitable to the Neptune-adapted Eremites organizing the expedition. The star HD 69830 was observed to have an asteroid belt rich in rare minerals, the preferred habitat of the microgravity-adapted Invariants. NGC 2423–3 b, also called Mayor’s Star, in the open cluster NGC 2423, boasted a super-jovian world ten times the size of Jupiter, with the type of collapsed-matter diamond core that made sophogenesis of a megascale logic diamond so practicable. All these stars were closer than Eta Carina not by tens or hundreds, but by thousands of light- years. All were in the Orion Arm.

Eta Carina was the worst choice. And so, by the backward logic of the Warlocks, it was the last place anyone would look for them.

A megascale structure surrounded Canopus, magnetically squeezing the star like an orange. The fields released a vent of energy, which a series of transformation rings gathered, lased, focused, and aimed. No one can see a laser in a vacuum, unless he steps in the path. If any eyes were watching Canopus, they saw the output dim, and knew the Diaspora was setting sail, but there was no way to detect toward which point of the compass that vast wash of energy was directed.

(A mystery surrounds the decision. An examination of the thought-patterns kept in record, or reconstructed by paleopsychoarcheologists, reveals an anomaly. When the same debate is run with the same minds with the exact same thoughts in modern simulation, the simulations reach a different result. This implies that a virus-thought altered the outcome. Who now knows what actually influenced them?)

Meanwhile, for ten millennia, Ulysses lived alone with his fish, and a taciturn chess partner dubbed Other-Ulysses.

Ulysses had, as part of his operational psychology, a memory casket containing a personality (based on Cold Duke psychological templates) capable of never being lonely, capable of facing unflinchingly the fact that he would never see another human being or human machine again.

All he had to do was open it, and his capacity for love, his desire for it, would be burned away forever. The new him would never go back to human psychology because it would never be able to imagine any reason to do so.

Ulysses was actually toying with the locks on that casket when messages came from the orbital telescopes his remotes had sent out, that the star Canopus was blazing like the eye of a Cyclopes, burning like the Bethlehem Star.

In a delirium of happy disbelief, he began to make ready the radiation-poisoned wilderness of Eta Carina for human habitation.

By the time the fleet from Canopus arrived, the system was filled with dolphins.