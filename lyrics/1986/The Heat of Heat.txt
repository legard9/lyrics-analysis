Something you know isn't right
The reasons somehow are all wrong
But you’re caught in a moment
In the heat of the night
Where compassion just doesn't belong

Nowhere to hide
You got no place to run
And no need to understand
You can fight it for a moment
Fight for your rights
But you fight like a man

'Cause in the heat of the night
In a moment of passion
In the heat of the night
There’s a chain reaction
In the heat of the night
When no one is ever to blame

Someone is looking at you
Someone who wants you tonight
You turn for a while
To see that look in his eyes
That no one could ever deny

You try to resist
You try to be strong
But you know your control is all gone
You can't understand
You don't have a reason
You can't let him go

'Cause in the heat of the night
In a moment of passion
In the heat of the night
There's a strange attraction
In the heat of the night
When no one remembers your name

In the heat of the night
In a moment of passion
In the heat of the night
You can feel the action
In the heat of the night
When no one is ever to blame

In the heat of the night
In a moment of passion
In the heat of the night
There's a chain reaction
In the heat of the night
When no one remembers your name