We're the Beta Band and we're nice and clean
We're always polite and hardly ever mean
Times have changed, we used to be smelly
We lived in a squat 'til a punk nicked our telly

Since we've been signed we eat real good
We always wash our hands and chew our food
Quashie's rotis are our favorite dish
Served spicy with chicken or fish

In the world of fashion there's two shining lights
Nancy and Caroline they helped us alright
And Gordon Anderson played a big part
Just when the Beta Band was about to start

It all started in London town
When we gave our demo to Phil Brown
Then we met Miles at Parlophone
And let him hear "Dog's Got a Bone"

He said go to Falkner and put it on wax
So the next week was spent cutting four tracks
A manager was needed to make us complete
Miles knew a Wigner we should meet

(Dedicated to KRS-One
All the people out there
The GZA, the RZA, the RZA
Here we go now)

His name was Dave with the broken specs
A quiet manner and enough respect
We met at Nachos, cleaned the plate
A meal like that yo we never ate

Then we met Brian and the Microdot crew
Martin, Matt and John Platt too
Adrenalin Village was the place to be
We had no money so they got us in free

The first thing Dave did as boss
Make us play Water Rats in Kings Cross
We played five songs, got credit for four
Went down well so we decided to tour

Next up, yo Henry and Nich
Making the show go without a hitch
Up and down the country and side to side
The big yin driving yo bumpy ride

Miles gave us an album deal
We said yes and went for a meal
Drinking champagne at EMI
The irony almost made us cry

(Big love, motherfucker, big love, stone cold love, stone cold love, man, all over Fife man, this one's for Scotland man, Scotland)

(Yo yo yo yo, we're going into the next section now, it's gonna be rock 'n' roll, man, c'mon, here's - that - sound, yoyoyo-yo-yo)

Robin Jones, has got the-

Well we went to Wales and we fannied around
We ended up with "The Patty Patty Sound"
With an open mind, we went to achieve
Chris said ???? you got it all on me

?????

Well we started to get known on Radio 1
When Jo Whiley got a fax from my mum
Mary Ann Hobbs is a very nice lady
I paid a man ??there every day

We lived in this house, it was record-free
And we started to record our third EP
With a lot of sax and a mardi gras
All we needed was a little soul

Well we thought this time where are we gonna start
We made a new groove and ???
????
We called it Los Amigos Del Beta Bandidos, baby