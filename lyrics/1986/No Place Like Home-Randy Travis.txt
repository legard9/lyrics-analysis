Well there's my easy chair, just sittin' there
I've spent a lot of time
Thinkin' of this perfect love
I know is yours and mine
And oh, I know how I love that old picture on the wall
Of you and me and the kids
My most favorite times in life
I've spent right here where we live
There's no place like home
There's no place like home
It just hit me as I was leavin'
There's no place like home
From the bedroom, I smell perfume
My favorite kind you wore
And it brings back memories of all those nights
Behind our bedroom door
And the saddest thing, I think, I've ever seen
Was my closet all cleaned out
It's sad to think that one must leave
'Coz we can't work things out
There's no place like home
There's no place like home
It just hit me as I was leavin'
There's no place like home
So, baby, say that I can stay
For just a day or so
Then maybe I can change your mind
And I won't have to go
Just look into these baby blues
And tell me it's okay
I love that smile, it drives me wild
No, love won't die today
There's no place like home
There's no place like home