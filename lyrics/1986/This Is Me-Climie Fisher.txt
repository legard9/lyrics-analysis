This is me
I don't wear a disguise
I know you'd see right through it
Oh
What you see with your naked eyes
Is the heart of me - that's what I am
I will not compromise
I'm not chained to your side
What I say is what I mean
This is me - so don't try and change it
This is real - so don't rearrange it
And don't hold a gun to my heart
Not unless you're ready to use it
If you think you can tear me apart
Take a shot
I won't refuse it
This is me - so don't try and change it
This is real - so don't rearrange it
This is me - find someone else!
This is me - don't try and change it
What you see with your naked eyes
Is the heart of me - that's what I am
I will not compromise
I don't like living lies
What I say is what I mean
This is me - don't try and change it
This is real - don't rearrange it
Find someone else!
This is me - don't try and change it
Find someone else!
This is real - don't rearrange it