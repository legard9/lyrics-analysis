[Chorus: Ray Horton]
I remember the time the time that we had
I remember the things that use to make you mad
And I wish I could turn back the time
And I wish I wouldn't cry every night
I remember the time

[Verse 1]
Clock always tickin' that's no question
I did you wrong I must confess not my intention
When I mention your name the pain I feel ashamed
If i could I would it's only me that I can blame
My time was on I had to move no substitution
I took the risk although no conclusion
Lack of piece of mind to go the same direction
Time is never personal you win or loose relationships
That's the way how it goes
Deeply hurt from your head to your toes
Slow mo' see you dragging down the hole
You should be where you belong in the front row
I cried everyday improved my behaviour
You can't see what I see tell me why are you in anger
Forgive your enemies let the Lord be your saviour
In god I will trust 'cause I know he will remember

[Chorus: Ray Horton] x2
I remember the time the time that we had
I remember the things that use to make you mad
And I wish I could turn back the time
And I wish I wouldn't cry every night
I remember the time

[Verse 2]
Join me on a ride what's the clue explanation
Talk about the time you and me let's go fishin'
Brainstorm' I bring the vibes shut your mouth better listen
Fighting everyday still don't know the definition
Time is money and time will tell
Sunshine in heaven or grill in hell
Society makes the pace join the race and obey
One for the money and two the space three hooray
I wanna thank G.O.D. the inventor
Kept at least an eye on me and put me in the center
I remember 1996 in december
Booya is the name and nana is a member
I cried everyday re-arranged my behaviour
You can't see what I see tell why are you in anger
Forgive your enemies let the Lord be your saviour
Finally he'll put you through 'cause he will remember

[Chorus: Ray Horton] x2
I remember the time the time that we had
I remember the things that use to make you mad
And I wish I could turn back the time
And I wish I wouldn't cry every night
I remember the time