The autumn leaves burning red
With the last light of the sun
The day is almost over
The day is almost done
Perhaps tonight I'll sit and watch
The stars come out
Their liquid hazy light
Holding time still
Bringing old memories to life
But I'm restless, restless, so restless