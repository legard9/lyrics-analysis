(Michael)
December 25th, 2016
I used to imagine this day as a kid
I just never knew the date, until now
At last, this; Self Entitled
My struggles, my journey, my story, my life
Everything
The good & the bad
An autobiography of my life, my story
Let's go

I just want to make a change
Put my town on, so they respect my name
It ain't about the fame, but what's in the frame
This picture's worth a thousand words, including pain
Self-proclaimed, I ain't need no help
This whole damn tape, I did it all myself
I had notebooks filled, you couldn't fit nothing else
I was mixing all night, feeling 'Self Entitled'
And now my phone's blowing up, but couldn't get an answer
I've been rolling this up, and been smoking cancer
Where's Maldo? Trying to perfect his Master
With all the odds against me, I started working faster
I am an artist, don't call it rapper
Michelangelo with that Sistine Chapel
I been a music-addict, how I get these samples
Trying to make it out of Jersey, like the company Campbell's
And recently, I've been missing Grandpa
And this year, I almost lost my Nana
Made it hard to breathe, like I was feeling asthma
Made it hard to see, I couldn't see the dashboard (Damn)
But I made it home
A little more empty now that you're gone
I'm getting chills when I'm writing a song
Because everytime I'm writing, I know I'm not alone
(I can feel it!)
There!
Like you were still right next to me
I'm cooking in the booth like we got a great recipe
If you could hear it now, you'd be so proud of me
You said I need to vent, and I finally got it out of me!
Middle fingers now up if you doubted me!
Inside my mind right now is a gallery
I'm getting teary-eyed, looking like allergies
I'm finna' tell my whole story, no fallacy!
Broken watch on my wrist, it don't tell time
I ain't got nothing to risk, I'm in Hell now!
My shit is not for the kids who had a hand-out!
By the end of this, tell me who you Hail now?!
Tell me who you Hail now!
The same child with dreams is all grown now!
Did it all on his own and never sold-out
In a few years, I hope this makes you so proud
When you looking back
This tape was my life, on & off the tracks
Between all the girls who brought an impact
The ones who left me hurt, ain't getting a call back
(Ain't getting a call back)
I got trust issues
I been in love, and I've been burned too
Try to step to me, and get burnt too
Because there's nothing in the way of my debut!
For all those endless nights
That I stayed home, and I chose to write
Better off alone, than I was in life
Because I lost a couple friends trying to catch that high!
But I'm high right now!
Chasing my dream, won't ever look down!
Fuck around and just might die right now!
But I hope to leave a mark on my town, somehow!
Somehow, some way
Because I always found the right words to say
If you want to hear my voice, you could always hit play
I always kept it real, because the trends won't stay
(Trends won't stay)
And be remembered
The type of lyrics that'll last forever
The type of words that express the effort
My life in verses, I hope you get the message
(Hope you get the message)
I just want to live forever
Through this music, we are still together
It's on recording for your own pleasure
All the nights I was under pressure
Felling like I'm gonna' pop!
Nervous breakdown, the ideas won't stop!
I''m 22 now, what if music really flop?!
Or I listened to what they said, and this tape never dropped?!
Maldo
(Motherfucker!)
(Yeah!)
(Self Entitled)
(2016)
(Yeah)
(I can feel it!)