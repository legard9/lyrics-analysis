She had hair like Jeannie Shrimpton, back in 1965
She had legs that never ended
I was halfway paralyzed
She was tall and cool and pretty
And she dressed as black as coal
If she'd ask me to, I'd murder
I would gladly lose my soul

[Chorus]
Now I lie in bed and think of her
Sometimes I even weep
Then I dream of her, behind the wall of sleep

Well she held a bass guitar and
She was playing in a band
And she stood just like Bill Wyman
Now I am her biggest fan
Now I know I'm one of many who
Would like to be your friend
And I've got to find a way
To let you know I'm not like them

[Chorus]
Now I lie in bed and think of her
Sometimes I even weep
Then I dream of her, behind the wall of sleep

Got your number from a friend of mine
Who lives in your hometown
Called you up to have a drink
Your roommate said you weren't around
Now I know I'm one of many
Who would like to be your friend
And I've got to find a way to
Let you know I'm not like them

[Chorus]
Now I lie in bed and think of her
Sometimes I even weep
Then I dream of her, behind the wall of sleep...
Behind the wall of sleep, behind the wall of sleep

Behind the wall of Sleep