I thought I heard a lover's sigh
It wasn't very loud
It came as I was passing by
Somebody else's cloud

The curtains were the finest lace
And when the night winds blew
I saw the dew upon her face
And other places too

Nobody knows what you do to me
And nobody's past is history
But the thing they should know seems to be
That it's nobody's business at all

I took a walk down lovers lane
A quick call to the boss
And yet again it seems my gain
Is somebody else's loss

Automobilia nowhere steer
My camera standing by
I wait for evening mists to clear
So I can find out why

Nobody knows what you do to me
And nobody's past is history
But the thing they should know seems to be
That it's nobody's business at all