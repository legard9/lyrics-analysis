[Verse 1]
The starship is falling
No communication, no one's calling
Only if I'd known this knowledge
I'd spread my wings and fly right by you

[Pre-Chorus]
She get to lookin' like she been waiting for me
Contemplating, a thousand days of war
She waitin' for me to pay for it
Love no more, love not war
I've been tourin' back and forth
If you hear a heartbeat, it's yours
No, it ain't mine, I don't feel alive

[Chorus]
So beam me up and let me meet my maker
So many questions I need answers to, the wake up
Can you tell me 'bout love?
Sit me down (Oh, God)
All these other people don't have a clue (Nah, they don't know about it)
Can you help me out, me out?
God, would you please take a second and hear me out? (Hear me)
Oh, please
If I scream and shout, is it loud enough? (Hey!)
Enough for Heaven to hear me?
Oh, God
Enough for Heaven to hear me?
Dear God

[Verse 2]
Software is crashing, oh no
I can't take too much more of this
S.O.S. and no one's coming
I'm hoping God can solve my problems (Help me)
She still won't even look at me, she just keep on walking (Why?)

[Pre-Chorus]
She get to lookin' like she been waiting for me (Waiting for me)
Contemplating, a thousand days of war (No-no, no)
She waitin' for me to pay for it
Love no more, love not war (Not war)
I've been tourin' back and forth
If you hear a heartbeat, it's yours (Ow!)
No, it ain't mine, I don't feel alive (Ow!)

[Chorus]
So beam me up and let me meet my maker (Me)
So many questions I need answers to, the wake up
Can you tell me 'bout love?
Sit me down (Oh, God)
All these other people don't have a clue (Nah, they don't know about it)
Can you help me out, me out? (Ooh-ooh)
God, would you please take a second and hear me out? (Hear me, oh please)
Oh, please
If I scream and shout, is it loud enough? (Hey!)
Enough for Heaven to hear me?
Oh, God
Enough for Heaven to hear me?
Dear God

[Verse 3]
Huh, if you grant me this wish, one wish, this wish, Lord, I promise
I'll never ask for anything else
'Cause I can't do it by myself
It's too hard
I can't play God, huh, that's your job
I'm just a simple man who needs his heart repaired
In purgatory, but not if she's there
I know I'm askin' for a lot but
Tryna break these chains I'm locked from
I'll no longer be scared
If you're really up there

[Chorus]
So beam me up and let me meet my maker
So many questions I need answers to, the wake up (I need you, oh)
Can you tell me 'bout love? (Now, now, now, now, now)
Sit me down (Oh, God)
All these other people don't have a clue (Ooh, nah, they don't know about it)
Can you help me out, me out? (Ooh-ooh)
God, would you please take a second and hear me out? (Hear me, hear me)
Oh, please
If I scream and shout, is it loud enough? (Hey!)
Enough for Heaven to hear me?
Oh, God
Enough for Heaven to hear me?
Dear God