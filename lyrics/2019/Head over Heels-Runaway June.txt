[Verse 1]
We got good at the drunk dial game
Let it go a little longer than it should
You call me up, say "Head my way"
Yeah, and I always would
Wake up in your arms from the night before
Pick my stilettos up off the floor
A Walk of shame out your front door
But not no more

[Chorus]
'Cause these ain’t my late night, lace up, black leather shoes
These ain't my downtown, dance around, waitin' on you
These ain’t my lonely cab ride, one night thrills
These ain't my "You get drunk, call me up, and head over" heels
(These ain't my head over, head over heels)
Head over heels
(These ain't my head over, head over heels)

[Verse 2]
I see your number lighting up my phone
At 2AM, yeah you're right on time
And you don't wanna go home alone
Yeah, but you will tonight
'Cause I'm gonna be here all night long
Breakin’ ’em in to a let go song
Can tell by the way that I'm moving, I’m moving on

[Chorus]
'Cause these ain't my late night, lace up, black leather shoes
These ain't my downtown, dance around, waitin’ on you
These ain't my lonely cab ride, one night thrills
These ain't my "You get drunk, call me up, and head over" heels
(These ain't my head over, head over heels)
Head over heels
(Ohh-ohh)

[Bridge]
These ain't my late night (These ain't my), lace up, black leather shoes
These ain't my downtown (These ain't my) dance around, waitin' on you

[Chorus]
'Cause these ain't my late night, lace up, black leather shoes
These ain't my downtown, dance around, waitin' on you
These ain't my lonely cab ride, one night thrills
These ain't my "You get drunk, call me up, and head over" heels
(These ain't my head over, head over heels)
Head over heels
(These ain't my head over, head over heels)
How does that feel, oh
(These ain't my head over, head over heels)
These ain't my head over heels
(These ain't my head over, head over heels)

[Outro]
I see your number lighting up my phone
At 2AM, yeah you're right on time