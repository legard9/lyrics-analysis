[Verse 1]
Mm, you can run away from it all
But like a boomerang
It'll all come back
You can hang on until you fall
But even when you land
You'll be on your back

[Pre-Chorus]
Like mannequins all draped in designers
Have we forgotten where we come from?
With bottled hopes and dreams and desires
Must have forgotten that

[Chorus]
We got bills to pay
We got rules to break
We got risks to take
We got things to say
No one ever said
It would be okay
But we'll find a way
When there's love
And we got love
(Ah, ah)

[Post-Chorus]
Yeah, it's okay 'cause we got love (Ah, ah)
Yeah, it's okay 'cause we got love
Oh-oh, yeah, it's okay 'cause we got love

[Verse 2]
When we think that we know it all
Like a fighter in a ring
We get knocked back
Mm, tread carefully where you walk
When the world is at your feet
Stay on track

[Pre-Chorus]
Oh, like mannequins all draped in designers
Have we forgotten where we come from?
With bottled hopes and dreams and desires
Must have forgotten that

[Chorus]
We got bills to pay
We got rules to break
We got risks to take
We got things to say
No one ever said
It would be okay
But we'll find a way
When there's love
And we got love

[Post-Chorus]
Oh-oh, yeah, it's okay 'cause we got love

[Bridge]
When all is said and done
Our old flame's been and gone
Take some time to look how far we've come
Remember that we got love (Ha-ah-ah-ooh)
Yeah, it's okay 'cause we got love (We got love, yeah)
Yeah, it's okay 'cause we got love (Oh-oh)

[Outro]
We got, we got love, yeah (Oh-oh-ooh)
Yeah, it's okay 'cause we got love
(Yeah, it's okay 'cause we got love, oh-oh)
Yeah, it's okay 'cause we got love