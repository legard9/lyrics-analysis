[Verse 1]
Yeah you might know pretty well what I've been hiding
Yeah you have been spinning in my mind, again
Remember that night when I said that I got over you? I lied
But you already knew it, right?

[Pre-Chorus]
The way your body feels against mine
Yeah you still make me blush
Show me that you want me bad
(Show me that you want me bad)
When I say I hate you, I mean I love you
I'm so complicated, you too
But I want you as bad as you do
This is a game for two

[Chorus]
We-we can’t get enough, ooh
Will never be too much, ooh
You know, I know, we know, ooh
You got me so high, ooh
Stronger than a crush, ooh
This is how I feel about you
We can’t get enough
(Nana-nana-naah-nana, ha-ha-ha-ha)
(Stronger than a crush, stronger than a crush) (x3)

[Verse 2]
Yeah I’m gonna make you lose your mind tonight
('Till you can’t get enough)
Baby let’s keep this on replay
('Till we can’t get enough)
While I sing some high notes on your bed
('Till I can’t get enough)
But we can’t get enough (We can’t get enough)
We can't get enough yaya

[Pre-Chorus]
The way your body feels against mine
Yeah you still make me blush
Show me that you want me bad
(You want me bad)
When I say I hate you, I mean I love you
I'm so complicated, you too
But I want you as bad as you do
This is a game for two

[Chorus]
We-we can’t get enough, ooh
Will never be too much, ooh
You know, I know, we know, ooh
You got me so high, ooh
Stronger than a crush, ooh
This is how I feel about you
We can’t get enough

[Bridge]
(Ha-ha-ha-ha-ha)
You got me so high
Stronger than a crush (Ooh, ah)
We can’t get enou-gh enou-gh (No) (Ooh, ah)
(Ha-ha-ha-ha)
We can’t get enough
(Ha-ha-ha-ha) (ha-ah)

[Outro]
(Na-na-na-na-na-na, hmm-aah)
You got me so high
Stronger than a crush
(Na-na-na-na-na-na, hmm-aah)
Stronger than a crush
Siempre en mi mente estas (Hmm, na-na-na)