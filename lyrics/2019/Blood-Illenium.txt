[Intro]
We're blood
We're blood

[Verse 1]
When you find your mind in a dark place
Hold on, it won't be long
Before you see my face
I'm coming to the rescue
Take your hand, comfort you
It's the least that I can do

[Chorus]
'Cause we're blood
Oh, we're blood
Oh, we're blood
Oh, we're blood

[Drop]
We're blood
We're blood
We're blood
Oh, we're blood

[Verse 2]
I heard you told your friends in the schoolyard
You were getting it tough, getting it hard
You know it nearly broke my heart
I'm coming to defend you
Live or die, see it through
It's the least that I can do

[Chorus]
'Cause we're blood
Oh, we're blood

[Drop]
We're blood
We're blood
Oh, we're blood

[Bridge]
'Cause we're blood
Oh, we're blood
Oh, we're blood
Oh, we're blood

[Chorus]
We're blood
Oh, we're blood
Oh, we're blood
Oh, we're blood

[Drop]
We're blood
Oh, we're blood
Oh, we're blood
Oh, we're blood