ENGLISH TRANSLATION
It smells like you
The road that I walk on
I plug my earphone to my status
My true feelings lie beyond there
Baby why you far away
I can’t get used to you not being here
I feel like something is missing
I just want to be together with you always
When I realize we were only be able to meet inside this smartphone
You appeared before my eyes
Today too I feel the sadness of not able to see you
Contrary to my feelings time passes rapidly
Even if I feel insecure
I believe that my heart will not leave you
My heart will reach past beyond the wind
I am always under the same sky my baby
Everything is for you~
Everything is alright
Even if there is no answer
Everything is inside your smile
Truth
Even if we are apart
Our hearts are connected to each other
Forever with you
Flutter already
Flower petals shower
Sparkle already
İnside the sun
I am embracing you always
Inside my heart forever
Flutter already
Flower petals shower
Sparkle already
Under the sun forever just the two of us
It is alright since we are connected always
We live far away from each other
Today too I feel lonely
I want to see you
The night that I am alone I unnecessarily feel the lonelier
With distance my heart opens more
It feels unbearable when I hear your rumor
I become more frustrated with more time passes
The amount that over flows always
Oh girl your smile still remains here
Even if we are apart my feelings are
Still the same, we are always together
Everything is for you~
Everything is alright
Even if there is no answer
Everything is inside your smile
Truth
Even if we are apart
Our hearts are connected to each other
Forever with you
Flutter already
Flower petals shower
Sparkle already
İnside the sun
I am embracing you always
Inside my heart forever
Flutter already
Flower petals shower
Sparkle already
Under the sun forever just the two of us
It is alright since we are connected always
Even if we are far away
We are looking at the same sky
Even if something happens
If you are here I am not afraid anymore
I believe
Our love will lit for eternity
It will always stay beautiful as it is
It will always be like cherry blossom pedals
Everything is for you~
Everything is alright
Even if there is no answer
Everything is inside your smile
Truth
Even if we are apart
Our hearts are connected to each other
Forever with you
Flutter already
Flower petals shower
Sparkle already
İnside the sun
I am embracing you always
Inside my heart forever
Flutter already
Flower petals shower
Sparkle already
Under the sun forever just the two of us
It is alright since we are connected always
JAPANESE ORIGINAL
君の香りがする
この道を辿って行く
イヤホン差し込むこの鼓動に
確かな想いがその先にある
Baby why you far away？
慣れない君がいないと　なんかね
何か足りない気がする
ずっと一緒に2人いたいだけなのに
気付けば君はこのスマホの中だけで
しか会えない目の前に現れて
一緒にいれない寂しさ　今日も噛み締め
想いと裏腹　時間だけ過ぎてく
不安な気持ちになっても
距離の様　心離れないでと
信じて届ける風の先には
いつも俺がいる　同じ空の下 my baby…
すべて for you
答えなんて
なくていい全ては
君の笑顔の中にある
Truth
離れていても
心と心は繋がっている
Forever　君と
ヒラヒラ舞う
花びらのシャワー
キラキラ舞う
太陽の中
僕は抱きしめている
胸の中で君をずっと
ヒラヒラ舞う
花びらのシャワー
キラキラ舞う
太陽の下でずっと二人は
どんな時も繋がっているからね大丈夫
遠い場所　過ごす2人
今日は何かもうすっかり
寂しくて会いたい
独りの夜　よけい感じるその大きさに
距離と心　比例するなんて
噂が気になりたまらくなって
心配になるもっと　時は過ぎどんどん
溢れるため息の数も相当
Oh girl, 君のその笑顔が残ってるのさ
離れていても気持ちならいつも
同じ2人はいつまでも一緒
すべて for you
答えなんて
なくていい全ては
君の笑顔の中にある
Truth
離れていても
心と心は繋がっている
Forever　君と
ヒラヒラ舞う
花びらのシャワー
キラキラ舞う
太陽の中
僕は抱きしめている
胸の中で君をずっと
ヒラヒラ舞う
花びらのシャワー
キラキラ舞う
太陽の下でずっと二人は
どんな時も繋がっているからね大丈夫
例え遠くにいても
2人同じ空見てるよ
例え何があっても
君がいれば何も怖くはないよもう
信じ合ってるから
互いの愛が輝き合う様に
奇麗な2人のままさ
桜の花びらの様に
すべて for you
答えなんて
なくていい全ては
君の笑顔の中にある
Truth
離れていても
心と心は繋がっている
Forever　君と
ヒラヒラ舞う
花びらのシャワー
キラキラ舞う
太陽の中
僕は抱きしめている
胸の中で君をずっと
ヒラヒラ舞う
花びらのシャワー
キラキラ舞う
太陽の下でずっと二人は
どんな時も繋がっているからね大丈夫
ROMAJI
Kimi no kaori ga suru
Kono michi o tadotte iku
Iya hon sashikomu kono kodō ni
Tashika na omoi ga sono saki ni aru
Baby why you far away?
Nare nai kimi ga i nai to nan ka ne
Nani ka tari nai ki ga suru
Zutto issho ni 2 nin itai dake na noni
Kizuke ba kimi wa kono sumaho no naka dake de
Shika ae nai me no mae ni araware te
Issho ni ire nai sabishi sa kyō mo kamishime
Omoi to urahara jikan dake sugi te ku
Fuan na kimochi ni natte mo
Kyori no yō kokoro hanare nai de to
Shinji te todokeru kaze no saki ni wa
Itsumo ore ga iru onaji sora no shimo my baby?
Subete for you
Kotae nante
Naku te ii subete wa
Kimi no egao no naka ni aru
Truth
Hanare te i te mo
Kokoro to kokoro wa tsunagatte iru
Forever kimi to
Hirahira mau
Hanabira no shawā
Kirakira mau
Taiyō no naka
Boku wa dakishime te iru
Mune no naka de kimi o zutto
Hirahira mau
Hanabira no shawā
Kirakira mau
Taiyō no shita de zutto ni nin wa
Donna toki mo tsunagatte iru kara ne daijōbu
Tōi basho sugosu 2 nin
Kyō wa nani ka mō sukkari
Sabishiku te ai tai
Hitori no yoru yokei kanjiru sono ōki sa ni
Kyori to kokoro hirei suru nante
Uwasa ga ki ni nari tama raku natte
Shinpai ni naru motto toki wa sugi dondon
Afureru tameiki no kazu mo sōtō
Oh girl , kun no sono egao ga nokotteru no sa
Hanare te i te mo kimochi nara itsumo
Onaji 2 nin wa itsu made mo issho
Subete for you
Kotae nante
Naku te ii subete wa
Kimi no egao no naka ni aru
Truth
Hanare te i te mo
Kokoro to kokoro wa tsunagatte iru
Forever kimi to
Hirahira mau
Hanabira no shawā
Kirakira mau
Taiyō no naka
Boku wa dakishime te iru
Mune no naka de kimi o zutto
Hirahira mau
Hanabira no shawā
Kirakira mau
Taiyō no shita de zutto ni nin wa
Donna toki mo tsunagatte iru kara ne daijōbu
Tatoe tōku ni i te mo
2 nin onaji sora miteru yo
Tatoe nani ga atte mo
Kimi ga ire ba nani mo kowaku wa nai yo mō
Shinjiatteru kara
Tagai no ai ga kagayakiau yō ni
Kirei na 2 nin no mama sa
Sakura no hanabira no yō ni
Subete for you
Kotae nante
Naku te ii subete wa
Kimi no egao no naka ni aru
Truth
Hanare te i te mo
Kokoro to kokoro wa tsunagatte iru
Forever kimi to
Hirahira mau
Hanabira no shawā
Kirakira mau
Taiyō no naka
Boku wa dakishime te iru
Mune no naka de kimi o zutto
Hirahira mau
Hanabira no shawā
Kirakira mau
Taiyō no shita de zutto ni nin wa
Donna toki mo tsunagatte iru kara ne daijōbu