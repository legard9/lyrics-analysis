[Intro: Rita Ora]
You, to carry on for you, for you

[Verse 1: Rita Ora]
Walking alone and the shores are longing
I miss your footprints next to mine
Sure as the waves on the sand are washing
Your rhythm keeps my heart in time

[Chorus: Rita Ora]
You, you found me
Made me into something new
Led me through the deepest waters
I promise loud to carry on for you

[Post-Chorus: Rita Ora]
You, to carry on for you, for you
You, to carry on for you, for you
I'll carry on for you

[Verse 2: Rita Ora]
Talk to the wind on the open ocean
I wonder if you hear me too?
Wrapped in my arms with every moment, yeah
The memories that pull me through

[Chorus: Rita Ora]
You, you found me
Made me into something new (Oh, yeah)
Led me through the deepest waters
I promise loud to carry on to you (Ooh)

[Post-Chorus]
You, to carry on for you, for you
To carry on for you
You, to carry on for you, for you
I'll carry on for you

[Chorus: Rita Ora]
You, you found me (Oh, yeah)
Made me into something new (Into something new)
Led me through the deepest waters (Deepest)
I promise loud to carry on for you (Oh, carry on for you)

[Post-Chorus: Rita Ora]
You, to carry on for you, for you
You, to carry on for you, for you

[Bridge: Rita Ora]
Keep me running, keep me coming back to you
Hold me harder, love me like you know you do
Keep me running, keep me coming back to you
Hold me harder, love me like you know you do

[Chorus: Rita Ora]
You, you found me
Made me into something new
Led me through the deepest waters
I promise loud to carry on for you

[Post-Chorus: Rita Ora]
You, to carry on for you, for you
You, to carry on for you, for you
In the deepest waters
Oh, I'll carry on for you