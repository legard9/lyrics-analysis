Hangul

가라앉은 정적 속 텅 비어버린 방에
울리는 심장소리
외로움으로 애타게 노래하듯 우는
창 밖의 이 빗소리
이젠 너는 없다고 너는 없다고
맘을 달래 보지만
여전한 네 잔상을 쫓아야 하는 걸

Ah 차가운 빗속에서 젖어 떨던 너의
그 조그맣던 어깨에 닿았던 그 날이
내겐 아직 어제의 일인 것 같은데
I’m crying I’m crying
I’m crying cause I miss you

너의 눈동자는 이젠
나를 보지 않잖아
유리처럼 투명히
통과 시킬 뿐이잖아 Ah
제발 부탁이니까 내게 돌아와
차라리 내 곁에서 날 힘들게 해줘
내가 널 포기할 수 있게

네 맘 속엔 멎는 법을 잊은 듯
내리던 비
너의 우산이 되지 못한
부족했었던 나
그치지 않는 비를 향한 노래
사랑한다고

네 눈물을 알면서도 모른 척
무심히 외면했던 순간도
내 멋대로 혼자 그린 미래만을 향해
달려가던 마음도
이젠 오직 너만이 오직 너만이
내가 원한 모든 것
이제서야 알 것 같아 난 너의 마음을

Ah 아득히 번져가는 당연했던 우리
늘 따스하게 왼 손을 채워주던 온기
아직 내 가슴이 널 기억하잖아
I’m trying I’m trying
I’m trying to forget you

모든 기억들을
가슴에 담아두고 싶어
젖은 머리카락의 향기
살짝 스쳐갈 때 Ah
제발 부탁이니까 한 번이라도
너의 손을 잡고 다시 걷게 된다면
달라진 날 맹세 할 텐데

어느 새 내 맘에도
멈추지 않는 빗소리
영원하게만 느껴지는
너 없는 시간도
그치지 않는 비를 향한 노래
사랑한다고

너도 어딘가에서
같은 하늘을 보겠지
마치 네 사랑을 닮은 듯
따뜻한 비가 내려 와
슬픈 맘도 후회도 모두
천천히 흘려 보내줘
I’m crying I’m crying
I’m crying cause I’m missing you

이런 내 맘속에 내리는 비
언젠가는 멈출 거야 그때

너의 눈동자는 지금
어딜 향해 있는지
유리처럼 투명한
그 눈빛도 여전한지 Ah
제발 부탁이니까 내게 돌아와
차라리 내 곁에서 날 힘들게 해줘
내가 널 포기할 수 있게

네 맘 속엔 멎는 법을 잊은 듯
내리던 비
너의 우산이 되지 못한
부족했었던 나
그치지 않는 비를 향한 노래
사랑한다고

Romanization

Garaanjeun jeongjeok sok teong bieobeorin bange
Ullineun simjangsori
Oeroumeuro aetage noraehadeut uneun
Chang bakkui i bissori
Ijen neoneun eopsdago neoneun eopsdago
Mameul dallae bojiman
Yeojeonhan ne jansangeul jjochaya haneun geol

Ah chagaun bissogeseo jeojeo tteoldeon neoui
Geu jogeumahdeon eokkaee dahassdeon geu nari
Naegen ajik eojeui irin geot gateunde
I’m crying I’m crying
I’m crying cause I miss you

Neoui nundongjaneun ijen
Nareul boji anhjanha
Yuricheoreom tumyeonghi
Tonggwa sikil ppunijanha Ah
Jebal butaginikka naege dorawa
Charari nae gyeoteseo nal himdeulge haejwo
Naega neol pogihal su issge

Ne mam sogen meojneun beobeul ijeun deut
Naerideon bi
Neoui usani doeji mothan
Bujokhaesseossdeon na
Geuchiji anhneun bireul hyanghan norae
Saranghandago

Ne nunmureul almyeonseodo moreun cheok
Musimhi oemyeonhaessdeon sungando
Nae meotdaero honja geurin miraemaneul hyanghae
Dallyeogadeon maeumdo
Ijen ojik neomani ojik neomani
Naega wonhan modeun geot
Ijeseoya al geot gata nan neoui maeumeul

Ah adeukhi beonjyeoganeun dangyeonhaessdeon uri
Neul ttaseuhage oen soneul chaewojudeon ongi
Ajik nae gaseumi neol gieokhajanha
I’m trying I’m trying
I’m trying to forget you

Modeun gieokdeureul
Gaseume damadugo sipeo
Jeojeun meorikaragui hyanggi
Saljjak seuchyeogal ttae Ah
Jebal butaginikka han beonirado
Neoui soneul japgo dasi geodge doendamyeon
Dallajin nal maengse hal tende

Eoneu sae nae mamedo
Meomchuji anhneun bissori
Yeongwonhageman neukkyeojineun
Neo eopsneun sigando
Geuchiji anhneun bireul hyanghan norae
Saranghandago

Neodo eodingaeseo
Gateun haneureul bogessji
Machi ne sarangeul dalmeun deut
Ttatteushan biga naeryeo wa
Seulpeun mamdo huhoedo modu
Cheoncheonhi heullyeo bonaejwo
I’m crying I’m crying
I’m crying cause I’m missing you

Ireon nae mamsoge naerineun bi
Eonjenganeun meomchul geoya geuttae

Neoui nundongjaneun jigeum
Eodil hyanghae issneunji
Yuricheoreom tumyeonghan
Geu nunbichdo yeojeonhanji Ah
Jebal butaginikka naege dorawa
Charari nae gyeoteseo nal himdeulge haejwo
Naega neol pogihal su issge

Ne mam sogen meojneun beobeul ijeun deut
Naerideon bi
Neoui usani doeji mothan
Bujokhaesseossdeon na
Geuchiji anhneun bireul hyanghan norae
Saranghandago

English Translation

In the settling silence, in the empty room
I hear my ringing heartbeat
As if it’s singing out of loneliness
The sound of the rain cries outside the window
You’re not here now, you’re not here
I try to comfort my heart but
I’m still chasing after the afterimage of you

That day I touched your small shoulder
As you were trembling, wet with rain
Still feels like it was yesterday
I’m crying I’m crying
I’m crying cause I miss you

In your eyes
I can’t see myself anymore
Like clear glass
I just go right through
Please, I’m asking you, come back to me
Stay by my side and make things hard for me
So I can give you up

The rain fell in your heart
As if it forgot how to stop
I wasn’t good enough for you
I couldn’t be your umbrella
A song for the unstopping rain
I love you

I knew your tears but I ignored them
Indifferently turned away
I only faced the future that I thought out
Now my heart only runs to you
You’re the only one I want
Now I finally know how you felt

I took us for granted and now we’re so faraway
Your warmth always heated up my left hand
My heart still remembers you
I’m trying I’m trying
I’m trying to forget you

I want to place
All the memories in my heart
When the scent of your wet hair
Passes through my nose
Please, I’m asking you, just once
If I could hold your hand and walk
I swear I will change

In my heart
The rain won’t stop either
This time without you
Feels like it’ll last forever
A song for the unstopping rain
I love you

I’m sure from somewhere
You’re looking at the same sky
Resembling your love
A warm rain falls
Making me let go of all my sadness and regret
I’m crying I’m crying
I’m crying cause I’m missing you

Rain is falling in my heart
When will it stop?

Where are your eyes looking at now?
Are your eyes still clear as glass?
Please, I’m asking you, come back to me
Stay by my side and make things hard for me
So I can give you up

The rain fell in your heart
As if it forgot how to stop
I wasn’t good enough for you
I couldn’t be your umbrella
A song for the unstopping rain
I love you