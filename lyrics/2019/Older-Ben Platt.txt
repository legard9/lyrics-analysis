[Verse 1]
I was walking down 57th Street towards Park
With the same old song in my same old heart
Making plans I'm gonna do when I get older
Passed a gray-haired man and I found his eyes
It's like he knew my thoughts and he read my mind
Saying life is gonna find you when it's supposed to
Felt my feet stop short and I turned my head
Tried to lean into every word he said, then he said

[Chorus]
When you are younger, you'll wish you're older
Then when you're older, you'll wish for time to turn around
Don't let your wonder turn into closure
When you get older, when you get older

[Post-Chorus]
Oh, oh, oh, oh
Oh, oh, oh

[Verse 2]
Have I killed my thoughts right before their prime?
Have I bit my tongue one too many times
Have I said it all the way I really meant to?
If I wait 'til my tomorrow comes
Is the waiting all I've ever done?
And will I get to, get to know myself in the place I am
Get to fall in love with another man
 and understand

[Chorus]
When you are younger, you'll wish you're older
Then when you're older, you'll wish for time to turn around
Don't let your wonder turn into closure
When you get older, when you get older

[Post-Chorus]
Oh, oh, oh, oh
Oh, oh, oh

[Chorus]
When you are younger, you'll wish you're older
When you get older, you'll wish for time to turn around
Don't let your wonder turn into closure
When you get older, when you get older

[Outro]
Oh, oh, oh, oh