[Verse 1]
Pull this car over, babe
Don't pretend like you don't know
A lot of girls have time for this shit
Honestly, I don't

[Pre-Chorus 1]
He said, "Why you cryin', baby?
Why you cryin', baby?
What did I do?" Oh
I said, shut up and kiss me, oh
'Cause I don't wanna be here for long

[Chorus]
I swore you'd never see this side
But it's so hard to say goodbye
I don't need to apologize
Us big girls gotta cry

[Post-Chorus]
Ooh, ooh, ooh
Ooh, ooh, ooh

[Verse 2]
You should feel honored boy, uh
You got me feelin' this much
I clear my schedule for you
Let my guard down for you
And you gon' make me put it back up

[Pre-Chorus 2]
Oh, you know why I'm cryin'
Why I'm cryin', baby
You don't pick up the phone
You do the same thing to your friends and family
You gon' end up alone

[Chorus]
I swore you'd never see this side
But it's so hard to say goodbye
I don't need to apologize
Us big girls got to cry

[Post-Chorus]
Ooh, ooh, ooh
Ooh, ooh, ooh

[Bridge]
It feels so good, so good sometimes
Some big girls gotta cry
After this is done, just take me home
I'm feelin' vulnerable
I don't need to apologize
Us big girls gotta cry

[Outro]
Oh, oh, oh
Oh, oh, oh
Oh, oh, oh
Oh, oh, oh
Ooh, it's my life
You know it hurts sometimes
My love is your love
Why can't your love be mine?
Ooh, ooh
Oh, I get a little lost sometimes
Sometimes you need to cry