[Verse 1: Jessie Reyez]
You're so sweet to me
You fill me in when I black out
Pull my dress down when my ass out
Love me despite my big mouth
Up the ladder, to the billboard
Off the edge just so we could feel more
Buy some liquor, now we piss poor
Fuck your curfew, let's just kiss more

[Pre-Chorus: Jessie Reyez]
Till we see the lights over the city
If I gotta go soon, will you miss me?
You say you like old school, so my phone's playin' Biggie
We're playing Truth or Dare and 21 Questions

[Chorus: Bea Miller]
If you ask me where I'm from
I say, "You", 'cause you feel like home
Ask me, "Is this love?"
I say, "I feel it in my bone"
I need you to know the feelings new
But if you ask me where I'm from
I say, "You", 'cause you feel like home

[Post-Chorus: Bea Miller]
Home, feel like I'm at home

[Verse 2: Bea Miller]
You don't know what you mean to me
Even when you're the reason I'm pissed off
You make me smile when I'm pissed off
Cheer me on when I miss shots
The moon is tired, doesn't matter
Can't go home, I'm tired of mastur-
Bating Beta, I need an alpha
We're hella faded, could we stay laughing?

[Pre-Chorus: Bea Miller]
Till we see the lights over the city?
If I gotta go soon, will you miss me?
You say you like old school, so my phone's playin' Biggie
We're playing Truth or Dare and 21 Questions

[Chorus: Bea Miller]
If you ask me where I'm from
I say "You", 'cause you feel like home
Ask me, "Is this love?"
I say, "I feel it in my bone"
I need you to know the feelings new
But if you ask me where I'm from
I say, "You", 'cause you feel like home

[Post-Chorus: Bea Miller]
Home, feel like I'm at home

[Verse 3: Jessie Reyez & Bea Miller]
Pinky promise you'll still be nice
In three months when we're on thin ice
Perfect doesn't last past tonight
But I pinky promise I'll try
No games, unless you wanna play
We could play 21 Questions

[Chorus: Bea Miller & Jessie Reyez]
If you ask me where I'm from
I say, "You", 'cause you feel like home
Ask me "Is this love?"
I say, "I feel it in my bones"
I need you to know the feelings new
But if you ask me where I'm from
I say, "You", 'cause you feel like home

[Post-Chorus: Bea Miller]
Home, feel like I'm at home

[Outro: Bea Miller]
Home, feel like I'm at home