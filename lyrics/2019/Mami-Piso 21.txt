[Letra de "Mami"]

[Intro: Lorduy]
Ey, yeah
Yeah-eh, eh
Oh

[Coro: Lorduy, DIM & Will.i.am]
Mami, eres dulce como azúcar (Baby, dale, dale)
Tienes ese toque que me gusta (Mami, dale, dale)
Hace que yo diga "Aleluya" (Espera, let me translate)
Girl, you got me saying "Hallelujah" (Espera, let me translate)

[Verso 1: Pablito & Will.i.am]
Me gusta cuando baila' lento, pero también rápido
That means that I love it when you shake it fast and shake it slow
Si nadie te llega, pues, seguro te llegaré yo
That means if you ain't got nobody, I'll be your Romeo
Ya sería una locura si tú te escaparas conmigo
That means when I go runaway, would you go, like, "I'm not loco"?
Yo te toco de a poco, this is just the intro
Aleluya es el momento cuando estamos tú y yo

[Coro: DIM, Lourdy & Will.i.am]
Mami, eres dulce como azúcar, yeah (Baby, dale, dale)
Tienes ese toque que me gusta, oh (Mami, dale, dale)
Hace que yo diga "Aleluya" (Espera, let me translate)
Girl, you got me saying "Hallelujah" (Espera, let me translate)

[Puente: DIM, Lorduy & Will.i.am]
Yo te amo (Means I love you)
Yo te quiero (Means I love you)
No te dejo sola (Means I love you)
Me quedo contigo (Because I love you)
Yo te amo (Means I love you)
Yo te quiero (Means I love you)
No te dejo sola (Means I love you)
Me quedo contigo (Because I love you)

[Verso 2: Will.i.am, Pablito, Taboo, *Will.i.am & Taboo*]
Baby, won't you let me put my body over your body?
Eso e' lo que quiero, tu cuerpo cerca, sólo pa' mí
We can keep it innocent or we can get really naughty
Eso e' romántico, pero también sexy
Girl, you got me living, living out my fantasy
Me tienes viviendo, me tienes en éxtasis
You got me drunk like the Hennessy, (Tomemo' par de Hennessy)
And Hallelujah is the moment when you stand it next to me
Girl, you got me feeling hot, me siento caliente
Always running through my mind, corriendo por mi mente
You're the first lady, baby, call me "Presidente"
I'ma love you slow-mo, love you suavemente
*Don't stop, get it, get it, let me see you do the round*
Girl, I'm committed, got the ring, never put it down
Baby, you're the queen, I'm the king and we got the crown
Let me translate, wait a minute, hold it now

[Refrán: DIM]
Mami, mami, mami, mami
Mami, mami, mami, mami

[Coro: DIM, Lorduy & Will.i.am]
Mami, eres dulce como azúcar, yeah (Baby, dale, dale)
Tienes ese toque que me gusta, oh (Mami, dale, dale)
Hace que yo diga "Aleluya" (Espera, let me translate)
Girl, you got me saying "Hallelujah" (Espera, let me translate)

[Puente: DIM, Lorduy & Will.i.am]
Yo te amo (Means I love you)
Yo te quiero (Means I love you)
No te dejo sola (Means I love you)
Me quedo contigo (Because I love you)
Yo te amo (Means I love you)
Yo te quiero (Means I love you)
No te dejo sola (Means I love you)
Me quedo contigo (Because I love you)
Yo te amo (Means I love you)
Yo te quiero (Means I love you)
No te dejo sola (Means I love you; Uoh, oh)
Me quedo contigo (Because I love you)
Yo te amo (Means I love you)
Yo te quiero (Means I love you)
No te dejo sola (Means I love you; Uoh, oh)
Me quedo contigo (Because I love you)

[Outro: Lorduy, DIM, Lorduy & DIM]
Piso 21, ma'
Black Eyed Peas
Black Eyed Peas
Mo-Mo-Mosty, sú-súbete