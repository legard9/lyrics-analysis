[Verse 1]
Waited so long, thought you'd never come
You proved me wrong 'cause I don't know where it felt so right
If I could go, go back in time
What I would do, is love you sooner

[Pre-Chorus]
I wish I was there for you
Wish I was holding you closer than close everyday of your life
I wish I was there for you
And I wish that you were there for me too

[Chorus]
I wish I was there for you
(There for you, there for you, there for you
There for you, there for you)
I wish I was there for you
(There for you, there for you, there for you
There for you, there for you)

(There for you, there for you, there for you
There for you, there for you, there for you)
I wish I was there for you
And I wish that you were there for me too
(There for you, there for you, there for you)

[Verse 2]
I lose my mind, going back in time
Were you mine, oh, mine?
So many years, so many tears, wishing that you were over here
Maybe that's how it needed to be for you to get to me

[Pre-Chorus]
I wish I was there for you
Wish I was holding you closer than close everyday of your life
I wish I was there for you
'Cause when we're together, there's so much to love, there's so little time
I wish I was there for you
Wish I was holding you closer than close everyday of your life
I wish I was there for you
And I wish that you were there for me too

[Chorus]
I wish I was there for you
(There for you, there for you, there for you
There for you, there for you)
I wish I was there for you
(There for you, there for you, there for you
There for you, there for you)

(There for you, there for you, there for you
There for you, there for you, there for you)
I wish I was there for you
And I wish that you were there for me too
(There for you, there for you, there for you)