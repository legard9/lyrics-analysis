[Verse 1]
Lovin'
I'm gonna keep it coming
Why can't you feel my love?
I'm trying so hard
Trust in me
I won't let you down
Just give me a chance
'Cause I've changed, I've become a better man

[Pre-Chorus]
Woah, woah, woah, woah

[Chorus]
Give me one more chance to make it right
All I'm asking for is time
One more chance to prove
My love, my love, my love, my love
You don't have to say I'm wrong
'Cause I've know it all along
One more chance to prove
My love, my love, my love, my love

[Post-Chorus]
Woah, woah
My love, my love, my love, my love
Woah, woah
My love, my love, my love, my love

[Verse 2]
Please believe me
Not running 'round anymore
I only have eyes for you
And I've changed
There's a sunset
Why don't you hold my hand?
Let's keep on the right side, oh
Let's leave tonight

[Pre-Chorus]
Woah, woah, woah, woah

[Chorus]
Give me one more chance to make it right
All I'm asking for is time
One more chance to prove
My love, my love, my love, my love
You don't have to say I'm wrong
'Cause I've know it all along
One more chance to prove
My love, my love, my love, my love

[Post-Chorus]
Woah, woah
My love, my love, my love, my love
Woah, woah
My love, my love, my love, my love

[Bridge]
Don't throw this away
Please, baby, stay
Oh, oh
Don't think of tomorrow
Live for today
Tell me what I need to know
Tell me what I need to know

[Pre-Chorus]
Woah, woah, woah, woah

[Chorus]
Give me one more chance to make it right
All I'm asking for is time
One more chance to prove
My love, my love, my love, my love
You don't have to say I'm wrong
'Cause I've know it all along
One more chance to prove
My love, my love, my love, my love

[Post-Chorus]
Woah, woah
My love, my love, my love, my love
Woah, woah
My love, my love, my love, my love

[Outro]
My love, my love, my love, my love
My love, my love, my love, my love