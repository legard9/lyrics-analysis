[Verse]
It's just the way it is
So many lives I lived
So many layers more
You're peeling off my core
You're unafraid and curious
You’re honest you are generous, uh-oo-hoo

[Chorus]
You let the moon shine in
But I can't break out of my dark pattern
And still you will take my hand
And I have to slowly unlearn everything
Unlearn and stay open

[Verse]
I have to look away
Retract into my space
And gather all my strength
To openly embrace
A familiar melancholy
Wanna shake it off and set it free
Set it free, uh-ho

[Chorus]
You let the moon shine in
But I can't break out of my dark pattern
And still you will take my hand
And I have to slowly unlearn everything
Unlearn and stay open
Open