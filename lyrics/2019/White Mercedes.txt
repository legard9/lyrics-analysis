Gonaïves looked white under the hot sun with a black sky behind it filled with rain that would not fall. It was even hot at sea, and it got hotter as they approached the stone and cinder-block ramparts.

The quay below was chaos. There were trucks and cars, but mostly large handcarts and children chasing them, hoping for something that dropped. The port official boarded and Izzy Goldstein told him it was “the NANH from Miami,” and the official, hearing “the nen from Miami,” smiled. Izzy supposed that the man was laughing at his French. The official said something in Creole and Izzy looked confused, and then the man said in very good English, “How much are you gonna pay to dock here?”

DeeDee took over and Izzy was led by a deckhand down to the crowd, and in the middle of it he was introduced to the most beautiful man he had ever seen. Jobo was tall, broadshouldered, lean, and muscular, and his skin had the satiny luster of burnished wood, perhaps a very dark walnut. He escorted Izzy to a polished white Mercedes that clearly did not belong there in the ramshackle port.

Jobo seemed a pleasant young man, there was a sweetness to him, but when he sat in the driver’s seat and turned the key, he was transformed. With his fist he pounded ferocious blasts of the car horn and left no doubt that anyone in his way would soon be under his tires. The crowd parted and they were on their way, climbing only slightly as they left the steamy dilapidated city and entered the last green village on the edge of a bone-colored Saharan landscape that rolled on and on like a sea.

Again Jobo honked the horn insistently in front of an iron gate, which, to the great excitement of Izzy, was fashioned into a swarm of black metal snakes. A boy appeared, and with every ounce of his small body managed to push the gate open. They entered a lush tropical world of ponds and fountains and green and orange broad-leafed plants and drooping magenta bougainvillea and coral-colored hibiscus sticking out their tongues suggestively. Rising above this forest were highpitched roofs and wide balconies.

They got out of the vehicle and stepped up to a wide, high-ceilinged porch with a tiled floor and large potted plants. Between two lazy banana bushes was a tall cage about two yards square. Inside was a leopard, lean with angry yellow eyes and ears cocked back and fur like silken fabric in black and rust and ocher. The cat was pacing back and forth, as though exercising to keep in shape. But Izzy couldn't help thinking about himself. He was hoping someone was about to offer him a tall, cold drink.

*****
When Haiti was sent away, many of the lwas-including Damballah, èzili, Legba, and Agwe -went as well, but most of the animals stayed in Africa. However, the goddess of love, èzili Freda, kept one leopard because she could not resist beautiful things. She wanted to keep the leopard the same way that she kept closets full of beautiful dresses and fine jewelry. The leopard tried to run away, so she kept it in a pink-jeweled cage.

*****
Jobo ushered Izzy inside, holding open a large glass door that did not fit with the rest of the house. Izzy’s body instantly hardened to a tense knot. It was as though he had walked into a refrigerator, possibly a freezer. He was not sure but thought he saw traces of vapor from his breath. A furry red creature glided toward him speaking the same formal and emotionless French of his ninth grade teacher who had always called him Pierre because she said there was no way to say Izzy in French.

"Bonjour, bienvenue. Comment allez-vous?" she said with a smile made of wood. She was wrapped in a thick red fox coat. Her body stuck out at angles, a hard thin body. Her straightened black hair was swept up on her head. She wore shiny dark-purple lip gloss with an even darker liner. Her green eyes were also traced in black, which matched the carefully painted polish on her long nails filed to severe points. All this dark ornamentation on her gaunt face made her skin look pale with a flat finish, like gray cardboard.


On one finger was a very large emerald that was close to matching her eyes, and when she held her long hand to her face, the stone appeared to be a third eye. She would have been attractive except that everything about her seemed hard. Even her face was boney. Maybe, Izzy thought, she understood this and wore the fur to try to appear softer.

She turned to Jobo and ordered him in French to fetch a cold bottle, which was exactly what Izzy wanted to hear. To Izzy it seemed odd-here he was, trying to learn Creole-that a Haitian would speak to another in French, even though Jobo answered only in Creole. Izzy soon realized that she also spoke nearly perfect American English. So who was the French for? Even when she spoke English, she punctuated everything with “N’est-ce pas?”

Jobo returned with two very long crystal champagne flutes and a bottle of champagne, which he opened with the craftsmanship of a well-trained wine steward. It was cold and bubbly with a flush of rose like the blush on her protruding cheekbones, though probably more natural.

“Pink champagne, n’est-ce pas?” she said. “Don’t you love pink champagne?”

“èzili’s drink,” said Izzy, who knew that the goddess loves luxury and her favorite color is pink. The smile flew off her face like a popped button, leaving Izzy to wonder what he had said that was wrong.

She offered him a building near the port that he could use as the NANH warehouse, although when he said “NANH warehouse,” she smiled. She could also provide a staff for distribution of the goods he brought in so that he simply had to bring them in and the rest would be taken care of. She asked nothing for this service, simply explaining, “I am Haitian and I love my people.” He was moved but he thought he detected a certain angry glow in Jobo’s eyes while she was speaking.

“All I ask, mon cher…” She paused and he thought maybe she was going to ask about aid to a favorite cause. Which in fact may have been the case. “Gasoline, N’est-ce pas?”

“Gasoline?”

“Mais, oui. Beaucoup, beaucoup. I will tell you how many barrels.”

“But, ah, Madame Dumas?” He was now so cold his teeth were chattering.

“Oui,” she said softly like a kiss.

“How do I justify spending relief money like that?”

“Ah-bas, c’est tout correct, n’est-ce pas. It is an operating expense, n’est-ce pas? It’s for my generators,” and she moved her green eyes across the ceiling. “This takes a lot of gasoline, n’est-ce pas? And then there are the freezers for the meat, n’est-ce pas?”

He supposed that she was keeping meat for the village and that would be a worthwhile thing to subsidize. Far safer than leaving meat out in this tropical heat. Although you could keep food fresh forever in this living room.

“As a matter of fact, I am going to buy a freezer compartment for your ship. You can bring down meat.”

“That is a wonderful idea. Put some protein in people’s diets.”

“Eh, oui,” she replied in a distant philosophical tone. “Jobo, this reminds me. Feeding time.” And then she said something in Creole that Izzy didn’t grasp, though it sounded like a comment about Jobo’s shirt, which he then removed as he went out into the heat. She smiled at Izzy and added, “He is too beautiful for clothes, n’est-ce pas?”

Izzy nodded, unsure of how to answer.

“So it’s all arranged. My man is paying them off so you can unload right now”

“Paying the…?”

“All taken care of,” she said merrily, with a gesture like washing her hands. He was informed that he would be staying in Madame’s house, which he did not feel entirely comfortable about, but he had no other ideas of where to stay.

He was put in a room just as cold, with carved wooden panels and a ceiling fan for which there was no real use. Evidently the air-conditioning could not be turned down or off, and the windows did not open. But the bed was equipped with fluffy goose-down quilts imported from Austria.

Izzy went outside to warm up. Jobo, with a large ring holding many keys, was coming from a wooden shed with a package. He stepped up to the porch and over to the leopard cage. He unwrapped the package and took out what looked like two sirloin steaks. Izzy assumed he was mistaken about the cut, but the steaks were nicely marbled. All the while the leopard paced, stopping only for a second to snarl. The animal was dangerous and Izzy could see claw marks-parallel lines on both sides of Jobo’s shirtless back.

“Jobo, is there some kind of a ceremony I can see?”

Jobo showed a sweet smile. “You want to see some real Vodou?”

“Yes, exactly.”

“I can arrange it, but it koute chè.”

“How expensive?”

“Anpil. Anpil. I will take you to Kola.”

“Kola is the ougan?

“He’s a bòkò. He can fix it. I’ll go talk to him now.” And with that, shirtless and with claw marks showing, he walked down the driveway and out the gate of iron snakes.

Izzy sat on the porch watching the incessant pacing of the leopard. The cat had one of the steaks in his mouth but he didn’t stop moving, not even to eat. Izzy thought about the Vodou priest named Kola. Did “kola” mean a line? A queue? It must mean something.

The leopard pleaded with Agwe that he did not want to be locked in a cage and asked to be taken back to Africa. But Agwe said, “I can only take spirits back after they die.”

“Then kill me.  I want to go back,” said the leopard.

Haitians like nicknames. Dieudonné was DeeDee. Ti Morne Joli was always called Joli. Madame Dumas was Lechat, the bòkò was Kola, Jobo was Beau. And Izzy? Everyone in Joli called him Blan.

“What is this blan up to?” asked Kola, a short stocky man with a powerful body, shirtless like Jobo, sitting under a leafy tree, on the stripped-bare engine block of a long-dead car. All the other parts had been sold and someday the block would be too. He dug in the earth with a trawl and pulled out two small green Coca-Cola bottles, felt them to see if the ground had kept them cool, dusted them off with his thick but skilled fingers, and handed one to Jobo.

Jobo smiled. “What do blans want? He wants a Vodou ceremony.”

“A Vodou ceremony?” Kola had a wide toothy smile. He rubbed his stomach. He was proud of his belly because he was the only one in Joli who had one. “San dola. Tell him there is a nice ceremony for a hundred dollars. For one-fifty I can show him something special.”

Jobo nodded.

“But what does this blan want? Is he bringing blan doctors and their medicines?”

“No, I don’t think so. But I wanted to talk to you about your medicines.”

“You need a powder?” asked Kola.

Jobo looked at the ground and shook his head.

“Do you have money for my powders? What do you need? A rash, a headache, a fever, the stomach? What would you like to do?”

“You know my aunt’s baby died today?”

“I know.”

“Do you know how much meat Madame Lechat has? Do you know? Anpil, anpil. Three big freezers. It all goes to that cat. If she died, everyone in Joli could eat meat for three weeks.”

“Yes, but for a great lady like that, that koute chè. Do you have that money?”

“No. But if I did, you could help me?”

“It would cost less to kill the leopard. Then she wouldn’t need the meat. Maybe you could take it.”

“I don’t just want the meat.”

“What do you want?”

“M vle jistis.”

“Ah, justice. Justice costs. Justice is very expensive.”

Izzy was pacing the porch, almost the same strides as the leopard, though he didn’t realize it.  Something about that animal made him restless. That and an occasional harsh cry from upstairs. “Jobo! Jobo!” Finally, she came downstairs and out onto the porch. The light from inside was shining on her. She was wearing a long silk shift and he could see that there was no shape to her body-just long and thin. He also saw through what was left of the makeup that she was a bit older than he had first thought.

“Have you seen Jobo?”

“He went to the ougan to arrange a ceremony for me.”

“The oun…?”

“Kola?”

“Ah, the bòkò, Kola.” Then her green eyes darted past him. It was Jobo coming back. Izzy sensed that he should retreat to the other end of the long tiled porch.

“Jobo,” she called out. “Jobo, viens ici. Viens.” She spoke in that melodious high pitch used by Frenchwomen when calling their pets.

And he did come, and she put her arms around him, her cardboard gray hands looking bright against his dark back as she dug her black-polished nails into his skin.

“DeeDee, can you help me?” asked Jobo.

“What do you need?”

“Money.”

DeeDee laughed.

“I need to pay for something very expensive. Just one time I need some money. I can work.”

“Why don’t you ask the blan?”

“This is not the blan’s business.”

DeeDee understood and told him that he was loading a shipment of mangoes on the NANH late that night.

“I’m not sure I can get away at night.”

“Late-late. I am paying very well for this particular shipment-of mangoes. Give her a lot of champagne.”

“Mais oui.”

DeeDee paid off all the port officials with money from Madame Dumas and the NANH untied and set her bow northwest to round the peninsula and head to Miami. The mangoes helped the ballast but the freighter was still sitting a little too high in the water. They had to hope there were no storms. Izzy was surprised when he inspected that the mangoes were just piled in the hold without any crates. “It will take forever to off-load,” he complained. DeeDee shrugged.

Then Izzy noticed they were off course, but DeeDee explained that they had to make a quick stop.

“To take on more ballast?” asked Izzy.

There was no answer, but DeeDee was busy navigating. They dropped anchor by a reef-a strip of white sand and a grove of palm trees in the middle of the turquoise sea. Izzy saw nothing heavy to load on the boat.

Then the crew lifted the cover off the hold and Izzy was astounded by what he saw next. Haitian men and women, one child of about eight, under the yellow mangoes. They staggered up, their limbs stiff and their eyes blinded by the hot light. Some were almost naked. They were hurriedly helped to the beach on their shaky legs. There were eleven of them, including three who were dragged and appeared to be dead.

Shouting erupted in Creole. Arms flayed the hot air angrily. They were saying, “This is not Miami! You took our money!” Some pleaded, “Please, don’t leave us.” But DeeDee insisted that this boat was too big to bring them in and that small boats would come tonight to drop them on the Florida coast.

Izzy was angry and fought with DeeDee all the way to Florida. DeeDee’s answers made no sense to him.

“Why are we doing this?”

“Because we can’t bring them into Miami.”

“Why were we carrying them at all?”

“They needed the help, Izzy.”

“I have to tell the Coast Guard. They’ll starve in that place.”

“No. It’s all arranged. Boats will come for them tonight.”

“They said they paid. Who got the money?”

“The mango growers.”

They tied up on the Miami River.  But they were not going to be able to return to Haiti: there was a coup d’etat. Little Haiti was intoxicated with the news. A new government was being formed. There were curfews. There was rioting in Port-au-Prince. In Gonaïves, a mob attacked the NANH warehouse, took everything, then tore down the building a chunk at a time with rocks and machetes. After a day, all that remained of the two-story building was a few steel reinforcing rods sticking out of the ground.

DeeDee soon vanished and it was said in Little Haiti that he was now an official in the new government. Izzy hadn’t realized he was involved in politics. He had never seemed interested in anything but commerce. Then a man approached Izzy one afternoon alongside his boat on the river. Izzy recognized him. He was usually in Bermuda shorts with an I Love Miami T-shirt , a Marlins hat, and a camera. But this day he was wearing a suit and showed Izzy something that said he was an FBI agent.

Kola had a new Coca-Cola cabinet. It was red-and-white metal with a glass door. A stray rock from the riot had dented one side but the door was intact.

Of course, it didn’t keep anything cold because Kola had no electricity. But it was a good cabinet and he kept it behind the temple to store his bones, herbs, potions, and powders. His Coca-Cola was still in the ground where it was cool.

Soon Madame Dumas began experiencing something completely new to her. She started to sweat. Even in her airconditioning she was sweating. It poured out of her forehead and ran down her fine cheekbones, and from under her arms a rivulet flowed to the small of her back; under her breasts, sweat soaked her stomach. Her pink silk shift had turned cranberry with wetness. And as the sweat poured out, she became weaker and weaker-while Jobo watched.

Madame Dumas collapsed on the living room floor and crawled to the couch. She looked up at Jobo with her arm reaching out at him. “Jobo, aide-moi.” Help me.

He only stared at her.

“I need a doctor.”,

“I can’t get a doctor. The roads are closed. The coup.”

“Oh, yes, the coup,” she muttered, as though there was a secret irony to this that only she could appreciate. “Then the bòkò. Can’t he make a powder to fix me?”

“Mais oui,” Jobo answered, appreciating his own secret irony.

“Vas-y. Get something!”

Jobo left and did not come back for hours. When he did, Madame Dumas was not sweating anymore. She was stretched across the cool floor tiles-dead.

Jobo unceremoniously removed all her clothes and carried her out to the leopard cage. He opened the door and dumped her on the floor.  The leopard, who he had not fed in three days, was so startled that he stopped pacing. He walked over to the body and sniffed it as Jobo started to close the cage. Suddenly, the cat leapt over Jobo, knocking him down, and off the porch into the bush, over the wall in graceful flight, and was never seen again. He might have run to the arid desert in the northwest and managed to find a way to survive there. Or maybe he ran along the Artibonite River to hide out in the mountains above the valley where many others have hidden.

The leopard had left Jobo with the question of what to do with the unwanted remains of Madame Dumas. He consulted Kola but neither could come up with a good solution. Several days later, while Jobo was still contemplating this dilemma, someone started clanging the locked gate. Jobo ran down and saw Kola framed in black iron snakes. He explained that Madame had a family that wanted both the house and the body. They wanted to bury it in France, but Air France had suspended flights because of the coup.

“Eh oui,” said Jobo, who had never seen an airplane close up, with feigned comprehension.

“Poutan!” shouted Kola, raising his stubby index finger to make a point. “I told them if they want to come get the body in a week or even two, I can use magic to keep it in perfect condition.”

“Magic?”

“Mais oui. And you have that magic in your house. It is the magic of meat.”

Now Jobo understood. “And they will pay?”

“Gwo nèg koute chè,” Kola said. You have to pay a lot for an important person.

Jobo smiled. “Anpil, anpil dola?”

“Anpil. Very expensive.”

After Kola left, Jobo went back to the cage and picked up Madame. She still had not stiffened much. He emptied one of the big top-loading freezers and dumped her in. She landed in a most undignified pose and was soon petrified in ice, to be thawed and served up properly by magic in due time.

Jobo was right. Once the freezers had been emptied, the people of Ti Morne Joli ate meat for three weeks.  Many became sick because they were unaccustomed to such a rich diet. But it was not likely to happen again.
*****
It was Damballah who finally confronted èzili, bribed her with dresses and bracelets and pink elixirs until she set the leopard free. The animal ran and ran and ran, as though he could run all the way back to Africa. But the ocean was there now. He ran so hard that he turned into a man. That was the first Haitian, and that is why Haitian people always struggle so hard to be free

*****

“I want to talk to my lawyer,” said Izzy.

“I think you need a new lawyer. He’s been arrested. Seems you were just a small part of the operation.”

Izzy thought, They arrested the Anglo. Isn’t that something? They got the Anglo. Then he spoke: “Why do you say that the goods were stolen? Everything was paid for.”

“We were watching you. What tipped us off was that you had Coca-Cola machines. You can’t buy them. Only the Coca-Cola Company owns them. You’re not the Coca-Cola Company, are you?”