InternationalRanking (100)100. Andrew Huang, "Summertime Feeling"
99. Mark Ronson, "Late Night Feelings" (featuring Lykke Li)
98. Alec Wigdahl, "Misbehaving"
97. The Unlikely Candidates, "Novocaine"
96. Lil Peep and iLoveMakonnen, "I've Been Waiting" (featuring Fall Out Boy)
95. Juice WRLD, "Robbery"
94. The Amazons, "Mother"
93. DJ Shadow, "Rocket Fuel" (featuring De La Soul)
92. Common, "Good Morning Love" (featuring Samora Pinderhughes)
91. BADBADNOTGOOD, "Key To Love (Is Understanding)" (featuring Jonah Yano)
90. Guts, "Kenke Corner"
89. Skyzoo & Pete Rock, "Eastern Conference All-Stars" (featuring Elzhi, Benny The Butcher, Conway The Machine and Westside Gunn)
88. Beck, "Tarantula"
87. Chronixx, "Jah Is There"
86. The Cranberries, "All Over Now"
85. The National, "Light Years"
84. Free Nationals, "Time" (featuring Mac Miller and Kali Uchis)
83. WALK THE MOON, "Timebomb"
82. COUNTERFEIT. "It Gets Better"
81. Open Mike Eagle, "The Edge of New Clothes"
80. Nipsey Hussle, "Racks In The Middle" (featuring Roddy Ricch and Hit-Boy)
79. CupcakKe, "Squidward Nose"
78. Tierra Whack, "Only Child"
77. Charlie Puth, "Mother"
76. Tkay Maidza, "Awake" (featuring JPEGMAFIA)
75. Flying Lotus, "Black Balloons Reprise" (featuring Denzel Curry)
74. James Blake, "Barefoot In The Park" (featuring ROSALÍA)
73. Skip Marley, "That's Not True" (featuring Damian Marley)
72. 2 Chainz, "Forgiven" (featuring Marsha Ambrosius)
71. Aloe Blacc, "Getting Started" (featuring JID)
70. Ari Lennox, "BMO"
69. R.LUM.R, "Boys Should Never Cry"
68. Masego, "Big Girls"
67. Mereba, "Sandstorm" (featuring JID)
66. Lil Nas X, "Old Town Road"
65. Brittany Howard, "History Repeats"
64. 1K Phew, "How We Coming" (featuring Ty Brasel and WHATUPRG)
63. Rich Brian, "Yellow" (featuring Bekon)
62. Doja Cat, "Tia Tamera" (featuring Rico Nasty)
61. Tank And The Bangas, "Forgetfulness"
60. Obongjayar, "Still Sun"
59. Blood Orange, "Benzo"
58. H.E.R. "Lord Is Coming (Remix)" (featuring YBN Cordae)
57. Avicii, "SOS" (featuring Aloe Blacc)
56. Calvin Harris and Rag'n'Bone Man, "Giant"
55. black midi, "bmbmbm"
54. Zach Fox and Kenny Beats, "Jesus is the One (I've Got Depression)"
53. Madame Monsieur, "Comme un homme" (featuring Youssoupha)
52. Coldplay, "Arabesque"
51. Missy Elliott, "Throw It Back"
50. Charles Bradley, "Lonely As You Are"
49. Durand Jones & The Indications, "Sea Gets Hotter"
48. Moses Sumney, "Virile"
47. 21 Savage, "a lot" (featuring J. Cole)
46. Channel Tres, "Black Moses" (featuring JPEGMAFIA)
45. Protoje, "Not Another Word" (featuring Lila Iké and Agent Sasco)
44. AWOLNATION, "California Halo Blue"
43. Dreamville, "Sacrifices" (with J. Cole, EARTHGANG, Smino and Saba)
42. Quelle Chris, "WYRM"
41. nobigdyl. "STIX"
40. Gang Starr, "Family and Loyalty" (featuring J. Cole)
39. Danny Brown, "Best Life"
38. Yola, "Faraway Look"
37. JPEGMAFIA, "Jesus Forgive Me..."
36. Florence + The Machine, "Moderation"
35. Anna Wise, "Abracadabra" (featuring Little Simz)
34. Jade Bird, "My Motto"
33. Rapsody, "Ibtihaj" (featuring D'Angelo and RZA)
32. Lizzo, "Tempo" (featuring Missy Elliott)
31. EARTHGANG, "UP"
30. Skepta, "Glow In The Dark"
29. Sampa The Great, "Final Form"
28. Andy Mineo, "I DON'T NEED YOU (DEMO).wav"
27. Denzel Curry, "RICKY"
26. Sam Fender, "Hypersonic Missiles"
25. Los Chikos del Maíz, "El Extraño Viaje" (featuring Ana Tijoux)
24. YBN Cordae, "RNP" (featuring Anderson .Paak)
23. Injury Reserve, "Jawbreaker" (featuring Rico Nasty and Pro Teens)
22. Abi Ocia, "Offering"
21. Mavis Staples, "Change"
20. Samm Henshaw, "Church" (featuring EARTHGANG)
19. Michael Kiwanuka, "You Ain't The Problem"
18. AURORA, "The Seed"
17. Billie Eilish, "all the good girls go to hell"
16. Vampire Weekend, "Harmony Hall"
15. Little Simz, "Venom"
14. Black Pumas, "Colors"
13. Sjava, "Umama"
12. J. Monty, "100 Bars, Part 4"
11. Naughty Boy, "Bungee Jumping" (featuring Emeli Sandé and Rahat Fateh Ali Khan)
10. Anderson .Paak, "Come Home" (featuring André 3000)
9. Koffee, "Rapture"
8. Charlie XCX, "Gone" (featuring Christine and the Queens)
7. Tyler, The Creator, "EARFQUAKE"
6. Joy Oladokun, "Sunday"
5. clipping, "Run For Your Life" (featuring La Chat)
4. Marlon Craft, "Gang S**t"
3. Seba Kaapstad, "Don't"
2. The Killers, "Land of the Free"
1. BROCKHAMPTON, "NO HALO"
ItaliaClassifica (50)50. DJ Fastcut, "Diss In Formazione"
49. Tre Allegri Ragazzi Morti, "C'era un ragazzo che come me non assomigliava a nessuno"
48. Boomdabash, "Per un milione"
47. Francesco Renga, "Normale" (con Ermal Meta)
46. Gianna Nannini, "La differenza"
45. Laïoung, "5 € per morire"
44. Nek, "La storia del mondo"
43. Charlie Charles e Dardust, "Calipso" (con Sfera Ebbasta, Fabri Fibra e Mahmood)
42. Tommaso Paradiso, "Non avere paura"
41. Ermal Meta, "Un'altra volta da rischiare" (con J-Ax)
40. PSICOLOGI, "Alessandra"
39. Achille Lauro, "1969"
38. Zucchero, "Spirito nel buio"
37. Ultimo, "Quando fuori piove"
36. Ligabue, "Luci d'America"
35. Fiorella Mannoia, "Imparare ad essere una donna"
34. Loredana Berté, "Cosa ti aspetti da me"
33. Biagio Antonacci, "Ci siamo capiti male"
32. Renato Zero, "La vetrina"
31. Takagi & Ketra, "La luna e la gatta" (con Jovanotti, Tommaso Paradiso e Calcutta)
30. Franco Battiato, "Torneremo ancora"
29. Francesca Michielin, "CHEYENNE"
28. Arisa, "Mi sento bene"
27. Colapesce e MACE, "Immaginario"
26. Fadi, "Canzone leggera"
25. Giovanni Truppi, "L'unica oltre l'amore"
24. Ghemon, "Rose viola"
23. Jovanotti, "Nuova era"
22. Dolcenera, "Amaremare"
21. Rkomi, "Blu" (con Elisa)
20. Mina e Ivano Fossati, "Tex Mex"
19. Cesare Cremonini, "Al telefono"
18. Motta, "Dov'é l'Italia"
17. Fabrizio Moro, "Per me"
16. Vinicio Capossela, "Il povero Cristo"
15. Gomma, "Tamburo"
14. Paola Turci, "L'ultimo ostacolo"
13. Margherita Vicario, "Abaué (Morte di un Trap Boy)"
12. Lucio Corsi, "Cosa faremo da grandi?"
11. Marco Mengoni, "Duemila volte"
10. Nada, "Dove sono i tuoi occhi"
9. Murubutu, "Le notti bianche" (con Claver Gold)
8. Coma_Cose, "MANCARSI"
7. Clementino, "Babylon" (con Caparezza)
6. Eugenio in Via Di Gioia, "Lettera al prossimo"
5. Mahmood, "Soldi"
4. Levante, "Lo stretto necessario" (con Carmen Consoli)
3. Daniele Silvestri, "Argentovivo" (con Rancore e Manuel Agnelli)
2. Brunori Sas, "Al di là dell'amore"
1. Niccolò Fabi, "Io sono l'altro"
Menzioni speciali/Honorable mentionsEsclusi/Excluded150. Zacari, "Midas Touch"
149. Pivot Gang, "Studio Ground Rules"
148. Bobby Oroza, "Your Love Is Too Cold"
147. Blu and Exile, "True & Livin'"
146. Paul McCartney, "Get Enough"
145. Kelsey Lu, "Why Knock For You"
144. Danger Mouse & Sparklehorse, "Ninjarous" (featuring MF DOOM)
143. Little Brother, "Black Magic (Make It Better)"
142. Inspectah Deck, "Can't Stay Away"
141. Wiki and Madlib, "Eggs"
140. KOTA The Friend, "For Colored Boys"
139. Mark Morton, "Cross Off" (featuring Chester Bennington)
138. Lil Tecca, "Ransom"
137. Steve Lacy, "Playground"
136. BJ The Chicago Kid, "Time Today (Remix)" (with Ari Lennox)
135. Sam Smith, "How Do You Sleep?"
134. 03 Greedo and Kenny Beats, "Disco..." (featuring Freddie Gibbs)
133. Ant Clemons, "4 Letter Word"
132. Rico Nasty, "Time Flies"
131. Noname, "Song 32"
130. Ziggy Marley, "Friday's On!"
129. Beast Coast, "Left Hand"
128. Chris Rivers, "N.A.S.A." (featuring Oswin Benjamin)
127. Pusha T, "Coming Home" (featuring Lauryn Hill)
126. Kembe X, "Voices"
125. The Snuts, "All Your Friends"
124. The 1975, "People"
123. Ghetto Sage, "Häagen Dazs"
122. Kelly Finnigan, "I Don't Wanna Wait"
121. Tom Walker, "Better Half Of Me"
120. Tinariwen, "Tiqkal Tarha" (featuring Micha Nelson)
119. NSG, "Natural Disaster"
118. Norah Jones, "Take It Away" (featuring Tarriona "Tank" Ball)
117. Bombay Bicycle Club, "Eat, Sleep, Wake (Nothing But You)"
116. Beyoncé, "MY POWER" (featuring Tierra Whack, Moonchild Sanelly, Nija and Busiswa)
115. Lee Fields and The Expressions, "You're What's Needed In My Life"
114. Barns Courtney, "Hollow"
113. Emeli Sandé, "Sparrow"
112. Dua Lipa, "Swan Song"
111. Alfie Templeman, "Stop Thinking (About Me)"
110. Skunk Anansie, "What You Do For Love"
109. OneRepublic, "Rescue Me"
108. NF, "Time"
107. ROSALÍA, J Balvin and El Guincho, "Con Altúra"
106. slowthai and Denzel Curry, "Psycho"
105. L'Orange & Jeremiah Jae, "Dead Battery"
104. Amaarae, "Spend Some Time" (featuring Wande Coal)
103. Phora, "The Dream"
102. Booba, "Arc-en-ciel"
101. The Weeknd, "Blinding Lights"

---

72. Dimartino, "Giorni buoni"
71. Dente, "Adieu"
70. Birthh, "Supermarkets"
69. Frenetik & Orang3, "The Giant" (con Wrongonyou)
68. Irene Grandi, "I passi dell'amore"
67. Gino Paoli, "Inverno: Quando avevo vent'anni / Eravamo sempre insieme / Ho incontrato la tristezza / Voglio morir malato"
66. I Hate My Village, "Tony Hawk of Ghana"
65. Max Pezzali, "In questa città"
64. Ex-Otago, "Solo una canzone"
63. Negrita, "I ragazzi stanno bene"
62. CLAVDIO, "Cuore"
61. Negramaro, "Cosa c'é dall'altra parte"
60. Vasco Rossi, "Se ti potessi dire"
59. Emma, "Stupida allegria"
58. Funk Shui Project e Davide Shorty, "Solo con me" (con Johnny Marsiglia)
57. Willie Peyote, "La tua futura ex moglie"
56. The Zen Circus, "L'amore é una dittatura"
55. Aiello, "LA MIA ULTIMA STORIA"
54. Ensi, "MIRA" (con Madame)
53. Francesco Gabbani, "É un'altra cosa"
52. Elisa, "Vivere tutte le vite" (con Carl Brave)
51. LIBERATO, "OI MARÌ"
Meritevoli/New Discovers- Amerigo Gazaway, "B.I.G. Poppa's Got A Brand New Bag"
- Les Amazones d'Afrique, "Heavy"
- Coop, "Help Me" (featuring Drew Weeks)
- A. I. The Anomaly, "Eastside"
- Spirit, "Follow God (Remix)"
- Oswin Benjamin, "Sway's Universe Freestyle"
- Nolan The Ninja, "Oranges"
- Reconcile, "Had to Be Strong"
- Aaron Cole, "FASHO"
- Marshmello, "Tongue Tied" (featuring YUNGBLUD and blackbear)
- BABii, Kai Whiston, Iglooghost, "Lamb"
- The Struts, "Dancing In The Street"
- Caribou, "Home"
- Arlo Parks, "george"
- DRAM, "The Lay Down" (with H.E.R. and watt)
- The William Singers, "Don't Give Up"
- The Black Keys, "Go"
- Annie Lennox, "Apatura Iris"
- Jake James, "Two"
- Lenny Kravitz, "Here To Love"
- The Good Ones, "Despite It All I Still Love You, Dear Friend" (featuring Tunde Adebimpe)
- Jay Park, "Legacy" (featuring Shelby The Singer and Gifted Gab)
- FKJ, "Risk" (featuring Bas)
- Alex Cameron, "Divorce"
- John Legend, "Preach"
- Katy Perry, "Never Really Over"
- ASADI, "Palace"
- GKR, "ENN AĐ LÆRA"
- Rema, "Dumebi"
- Angelique Kidjo, "La Vida Es Un Carnaval"
- Marracash, "MADAME - L'anima" (con Madame)
- Andrew Bird, "Sysiphus"
- Alice Merton, "Learn to Live"
- Imagine Dragons, "Birds (Remix)" (with Elisa)
- Tom Petty and The Heartbreakers, "For Real"
- Purple Mountains, "Darkness and Cold"
- Leonard Cohen, "Thanks for the Dance"
- AMA, "Slip"
- Manafest, "Plan For Me"
Non classificabili/Uneligible- J. P. Bimeni & The Black Belts, "I Miss You"
- Thee Lakesiders, "Parachute"
- Mumford & Sons, "Woman"
- half•alive, "still feel."
- Silverson Pickups, "It Doesn't Matter Why"
- Papa Roach, "Not the Only One"
- Chaka Khan, "Like Sugar"
- Lecrae and Andy Mineo, "Coming In Hot"
- Alfa Mist, "Keep On"
- Brandi Carlile, "The Mother"
- Hozier, "Nina Cried Power" (with Mavis Staples)
- Nujabes, "Another Reflection"
- Afro B, "Drogba (Joanna)"
- for KING & COUNTRY, "God Only Knows"
- Oddisee, "You Grew Up"
- Daymé Arocena, "La Rumba Me Llamo Yo"
- Andy Mineo & Wordsplayed, "DANCE (YOU SEE IT)"
- Skip Marley, "Refugee"
- Young Fathers, "In My View"
- American Authors, "Say Amen" (with Billy Raffoul)
- Seinabo Sey, "I Owe You Nothing"
- Death Grips, "Hacker"
- Janka Nabay & The Bubu Gang, "Somebody"
- Sharon Jones & The Dap-Kings, "I Learned The Hard Way"
- Tom Grennan, "Barbed Wire"
- Gaelynn Lea, "Someday We'll Linger In The Sun"
- David Bowie, "I Can't Give Everything Away"
- Mumford and Sons, "Woman"
- Pangolin, "Double Consciousness"
- Kae Sun, "The Moment"
- Frankenstein, "The Rain Is Gone"
- The Roots, "The OtherSide"
- Mike Shinoda, "World's On Fire"
- Alessandra Amoroso, "Forza e coraggio"
- Any Other, "Walkthrough"
- Lewis Capaldi, "Someone You Loved"
- Tank And The Bangas, "I'll Be Seeing You"
- SBTRKT, "Hold On" (featuring Sampha)
- Sampha, "No One Knows Me (Like The Piano)"