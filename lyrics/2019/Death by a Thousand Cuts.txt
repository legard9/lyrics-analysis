[Intro: Drakos (G2)]
What up, Vedi?
Rapping OG this time
Just another loser to bet on

[Intro: Vedius (OG)]
Uh, Daniel Drakos
Hosting, casting, rapping
Tell me, do you wake up mediocre
Or is it just the conscious death?

[Pre-Verse 1: Drakos (G2)]
Maybe we should've let you film in that bathtub with xPeke
Seeing how much brown-nosing OG
What else I got?

[Verse 1: Drakos (G2)]
Motivation, work, improvement, team
Your only recent achievement was going 0 and 13
Mechanics like you ain't got hands
Brain dead boy, you don't understand
Split up the map, that's your only plan
1-3-1  and the game's done, man
One good half a split under your belt
You think you're back at the top
Hate to break it to you
You and your boosted crew
Play G2 and you'll get dropped
Number one for a reason
Crushed the whole season
We'll leave you crying
We'll leave you bleeding
We gonna end this you bet
Remember the first time we met?
Killed 'em so many times thinking that they're color blind
Son, it's just a grey screen, tab and check your score line
We can beat you with MikyX
We can beat you with PromisQ
No matter what you try to do
It's never gonna be enough for you
From fiestas to siestas, boy, you put me to sleep
Nobody wants to watch you stall a game for a week

[Pre-Verse 2: Vedius (OG)]
Sure, Drakos, you pretty good
But honestly that Tsepha was much better

[Verse 2: Vedius (OG)]
Forget the name "kings", we're the OG
Put your hands together if you all remember me
We were here for the best years
2015 and the whole world cheers
Carried EU unlike G2
Dropped out of groups, fans coming for you
That Origen original
When we beat you, it'll be clinical
A thousand cuts is all it's gonna take
Slow and steady's how we win this race
Based on recent play I thought MikyX was PromisQ
My nan could land more skillshots and she's blind, I promise you
You been skipping steps like Carlos skipping leg day
50/50 barons, what the fuck does that say?
That you'll risk it all on a single call?
Lost to SK, now I see the fall
You can't keep up, so you playing safe
How many excuses 'till we seal your fate?
It's bad drafts, it's bad reads
It's Miky's wrist, it's just greed
When are you gonna admit
Your boys are just playing like shit?

[Pre-Verse 3: Drakos (G2)]
Vedius, my boys playing like shit is still twice as good as your boys
You should have to go through some motherfucking play-ins
Even to get a chance to go toe to toe with the best

[Verse 3: Drakos (G2)]
OG with revisionist history
Like 2016 was a mystery
Don't worry, Europe, I won't let you forget
Worlds to relegations, the worst record yet
Pray you can beat me, pray for that TP
Bet on that backdoor, boy, you're just greedy
Nuke gonna get nuked, boy, ain't got no jukes
Making a career out of being third best
Caps gonna cap him, remake like a bad scrim
Versus G2, gonna die like the rest
Breaking the game like we're certainlyT
Akali outplaying, I get those for free
Is this a botlane, one that can challenge
Someone can push us, someone with talent?
No, it's just Patrik, just a little scared kid
Just picked up 'cause no one tried to outbid
But it's okay 'cause we paired him with Mithy
He might be washed-up but his brand is nifty
He can't push his buttons, he does seem to int
He sounds nice in interviews, I guess that's it
But hey, OG, you won the grand prize
This'll be your only high
To lose to G2 first
So everyone can see that you're the worst
Come back in finals, we'll hit you twice
Might give you a game if we're nice

[Pre-Verse 4: Sjokz (OG)]
Hey, Vedi, what?
You really think you need help with this fool?
Are you kidding me?
He can't even cast a Worlds quarterfinal
Like, like serious?

[Verse 4: Sjokz (OG)]
G2 vacation started early this year
You're losing to Splyce, ha, we've got nothing to fear
Wunder solving world hunger
Jankos missing his shit
Perkz' Sivir mid lane
The boy can't even right-click
Remember the last time we met?
Caps solo killed, no threat
Closed it out cleanly, you bet
Give us that Zed, Nuke's fed
Give us that Zed, you're dead
I'm not even boasting
Caps has a champion pool
But our boy Nuke has an ocean
So get ready to drown in it
We'll beat you down with it
Excuse me, Mr Jankos, what's that caught in your throat?
Another best of five, you're choking, your career is a joke
But at least you're better than Perkz
Ran from his own lane, that's worse
So keep hiding bot side, Caps will come in a jiffy
In the meantime please enjoy getting smacked up by Mithy
I see the cracks in the surface
I know your flexpicks are worthless
You know your empire's about to expire
You know that you do not deserve this
So try another verse
Go on and try another diss
You can take a shot at the queen but you best not miss