[Chorus]
'Cause we made it
Underestimated
And always underrated
Now we’re saying goodbye
Waving to the hard times
Yeah, it's gonna be alright
Like the first time
Met you at your doorstep
Remember how it tasted
Looking into your eyes
Baby, you were still high
Never coming down with your hand in mine

[Post-Chorus]
(Yeah, yeah, yeah)
(Yeah, yeah, yeah)
(Yeah, yeah, yeah)

[Verse 1]
Oh my, I remember those nights
Meet you at your uni', cheap drinks, drink ’em all night
Staying out 'til sunrise
Share a single bed and tell each other what we dream about
Things we'd never say to someone else out loud
We were only kids, just tryna work it out
Wonder what they'd think if they could see us now, yeah

[Chorus]
'Cause we made it
Underestimated
And always underrated
Now we're saying goodbye (Goodbye)
Waving to the hard times
Knew that we would be alright
From the first time
Met you at your doorstep
Remember how it tasted
Looking into your eyes
Baby, you were still high
Never coming down with your hand in mine

[Post-Chorus]
(Yeah, yeah, yeah)
(Yeah, yeah, yeah)
(Yeah, yeah, yeah)
(Yeah, oh)

[Verse 2]
Oh, God, what I could've become
Don't know why they put all of this on us when we’re so young
Done a pretty good job dealing with it all
When you’re here, don't need to say no more
Nothing in the world that I would change it for
Singing something pop-y on the same four chords
Used to worry ’bout it, but I don't no more, yeah

[Chorus]
'Cause we made it
Underestimated
And always underrated
Now we're saying goodbye (Goodbye)
Waving to the hard times
Smoke something, drink something
Yeah, just like the first time
Met you at your doorstep
Remember how it tasted
Looking into your eyes
Baby, you were still high
Never coming down with your hand in mine

[Post-Chorus]
(Yeah, yeah, yeah)
(Yeah, yeah, yeah) With your hand in mine
(Yeah, yeah, yeah) No, no, no, no
’Cause we made it
(Yeah, yeah, yeah)
(Yeah, yeah, yeah) Yeah, 'cause we made it
(Yeah, yeah, yeah)
Never coming down with your hand in mine

[Outro]
(Yeah, yeah, yeah)
(Yeah, yeah, yeah)
('Cause we made it)
(Yeah, yeah, yeah)