[Verse 1]
Mother, oh mother
Is it time to wake up yet?
I know I'm not ready
Mother, oh mother
Is it time to let go now?
I know I'm not ready

[Pre-Chorus]
Something in the woods is coming close enough to hear
If I close my eyes, will I disappear?

[Chorus]
Let's just stay asleep
'Cause when I sleep, I still can see you
Let's just stay asleep
(Ooh)

[Verse 2]
Mother, oh mother
Do I have to come in now?
Is the day almost over?
Mother, oh mother
Do I have to move on now?
Is this almost over?

[Pre-Chorus]
Someone said the sky is falling, tell me, is it true?
Everything's alright when I'm with you

[Chorus]
Let's just stay asleep
'Cause when I sleep, I still can hear you
Let's just stay asleep

[Bridge]
"Just close my eyes"
You said, you said
"Just close my eyes"
"Just close my eyes"
You said, you said
"Just close my eyes"
(Ah-ooh)

[Chorus]
Let's just stay asleep
'Cause when I sleep, I still can feel you
Let's just stay asleep

[Outro]
Let's stay asleep
(Ah-ooh)