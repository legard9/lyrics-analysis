[Verse 1]
You don't say I'm pretty
Not like you used to
And I'd never admit it
I'm trying so hard for you

[Pre-Chrous]
But all of the moments I treasure
In the morning you won't remember
Don't know why I can't keep it together, long enough to try

[Chorus]
I make big things out of little things
And I watch you keep missing them
You don't get why it's killing me, every time you mess this up
Set myself on fire to keep us warm
Swear I lose my mind watching you ignore
All the little things, the little things mean so much more

[Verse 2]
So no, I'm not finished, no
Don't look at me crazy
I hate when you say shit you don't mean
Just 'cause you're angry, mmh

[Pre-Chrous 2]
'Cause all of the moments I treasure
You're too messed up to remember
Don't know why you can't keep it together, long enough to try

[Chorus]
I make big things out of little things
And I watch you keep missing them
You don't get why it's killing me, every time you mess this up
Set myself on fire to keep us warm
Swear I lose my mind watching you ignore
All the little things, the little things mean so much more

[Bridge]
Yeah, they mean so much more
All the little things, all the little things, all the little things
Yeah, they mean so much more
All the little things, all the little things
Mmh...

[Chorus]
I make big things out of little things
And I watch you keep missing them
You don't get why it's killing me, every time you mess it up
Set myself on fire to keep us warm
Swear I lose my mind watching you ignore
All the little things, the little things mean so much more

[Outro]
Yeah, they mean so much more