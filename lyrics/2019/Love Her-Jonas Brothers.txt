[Verse 1: Nick Jonas]
Drive me crazy, make me mental
No other buttons she can push
One second she's Miss Sentimental
Then she’s afraid she's said too much

[Pre-Chorus: Nick Jonas]
Opposites attract and we're the livin’ proof of this
But I keep on comin' back like a magnet

[Chorus: Nick Jonas]
'Cause when you love her
No matter the fight you know she's always right
And that's alright
And they say love can hurt
But seein' her smile can get you every time
Yeah, every time
Because you love her, la-la-lala-lala
Love her, yeah

[Verse 2: Joe Jonas]
I put my selfish ways in boxes
And shipped 'em back to where they came
Will never let it get close to bein' toxic
And I promise I’ll never walk away

[Pre-Chorus: Joe Jonas]
Opposites attract and we’re the livin' proof of this
But I keep on comin’ back like a magnet

[Chorus: Joe Jonas]
'Cause when you love her
No matter the fight you know she's always right
And that's alright
And they say love can hurt
But seein’ her smile can get you every time
Yeah, every time
Because you love her, la-la-lala-lala
Love her, yeah

[Bridge: Nick Jonas]
Gotta learn to let the small things go
And know it's always far from perfect
And I know that we can get emotional
But the hardest parts are always worth it

[Chorus: Joe Jonas & 
Nick Jonas
]
'Cause when you love her
No matter the fight you know she's always right
And that's alright
And they say love can hurt
But seein' her smile will get you every time
Yeah, every time
Because you love her, la-la-lala-lala
Love her, yeah

[Outro: Joe Jonas & 
Nick Jonas
]
Because you love her, la-la-lala-lala
Love her, yeah
Because you
Because you love her