[Chorus]
What I wish for
Is just a little time with you
You have everything the world can offer
Do you really want me too?

[Verse 1]
They say December is a time of giving
Have you even got the time for breathing?
You've been running in a wheel the whole year
Did you even see the seasons changing?

[Pre-Chorus]
And now the snow's falling
Let's go outside, it's gone in the morning
Now the snow's falling
Yeah, the night's calling
Let's go outside, it's gone in the morning
Now the snow's falling

[Chorus]
What I wish for
Is just a little time with you
You have everything the world can offer
Do you really want me too? (Do you really want me too?)
(Do you really want me?)
What I wish for
Is just a chance to be with you
You have everything the world can offer
Do you really want me too? (Do you really want me too?)
(Do you really want me?)

[Verse 2]
Busy, eighty minutes in an hour
Doesn't feel like getting smarter
We've been driving only on will power
Dreams are long and days are shorter

[Pre-Chorus]
And now the snow's falling
Let's go outside, it's gone in the morning
Now the snow's falling
Yeah, the night's calling
Let's go outside, it's gone in the morning
Now the snow's falling

[Chorus]
What I wish for
Is just a little time with you
You have everything the world can offer
Do you really want me too? (Do you really want me too?)
(Do you really want me?)
What I wish for
Is just a chance to be with you
You have everything the world can offer
Do you really want me too? (Will you make my wish come true?)
Do you really want me too? (Will you make my wish come true?)
Do you really want me too? (Will you make my wish come true?)
Do you really want me too? (Will you make my wish come true?)
Do you really want me?

[Outro]
You have everything the world can offer
Do you really want me too?