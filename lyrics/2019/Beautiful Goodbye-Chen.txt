[Verse 1]
Let’s talk about
All those things that we couldn’t say
When the breeze flows
Gently around us
Let’s sit face to face
And talk about our breakup

[Verse 2]
When
The winter passes and the flowers bloom
We said that
We’d be alright
But no matter how hard I try
I can't ignore
Your dying feelings

[Chorus]
When April fades away
Let’s walk away
As if nothing’s wrong
So that
Our last goodbye
Will be beautiful
Just smile a little longer until then
Smile

[Verse 3]
If you turn back time
To when we first met
Don’t stand
There underneath the streetlight
Don’t smile
And don’t push your hair back like that
So that
I can just pass you by

[Chorus]
When April fades away
Let’s walk away
As if nothing’s wrong
So that
Our last goodbye
Will be beautiful
Just smile a little longer until then

[Bridge]
Even if I comfort you
And ask why you’ve changed
You drift away
So meet someone
That’s better than me
I hope you can smile with him

[Chorus]
As you drift away
You fade away
Because we loved each other
Let’s say
Our last goodbyes
I pray that before this time ends
You can find happiness
Let’s not forget our love

[Outro]
You and me
Us
Together