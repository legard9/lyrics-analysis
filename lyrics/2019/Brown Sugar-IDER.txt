[Verse 1]
'Member when you made me feel less?
I'd be feeling kind of anxious every time I'd undress, but I
I don't really worry 'bout it now
I don't really wanna worry about it now that I
Feel you feeding me, keeping it right
Now you love me better than ever
And I'm filling you with light, so I
I don't really worry 'bout it now
I don't really wanna have to worry 'bout it now

[Pre-Chorus]
Ayy, ayy, ayy-ayy
Ayy-ayy-ayy-ayy
Ayy-ayy-ayy-ayy
Ayy, ayy, ayy-ayy
Ayy-ayy-ayy-ayy
Ayy-ayy-ayy-ayy

[Chorus]
I put myself right at the top of your plate
I knew I'd be something you'd wanna taste
Don't need to indicate a play if I could feel it, like I feel it
My baby lately gave me real peace
Put your love on me, I'll take you on my team
Don't need to complicate it, say it how I mean it, how I mean it

[Verse 2]
I can't believe what you meant to me now
We've been feeling so connected, wanna say it out loud
And I wanna leave brown sugar in my mouth
Do we really need to know what we're about?
Yeah, we're running around on the sky side of the clouds
Gotta chase me a little bit, baiting me, wanna fetch me down here
I'm so deep I'm swimming in your soul
Flash back, year '03, if only I could see me now

[Pre-Chorus]
Ayy, ayy, ayy-ayy
Ayy-ayy-ayy-ayy
Ayy-ayy-ayy-ayy
Ayy, ayy, ayy-ayy
Ayy-ayy-ayy-ayy
Ayy-ayy-ayy-ayy

[Bridge]
Hold me right up, watch me light up
Love the sight of us when you and I laugh
Sugar on my tongue, you put the air in my lungs
You make my blood sweet in the morning with my buttons undone
Take me higher, love me wider
Sweet saliva and I just wanna lie
I'm not shy, yeah, I don't need to try, yeah
In your fire and I think that I could die

[Chorus]
Ayy, ayy, ayy-ayy
I think that I could die
Ayy, ayy, ayy-ayy
Ayy-ayy-ayy-ayy
Yeah, I think that I could die
Ayy, ayy, ayy-ayy
Yeah, I think that I could die
Ayy, ayy, ayy-ayy
Ayy-ayy-ayy-ayy
Ayy, ayy, ayy-ayy

[Outro]
Hold me right up, watch me light up
Ayy, ayy, ayy-ayy
Ayy-ayy-ayy-ayy
Sugar on my tongue, you put the air in my lungs
Ayy-ayy-ayy-ayy
In the morning with my buttons undone
Ayy-ayy-ayy-ayy
Ayy-ayy-ayy-ayy
Take me higher, love me wider, love me wider
Ayy-ayy-ayy-ayy
I'm not shy, I don't need to try, I'm
Ayy-ayy-ayy-ayy
In your fire and I think that I could die