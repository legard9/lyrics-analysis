[Verse 1]
God, I'm on my knees again
God, I'm begging please again
I need You
Oh, I need You
Walking down these desert roads
Water for my thirsty soul
I need You
Oh, I need You

[Chorus]
Your forgiveness
Is like sweet, sweet honey on my lips
Like the sound of a symphony to my ears
Like holy water on my skin, 
hey!

[Verse 2]
Dead man walking, slave to sin
I wanna know about being born again
I need You
Oh, God, I need You
So, take me to the riverside
Take me under, baptize
I need You
Oh, God, I need You, oh-oh

[Chorus]
Your forgiveness
Is like sweet, sweet honey on my lips
Like the sound of a symphony to my ears
Like holy water on my skin
On my skin

[Bridge]
I don't wanna abuse Your grace
God, I need it every day
It's the only thing that ever really
Makes me wanna change
I don't wanna abuse Your grace
God, I need it every day
It's the only thing that ever really
Makes me wanna change
I don't wanna abuse Your grace
God, I need it every day
It's the only thing that ever really
Makes me wanna change, oh-oh-oh
I don't wanna abuse Your grace
God, I need it every day
It's the only thing that ever really
Makes me wanna change

[Chorus]
Your forgiveness
Is like sweet, sweet honey on my lips
Like the sound of a symphony to my ears
It's like holy water
Your forgiveness
Oh, is like sweet, sweet honey on my lips
Like the sound of a symphony to my ears
Oh, it's like holy water on my skin
Yeah, it's like holy water on my skin
Oh, it's like holy water