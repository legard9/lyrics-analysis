[Intro]
Always remember who you are

[Verse 1]
If you wanna be the girl in the picture
Well, you gotta meet her, hello!
Open your eyes, it's time you realize
Now is the time to shine
Baby, you're [?] and everybody knows
You got it and it goes from your soul
Sugar, you're a star even with your flaws
You're unstoppable

[Chorus]
Can't nobody bring you down when you're hot
All you've got to do is love yourself
And if you just believe that you're really all you need
You won't need nobody else
Oh, love yourself, love yourself, love yourself

[Verse 2]
From sea to shining sea, we all can come together
Make the world much better, uh-huh
Get up on your feet, we're dancing in the street
Celebrating liberty
Yeah, we've come this far, we've got so far to go
Hold on, baby, don't let go
If you feel alone, just ease into this song
Love yourself, you can't go wrong

[Chorus]
Can't nobody bring you down when you're hot
All you've got to do is love yourself
And if you just believe that you're really all you need
You won't need nobody else
Oh, love yourself, love yourself, love yourself

[Bridge]
Let me hear you say
Mirror, mirror on the wall
Confident and unstoppable
Self-love is the solution
Be a part of this revolution
Let me hear you say "revolution"
The category is revolution
Come on, bring it, bring it, revolution!
Come on, revolution, revolution, revolution
We got a revolution

[Chorus]
Can't nobody bring you down when you're hot
All you've got to do is love yourself
And if you just believe that you're the one you need
You won't need nobody else
Oh, love yourself, love yourself, love yourself
Oh, oh, oh, oh, oh, love yourself
It's easy, just love yourself

[?] hey, what are you waiting for?

[?]
Yeah, yeah, oh, oh

[Outro]
Always remember who you are
Don't let anyone get in your way
We need old-fashioned love in the world today