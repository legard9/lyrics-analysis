Nine years ago (early in my career as a middle school Language Arts teacher) I finally figured out what to do with all the novellas my students were asking me to read in my spare time. This was not a writing assignment…they were writing books on their own and I didn’t have time to read and comment on all of them. It was too overwhelming on top of the regular writing work I had assigned them, but I didn’t want to ignore their artistic endeavors. The work I did read was so imaginative! So I went online. Surely, there was a contest or a website where kids could submit their long works of fiction for others to read. That’s when Eureka happened. I found this little website devoted to a program called NaNoWriMo, or National Novel Writing Month. Too perfect!

2005 was the first year of the Young Writers Program (YWP) for an online challenge that was just starting to gain nationwide and worldwide notice. Teachers created accounts for students and then kids could join online forums to share ideas, give advice, name characters…anything to help them complete the challenge of writing a novel in one month. I think there were about a dozen classrooms in this first version of the YWP, including a few from Australia.

Now, every November, I organize NaNoWriMo events for students in my middle school. It’s a tradition that kids look forward to every year. What’s great about NaNoWriMo is that it’s led me towards several new passions:

•	developing my own creative writing as I teach my students to do the same
•	discovering new ways to engage learners through online communities
•	pursuing my Master’s in ILT

Without this discovery, I wouldn’t be the teacher I am today. Of course, there were several challenges (and still are) in taking time for a month of novel writing, but the effect it has had on my students and on my own philosophy of teaching is insurmountable!