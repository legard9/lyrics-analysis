[Verse 1]
I'm about to lose the allusion my heart
Pictures that I thought that could faint
Colours I’ve just come along getting blurred
Like it ain't never gonna be the same
And I know that I've messed up with the chances I got
All I do is try to make it right
All I do is try to make it right
Guess I’ll do this all my life

[Chorus]
Nothing ever seems to be easy
All I do is stand in the rain
I don't really know how to get there
Just tell me 'bout the rules of the game
Everybody says that I'm dreaming
But I'm just tryna cope with the pain
I don't really care for the rumours
Just tell me 'bout the rules of the game

[Post-Chorus]
Wanna play (Wanna play), wanna play (Play)
But I don't know ’bout the rules of the game
Wanna play (Wanna play), wanna play (Play)
But I don’t know 'bout the rules of the game

[Verse 2]
Images of colour giving peace to my heart
Taking all the doubts from my mind
Catch your body echo of the words I’ve said—"Please"
To let me keep up with the time
And I hope there'll be any other chances I get
And I hope I can take them with the mood that I have
Anything can make me get in close, but instead
Everything is pushing me away

[Chorus]
Nothing ever seems to be easy
All I do is stand in the rain
I don't really know how to get there
Just tell me 'bout the rules of the game
Everybody says that I’m dreaming
But I'm just tryna cope with the pain
I don't really care for the rumours
Just tell me 'bout the rules of the game

[Post-Chorus]
Wanna play (Wanna play), wanna play (Play)
But I don't know 'bout the rules of the game
Wanna play (Wanna play), wanna play (Play)
But I don't know 'bout the rules, don't know 'bout the rules of the game

[Instrumental Break]

[Chorus]
Nothing ever seems to be easy
All I do is stand in the rain
I don't really know how to get there
Just tell me 'bout the rules of the game
Everybody says that I'm dreaming
But I'm just tryna cope with the pain
I don't really care for the rumours
Just tell me 'bout the rules of the game

[Post-Chorus]
Wanna play (Wanna play), wanna play (Play)
But I don't know 'bout the rules of the game
Wanna play (Wanna play), wanna play (Play)
But I don't know 'bout the rules, don't know 'bout the rules of the game
But I don't know 'bout the rules, don't know 'bout the rules of the game