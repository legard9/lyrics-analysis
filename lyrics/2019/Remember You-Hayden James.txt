[Intro]
Ooh
Ooh

[Verse 1]
And it's true
I will remember you
I will remember you
Every time, oh
I see the light of the mornin'
With the shades half drawn and
I will remember you, oh
Every time (Ooh)

[Chorus]
I will remember you
I will remember you
I will remember
I will remember you
I will remember

[Verse 2]
I will remember you
And the way you used to lie
Rest your head upon my shoulders
Every time
I will remember you (I will remember you)
And the way you used to lie (Ooh)
And rest your head upon my shoulders
And every time

[Pre-Chorus]
And it's true
I will remember you
I will remember you
Every time
I see the light of the mornin'
With the shades half drawn and
I will remember you
Every time (Ooh)

[Chorus]
I will remember you
I will remember you
I will remember
I will remember you
I will remember
I will remember you
I will remember you

[Outro]
I will remember you
And the way you used to lie
Rest your head upon my shoulders
Every time
I will remember you (I will remember you)
And the way you used to lie (Ooh)
Rest your head upon my shoulders
Every time (Every time)
I will remember you