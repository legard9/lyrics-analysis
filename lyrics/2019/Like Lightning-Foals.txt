[Chorus]
It's about that time when
I gotta throw lightning
I could've been something
Without you now
It's about that time when
I gotta do something
Gotta be like lightning
Be somebody new now

[Post-Chorus]
Like lightning

[Verse 1]
I know my way back
I've seen that sky collapse
I've heard that thunder clap
I've seen that lightning crack
I know my way home
I'll mend those broken bones
I've seen that lightning crack

[Pre-Chorus]
Don't just think about yourself (Get some help)
Go get some help (They're coming for you)
They're coming for you
Don't just think about yourself (You need to get help)
Just get some help (They're coming for you)
They're coming for you

[Chorus]
It's about that time when
I gotta throw lightning
I could've been something
Without you now
It's about that time when
I gotta do something
Gotta be like lightning
Be somebody new now

[Post-Chorus]
Lightning
They're coming for you now

[Bridge]
Under a setting sun
I will not be undone
My day has just begun
Come here, break me off some
Under a setting sun
I will not be undone
My day has just begun

[Pre-Chorus]
Don't just think about yourself (Get some help)
Go get some help (They're coming for you)
They're coming for you (Be somebody new)
Don't just think about yourself (You need to get help)
Just get some help (They're coming for you)
They're coming for you (Be somebody new)
Out of the blue
You should think about yourself
You should get some help (They're coming for you)
They're coming for you (Be somebody new)
Be somebody new
Need to think about yourself
You should get some help
Be somebody new

[Chorus]
It's about that time when
I gotta throw lightning
I could've been something
Without you now
It's about that time when
I gotta do something
Gotta be like lightning
Be somebody new now

[Post-Chorus]
Like lightning
Like lightning