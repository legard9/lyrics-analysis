[Verse 1]
Landmines all over this town
I'm living in a haunted house
’Cause everything's reminding me of you
Hurts bad seeing you out
Knowing that you're happy now
You’re laughing like the way we used to do

[Pre-Chorus]
I feel it rising in me, uh
I feel the tide pulling deep
I never knew I could be so
Mad at the one that I love, no
Oh, oh

[Chorus]
I do bad things when I'm jealous
I do bad things, I can't help it, I can't help it
It's what you're doing to me, ruining me
Turning me inside out
It's what you're doing to me, ruining me
Turning me upside down
Yeah, I do bad things when I’m jealous
And I’m jealous a lot

[Verse 2]
I see the devil in me
Saying what I wanna hear
Telling me it's time to interfere

[Pre-Chorus]
I feel it rising in me
I feel the tide pulling deep
I never knew I could be so
Mad at the one that I love, no
Oh, oh

[Chorus]
I do bad things when I’m jealous (And I'm jealous)
I do bad things, I can't help it, I can't help it
It’s what you're doing to me, ruining me
Turning me inside out
It's what you're doing to me, ruining me
Turning me upside down
Yeah, I do bad things when I'm jealous
And I'm jealous a lot

[Post-Chorus]
I can't help it (A lot), I get jealous (A lot)
I can't help it (A lot), I get jealous (A lot)
I can't help it (A lot), I get jealous (A lot)
I can't help it (A lot), I get jealous

[Bridge]
I used to be so sweet
Now I see a change coming over me
You're breaking my heart
I knew from the start that you'd be the end of me

[Chorus]
I do bad things when I'm jealous (And I'm jealous)
I do bad things, I can't help it, I can't help it
It's what you're doing to me, ruining me
Turning me inside out
It's what you're doing to me, ruining me
Turning me upside down

[Outro]
I do bad things when I'm jealous (And I'm jealous)
(I get jealous)
I do bad, bad things, I can't help it (I can't help it)
(I get jealous)
It's what you're doing to me, ruining me
Turning me inside out
It's what you're doing to me, ruining me
Turning me upside down
Yeah, I do bad things when I'm jealous
And I'm jealous a lot