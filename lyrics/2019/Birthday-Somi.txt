[Intro]
Mm, mm, mm, mm, mm
Mm, mm, mm, mm, mm
Mm, mm, mm, mm, mm

[Verse 1]
Hey, boy, how you doin'?
뻔한 인사는 안할래
딱 잘라서 말할게
하루 하루 지루해
내 말 이해 못하면
하는 척이라도 해
나를 원하는 너 보단
내가 원하는 걸 원해

[Verse 2]
I got all the boys comin' ‘round to my yard
I'm shakin’ that thing like a pola-polaroid
I’ll give you that, ooh
You give me that too
It’s always a surprise when I step up in the room
It’s like

[Pre-Chorus]
이제 내 멋대로 매일매일
절대 없을거야 지루할 틈
예전에 날 찾지마 baby
새로운 나로 다시 태어날 테니까

[Chorus]
(So okay, okay)
이기적이라지만 어떡해 (Hey)
(So, okay, okay)
I’ma shake it, shake it, shake it like it was my birthday
(Ayy, ayy, da-la-la-la-la-la-la)
I’ma shake it, shake it, shake it like it was my birthday

[Post-Chorus]
Oops, you’re not invited
Yeah, you’re not invited
Oops, you’re not invited
오늘은 내 맘대로 할래
Everyday is my birthday, yeah

[Verse 3]
It goes bang bang
어때 love shot
터뜨려 fireworks, 심장에 팡팡
마주치면 다 감탄해 like, uh
그럼 난 반응해 당연해 like, duh

[Verse 4]
Well alright, alright, alright, alright
난 후 하고 불께, can you blow my mind?
더도 말고 덜도 말고 딱 오늘처럼
내 맘 흔들어줘, woah

[Pre-Chorus]
이제 내 멋대로 매일매일
절대 없을 거야 지루할 틈
예전에 날 찾지마 baby
네가 알던 난 여기 없으니까

[Chorus]
(So, okay, okay)
이기적이라지만 어떡해 (Hey)
(So, okay, okay)
I’ma shake it, shake it, shake it like it was my birthday
(Ayy, ayy, da-la-la-la-la-la-la)
I’ma shake it, shake it, shake it like it was my birthday

[Post-Chorus]
Oops, you’re not invited
Yeah, you’re not invited
Oops, you’re not invited
오늘은 내 맘대로 할래
Everyday is my birthday

[Bridge]
나의 365 and 24/7
일 월 화 수 목 금 토
It’s my birthday (Yeah)
It’s my birthday (Yeah)
나의 매순간 시간 분 초
새로 태어나는 기분이야 it’s my birthday
내가 잘 태어나서 잘난 걸 어떡해 (Da-la-la-la-la-la-la)
내가 잘 태어나서 잘난 걸 어떡해

[Post-Chorus]
Oops, you’re not invited
Yeah, you’re not invited
Oops, you’re not invited
오늘은 내 맘대로 할래
Everyday is my birthday
Yeah