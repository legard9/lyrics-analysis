Well, we'd all head to Waco, for the birth of our Lord
My folks and my brother in an '82 Ford
We'd pull in the driveway filled up with cars
Old aunts and old uncles, Lord I see stars
And then we'd run to the kitchen, you know kids and their games
Play fetch with old Buster, call each other names
Aunt Betty'd be singing while supper was cooking
We'd unwrap the gifts when no one was lookin'

Let's all gather 'round, Grandpa say the blessing
Aunt Jane, she fell asleep, Mary Kay burned the dressing
But we got all of our friends and family here
And I'm grateful for Christmas this year

Well, this year, we're in Houston, let's all get together
Man, I almost saw snow, can you believe this weather?
Who's gonna be here? Uncle Frank can't make it
Since Grandpa died, I don't know if Nana can take it
Well, this present's a sweater and the pie don't taste right
And Dad, and the TV, are startin' to fight
I wish I had a drink or maybe a dozen
Lord, what I'd give for one good lookin' cousin

So let's all gather 'round, Dad you say the blessing
Aunt Jane, she fell asleep, and Mary Kay forgot the dressing
But we got all of our friends and family here
And I'm grateful for Christmas this year

Hey Mom, how you doing? Yeah, I miss him too
Nah, the Christmas lights don't make your hair look blue
The cousins ain't coming and Jon's overseas
And I guess my wife loves her folks more than me
But the ladies from the church said, they might stop by
I brought you this picture, ahh, mama don't cry
Let's play cards and watch the news channel
I love you too, and thanks for the flannel

So let's all gather 'round, I guess I'll say the blessing
Aunt Jane, she fell asleep, and I never cared for dressing
But we got all of our friends and family here
And I'm grateful for Christmas this year
Oh, I'm grateful for Christmas this year