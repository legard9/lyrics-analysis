[Intro]
Terrified
T-Terrified

[Verse 1]
I don't mind being alone
I'm a giant inside of these walls
No, I never met one worth the fall
So I'm not fucking with any at all

[Pre-Chorus]
'Cause on the outside, I'm smiling like I've never been stung
But on the inside, I'm screaming at the top of my lungs
You know I like to play it off, but don't pretend that I'm dumb
That's why I run, that's why I run-run-run-run-run

[Chorus]
I'm not afraid of love, I'm terrified
It never feels like how they advertise
It's got big teeth, it's got an appetite
I'm not afraid of love, not afraid of love
T-t-t-t-t-t-t-terrified
T-t-t-t-t-t-t-terrified
T-t-t-t-t-t-t-terrified
I'm not afraid of love, I'm terrified

[Verse 2]
I should like being alone
'Cause if I love you, I'm letting you go
No, I'll never get sick of the fall
So addicted to losing it all

[Pre-Chorus]
'Cause on the outside, I'm smiling like I've never been stung
But on the inside, I'm screaming at the top of my lungs
You know I like to play it off, but don't pretend that I'm dumb
That's why I run, that's why I run-run-run-run-run

[Chorus]
I'm not afraid of love, I'm terrified
It never feels like how they advertise
It's got big teeth, I know I've seen it bite
I'm not afraid of love, not afraid of love
T-t-t-t-t-t-t-terrified
T-t-t-t-t-t-t-terrified
T-t-t-t-t-t-t-terrified
I'm not afraid of love, I'm terrified

[Bridge]
I'm not afraid, I'm kinda scared
If it's locked away, can't hurt me here (Hey)
I'm not afraid, no need to hide
I'm running, running, running, running 'til I die

[Chorus]
T-t-t-t-t-t-t-terrified
T-t-t-t-t-t-t-terrified
T-t-t-t-t-t-t-terrified
I'm not afraid of love, I'm terrified