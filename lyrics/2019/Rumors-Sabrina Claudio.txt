[Verse 1: Sabrina Claudio]
I heard (I heard)
That you want to be closer to me
I heard (I heard)
That you said you've seen me in your dreams

[Pre-Chorus: Sabrina Claudio]
He said, she said
You like the way I smile
He said, she said
You like my confidence and style
He said, she said
You wanna hold my hand and more
And more

[Chorus: Sabrina Claudio]
All these rumors spreading around
And I kinda like the way they sound
All these rumors 'bout you and me
How can we make this a reality? Oh

[Verse 2: ZAYN & 
Sabrina Claudio
]
He said she said your body's a temple
And I think I'd pray just for the sight of you
Deep thinkin', let me see inside of you (
Inside of you
)
I been sinkin', let me just confide in you

[Pre-Chorus: Sabrina Claudio & 
ZAYN
]
He said, she said
You like the way I smile
He said, she said
You like my confidence and style (
I like her confidence and style
)
He said, she said
You wanna hold my hand and more (
Hold her hand and more
)
And more

[Chorus: Sabrina Claudio & ZAYN]
All these rumors spreading around
And I kinda like the way they sound
All these rumors 'bout you and me
How can we make this a reality? Oh

[Post-Chorus: Sabrina Claudio]
He said, she said
Mmm
He said, she said
She said

[Bridge: Sabrina Claudio & ZAYN]
I like what I'm hearing, so my attention, you got tonight
If you're half as good as you say you are, let's stop wastin' time
Know we can keep talkin' through everybody that's in this room
Or we can come face-to-face and find out if it's true

[Chorus: Sabrina Claudio & ZAYN, 
Sabrina Claudio
]
All these rumors spreading around (
Spreading around
)
And I kinda like the way they sound (
They way they sound
)
All these rumors 'bout you and me (
Oh, you and me
)
How can we make this a reality? Oh

[Outro: Sabrina Claudio 
with ZAYN
]
He said, she said
He said, she said, yeah
He said, she said