[Verse 1]
You came home crying from school today
Walked past me, trying to hide the tears there on your face
It's hard for me to explain
How humans find the pleasure of causing people pain

[Pre-Chorus]
But sometimes, there's more to the story
And we don't know what's going on in
That home, behind closed doors
Maybe there is something wrong
So be brave, my little one
I know you can be strong
'Cause you are just like your mum

[Chorus]
Say my name
When you're hurting, darling
I'll take the demons away
But you know there will come a day
When I won't be there, but
I know that you'll be okay
'Cause you are my blood
You're my blood

[Verse 2]
Oh, I was 22 years old
My mother told me that you learn more as you go
But I've spent many nights alone
Do I need more for me to make this house a home?

'Cause sometimes there's more to the story
And they don't know what's going on
My life behind closed doors
Maybe there is nothing wrong
And I will keep holding on
I know I can be strong
'Cause I am just like my mum

[Bridge]
And she told me, "Say my name
When you're hurting, darling
I'll take the demons away
But you know there will come a day
When I won't be there, but
I know that you'll be okay
'Cause you are my blood"

[Breakdown]
(My blood, my blood, my blood)
You're my blood
(My blood, my blood, my blood)
You're my blood

[Bridge]
So, say my name
When you're hurting, darling
I'll take the demons away
But you know there will come a day
When I won't be there, but
I know that you'll be okay
'Cause you are my blood

[Chorus 2]
(My blood) my blood, you're my...
(My blood) yeah, you're my blood, you're my blood
(My blood) ooh-ooh (My blood)
(My blood) you're my blood