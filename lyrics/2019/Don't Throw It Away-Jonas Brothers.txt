[Verse 1: Nick Jonas]
Take pictures out of all the frames
Pack up your love with all your things
See if it helps, give it a week
I bet no one else gets you like me

[Pre-Chorus: Nick Jonas]
'Cause I know you think you’re better off without me now
Sayin' all you need is space
But baby, we can work this out

[Chorus: Nick Jonas & Joe Jonas]
But don't throw it away this time, just take a little time to think
Don’t throw it away, it's fine, just don't forget to think of me
Don't throw it away, your mind is messin' with your head again
Instead of walkin' away we should give it a break 'til we know what to say
Don't throw it away

[Verse 2: Nick Jonas]
Don’t save the words ’til it's too late
I know it hurts and that’s okay
If it's too much to open up, then give it time
I see it in your eyes so let's sleep on it tonight

[Pre-Chorus: Nick Jonas]
'Cause I know you think you’re better off without me now
Sayin' all you need is space
But baby, we can work this out

[Chorus: Nick Jonas & Joe Jonas]
But don't throw it away this time, just take a little time to think
Don't throw it away, it's fine, just don't forget to think of me
Don't throw it away, your mind is messin' with your head again
Instead of walkin' away we should give it a break 'til we know what to say
Don't throw it away

[Bridge: Joe Jonas & Nick Jonas]
Take your time-time, take your time, don't throw it away
Take your time-time, don't throw it away, yeah
Take your time-time, take your time, don't throw it away, yeah
Take your time-time, don't throw it away

[Chorus: Nick Jonas & Joe Jonas]
But don't throw it away this time, just take a little time to think
Don't throw it away, it's fine, just don't forget to think of me
Don't throw it away, your mind is messin' with your head again
Instead of walkin' away we should give it a break 'til we know what to say
Don't throw it away this time, just take a little time to think
Don't throw it away, it's fine, just don't forget to think of me
Don't throw it away, your mind is messin' with your head again
Instead of walkin' away we should give it a break 'til we know what to say
Don't throw it away