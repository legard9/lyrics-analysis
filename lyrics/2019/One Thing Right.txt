The interior of the hut as in Act I.
Nan lies on the bench, and is covered with a coat. Mítritch is sitting on the oven smoking.
MÍTRITCH.
Dear me! How they've made the place smell! Drat 'em! They've been spilling the fine stuff. Even tobacco don't get rid of the smell! It keeps tickling one's nose so. Oh Lord! But it's bedtime, I guess. 

[Approaches the lamp to put it out]
.
NAN

[jumps up, and remains sitting up]
 Daddy dear,

[7]
 don't put it out!
MÍTRITCH.
Not put it out? Why?
NAN.
Didn't you hear them making a row in the yard? 

[Listens]
 D'you hear, there in the barn again now?
MÍTRITCH.
What's that to you? I guess no one's asked you to mind! Lie down and sleep! And I'll turn down the light. 

[Turns down lamp]
.
NAN.
Daddy darling! Don't put it right out; leave a little bit if only as big as a mouse's eye, else it's so frightening!
MÍTRITCH

[laughs]
 All right, all right. 

[Sits down by her]
 What's there to be afraid of?
NAN.
How can one help being frightened, daddy! Sister did go on so! She was beating her head against the box! 

[Whispers]
 You know, I know … a little baby is going to be born.… It's already born, I think.…
MÍTRITCH.
Eh, what a little busybody it is! May the frogs kick her! Must needs know everything. Lie down and sleep! 

[Nan lies down]
 That's right! 

[Tucks her up]
 That's right! There now, if you know too much you'll grow old too soon.
NAN.
And you are going to lie on the oven?
MÍTRITCH.
Well, of course! What a little silly you are, now I come to look at you! Must needs know everything. 

[Tucks her up again, then stands up to go]
 There now, lie still and sleep! 

[Goes up to the oven]
.
NAN.
It gave just one cry, and now there's nothing to be heard.
MÍTRITCH.
Oh Lord! Gracious Nicholas! What is it you can't hear?
NAN.
The baby.
MÍTRITCH.
There is none, that's why you can't hear it.
NAN.
But I heard it! Blest if I didn't hear it! Such a thin voice!
MÍTRITCH.
Heard indeed! Much you heard! Well, if you know,—why then it was just such a little girl as you that the bogey popped into his bag and made off with.
NAN.
What bogey?
MÍTRITCH.
Why, just his very self! 

[Climbs up on to the oven]
 The oven is beautifully warm to-night. Quite a treat! Oh Lord! Gracious Nicholas!
NAN.
Daddy! are you going to sleep?
MÍTRITCH.
What else? Do you think I'm going to sing songs?
Silence.
NAN.
Daddy! Daddy, I say! They are digging! they're digging—don't you hear? Blest if they're not, they're digging!
MÍTRITCH.
What are you dreaming about? Digging! Digging in the night! Who's digging? The cow's rubbing herself, that's all. Digging indeed! Go to sleep I tell you, else I'll just put out the light!
NAN.
Daddy darling, don't put it out! I won't … truly, truly, I won't. It's so frightful!
MÍTRITCH.
Frightful? Don't be afraid and then it won't be frightful. Look at her, she's afraid, and then says it's frightful. How can it help being frightful if you are afraid? Eh, what a stupid little girl!
Silence.
The cricket chirps.
NAN

[whispers]
 Daddy! I say, daddy! Are you asleep?
MÍTRITCH.
Now then, what d'you want?
NAN.
What's the bogey like?
MÍTRITCH.
Why, like this! When he finds such a one as you, who won't sleep, he comes with a sack and pops the girl into it, then in he gets himself, head and all, lifts her dress, and gives her a fine whipping!
NAN. What with?
MÍTRITCH.
He takes a birch-broom with him.
NAN.
But he can't see there—inside the sack!
MÍTRITCH.
He'll see, no fear!
NAN.
But I'll bite him.
MÍTRITCH.
No, friend, him you can't bite!
NAN.
Daddy, there's some one coming! Who is it? Oh gracious goodness! Who can it be?
MÍTRITCH.
Well, if some one's coming, let them come! What's the matter with you? I suppose it's your mother!
Enter Anísya.
ANÍSYA.
Nan! 

[Nan pretends to be asleep]
 Mítritch!
MÍTRITCH.
What?
ANÍSYA.
What's the lamp burning for? We are going to sleep in the summer-hut.
MÍTRITCH.
Why, you see I've only just got straight. I'll put the light out all right.
ANÍSYA

[rummages in her box and grumbles]
 When a thing's wanted one never can find it!
MÍTRITCH.
Why, what is it you are looking for?
ANÍSYA.
I'm looking for a cross. Suppose it were to die unbaptized! It would be a sin, you know!
MÍTRITCH.
Of course it would! Everything in due order.… Have you found it?
ANÍSYA.
Yes, I've found it. 

[Exit]
.
MÍTRITCH.
That's right, else I'd have lent her mine. Oh Lord!
NAN

[jumps up trembling]
 Oh, oh, daddy! Don't go to sleep; for goodness' sake, don't! It's so frightful!
MÍTRITCH.
What's frightful?
NAN.
It will die—the little baby will! At Aunt Irene's the old woman also baptized the baby, and it died!
MÍTRITCH.
If it dies, they'll bury it!
NAN.
But maybe it wouldn't have died, only old Granny Matryóna's there! Didn't I hear what granny was saying? I heard her! Blest if I didn't!
MÍTRITCH.
What did you hear? Go to sleep, I tell you. Cover yourself up, head and all, and let's have an end of it!
NAN.
If it lived, I'd nurse it!
MÍTRITCH

[roars]
 Oh Lord!
NAN.
Where will they put it?
MÍTRITCH.
In the right place! It's no business of yours! Go to sleep I tell you, else mother will come; she'll give it you! 

[Silence]
.
NAN.
Daddy! Eh, daddy! That girl, you know, you were telling about—they didn't kill her?
MÍTRITCH.
That girl? Oh yes. That girl turned out all right!
NAN.
How was it? You were saying you found her?
MÍTRITCH.
Well, we just found her!
NAN.
But where did you find her? Do tell!
MÍTRITCH.
Why, in their own house; that's where! We came to a village, the soldiers began hunting about in the house, when suddenly there's that same little girl lying on the floor, flat on her stomach. We were going to give her a knock on the head, but all at once I felt that sorry, that I took her up in my arms; but no, she wouldn't let me! Made herself so heavy, quite a hundredweight, and caught hold where she could with her hands, so that one couldn't get them off! Well, so I began stroking her head. It was so bristly,—just like a hedgehog! So I stroked and stroked, and she quieted down at last. I soaked a bit of rusk and gave it her. She understood that, and began nibbling. What were we to do with her? We took her; took her, and began feeding and feeding her, and she got so used to us that we took her with us on the march, and so she went about with us. Ah, she was a fine girl!
NAN.
Yes, and not baptized?
MÍTRITCH.
Who can tell! They used to say, not altogether. 'Cos why, those people weren't our own.
NAN.
Germans?
MÍTRITCH.
What an idea! Germans! Not Germans, but Asiatics. They are just the same as Jews, but still not Jews. Polish, yet Asiatics. Curls … or, Curdlys is their name.… I've forgotten what it is!

[8]
 We called the girl Sáshka. She was a fine girl, Sáshka was! There now, I've forgotten everything I used to know! But that girl—the deuce take her—seems to be before my eyes now! Out of all my time of service, I remember how they flogged me, and I remember that girl. That's all I remember! She'd hang round one's neck, and one 'ud carry her so. That was a girl,—if you wanted a better you'd not find one! We gave her away afterwards. The captain's wife took her to bring up as her daughter. So—she was all right! How sorry the soldiers were to let her go!
NAN.
There now, daddy, and I remember when father was dying,—you were not living with us then. Well, he called Nikíta and says, “Forgive me, Nikíta!” he says, and begins to cry. 

[Sighs]
 That also felt very sad!
MÍTRITCH.
Yes; there now, so it is …
NAN.
Daddy! Daddy, I say! There they are again, making a noise in the cellar! Oh gracious heavens! Oh dear! Oh dear! Oh, daddy! They'll do something to it! They'll make away with it, and it's so little! Oh, oh! 

[Covers up her head and cries]
.
MÍTRITCH

[listening]
 Really they're up to some villainy, blow them to shivers! Oh, these women are vile creatures! One can't say much for men either; but women!… They are like wild beasts, and stick at nothing!
NAN

[rising]
 Daddy; I say, daddy!
MÍTRITCH.
Well, what now?
NAN.
The other day a traveller stayed the night; he said that when an infant died its soul goes up straight to heaven. Is that true?
MÍTRITCH.
Who can tell. I suppose so. Well?
NAN.
Oh, it would be best if I died too. 

[Whimpers]
.
MÍTRITCH.
Then you'd be off the list!
NAN.
Up to ten one's an infant, and maybe one's soul would go to God. Else one's sure to go to the bad!
MÍTRITCH.
And how to the bad? How should the likes of you not go to the bad? Who teaches you? What do you see? What do you hear? Only vileness! I, though I've not been taught much, still know a thing or two. I'm not quite like a peasant woman. A peasant woman, what is she? Just mud! There are many millions of the likes of you in Russia, and all as blind as moles—knowing nothing! All sorts of spells: how to stop the cattle-plague with a plough, and how to cure children by putting them under the perches in the hen-house! That's what they know!
NAN.
Yes, mother also did that!
MÍTRITCH.
Yes,—there it is,—just so! So many millions of girls and women, and all like beasts in a forest! As she grows up, so she dies! Never sees anything; never hears anything. A peasant,—he may learn something at the pub, or maybe in prison, or in the army,—as I did. But a woman? Let alone about God, she doesn't even know rightly what Friday it is! Friday! Friday! But ask her what's Friday? She don't know! They're like blind puppies, creeping about and poking their noses into the dung-heap.… All they know are their silly songs. Ho, ho, ho, ho! But what they mean by ho-ho, they don't know themselves!
NAN.
But I, daddy, I do know half the Lord's Prayer!
MÍTRITCH.
A lot you know! But what can one expect of you? Who teaches you? Only a tipsy peasant—with the strap perhaps! That's all the teaching you get! I don't know who'll have to answer for you. For a recruit, the drill-sergeant or the corporal has to answer; but for the likes of you there's no one responsible! Just as the cattle that have no herdsman are the most mischievous, so with you women—you are the stupidest class! The most foolish class is yours!
NAN. Then what's one to do?
MÍTRITCH.
That's what one has to do.… You just cover up your head and sleep! Oh Lord!
Silence. The cricket chirps.
NAN

[jumps up]
 Daddy! Some one's screaming awfully! Blest if some one isn't screaming! Daddy darling, it's coming here!
MÍTRITCH.
Cover up your head, I tell you!
Enter Nikíta, followed by Matryóna.
NIKÍTA.
What have they done with me? What have they done with me?
MATRYÓNA.
Have a drop, honey; have a drop of drink! What's the matter? 

[Fetches the spirits and sets the bottle before him]
.
NIKÍTA.
Give it here! Perhaps the drink will help me!
MATRYÓNA.
Mind! They're not asleep! Here you are, have a drop!
NIKÍTA.
What does it all mean? Why did you plan it? You might have taken it somewhere!
MATRYÓNA

[whispers]
 Sit still a bit and drink a little more, or have a smoke. It will ease your thoughts!
NIKÍTA.
My own mother! My turn seems to have come! How it began to whimper, and how the little bones crunched … krr … I'm not a man now!
MATRYÓNA.
Eh, now, what's the use of talking so silly! Of course it does seem fearsome at night, but wait till the daylight comes, and a day or two passes, and you'll forget to think of it! 

[Goes up to Nikíta and puts her hand on his shoulder]
.
NIKÍTA.
Go away from me! What have you done with me?
MATRYÓNA.
Come, come, sonnie! Now really, what's the matter with you? 

[Takes his hand]
.
NIKÍTA.
Go away from me! I'll kill you! It's all one to me now! I'll kill you!
MATRYÓNA.
Oh, oh, how frightened he's got! You should go and have a sleep now!
NIKÍTA.
I have nowhere to go; I'm lost!
MATRYÓNA

[shaking her head]
 Oh, oh, I'd better go and tidy things up. He'll sit and rest a bit, and it will pass! 

[Exit]
.
Nikíta
sits with his face in his hands. Mítritch and Nan seem stunned.
NIKÍTA.
It's whining! It's whining! It is really—there, there, quite plain! She'll bury it, really she will! 

[Runs to the door]
 Mother, don't bury it, it's alive.…
Enter Matryóna.
MATRYÓNA

[whispers]
 Now then, what is it? Heaven help you! Why won't you get to rest? How can it be alive? All its bones are crushed!
NIKÍTA.
Give me more drink! 

[Drinks]
.
MATRYÓNA.
Now go, sonnie. You'll fall asleep now all right.
NIKÍTA

[stands listening]
 Still alive … there … it's whining! Don't you hear?… There!
MATRYÓNA

[whispers]
 No! I tell you!
NIKÍTA.
Mother! My own mother! I've ruined my life! What have you done with me? Where am I to go? 

[Runs out of the hut; Matryóna follows him]
.
NAN.
Daddy dear, darling, they've smothered it!
MÍTRITCH

[angrily]
 Go to sleep, I tell you! Oh dear, may the frogs kick you! I'll give it to you with the broom! Go to sleep, I tell you!
NAN.
Daddy, my treasure! Something is catching hold of my shoulders, something is catching hold with its paws! Daddy dear … really, really … I must go! Daddy, darling! let me get up on the oven with you! Let me, for Heaven's sake! Catching hold … catching hold! Oh! 

[Runs to the stove]
.
MÍTRITCH.
See how they've frightened the girl.… What vile creatures they are! May the frogs kick them! Well then, climb up.
NAN

[climbs on oven]
 But don't you go away!
MÍTRITCH.
Where should I go to? Climb up, climb up! Oh Lord! Gracious Nicholas! Holy Mother!… How they have frighted the girl. 

[Covers her up]
 There's a little fool—really a little fool! How they've frighted her; really, they are vile creatures! The deuce take 'em!
Footnotes:

[7] Nan calls Mítritch “daddy” merely as a term of endearment.

[8] Probably Kurds.