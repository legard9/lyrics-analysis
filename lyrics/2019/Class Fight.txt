K-12 (The Film)
-[Woman]
Wake up. You’re gonna be late.
(The alarm clock rings. It then levitates and flies away.
Crybaby wakes up.)
-[Crybaby VO]
Wait. What was my dream again?
I remember feeling trapped in rising heat, and 
there were plastic or cardboard cutouts
 all around me, and my gums' roots were pushing out my teeth, and 
the voice of a thousand angels said to me, “It’s temporary.”
Maybe that should be my mantra as I step into what will inevitably be the worst years of my life. 
Ah yes, off to a world in which girls are to only wear pink dresses and boys blue pants. Well, I dyed my uniform and embroidered flowers on the sleeves.
(She pauses, and laughs.)
Speaking of, I should probably change into my uniform right now. Nah, that shit’s itchy. It can wait.
(Harp playing)
-[Crybaby VO]
Oh yeah. And in my dream, you know, when the air below me was sucking my teeth out of their sockets, I noticed that my front two teeth stayed and the gap between them just kept getting larger and larger. How’s that even possible? I know right?
I wish my mom wasn’t passed out right now and could at least drive me to the bus stop.
(She is seen in the kitchen, trying to pour a bowl of cereal. Instead a tarantula comes out of the box.)
Phillipe? What are you doing in there? Don’t scare me like that!
What was that angel’s name again? Was it Lilly or Layla? No, I think it was Lilith.
(Birds singing)
--
(Kids talking and laughing on the school bus)
-[Crybaby]
(sigh)
 I feel so sick.
-[Angelita]
I can’t believe we have to do this.
-[Crybaby]
Do you think we’ll make any friends?
-[Angelita]
I hope so, but at least we have each other.
-[Crybaby]
That’s true.
(Girls Laughing)
-[Boy]
Okay, okay. Stop. Stop.
(Kids laughing)
(One of the boys in the back throws a paper airplane at Crybaby. Inside, the paper reads "gap-toothed bitch.)
-[Crybaby]
Seriously? What does it say?
-[Angelita, to the boy in the back]
Real mature!
-[Blue boy 1 and blue boy 2]
Aw, sad little baby! Go on!
-[Kelly]
Does the bunny wabbit need some tissues?
-[Maya]
I bet she has some in her bra!
-[Angelita]
It’s okay, just ignore them. Just plastic assholes.
-[Blue boy 1 & blue boy 2]
Crybaby, wah-wah!
-[Kids]
Wah-wah! Crybaby!
-[Kids]
Wah-wah! Wah-wah, wah-wah!
-[Crybaby VO]
This is K-12

[“WHEELS ON THE BUS”]
(Horn honking)
(Kids on the bus screaming as the bus is thrown off the road and into the body of water below.)
(Somber music as the kids realize they are underwater.)
-[Angelita, to Crybaby]
C’mon! Get up!  What are you doing?
(Scared mumbling)
-[Kids]
Ahhhhh...
(String music and wind as the bus is suddenly lifted out of the water.)
-[Crybaby]
We should hang here more often.
-[Angelita]
It’s so much better up here.
-[Boy]
This isn’t real... this isn’t real...
-[Crybaby]
Should we land this thing?
-[Angelita]
Mmm, nah.
-[Boy]
I’m gonna jump out this window!
(Harp music)
(Screams)
(Wind)
(Bell chiming)
-[Crybaby]
I don’t know why people are so scared of death.
-[Angelita]
I agree. It’s just another part of life.
-[Crybaby]
You start in the womb and you end in the tomb.
-[Angelita]
Wait a second. Where is everybody?
-[Crybaby]
I don’t know if it’s because it’s cold in here, but I’m getting kind of a creepy chill down my back...
-[Together]
I love it!
(Startling music as something ghost-like passes from behind.)
-[Angelita]
What even was that?
-[Crybaby]
Scary
-[Crybaby]
(sighs.)
What's the room number again?
-[Angelita]
222.
-[Crybaby]
221...
-[Angelita]
223...
-[Together]
...222.
(Door knob turning and opening)
-[Ms. Daphne]
Class, would you like to inform Crybaby and Angelita of what rule they did not follow today?
-[Whole class]
When the bell rings, you must be in your assigned seat.
-[Ms. Daphne]
What are ya deaf? Sit your asses down!
-[Crybaby and Angelita, quietly]
Sorry, Ms. Daphne.
(The loudspeaker notification beeps.)
-[Loudspeaker]
All must stand for the pledge of allegiance.
All rise!
-[Ms. Daphne]
Henry! On your feet!
-[Loudspeaker and students]
I pledge allegiance to the flag of the United States of America and to the Republic for which it stands...
-[Ms. Daphne]
I’m being generous with you, boy
-[Loudspeaker and students]
...One nation under God...
-[Ms. Daphne]
Last chance to stand...
-[Loudspeaker and students]
...indivisible...
-[Ms. Daphne]
...and show some respect!
-[Loudspeaker and students]
...with liberty and justice for all.
-[Henry]
You hear that? Liberty and justice for all? That’s bullshit.
(Ms. Daphne presses a button. It beeps, calling security to take him away.)
(The door opens, violently.)
-[Man]
Grab the boy.
-[Man]
Get this one.
-[Man]
Quiet!
-[Henry]
Get off me! I didn’t do anything!
-[Man]
Take him to the holding!
-[Man]
Put him in the back. This is what you get!
(All three exit, the door slams shut.)

[“CLASS FIGHT”]
(Time skips to naptime. There are star lights strung across the room and beds are laid out for naptime. Crybaby and Angelita discover a secret passage under the bed.)
(Girl students enter, laughing. The door closes.)
(Crybaby flicks on a light. She is now underneath the beds.)
-[Crybaby]
Angelita?
-[Angelita]
I’m over here. Can you hear me?
-[Crybaby]
Yea, where are you?
-[Angelita]
I’m trying to follow your voice.
-[Angelita]
There you are.
-[Crybaby]
Finally. How do they expect us to sleep in here?
-[Angelita]
I don’t know. I am kind of sleepy though...
-[Crybaby]
Well, wake up. I got that thrown at my head.
-[Angelita]
Who drew this?
-[Crybaby]
That girl Kelly. I think she’s tryin’ to like rip my head off or something
-[Angelita]
Well, what are you gonna do? You gotta defend yourself!
-[Crybaby]
I can’t defend myself. I don’t know how to fight, I just, I don’t even know how to do that— Ow!
-[Angelita]
Hold it together, girl. We are capable of doing whatever we want. Remember?
-[Crybaby]
No. I can’t, it’s cheating.
-[Angelita]
Do you really think we’d have these abilities if we weren’t supposed to use them?
-[Crybaby]
I don’t know. I just feel like I have an unfair advantage.
-[Angelita]
Well, I don’t know. Why don’t you call your mom and ask her what she thinks you should do?
-[Crybaby]
My mom?
-[Angelita]
Mmhmm
-[Crybaby]
No, it’s okay. She’s probably sleeping right now and honestly, I don’t even think she would pick up the ph—
(Angelita makes a telephone appear, and hands the receiver to Crybaby.)
-[Crybaby]
Fine. She’s not going to pick up.
-[Angelita]
We’ll see.
(The phone rings. We see Crybaby's mother, passed out and on the floor beside a glass of what appears to be alcohol. Crybaby sighs.)

[Continuation of “CLASS FIGHT”]
(The scene switches to the outdoor playground. Crybaby and Angelita are talking to Brandon in the sandbox.)
-[Kelly]
Did you see what she was wearing? Like it was embarrassing for everyone! And, like, I told her. I was trying to be helpful. I gave her advice. I was like, "that’s ugly". And she took it, like, personally. She was getting all mad about it, I was like, "I’m helping you!"
-[Girl]
Hey, Kelly?
-[Kelly]
Stop. Don’t interrupt me.
-[Girl]
Bu---
-[Kelly]
Shhh. Still talking
-[Girl]
Yea, I—
-[Kelly]
I just don’t understand why you wouldn’t take my advice. I give really good advice to people and it’s just a
--- are you even listening to me?
(The other girls murmur and point to Crybaby, Angelita, and Brandon. Kelly turns around and charges Crybaby. A fight bell rings.)

[Continuation of “CLASS FIGHT”]
-[Angelita]
Watch out!
(Kelly slaps Crybaby. Hard. Enough to make Crybaby fall back onto the ground.)
-[Crybaby]
Ugh!
(The kids yell while Kelly slaps Crybaby repeatedly.)
-[Kids chant]
Fight! Fight! Fight!
(Kelly draws a knife, and cuts Crybaby's arm, making it bleed.)
-[Kids chant]
Fight! Fight! Fight!
(Kelly laughing)
(Crybaby screaming)
(Ms. Daphne is quickly made aware of the situation and blows a whistle. The kids stop.)
-[Ms. Daphne] What the fuck!

[Continuation of “CLASS FIGHT”]
(Crybaby's powers become out of control, and she starts to choke Kelly with her hair.)
-[Kelly]
What are you doing? Put me down you freak!
(The scene ends. Ms. Daphne pulls both Crybaby and Kelly by the ears to the Principal's office.)
-[Ms. Daphne]
I would have thought that young ladies like you would know better.
-[Kelly]
Quit it! My mom spent good money getting these ears done
-[Ms. Daphne]
You have really blown it this time. Sit down. I mean it.
-[Crybaby VO]
I shouldn't have lost control like that. She’s not a bad person, she’s just projecting her insecurities onto me.
-[Kelly VO]
She didn’t even leave a mark on me. I broke skin.
(Clock ticks as the scene moves to the Principal's office, where the Principal is seen talking to a staff member.)
-[Principal]
You said you dress as a woman now?
-[Ms. Harper]
Yes, I’m transitioning. Look—
-[Principal]
Transitioning? No, no. I can’t permit you to influence the children with this ridiculous behaviour. And so what from now on you want us to address you as—
-[Ms. Harper]
Ms. Harper. Correct. The kids really connect with me. I love my job and I’d really like to stay, but I need to be my most authentic self. I am a woman.
-[Ms. Daphne]
A woman? You hear this?
-[Principal]
Mr. Harper, you’re fired.
- [Kid eating glue]
Mmmm...
(Crybaby looks around the room, and spots another kid popping pills.)
-[Crybaby]
What are those?
-[Thomas]
He forces us to take these pills. Each colour is a different dosage. I’m on pink. It’s the lightest dose.
-[Crybaby]
Why is he forcing medication on everyone?
-[Thomas]
To control us. So we can’t leave.
-[Kelly, to Kid eating glue]
That’s fucking disgusting!
(The clock ticks. Crybaby sighs, and then makes a phone appear and dials the Principal's office's number. The phone rings.)

[“THE PRINCIPAL”]
-[Ms. Daphne]
(Crying)
 Idiots! Save him!
(The scene shifts to a fourth grade class. Crybaby walks in late from the principal's office and closes the door. All eyes are on her, and she sits down. The teacher shows that it is show and tell day, and then opens a compartment to reveal a doll on strings. The students bang on their desks.)

[“SHOW AND TELL”]
(The students clap and cheer, then circle around the doll. Then the doll breaks, spilling its "guts" everywhere.)
-[Students]
Ew! Gross!
(Vacuum running)
(Cough)
(Wind)
(Sneeze)

[“NURSE’S OFFICE”]
-[Crybaby and Angelita]
Wait!
-[Angelita]
Why are you leaving us here?
-[Crybaby]
We need to come with you!
-[Lilith]
Continue on and live your truth. Do not hold fear in your heart.
-[Crybaby]
Please. We don’t want to die here.
-[Lilith]
You’ve had many bodies before this and you’ll continue to have more. You’re immortal.
-[Angelita]
But we’re exhausted!
-[Crybaby]
We don’t wanna feel this anymore!
-[Lilith]
We must feel the physical pain of this world because the only way that we can truly learn is through experience.
-[Crybaby]
Please!
(The scene changes.)
-[Angelita]
Oh no. I’m gonna be late for class.
-[Crybaby]
Who cares at this point?
-[Angelita]
True. I’m so exhausted.
-[Crybaby]
Same.
(The girls encounter a room that seems to have music playing in the room.)
-[Crybaby]
Do you hear that?
-[Angelita]
I do. I think it’s coming from over here.
-[Crybaby]
Should we check it out?
-[Angelita]
Yeah. Okay, count of three.
-[Together]
One, two, three!
(They open the door to reveal ghosts, dancing to the music playing. The ghosts don't notice them.)
-[Angelita]
Woah!
-[Crybaby]
Achoo!
(The music stops. The ghosts all turn to look at Crybaby and Angelita.)
-[Angelita]
H-hello. You all look ravishing.
-[Crybaby]
Bewitching even!
(The ghosts charge towards them. Angelita closes the door just in time, and the ghosts claw at the door.)
-[Crybaby]
We should get to class... bye.
-[Angelita]
Okay, see you later. Bye.
-[Mr. Cornwell]
Tragedy and comedy. Two sides of the same dramatic coin and we are going to be exploring both in this play. Even the latecomers.
(He nods to Crybaby as she walks in late.)
Alright, everyone, please turn to your scripts. Look to page two and find the roles to which you’ve been assigned.
-[Crybaby]
Right, so about that. Um, is it possible to get assigned a different role? Maybe one that’s not so domestic? Like a film director? Or the President of the United States!
-[Mr. Cornwell]
A harlot perhaps?
(He and the students laugh.)
-[Boy]
President? What a joke. Your kind are too soft and too sensitive to handle a man’s job.
-[Crybaby]
Having a larger capacity to feel and express emotion are one of the many qualities that make us superior to your kind.

[“DRAMA CLUB”]
-[Boy]
Ahhhhhh!!!
-[Crybaby]
You’re being brainwashed!
-[Mr. Cornwell]
What is this? What is she doing? Go get her. Go on!
-[Crybaby]
Can’t you see what’s happening? Open your fucking eyes! Get off of me! Get off! Wake up!

[Continuation of “DRAMA CLUB”]
(Shovel scooping sounds)
(Crybaby and Angelita bury the Principal's body)
(Birds chirping)
(Panting)
(Sigh)
-[Crybaby]
Wanna go play tennis?
-[Angelita]
Ooh, yeah.
-[Celeste]
Finish first draft of ethics essay. A treatise on co-ed education. Boys are the problem.
-[Crybaby]
Celeste, wait up!
-[Celeste]
Oh, hey! Umm... Did you guys find a place to dump the principal?
-[Crybaby]
Yeah, just did it. Done deal.
-[Celeste]
Perfect.
-[Angelita]
Did you guys know there are actually two holes down there?
-[Celeste]
You didn’t know you had a butthole?
-[Angelita]
No, I mean in your vagina. There are two separate holes!
-[Crybaby]
Are you sure?
-[Angelita]
Mmhmm.
-[Crybaby]
I think I need to see a doctor.
-[Celeste]
Hey guys....?
-[Angelita]
Yeah, there’s your urethra and vagina.
-[Celeste]
You guys?
-[Crybaby]
Oh okay, got it. So, one hole that you can pee out of and the other one that you can—
-[Celeste]
Why does no one listen to me? Guys! The ball is floating away!
-[Crybaby]
Oh shit, the ball! I feel like we’re on the wrong way.
-[Angelita]
I swear, I just saw it.
(Lockers opening and closing)

[“STRAWBERRY SHORTCAKE"]
-[Lilith]
What’s wrong, my dear?
-[Crybaby]
I just don’t want to do this anymore. I don’t want to feel this pain and I don’t want to be here on Earth. Please just take me with you. (sigh) I just want to be up there
-[Lilith]
The greatest power you will hold is that of acceptance. Any storm you face will transform itself into a crystalline rainbow. In divine timing.
-[Angelita]
Hey.
-[Crybaby]
Hmm?
-[Celeste]
You there?
-[Crybaby]
Yeah
- [Angelita]
(painful sigh)
-[Crybaby]
You ok?
-[Angelita]
Ahh, I feel so sick. Like this is the worst stomach pain I’ve ever experienced.
-[Crybaby and Celeste]
Ohhhh...
(The three of them enter the bathroom.)
-[Angelita]
Ugh, why right now!?
-[Celeste]
Well... I mean it had to happen sometime.
-[Crybaby]
It says there’s one tampon left. Do either of you have any change?
-[Celeste and Angelita]
No...
(Crybaby sighs. She then opens the machine by herself.)
-[Angelita]
Yes!
-[Crybaby]
It’s fucking empty. Whatever. Who knows what kind of crazy shit’s in them anyways.
-[Celeste]
I mean yes, toxic shock is real, but still tampons should be free.
-[Angelita]
What do I do now?
-[Crybaby and Celeste]
Toilet paper.
-[Angelita]
Mmmph.
-[Celeste]
You good in there?
-[Angelita]
Yeah
-[Celeste]
You sure?
-[Angelita]
Yeah, I’m almost done
-[Celeste]
Need me to get you another roll of toilet paper?
(Crybaby laughs)
-[Angelita]
That’d be great, yeah.
-[Crybaby]
I think I’m going to be late for class.
-[Angelita]
Empathize with me a little!
-[Crybaby]
I’m sorry.
-[Angelita]
Okay. Ummm...
-[Crybaby]
Cute sweater. Where’d you get that from?
-[Angelita]
Just whipped a little something up. What? I wasn’t going to walk around looking like I sat in raspberries all day.
(The door opens and one of the nurses enters. She looks at everyone, and then heads into a stall.)
-[Nurse]
Get back to class! You little bitch
-[Angelita]
You guys. I don’t know how long I can go using this toilet paper method. It’s so annoying. It keeps moving around down there
-[Crybaby]
Well, good thing you won’t have to deal with it much longer.
(Crybaby reveals that she stole a tampon from the nurse that walked in.)
-[Angelita]
Thank the Goddess!
-[Nurse]
Shit. Are you serious? Fuck!
(The scene changes to the lunch room.)
-[Boy]
Uhh... You go ahead. Let me know how it is
-[Girl]
Ok, I’ll cut you

[“LUNCHBOX FRIENDS”]
-[Crybaby]
Hey.
-[Celeste]
Thank God you’re back! I thought you were busy becoming a plastic carbon copy.
-[Crybaby]
This is Magnolia. Say hi.
-[Magnolia]
Nice to meet you ladies.
-[Celeste]
Welcome!
-[Angelita]
Hi...
(They glance at another girl sitting with the popular girls, but she appears to have the same powers as the rest of them.)
-[Crybaby]
Holy shit!
-[Angelita]
Is she—
-[Celeste]
One of us?
-[Crybaby]
But, how do we even go up to her? She’s sitting with Kelly!
-[Magnolia]
Food fight, anyone?
-[Crybaby]
Hmmmm.
-[Angelita]
Hmm
-[Celeste]
I like this girl.
(All of them laugh.)
-[Angelita]
Hmm?
-[Crybaby]
What? So I just throw it?
-[Angelita]
Pretty sure that’s how food fights go down.
-[Celeste]
Yeah, take it, take it, take it.
-[Crybaby]
Ok, fine. Ok. Three... Two... One... I don’t know.
-[Magnolia]
Just do it already!
-[Crybaby]
Okay!
(Crybaby throws a plate of food, and it hits Maya.)
-[Maya]
Oww!
-[Blue boy]
Food fight!
-[Kelly]
You’re fucking kidding me! You’re all heathens!
-[Magnolia]
I’m gonna get in on this!
-[Celeste]
Uh uh, not with this hair. I just had wash day.
-[Crybaby]
Okay, so should we go up to her?
-[Kelly]
Get out of my way!
-[Angelita]
She’s leaving with Kelly. Let’s go after them
-[Celeste]
Yea, let’s just start a parade. No. Crybaby, I think you need to do this alone
-[Crybaby]
Okay. Wish me luck.
-[Magnolia]
Alright, good luck bitch.
-[Celeste]
So barbaric.
-[Magnolia]
Are you sure I can’t?
-[Celeste]
If you don’t put that floor spaghetti down...
-[Magnolia]
Man.

[“ORANGE JUICE”]
-[Crybaby]
Wanna know something I’ve learned about bodies? They don’t define us. We aren’t our bodies. They’re just temporary. I know it seems impossible, but try not to expect shallow people to love you. They don’t even have the capacity to understand how amazing you are. And we all have to learn to love ourselves without the approval of others
-[Fleur]
I know. It’s just extremely overwhelming when everyone around you is making you feel like you aren’t good enough. As if you’re not deserving the same kind of love if you’re different
-[Crybaby]
Everyone is deserving of love. Everyone
(They both go back to the cafeteria, where the food fight is still going down.)
-[Angelita]
Crybaby, over here
-[Kid yelling]
Banana! Eat it! Nanananana!
-[Crybaby]
This is Fleur
-[Kelly]
Lucy! What did we talk about? Don’t touch me!
-[Lucy]
I know, I’m sorry. Isn’t that Crybaby with Fleur over there?
-[Kelly]
Ugh!
-[Celeste]
It’s crazy over there!
-[Crybaby]
It’s chaotic!
(The door opens to reveal the principal's son, Leo. Everyone freezes.)
-[Kid]
It’s Leo... It’s the principal’s son
-[Leo]
Who started this?
-[Kelly]
It was that bitch, Crybaby. Behind the counter!

[“DETENTION”]
-[Teacher]
Shut up!
-[Celeste]
Okay, guys, I think I have a pretty good plan to get us out of this shithole. Angelita, I need you to keep everyone in the gym. Keep them in one place and try to keep them quiet—
-[Crybaby VO, reading a piece of paper in her locker]
I caught a sparkle of light in your eyes that day. I bathed in it, was transported to heaven’s gates by it, but I fall at my feet. My words shall only be composed. I can’t speak. My inner voice calling my outer self weak admiring from afar secretly—
-[Celeste]
Crybaby? Earth to Crybaby. Are you in?
-[Crybaby]
Yeah. Sorry!
-[Celeste]
Your job is to manipulate Satan himself into letting you into his office so we can hijack that loudspeaker and get everyone out of here.
-[Magnolia]
What’s that?
-[Crybaby]
I think it’s a love letter.
-[Magnolia]
A love letter from who?
-[Crybaby]
I don’t know. It doesn’t say.
-[Celeste]
Okay guys, back to the plan. We already got rid of the principal, so that just leaves Leo.
-[Celeste via monitor]
We walk into his office, make a run for it and then—
-[Angelita]
Set this place on fire?
-[Celeste]
Exactly
-[Fleur]
I don’t know about that guys
-[Crybaby]
I agree. Fire’s too cliché
-[Celeste]
I mean yeah, but at this point, what other options do we have?

[“TEACHER’S PET”]
(The scene shifts to the courtyard, where Crybaby and Angelita are smoking a joint.)
-[Angelita]
I hope we don’t get caught
-[Crybaby]
We should be fine.
...Do you ever wonder who we were to each other in a past life?
-[Angelita]
Oh, for sure. We’ve probably had hundreds of lives together.
-[Crybaby]
You could’ve been my mom.
-[Angelita]
Yeah, or we could’ve been...
-[Together]
Conjoined twins!
-[Crybaby]
And our parents sold us to a fucking freak show!
-[Angelita]
That would explain a lot.
-[Crybaby]
That would. It’s crazy how many medical benefits mary jane has.
-[Angelita]
I know, and no one gives her any credit.
-[Crybaby]
We don’t deserve her.
-[Angelita]
Wow.
-[Crybaby]
Speaking of magical plants.

[“HIGH SCHOOL SWEETHEARTS”]
(The scene changes, and focuses on Ben. He is sitting in the hallway, around the corner from Crybaby.)
-[Ben]
You got this. Super easy. Don’t stress. Uh, fuck. You wanna go to the dance? Do you wanna go to the dance with me?
-[Leo]
You alright? Hey.
-[Crybaby]
Hi.
-[Leo]
You okay?
-[Crybaby]
Mmmhmm.
-[Leo]
You sure?
-[Crybaby]
Yea, what’s up?
-[Leo]
Um, I uh, was looking for you because I wanted to ask you. I would like you to go to the dance with me.
-[Crybaby]
Really?
-[Leo]
Yeah.
-[Crybaby]
Why?
-[Leo]
I don’t know if you got my note but...
-[Crybaby VO]
Wait, he wrote the letter? With that poetic touch, I just assumed it must have been written by a girl. Damn, well you know what they say. Keep your friends close and your enemies closer
-[Crybaby]
I’d love to go with you!
-[Leo]
Good.
-[Crybaby]
Cool.
-[Leo]
Okay. Great.

[Thunder]

[Party music* Tonight you belong to me]

[Kids talking and dancing]
-[Kelly] Brandon why are you touching her? Why are you touching her? Lucy how could you do this to me, he’s mine. You’re just gonna stand there and watch this happen? I made you. You guys are nothing without me. Ughh
-[Magnolia] Ladies, this place looks sort of beautiful don’t you think?
-[Celeste] It’ll make some beautiful ashes
-[Fleur] I’m really nervous. Should we really be doing this?
-[Angelita] It’ll be fine. Tonight’s the night
-[Celeste] Just stick to the plan. I made it. It’s meticulous. You’ll be okay

[Background music continues]

[Dripping liquid]
-[Celeste] Ladies. You know what you’ve gotta do
-[Fleur] Wait. Why is she here?
-[Blue boy] Angelita, do you wanna dance with me now?
-[Angelita] Don’t you wanna fuck off now? Ugh
-[Celeste] There is no way in hell you had enough time to get rid of Leo
-[Fleur] Yea, shouldn’t you be up there?
-[Magnolia] Where?
-[Celeste] The loudspeaker
-[Magnolia] Yea. Why aren’t you up there?
-[Crybaby] Okay, so I got asked to the dance by Leo
-[All girls] What?!
-[Crybaby] I know
-[Celeste] Girl. Seriously? You let that monster manipulate you?
-[Crybaby] Well—
-[Fleur] Even I didn’t abandon ship this time, and trust me, I wanted to
-[Magnolia] I thought we were actually going to get out of this place for good
-[Crybaby] Look, I’m sorry. I am. Seriously? Look, I don’t know, I guess I was just—
-[Angelita] Being selfish
-[Crybaby] Forget it
-[Angelita] Wait!
-[Celeste] Nope. It’s fine, just let her go. It’s fine

[Door closes]

[Water running]
-[Crybaby] You fucked up the whole plan. And all for some asshole who doesn’t even like you. Real smart

[Lilith] Eh, don’t beat yourself up

[Slow party music plays]

[Background conversations]

[Person taps on microphone]

[Audio feedback] -Ow. Ugh
-[Leo over loudspeaker] Good evening and welcome to K-12’s 150th school dance
-[Celeste] This isn’t part of the plan

[Applause]
-Woo! Woo!
-[Leo over loudspeaker] Now, before I let you all get back to dancing all night, quite literally, I would just like to say just one thing. Crybaby. I know what you’ve been up to. I’ve been watching you for a long time now. And did you all think that you could just kill my father, get away with it, have your nice little dance and leave?
-[Magnolia] Yes

[Leo laughing]
-[Leo over loudspeaker] I don’t think so Seniors. Not tonight
-[Blue boy] The doors are locked
-[Leo over loudspeaker] So dance. Dance!

[“RECESS”]
-[Leo laughing] Dance!!
-[Angelita] What’s happening?
-[Fleur] What’s going on?
-[Magnolia] I mean I can’t stop
-[Celeste] We gotta do something guys. I mean I love dancing but this is kinda ridiculous
-[Angelita] We need to find Crybaby and stop this now
-[Celeste] My powers won’t start. I’ve lost control
-[Magnolia] Yea, I can’t use mine neither
-[Angelita] Where is she?
-[Fleur] My legs are killing me

[Continuation of “RECESS”]
-[Leo] Hello miss. Can I help you?
-[Crybaby as Lorelai] Hi, I’m Lorelai. I’m here to apply for the assistant position
-[Leo] Um, yeah. The assistant position-

[Continuation of “RECESS”]
-[Magnolia] Where is she?
-[Fleur] I can’t do this anymore
-[Celeste] Crybaby where are you?

[Kissing]
-[Crybaby as Lorelai] You know, I’ve always had this fantasy
-[Leo] What’s that?
-[Crybaby as Lorelai] Of getting nailed in a really confined space
-[Leo] Really?
-[Crybaby as Lorelai] Mmhmm
-[Leo] Well you are in luck. I had a dream like this once
-[Real Crybaby] Me too
-[Leo] Woah, woah, woah!

[Door locking]
-[Leo] You bitch, let me out. [Banging on the door] Hey! Hey!

[Slow dance music]

[Record scratch]

[Crowd] Woah!
-[Magnolia] Yes! Yes!
-[Crybaby over loudspeaker] Everybody, evacuate the school immediately. Go, now!
-[Angelita] She’s safe
-[Celeste] Come on, let’s go

[Kids happy and laughing]

[Students] Move, move! Get out, get out!
-[Fleur] Hey, let’s go! Come on!

[Dramatic music]

[Kids celebrating while leaving]
-[Leo] Hey! Shit
-[Crybaby] What do I do? Oh my god. Think. Think
-[Ben] Hey! Hi
-[Crybaby] Shit! You have to go downstairs
-[Ben] It’s alright, I didn’t mean to startle you. I’m here to help
-[Crybaby] Look, I appreciate that but I really have to—
-[Ben] I’m Ben. I’m the one who wrote the love letter

[Dramatic music]

[Kids celebrating being free]
-[Crybaby] Okay. How do we destroy this place? I don’t know what to do. Do you have any ideas?
-[Ben] Uh. Do you know how to blow spit bubbles?
-[Crybaby] What? What are you talking about? Why?
-[Ben] We can trap the school. We can lift the school up in a bubble
-[Crybaby] I don’t know. Okay, let’s try it

[Dramatic string sound]

[Door unlocking]

[Kids out of breath running]
-[Magnolia] Where’s Crybaby?
-[Fleur] I don’t know
-[Celeste] Have you guys seen Crybaby?
-[Angelita] No
-[Celeste] That’s— Look
-[Crybaby] Are you ready?
-[Ben] Yea. I mean no. I haven’t really done this before so I can’t—
-[Crybaby] Ben. It’s our only option
-[Ben] Sorry
-[Crybaby] We have to try this, okay?
-[Ben] Okay. You’re right. You’re right

[Dramatic music]

[Blowing bubbles]
-[Fleur] Woah
-[Celeste] Well, damn
-[Angelita] Pinch me if I’m dreaming. Ow
-[Crybaby] Holy shit, we don’t have much time until we’re too high up. We need to jump. Right now. On the count of three
-[Ben] If we jump, we’ll die
-[Crybaby] Fear of death is irrational right now Ben. Either we die in a few minutes, or we attempt to live right now. Okay?

[Dramatic string music]
-[Crybaby] Now
-[Crybaby and Ben] Ahhhhh!
-[Ben] Oh, ahh. What the fuck just happened? That’s it
-[Angelita] Il y a du sang partout
-[Celeste] What does that mean?
-[Angelita] There’s blood everywhere. I don’t really know French. It’s just something I picked up as a kid. Had a French nanny. What?
-[Crybaby] Oh my God, it’s Lilith! [Laughing]
-[Angelita] Guys come on! Are you coming?

[“FIRE DRILL”]