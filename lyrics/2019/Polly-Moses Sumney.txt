[Verse 1]
You remain in motion
Bottom of the ocean
Not yet sick of sycophants
Tellin' you their true lies
Like, "No moon is higher,"
Hollow as a hallway
Your fist fits right through me
If I split my body into two men
Would you then love me better?
Octopus myself so you weather this

[Pre-Chorus]
Sea-ea-ea polly
Sea-ea-ea polly
See-ee-ee, see me

[Chorus]
You love dancin' with me
Or you just love dancin'
Polly, polly, polly

[Verse 2]
I don't wanna live here
Sometimes don't wanna live at all
I want to be cotton candy
In the mouth of many a lover
Saccharine
 and slick technicolor
I'll dissolve
I know that won't solve this (I want to dissolve)
Evolve into rain and spit
You make me go unstitched
Oh, yeah

[Chorus]
Are you dancin' with me?
Or just merely dancin'?
Polly, polly, polly
Yeah

[Verse 3]
One, two, three, four, five, six
Am I just your Friday dick?
Cornucopia of just-in-cases
You'll never have to chase this

[Outro]
Woah, polly, oh, polly
Obviously don't think much of me
Polly, yeah, oh, polly
Polly, yeah, polly, yeah
Polly, ooh