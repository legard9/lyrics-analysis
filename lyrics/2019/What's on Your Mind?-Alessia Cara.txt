[Verse 1]
Poured out my heart last night
Wrote a song with some good lines
I spill my feelings on you sometimes
But at least you know
Had a one-way talk with your voicemail
Called out, but you weren't there and
Now you're back and it's not fair
What you're doing

[Pre-Chorus 1]
You say you thought about me all along
(Say you thought about me)
But never said a thing till I was gone (Ah)

[Chorus]
How am I supposed to know what's on your mind?
I don't have a crystal ball
I can't see through your walls
You should know better
How am I supposed to know what's on your mind?
I'm tired of cracking codes
If you want me, let me know
You should know better
Ah, better

[Verse 2]
You bottle it up like fine wine
But don't rely on the grapevine
I always fall for the shy guy
What was I expecting?
So lay it all out on the table
You felt it, but you never said so
Even Juliet and Romeo couldn't outsmart communication

[Pre-Chorus 2]
If you don't wanna talk, that's fine by me
(You don't want to talk, that's fine, fine by me)
But don't get mad when I get up and leave (Ah)

[Chorus]
How am I supposed to know what's on your mind?
I don't have a crystal ball
I can't see through your walls
You should know better
How am I supposed to know what's on your mind?
I'm tired of cracking codes
If you want me let me know
You should know better
Ah, better

[Bridge]
Speak now or forever hold your tongue
(You should know better, you should know better)
Speak now or forever hold your tongue
(You should know better, you should know better)

[Chorus]
How am I supposed to know what's on your mind?
I don't have a crystal ball (Crystal ball)
I can't see through your walls
You should know better
How am I supposed to know what's on your mind?
I'm tired of cracking codes (Yeah)
If you want me let me know
You should know better
Ah, better (No, no, no, no, no, no, eh)

[Outro]
You should know better
You should know better
Speak now or forever, yeah
You should know better
You should know better