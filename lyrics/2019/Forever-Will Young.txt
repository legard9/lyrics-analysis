[Verse 1]
Alone in this city, where have you been?
The lights, they blind me to my soul
Everyone's waking up, I am not
I'm alone in this city, city

[Pre-Chorus]
I will wait for you
Ships will sail for you
Stand on cliffs for you
Forever, forever

[Chorus]
Forever, forever
Forever, forever
Ahh, ahh, ahh, ahh
Forever, forever

[Verse 2]
People are saying things, I can't hear
How can you be home, when I still feel you so near
The world is crashing around me, without you by my side

[Pre-Chorus]
I will wait for you
Ships will sail for you
Stand on cliffs for you
Forever, forever

[Chorus]
Forever, forever
Forever, forever
Ahh, ahh, ahh, ahh
Forever, forever

[Bridge]
Running with no where to go, I am lost
I'm alone in this city, city
Oh

[Pre-Chorus]
I will wait for you
Ships will sail for you
Stand on cliffs for you
Forever, forever
Forever, forever

[Chorus]
Forever, forever
Forever, forever
Ahh, ahh, ahh, ahh
Forever, forever

[Outro]
Forever