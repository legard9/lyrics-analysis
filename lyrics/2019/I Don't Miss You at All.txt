English

[Hook: Jin, Jungkook]
Last Christmas I gave you my heart
But the very next day you gave it away
This year to save me from tears
I’ll give it to someone special

[Verse 1: Suga]
Our boss was indifferent, even three years ago
We’ve never had the dinner outing that everyone else has
Outside, it’s like a party
Us guys are bunched together as if we’re in the army
This year I’ll be either practicing or sitting alone in the corner
Nothing changes no matter how hard I try, I’m solo
But boss, we don’t want a solo victory
All we want is a dinner outing

[Verse 2: RM]
I can’t stand it anymore because I’m so lonely
It’s the middle of December and my lips are chapped
I haven’t matched up to anybody these days, I should give up
No girlfriend either because our debut is soon
Hah idols are getting married yet
Separating the male and female trainees? How does that make sense
We can’t love so how do we write love song lyrics
If it were up to me, I’d be encouraging dating

[Refrain: Jin, Jimin, V]
(They tell me to come) The company keeps telling me to practice
(This year, again) I spent my whole day practicing
(I’m crying) I’m still a trainee
I really want to debut next year

[Verse 3: RM]
Over here poop poop poop hey over there poop poop poop
This isn’t snow, it’s poop poop poop!
Look at the sky, it’s raining white poop
God really has no manners at all, none at all
Even my forever alone friend Myungsoo is going to a solo victory party
I want to rest on that day and at least hold a snowman’s hand
Our company, our boss - I don’t like any of you
I miss my mom

[Verse 4: Jungkook]
My upcoming second Christmas, I don’t even remember what I did last year
That’s how boring and uneventful that day was
My name is Jeon Jungkook, my other name is maknae
Then I should be doing aegyo, as a 9th grader should
Boss, you’re the one who is in charge of Big HIt
Manager, even the other official is at home, sleeping
(Oh my god!) I’m going to cry if I can’t play
If I can’t, I’ll kneel down in front of the company door

[Refrain: Jin, Jimin, V]
(They tell me to come) The company keeps telling me to practice
(This year, again) I spent my whole day practicing
(I’m crying) I’m still a trainee
I really want to debut next year

[Hook: Jin, Jungkook]
Last Christmas I gave you my heart
But the very next day you gave it away
This year to save me from tears
I’ll give it to someone special

[Bang Sihyuk]
Ayo, Hitman Bang introduces Hit It, the 2nd audition
Dominate your opponent with rapping, dancing, or singing~

[Verse 5: Suga]
I entered Big Hit 3 years ago through Hit It
Making it through the obstacles, history in the making
We hold the Bangtan badge
But don’t spit out rhymes and flow like our boss

[Verse 6: RM]
I’m a bumpkin from Ilsan
Making it to the nation’s top 1%
I got an unexpected call during my midterms
That was it, I became solo after picking up that call

[Hook: RM]
Last Christmas I gave you my heart
But the very next day you gave it away
This year to save me from tears
I’ll give it to someone special

Merry Christmas everyone

Hangul

[Hook: 진, 정국]
Last Christmas I gave you my heart
But the very next day you gave it away
This year to save me from tears
I’ll give it to someone special

[Verse 1: 슈가]
3년전부터 사장님은 무심하셨고
남들 다 하는 회식 한 번 한 적 없었죠
밖은 축제 분위기
우린 남자 여럿이서 군대 분위기
올해도 또 홀로 연습 아니면 방구석
구태여 뭘 해도 안 생겨요 우린 솔로
인데 우린 사장님 솔로대첩
같은 건 안 바래요 원하는 건 회식 한 번

[Verse 2: 랩몬스터]
도저히 참을 수 없어 이젠 심하게 외로워서
12월의 한가운데 벌써 입술이 텄어
요즘 맞춰본 적도 없는데 떼 버릴까
여자친구도 데뷔도 지금은 너무 머니까
하 아이돌도 결혼하는 판에
남녀연습생부동석? 이게 말이나 돼
사랑을 못하는데 어찌 사랑 가사를 써
지금이라도 연애를 권장해보겠어 나라면 더

[Refrain: 진, 지민, 뷔]
(오래요)회사에선 연습하래요
(올해도)연습으로 날을 지샜죠
(우네요)아직도 난 연습생인데
내년엔 꼭 데뷔할래

[Verse 3: 랩몬스터]
여기 똥똥똥 hey 저기 똥똥똥
이건 눈이 아니여 똥똥똥!
저것 좀 봐 하늘에서 하얀 똥이 내려와
하느님도 참 무슨 매너가 없어 매너가
내 모태솔로 친구 영수도 솔로대첩을 가는데
나도 그 날은 좀 쉬고 눈사람 손이라도 잡을래
회사도 사장님도 모두 다 미워
나도 엄마가 보고싶어

[Verse 4: 정국]
다가오는 두번째 크리스마스, 작년엔 뭘 했었는지 기억도 안 나
그 정도로 재미없었던 기념일을 보냈었던 나
내 이름은 전정국이 그리고 다른 이름은 막내
그렇다면 애교를 부려야지 중3답게
사장님 당신은 빅히트를 지키는 가장님
과장님, 차장님도 집에서 쉬신다던디
(Oh my god!) 나 못 놀면 진짜 막 울어
안 되면 회사 찾아가 문 앞에서 딱 꿇어

[Refrain: 진, 지민, 뷔]
(오래요)회사에선 연습하래요
(올해도)연습으로 날을 지샜죠
(우네요)아직도 난 연습생인데
내년엔 꼭 데뷔할래

[Hook: 진, 정국]
Last Christmas I gave you my heart
But the very next day you gave it away
This year to save me from tears
I’ll give it to someone special

[: 방시혁]
Ayo, hitman bang introduces, Hit it the 2nd audition
랩, 댄스, 노래로 상대방의 기선을 제압해~

[Verse 5: 슈가]
3년 전 힛잇을 통해 들어왔던 빅힛
우여곡절을 겪고 history in the making
우린 방탄이란 뱃지
를 달고 라임과 플로를 사장님처럼은 안 뱉지

[Verse 6: 랩몬스터]
나 원래 일산 촌놈 출신 빡빡이
전국 1% 찍고 갑자기
중간고사 때 걸려온 뜬금없는 전화
그거였어 그거 받고 솔로가 됐어 나..

[Hook: 랩몬스터]
Last Christmas I gave you my heart
But the very next day you gave it away
This year to save me from tears
I’ll give it to someone special

Merry Christmas everyone

Romanized

[Hook: Jin, Jeongguk]
Last Christmat I gave you my heart
But the very next day you gave it away
This year to save me from tears
I’ll give it to someone special

[Verse 1: Suga]
3nyeonjeonbuteo sajangnimeun musimhasyeossgo
Namdeul da haneun hoesik han beon han jeok eopseossjyo
Bakkeun chukje bunwigi
Urin namja yeoreosiseo gundae bunwigi
Olhaedo tto hollo yeonseup animyeon bangguseok
Gutaeyeo mwol haedo an saenggyeoyo urin sollo
Inde urin sajangnim sollodaecheop
Gateun geon an baraeyo wonhaneun geon hoesik han beon

[Verse 2: Rap Monster]
Dojeohi chameul su eopseo ijen simhage oerowoseo
12worui hangaunde beolsseo ipsuri teosseo
Yojeum majchwobon jeokdo eopsneunde tte beorilkka
Yeojachingudo debwido jigeumeun neomu meonikka
Ha aidoldo gyeolhonhaneun pane
Namnyeoyeonseupsaengbudongseok? ige marina dwae
Sarangeul moshaneunde eojji sarang gasareul sseo
Jigeumirado yeonaereul gwonjanghaebogesseo naramyeon deo

[Refrain: Jin, Jimin, V]
(oraeyo)hoesaeseon yeonseupharaeyo
(olhaedo)yeonseubeuro nareul jisaessjyo
(uneyo)ajikdo nan yeonseupsaenginde
Naenyeonen kkok debwihallae

[Verse 3: Rap Monster]
Yeogi ttongttongttong hey jeogi ttongttongttong
Igeon nuni aniyeo ttongttongttong!
Jeogeot jom bwa haneureseo hayan ttongi naeryeowa
Haneunimdo cham museun maeneoga eopseo maeneoga
Nae motaesollo chingu yeongsudo sollodaecheobeul ganeunde
Nado geu nareun jom swigo nunsaram sonirado jabeullae
Hoesado sajangnimdo modu da miwo
Nado eommaga bogosipeo

[Verse 4: Jeongguk]
Dagaoneun dubeonjjae keuriseumaseu, jaknyeonen mwol haesseossneunji gieokdo an na
Geu jeongdoro jaemieopseossdeon ginyeomireul bonaesseossdeon na
Nae ireumeun jeonjeonggugi geurigo dareun ireumeun maknae
Geureohdamyeon aegyoreul buryeoyaji jung3dapge
Sajangnim dangsineun bikhiteureul jikineun gajangnim
Gwajangnim, chajangnimdo jibeseo swisindadeondi
(Oh my god!) na mot nolmyeon jinjja mak ureo
An doemyeon hoesa chajaga mun apeseo ttak kkulheo

[Refrain: Jin, Jimin, V]
(oraeyo)hoesaeseon yeonseupharaeyo
(olhaedo)yeonseubeuro nareul jisaessjyo
(uneyo)ajikdo nan yeonseupsaenginde
Naenyeonen kkok debwihallae

[Hook: Jin, Jeongguk]
Last Christmat I gave you my heart
But the very next day you gave it away
This year to save me from tears
I’ll give it to someone special

[Bang Shi-hyuk]
Ayo, hitman bang introducet, Hit it the 2nd audition
Raep, daenseu, noraero sangdaebangui giseoneul jeaphae~

[Verse 5: Suga]
3nyeon jeon hisiseul tonghae deureowassdeon bikhis
Uyeogokjeoreul gyeokkgo history in the making
Urin bangtaniran baesji
Reul dalgo raimgwa peulloreul sajangnimcheoreomeun an baetji

[Verse 6: Rap Monster]
Na wonrae ilsan chonnom chulsin ppakppagi
Jeonguk 1% jjikgo gapjagi
Junggangosa ttae geollyeoon tteungeumeopsneun jeonhwa
Geugeoyeosseo geugeo batgo solloga dwaesseo na..

[Hook: Rap Monster]
Last Christmat I gave you my heart
But the very next day you gave it away
This year to save me from tears
I’ll give it to someone special

Merry Christmas everyone