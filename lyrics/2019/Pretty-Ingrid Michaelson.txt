[Verse 1]
I'm done spinning ’round and 'round
Planting my feet in the ground
I'm not afraid of the dark
I’m not afraid to get hurt
Head above the clouds
Mama, come look at me now
I'm not afraid of the world
I'm gonna fight like a girl

[Pre-Chorus]
Running around with my long hair
Tearing my dress, and I don't care
If you're looking for something beautiful

[Chorus]
I'm pretty sure that I'm all good
Walking away from you like I should
Washing it all away
I'm not just pretty
No, I’m pretty damn good

[Verse 2]
Rosy cheeks and lips
She talks, but nobody listens
That’s just the way of the world
I gotta fight like a girl

[Pre-Chorus]
Running around with her long hair
Tearing her dress, and she don't care
If you’re looking for something beautiful

[Chorus]
I'm pretty sure that I'm all good
Walking away from you like I should
Washing it all away
I'm not just pretty
No, I’m pretty damn good
Sure in my own skin
Again and again and again
I am my everything
I'm not just pretty
No, I'm pretty damn good

[Bridge]
I'm pretty
I'm pretty much a mess
But I'm pretty good
I'm gonna fight like a girl

[Chorus]
I'm pretty sure that I'm all good
Walking away from you like I should
Washing it all away
No, I'm not just pretty
No, I'm pretty goddamn good
Sure in my own skin
Again and again and again
I am my everything
No, I'm not just pretty
No, I'm pretty damn good