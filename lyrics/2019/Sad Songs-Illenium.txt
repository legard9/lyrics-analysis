[Verse 1]
Told me I shouldn't watch, got in your car
Then we were nothing
All the nights in your room, shit we got through
Left in your dust, yeah
Don't know what you got, but I thought we had it
Now I'm supposed to act like it doesn't matter
But I'm writing it down, getting it out
Here's to me hoping

[Chorus]
Someday I'll come home
And feel like no one's even gone
Play this at my shows
And know that I've been moving on
Someday I'll go to bed and I'll forget I'm lyin' in your spot
But until someday comes
I'll be writing sad songs

[Verse 2]
Wish that there was a way I could have saved
One little moment, yeah
Pull it out when I can handle the pain
And know when it's over
I was so intact on the night I met you
Now I'm making sad songs inside my bedroom
But I'm writing it down, getting it out
Here's to me hoping

[Chorus]
Someday I'll come home
And feel like no one's even gone
Play this at my shows
And know that I've been moving on
Someday I'll go to bed and I'll forget I'm lyin' in your spot
But until someday comes
I'll be writing sad songs

[Bridge]
Ooh, ooh
I'll be writing sad songs
Ooh, ooh
I'll be writing sad songs
Ooh, ooh
I'll be writing sad songs (Sad songs)
Ooh, ooh
I'll be writing sad songs

[Drop]
I'll be writing sad songs

[Chorus]
Someday I'll come home
And feel like no one's even gone
Play this at my shows
And know that I've been moving on
Someday I'll go to bed and I'll forget I'm lyin' in your spot
But until someday comes
I'll be writing sad songs