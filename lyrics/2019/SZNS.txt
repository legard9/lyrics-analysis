Emptiness is filling my soul
The world is going down
Everyone has gone
I want to know what is going on
I want to throw all the feelings away
To become senseless
And not to be afraid
My body is bleeding
And I don't feel pain
I don't need a savior
Death take me away
Lie!
Greed, lust, sloth, gluttony

Wrath, envy, pride
Lie!
I don't need a help!
I don't want to be here anymore
All hope is gone
All things are done
I become free
No pain I can feel
And every sin I've done
Is just gone...