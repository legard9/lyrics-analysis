[JANE]
If you want this choice position
Have a cheery disposition
Rosy cheeks, no warts

[MICHAEL, spoken]
That's the part I put in!

[JANE, sung]
Play games, all sorts
You must be kind, you must be witty
Very sweet and fairly pretty

[MR. BANKS, spoken]
Of all the ridiculous ideas -

[MRS. BANKS]
Oh George, please!

[JANE, sung]
Take us on outings, give us treats

[MICHAEL]
Sing songs, bring sweets

Never be cross or cruel
Never give us castor oil or gruel - eugh!

[JANE]
Love us as a son and daughter -

[MICHAEL]
And never smell of barley water

(spoken)
I put that one in too!

[JANE, sung]
If you won't scold and dominate us
We will never give you cause to hate us

We won't hide your spectacles so you can't see

[MICHAEL]
Put toads in your bed, or pepper in your tea

[JANE]
Hurry, Nanny
Many thanks

[BOTH]
Sincerely

[JANE]
Jane

[MICHAEL]
And Michael!

[BOTH]
Banks