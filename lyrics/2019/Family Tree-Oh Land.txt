[Verse 1]
I used to love you
Now I doubt it all
That's what you said
Sometimes I hurt others
Sometimes myself along the way
And I should've known better than to fall again
Heart's ran astray
Sometimes I love others
Sometimes myself along the way

[Verse 2]
I'm too young to stop taking risks
And too late to start
Sometimes I risk others
Sometimes I risk my trembling heart

[Chorus 1]
Pain free
Hope is dancing so desperately
I did expect you to shake me
Falling leaves on my family tree
Pain free
Hope is dancing so desperately
I did expect you to change me
Falling leaves on my family tree
Falling leaves on my family tree

[Verse 3]
I'll write a million songs to feel better
To understand
Sometimes I feel nothing
Sometimes it all comes washing in
And the little yellow sandals in the hallway
Split them in two
Sometimes I hurt others
Sometimes myself and sometimes you

[Chorus 2]
Hurt me
Tried to guard myself properly
But you're soaking in deeply
From the roots of my family tree
Sorry
I did expect you to hate me
Spring is coming to save me
Rain on me and my family tree
Rain on me and my family tree
Falling leaves

[Verse 4]
I throw myself like sticks in the water
Making a wave
Sometimes you swim with me
Sometimes we sink into the lake