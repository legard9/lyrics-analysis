[Intro]
So, we must ask ourselves, what is the dictionary definition of terrorism? The systematic use of terror especially as a means of coercion, but what is terror? According to the dictionary I hold in my hand, terror is violent or destructive acts, such as bombing committed by groups in order to intimidate a population, or government into granting their demands. So what's a terrorist?

[Hook]
They're calling me a terrorist
Like they don't know who the terror is
When they put it on me, I tell them this
I'm all about peace and love
They calling me a terrorist
Like they don't know who the terror is
Insulting my intelligence
Oh how these people judge

[Verse 1]
It seems like the Rag-heads and Pakis are worrying your dad
But your dad's favourite food is curry and kebab
It's funny, but it's sad
How they make your mummy hurry with her bags
Rather read the Sun than study all the facts
Tell me, what's the bigger threat to human society
BAE Systems or home-made IED's?
Remote-controlled drones killing off human lives
Or man with home-made bomb committing suicide?
I know you were terrified when you saw the towers fall
It's all terror, but some forms are more powerful
It seems nuts, how could there be such agony?
When more Israelis die from peanut allergies
It's like the definition didn't ever exist
I guess it's all just dependent who your nemesis is
Irrelevant how eloquent the rhetoric peddler is
They're telling fibs, now tell us who the real terrorist is

[Hook]
They're calling me a terrorist
Like they don't know who the terror is
When they put it on me, I tell them this
I'm all about peace and love
They calling me a terrorist
Like they don't know who the terror is
Insulting my intelligence
Oh how these people judge

[Verse 2]
Lumumba was democracy, Mosadegh was democracy
Allende was democracy; hypocrisy, it bothers me
Call you terrorists if you don't wanna be a colony
Refuse to bow down to a policy of robberies
Is terrorism my lyrics?
When more Vietnam vets kill themselves
After the war than die in it
This is very basic; one nation in the world
Has over a thousand military bases
They say it's religion, when clearly it isn't
It's not just Muslims that oppose your imperialism
Is Hugo Chavez a Muslim? Nah… I didn't think so
Is Castro a Muslim? Nah… I didn't think so
It's like the definition didn't ever exist
I guess it's all just dependent who your nemesis is
Irrelevant how eloquent the rhetoric peddler is
They're telling fibs, now tell us who the terrorist is

[Hook]
They're calling me a terrorist
Like they don't know who the terror is
When they put it on me, I tell them this
I'm all about peace and love
They calling me a terrorist
Like they don't know who the terror is
Insulting my intelligence
Oh how these people judge

[Bridge]
You think that I don’t know
But I know, I know, I know
You think that we don’t know, but we know
You think that I don’t know
But I know, I know, I know
You think that we don’t know, but we do

[Outro]
Was Building 7 terrorism?
Was nano-thermite terrorism?
Diego Garcia was terrorism
I am conscious the Contras was terrorism
Phosphorous that burns hands – that is terrorism
Irgun and Stern Gang, that was terrorism
What they did in Hiroshima was terrorism
What they did in Fallujah was terrorism
Mandela ANC – they called terrorism
Gerry Adams IRA – they called terrorism
Erik Prince Blackwater – it was terrorism
Oklahoma, McVeigh – that was terrorism
Everyday USA – that is terrorism
Everyday UK – that is terrorism, every day

Every day, every day, every day, every day, every day, every day

You think that we don't know know but we do...