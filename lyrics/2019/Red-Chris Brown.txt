[Intro]
That's one small step for man
One giant leap for mankind
Columbia, Columbia, this is Houston AOS, over
(Columbia)

[Verse 1]
Can I? Wait, can I? Should I?
Rewind, just remind me that I'm down
Knocked down
And it's your year
I should have realized, realized, from the beginnin'
You right, you right with that, baby (Huh)
Yes, I know you better
Girl, nobody else matters

[Pre-Chorus]
If you stay away from me too long, I
I might swim for it
If you keep your love from me, girl, I might
I'ma just keep going
'Round and 'round in circles
Could you stop fuckin' with my head?
'Cause I see the color purple
Don't break my heart

[Chorus]
Don't wanna see
Red, red (Don't wanna see)
Red (Oh, no, no, no)
Don't wanna see, see
Red
Eyes turnin' blood red
I don't want this, bringin' out a part of me
(Don't wanna see)

[Verse 2]
Don't wanna go back and forth, back and forth
And you know it's a waste of time, baby
To get through to you, oh-whoa
You keep arguin' always 'bout the same thing
But you don't mean it, no, no
And I can't believe that you be so cold
You can't even keep a low pro'

[Pre-Chorus]
If you stay away from me too long, I (Stay away from me)
I might swim for it
If you keep your love from me, girl, I might
I'ma just keep going (Ooh)
'Round and 'round in circles
Could you stop fuckin' with my head? (Head)
'Cause I see the color purple (Ooh)
Don't break my heart (Ooh)

[Chorus]
Don't wanna see
Red, red (Baby, I don't wanna see)
(Don't wanna see)
Red (Ooh, ooh, ooh)
Don't wanna see, see
Red (Ooh, ooh)
Eyes turnin' blood red
I don't want this, bringin' out a part of me
(Don't wanna see)

[Guitar Solo: Xeryus L. Gittens]

[Chorus]
Don't wanna see
Red (Don't wanna see)
Red (Ooh, ooh, ooh)
Don't wanna see, see
Red (Ooh, ooh)
Eyes turnin' blood red
I don't want this bringin' out a part of me (Yeah)
(Don't wanna see, see)

[Outro]
Bringin' out a part of me
Ooh
Red