[Intro]
Ladies & gentlemen (Fizzle, Cory Mo)
Introducing K. Michelle

[Verse 1]
Tonight I'm feeling sexy (Woah)
I can make you overdose, make it overflow, if you let me (Sheesh)
Like a river, my waterfalls'll breakdown a levee
You gon' get soaked, so bring a raincoat
When we finish you gon' see a rainbow
Come swim in my waters like a sailboat
You just may get lost in me like Nemo
Come sip on it, as I drip on it
You gon' have to change my name to H2O

[Pre-Chorus]
You love it when I call your name
Daddy, daddy
You love it when I make it rain
Daddy, daddy

[Chorus]
Storms will come
Make me overflow (Make me overflow)
Can you stand the rain?
When we get done
I still want some more (I still want some more)
Can you make it rain?

[Verse 2]
I can get it wetter than a thunderstorm (Hey)
Make love from the midnight 'til the early morn (Ayy-ayy-ayy)
Wakin' you up like some Folgers in your cup
That morning brew, that's what I do (Ayy)
Baby, can you swim deep? (Swim deep)
Diving in the ocean while you in the sheets (In the sheets, woah)
What stroke do you got for me?
You know I'm trying to drown you, oh

[Pre-Chorus]
You love it when I call your name
Daddy, daddy
You love it when I make it rain
Daddy, daddy

[Chorus]
Storms will come
Make me overflow (Make me overflow)
Can you stand the rain? (Yeah, yeah)
When we get done (We get done)
I still want some more (I still want some more)
Can you make it rain? (Can you make it rain? Yeah, hey)

[Bridge]
Every time you pull up, boy, it's a tidal wave
Ooh, she get wet up, we drip the night away
Boy, it ain't no rush, take your time with me
Boy, I love when you keep it right there, no, don't ever ever stop
I'm gon' ride, baby, it's you and I
Through the good and bad, I'ma be your ride or die
And when the storm comes, and the rain falls
We'll be making up, we'll be making love

[Chorus]
Storms will come
Make me overflow (Make me overflow)
(Can you stand the rain? Yeah)
Can you stand the rain? (Oh, oh, can you stand the rain?)
When we get done (Get done, yeah)
I still want some more (I still want some more, yeah)
Can you make it rain? (Make it rain, yeah)