[Intro]
Eh

[Verse 1]
Say my name
And save me once again
Just say my name
Too far gone
Is this where I belong?
Am I too far gone? (Eh)

[Pre-Chorus]
I can hear them in my head and I
I can hear them and I wanna get 'em out
I can hear them in my head
Getting louder now

[Chorus]
So endlessly
These voices keep on calling me to rise
These voices keep on praying for me
These voices keep on praying for me
I can't stop them now

[Verse 2]
Each step I take
I make the same mistake
But they scream my name (Eh)
And I know it's wrong
To keep marching on and on
But I'm too far gone

[Pre-Chorus]
I can hear them in my head and I
I can hear them and I wanna get 'em out
I can hear them in my head
Getting louder now

[Chorus]
So endlessly
These voices keep on calling me to rise
These voices keep on praying for me
These voices keep on praying for me
I can't stop them now

[Bridge]
I keep hearin' them
Hearin' them come
Hearin' them voices
I keep hearin' them
Hearin' them come
Hearin' them voices
I keep hearin' them
Hearin' them come
Hearin' them voices
I keep hearin' them
Hearin' them come
Hearin' them voices

[Pre-Chorus]
I heard them say
I heard them say
I can't stop them now
I can hear them in my head and I (I can't stop them now)
I can hear them and I wanna get 'em out
I can hear them in my head
Getting louder now

[Chorus]
So endlessly
These voices keep on calling me to rise (They keep on calling me, yeah)
These voices keep on praying for me
These voices keep on praying for me
I can't stop them now

[Outro]
I can't stop them now
(Eh)