[Verse 1]
Hanging from an open door
With a glimpse of words unsaid
The white light starts to crawl
Into a higher place
And you wish you could be the one
To find your own embrace
But what you had is far from gone
What you had is far from gone

[Pre-Chorus 1]
Take my hand tonight
Before the night starts to unwind
The world will wake us up inside
The world will make us come alive
Take my hand tonight
Before the night starts to unwind
Take my hand tonight (tonight)

[Chorus]
'Cause I'm not ready to let you go (go)
You've got me losing control
No, I'm not ready to let you go (go)
Is this what you want? Want? Want?

[Verse 2]
White walls tumble down
Falling at the beat of your own drum
But this is what you waited for
This is what you waited for
I'm reaching out
Please turn around
I'm breathing out
Please make a sound

[Pre-Chorus 2]
Take my hand tonight
Before the night starts to unwind
Take my hand tonight (tonight)

[Chorus]
'Cause I'm not ready to let you go (go)
You've got me losing control
No, I'm not ready to let you go (go)
Is this what you want? Want? Want?

[Outro]
You've got me losing control
You've got me losing control
Is this what you want? Want? Want?