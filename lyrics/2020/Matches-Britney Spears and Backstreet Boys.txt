[Verse 1: Backstreet Boys & 
Britney Spears
]
I taste you in the air, your
Energy everywhere, uh-huh
It's borderline unfair
You know, I can see your brain, it's screamin' my name, 
oh
And if it's up to me, I'd
I'd meet you in-between my
Midnight Egyptian sheets
You know, I can see your brain, think you feel the same, 
oh

[Pre-Chorus: Backstreet Boys]
Oh, if they dusted me for prints
They'd find you all over me
All you all over me
Nothing ever quite felt like this
Our fire is killing me
The good kind of killing me

[Chorus: Britney Spears]
Like playing with matches, matches, matches
This might leave some damage, damage, damage
Thе good kind of damage, damage, damage
Likе playing with matches, matches, matches
Like playing with matches

[Drop: Britney Spears]
Like playing with matches
Playing with matches

[Verse 2: Britney Spears]
I like the way you dress, yeah
And then how you undress, yeah
Like it here on your chest
No, there ain’t no better place to catch my breath, yeah

[Pre-Chorus: Britney Spears]
Oh, if they dusted me for prints
They'd find you all over me
Are you all over me?
Nothing ever quite felt like this
Our fire is killing me
The good kind of killing me

[Chorus: Britney Spears]
Like playing with matches, matches, matches
This might leave some damage, damage, damage
The good kind of damage, damage, damage
Like playing with matches, matches, matches
Like playing with

[Bridge: Backstreet Boys]
Ah, ah, ah
Want you good in the worst way
Ah, ah, ah
Want you good in the worst way

[Chorus: Britney Spears]
Like playing with matches, matches, matches
This might leave some damage, damage, damage
The good kind of damage, damage, damage
Like playing with matches, matches, matches
Like playing with matches

[Drop: Britney Spears]
Like playing with matches
Playing with matches
Like playing with matches
Playing with matches