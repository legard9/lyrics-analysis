[Verse 1]
It's right here in the bed
No, lights won't turn it off
Won't let me sleep just yet
I know, I know
It's right here in the shower
Water won't take it off
Don't wanna leave just yet
I know, I know

[Pre-Chorus]
And oh-oh-oh
Pain is what happens with left over love
And it's right here, right here
Oh-oh-oh

[Chorus]
Rip my heart out in the fallout
Killing me right now, but I
I'd rather feel something than be numb
I'd rather feel something
Run right through it, right into it
Know it ain't easy, but I
I'd rather feel something than be numb
I'd rather feel something

[Post-Chorus]
Rather feel something
I'd rather feel something

[Verse 2]
It's right there in the car
Next to me on the seat
Riding with me in the streets
I go, I go

[Pre-Chorus]
And oh-oh-oh
Pain is what happens with left over love
And it's right here, right here, yeah

[Chorus]
Rip my heart out in the fallout
Killing me right now, but I
I'd rather feel something than be numb
I'd rather feel something
Went right through it, right into it
Know it ain't easy, but I
I'd rather feel something than be numb
I'd rather feel something

[Post-Chorus]
Rather feel something
I'd rather feel something

[Outro]
Rip my heart out in the fall out
Killing me right now, but I
I'd rather feel something than be numb
I'd rather feel something