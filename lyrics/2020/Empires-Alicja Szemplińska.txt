[Verse 1]
Ashes to ashes, dust into dust
I'll follow you through black
Try to forgive that I'm not awake
I'm dreaming this dream could last

[Pre-Chorus]
No looking down
There's fool's gold in our eyes

[Chorus]
Burning an empire
Happens so easy
Playing with fire
Maybe it's you and me
Burning an empire
Is it our fault?
We rise and fall

[Verse 2]
Like moths to a flame
Like bird to a pane of glass
Hoping for change but we do the same
We're gasoline and a match

[Pre-Chorus]
No looking down
There's fool's gold in our eyes

[Chorus]
Burning an empire
Happens so easy
Playing with fire
Maybe it's you and me
Burning an empire
Is it our fault?
We rise and fall

[Bridge]
We just want it all
Used to be a tower so tall
Now we only are crumbling walls
We rise and fall
Moth to the flame, as we do the same
Like a bird to a pane of glass

[Outro]
Down to the wire
Empires fall