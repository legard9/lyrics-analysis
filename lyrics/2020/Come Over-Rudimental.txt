[Chorus: Anne-Marie]
You should just come over
And I can just show ya
Let me do-over, yeah, yeah, yeah, yeah
You should just come over
And I can just show ya
Let me do-over, yeah, yeah, yeah, yeah

[Verse 1: Anne-Marie]
You living your life
Having a real good time
Better than when you were mine
I gotta be honest
I got regrets
Never gave you respect
Shoulda shown you that I cared
It's time I be honest

[Pre-Chorus: Anne-Marie]
I had to mess up so I could understand
You wеre the best thing that I еver, ever had
And I'll do anything for us to go back, no
If I could do it over, I would hold you closer
Fix the thing that left us broken
'Cause baby now I'm older, and my love's much bolder
I will never lose you again

[Chorus: Anne-Marie]
You should just come over
And I can just show ya
Let me do-over, yeah, yeah, yeah, yeah
You should just come over
And I can just show ya
Let me do-over, yeah, yeah, yeah, yeah

[Verse 2: Anne-Marie]
I miss what we had
And I want it all back
Can you give me one more chance?
I'm different, I promise
We'll make it work, yeah
I'll give you what you deserve
No, I will never be turned
I stick to my promise

[Pre-Chorus: Anne-Marie]
I had to mess up so I could understand
You were the best thing that I ever, ever had
And I'll do anything for us to go back, no
If I could do it over, I would hold you closer
Fix the thing that left us broken
'Cause baby now I'm older, and my love's much bolder
I will never lose you again

[Chorus: Anne-Marie, Tion Wayne]
You should just come over
And I can just show ya
Let me do-over, yeah, yeah, yeah, yeah
You should just come over
And I can just show ya
Let me do-over, yeah, yeah, yeah, yeah
(Huh, yo, T Wayne from the nine)

[Verse 3: Tion Wayne]
My love's like roulette, roulette
Once you left, I don't know who's next
Living my life, two-step, two-step
Two girl in my car, dark skin, brunette
M-way, Rolls Royce, double R
Switched up from corned beef to caviar
Make ups I don't like the war
Mi Amor, I ain't felt like this before
Me first then my gwolla
She said swallow your pride and let me holla
Came back and I shut down the summer
When I cop it, I bust down, I got her
She wanna do-over, sex in the Range Rover
But baby, it's over, yeah, yeah, yeah, yeah, yeah

[Chorus: Anne-Marie]
You should just come over (Yeah)
And I can just show ya
Let me do-over, yeah, yeah, yeah, yeah (Let me do-over)
You should just come over (You should come over)
And I can just show ya (I can just show ya)
Let me do-over, yeah, yeah, yeah, yeah
You should just come over (You should come over)
And I can just show ya (And I can just show ya)
Let me do-over, yeah, yeah, yeah, yeah (No)
You should just come over (Yeah)
And I can just show ya (Show ya)
Let me do-over, yeah, yeah, yeah, yeah (Over, no)
You should just come over, oh

[Outro]
Alright (no)
Yeah, gluten free and vegan for Holly and D, please
(Alright, thanks) Alright, thanks a lot, see ya, bye