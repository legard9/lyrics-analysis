"Wake up"

[Intro: Freddy Krueger]
Freddy Krueger's the name
You know my game
Elm Street's the place, if you've got the time
Listen to this, you'll bust a rhyme!

[Verse 1: Fat Boys]
Fred Krueger the myth or Fred Krueger the man
It doesn't matter cause I'm still
Rapping bout him, understand?
So sit back Jack, I'm gonna bust a rhyme
Grab a hold of your friends, it's Krueger time! (Freddy!)

[Freddy]
It's time for Freddy
See, I'm a popular guy
If you don't know yet
You're gonna find out why

[Verse 2: Fat Boys]
Now the place was Elm Street late one night
Lookin' like a ghost town, no one in sight
With a hat like a vagabond
Standing like a flasher
Mr. Big Time, Fred Krueger, dream crasher
Trying to find a girl to fit his fancy
Not once, not twice, but three times, Nancy!
All the people sleeping, snoozing and dreaming
While Krueger's on the corner of Elm Street scheming
Ha, they better wake up and listen to this
Because the blades on Freddy Krueger's hands won't miss!

[Nancy Thompson & 
Freddy Krueger
]
D-d-d-don't fall asleep
I'm your boyfriend now!

[Chorus]
1 AM and Freddy's here
The supreme dream maker, the master of fear
When you see night coming, stay away from the dark
Watch out for Freddy, he'll burst your heart
1 AM and Freddy's here (Gonna bust your heart)
The supreme dream maker, the master of fear
When you see night coming, stay away from the dark
Watch out for Freddy, he'll burst your heart

[Verse 3: Fat Boys]
Now Nancy's father was a cop, you see
But he didn't help her out because he never believed
See Tina paid a visit, one day in school
Freddy using her mind as his tool
Hard-headed close-minded cop closed his eyes
And soon to be nightfall, Krueger would rise
Rise from the grave and he won't behave
And with the face he got, he don't need to shave (Freddy!)
He got style, yellow teeth for a smile
Fred Krueger don't need no fingernail file
He will never stop, brother he won't cease
So forget about calling on the police
Cause Fred Krueger, his appearance will move ya
A step into a regular dream will soothe ya
He wouldn't like you like you're liking the beat
I tell you, never think about napping on E Street
Never light no match, leave your lighter alone
Cause like Santa Claus, Krueger will enter your home

[Nancy Thompson & 
Freddy Krueger
]
Fred Krueger, mom. Fred Krueger!
I'm here

[Verse 4: Fat Boys]
Even in part three, the Dream Warriors failed
And Mr. Big Time, Freddy Krueger prevailed
It was his prime time, I know you'll never forget
What he did to the girl with the TV set
But you can't stop Freddy cuz he's cool as ice
Coming right back at ya to slash and slice
Like a Ginsu blade, like a blender, he'll blitz ya
Fred Krueger's putting on the ritz

[Freddy Krueger]
You didn't think you was gonna get away from me now, did ya?
(Are you ready, are you ready, are you ready for Freddy?)

[Chorus]
3 AM and Freddy's here
The supreme dream maker, the master of fear
When you see night coming, stay away from the dark
Watch out for Freddy, he'll burst your heart
3 AM and Freddy's here (Gonna bust your heart)
The supreme dream maker, the master of fear (He was burned)
When you see night coming, stay away from the dark (He wears a weird hat)
Watch out for Freddy, he'll burst your heart (And a red and green sweater, really dirty)
Yo, who's this Freddy guy you hear about, man?
I don't know, he's-
Man, he was-
What does that- Ahh!

[Verse 5: Freddy Krueger]
You see, I make dreams
Nightmares, to be exact
Go to sleep for a moment
You'll be gone like that
You know it takes just a moment
For me to make you mine
In the movies, I thrill
But on the mic, I rhyme!
(Get ready, get ready, get ready, get ready for Freddy)

[Chorus]
3 AM and Freddy's here (B-b-b-bust your heart)
The supreme dream maker, the master of fear
When you see night coming, stay away from the dark
Watch out for Freddy, he'll burst your heart
3 AM and Freddy's here
The supreme dream maker, the master of fear
When you see night coming, stay away from the dark
Watch out for Freddy, he'll burst your heart
3 AM and Freddy's here (B-b-b-bust bust bust bust your heart)
The supreme dream maker, the master of fear
When you see night coming...