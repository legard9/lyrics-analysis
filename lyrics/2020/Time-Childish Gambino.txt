[Verse 1: Childish Gambino]
Seven billion people
Tryna free themselves
Said a billion prayers
Tryna save myself
I can see it coming
But it's moving fast

[Verse 2: Ariana Grande]
Been through a hurricane
With the sunroof down
Dancing with no pain
We wait to see the sunrise
It's a holiday (Hey)
When you're around (When you're around)
Breath of fresh air like a cold winter breeze
And I can feel it slow down, oh

[Chorus: Childish Gambino]
Maybe all the stars in the night are really dreams
Maybe this whole world ain't exactly what it seems
Maybe the sky will fall down on tomorrow
But one thing's for certain, baby
We're running out of time

[Post-Chorus: Childish Gambino]
Oh, time, time, yeah

[Verse 3: Childish Gambino]
Running after something
But I don't know what
Am I running to?
Too afraid to stop
Hundred miles an hour
With no seatbelt on
Time is everlasting
I can't wait that long (Ooh)

[Chorus: Childish Gambino]
Maybe all the stars in the night are really dreams (All the stars)
Maybe this whole world ain't exactly what it seems (This whole world)
Maybe the sky will fall down on tomorrow (Ooh)
But one thing's for certain, baby
We're running out of time

[Verse 4: Ariana Grande]
My feet are falling to the bottom of the ocean
Running out of time, out of time, uh, oh yeah
High on emotion, I can feel it moving forward
Running out of time, running out
Running out, running out of time

[Chorus: Brent Jones & The Best Life Singers]
Maybe all the stars in the night are really dreams (All the stars)
Maybe this whole world ain't exactly what it seems (This whole world)
Maybe the sky will fall down on tomorrow (Ooh)
But one thing's for certain, baby
We're running out of time

[Bridge: Childish Gambino]
(Time)
(Time)
Uh
It's about power (Time)
All about power (Time)
Oh, I'm in time
I'm just tryna feel it (Time)
Get a little bit stronger
Gotta go all out (Time)
A nigga gon' ball out
Try not to fall out
Tryna be better
Gotta get better (Time)
Too many pressure
Nigga need pressure
People need pressure
Uh, woo

[Outro]
Running out of time
We're running
Running out of time (Woah)
We're running
Running out of time
We're running