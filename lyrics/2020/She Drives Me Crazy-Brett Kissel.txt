[Verse 1]
She walks in like a ten, lookin' knock out
Turnin' heads, drop me dead, spinnin' right 'round
She's more than messin' with my mind
She's that tiger kinda wild, got the devil in her eyes (Ayy, ayy, ayy)
She's got that body that I crave, I can't help but misbehave
It's the way she looks tonight

[Chorus]
She drives me cra-a, a-a-a-a-a...
A-a, a-a-a-a-a
A-a, a-a-a-a-a-azy
She drives me cra-a, a-a-a-a-a...
A-a, a-a-a-a-a
A-a, a-a-a-a-a
She drives me crazy

[Post-Chorus]
She drives me crazy
Cra-a, a-a-a-a-a...
A-a, a-a-a-a-a
A-a, a-a-a-a-a-azy
She drives me crazy

[Verse 2]
She sways her hips, midnight kiss, taste tequila
Neon glow, hold me close, God, I need her
She's more than messin' with my soul
Got that Marilyn Monroe, I can't help but lose control

[Chorus]
She drives me cra-a, a-a-a-a-a...
A-a, a-a-a-a-a
A-a, a-a-a-a-a-azy
She drives me cra-a, a-a-a-a-a...
A-a, a-a-a-a-a
A-a, a-a-a-a-a-azy
She drives me crazy

[Post-Chorus]
She drives me crazy
Cra-a, a-a-a-a-a...
A-a, a-a-a-a-a
A-a, a-a-a-a-a-azy
She drives me crazy

[Instrumental Bridge]

[Verse 3]
Yeah, that girl takes up all of my time
I spend every dollar, I spend every dime
She stole my heart and took over my mind
It's loco, game over, she drives me crazy

[Bridge]
Yeah, she drives me, drive
She drives me crazy, crazy
She walks in like a ten, lookin' knock out
Turnin' heads, drop me dead

[Outro]
She drives...
Cra-a, a-a-a-a-a... (Crazy, crazy, crazy)
A-a, a-a-a-a-a
A-a, a-a-a-a-a-azy
She drives me... (Crazy, crazy)
Cra-a, a-a-a-a-a... (Crazy, crazy, crazy)
A-a, a-a-a-a-a
A-a, a-a-a-a-a-azy
She drives me crazy
A-a, a-a-a-a-a
A-a, a-a-a-a-a
A-a, a-a-a-a-a-azy, a-a-a