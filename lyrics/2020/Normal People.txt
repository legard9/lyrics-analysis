[Verse 1: 2D]
I'm a pale imitator of a boy in the sky
With a cap in his head, and a knot in his tie
I'm the light in the mall when the power is gone
A shadow in a corner, just playin' along!
I'm gonna lay in my bed, I'm rolling aside
But if a get a car, uh, I'm hoping to ride
Because I know if I ever say, "I'm leaving." to you
You've got a holding chain and you don't know what to do!
You'd stop

[Chorus: James Murphy]
You wanna do it, but you don't know what you doing, baby, aah
You wanna feel it, but you don't know what you're feeling tonight (Oh, hey, how will we do it?)
And if you're thinking that I don't know what you're thinking, baby, aah
You go on thinking and you gonna make it alright!
(Oh, hey, how will you do?)

[Verse 2: 2D]
I'm impregnable, incredible, and set in the quo
I'm a late in my ride, and not a heart in soul
I got Saturday night, enough been said
And if I ever had to do it
Well, you know I wouldn't care
I'd just get down, I loving the feat
And if I wanna talk back, the message is free
Then there's a fire, a ray, and out in the sun
And if you can’t partake, you get ‘em alive
You get a heartbreak

[Chorus: James Murphy]
You wanna do it, but you don't know what you doing, baby, aah
You wanna feel it, but you don't know what you're feeling tonight (Oh, hey, how will we do it?)
And if you're thinking that I don't know what you're thinking, baby, aah
You go on thinking and you gonna make it alright!
(Oh, hey, how will you do?)

[Post-Chorus: Andre 3000]
Every time we try, we get nowhere
Wouldn't it be nice if we were just normal people? Yeah
Trying so hard to act like we don't care
But it's true you do, nothing is left so I guess I'm right

[Verse 3: André 3000]
New word - onomatopoeia
Boom! Quit acting like you don't wanna be here
Fuck around and get jumped like leap year
Glock in the glove make you really wanna leave me alone
Get the fuck on, gone
Okay, okay, okay, back to the happy song
Rap ain't nothing but the art of talking shit
My girl look pretty up there ridin' dick
My plaid pants, my solid future
Asinine ass and a gorgeous coochie
I'm an outcast but you're into me
Summer got mad 'cause winter blew me
That juicy fruit, that splooshy sploosh
Generation X on bloopty bloop
Get doop, gotta get doopty doop
Everybody hit the floor, we through the roof
Like a chimney, I commend me
How come it be some lame ass nigga talking about, "Ah, he don't rap enough?"
But y'all rap a lot, and I'm like wrap it up, ho
We ain't Scarface, we ain't Willie D
We ain't Bushwick, ye ain't killin' me
Better play with your motherfucking mama
Bet you still stay with your motherfucking mama
Keep sleeping on me, I'mma rock my pajamas
In the daytime, I swear, I promise
Dare a nigga "say something" tear a nigga face off
How come blacks don't play baseball?
Y'all white, know y'all can taste all
This fly shit I stay finna take off

[Refrain: André 3000]
Do ya damn thang, do ya thang-a-thang
Do ya damn thang, do ya thang-a-thang
Do ya damn thang, do ya thang-a-thang
Yee ain't fresh squeezed juice, nigga you that Tang
Do ya damn thang, do ya thang-a-thang
Do ya damn thang, do ya thang-a-thang
Do ya damn thang, do ya thang-a-thang
Wait, you really Slick Rick? Nope, you Dana Dane

[Verse 4: André 3000]
Uh, let it up, head erupts
A lava language and the vocal volcanic (Yeah)
If it ain't fixed don't broke it, don't panic (Yeah)
If it ain't hits, it ain't shit, God dammit (Yeah)
If it ain't this, it ain't dope, it don't flush
And if it ain't hip or don't hop, well then hush
"Man they sound like…"
"Man they stole yo…"
"Man they look like…"
Nope, it ain't us

[Refrain: André 3000]
Do ya damn thang, do ya thang-a-thang
Do ya damn thang, do ya thang-a-thang
Do ya damn thang, do ya thang-a-thang
Is you really Slick Rick? Nope, you Dana Dane
Do ya damn thang, do ya thang-a-thang
Do ya damn thang, do ya thang-a-thang
Do ya damn thang, do ya thang-a-thang
Yee ain't fresh squeezed juice, nigga you that Tang

[Bridge: André 3000]
Every time we try, we get nowhere
Wouldn't it be nice if we were just normal people? Yeah
Trying so hard to act like we don't care, I don't care
But it's true you do, nothing is left, so I guess I'm right
Flip the page, our days are revelations, hide!
Space is strange, doctor, I've got no patience!
Oh, it's all a part of the process
Okay, okay
Nothin's new, it's true, cool, I admit, shit, I guess you're right!

[Outro 1: André 3000]
I even mumble I'm the shit
Need mumble, I can mumble
I can mumble that I'm the shit
I'm the shit!
I'm the shit!
Yeah, I'm the shit!
Ha, I'm the shit!
I know it sound good to you!
Cause it sound excellent to me!
I'm the shit, yeah!
Yeah, I'm the shit!
I do it cause I'm convinced
I do it cause I'm the shit
I'm never on the fence!
Ah! I'm never on the fence!
Yeah! I'm the shit!
Yeah! I'm the shit!
Yeah!
I could even take keys on y'all!
I can even make it a hundred degrees on y'all, I'm the shit!
Yeah, yeah! I'm the shit!
Ah, I'm the shit!
Mumble cause I'm the shit
Yeah, I can mumble cause I'm the shit, I can mumble
I don't have to say shit cause I'm the shit
I don't have to say a goddamn thing cause I'm the motherfucking shit
And you know it!
I'm the shit!
Say it wit' me!
I'm the shit
Sometimes I feel like the shit
Sometimes I feel like the shit, sometimes I feel like I'm shit
Sometimes I wanna stand for somethin', then sometimes wanna sit
I didn't really plan on cussin', but sometime it just slip
Ah! I'm the shit!
I'm the shit!
Yeah, I'm the shit!
Ah! I'm the shit!
Yeah! I'm the shit!
Ahh, yeah! I'm the shit!
I know it sound good to you
'Cause it sound good to meeeeee
I'm the shit!
Yeah, yeah, I'm the shit!
I can even walk away from the mic
'Cause I'm the shit! I did it tonight
I'm the shit! Yeah, I do it riiiight
I'm the shit! I don't even have to sing in key
Ohhh! It's all about me! I'm the shit!
I'm crawling back to the mic! I'm the shit!
I could do this shit all night! I'm the shit!
I could do it in the day, I'm the shit!
I could do it any way, I'm the shit!
Yeah, I'm the shit!
That's it
Yeah, I can say it
Now I can say it, don't you see it?
I don't even sweat!
Ah! I'm the shit!
I'm the shit!
I said I'm the shit!
It might sound good to you
It sound excellent to me
Fuck! I'm the shit!
I'm the shit!
I take a piss cause I'm the shit!
I can fart on it, I'm the shit!
I get smart on it, I'm the shit!
I get dumb on it, I'm the shit!
I make her crumb on it, I'm the shit!
The sum of it; I'm the shit!
The sun love me, I'm the shit!
The moon is jealous of me!
The sun is jealous of me!
The rain is jealous of me!
EVERYTHING IS JEALOUS OF ME!
I'm THE SHIT!
I'm THE SHIT!
I'm THE SHIIIIIIIIIIIIIIAAAHHHH
AHHHHH-AH-AH-AH-AH, AH-AH-AH-AH, AH-AH-AH-AH
You can't have a piece of my pie! I'm the shit!
I'm that guy! I'm the shit!
I don't know why! But I'm the shit!
Yeah, I'm the shit
Don't even have to scream
I don't even have to dream
I'm the shit, I make you cream
Cause I'm the shit
Yeahhh, I'm the shit
When I stand up, everybody sit, I'm the shit
Asshole

[Break]

[Outro 2: André 3000]
Sometime I feel like the shit, sometime I feel like I'm shit
Sometime I wanna stand for somethin', then sometime wanna sit
Didn't really plan on cussin', but sometime it just slip
When you the mayonnaise and the mustard, you can't relish in it
And when them panties turn to custard, who the hell do they miss?
And when the nanny fucks the husband, what does she tell the kids?
That's a whole 'nother discussion, we can't fit in our kicks
That's a whole 'nother discussion, you can't fit in these kicks
Yeah
The way I feel tonight, I think I'll wear these shades for the rest of my life
Yeah
Yeah
The way I feel tonight, I think I'll wear these shades for the rest of my life
The way I feel tonight, I think I'm-a wear these shades for the rest of my life
That way, you won't be able to see the disappointment in my eyes
Cause they don't lie (don't lie), not to my eyes (not to my eyes, not to my eyes)
Nah, they don't lie (don't lie), not to my eyes, (not to my eyes…) un-un
(They don't lie, they don't lie)
(Not to my eyes, not to my eyes)
(Not to my eyes, not to my eyes)
(Not to my eyes, not to my eyes)