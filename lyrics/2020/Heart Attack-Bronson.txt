[Verse: lau.ra]
I've been patient
But I won't follow
'Cause I don't wanna be your friend
Why does it matter in the end?
I want a little bit of lightning
No, I don't wanna be your friend
'Cause these feelings are violent

[Chorus: lau.ra]
This love's a heart attack
This love's a heart attack
This love's a heart attack

[Verse: lau.ra]
See, I've been patient
But I won't follow
'Cause I don't wanna be your friend
Why does it matter if it ends?
I want a little bit of lightning
No, I don't wanna be your friend
I want a little bit of lightning
No, I don't wanna be your friend
I want a little bit of lightning
Why does it matter if it ends?
But I don't wanna live in silence
Why does it matter in the end?
I want a little bit of lightning
No, I don't wanna be your friend
Because these feelings are violent
Why does it matter in the end?

[Chorus: lau.ra]
This love's a heart attack
This love's a heart attack
This love's a heart attack
This love's a heart attack

[Post-Chorus: lau.ra]
This love's a heart attack
And no, I won't go back (Yeah)
This love's a heart attack (Oh)
This love's a heart attack (Oh, you)

[Outro: lau.ra]
Why does it matter anyway
Why does it matter in the end?
Why does it matter anyway
Why does it matter in the end?