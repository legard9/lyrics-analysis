[Verse 1]
Swimming in a pool of people
The only one I see is you
All these bodies dancing, but it's
Like we're standing in an empty room
We got the love in our blood now, yeah
Feels like we're coming up now, yeah
Drowning in a pool of people
Now all I ever see is you

[Pre-Chorus]
You pull me back like nobody, nobody could
I tried but I can't fight the feeling
Feels like dreaming

[Chorus]
So give me that summertime
Sweet like caramel
The way that you get me high
Is something like, something like chemicals
Oh oh oh oh oh oh oh oh oh oh oh oh
Something like, something like chemicals
Oh oh oh oh oh oh oh oh oh oh oh oh

[Verse 2]
Bungee jumping out a jet plane
Adrenaline state of mind
Super powers, I'm all Bruce Wayne
It's a feeling that I kinda like
Yeah, the way you got me free falling
Make me want to cash a cheque all in
Every time I got you with me
I'm feeling like I lose my mind

[Pre-Chorus]
You pull me back like nobody, nobody could
I tried but I can't fight the feeling
Feels like dreaming

[Chorus]
So give me that summertime
Sweet like caramel
The way that you get me high
Is something like, something like chemicals
Oh oh oh oh oh oh oh oh oh oh oh oh
Something like, something like chemicals
Oh oh oh oh oh oh oh oh oh oh oh oh

[Bridge]
You pull me back like nobody, nobody could
I tried but I can't fight the feeling
Feels like dreaming
I can't fight the feeling
Feels like dreaming

[Chorus]
So give me that summertime
Sweet like caramel
The way that you get me high
Is something like, something like chemicals
Oh oh oh oh oh oh oh oh oh oh oh oh
Something like, something like chemicals
Oh oh oh oh oh oh oh oh oh oh oh oh

[Post-Chorus]
Something like, something like chemicals