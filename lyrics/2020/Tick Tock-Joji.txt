[Intro]
Yeah

[Verse 1]
Tick-tock
Heavy like a Brink's truck
Looking like I'm tip-top
Shining like a wristwatch
Time will grab your wrist
Lock it down 'til the thing pop
Can you stick around for a minute 'til the ring stop?
Please, God
Tick-tock
Heavy like a Brink's truck
Looking like I'm tip-top
Shining like a wristwatch
Time will grab your wrist
Lock it down 'til the thing pop
Can you stick around for a minute 'til the ring stop?
Please, God

[Chorus]
As time keeps slipping away (Slipping away)
Girl, don't start feeling a way (Feeling a way)
You and I, we are one and the same
Loving in pain, loving in (Loving in pain)
As time keeps slipping away (Slipping away)
Girl, don't start feeling a way (Feeling a way)
You and I, we are one and the same
Loving in pain, loving in

[Verse 2]
Tick-tock
Gimme, gimme big bucks
That is all I do, girl
I just hit the jackpot
I wish you were here with me now so I could feel some
I wish you were here to hold me down like a real one, real one
Live long
Wanna be a big shot
Should've stayed away
But always had a weak spot
I've been on the road and I'm sorry for the mix-up
If you still love me, can you see me during liftoff? (Liftoff)

[Chorus]
As time keeps slipping away (Slipping away)
Girl, don't start feeling a way (Feeling a way)
You and I, we are one and the same
Loving in pain, loving in (Loving in pain)
As time keeps slipping away (Slipping away)
Girl, don't start feeling a way (Feeling a way)
You and I, we are one and the same
Loving in pain, loving in

[Outro]
With my hands in the ocean
With my hands in the ocean
With my hands in the ocean, I pay
I pray, oh, I pray