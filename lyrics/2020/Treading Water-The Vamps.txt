[Verse 1]
This lonely heart of mine grows colder every night
And this glass upon my eye falls victim to the light

[Pre-Chorus]
Stuck in Ca-alifornia, world out before ya
But somethin's missin' (Somethin's missin')
You should be happy, whatever that means
But something feels different

[Chorus]
So wherever you are
Hope you don't take too long
I need a little love in my life
I need a little love in my life
I'm breakin' my own rules (My own rules, my own rules)
You can't hurry things along
So when you need a little love in life
You'll find a little love

[Verse 2]
Been pushin' love away to save myself the hurt
Ain't much heart left to break so clearly it don't work, work, work

[Pre-Chorus]
Stuck in Ca-alifornia, world out before ya
But somethin's missin' (Somethin's missin')
I should be happy, whatever that means
I need anything to make me feel

[Chorus]
So wherever you are
Hope you don't take too long
I need a little love in my life
I need a little love in my life
I'm breakin' my own rules
(My own rules, my own rules, my own rules)
You can't hurry things along
So when you need a little love in life (Little love, little love)
You'll find a little love

[Outro]
Grew up underneath the rising love
Watched it battle through the turbulence
I just wanna feel understood
Patiently, I waited patiently
To share all of my insecurities
First, I really gotta work on me
Grew up underneath the rising love
Watched it battle through the turbulence
I just wanna feel understood
(I keep shutting down, mm)
Patiently, I waited patiently
To share all of my insecurities
First, I really gotta work on me
(Mm, mm, mm, mm, mm)
Grew up underneath the rising love
Watched it battle through the turbulence
I just wanna feel understood
(I keep shutting down, mm)
Patiently, I waited patiently
To share all of my insecurities
First, I really gotta work on me
(Lay down with me now, mm)
Grew up underneath the rising love
Watched it battle through the turbulence
I just wanna feel understood
(I keep shutting down, mm)
Patiently, I waited patiently
To share all of my insecurities
(Lay down with me now)
First, I really gotta work on me