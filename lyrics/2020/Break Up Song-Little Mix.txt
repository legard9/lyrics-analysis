[Verse 1: Leigh-Anne]
This is not a second chance, no, no, baby
This is not a new romance, not tonight (Ah-ah-ah)
This is for all the nights I cried for you, baby
Hoping you could be the one that could love me right

[Pre-Chorus: Jesy]
I'll be good all by myself
Yeah, I'll find a way to dance without you
In the middle of the crowd
I'll forget all of the pain inside, oh

[Chorus: All]
So tonight, I'll sing another, another break-up song
So turn it up, let it play on and on and on and on
For all of the times they screwed us over
Let it play on and on and on
Just another break-up song

[Post-Chorus: Perrie & All]
Ain't no more tears (Oh)
Ain't gonna cry (Oh)
Boy, I'll do anything to get you off my mind
I'm gonna dance (Oh)
Under the lights (Oh)
Boy, I'll do anything to get you off my mind

[Verse 2: Perrie]
I ain't even gonna call ya, no, baby
The best thing I ever did was to let you go (Oh-oh-oh)
Did you think you were the only one who could save me?
I ain't gonna take you back like I did before (No, woah)

[Pre-Chorus: Jesy & Leigh-Anne]
'Cause I'll be good all by myself
Yeah, I'll find a way to dance without you (Dance without you)
In the middle of the crowd
And forget all of the pain inside, oh

[Chorus: All, Leigh-Anne & Perrie]
So tonight, I'll sing another, another break-up song (Break up song)
So turn it up, let it play on and on and on and on (On and on)
For all of the times they screwed us over
Let it play on and on and on
Just another break-up song

[Post-Chorus: Perrie, All, Perrie & Jesy]
Ain't no more tears (Oh)
Ain't gonna cry (Oh, oh-ho)
Boy, I'll do anything to get you off my mind
I'm gonna dance (I'm gonna dance)
Under the lights (Under the lights)
Boy, I'll do anything to get you off my mind

[Bridge: Jade & Jesy]
I don't wanna turn back time
'Cause what's another lonely night?
I know under these lights, I'm good without you
(I'm good, I'm good, I'm good)
For all those tears that I cried
I'll sing it louder tonight
Let it play on and on and, runnin' on and on (Oh, oh)

[Chorus: All, Perrie, Jesy & Jade]
So tonight, I'll sing another (Oh)
Another break-up song (Another break up song, babe)
So turn it up, let it play on and on and on and on (Oh yeah, yeah)
For all of the times they screwed us over (All the times they screwed us over)
Let it play on and on and on (And on and on)
Just another break-up song

[Outro: All, Jesy & Jade]
I don't wanna turn back time (Oh)
'Cause what's another lonely night? (Lonely night, baby)
I know under these lights, I'm good without you
(I'm good, I'm good, I'm good)
For all those tears that I cried (Oh)
I'll sing it louder tonight
Let it play on and on and on
Just another break-up song