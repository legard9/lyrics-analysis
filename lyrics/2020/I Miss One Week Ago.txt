[Verse 1]
I miss one week ago
One week ago from now
I worried that I'd lost my phone
And everyone who tried to reach me would be quite annoyed
One week ago I worried 'bout a phone

[Verse 2]
I miss one week ago
One week ago from now
When I was running late again
And missed my exit on the freeway
Couldn't make a turn
One week ago I worried I was late

[Verse 3]
I miss one week ago
One week ago from now
I wondered who would appreciate
Or even get the music that I'm trying to create
One week ago I worried out of vain

[Verse 4]
I miss one week ago
One week ago from now
I got upset at 6 A.M
When birds were chirping violently
And the sun was peeking in
One week ago I worried
Would I fall asleep again?
(Fall asleep again)

[Verse 5]
I miss one week ago
One week ago from now
I worried 'bout a million things
And all of them are irrelevant 'cause everything has changed
And of all my previous worries, I'm ashamed