[원호 "Open Mind" 가사]

[Verse 1]
넌 실루엣도 뭔가 달라, baby
따라 그려 보고 싶어 lady
아무도 몰래 훔친 너의 shadow
다르게 흘러 가는 너의 채도

[Pre-Chorus]
온몸이 짜릿해지게
더 새로운 세상을 줄게
Don't you want somethin' different?

[Chorus]
(Ooh, oh, ah) You don't have to love me
(Ooh, oh, ah) 'Cause we want the same thing
상상하지 마 완전 다를 테니까
오늘 밤은, keep an open mind, girl

[Post-Chorus]
Keep an open mind, girl (Oh)
Keep an open mind, girl

[Verse 2]
처음 느끼는 감각을 전부 깨워 (Ah)
넌 다시 태어나게 돼 지금부턴 (Uh)
나란 세곌 감당 하고 난 뒤엔, yeah
넌 중독될 거야 내게 장담할게

[Pre-Chorus]
온몸이 짜릿해지게
더 새로운 세상을 줄게
Don't you want somethin' different?

[Chorus]
(Ooh, oh, ah) You don't have to love me
(Ooh, oh, ah) 'Cause we want the same thing
상상하지 마 완전 다를 테니까
오늘 밤은, keep an open mind, girl

[Post-Chorus]
Oh (Ah), you and I (Ah)
미쳐도 좋아 망설일 거 없잖아
(Open mind, girl)
Oh (Ah), you and I (Ah)
후횐 없어 걱정 따윈 사치야
(Open mind, girl)

[Chorus]
(Ooh, oh, ah) You don't have to love me
(Ooh, oh, ah) We both want the same thing
상상하지 마 완전 다를 테니까
오늘 밤은, keep an open mind, girl

[Post-Chorus]
Oh (Ah), you and I (You and I, girl)
미쳐도 좋아 망설일 거 없잖아
(Open mind, girl)
Oh (Ah), you and I (You and I, girl)
후횐 없어 걱정 따윈 사치야
(Open mind, girl)

[Outro]
(Ooh, oh, ah)
Keep an open mind, girl