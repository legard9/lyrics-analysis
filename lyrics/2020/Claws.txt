A new dawn pure like the hawthorn
And the squall preceding her call
The world is crying for love
And her light is what we're looking for

Primeval high choir
Barefoot in the mire

Proudly she walks
The darkness she stalks
Her white fur from flames and cold protects her
Her howling echoes like a song divine
Her claws the end of all demons sign

Primeval high choir
Barefoot in the mire

Your voice is your claw
May your song be heard
A magic place you can draw
Spread the art into the world