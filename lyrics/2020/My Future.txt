Moonlight all around
(My eyes are shining)
Shadows on the ground
(My mind is flying)
Will there ever be an end?
(There's no defining)
Forever running from this lunacy
Moonlight coming down
(My hope igniting)
Beaming through the clouds
(My thoughts colliding)
Will I never see the end?
(Just keep surviving)
Forever searching for my destiny
Battlefield...
Illuminated...
What will be when it's over?
Can you see?