[Verse 1: Carly Rae Jepsen]
I'm at a war with myself
We go back to my place
Take my makeup off
Show you my best disguise
You believe in my heart
You believe in my kind
It's a tragedy, boy
Every time you ask me

[Chorus: Carly Rae Jepsen]
I don't know what I'm feeling, but I believe
I was thinking 'bout making a comeback, back to me
And I won't say you're the reason I was on my knees
But I'm thinking 'bout making a comeback, back to me

[Verse 2: Carly Rae Jepsen & Bleachers]
I'm at peace in the dark
When I know that you're near
Hear the breath of your heart
Singing me a lullaby
All those traveling years
'Til we said our goodbye
And I show up to your place
You don't even ask me why

[Chorus: Carly Rae Jepsen with Bleachers]
I don't know what I'm feeling, but I believe
I was thinking 'bout making a comeback, back to me
And I won't say you're the reason I was on my knees
But I'm thinking 'bout making a comeback, back to me

[Bridge: Carly Rae Jepsen]
And if you wanna know why I have no regrets
Sometimes, you gotta dig low to get 'round to it
And there is nothing I'm so sure of, nothing I'm so sure of
If you wanna know why I have no regrets

[Verse 3: Carly Rae Jepsen & Bleachers]
Well, then you'll come with me
I'm the keeper of that beat
And the fire under your feet
I'm the keeper, man, I am the keeper
Come, baby
Dance the night down on your knees
Dance your heart down onto your sleeve
I'm the keeper, man, I am the keeper
And I am the keeper

[Pre-Chorus: Carly Rae Jepsen]
I don't know what I'm feeling, but I believe
I was thinking about making a comeback, back to me

[Chorus: Carly Rae Jepsen & Bleachers]
I don't know what I'm feeling, but I believe
I was thinking that maybe you'll come back, back to me
And I won't say you're the reason I was on my knees
But I'm thinking that maybe you'll come back, come back to me

[Outro: Carly Rae Jepsen & Bleachers]
Come with me
I'm the keeper of that beat
And the fire under your feet
I'm the keeper, man, I am the keeper
Come, baby
Dance the night down on your knees
Dance your heart down onto your sleeve
I'm the keeper, man, I am the keeper