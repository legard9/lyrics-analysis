[Intro]
Woo!

[Verse 1]
I got the good if you got the time
I got the moon if you got the shine
I got the back if you got the beat
Got that solid gold country 33 on repeat

[Chorus]
All night
Get, get, get to livin', feelin' alright
Dancin' out the denim in 'em, oh my, Levi's
I got the fuse if you got the light
I got the all if y'all got the night
All night
All night
Woo!

[Verse 2]
I got the rebel if you got the yell (Hey)
I got the raisin', if you got the hell
I got the zig (I got the zig) if you got the zag
Got that good life, hell of a time in a bag

[Chorus]
All night
Get, get, get to livin', feelin' alright
Dancin' out the denim in 'em, oh my, Levi's
I got the fuse if you got the light
I got the all if y'all got the night
All night

[Bridge]
What good is a saint if you ain't got the sinnin'?
What good is a life if you don't get to livin'?
What good is the one if you ain't got the other?
Takes two to tango, let's get this thing goin'

[Chorus]
All night
Get, get, get to livin', feelin' alright
Dancin' out the denim in 'em, oh my, Levi's
I got the fuse if you got the light
I got the all if y'all got the night
All night
All night
All night
All night
All night