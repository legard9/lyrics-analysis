[Intro: various samples]
Whether or not

[Hook]
Some think I'm clever, others think I'm the one
Who makes too many references to weather
Or not
Or not

[Verse 1]
I set it off on stage, I got something
I can't put my finger on that others don't got
So I try real hard to analyze the operation
And end up falling even deeper in thought
A vibe's just a vibe, most times I can't describe it
And life delays death, I'm tryna survive it
The seven sins still haunt me at night
I was afraid of the dark, but heard death in the light
So reverse that, reword that, I'm afraid of nothing now
I used to front a little, I ain't even bluffin' now
The price to pay, well, probably got it double now
It's double trouble, double trouble when your other gal
Is catching up, I used to live in the weed spot
Crash came knocking through the door, thirteen cops
All just to find a little pistol and weed
Beat the case, now I shall proceed, whether or not

[Hook]
Some think I'm clever
Others think I'm the one
Who makes too many references to weather
Or not
Or not
Some think I'm clever
Others think I'm the one
Who makes too many references to weather
Or not
Or not

[Verse 2]
I pick a pad up and write a rhyme, it's that easy
Oil smear the ink when my hand brush across breezy
I caught the holy ghost, born to roll, raised by the corner most
The coroner, the siren singing with the corner folks
The corner store cats don't call them opportunists
Don't work, don't eat, but they just not distributors
I've been dope since the riots of loot in '92
I've been spitting that dope with Dilated it's due
I take my chances and live between the checks of advances
Specs detail down to nano with the answers
Cannabis lit, you can't tell me a hot damn
Thinkin' when I sing a song believing I dropped that
I left off from last time and now it's been a step up
And every LP that I drop I keep my rep down
Best kept then I jet town, bet that I'm westbound
No motherfucking rest now, weather or not

[Hook]
Some think I'm clever
Others think I'm the one
Who makes too many references to weather
Or not
Or not
Some think I'm clever
Others think I'm the one
Who makes too many references to weather
Or not
Or not

[Verse 3]
Love your fans and like your label
And I ain't making shit for neither, lay that on the table
I'm making shit for me until the vision's done
And let the rhythm hit 'em when the system bump
I'm here to tip the scale in your area
The ups are high, the downs build your character
Middle ground's a dangerous place
'Cause mediocrity be socking everyone in the face
Still a race to the finish line
I keep the pace, the difference how I finish mine
It's 'cause I disregard the competition
In my own lane I paint the composition
Calculated moves and working angles
Sweating like a pig but rapping like an angel
That's how the game goes 'til everything remains raw
Staring at the sun, but I'm wishing that the rain fall

[Hook]
Some think I'm clever
Others think I'm the one
Who makes too many references to weather
Or not
Or not
Some think I'm clever
Others think I'm the one
Who makes too many references to weather
Or not
Or not