[Intro]
Freedom-dom, freedom-dom, freedom
Freedom-dom I've been looking for
Freedom-dom, freedom-dom, freedom
Freedom-dom I've been looking for

[Verse 1]
I was living a lie, living a lie
This is my confession
I was living a lie before we met
There were so many nights, so many nights
Full of dark temptation
There was so many nights that I regret

[Pre-Chorus]
You give me something that I can hold on to
A little light when I'm down on my knees
I was so lost in myself when I found you
But in that moment you made me believe

[Chorus]
You give me freedom, freedom
Freedom I've been looking for
Freedom, freedom is you
You give me freedom, freedom
Freedom I've been looking for
Freedom, freedom is you

[Pre-Drop]
You give me freedom-dom, freedom-dom, freedom
You give me freedom-dom I've been looking for
You give me freedom-dom, freedom-dom, freedom
You give me freedom-dom I've been looking for

[Drop]
You give me freedom-dom, freedom-dom, freedom
You give me freedom-dom I've been looking for
You give me freedom-dom, freedom-dom, freedom
You give me freedom-dom I've been looking for

[Verse 2]
I didn't care, I didn't care enough
To stop me falling
I didn't care about myself
'Til you lifted me up, lifted me up
When I was down and out
It's the highest I have ever felt

[Pre-Chorus]
You give me something that I can hold on to
A little light when I'm down on my knees
I was so lost in myself when I found you
But in that moment you made me believe

[Chorus]
You give me freedom, freedom
Freedom I've been looking for
Freedom, freedom is you
You give me freedom, freedom
Freedom I've been looking for
Freedom, freedom is you

[Pre-Drop]
You give me freedom-dom, freedom-dom, freedom
You give me freedom-dom I've been looking for
You give me freedom-dom, freedom-dom, freedom
You give me freedom-dom I've been looking for

[Drop]
You give me freedom-dom, freedom-dom, freedom
You give me freedom-dom I've been looking for
You give me freedom-dom, freedom-dom, freedom
You give me freedom-dom I've been looking for

[Outro]
You give me freedom, freedom
Freedom I've been looking for
Freedom, freedom is you