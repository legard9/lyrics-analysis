[이달의 소녀 "So What" 가사]

[Intro]
I'm so bad
I'm so bad

[Verse 1]
늘 뻔한 다-다-답
길게도 blah, blah, blah
더 늘어지기 전에 보란 듯이
걸러, 겁내지 마

[Pre-Chorus]
골라 봐 자 tic tac toe
O 아님 X 잖니 (Ha)
더 크게 sho-shout!
너답게 ho-hot!

[Chorus]
Eh-eh-eh
감출 수도 없게 burn
Oh-oh-oh
달라지는 네 눈빛 (Hey)
까마득히 높던 벽
넘어 보일게
아찔한 눈앞 저 멀리
Follow me

[Refrain]
가시 돋친 게, so what?
얼음 같은 게, so what?
겁이 없는 게 (Bad)
어때서? So what?
(Ba-ba-ba-bad)
Take that, so what?

[Post-Chorus]
I'm so bad (그게 어때?)
I'm so bad (뭐 어때?)
I'm so bad (더 자유롭게)
I'm so bad

[Verse 2]
박차고 일어나 달리는 법
떠오르게 해 줄게
뜨겁게 널 (Woo-woo-woo-woo)
끌어내 더 (Woo-woo-woo-woo)
널 위한 세상의 중심은 너
비좁은 새장은 no
좀 더 높이 (Woo-woo-woo-woo)
가볍게 skip (On my own feet)
So what?!

[Pre-Chorus]
어서 따라와 봐봐
이끌린 대로 don't worry (Hey)
강렬한 shock shock!
벌써 넌 can't stop

[Chorus]
Eh-eh-eh
불꽃으로 날려 burn
Oh-oh-oh
뜨거워진 네 눈빛 (Hey)
가장 높이 빛난 별
잡아 보일게
기다렸다면 끝없이
Follow me

[Refrain]
가시 돋친 게, so what?
얼음 같은 게, so what?
겁이 없는 게 (Bad)
어때서? So what?
(Ba-ba-ba-bad)
Take that, so what?

[Post-Chorus]
I'm so bad (그게 어때?)
I'm so bad (뭐 어때?)
I'm so bad (더 자유롭게)
I'm so bad

[Bridge]
느낌이 와
터질 듯한
심장을 믿고 넌 멈추지 마 (멈추지 마)
Don’t wanna wait no more
온 세상을 누비고 (No-oh)
유난히 더 눈부실 tonight

더 붉게 타올라
원한다면 가져 다
거침없이 빛나니까
Bad, bad, so what? (I'm so bad)
좀 더 벅차올라
가능성을 열어 놔 (Yeah-eah-eah)
절대 널 막을 수 없게 (I'm so bad)
So

[Chorus]
Eh-eh-eh
강렬한 네 맘을 burn (강렬한 네 맘을)
Oh-oh-oh
달아오른 이 느낌 (Hey)
맘을 가둔 유리 벽
넘어 보일게
눈부신 세상 더 멀리
Follow me

[Refrain]
가시 돋친 게, so what?
얼음 같은 게, so what?
겁이 없는 게 (Bad)
어때서? So what?
(Ba-ba-ba-bad)
Take that, so what?

[Outro]
I'm so bad (그게 어때?)
I'm so bad (뭐 어때?)
I'm so bad (더 자유롭게)
I'm so bad