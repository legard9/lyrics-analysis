[Intro: will.i.am]
Love 'til infinity

[Verse 1: LAY]
I thought of the attraction
Even if it don't make sense
Heaven is destruction
Of what I thought I knew back then

[Pre-Chorus: LAY]
I just wanna fight you
When we're standin' face-to-face
Freakin' out ‘cause it's gettin' real
So this is how it feels
Oh-oh

[Chorus: LAY, LAY & will.i.am]
And with every rule you break
Every risk you dare to take
There's one thing I can't ignore
It's that I love you more
It's that I love you more
It's that I love you more
There's one thing I can't ignore
It's that I love you more

[Verse 2: LAY]
Had to be my saviour
Love to hate that you're so right
What's with your bad behaviour?
Is it a mirror of mine?

[Pre-Chorus: LAY, LAY & will.i.am]
I just wanna fight you (Love)
When we're standin' face-to-face
Freakin' out ‘cause it's gettin' real
So this is how it feels
Oh-oh

[Chorus: LAY, LAY & will.i.am]
And with every rule you break
Every risk you dare to take (Every risk you take)
There's one thing I can't ignore (Oh)
It's that I love you more
It's that I love you more
It's that I love you more (Love, love)
There's one thing I can't ignore
It's that I love you more

[Verse 3: will.i.am, LAY]
Girl, I love you 'til infin-infin-infinity
Love you with all my abil-abil-abilities
Love you since you walked into my vicinity
Girl, you number one, you the top pedigree
Can't nobody touch ya, no comparabilities
Me and you, we got that elec-electricity (Zz)
We gon' start a family, really, no fantasy
You plus me is three
One, two, three, start the legacy, uh
Let me love you with infinite possibilities (Oh)
Hand on heart, I hope we learn from our stupidities  (Oh)
I promise I'ma protect all your vulnerabilities (Oh yeah, oh yeah)
Simply put, I love you, multiplicity

[Chorus: LAY, LAY & will.i.am]
And with every rule you break
Every risk you dare to take
There's one thing I can't ignore
It's that I love you more (Love you more)
It's that I love you more (I love you more)
It's that I love you more (That I love you)
There's one thing I can't ignore (No more)
It's that I love you more (Ooh)

[Outro: LAY, will.i.am]
Oh, woah, woah
Oh, woah, woah
Oh yeah, oh yeah
Oh, woah, woah
Love, love 'til infinity
Oh, woah, woah
Oh, woah, woah
I love you more
Oh, woah, woah
I love you more
Oh, woah, woah
I love you more
Love, love, love 'til infinity
Oh, woah, woah