NANNA'S SONG
Ralph McTell
If I take you dancing down the streets to watch you laughing
And stop still in the spring night rain just to watch you smile again
Understand I hold you hand a little tight as if by this
I'll stop the night from running into morning light too soon
Ice cream and candy bars, a Paris moon and Paris stars
Can you count the times that we heard the chimes of Notre Dame
Across the Seine to remind us sadly once again
Time just like the river was swiftly passing by
Just a few reminders of the little things that bind us
Do they make you sad or make you glad to think about the times we had
In my dreams you're dancing in the embers of the evening
And I'll hold your hand a little tight just to stop this special night
From running into morning light too soon