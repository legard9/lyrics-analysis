[Verse 1: Alicia Keys]
Yeah, well
I never thought I'd find it
But it's always been clear
All this time that I've been tryin'
You were standin' right here
I never thought I'd go far
And all this time I've been fightin'
'Cause everything was so hard
You didn't give up on me, baby, no

[Pre-Chorus: Alicia Keys]
I know you got me
There's nothin' like your love
You know I got you

[Chorus: Alicia Keys]
And if I could stay, I'll stay here forever
And if I could be here, we'll be here together
'Cause did you know that you save me?
Did you know that you save me from the fall?
Did you know that you save me?
'Cause we've been through it all
And I owe it all to you, I owe it all
I owe it all to you, I owe you all

[Post-Chorus: Alicia Keys & Snoh Aalegra]
I know that you rock with me
You know I'll be rockin' with you
Know that you rock with me
You know I'll be rockin' with you

[Verse 2: Snoh Aalegra]
Needed to be reminded
'Cause it wasn't all clear
No reason to be hidin'
'Cause you're standin' right here
I just wanna go far
All this time I've been fightin'
'Cause everything is so hard
But you won't give up on me, hey, yeah

[Pre-Chorus: Snoh Aalegra]
I know you got me
And there's nothin' like your love
You know I got you

[Chorus: Snoh Aalegra]
And if I could stay, I'll stay here forever
And if I could be here, we'll be here together
'Cause did you know that you save me?
Did you know that you save me from the fall?
Did you know that you save me?
'Cause we've been through it all
And I owe it all to you, I owe it all
And I owe it all to you, I owe you all

[Post-Chorus: Alicia Keys & Snoh Aalegra]
I know that you rock with me
You know I'll be rockin' with you
Know that you rock with me
You know I'll be rockin' with you
I know that you rock with me
You know I'll be rockin' with you
Know that you rock with me
You know I'll be rockin' with you