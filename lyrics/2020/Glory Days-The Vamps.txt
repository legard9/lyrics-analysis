[Intro]
These are our glory days (These are our glory days)

[Verse 1]
I don't wanna jinx this thing
But I think we got somethin'
Swimmin' in our feelings, starin' at the ceiling
I don't wanna take this slow
Or wait 'til the fire grows cold
We should turn the lights down
Focus on the right now

[Pre-Chorus]
Maybe we don't need to be looking forward, no
Looking forward
Baby, let's not wait for what's 'round the corner
Around the corner

[Chorus]
These are our, these are our glory days
These are our, these are our glory days
I just wanna lie back in your room
While I'm gettin' high off your fumes
And I'm thinkin' these are our, these are our
These are our, these are our, these are our glory days

[Verse 2]
I just wanna take some time to say
You should take a minute out today
Go and put your phone on airplane
Can we connect and communicate?
I'm sick of stackin' shelves inside my mind
I'mma take a mount and realign
Took a little while to realize
These are the best days of our lives

[Pre-Chorus]
Maybe we don't need to be looking forward, no
Looking forward
Baby, let's not wait for what's 'round the corner
Around the corner

[Chorus]
These are our, these are our glory days
These are our, these are our glory days
I just wanna lie back in your room
While I'm gettin' high off your fumes
And I'm thinkin' these are our, these are our
These are our, these are our, these are our glory days
These are our, these are our
These are our, these are our, these are our glory days
These are our, these are our
These are our, these are our, these are our glory days

[Bridge]
Keep on breathin', just keep on breathin'
Keep on breathin', just keep on breathin'
It feels like, oh

[Chorus]
These are our, these are our glory days
These are our, these are our glory days
I just wanna lie back in your room
While I'm gettin' high off your fumes
And I'm thinkin' these are our, these are our
These are our, these are our, these are our glory days (Woah, oh)
These are our, these are our
These are our, these are our, these are our glory days (Woah, oh)
These are our, these are our
These are our, these are our, these are our glory days