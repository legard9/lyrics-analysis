Scene:	The Caltech cafeteria.
SHELDON:	Here’s the problem with teleportation.
LEONARD:	Lay it on me.
SHELDON:	Assuming the device could be invented which would identify the quantum state of matter of an individual in one location, and transmit that pattern to a distant location for reassembly, you would not have actually transported the individual. You would have destroyed him in one location, and recreated him in another.
LEONARD:	How about that.
SHELDON:	Personally, I would never use a transporter. Because the original Sheldon would have to be disintegrated in order to create a new Sheldon.
LEONARD:	Would the new Sheldon be in any way an improvement on the old Sheldon?
SHELDON:	No, he would be exactly the same.
Leonard	That is a problem.
SHELDON:	So, you see it too.
GABLEHOUSER:	(Arriving) Dr Hofstadter, Dr Cooper.
TOGETHER:	Gablehouser.
GABLEHOUSER:	Gentlemen, I’d like you to meet Dennis Kim. Dennis is a highly sought after Doctorial candidate and we’re hoping to have him do his graduate work here.
Leonard	Graduate work, very impressive.
GABLEHOUSER:	And he’s only fifteen years old.
SHELDON:	Not bad, I myself started graduate school at fourteen.
DENNIS:	Well, I lost a year while my family was tunnelling out of North Korea.
LEONARD:	Advantage Kim.
GABLEHOUSER:	I thought maybe you boys could show Dennis around, let him see why we’re the best physics research facility in the country.
DENNIS:	I already know you’re not. You don’t have an open science grid computer, or a free electron laser, and the string theory research being done here is nothing but a dead end.
SHELDON:	Excuse me, that is my research, and it is by no means a dead end.
DENNIS:	Well, obviously you don’t see it yet, but trust me, you will.
GABLEHOUSER:	Dennis, we discussed this, we’re in the process of updating our equipment, and we welcome your input on our research goals, and we’ve agreed to look the other way if you want to use up to 20% of the grant money you attract to smuggle your grandfather out of Pyongyang. We want him here boys, make it happen.
LEONARD:	Yes sir.
SHELDON:  You can count on us, we’re on it. What the hell do you mean, dead end.
DENNIS:	I mean, the whole landscape of false vacuums in string theory could be as large as ten to the five-hundredth power. In addition… ooh, look, chocolate milk.
SHELDON: I sense a disturbance in the force.
Leonard	(In a Yoda voice) A bad feeling I have about this, mmm-hmmm.
Credits sequence
Scene:	A corridor.
LEONARD:	So, Dennis, how long have you been in America.
DENNIS:	A year and a half.
LEONARD:	No kidding, you speak English really well.
DENNIS:	So do you. Except for your tendency to end sentences with prepositions.
LEONARD:	What are you talking about?
DENNIS:	That.
SHELDON:	He’s not wrong. Alright, and this is my office.
DENNIS:	Is this part of the tour?
SHELDON:	Nope. Goodbye.
LEONARD:	Come on, Sheldon, we’ve hardly shown him anything.
SHELDON: Oh, alright, this is my desk, these are my books, this is my door, please close it behind you. Goodbye.
DENNIS:	Looks like you’re doing work in quantum loop corrections.
SHELDON:	Keen observation, goodbye.
DENNIS:	You see where you went wrong, don’t you?
SHELDON:	Leonard.
LEONARD:	Huh, yeah?
SHELDON:	Get him out.
LEONARD:	Come on, Dennis, I’ll show you the rec center, they’ve got nautilus equipment.
DENNIS:	Do I look like I lift weights.
LEONARD:	Not heavy ones.
DENNIS:	It’s startling to me you haven’t considered a Lorentz invariant field theory approach.
SHELDON:	You think I haven’t considered it? You really think I haven’t considered it?
DENNIS:	Have you considered it?
SHELDON:	Get him out Leonard.
LEONARD:	Come on, Dennis, I’ll show you the radiation lab.
DENNIS:	Wow, you won the Stephenson award.
SHELDON:	Yes, in fact I am the youngest person ever to win it.
DENNIS:	Really, how old?
SHELDON:	Fourteen and a half.
DENNIS:	You were the youngest person ever to win it.
LEONARD:	It’s like looking into an obnoxious little mirror, isn’t it?
Scene	The living room of the apartment.
PENNY	
(To Raj)
 Mmm, this is really delicious, isn’t it? 
(Raj looks uncomfortable, then nods.)
 Still can’t talk to me unless you’re drunk, huh? 
(Shakes head)
 Oh, sweetie, you are so damaged.
HOWARD:	Hey, I’m damaged too. How about a hug for Howie?
PENNY:	Sure. Raj, hug Howard.
SHELDON:	
(Dramatically)
 Uh-uh-uh.
LEONARD:	Something you’d like to share? A tale of woe perhaps.
SHELDON:	Fifteen years old. Dennis Kim is fifteen years old, and he’s already correcting my work. Today I went from being Wolfgang Amadeus Mozart to… you know, that other guy.
HOWARD:	Antonio Salieri?
SHELDON:	Oh, God, now even you’re smarter than me.
HOWARD:	You know, Sheldon, you don’t have so many friends that you can afford to start insulting them.
LEONARD:	Just eat, Sheldon, you’ll feel better.
SHELDON:	Why waste food. In Texas when a cow goes dry they don’t keep feeding it, they just take her out and shoot her between the eyes.
PENNY:	I’m confused, did Sheldon stop giving milk?
LEONARD:	You can’t let this kid get to you. You always knew that someday someone would come along who was younger and smarter.
SHELDON: Yes, but I assumed I would have been dead hundreds of years, and that there would be an asterisk by his name because he would be a cyborg.
PENNY:	So, you’ve got a bit of competition, I really don’t see what the big deal is.
SHELDON:	Well of course you don’t, you’ve never excelled at anything.
PENNY:	I don’t understand, exactly how did he get any friends in the first place?
HOWARD:	We liked Leonard.
LEONARD	Well, what are you going to do, Sheldon, give up?
SHELDON:	Yes. That’s what a rational person does when his entire life’s work is invalidated by a post-pubescent Asian wunderkind. He ceases his fruitless efforts, he donates his body to scientific research, and he waits to die.
PENNY:	You know, I’m confused again, is he waiting, or do we get to shoot him between the eyes?
Scene:	The same, later that night
SHELDON:	Hey.
Leonard	Hey.
SHELDON:	I’ve decided you’re right. My career is not over.
Leonard	Great.
SHELDON:	But, since the arrival of Dennis Kim has rendered my research pointless, I just have to find something else to focus on.
Leonard	Great.
SHELDON:	So I’ve decided, I’m going to collaborate with you.
Leonard	Great.
SHELDON:	What exactly is it you do? I know you chatter on about it all the time, but I’ve never really paid attention.
Leonard	Okay, well, right now I’m designing an experiment to study the soft component of cosmic radiation at sea-level, but I really don’t need any help.
SHELDON:	Oh, sure you do. Now, see, what’s this here in the schematic, is that a laser array?
Leonard	Yes.
SHELDON:	No. Hmmm. What happens if you use argon lasers instead of helium neon?
Leonard	It would blow up.
SHELDON:	Are you sure?
Leonard	Pretty sure.
SHELDON:	Pretty sure’s not very scientific, is this how you normally work, just hunches and guesses and stuff?
Leonard	Okay, Sheldon, I understand that you’re going through a bit of a career crisis, you’re searching for some other area where you can feel valuable and productive but I need to tell you something and I want you to listen carefully.
SHELDON:	Alright.
Leonard	Go away.
Sheldon	If you’re concerned about sharing credit with me, your name can go first… I’m going.
Scene	Howard’s lab.
Howard	(Into phone) It’s a small brown paper bag, Ma, I’m looking at it right now. (Pause.) Why would I make that up, there’s no ding-dong in it. (Pause.) How are two ding-dongs tomorrow going to help me today?
SHELDON:	(Entering) So, this is engineering, huh?
Howard	(Into phone) I’ll talk to you later.
SHELDON:	Engineering. Where the noble semi-skilled laborers execute the vision of those who think and dream. Hello, oompah-loompahs of science.
Howard	Sheldon, what are you doing here?
SHELDON:	I just came by to say hello.
Howard	I’ve been at this lab for three years, you’ve never came by to say hello.
SHELDON:	Well, up until now I’ve had better things to do. So, what are we making today?
Howard	A small payload support structure for a European science experimental package that’s going up on the next space shuttle.
SHELDON:	Really, how does it work?
Howard	When this is done, it will be attached to the payload bay, and the sensor apparatus will rest on it.
SHELDON:	Uh, huh. So it’s a shelf?
Howard	No, you don’t understand, during acceleration it needs to stay perfectly level and provide… yeah, okay, it’s a shelf.
SHELDON:	Now, I notice you’re using titanium, did you give any consideration to carbon nanotubes, they’re lighter, cheaper and half twice the tensile strength.
Howard	Sheldon, there’s a diploma in my office that says I have a masters in engineering.
SHELDON:	And you also have a note from your mother that says I love you, bubbula. But neither of those is a cogent argument for titanium over nanotubes.
Howard	Sheldon.
SHELDON:	Yes.
Howard	Go away.
SHELDON:	Did Leonard tell you to say that?
Howard	No, I thought of it all by myself.
SHELDON:	Huh. It can’t be a coincidence. There must be some causal link I’m missing.
Scene	Raj is exiting his office.
Raj	Go away. (Sheldon exits)
SHELDON:	Curiouser and curiouser.
Scene	Apartment 4A
Howard	(Entering) Is he here?
Leonard	If he were, I wouldn’t be.
Raj	Do you know what he did. He watched me work for ten minutes, and then started to design a simple piece of software that could replace me.
Leonard	Is that even possible?
Raj	As it turns out, yes.
Howard	Something’s got to be done about him, Leonard.
Leonard	Like what? He’ll never be able to cope with the fact that some fifteen year-old kid is smarter and more accomplished than he is.
Raj	Well, what if something were to happen to this boy so he was no longer a threat to Sheldon?
Howard	Then our problem would be solved.
Leonard	Hang on, are we talking about murdering Dennis Kim? I’m not saying no.
Howard	We don’t have to go that far, there are other means available.
Raj	We can’t send him back to North Korea. He knows how to get out.
Howard	The only thing we need to do is make this Kim kid lose his focus.
Leonard	That won’t happen, he’s not interested in anything but physics.
Howard	What about biology?
Leonard	What?
Howard	You know, biology? The one thing that can completely derail a world class mind.
Leonard	Howard, he’s fifteen.
Howard	Yeah, so, when I was fifteen I met Denise Polmerry and my grade point average fell from a 5.0 to a 1.8.
Raj	She was sleeping with you?
Howard	No, I just wasted a lot of time thinking about what it would be like if she did.
SHELDON:	(Entering) Oh, good, you’re all here. Look, I’ve decided that if the three of you drop whatever it is you’re working on and join me, we could lick cold fusion in less than a decade, twelve years tops. (They stare at him.) Go away? (They nod) Hmm. Could it be me?
Scene	Outside Apartment 4B.
Penny	(opening door) Oh, hey guys, what’s up?
Howard	We need a hot fifteen year-old Asian girl with a thing for smart guys.
Penny	What?
Leonard	Howard, that’s racist, any fifteen year-old girl will do the trick. (Penny slams door.)
Raj	It’s possible she may have misunderstood us.
Scene	Sheldon's office. He is making measurements on maps. There is a knock on the door.
Gablehouser	(Entering) Dr Cooper? Oh, are we interrupting?
SHELDON:	No, no, please, come in. Yeah, I think you’ll appreciate this, very exciting.
Gablehouser	Oh, what are you working on?
SHELDON:	Something remarkable. Since my prospects for the Nobel Prize in physics have disappeared, thank you very much, I’ve decided to refocus my efforts and use my people skills to win the Nobel Peace Prize. Look, I’m going to solve the Middle-East Crisis by building an exact replica of Jerusalem in the middle of the Mexican desert.
Gablehouser	To what end?
SHELDON:	You know, it’s like the baseball movie, build it and they will come.
Gablehouser	Who will come?
SHELDON:	The Jewish people.
Gablehouser	What if they don’t come.
SHELDON:	We’ll make it nice, put out a spread.
Gablehouser	Okay, well, um, speaking of spreads, we’re having a small welcoming party this afternoon for Mr Kim, who’s agreed to join us here at the University.
SHELDON:	Of course he has, the oracle told us little Neo was the one. You can see the Matrix, can’t you.
Gablehouser	Okay, well, uh, obviously you’re very busy with your… uh, um, come Dennis. You’ll have to excuse Dr Cooper, he’s been under a lot of… um… he’s nuts.
SHELDON:	(Voice off, sings to a Mexican tune) Ah, la-la-la, Hava Nagila. They’ll come, they’ll settle and I’ll win the prize…
Scene	The welcoming party
SHELDON:	I really don’t understand your objections, Professor Goldfarb, why wouldn’t the Senoran Desert make a perfectly good promised land?
Goldfarb	Go away.
SHELDON:	We could call it Nuevo Cherusalem.
Goldfarb	Please go away.
SHELDON:	Said Pharoah to Moses.
Gablehouser	Why are all these young women here?
Leonard	It’s take your daughter to work day.
Gablehouser	Really, I was not aware of that.
Raj	Oh, yes. There was a very official email that was sent to everyone whose insurance files indicated they had daughters between the ages of 14 and 16.
Gablehouser	Hm?
Howard	Smooth.
Raj	Thank you.
Gablehouser	There’s the man of the hour.
Leonard	Okay, so we now have a socially awkward genius in a room full of attractive age-appropriate women.
Howard	All he has to do now is hook up with one of them. (The look at Dennis, who is picking his ear.)
Leonard	Does anyone else see the flaw in this plan?
Raj	We need a social catalyst.
Leonard	Like what? We can’t get fifteen year-old girls drunk.
Howard	Or can we?
Leonard	No, we can’t.
Howard	I don’t think you mean we can’t. I think you mean we shouldn’t.
SHELDON:	Hey, Howard. You’re a Jew. If there was another wailing wall, exactly like the one in Jerusalem, but close to taco stands and cheap prescription drugs, would you still be able to wail at it? Okay, it’s definitely me.
Leonard	Okay, we cannot leave this to chance, lets pick a girl, and figure out how to get her together with Dennis.
Raj	Okay. How about that one.
Howard	Uh-uh. I know the type, cheerleader, student council, goes out with jocks, won’t even look at anybody in the gifted program. And if, after two years of begging, she does agree to go out with you, it turns out to be a set-up and you’re in the back seat of your mom’s car with your pants off while the whole football team laughs at you.
Raj	Are you crying?
Howard	No, I have allergies.
Raj	Okay, uh, how about her?
Leonard	Sure. If he wants to spend a couple of years doing her homework while she drinks herself into a stupor with non-fat White Russians, while you’re the one holding her head out of the toilet while she’s puking and telling you she wishes more guys were like you, and they she gets into Cornell because you wrote her essay for her, and you drive up to visit her one weekend and she acts like she doesn’t even know you.
Raj	Okay, so not her either. How about her?
Howard	Interesting, kind of pretty, a little chubby so probably low self-esteem.
Leonard	I think that’s our girl. One of us should go talk to her.
Raj	I can’t talk to her, you do it.
Leonard	I can’t just go up and talk to her. Howard, you talk to her.
Howard	Oh no, she’ll never go for the kid once she gets a peek at this.
Raj	You know, if we were in India this would be simpler. Five minutes with her dad, twenty goats and a laptop and we’d be done.
Leonard	Well, we’re not in India.
Raj	Alright, why don’t we do it your way then? We’ll arrange for this girl to move in across the hall from Dennis so he can pathetically moon over her for months on end.
Leonard	Okay, that was uncalled for.
Raj	You started it, dude.
Gablehouser	Could I have everyone’s attention please. What a wonderful occasion this is. And how fortunate that it should happen to fall on take your daughter to work day. We’re here to welcome Mr Dennis Kim to our little family.
Sheldon	(Sarcastically) Welcome Dennis Kim.
Gablehouser	Mr Kim was not only the valedictorian at Stamford University, he is also the youngest recipient of the prestigious Stephenson Award.
SHELDON:	Youngest till the cyborgs rise up!
Gablehouser	And now, without any further ado, let me introduce the man of the hour, Mr Dennis Kim. Dennis! Dennis!
Dennis	What?
Gablehouser	Would you like to tell us a little bit about your upcoming research.
Dennis	Um, no thanks. I’m going to the mall with Emma.
Gablehouser	Well, uh, well, uh….
Leonard	The kid got a girl.
Raj	Unbelievable.
Howard	Did anyone see how he did it?
SHELDON:	(To Gablehouser) Don’t worry, I’ve got this. Ladies and Gentlemen, honored daughters. While Mr Kim, by virtue of his youth and naivety, has fallen prey to the inexplicable need for human contact, let me step in and assure you that my research will go on uninterrupted, and that social relationships will continue to baffle and repulse me. Thank you.
Howard	He’s back.
Leonard	Yeah, mission accomplished.
Raj	Forget the mission, how did that little yutz get a girl on his own?
Howard	I guess times have changed since we were young. Smart is the new sexy.
Leonard	Well, why do we go home alone every night, we’re still smart.
Raj	Maybe we’re too smart. So smart it’s offputting.
Howard	Yeah, let’s go with that.
Scene	The park. The four guys are carrying remote control rockets.
Howard	Unbelievable. Components I built are on the International Space Station, and I get a ticket for launching a model rocket in the park.
Leonard	I don’t know if the ticket was so much for the launch as it was for you telling the policewoman “you have to frisk me, I have a rocket in my pants.”
Raj	Hey, look at that. (There is a group of youngsters on the grass, laying about. One is playing a guitar. Dennis Kim is among them. He is drinking something from a bottle in a brown paper bag.) It’s Dennis Kim.
Howard	Wow, I almost didn’t recognize him.
Raj	You know, I kind of feel bad about what we did to him. (Dennis is now snogging the face off Emma)
Leonard	Yeah, we really ruined his life.
SHELDON:	Screw him, he was weak.