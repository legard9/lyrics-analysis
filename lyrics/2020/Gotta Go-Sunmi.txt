[Verse 1]
그만 울어대 boy (Ring, ring)
지겹지도 않니 넌
뭐가 불만인데?
네가 나한테 뭔데?
말하려는 게 뭔데?
뭘 말하고 싶은데?
그럼 뭐 어쩔 건데?
난 내 갈 길 갈게

[Pre-Chorus]
Don't make me ring in the night
I got no time to hang up
대답 없는 날 붙잡을 생각은 하지 마
지금은 전화를 받을 수 없어
I'm sorry, oh no

[Chorus]
(Gotta, gotta, gotta go)
I gotta, gotta, gotta go
I gotta, gotta, gotta go

[Post-Chorus]
난 나빠 everyday
넌 아파 매일매일
나는 여기까지니까
Don't miss me, boy

[Verse 2]
또 왜 이러나 싶네
속이 타들어 간대
같은 말만 하네
뚜뚜뚜
관심 없어 알아서 해

[Pre-Chorus]
Don't make me ring in the night
I got no time to hang up
대답 없는 날 붙잡을 생각은 하지 마
지금은 전화를 받을 수 없어
I'm sorry, oh no

[Chorus]
(Gotta, gotta, gotta go)
I gotta, gotta, gotta go
I gotta, gotta, gotta go

[Post-Chorus]
난 나빠 everyday
넌 아파 매일매일
나는 여기까지니까
Don't miss me, boy

[Bridge]
I'm so done with your calls
You're not the one that I hold
그만 내려놔
제발 이 손 좀 치워
And I would just say like

[Chorus]
(Gotta, gotta, gotta go)
I gotta, gotta, gotta go
I gotta, gotta, gotta go

[Post-Chorus]
난 나빠 everyday
넌 아파 매일매일
나는 여기까지니까
Don't miss me, boy

[Outro]
I gotta, gotta, gotta go
I gotta, gotta, gotta go