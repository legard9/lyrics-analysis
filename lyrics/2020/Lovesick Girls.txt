[Intro: All]
(Lovesick girls)
(Lovesick girls)

[Verse 1: Jennie, 
All
]
Yeong wonhan bam
Changmun eopsneun bange uril gadun love (
Love
)
What can we say?
Maebeon apado oechineun love (
Love
)

[Verse 2: Lisa]
Dachigo manggajyeodo na
Mwol mitgo beotineun geoya
Eochapi tteonamyeon sangcheotuseongin chaero miwohage doegreol
Kkeutjangeul bogi jeon kkeutnael sun eobseo
Iapeumeul gidarin geotcheoreom

[Pre-Chorus: Jisoo, 
Rosé
]
Ama da jamkkan iljido molla
Urin mueol chajaseo hemaeneun geolkka
But I don't care I'll do it over and over
Nae sesang sogen neoman isseumyeon dwae

[Chorus: All]
We are the lovesick girls
Ne meottaero nae salangeul kkeunnael sun eopseo
We are the lovesick girls
Iapeum eobsin nan amu uimiga eopseo

[Post-Chorus: Rosé & Jennie]
But we were born to be alone
Yeah, we were born to be alone
Yeah, we were born to be alone
But why we still looking for love?

[Verse 3: Lisa, 
Jennie
]
No love letters, no X and O's
No love, never, my exes know
No diamond rings, that set in stone
To the left, better left alone
Didn't wanna be a princess, I'm priceless
A prince not even on my list
Love is a drug that I quit
No doctor could help when I'm lovesick

[Pre-Chorus: Rosé, 
Jisoo
]
Ama da jamkkan iljido molla
Urin mueol chajaseo hemaeneun geolkka
Buranhan nae nunppit soge neol dama
Apeudeorado neoman isseumyeon dwae

[Chorus: All]
We are the lovesick girls
Ne meottaero nae salangeul kkeunnael sun eopseo
We are the lovesick girls
Iapeum eobsin nan amu uimiga eopseo

[Post-Chorus: Rosé & Jennie]
But we were born to be alone
Yeah, we were born to be alone
Yeah, we were born to be alone
But why we still looking for love

[Bridge: Rosé, 
Jisoo
]
Sarangeun slippin' and fallin'
Sarangeun killin' your darlin'
Apeuda amulmyeontto chajaoneun i geop eomneun tteollim
Deulliji anha what you say
Iapeumi nan haengbokae
Nareul bulssanghae haneun nega nae nunen deo bulssanghae

[Chorus: All]
We are the lovesick girls
Ne meottaero nae salangeul kkeunnael sun eopseo
We are the lovesick girls
Iapeumeobsin nan amu uimiga eopseo

[Spoken: Lisa]
One, two

[Outro: All, 
Rosé
, 
Jennie
]
(Lovesick girls) 
Modu gyeolguk tteonagago
(Lovesick girls) 
Nae nunmuri mudyeojyeodo
(Lovesick girls) 
Apeugotto apado
(Lovesick girls)
But we're still looking for love