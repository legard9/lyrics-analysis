Homestuck

A young man stands in his bedroom. It just so happens that today, the 13th of April, 2009, is this young man's birthday. Though it was thirteen years ago he was given life, it is only today he will be given a name!

What will the name of this young man be?

> Enter name.

> Try Again.

> Examine room.

Your name is JOHN. As was previously mentioned it is your BIRTHDAY. A number of CAKES are scattered about your room. You have a variety of INTERESTS. You have a passion for REALLY TERRIBLE MOVIES. You like to program computers but you are NOT VERY GOOD AT IT. You have a fondness for PARANORMAL LORE, and are an aspiring AMATEUR MAGICIAN. You also like to play GAMES sometimes.

What will you do?

> John: Quickly retrieve arms from drawer.

Your ARMS are in your MAGIC CHEST, pooplord!

> Remove CAKE from MAGIC CHEST.

Out of sympathy for John's perceived lack of arms, you pick up the CAKE for him and put it on his BED.

> John: Quickly retrieve arms from MAGIC CHEST.

You retrieve your FAKE ARMS from the chest. You use these for HILARIOUS ANTICS.

You CAPTCHALOGUE them in your SYLLADEX. You have no idea what that actually means though.

There are other items in the chest.

> John: Examine contents of chest.

In here you keep an array of humorous and mystical ARTIFACTS, each one a devastating weapon in the hands of a SKILLED MAGICIAN or a CUNNING PRANKSTER.

You are neither of these things.

Among the ARTIFACTS are: TWO (2) FAKE ARMS [CURRENTLY CAPTCHALOGUED IN YOUR SYLLADEX], ONE (1) PAIR OF TRICK HANDCUFFS, ONE (1) STUNT SWORD, ONE (1) MAGICIAN'S HAT, ONE (1) PAIR OF BEAGLE PUSS GLASSES, SEVERAL (~) SMOKE PELLETS, SEVERAL (~) BLOOD CAPSULES, and ONE (1) COPY OF COLONEL SASSACRE'S DAUNTING TEXT OF MAGICAL FRIVOLITY AND PRACTICAL JAPERY, and ONE (1) COPY OF HARRY ANDERSON'S "WISE GUY", BY MIKE CAVENEY.

Some of this stuff may come in handy at some point. For now, you decide to just take the SMOKE PELLETS.

> John: Captchalogue smoke pellets.

You stow the SMOKE PELLETS on one of your CAPTCHALOGUE CARDS in your SYLLADEX.

You still aren't totally sure what that means, but you are starting to get the hang of the vernacular at least.

You have two empty CAPTCHALOGUE CARDS remaining.

> John: Equip fake arms.

You aren't totally sure if "EQUIP" is a verb copasetic with the abstract behavioral medium in which you dwell, but you give it a try anyway.

Unfortunately, you cannot access the FAKE ARMS! Their card is underneath the one you just used to captchalogue the SMOKE PELLETS. You will have to use the pellets first in order to access the arms. But this is probably unadvisable, since you'd just make your room lousy with smoke!

Your SYLLADEX'S FETCH MODUS is currently dictated by the logic of a STACK DATA STRUCTURE. You were never all that great with data structures and you find the concept puzzling and mildly irritating.

But with any hope, perhaps you will advance new, more practical FETCH MODI for your SYLLADEX with a little more experience.

> John: Examine Problem Sleuth Poster.

Is it even possible to get any more hard boiled than that? You really doubt it. This poster was one of your wisest purchases.

There is a nice spot on the wall next to it. You've been meaning to hang another poster there soon.

> John: Read note on drawer.

This note is rich with the aromas of FATHERLY AFTERSHAVES AND COLOGNES.

Beside the note is a ROLLED UP POSTER.

> John: Take poster.

Another BIRTHDAY ARTIFACT. You wonder what is printed on the poster.

You'll need some way to hang it on your wall.

> John: Acquire hammer and nails. They will come in handy.

You first place the HAMMER into your SYLLADEX.

But now all of your CAPTCHALOGUE CARDS are full. You wonder what will happen if you try to take the NAILS?

You guess it doesn't hurt to try.

> John: Take nails.

You captchalogue FOUR (4) NAILS into the top card, and push all the ARTIFACTS down a card.

The FAKE ARMS are pushed entirely out of the deck!!!

Oh well. They're probably completely useless anyway. But you probably don't want to do that again, unless you want to drop the SMOKE PELLETS and suffer the consequences.

In any case, you now feel like you have gathered enough things to get down to business and do some really important stuff. The next thing you do will probably be exceptionally meaningful.

> John: Squawk like an imbecile and shit on your desk.

This is the dumbest idea you've had in weeks!!!

STUPID STUPID STUPID.

And yet the polished surface of your desk...

It beckons.

> John: Combine the nails and hammer.

You MERGE the top two cards.

The HAMMER and NAILS are now captchalogued on the same card and can be used together.

> John: Use hammer/nails on poster.

You use the HAMMER and NAILS card IN CONJUNCTION with the card beneath it.

> John: Nail poster to wall.

You use the HAMMER, NAILS, and POSTER on the blank space on the wall.

It's glorious. Exactly what you wanted. The old man really came through this time.

> John: Examine Con Air poster.

PUT THE BUNNY BACK IN THE BOX.

I SAID, PUT THE BUNNY BACK IN THE BOX.

WHY COULDN'T YOU PUT THE BUNNY BACK IN THE BOX?

> John: Examine Deep Impact poster.

Morgan Freeman's genteel, homespun mannerisms were perfect qualities for a president residing over a crisis.

OCEANS RISE. CITIES FALL. HOPE SURVIVES.

WOW.

Films about impending apocalypse fascinate you. Plus, a black president??? Now you've seen everything!

> John: Examine calendar.

You've marked your birthday, the 13th of April. Another day you marked was supposed to be the arrival date for the highly touted SBURB BETA LAUNCH.

It's been three days already. It's starting to become a sore subject with you.

> John: Eat cake.

You are sick to death of cake!!! You've been eating it all day. And you have no intention of clogging your SYLLADEX with it either. The CAKE stays put for now.

You hear a notice from your COMPUTER. Someone is messaging you.

> John: Examine incoming message.

You pull up to your COMPUTER. This is where you spend most of your time. You decorated your desktop with some rather handsome WALLPAPER which you made yourself. You are really proud of it.

Your desktop is also littered with various PROGRAMMING PROJECT FILES. You are so bad at programming sometimes you wonder why you even bother with it.

Your PESTERCHUM application is flashing. Someone is trying to get in touch with you.

> John: Open Pesterchum.

Only one of your CHUMS is logged in. He's sent you a message.

> John: Open message.

-- turntechGodhead [TG] began pestering ectoBiologist [EB] at 16:13 --

TG: hey so what sort of insane loot did you rake in today
EB: i got a little monsters poster, it's so awesome. i'm going to watch it again today, the applejuice scene was so funny.
TG: oh hell that is such a coincidence i just found an unopened container of apple juice in my closet it is like fucking christmas up in here
EB: ok thats fine, but i just have one question and then a word of caution. have you ever seen a movie called little monsters starring howie mandel and fred savage?
TG: but
TG: the seal on the bottle is unbroken
TG: are you suggesting someone put piss in my apple juice at the factory
EB: all im saying is don't you think monster howie mandel has the power to do something as simple as reseal a bottle?
EB: try using your brain numbnuts.
TG: why did the fat kid or whoever drank it know what piss tasted like
TG: i mean his reaction was nigh instantaneous
EB: it was the 15th day in a row howie mandel peed in his juice.
TG: ok i can accept that
TG: monster B-list celebrity douchebags are cunning and persistent pranksters
TG: also fred savage has a really punchable face
TG: but who cares about this lets stop talking about it
TG: did you get the beta yet
EB: no.
EB: did you?
TG: man i got two copies already
TG: but i dont care im not going to play it or anything the game sounds boring
TG: did you see how it got slammed in game bro????
EB: game bro is a joke and we both know it.
TG: yeah
TG: why dont you go check your mail maybe its there now
EB: alright.

> John: Look out window.

You see the view of your yard from your window.

Hanging from the tree is your TIRE SWING. In a kid's yard, a tree without a tire swing is like a proper gentleman without a monocle. That is to say, HE CAN HARDLY BE CONSIDERED A TERRIBLY PROPER GENTLEMAN AT ALL.

And there beside your driveway is the mailbox.

> John: Examine mailbox.

The little red arm-swingy-dealy thing or whatever it is called is flipped up!

What the hell is that thing called anyway. You do not have time for these semantics. The red flippy-lever thing means you have new mail. And that means the beta might be here!

> John: Go outside and check mailbox.

You are about to hurry down stairs when you hear a car pull into the driveway. It looks like your DAD has returned from the grocery store.

Oh great. He is beating you to the mail.

> John: Forget it. Check mail later.

If you go down stairs to get it, he will likely monopolize hours of your time. You decide to chill out up here for a while until the dust settles.

Sometimes you feel like you are trapped in this room. Stuck, if you will, in a sense which possibly borders on the titular.

And now your chum is pestering you again. The clockwork of friendship turns ceaselessly, operating the swing-lever dealies of harassment in perpetuity!

Whatever. The dude can just hold his damn horses.

> John: Examine games on CD rack.

You've put countless manhours into this assortment of quality titles.

> John: Read COLONEL SASSACRE'S DAUNTING TEXT.

You decide to consult with the Colonel's bottomless wisdom. Good grief this thing is huge. It could kill a cat if you dropped it.

But to really dig into this hefty book, you will have to captchalogue it. You are not sure you are ready to logjam your other ARTIFACTS beneath it just yet.

> John: Captchalogue fake arms again.

What did you just say?? You don't want to clog up your...

Oh, Jesus. In a momentary lapse of concentration, you accidentally captchalogue the arms again.

> John: Set Pesterchum status to "bully".

You don't think the situation is quite dire enough to go all the way to "RANCOROUS", but you still feel the PESTERCHUM client should reflect your mood change in some way.

"BULLY" will have to do. You guess.

This unsurprisingly does nothing whatsoever.

Oh, right, you forgot your chum is still pestering you.

> John: Answer chum.

TG: is it there
TG: plz say yes
TG: maybe you can play with TT shes been pestering me all day about it
TG: shes mackin on me so hard all the time i start to feel embarrassed for her
TG: i mean not that i can blame her or anything
EB: yes, it is understandable because you are really attractive. i am attracted to you.
TG: thank you
EB: jk haha.
EB: no, i don't have it yet.
EB: my dad has the mail and i guess i have to go get it from him and see if it's there.
EB: and i've been busy spending all afternoon shitting around with my stupid sylladex.
EB: it's so frustrating.
TG: whats your modus
EB: what?
TG: how do you retrieve artifacts from it
EB: oh. like one at a time i guess. and if i put too much in, something falls out.
TG: stack?? hahahahahaha
EB: what is yours?
TG: hash map
TG: my bro taught me a few tricks he basically knows everything and is awesome
EB: what the hell is that?
TG: you should probably brush up on your data structures
EB: i guess.
TG: did you at least allocate your strife specibus
EB: no.
TG: it could free up a card for you
TG: plus let you attack stuff whenever things get too hot to handle
TG: which is never
TG: what have you got
EB: well, i've got a hammer but it's trapped under some arms.
TG: wow you really suck at this dont you
TG: just get rid of the arms and then allocate the hammer to the specibus
EB: how?
TG: i dont know just use the arms on any old thing and see if it works

> John: Combine fake arms with cake.

You stick the FAKE ARMS in the CAKE on your bed.

This definitely makes the CAKE at least 300% more hilarious. You're sure COLONEL SASSACRE would know the precise index of elevated hilarity.

> John: Allocate hammer to strife specibus.

You check the back of your STRIFE SPECIBUS for the KIND ABSTRATUS you have in mind for it.

> John: Select "HAMMER".

Your STRIFE SPECIBUS has been ALLOCATED with the HAMMERKIND ABSTRATUS.

The HAMMER has been moved from your CAPTCHALOGUE DECK to your STRIFE DECK.

> John: Report progress to TG.

EB: ok, i did it.
TG: hammerkind?
EB: yeah.
TG: ok that will be the permanent allocation for your specibus
TG: i guess i should have mentioned that
EB: uh...
TG: hope you like hammers dude!
EB: yeah, that's fine i guess. i can't imagine it's going to be all that relevant.

> John: Captchalogue Colonel's big book.

Now that you've got some space in your SYLLADEX to work with, you figure you might as well start squandering it immediately.

Ordinarily this ridiculous book would be way too heavy to carry around in any practical way. You guess maybe this is one respect in which the cards present some convenience.

> John: Examine GameBro Magazine.

> John: Read article.

> John: Captchalogue GameBro.

It might come in handy if you ever need something that burns easily.

> John: Captchalogue magician's hat.

You expend your final card on the MAGICIAN'S HAT.

> John: Get funny glasses too.

You don't have a free card in your SYLLADEX!

However, you are able to MERGE the BEAGLE PUSS with the MAGICIAN'S HAT to create a CLEVER DISGUISE.

> John: Wear disguise to fool dad.

John? Who is this "John" you speak of? You are quite certain there has never been, nor ever will be...

Yeah, this is a really shitty disguise.

While you are wearing the items, they remain on the card, but it is temporarily removed from the deck, thus freeing up the cards beneath it.

> John: Leave room.

You exit into the HALLWAY.

On one wall hangs a picture of a fella who sure knows how to have a laugh, a man after your own heart. You always thought he looked a lot like Michael Cera. But your DAD swears on the many HALLOWED TOMBS of Egypt that it is not. You're not sure about that though.

On the other wall is one of your DAD'S stupid clowns. Or HARLEQUINS, as he is quick to correct anyone who would venture such brazen assumption.

> John: Go downstairs.

The accursed odor of fresh baking wafts into your newfound nostrils. Something is brewing in the KITCHEN. It must be the connivings of your arch nemesis, BETTY CROCKER, and the rich, buttery aroma of her plot stinks to high heaven.

This mission is going to be more difficult than you imagined.

> John: Admire harlequins.

You check out the shelves of FANCIFUL HARLEQUINS.

Look at this fucking garbage. You hate this stuff. Funny is funny, but your DAD sure can be a real cornball.

Sometimes at night you pray for burglars.

> John: Examine fireplace.

A bright orange flame flickers in the FIREPLACE. It doesn't matter that it's April and not terribly chilly outside. In a home, a FIREPLACE needs a fire, because that's what FIREPLACE is for. A fire BELONGS in a FIREPLACE, dammit, cata(ptcha)gorically, at all times, without exception.

As domestic myth of unaccountable origin holds, a home borrows the spirit of the flame for as long as it makes a guest of it, much as the moon takes liberty with the sun's rays.

"The moon's an arrant thief, and her pale fire she snatches from the sun." -Mark Twain

You are almost certain Mark Twain said that.

> John: Toss GameBro into fire.

It doesn't burn as quickly as you hoped.

Each GAMEBRO MAGAZINE is guaranteed to be printed on 40% recycled asbestos. For big ups to Mother Earth, yo.

> John: Fondly regard cremation.

You examine the SACRED URN containing your departed NANNA'S ASHES.

When your father gives her portrait a wistful glance now and then, you can tell it brings back painful memories. A tall bookshelf. A ladder. An unabridged COLONEL SASSACRE'S.

He never wants to talk about it.

> John: Topple urn.

You clumsily mishandle the SACRED URN. Ash is everywhere.

In retrospect, upon mulling cinematic tropes regarding ash-filled urns, this outcome was a virtual certainty.

You'd probably better clean it up before DAD finds it.

> John: Combine father's pipe with clever disguise.

You think now would be a good time to beef up your CLEVER DISGUISE.

> John: Examine oversized gift.

Contemplating what could be inside this package is sort of exciting, but it makes you a little nervous at the same time.

> John: Open large present.

Oh hell no.

> John: Captchalogue ashes.

First you prop the HARLEQUIN DOLL up on the couch. Having it in the middle of the floor sprawled out all akimbo like that struck you as unseemly.

You captchalogue the ASHES to your available card.

> John: Combine ashes with urn.

You merge the SACRED URN with the ASHES.

Most of the ASH is back in the URN, but it's a total mess. Really it probably would have been tidier if you just used a broom and dustpan.

> John: Put urn back.

No one will be the wiser.

Except maybe for people with eyes.

> John: Go get fake arms again.

You just got another BRILLIANT idea for something to do with those pointless arms. You pry them out of the CAKE and captchalogue them.

Looks like PESTERCHUM is acting up again.