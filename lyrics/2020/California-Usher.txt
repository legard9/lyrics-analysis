[Intro: Usher]
Yeah, ayy, ayy, ayy
They done opened Atlanta
I ain't goin' out, though (Yeah)
Usher, Usher, Usher

[Verse 1: Usher]
It's been a while now, hope you don't hate me
Ain't been on beast mode, I been in the house down south goin' crazy
Ain't been droppin' pins like I used to
Ain't been on savage like I'm used to
Not really tryna say it, but I miss you lately (I miss you)
Wish you was on my time, wish you was by my side (My side)
We be gettin' lit right now, sippin' D'USSÉ right now
Babe, can you forgive me? Stuck in the city
But if we wasn't locked down right now, we be turnin' up right now

[Chorus: Usher]
How they doin' it in California?
Wanna change the scene, lemme come and join ya
Stretched out at the Hotel California
Girl, I'm tryna meet, I'ma pull up on ya
How's the view from California?
I'm on my way to come and join ya
Stretched out at the Hotel California
If you're tryna see me, I'ma pull up on ya

[Verse 2: Tyga]
Ayy, I'ma drop my top and pop the trunk (Pop it)
Gotta watch for the paps and PopAlert (Pop her)
Sit you in a Rolls-Royce, I got the truck (Yuh)
I'm the man of my city, they show me love (Show me love)
Let me speed things up and glitch you up (Uh)
When you see me with my shawty, don't interrupt (Nope)
Palm trees in the back, that's for good luck (Yuh)
Laid 'til the sunset, you tearin' it up (Ha)
Got a question: can I make you mine?
Can I take you courtside to see LeBron?
Can I put you in a condo in the sky?
Can I put your necklace on wintertime
Man, I got a lot ridin' on my line
And I got a lot of money on my mind (Yeah)
Maybe we can go for it, fly you from the south for it
When they open up, I'ma show you how they

[Chorus: Usher & Tyga]
How they doin' in California (Yeah)
Wanna change the scene, lemme come and join ya
Stretched out at the Hotel California
Girl, I'm tryna meet, I'ma pull up on ya (I'ma pull up)

[Bridge: Usher]
Wish you was on my time, wishin' you was by my side (My side)
We be gettin' lit right now, sippin' D'USSÉ right now
Babe, can you forgive me? I'm in between cities (Ooh yeah)
If we wasn't locked down right now, we be lockin' up right now (Aye!)

[Outro: Usher]
Pull up, yeah, yeah, yeah, pull up
How's the view from California?
Pull up, pull up, yeah, gotta pull up, pull up
Pull up, pull up, pull up, I'ma pull up