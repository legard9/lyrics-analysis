[Hook]
Oi oi (Cheeky Nando's)
Medium hot (Cheeky Nando's)
Perinaise sauce (Cheeky Nando's)
Boneless thighs (Cheeky Nando's)
Skinny jeans on (Cheeky Nando's)
Two sides for my wings (Cheeky Nando's)
Peri Peri sauce (Cheeky Nando's)
Out with the boys (Cheeky Nando's)

[Verse 1: Vuj]
If it's not cheeky, you didn't go
Gotta make sure it's a cheeky Nando's
Slicked back hair, Air Max on
Looking real cheeky for my Nando's
Walk up to the till, give me some nuts
Don't want a drink but I'll take a cup
She said "that's for water", yeah alright
I'm a cheeky lad so I fill it with Sprite

[Verse 2: Mim]
I go to Nando's, it's all popped off
Corn on the cob, gets knocked off
When I draw for the olives, I go pop, pop, pop
Hot sauce goes to the top, top, top
My wallet is tight, my girl's not, not, not
I'm Asian, handle it, hot, hot, hot
Get a refill then I bop, bop, bop
I've got two in my hand, I wanna drop, drop drop

[Hook]
Oi oi (Cheeky Nando's)
Medium hot (Cheeky Nando's)
Perinaise sauce (Cheeky Nando's)
Boneless thighs (Cheeky Nando's)
Skinny jeans on (Cheeky Nando's)
Two sides of wings (Cheeky Nando's)
Peri Peri sauce (Cheeky Nando's)
Out with the boys (Cheeky Nando's)

[Verse 3: Mim]
I've got a bottomless drink and a line in my head
I'll take a rice and a garlic bread
Mash potato, you know I'm real
Fino sides for the vegan meal

[Verse 4: Vuj]
Halloumi, come through me, Julie
KFC boys, they wanna try and screw me?
Banter, chicken gets wrapped up
Don't like Nando's? Go home you're an actor

[Bridge: Claira Hermet]
Excuse me, have you been to Nando's before?

[Verse 5: Klayze]
You what? I bring all my birds to this spot
Extra small jeans, plain white top
Don't be shy with the sauces love
Oh hello, you're gorgeous love
Get the perinaise out, sort us a tub
Proper naughty bit of grub
Yeah, everything's fine with my meal
I just sat myself down, chill
I'm sweet for the nuts and the olives
You're hotter than the ten chillis in my wallet
You like that one?
When you're next out, try that one
A bit of banter with James and Steve
But if it ain't cheeky enough, we'll leave

[Hook]
Oi oi (Cheeky Nando's)
Medium hot (Cheeky Nando's)
Perinaise sauce (Cheeky Nando's)
Boneless thighs (Cheeky Nando's)
Skinny jeans on (Cheeky Nando's)
Two sides of wings (Cheeky Nando's)
Peri Peri sauce (Cheeky Nando's)
Out with the boys (Cheeky Nando's)