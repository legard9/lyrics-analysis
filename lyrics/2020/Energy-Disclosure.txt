[Build]
Whaddup, whaddup, whaddup, whaddup?
Alright, look, look, look, look, look
Just in case you ain't got it by now
Listen to me, bring it in
If you are alive
I know you ain't reach your best yet
You got more, you could do more
You could see more, you could be more, alright?
Right now, you should feel invincible
Powerful, strong, look
Where your focus goes, your energy flows
Are you hearin' me?

[Drop]
A-ha
Now we goin' take it to another level
Right now, you should feel invincible
Powerful, strong, look
A-ha

[Build]
A  lot of you, you're not where you wanna be
You thinkin' of that negative stuff
You talkin' negativity
You in that negative zone
I need you to do me a favor
I need you to know that
In order for you to get on that next level
The one thing you need to do
To go where you've never gone before
Is to change the way you think
Where your focus goes, your energy flows
Are you hearin' me?

[Drop]
Right now, you should feel invincible
Powerful, strong, look
A-ha