[Verse 1]
Typical you, always wanting what you can't have
Soon as I moved on, started missing me so bad
Saying maybe we should try again, try to get it right again
Saying do we have to be done?
Don't know why you said goodbye then
And honestly I get where you're coming from 'cause

[Chorus]
If I were you, I'd miss my kiss
I'd miss me keeping you up all night
If I were you, I know that losing me would make me lose my mind
You ain't gonna find nobody like me
I'm the best you ever had and I'm always gonna be, so
If I were you. if I were you
I'd want me back too

[Verse 2]
Yeah, I'd wanna go back to that weekend out in LA
Bet that hotel bed ain't never been the same, babe
Dammit, we were so good then
Think of what we could've been
If you'da just tried
'Cause I think that I'm worth it
I thought you were perfect
But you had your time

[Chorus]
If I were you, I'd miss my kiss
I'd miss me keeping you up all night
If I were you, I know that losing me would make me lose my mind
You ain't gonna find nobody like me
I'm the best you ever had and I'm always gonna be, so
If I were you, if I were you
I'd want me back too

[Bridge]
Typical you, always wanting what you can't have

[Chorus]
If I were you, I'd miss my kiss
I'd miss me keeping you up all night
If I were you, I know that losing me would make me lose my mind
You ain't gonna find nobody like me
I'm the best you ever had and I'm always gonna be, so
If I were you, if I were you
I'd want me back too
I'd want me back too