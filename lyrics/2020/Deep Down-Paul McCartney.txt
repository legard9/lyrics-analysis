[Chorus 1]
Gonna get deep down
Wanna do it right
Wanna get deep down
And look around this end of town tonight

[Chorus 2]
Wanna get you up
Gonna take a bite
Gotta let me know
If we can throw a party every night

[Chorus 1]
Wanna get deep down
Wanna do it right
Wanna get deep down
And look around this end of town tonight

[Chorus 2]
I wanna pick you up
Gonna get a bite
Gonna let me know
And we can throw a party every night

[Chorus 1]
Gonna get deep down
Wanna do it right
Wanna get deep down
And look around this part of town tonight

[Chorus 3]
Wanna get deep down
Wanna get deep down
Wanna get deep down
Wanna get deep down

[Chorus 1]
Wanna get deep down
I wanna do it right
I wanna get deep down
And look around this end of town tonight

[Chorus 1]
Wanna get deep down
I wanna do it right
I wanna get deep down
And look around this end of town tonight

[Chorus 2]
I wanna pick you up
Gonna get a bite
Gonna let me know
And we can throw a party every night

[Chorus 1]
Gonna get deep down
Wanna do it right
I wanna get deep down
And look around this part of town tonight

[Chorus 2]
I wanna get you up
I wanna have a bite
I wanna let you know
That we can throw a party every night

[Chorus 1]
Wanna get deep down
Wanna do it right
Wanna get deep down
And look around this end of town tonight

[Chorus 1]
I wanna get deep down
Wanna do it right
I wanna get deep down
And look around this end of town tonight

[Chorus 3]
Wanna get deep down
Wanna get deep down
Wanna get deep down
Wanna get deep down

[Bridge]
I need to talk to you
I wanna talk to you
Come let me be with you tonight
I need to talk to you
I wanna talk to you
Come let me be with you tonight

[Chorus 1]
Wanna get deep down
Wanna do it right
Wanna get deep down
And look around this end of town tonight

[Chorus 2]
Wanna get you up (I need to talk to you)
I wanna get a bite (I wanna talk to you)
Gonna let me know (Come let me be with you)
If we can throw a party every night (Tonight)

[Chorus 2]
I wanna pick you up
I wanna get it right
Gonna let me know
And we can throw a party every night (Yeah, yeah, yeah)

[Chorus 1]
Gonna get deep down
Gonna do it right
I wanna get deep down
And look around this part of town tonight (Yeah)

[Chorus 1]
Get deep down (Get deep down)
Do it right (Wanna do it right)
Get deep down
Look around end of town tonight (Oh yeah)

[Chorus 3]
Get deep down
Get deep down
Get deep down
Get deep down

[Outro]
Get deep down
Get deep down
Get deep down
Get deep down (With you)
Down, down, down, down, down
I wanna get deep down
I wanna get deep down tonight, yeah yeah
I need to talk to you
I wanna talk to you
Come let me be with you tonight, tonight
Tonight