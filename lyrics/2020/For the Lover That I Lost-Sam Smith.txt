[Verse 1]
Think about your lips and the way they kiss
There's so much I really miss about you
Sitting on the beach, you were still in reach
And I haven't felt free without you

[Pre-Chorus]
All of the memories feel like magic
All of the fighting seems so sweet
All that we were, my love, was tragic
And you're the last thing that I need

[Chorus]
So I lay a dozen roses
For the lover that I've lost
I stand by all my choices
Even though I paid the cost
Oh, all those nights, the lows and highs
I share them all with you
So I lay a dozen roses
I lay them there, I lay them there for you, mmm

[Verse 2]
You've been on my mind every single night
I can't visualize life without you
I've been tryin' to go a week without losing sleep
But there's something that I need to go through

[Pre-Chorus]
All of the memories feel like magic
All of the fighting seems so sweet
All that we were, my love, was tragic
And you're the last thing that I need

[Chorus]
So I lay a dozen roses
For the lover that I've lost
I stand by all my choices
Even though I paid the cost
All those nights, the lows and highs
I share them all with you
So I lay a dozen roses
I lay them there, I lay them there for you

[Post-Chorus]
Oh-oh-oh
Oh-oh-oh

[Chorus]
So I lay a dozen roses
For the lover that I've lost
I stand by all my choices
Even though I paid the cost
All those nights, the lows and highs
I share them all with you
So I lay a dozen roses
I lay them there, I lay them there for you