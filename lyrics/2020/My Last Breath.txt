Hangul

[Verse 1]
긴 꿈을 꾸다 깨어난 걸까
내가 널 알아본 순간
이름도 없던 내 사랑은
시작된 건지도 몰라
널 알기 전 저 하늘도
늘 이렇게 눈부셨던 걸까

[Chorus]
만에 하나 우리 사랑이 된다면
만에 하나 너와 시작될 수 있다면
아껴 왔던 마음과
끝을 모를 사랑을 널 위해 쓸 거야
내 모든 순간을 다 너에게

[Verse 2]
난 너와 보고 또 너와 듣고
너와 숨 쉬는 모든 게
버릴 거 없이 행복해서
네 옆에 멈추고 싶어
널 알기 전 흐릿했던
내 모든 게 기억나질 않아

[Chorus]
만에 하나 우리 사랑이 된다면
만에 하나 너와 시작될 수 있다면
아껴 왔던 마음과
끝을 모를 사랑을 널 위해 쓸 거야
내 모든 순간을 다 너에게

[Verse 3]
기억해 기억해
이곳에 있을게
영원히 영원히
이렇게 네 곁에

[Outro]
단 하루만 내게 허락된다 해도
단 하루만 너와 사랑할 수 있다면
잊지 못할 오늘과
변치 않을 기억들
내가 가진 사랑을 모두 다
널 위해 쓸 거야
마지막 숨까지 다 너에게
Romanization
Gin kkumeul kkuda
Kkaeeonan geolkka
Naega neol arabon sungan
Ireumdo eopsdeon nae sarangeun
Sijakdoen geonjido molla
Neol algi jeon jeo haneuldo
Neul ireohge nunbusyeossdeon geolkka
Mane hana
Uri sarangi doendamyeon
Mane hana
Neowa sijakdoel su issdamyeon
Akkyeo wassdeon maeumgwa
Kkeuteul moreul sarangeul
Neol wihae sseul geoya
Nae modeun sunganeul da
Neoege
Nan neowa bogo
Tto neowa deutgo
Neowa sum swineun modeun ge
Beoril geo eopsi haengbokhaeseo
Ne yeope meomchugo sipeo
Neol algi jeon heurishaessdeon
Nae modeun ge gieoknajil anha
Mane hana
Uri sarangi doendamyeon
Mane hana
Neowa sijakdoel su issdamyeon
Akkyeo wassdeon maeumgwa
Kkeuteul moreul sarangeul
Neol wihae sseul geoya
Nae modeun sunganeul da
Neoege
Gieokhae gieokhae
Igose isseulge
Yeongwonhi yeongwonhi
Ireohge ne gyeote
Dan haruman
Naege heorakdoenda haedo
Dan haruman
Neowa saranghal su issdamyeon
Ijji moshal oneulgwa
Byeonchi anheul gieokdeul
Naega gajin sarangeul modu da
Neol wihae sseul geoya
Majimak sumkkaji da
Neoege
English Translation
Have I woken up from a long dream?
The moment I recognized you
My love that was once nameless
Might have then started
Before I knew you
Was the sky this dazzling too?
If our love is possible
If I can start with you
My feelings that I saved up
The love without an end
I will use for you
All of my moments
For you
With you, I see
With you, I hear
With you, I breathe
Everything makes me so happy
I want to stay next to you
Before I knew you
I don’t remember all the blurry things about me
If our love is possible
If I can start with you
My feelings that I saved up
The love without an end
I will use for you
All of my moments
For you
Remember, remember
I’ll be here
Forever, forever
By your side
If I am allowed only one day
If I can love you for only one day
With the unforgettable today
And memories that will never change
All of my love
All of it
I will use for you
Even my last breath
For you