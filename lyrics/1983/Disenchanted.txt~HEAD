Parts I and II.
The readings listed in the historical sections combine essential texts by canonical writers and secondary readings pertinent to each writer's work. The list of canonical works reflects the faculty's judgment of the works a student should command bythe time of the general examination; it does not aspire to be comprehensive. The secondary readings aim to illustrate an array of perspectives in the recent scholarly literature. While no particular secondary readings are specifically required, it will be difficult to demonstrate knowledge of the primary thinkers listed without some appreciation of the major controversies about their works.
Part III.
The readings listed are intended to represent a range of views and approaches to several basic concepts and normative doctrines found in contemporary political theory. Again, the list does not aim to be comprehensive. Although students are not expected to be conversant with all of the works listed, they should be familiar with the leading ideas and concerns in the contemporary literature under most of the subheadings.
Appendix. In addition to studying the thinkers and concepts listed in the three main portions of the reading list students may find it helpful to do some reading about general issues of methodology in the study of political thought. The works listed in the Appendix represent several perspectives.
I. Ancient and medieval political theory
Thucydides, 
The Peloponnesian War
,
 I; II; 1-50, 70-86; V, 84-1 15; VI [entire; not 1-32 only]; VII, 60-87
- W. Robert Connor, 
Thucydides
- Steven Forde, 
The Ambition to Rude
- Raymond Geuss, 
Outside Ethics
, ch.13
- Gerald M. Mara, 
The Civic Conversations of Thucydides and Plato: classical political philosophy and the limits of democracy
- S. Sara Monoson, 
Plato's Democratic Entanglements
, ch.3
- Josiah Ober, 
Political Dissent in Democratic Athens
, ch. 2
**Plato, 
Apology
;
 
Crito
;
 
Republic
;
 
Statesman
;
 
Laws
, Stephanus sections 624-632, 641-650, 659-664, 690-695, 699-702, 704-705, 709-747, 752-780, 853-858, 861-864, 875, 961-969
- Julia Annas, 
Introduction to Plato's Republic
- Danielle Allen, 
Why Plato Wrote
- Allan Bloom, 
‘Interpretive Essay’ in The Republic of Plato
, ed. Bloom
- Christopher Bobonich, 
Plato’s Utopia Recast
, ch. 5
- J. Peter Euben, 
The Tragedy of Political Theory
, chs 7, 8
- Terence Irwin, 
Plato's Ethics
, chs 1, 11-18, 20
- Josiah Ober, 
Political Dissent in Democratic Athens
, chs 1, 4
- C.D.C. Reeve, 
Philosopher-Kings
- Malcolm Schofield, Plato, 
Political Philosophy
- Gregory Vlastos, Socrates: 
Ironist and Moral Philosopher
, and 
Platonic Studies
, nos 5 and 6
**Aristotle, 
Nicomachean Ethics
;
 
Politics
- John Cooper, 
Reason and Human Good in Aristotle
- Jill Frank, 
A Democracy of Distinction
- Richard Kraut, 
Aristotle: Political Philosophy
- Jonathan Lear, 
Aristotle: The Desire to Understand
- Stephen Salkever, 
Finding the Mean
- Arlene Saxonhouse, 
Fear of Diversity
, Part III (chs 8, 9)
- Aristide Tessitore, 
Reading Aristotle's Ethics
- Bernard Yack, 
The Problems of a Political Animal
Cicero, 
On the Commonwealth (De Republica)
,
 Bks I, entire; 
III, entire;
 VI, 
"Dream of Scipio"
 only; 
On the Laws (De Legibus)
, Bks I and II, entire;
 
On Duties (De Officiis)
,
 entire
- Anthony Everitt, 
Cicero
- Bryan Garsten, 
Saving Persuasion
, ch. 5
- A.A. Long, "Cicero’s politics in De officiis (On Duties)", in A. Laks and M. Schofield (eds), 
Justice and Generosity
- Malcolm Schofield, 
Saving the City
, ch. 10
- E. W. Steel, 
Cicero, Rhetoric, and Empire
- Neal Wood. 
Cicero‘s Social and Political Thought
Augustine, 
The City of God
,
 Books II-V; VII, 1-11; XIV, 28; XV, 1-5; XIX, 4-22, 25-28; XX, 1-2; XXII, 1-8, 30
- Peter Brown, 
Augustine of Hippo
- Henry Chadwick, 
Augustine
- Herbert A. Deane, 
The Political and Social Ideas of St. Augustine
- Peter Garnsey, 
Ideas of Slavery from Aristotle to Augustine
, ch. 13
- R.A. Markus, 
Saeculum: history and society in the theology of St. Augustine
- R. Martin, "The two cities in Augustine’s political philosophy," 
Journal of History of Ideas
 33 (1972), 195-216
- Reinhold Niebuhr, "Augustine's Political Realism," in 
Christian Realism and Political Problems
- J. Rist, 
Augustine
Aquinas, 
Summa contra Gentiles
,
 I:3, 4, 7, 8; II: 68; III: 2,3,25, 27, 32, 37, 48, 51, 53. 63, 64, 81; IV: 54, 76; 
De Regimine Principum
 1-6, 12, 14, 15; 
Summa Theologiae
 I, qq. 2, 12, 20, 75, 79, 85, 92. 96, 98; I-II, qq. 3,5,21, 62, 81, 
90-97 (
Treatise on Law
),
 100, 105, 109; II-II, qq. 10. 11, 12, 40, 42, 57, 60, 64, 66, 69, 77, 78, 194, 110, 150, 152, 154; III, qu. 8; Supplement, qu. 52 (these selections can be found in St. Thomas Aquinas on Politics and Ethics [Norton Critical Editions], ed. P. Sigmund).
- J.H. Burns, ed., 
Cambridge History of Medieval Political Thought
- J.P. Canning, 
A History of Medieval Political Thought 300-1450
 (1996), ch. 3
- A.P. d'Entreves, 
Natural Law
- John Finnis, 
Aquinas: Moral, Political, and Legal Theory
- Norman Kretzmann and Eleonore Stump, eds. 
Cambridge Companion to Aquinas
: esp. ch. by Sigmund
- N. Kretzmann, A. Kenny, J. Pinborg, E. Stumb, eds, 
The Cambridge History of Later Medieval Philosophy
: esp. chapters by Barnes, Dunbabin, Luscombe (both) and McGrade
II. Modern political theory
**Machiavelli, 
The Prince
;
 
The Discourses
- F. Gilbert, 
Machiavelli and Guicciardini: Politics and History in Sixteenth-Century Italy
 (1984 edn)
- Mark Hulliung, 
Citizen Machiavelli
- Harvey Mansfield, 
Machiavelli 's Virtue
- Hannah Pitkin, 
Fortune is a Woman
- J.G.A. Pocock, 
The Machiavellian Moment
- Quentin Skinner, 
Machiavelli
**Hobbes, 
Leviathan
- Jean Hampton, 
Hobbes and the Social Contract Tradition
- Noel Malcolm, 
Aspects of Hobbes
- Michael Oakeshott, 
‘Introduction to Leviathan’ in Rationalism in Politics
- Quentin Skinner, 
Reason and Rhetoric
, ch. 8; 
Visions of Politics
, vol. 3; 
Hobbes and Republican Liberty
- Leo Strauss, 
The Political Philosophy of Hobbes
- Richard Tuck, 
Hobbes and Philosophy and Government
, 1572-1651
**Locke, 
First Treatise of Civil Government
, §§1, 3, 23, 33, 40-48, 56, 58-66, 86-87, 89-94; 
Second Treatise of Civil Government
;
 
A Letter Concerning Toleration
- Richard Ashcraft, 
Revolutionary Politics and Locke 's Two Treatises of Government
- John Dunn, 
The Political Thought of John Locke
- Ruth Grant, 
John Locke 's Liberalism
- Peter Laslett, "Introduction" to CUP edition of 
Two Treatises of Government
- A. John Simmons, 
The Lockean Theory of Rights
; 
On the Edge of Anarchy
; 
Moral Principles and Political Obligations
, ch. on tacit consent
- James Tully, 
An Approach to Political Philosophy: Locke in Contexts
- Jeremy Waldron, 
God, Locke, and Equality
; 
The Right to Private Property
, ch. 6
- J. Horton and S. Mendus (eds) 
John Locke: A letter concerning toleration in focus
Montesquieu, 
The Spirit of the Laws
, Bks 1-6; Bk 7 chs 1, 9. 15-17; Bk 8; Bk 9 chs 1-5; Bk 10 chs 1 - 11; Bk 11 chs 1-6; Bk 12, chs 1-4; Bk 14 chs 1-6, 9-10, 15; Bk 15; Bk 16, chs 1-4, 9-10; Bk 17; Bk 18 chs 1-17; Bk 19 chs 1-16, 27; Bk 20 chs 1-14, 23; Bk 21 chs 1-5, 20-23; Bk 23, chs 28-29; Bk 24, chs 1-8, 19-20; Bk 25 chs 1-2, 9-15; Bk 26, chs 1-3, 20-23; Bk 29, chs 1, 16, 194
- H.E. Ellis, "Montesquieu’s Modern Politics: The Spirit of the Laws and the problem of modern monarchy in Old Regime France," 
History of Political Thought
, 10 (1989), 665-700
- Nannerl Keohane, 
Philosophy and the State in France: The Renaissance to the Enlightenment
- Thomas Pangle, 
Montesquieu’s Philosophy of Liberalism
- Melvin Richter, "Comparative Political Analysis in Montesquieu and Tocqueville," 
Comparative Politics
 1 (1969), 129-160
- Judith Shklar, 
Montesquieu
- R. Shackelton, ed.. 
Essays on Montesquieu and the Enlightenment
- D. Carrithers, M. Mosher, and P. Rahe (eds), 
Montesquieu's Science of Politics
Hume, 
A Treatise of Human Nature
,
 Book III. Parts I and II; "Of the Original Contract" in 
Essays
- Stephen Buckle, 
Natural Law and the Theory of Property: Grotius to Hume
, ch.5
- Duncan Forbes, 
Hume's Philosophical Politics
- Knud Haakonssen, 
The Science of a Legislator: the Natural Jurisprudence of David Hume and Adam Smith
- David Miller, 
Philosophy and Ideology in Hume's Political Thought
- Frederick Whelan, 
Order and Artifice in Hume's Political Philosophy
- Alexander Broadie, ed., 
Cambridge Companion to the Scottish Enlightenment
- R.H. Campell and A.S. Skinner (eds), 
The Origins and Nature of the Scottish Enlightenment
**Rousseau, 
Discourse on the Sciences and Arts
; 
Discourse on the Origin of Inequality
; 
On The Social Contract
 (recommended: Emile and The Government of Poland)
- Joshua Cohen, 
A Free Community of Equals
- N.J.H. Dent, 
Rousseau: An Introduction to his Psychological, Social, and Political Theory
- Arthur Melzer, 
The Natural Goodness of Man: On the System of Rousseau’s Thought
- Frederick Neuhouser, "Freedom, Dependence, and the General Will," 
Philosophical Review
, 102 (1993), 363-395 and 
Rousseau’s Theodicy of Self-Love: Evil, Rationality, and the Drive for Recognition
- Susan Okin, 
Women in Western Political Thought
, pt. III
- Judith Shklar, 
Men and Citizens
- Jean Starobinski, 
Jean-Jacques Rousseau: Transparency and Obstruction
- Patrick Riley, ed., 
Cambridge Companion to Rousseau
Bentham, 
Introduction to Principles of Morals and Legislation
, chs. 1-5, 10. 12-14, 17; 
Nonsense Upon Stilts
 (in Bentham, Rights, Representation, and Reform, pp. 319-401)
- Lea Campos Boralevi, 
Bentham and the Oppressed
- H.L.A. Hart, 
Essays on Bentham
- Douglas G. Long, 
Bentham on Liberty
- Mary P. Mack, 
Jeremy Bentham
- Frederick Rosen. 
Jeremy Bentham and Representative Democracy
- Nancy Rosenblum, 
Bentham 's Theory of the State
- Philip Schofield, 
Utility and Democracy: The Political Thought of Jeremy Bentham 
Smith, 
The Wealth of Nations
,
 Bk I chs. 1-3; Bk III chs. 1,4; Bk IV, chs.1-3, 5 (including the "Digression"), 7 (Part 3); Bk V chs. 1, 2 (Part I); 
The Theory of Moral Sentiments
- Samuel Fleischacker, 
On Adam Smith's Wealth of Nations: A Philosophical Companion
- Charles Griswold, 
Adam Smith and the Virtues of Enlightenment
- Knud Haakonssen, 
The Science of a Legislator: the Natural Jurisprudence of David Hume and Adam Smith
- Albert Hirschman, 
The Passions and the Interests
- Istvan Hont and Michael Ignatieff, eds., 
Wealth and Virtue
- Istvan Hont, 
Jealousy of Trade
 (Cambridge, Mass., 2005), "Introduction" and chs 5-6
- Andrew Skinner and Thomas Wilson (eds) 
Essays on Adam Smith
Jay, Madison, and Hamilton, 
The Federalist Papers
,
 nos 1, 10, 14-18, 37, 47-49, 51-57, 62-63, 70-71, 78, 84; 
The Anti-Federalist
, ed. H. Storing, abridged M. Dry; 
Essays of "Brutus"
, nos 1-4
- David Epstein, 
The Political Theory of the Federalist
- Henry May, 
The Enlightenment in America
- Thomas Pangle, 
The Spirit of Modern Republicanism
- J. G. A. Pocock, "1776: The Revolution against Parliament," in Pocock (ed.), 
Three British Revolutions: 1641, 1688 and 1776
, pp. 265-88
- P. Rahe, 
Republics, Ancient and Modern, vol.3: Inventions of Prudence: Constituting the American Regime
- Rogers Smith, 
Civic Ideals
- H. J. Storing, 
What the Anti-Federalists were For
- Gordon Wood, 
The Creation of the American Republic
, chs 2, 12, 13, 15
- Michael Zuckert, 
The Natural Rights Republic
Burke, 
Pre-Revolutionary Writings
, ed. I. Harris; 
Reflections on the Revolution in France
; 
Speech on Fox's East India Bill
; 
Speech in Opening the Impeachment of Warren Hastings
 (for Fox and Hastings speeches, see D. Bromwich, ed., On Empire, Liberty, and Reform; or J. Welsh and D. Fidler, eds, Empire and Community)
- David Bromwich, "Introduction" to 
Burke, On Empire, Liberty, and Reform
- James Conniff, 
The Useful Cobbler: Edmund Burke and the Politics of Progress
- Conor Cruise O'Brien, 
The Great Melody
- J.G.A. Pocock, 
Politics, Language, and Time
, ch. 6; 
Virtue, Commerce and History
, ch. 10
- Frederick Whelan, 
Edmund Burke and India
- Stephen K. White, 
Edmund Burke: Modernity, Politics, and Aesthetics
**Kant, 
Idea, for a Universal History
; 
What is Enlightenment?
; 
Conjectures on the Beginning of Human History
; 
On the Common Saying: “That May Be Correct in Theory, but It Is Of No Use in Practice”
, part II; 
Toward Perpetual Peace: Groundwork of the Metaphysics of Morals
; 
The Metaphysics of Morals
: Preface, Introduction, 
Doctrine of Right
: Introduction through §27, §§41-42, 43-62; 
Doctrine of Virtue
: Preface, Introduction, §§4, 11, 12, 16-18, 19-22, 29-31, 34-35, 37-38, 47-48
- Katrin Flikschuh, 
Kant and Modern Political Thought
- Leslie Mullholland, 
Kant’s System of Rights
- Sankar Muthu, 
Enlightenment against Empire
- Onora O'Neill, 
Constructions of Reason
, chs 1, 2
- Allen D. Rosen, 
Kant's Theory of Justice
- Arthur Ripstein, 
Force and Freedom
- Allen Wood, 
Kant's Ethical Thought
- Mark Timmons, ed., 
Kant’s Metaphysics of Morals: Interpretive Essays
- Howard S. Williams, 
Kant's Political Philosophy
*Hegel, 
The Phenomenology of Spirit: Preface
; 
Introduction
; 
Lordship and Bondage
; 
Absolute Freedom and Terror
; 
The Philosophy of Right
- Shlomo Avineri, 
Hegel's Theory of the Modern State
- Frederick Neuhouser, 
Foundations of Hegel 's Social Theory
- Z.A. Pelczynski, ed., 
The State and Civil Society
- Robert Pippin, 
Idealism as Modernism: Hegelian Variations
, chs 1, 4, 5
- Charles Taylor, 
Hegel
- Allen Wood, 
Hegel 's Ethical Thought
Tocqueville, 
Democracy in America
, Intro.; Vol. 1: Part I, chs 3-5; Part II, chs 1-4, 6-10; Vol. II: Part I, chs 1-4, 8, 10, 13, 17, 20; Part II, chs 1-8, 11-15, 18, 20; Part III, chs 8, 9, 1 1. -13, 17, 19, 21, 22; Part IV, chs 1-86
George Armstrong Kelly, 
The Humane Comedy: Constant, Tocqueville, and French Liberalism
- Jack Lively, 
Social and Political Thought of Alexis de Tocqueville
- Pierre Manent, 
Tocqueville and the Nature of Democracy
- L. Siedentop, 
Tocqueville, and ‘Two Liberal Traditions’
 in A. Ryan, ed., 
The Idea of Freedom
- Cheryl Welch, 
De Tocqueville
- Sheldon Wolin, 
Tocqueville Between Two Worlds
**Marx, 
On the Jewish Question
, 
Contribution to the Critique of Hegel 's Philosophy of Right: Introduction
; 
Economic and Philosophical Manuscripts of 1844
; 
The German Ideology, Part I
; 
Manifesto of the Communist Party
;
 
Capital
, selections from vols I and III; 
Critique of the Gotha Programme
, 
The 
Eighteenth Brumaire of Louis Bonaparte
,
 
The Civil War in France
 (excerpts in The Marx-Engels Reader, 2nd edition, ed. Tucker)
- Shlomo Avineri, 
The Social and Political Thought of Karl Marx
- Isaiah Berlin, "Historical Materialism," in 
Four Essays on Liberty
- G.A. Cohen, 
Karl Marx's Theory of History
- Jon Elster, 
Making Sense of Marx
- Leszek Kolakowski, 
Main Currents of Marxism
- David Leopold, 
The Young Karl Marx
- Steven Lukes, 
Marxism and Morality
- G. Stedman Jones, "Introduction" to 
The Communist Manifesto
, ed. G. Stedman Jones
- Jonathan Wolff, 
Why Read Marx Today?
- Allen W. Wood, 
Karl Marx 
**J. S. Mill, 
Utilitarianism
; 
On Liberty
;
 
Considerations on Representative Government
; 
The Subjection of Women
;
 
Principles of Political Economy
, 7th edition, Book IV, chs 6-7, Book V, chs 1, 11
- F. R. Berger, 
Happiness, Justice and Freedom: The Moral and Political Philosophy of J.S. Mill
- S. Collini, D. Winch, and J. Burrow, 
That Noble Science of Politics
- Susan Okin, 
Women in Western Political Thought
, ch. 9
- Andrew Pyle. ed., 
Liberty: Contemporary Responses to John Stuart Mill
- Alan Ryan, 
J.S. Mill
- John Skorupski, 
John Stuart Mill
- John Skorupski, ed., 
The Cambridge Companion to Mill
- C.L. Ten, 
Mill on Liberty
, esp. ch. 2
- Nadia Urbinati, 
Mill on Democracy
- Dennis Thompson, 
John Stuart Mill and Representative Government
Nietzsche, 
On the Uses and Disadvantages of History for Life
; 
Beyond Good and Evil
;
 
Genealogy of Morals
- Steven Aschheim, 
The Nietzsche Legacy in Germany
- Peter Bergmann, 
Nietzsche: The Last Antipolitical German
- Alexander Nehamas, 
Nietzsche: Life as Literature
- Richard Schacht, ed., 
Nietzsche, Genealogy Morality
; and 
Nietzsche's Postmoralism
- Tracy Strong, 
Friedrich Nietzsche and the Politics of Transfiguration
- Michael Tanner, 
Nietzsche
- Raymond Geuss, 
Nietzsche and genealogy
; 
Kultur, Bildung, Geist
; and 
Nietzsche and morality
 all repr. in 
Geuss, Morality, Culture, and History
- Brian Leiter, 
Routledge Philosophy Guidebook to Nietzsche on Morality
Weber, "The Profession and Vocation of Politics," "Suffrage and Democracy in Germany," and "Parliament and Government in Germany under a New Political Order," all in 
Weber: Political Writings
, ed. P. Lassman and R. Speirs; "The Types of Legitimate Domination," in 
Max Weber: Economy and Society
, ed. G. Roth and C. Wittich, vol. 1; "Economy and Law," in ibid., vol. 2
- Peter Breiner, 
Max Weber and Democratic Politics
- Wolfgang Mommsen, 
Max Weber and German Politics, 1890-1920
- Chris Thornhill, "Max Weber", in: 
Political Theory in Modern Germany
- Richard Bellamy, "Liberalism Disenchanted", in: 
Liberalism and Modern Society
- Wilhelm Hennis, 
Max Weber: Essays in Reconstruction
- Lawrence Scaff, 
Fleeing the Iron Cage
III. Norms and concepts
1. Authority and political obligation
- Hannah Arendt, "What is Authority?," in 
Arendt, Between Past and Future
- Hugo Bedau, ed. 
Civil Disobedience in Focus
 (essays by Thoreau, King, Haksar, Raz, Greenawalt)
- Ronald Dworkin, 
Law's Empire
, ch 6
- John Rawls, 
A Theory of Justice
- Joseph Raz, 
The Morality of Freedom
- J. Raz, "Introduction" to Raz (ed.) 
Authority
- A. John Simmons, 
Moral Principles and Political Obligations
; 
Justification and Legitimacy
- Michael Walzer, 
Obligations
- Max Weber, "Politics as a Vocation", "Bureaucracy," "The Sociology of Charismatic Authority," in H. H. Gerth and C. W. Mills (eds) 
From Max Weber: Essays in Sociology
, chs 4, 8-9
- Robert Paul Wolff, 
In Defense of Anarchy
2. Constitutionalism and the rule of law
- Ronald Dworkin, 
Law’s Empire
; 
Freedom 's Law
- Jon Elster, ed., 
Democracy and Constitutionalism
- John Hart Ely, 
Democracy and Distrust: A Theory of Judicial Review
- F.A. Hayek, 
The Constitution of Liberty
- John Rawls, 
A Theory of Justice
 and Political Liberalism
- Joseph Raz, 
The Authority of Law and Ethics and the Public Domain
. ch. 17
- Jeremy Waldron, 
Liberal Rights
; 
Law and Disagreement
3. Democracy
- Joshua Cohen, 
Philosophy, Politics, and Democracy
- Hannah Arendt, 
The Human Condition
- Monica Brito Vieira and David Runciman, 
Representation
- Robert Dahl, 
Democracy and its Critics
- Anthony Downs, 
An Economic Theory of Democracy
- David Estlund, ed.. 
Democracy
 (papers by Christiano, Waldron. Cohen, Habermas. Miller)
- David Estlund, 
Democratic Authority
- Amy Gutmann and Dennis Thompson, 
Democracy and Disagreement
- Jurgen Habermas, 
Between Facts and Norms
- Bernard Manin, 
The Principles of Representative Government
- Hannah Pitkin. 
The Concept of Representation
- Adam Przeworksi, "A Minimalist Conception of Democracy: A Defense," in I. Shapiro and C. Hacker-Cordon (eds) 
Democracy’s Value
- John Rawls, 
A Theory of Justice
- John Rawls, 
Political Liberalism
- Carl Schmitt, 
The Crisis of Parliamentary Democracy
- J.A. Schumpeter,
 Capitalism, Socialism and Democracy
, Part IV
- Iris M. Young, 
Democracy and Inclusion
4. Freedom
- Hannah Arendt, 
The Human Condition
- Isaiah Berlin, 
Four Essays on Liberty
- Patrick Devlin, 
The Enforcement of Morals
- Ronald Dworkin, 
Taking Rights Seriously
- Joel Feinberg, 
Rights, Justice and the Bounds of Liberty
- H.L.A. Hart, 
Law, Liberty, and Morality
- G. C. MacCallum, "Negative and Positive Freedom," Phil. Rev. 76 (1967), 312-34, - repr. in P. Laslett and others, eds., 
Philosophy, Politics and Society
, 4th series
- David Miller, ed., 
Liberty
 (esp. articles by Hayek, Arendt, MacCallum, Cohen, Taylor, Skinner)
- Robert Nozick, 
Anarchy State and Utopia
- John Rawls, 
A Theory of Justice
- John Rawls, 
Political Liberalism
- Joseph Raz, 
The Morality of Freedom
- T. M. Scanlon, "A Theory of Freedom of Expression," 
Philosophy & Public Affairs
 1 (1971)
- Quentin Skinner, 
Liberty Before Liberalism
- Jeremy Waldron, 
Liberal Rights
5. Global justice
- David Miller, 
On Nationality
- David Miller, 
National Responsibility and Global Justice
- Joshua Cohen, "Minimalism about Human Rights: the best we can hope for?" 
Journal of Political Philosophy
 12 (2004) 190-213
- Thomas Nagel, "The Problem of Global Justice," 
Philosophy & Public Affairs
 33 (2005) 113-47
- Thomas Pogge, 
World Poverty and Human Rights
- John Rawls, 
The Law of Peoples
- Carl Schmitt, 
The Concept of the Political
- Henry Shue, 
Basic Rights
- Yael Tamir. 
Liberal Nationalism
- Michael Walzer, 
Just and Unjust Wars
6. Identity, difference and pluralism
- Benedict Anderson, 
Imagined Communities
- Anthony Appiah and Amy Gutmann, 
Color Conscious
- Brian Barry, 
Culture and Equality
- Seyla Benhabib, et al., 
Feminist Contentions: A Philosophical Exchange
- Seyla Benhabib, ed., 
Democracy and Difference
- Ernest Gellner, 
Nations and Nationalism
- Will Kymlicka, 
Multicultural Citizenship
- David Miller, 
On Nationality
- Susan Okin, 
Justice, Gender and the Family
- Carole Pateman, 
The Sexual Contract
- Yael Tamir. 
Liberal Nationalism
- Charles Taylor, "The Politics of Recognition," in A. Gutmann, ed.,
Multiculturalism and the Politics of Recognition
- Iris Marion Young, 
Justice and the Politics of Difference
7. Justice and equality
- Elizabeth Anderson, "What is the point of equality?," 
Ethics
 109 (1999)
- M. Clayton and A. Williams (eds) 
The Ideal of Equality
 (papers by: Nagel, Scanlon, Parfit)
- G.A.Cohen, "On the Currency of Egalitarian Justice," 
Ethics
 99 (1989)
- G.A. Cohen, 
Self-Ownership, Freedom and Equality
- G.A. Cohen, 
Rescuing Justice and Equality
- Ronald Dworkin, 
Sovereign Virtue
- Harry Frankfurt, ‘Equality as a Moral Ideal,’ 
Ethics
, 1987 (or as repr. in his 
The Importance of What We Care About
)
- Thomas Nagel, 
Equality and Partiality
- Robert Nozick, 
Anarchy State and Utopia
- Susan Okin. 
Justice, Gender and the Family
- John Rawls, 
A Theory of Justice
- John Rawls, 
Political Liberalism
- Michael Sandel, 
Liberalism and the Limits of Justice
- T. M. Scanlon, "Contractualism and Utilitarianism," in A. Sen and B. William, eds., 
Utilitarianism and Beyond
, or in Scanlon, 
The Difficulty of Toleration
- Amartya Sen, "Equality of What?" in Sen, 
Choice, Welfare, and Measurement
- Michael Walzer, 
Spheres of Justice
- Bernard Williams, "The Idea of Equality," repr. in Williams, 
Problems of the Self
- Iris Marion Young, 
Justice and the Politics of Difference
8. Power
- Brian Barry, 
Democracy, Power and Justice
 (essays on power)
- Michel Foucault, 
Discipline and Punish
- Michel Foucault, "Power, Right, Truth," in P. Pettit and R. Goodin (eds) 
A Companion to Political Philosophy
- Albert Hirschmann, 
Exit, Voice, and Loyalty
- Steven Lukes, 
Power: a radical view
- Steven Lukes, ed., 
Power
- Robert Nozick, "Coercion," in S. Morgenbesser and M. White (eds) 
Philosophy, Science and Method: Essays in Honor of Ernest Nagel
- Max Weber, "Politics as a Vocation" and "Bureaucracy," in H.H. Gerth and C.W. Mills (eds) 
From Max Weber
, chs 4, 8
9. Public reason
- Seyla Benhabib, 
Situating the Self: Gender, Community, and Postmodernism in Contemporary Ethics
- Joshua Cohen, "Truth and Public Reason," 
Philosophy and Public Affairs
 37 (2009) 2-42
- Raymond Geuss, 
The Idea of a Critical Theory
- Jürgen Habermas, "Discourse Ethics: Notes on a Program of Philosophical Justification," in Habermas, 
Moral Consciousness and Communicative Action
- Alasdair Maclntyre, 
After Virtue
- Michael Oakeshott, 
Rationalism in Politics
- John Rawls, 
Political Liberalism
- John Rawls, "The Idea of Public Reason Revisited," in 
The Law of Peoples
- Michael Walzer, 
The Company of Critics
- Bernard Williams, 
Ethics and the Limits of Philosophy
- Bernard Williams, 
In the Beginning Was the Deed
10. Rights
- Joel Feinberg, ‘The Nature and Value of Rights,’ repr. in 
Rights, Justice and the Bounds of Liberty
- John Finnis, 
Natural Law and Natural Rights
- W. N. Hohfeld, 
Fundamental Legal Conceptions
- Robert Nozick, 
Anarchy State and Utopia
- Henry Shue, 
Basic Rights
- Charles Taylor, ‘Atomism,’ in Taylor, 
Philosophical Papers
, vol. 2
- Jeremy Waldron, ed.. Rights, esp. 
Introduction
, articles by Hart and MacDonald
- Jeremy Waldron, 
The Right to Private Property
 (On rights, also consider works by Dworkin, Nozick, Rawls, Raz and Waldron under Freedom above.)
Appendix: Approaches to the study of political thought
- Isaiah Berlin, "Does Political Theory Still Exist?," 
Philosophy, Politics and Society
, ed. P. Laslett and W.G. Runciman, second series, repr. in Berlin, 
The Proper Study of Mankind
- William Connolly, "Essentially Contested Concepts in Politics," in 
The Terms of Political Discourse
- Michael Freeden, 
Ideologies and Political Theory
- John Rawls, 
Lectures on the History of Political Philosophy
- Quentin Skinner, ‘Meaning and Understanding in the History of Ideas,’ 
History and Theory
 8 (1969), 3-53, repr. in J. Tully, ed., 
Meaning and context: Quentin Skinner and his critics
; revised version in Skinner, 
Visions of Politics
, vol.1, with other relevant essays on method
- Leo Strauss, "What is Political Philosophy?," "Persecution and the Art of Writing," repr. in 
What is Political Philosophy?
 James Tully, ed., 
Meaning and Context
- Sheldon Wolin, "Political Theory as a Vocation," APSR 63 (1969) 1062-82, repr. in M. Fleisher, ed., 
Machiavelli and the Nature of Political Thought