[Intro]
Yeah, gypsy!

[Verse 1]
She was straight from Hell
But you never could tell
'Cause you were 
blinded by her light
She could crack your brain
With magic pain
And 
turn a paler shade of white
Well I was on the free
Just me and me
And just about to sail
When I heard the voice
Said you got a choice
The hammer or the nail

[Chorus]
You'll be ridin'
You'll be ridin' on the gypsy
On the gypsy

[Verse 2]
Well I rolled the bones
To see who'd own
My mind and what's within
And it's a given rule
That we're all fools
And need to have a little sin

[Chorus]
So I'm ridin'
Well I'm ridin' on the gypsy
On the gypsy queen

[Verse 3]
Well, she was straight from Hell
But you never could tell
'Cause you were blinded by the light
So she cracked my brain
With magic pain
And turned my left around to right, right

[Chorus]
So I'm ridin'
I'm still ridin'
Yes I'm ridin' on the gypsy
On the gypsy queen
She's mean

[Outro]
Ridin' on the gypsy
Yeah, yeah
Yeah, yeah, on the queen
So, so mean