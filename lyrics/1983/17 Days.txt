Thank you Houston and good morning, NRA! Colonel North,
I appreciate your kind words. You are a genuine American hero and,
for your service to God, to country and to your NRA, we salute you
and thank you, sir!

At this gathering one year ago, I predicted that our freedom might
soon face its greatest threat ever. I spent the past year warning gun
owners all over this country that, if re-elected, President Obama
would launch an all-out, historic assault against the Second
Amendment and the personal freedom of hundreds of millions of
law-abiding Americans.

The news media called me paranoid. Obama vehemently denied his
anti-gun agenda, mocked us, they even passed out fliers saying he
would protect our rights — and a lot of Americans were deceived
into believing him. Deceived.

It didn’t take long for the real Barack Obama to show himself.
Even before he was sworn into office, before his inauguration, the
president launched his all-out siege against our rights. From gun bans
to magazine bans to convoluted schemes tantamount to national
registration of every gun owner in the country.

From executive orders voted on by no one to vice presidential
commissions and a flurry of legislative attacks, to U.N. treaties to
gut our Second Amendment, speeches and anti-gun rallies, media
appearances and the heavy-handed weight of the presidential
bully pulpit.

Apparently, there is nothing the president will not do to get something
— anything — through Congress to advance his agenda to destroy the
Second Amendment. Nothing.

So far, thanks to you and millions of Americans like you, that’s exactly
what President Obama has gotten — absolutely nothing!

A lot of courageous men and women in the U.S. House and Senate
have stood up to the president and defended our great freedom.
They’ve taken a lot of heat from the president, Michael Bloomberg,
and the media, so it’s really important that they hear from every NRA
member and gun owner and American who values that freedom.

To those Senators and Congressmen who have stood with the Second
Amendment, we say thank you and ask you to keep defending our
rights. You have stood with us and represented your home states —
let there be no doubt we stand firmly with you!

That’s important, because while the Senate vote less than two weeks
ago is significant, it is but one skirmish in what can only be defined as
a long war against our constitutional rights.

We are in the midst of a once-in-a-generation fight for everything we
care about. We have a chance to secure our freedom for a generation,
or to lose it forever. We must remain vigilant, ever resolute, and
steadfastly growing and preparing for the even more critical battles
that loom before us.

I am proud to report that the state of our NRA is stronger and larger
than it has ever been. Our commitment to freedom is unwavering,
our growth unprecedented.

Today, the NRA is a record 5 million strong! Even as thousands of
Americans join our cause every day, the media and political elites
denigrate us. They cringe at the sight of long lines at gun shows.
They mock Americans who are buying firearms and ammunition at
a record pace. They scorn and scold the NRA. They don’t get it
because they don’t get America.

President Obama, the President of the United States of America,
held a press conference 17 days ago and angrily called the NRA liars.
Liars. Really?

This from a man who spent his entire re-election campaign saying
he supported our Second Amendment rights and would never try to
take away anyone’s gun. And he calls us liars?

This from the president who repeatedly claimed that 40 percent of
firearms sales don’t involve a background check. That was never true,
and the Washington Post gave the president three Pinocchio’s for that
one. And he calls us liars?

The biggest whopper of all, one of the president’s favorite lines, is that
90 percent of Americans support his background check bill. The media
can’t rant about anything else — 90 percent don’t want criminals or
the mentally ill to get their hands on guns.

Well, I don’t know what kind of polling they do at the White House,
but I do know this: When it comes to keeping guns out of the hands
of violent criminals or the mentally deranged, NRA members agree
100 percent!

But Mr. President, the bill you backed wouldn’t accomplish that goal.
Your bill was for a check that criminals avoid. Your bill ordered the
law-abiding to participate in a maze of regulation that could criminalize
lawful firearms transactions and potentially create a massive
government list of every gun-owning citizen in the country.

The Schumer bill you first supported — and still support — would
create a database of every gun owner in America. The Manchin-
Toomey bill you later backed wouldn’t have prevented Newtown,
wouldn’t have prevented Tucson or Aurora, and won’t prevent the next
tragedy. None of it has anything to do with keeping any of our children
safer at any school anywhere.

That’s why the president couldn’t get 90 percent of the Senate to
go along with him, because Americans saw through the political
posturing. They treasure their freedom and they don’t want
government to take that freedom away. As they say in Texas,
the president’s 90 percent is all hat and no cattle.

I ran into a member of Congress just a couple of weeks ago. We spoke
for a moment and then he said, “Wayne, I guess I have to go back and
listen to 90 percent of the phone calls that are not coming in.”

That’s a true story! And it tells you everything you need to know about
Obama’s empty 90 percent.

So Mr. President, you can give all the speeches you want, you can
conjure up all the polls you can and call NRA members all the nasty
names you can think of, but your gun control legislation wouldn’t stop
one criminal, wouldn’t make anyone safer anywhere and that flawed
failure lost on its merits and got the defeat it deserved!

You know, the only “90” the president won’t talk about is Chicago.
His own hometown, now run by his own former chief of staff.
The president won’t talk about Chicago but he should, because in
the entire United States, Chicago ranks 90th out of 90 jurisdictions
in federal firearms prosecutions.

Dead last. But when I brought that up on Meet the Press, the
media ignored it.

The president doesn’t talk about that 90. And the national news media,
their cameras perched like vultures in the back of this hall, they
haven’t mustered the courage to walk into the White House briefing
room and ask about Chicago’s 90th ranking that is getting people killed
day and night — a shooting every 6.3 hours.

The deadliest city in America — the president’s own hometown —
ranks dead last in federal firearms prosecutions, and the media
doesn’t have the guts to ask him about it. If the president had one
clue about how to clean up violent crime, don’t you think he’d do
it in his own hometown?

If his policies brought us Chicago, why do we want to listen to him
any further? No, you’ll never hear the media ask him that. Maybe
it’s because all those reporters still have Obama bumper stickers
on their cars.

The national media and the political elites, they are all part of the
same class that thinks they’re smarter than we are. They know better
than we do. They can tell us what to do or not, what to own or not,
what to eat and drink or not, and how to live or not.

Take Michael Bloomberg. He’s gone from mayor of New York to the
title of National Nanny. From sugar to salt to trans fats to fruit drinks
to sodas, to what you can and can’t do or order in a restaurant, this
guy can’t seem to find enough ways to boss people around.

And now he wants to tell us who to elect or not? Seriously, I ask you,
if Michael Bloomberg weren’t a billionaire, would anybody even bother
to listen to him?

Now he’s joined with the president, created his own billionaire Super
PAC, ready to spend hundreds of millions to attack the NRA, demonize
gun owners, destroy elected officials who won’t bow down to his will,
and obliterate the Second Amendment.

All while the anti-gun media, which supposedly hates money in
politics, is all-too-happy to take — and all-to-breathless to brag about
— Bloomberg’s money in politics.

Already, they are conspiring in private. Re-grouping, planning,
preparing, organizing, even waiting for the, quote “next Newtown” —
the next tragedy to come, the next senseless, horrific crime to exploit.

Just the other day, an anti-gun spokesman told the National Journal,
quote, “The next Newtown is inevitable … those things can help inform
debate and galvanize people to act.”

Folks, politics does not get any more disgusting than that. They
wait to use the opportunity of violent tragedy, rather than to prevent
tragedy itself.

Let me say that again. Rather than implement solutions that could
prevent senseless violence, they choose broken policies that enable
tragedy. Tragedy they wait to exploit, by choice, for political gain.

We know that, even now, there are dangerous, deranged, evil people
throughout society preparing to unleash unspeakable violence in our
neighborhoods, our schools and our churches.

They use tragedy to try to blame us, to shame us into compromising
our freedom for their political agenda. They want to change America,
our culture and our values.

But this is America. The first country in the world founded not on a
race, not on a religion, not on a royalty, but on a set of God-given
principles we call inalienable rights.

We come from that line of patriots who broke from King George to
live their own lives as free people. And nowhere does freedom live
more than in our Second Amendment right to own a firearm to defend
ourselves, our families and our nation. Without that freedom,
we aren’t really free at all.

There is nothing more good and right and normal than an honest
American citizen owning a firearm to defend himself or protect
her family. They can try to blame and shame us with all their might,
but when it comes to defending the Second Amendment, we will never
sacrifice our freedom upon the altar of elitist acceptance. And we will
never surrender our guns — never!

More Americans today than ever before understand the principle of
the Second Amendment. The freedom it gives us as individuals to be
responsible for our own safety, protection and survival.

Imagine living in a large metropolitan area where lawful firearms
ownership is heavily regulated and discouraged. Imagine waking up
to a phone call from the police, warning that a terrorist event is
occurring outside and ordering you to stay inside your home.

I’m talking, of course, about Boston. Where residents were imprisoned
behind the locked doors of their homes — a terrorist with bombs and
guns just outside. Frightened citizens, sheltered in place, with no
means to defend themselves or their families from whatever may
come crashing through the door.

How many Bostonians wished they had a gun two weeks ago?
How many other Americans now ponder that life-or-death question?

A recent national poll answered that question decidedly. With danger
lurking outside their doors, 69 percent of Americans said, YES, I want
my freedom, I want my Second Amendment, I want my gun!

Lying in wait is a terrorist, a deranged school shooter, a kidnapper, a
rapist, a murderer — waiting and planning and plotting — in every
community across this country. Lying in wait right now. No amount of
political schemes, congressional legislation, presidential commissions,
or media roundtables will ever change that inevitable reality.

I’ve said it before and I’ll say it again: No bill in Congress, no Rose
Garden speech will ever change the inescapable fact that the only way
to stop a bad guy with a gun is a good guy with a gun.

Boston proves it. When brave law enforcement officers did their jobs
so courageously, good guys with guns stopped terrorists with guns.

All over this country, people are more and more frustrated with
Washington and the political and media elites. They are dismayed
over a political debate that has nothing to do with addressing our
problems and everything to do with advancing an old, tired,
failed political agenda.

Everywhere I go, I’ve learned that the NRA is truly at the heart
of America’s heartland. That we are in the middle of the river of
America’s mainstream. That what we want is exactly what most
Americans want.

We know our mental health system is in shambles. We all want it
fixed. We want criminals with guns prosecuted and incarcerated.
We want the federal gun laws on the books right now enforced against
felons with guns, drug dealers with guns, and gangs with guns.
If they’d just do that, those violent criminals wouldn’t be on the way
to their next crime. They’d be in prison.

We all want our children to be safe and protected. That’s why we
proposed trained police and security officers in every school. There’s
not a mom or dad in America who wants to leave their children
unprotected.

If the Washington elites really wanted the same thing, they would
stop demonizing law-abiding gun owners. They would stop trying to
convince the American people that all gun owners are potential
criminals in waiting. And they would actually implement programs
that addressed our problems in a real and meaningful way.

Put police and trained armed security in every school. Enforce the
federal gun laws on the books right now. Interdict and incarcerate
violent criminals before they get to the next crime scene. Rebuild our
broken mental health system. Help the mentally ill by getting them
off our streets and into treatment. And for God’s sake, leave the
rest of us alone!

The political class and media class just don’t get it. In a lot of ways,
they’ve lost track of what this great nation is really all about. It’s about
US and people like us, all over this country. It’s always been about
“we, the people,” not the political class, all the way back to our
founding.

Here’s what I’m talking about.

[VIDEO PLAYS]

We are the people. This is our country.

This is a fight for our freedom, the freedom that separates us from
every other nation on earth. That freedom makes us stronger than
other countries. It makes us better than other countries.

That freedom is on the line and never more on the line than right now
and through the 2014 congressional elections.

Seventeen days ago, President Obama said this was only round one.
Round two is on the way, and they’re coming after us with a
vengeance to destroy us. To destroy us and every ounce of our
freedom. It is up to us, every single NRA member and gun owner,
all Americans, to get to work right now to meet them head-on with
an NRA strong enough and large enough to defeat any and all
threats to our freedom.

Today, we are a record 5 million strong. We must not and will not
slow down, not one single bit. By the time we’re finished, this NRA
must and will be 10 million strong. Ten million dedicated, 10 million
patriotic Americans who cherish freedom and all that is good and
right about America.

We don’t care if it’s round 1 or 2 or 15, this NRA will go the distance.
And no matter what it takes, we will never give up or compromise our
constitutional freedom — NOT ONE SINGLE INCH!

Our feet are planted firmly in the foundation of freedom, un-swayed
by the winds of political and media insanity. And to the political and
media elites who scorn us, we say let them be damned!

Fill your heart with pride. Clear your eyes with conviction. This is our
time to Stand and Fight, now and in the next election and the one
after that. Now and for the rest of our lives, to save our Second
Amendment for future generations.

From liberty’s defense, we will never back down. We will never
surrender. We will always stand, we will always fight. We will always
Stand and Fight for freedom!