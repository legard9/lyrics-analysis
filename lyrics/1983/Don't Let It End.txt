Translation

[Verse 1: Crush]
I wonder what will happen today
A lot of pretty girls are outside
Feel like I can fly anywhere when I lean on the wind
I wanna see the sunset at Some Sevit with you
Pretty brown eyes, you can call me late at night
Time is passing by and I don't wanna regret it
Every night, never mind
I don't wanna care about the fierce eyes on me
Who cares, who cares
Yeah, I'm down for you, girl
Just don't care

[Pre-Chorus: Crush]
I think a secret is forming right now
Going outside today, light-hearted
It'll end tomorrow
So take me into the night
I wanna go outside

[Chorus: Crush]
Just let me go outside-side
Let me go outside-side
Just let me go outside-ide
Take me for a ride
Take me into the night
Let's go wherever we feel like
Or we can meet up first and pick wherever you want
Malibu and Santa Monica
I like them all, they're all dope
Get ready to go, ready to go

[Verse 2: Beenzino]
(After a shower, I look in the mirror like)
I'm looking particularly handsome in the mirror today (yeah yeah)
I'm too sexy to go protect my country (damn right)
A Maison Margiela whip around my waist
My bangs are long like spider legs, too edgy
I'm almost ready to go outside tomorrow
It's hard to live a secretless life every night
I'm sick of dealing with the cameras outside
"Put it down, put it down"
When I'm walking, I see a bonita applebum
My eyes pop out, outside
It's hard to me; I don't wanna put them back in
Lemme go out
Pour down the hard liquor
I can't let go yet; don't let my freedom go
The night is hella young #nofilter

[Pre-Chorus: Crush]
I think a secret is forming right now
Going outside today, light-hearted
It'll end tomorrow
So take me into the night
I wanna go outside

[Chorus: Crush]
Just let me go outside-side
Let me go outside-side
Just let me go outside-ide
Take me for a ride
Take me into the night
Let's go wherever we feel like
Or we can meet up first and pick wherever you want
Malibu and Santa Monica
I like them all, they're all dope
Get ready to go, ready to go

[Bridge: Beenzino & Crush]
I'm letting go of the steering wheel tonight
My heart has no brakes to put on
Swerving left and right
Flying straight up to the moon
Got me real high
Got me for real
God speed for real
I'm speeding up (don't think too much)
Yo (swish)
Pedal to the metal, autobarn
Have fun like I'm out of my mind (Ibiza gets me high)
Jazz bar in the Cuba (Chico and Rita)
Don't anybody stop me, don't stop me, don't stop me

[Outro: Crush & Beenzino]
Wanna go outside
(I wanna go outside)
Let me go outside
(Let me go outside)
Take me for a ride
(Take me for a ride, take me for a ride)
Let me go outside
(Let me go outside)

Hangul

[Verse 1: Crush]
오늘은 무슨 일이 생길지 궁금해
밖으로 나가면 이쁜 여자들도 많은데 (ooh ooh)
바람에 기대면 어디든 날아갈 것 같애 (살랑, 살랑, 살랑, 살랑)
너와 함께 보고 싶어, 세빛둥둥 sunset (ayy)
Pretty brown eyes, you can call me late at night
지나가는 시간 후회하기 싫어
난 매일 밤 never mind (mind)
더 이상 따가운 눈치 보기 싫어
뭐 어때, 뭐 어때
Yeah, I'm down for you, girl
Just don't care

[Pre-Chorus: Crush]
비밀이 생길 것 같아, 지금
오늘은 밖으로 나가벼운 기분
내일이면 끝나 (oh no no no)
So take me into the night
Wanna go outside

[Chorus: Crush]
Just let me go outside-side
Let me go outside-side
Just let me go outside-ide
Take me for a ride (ride)
Take me into the night (night)
어디로든 떠나, 느낌 가는 곳 (yeah)
아님 일단 만나서 정해, wherever you want
Malibu, Santa Monica도 난 너무나 좋아, 다 쩔어 (yeah)
Get ready to go, ready to go

[Verse 2: Beenzino]
(After shower, I look in the mirror like)
오늘따라 거울 보는데 잘생겼지 (ya, yeah yah)
나라를 지키러 가기엔 I'm too sexy (thing, damn right)
허리에 감은 Maison Margiela 채찍 (후추, 후추, ooh)
거미 다리처럼 긴 앞머리, too edgy (uh)
거의 끝났어, 내일 나갈 채비는
비밀이 없는 삶 살기 힘들어, 매일 밤
밖에 나가 상대하기 지겹지, cameras
Put it down, put it down
걷다 보니까 bonita applebum
눈 튀어나와, outside (outside)
난 힘들어, 집어넣기가 싫어 (싫어)
Lemme go out
퍼부어, hard liquor (liquor)
못 놔, 아직은, don't let my freedom go
The night is hella young #nofilter

[Pre-Chorus: Crush]
비밀이 생길 것 같아, 지금
오늘은 밖으로 나가벼운 기분
내일이면 끝나 (oh no no no)
So take me into the night
Wanna go outside

[Chorus: Crush]
Just let me go outside-side
Let me go outside-side
Just let me go outside-ide
Take me for a ride (ride)
Take me into the night (night)
어디로든 떠나, 느낌 가는 곳 (yeah)
아님 일단 만나서 정해, wherever you want
Malibu, Santa Monica도 난 너무나 좋아, 다 쩔어 (yeah)
Get ready to go, ready to go

[Bridge: Beenzino & Crush]
오늘 밤 난 운전댈 놓아 (놔)
밟을 브레이크 없는 내 마음
Swerving left right (right, left, right)
Flying straight up to the moon
Got me real high (real high)
Got me for real
God speed for real (real high)
속력을 내, 난 (don't think too much)
Yo (swish)
Pedal to the metal, autobahn
정신 줄 놓고 놀아 (Ibiza gets me high)
Jazz bar in the Cuba (Chico and Rita)
아무도 말리지 마, 말리지 마, 말리지 마

[Outro: Crush & Beenzino]
Wanna go outside
(I wanna go outside)
Let me go outside
(Let me go outside)
Take me for a ride
(Take me for a ride, take me for a ride)
Let me go outside
(Let me go outside)

Romanization

[Verse 1: Crush]
Oneureun museun iri saenggilji gunggeumhae
Bakkeuro nagamyeon yeppeun yeojadeuldo manheunde
Barame gidaemyeon eodideun naragal geot gatae
Neowa hamkke bogo sipeo saebiccdungdung sunset
Pretty brown eyes
You can call me late at night
Jinaganeun sigan huhoehagi sirheo
Nan maeil bam never mind
Deo isang ttagaun nunchibogi sirheo
Mwo eottae mwo eottae
Yeah i’m down for you girl
Just don’t care

[Pre-Chorus: Crush]
Bimiri saenggil geot gata jigeum
Oneureun bakkeuro nagabyeoun gibun
Naeirimyeon kkeutna
So take me into the night
I wanna go outside

[Chorus: Crush]
Just let me go outside
Let me go outside
Just let me go outside
Take me for a ride
Take me into the night
Eodirodeun tteona neukkim ganeun got
Anim ildan mannaseo jeonghae
Wherever you want
Malibu, Santa Monicado
Nan neomuna joha da jjeoreo
Get ready to go

[Verse 2: Beenzino]
Oneulttara geoul boneunde jalsaenggyeossji
(yeah yeah)
Nalareul jikileo gagien
I’m too sexy (damn right)
Heolie gameun meiseun mareujiella chaejjik
Geomidaricheoreom gin apmeori Too edgy
Geoui kkeutnasseo nae nagal chaebin
Bimiri eopsneun salm salgi himdeureo maeil bam
Bakke naga sangdaehagi jigyeobji cameras

[Verse 3: Beenzino]
Put it down put it down
(going out going out )
Geodda bonikka bonita applebum
Nun twieonawa Outside
Nan himdeureo jib-eoneohgiga sirheo (out)
Lemme go out peobueo hard liquor
Mos noha ajigeun don’t let my freedom go
The night is hella young #nofilter

[Pre-Chorus: Crush]
Bimiri saenggil geot gata jigeum
Oneureun bakkeuro nagabyeoun gibun
Naeirimyeon kkeutna
So take me into the night
I wanna go outside

[Chorus: Crush]
Just let me go outside
Let me go outside
Just let me go outside
Take me for a ride
Take me into the night
Eodirodeun tteona neukkim ganeun got
Anim ildan mannaseo jeonghae
Wherever you want
Malibu, Santa Monicado
Nan neomuna joha da jjeoreo
Get ready to go

[Bridge: Beenzino & Crush]
Oneul bam nan unjeondaeleul noha
Balbeul beuleikeu eopsneun nae maeum
Swerving Left right
Flying straight up to the moon
Got me real high
Got me for real
God speed for real
Soglyeogeul nae nan
(don’t think too much)
(swish)
Pedal to the metal autobahn
Jeongsin jul nohgo nora
(ibiza gets me high)
Jazz bar in the Cuba
(chico and Lita)
Amudo mallijima mallijima mallijima

[Outro: Crush]
Wanna go outside
I wanna go outside
Let me go outside
Take me for a ride
Let me go outside