"I think you had better wait," Mrs. Brook said, "till I see if he has gone;" and on the arrival the next moment of the servants with the tea she was able to put her question. "Is Mr. Cashmore still with Miss Brookenham?"

"No, ma'am," the footman replied. "I let Mr. Cashmore out five minutes ago."

Vanderbank showed for the next short time by his behaviour what he felt at not yet being free to act on this; moving pointlessly about the room while the servants arranged the tea-table and taking no trouble to make, for appearance, any other talk. Mrs. Brook, on her side, took so little that the silence—which their temporary companions had all the effect of keeping up by conscious dawdling—became precisely one of those precious lights for the circle belowstairs which people fondly fancy they have not kindled when they have not spoken. But Vanderbank spoke again as soon as the door was closed. "Does he run in and out that way without even speaking to YOU?"

Mrs. Brook turned away from the fire that, late in May, was the only charm of the crude cold afternoon. "One would like to draw the curtains, wouldn't one? and gossip in the glow of the hearth."

"Oh 'gossip'!" Vanderbank wearily said as he came to her pretty table.

In the act of serving him she checked herself. "You wouldn't rather have it with HER?"

He balanced a moment. "Does she have a tea of her own?"

"Do you mean to say you don't know?"—Mrs. Brook asked it with surprise. "Such ignorance of what I do for her does tell, I think, the tale of how you've lately treated us."

"In not coming for so long?"

"For more weeks, for more months than I can count. Scarcely since—when was it?—the end of January, that night of Tishy's dinner."

"Yes, that awful night."

"Awful, you call it?"

"Awful."

"Well, the time without you," Mrs. Brook returned, "has been so bad that I'm afraid I've lost the impression of anything before." Then she offered the tea to his choice. "WILL you have it upstairs?"

He received the cup. "Yes, and here too." After which he said nothing again till, first pouring in milk to cool it, he had drunk his tea down. "That's not literally true, you know. I HAVE been in."

"Yes, but always with other people—you managed it somehow; the wrong ones. It hasn't counted."

"Ah in one way and another I think everything counts. And you forget I've dined."

"Oh—for once!"

"The once you asked me. So don't spoil the beauty of your own behaviour by mistimed reflexions. You've been, as usual, superior."
"Ah but there has been no beauty in it. There has been nothing," Mrs. Brook went on, "but bare bleak recognition, the curse of my hideous intelligence. We've fallen to pieces, and at least I'm not such a fool as not to have felt it in time. From the moment one did feel it why should one insist on vain forms? If YOU felt it, and were so ready to drop them, my part was what it has always been—to accept the inevitable. We shall never grow together again. The smash was too great."

Vanderbank for a little said nothing; then at last: "You ought to know how great!"

Whatever had happened her lovely look here survived it. "I?"

"The smash," he replied, "was indeed as complete, I think, as your intention. Each of the 'pieces' testifies to your success. Five minutes did it."

She appeared to wonder where he was going. "But surely not MY minutes. Where have you discovered that I made Mitchy's marriage?"

"Mitchy's marriage has nothing to do with it."

"I see." She had the old interest at least still at their service. "You think we might have survived that." A new thought of it seemed to glimmer. "I'm bound to say Mitchy's marriage promises elements."

"You did it that night at Mrs. Grendon's." He spoke as if he had not heard her. "It was a wonderful performance. You pulled us down—just closing with each of the great columns in its turn—as Samson pulled down the temple. I was at the time more or less bruised and buried and didn't in the agitation and confusion fully understand what had happened. But I understand now."

"Are you very sure?" Mrs. Brook earnestly asked.

"Well, I'm stupid compared with you, but you see I've taken my time. I've puzzled it out. I've lain awake on it: all the more that I've had to do it all myself—with the Mitchys in Italy and Greece. I've missed his aid."

"You'll have it now," Mrs. Brook kindly said. "They're coming back."

"And when do they arrive?"

"Any day, I believe."

"Has he written you?"

"No," said Mrs. Brook—"there it is. That's just the way we've fallen to pieces. But you'll of course have heard something."

"Never a word."

"Ah then it's complete."

Vanderbank thought a moment. "Not quite, is it?—I mean it won't be altogether unless he hasn't written to Nanda."

"Then HAS he?"—she was keen again.

"Oh I'm assuming. Don't YOU know?"

"How should I?"

This too he turned over. "Just as a consequence of your having, at Tishy's, so abruptly and wonderfully tackled the question that a few days later, as I afterwards gathered, was to be crowned with a measure of success not yet exhausted. Why, in other words—if it was to know so little about her and to get no nearer to her—did you bring about Nanda's return?"

There was a clear reason, her face said, if she could only remember it. "Why did I—?" Then as catching a light: "Fancy your asking me—at this time of day!"

"Ah you HAVE noticed that I haven't asked before? However," Van promptly added, "I know well enough what you notice. Nanda hasn't mentioned to you whether or no she has heard?"

"Absolutely not. But you don't suppose, I take it, that it was to pry into her affairs I called her in."

Vanderbank, on this, lighted for the first time with a laugh. "'Called her in'? How I like your expressions!"

"I do then, in spite of all," she eagerly asked, "remind you a little of the bon temps? Ah," she sighed, "I don't say anything good now. But of course I see Jane—though not so often either. It's from Jane I've heard of what she calls her 'young things.' It seems so odd to think of Mitchy as a young thing. He's as old as all time, and his wife, who the other day was about six, is now practically about forty. And I also saw Petherton," Mrs. Brook added, "on his return."

"His return from where?"

"Why he was with them at Corfu, Malta, Cyprus—I don't know where; yachting, spending Mitchy's money, 'larking,' he called it—I don't know what. He was with them for weeks."

"Till Jane, you mean, called him in?"

"I think it must have been that."

"Well, that's better," said Van, "than if Mitchy had had to call him out."

"Oh Mitchy—!" Mrs. Brook comprehensively sounded.

Her visitor quite assented. "Isn't he amazing?"

"Unique."

He had a short pause. "But what's she up to?"

It was apparently for Mrs. Brook a question of such variety of application that she brought out experimentally: "Jane?"

"Dear no. I think we've fathomed 'Jane,' haven't we?"

"Well," mused Mrs. Brook, "I'm by no means sure I have. Just of late I've had a new sense!"

"Yes, of what now?" Van amusedly put it as she held the note.

"Oh of depths below depths. But poor Jane—of course after all she's human. She's beside herself with one thing and another, but she can't in any consistency show it. She took her stand so on having with Petherton's aid formed Aggie for a femme charmante—"

"That it's too late to cry out that Petherton's aid can now be dispensed with? Do you mean then that he IS such a brute that after all Mitchy has done for him—?" Vanderbank, at the rising image, pulled up in easy disgust.

"I think him quite capable of considering with a magnificent insolence of selfishness that what Mitchy has MOST done will have been to make Aggie accessible in a way that—for decency and delicacy of course, things on which Petherton highly prides himself—she could naturally not be as a girl. Her marriage has simplified it."

Vanderbank took it all in. "'Accessible' is good!"

"Then—which was what I intended just now—Aggie has already become so—?"

Mrs. Brook, however, could as yet in fairness only wonder. "That's just what I'm dying to see."

Her companion smiled at it. "'Even in our ashes live their wonted fires'! But what do you make, in such a box, of poor Mitchy himself? His marriage can scarcely to such an extent have simplified HIM."

It was something, none the less, that Mrs. Brook had to weigh. "I don't know. I give it up. The thing was of a strangeness!"

Her friend also paused, and it was as if for a little, on either side of a gate on which they might have had their elbows, they remained looking at each other over it and over what was unsaid between them. "It WAS 'rum'!" he at last merely dropped.

It was scarce for Mrs. Brook, all the same—she seemed to feel after a moment—to surround the matter with an excess of silence.

"He did what a man does—especially in that business—when he doesn't do what he wants."

"Do you mean what somebody else wanted?"

"Well, what he himself DIDN'T. And if he's unhappy," she went on, "he'll know whom to pitch into."

"Ah," said Vanderbank, "even if he is he won't be the man to what you might call 'vent' it on her. He'll seek compensations elsewhere and won't mind any ridicule—!"

"Whom are you speaking of as 'her'?" Mrs. Brook asked as on feeling that something in her face had made him stop. "I wasn't referring," she explained, "to his wife."

"Oh!" said Vanderbank.

"Aggie doesn't matter," she went on.

"Oh!" he repeated. "You meant the Duchess?" he then threw off.

"Don't be silly!" she rejoined. "He MAY not become unhappy—God grant NOT!" she developed. "But if he does he'll take it out of Nanda."
Van appeared to challenge this. "'Take it out' of her?"

"Well, want to know, as some American asked me the other day of somebody, what she's 'going to do' about it."

Vanderbank, who had remained on his feet, stood still at this for a longer time than at anything yet. "But what CAN she 'do'—?"

"That's again just what I'm curious to see." Mrs. Brook then spoke with a glance at the clock. "But if you don't go up to her—!"

"My notion of seeing her alone may be defeated by her coming down on learning that I'm here?" He had taken out his watch. "I'll go in a moment. But, as a light on that danger, would YOU, in the circumstances, come down?"

Mrs. Brook, however, could for light only look darkness. "Oh you don't love ME!"

Vanderbank, still with his watch, stared then as an alternative at the fire. "You haven't yet told me you know, if Mr. Cashmore now comes EVERY day."

"My dear man, how can I say? You've just your occasion to find out."

"From HER, you mean?"

Mrs. Brook hesitated. "Unless you prefer the footman. Must I again remind you that, with her own sitting-room and one of the men, in addition to her maid, wholly at her orders, her independence is ideal?"

Vanderbank, who appeared to have been timing himself, put up his watch. "I'm bound to say then that with separations so established I understand less than ever your unforgettable explosion."

"Ah you come back to that?" she wearily asked. "And you find it, with all you've to think about, unforgettable?"

"Oh but there was a wild light in your eye—!"

"Well," Mrs. Brook said, "you see it now quite gone out." She had spoken more sadly than sharply, but her impatience had the next moment a flicker. "I called Nanda in because I wanted to."

"Precisely; but what I don't make out, you see, is what you've since gained by it."

"You mean she only hates me the more?"

Van's impatience, in the movement with which he turned from her, had a flare still sharper. "You know I'm incapable of meaning anything of the sort."

She waited a minute while his back was presented. "I sometimes think in effect that you're incapable of anything straightforward."

Vanderbank's movement had not been to the door, but he almost reached it after giving her, on this, a hard look. He then stopped short, however, to stare an instant still more fixedly into the hat he held in his hand; the consequence of which in turn was that he the next minute stood again before her chair. "Don't you call it straightforward of me just not to have come for so long?"

She had again to take time to say. "Is that an allusion to what—by the loss of your beautiful presence—I've failed to 'gain'? I dare say at any rate"—she gave him no time to reply—"that you feel you're quite as straightforward as I and that we're neither of us creatures of mere rash impulse. There was a time in fact, wasn't there? when we rather enjoyed each other's dim depths. If I wanted to fawn on you," she went on, "I might say that, with such a comrade in obliquity to wind and double about with, I'd risk losing myself in the mine. But why retort or recriminate? Let us not, for God's sake, be vulgar—we haven't yet, bad as it is, come to THAT. I CAN be, no doubt—I some day MUST be: I feel it looming at me out of the awful future as an inevitable fate. But let it be for when I'm old and horrible; not an hour before. I do want to live a little even yet. So you ought to let me off easily—even as I let you."

"Oh I know," said Vanderbank handsomely, "that there are things you don't put to me! You show a tact!"

"There it is. And I like much better," Mrs. Brook went on, "our speaking of it as delicacy than as duplicity. If you understand, it's so much saved."

"What I always understand more than anything else," he returned, "is the general truth that you're prodigious."

It was perhaps a little as relapse from tension that she had nothing against that. "As for instance when it WOULD be so easy—!"

"Yes, to take up what lies there, you yet so splendidly abstain."

"You literally press upon me my opportunity? It's YOU who are splendid!" she rather strangely laughed.

"Don't you at least want to say," he went on with a slight flush, "what you MOST obviously and naturally might?"

Appealed to on the question of underlying desire, Mrs. Brook went through the decent form of appearing to try to give it the benefit of any doubt. "Don't I want, you mean, to find out before you go up what YOU want? Shall you be too disappointed," she asked, "if I say that, since I shall probably learn, as we used to be told as children, 'all in good time,' I can wait till the light comes out of itself?"
Vanderbank still lingered. "You ARE deep!"

"You've only to be deeper."

"That's easy to say. I'm afraid at any rate you won't think I am," he pursued after a pause, "if I ask you what in the world—since Harold does keep Lady Fanny so quiet—Cashmore still requires Nanda's direction for."

"Ah find out!" said Mrs. Brook.

"Isn't Mrs. Donner quite shelved?"

"Find out," she repeated.

Vanderbank had reached the door and had his hand on the latch, but there was still something else. "You scarce suppose, I imagine, that she has come to like him 'for himself?"

"Find out!" And Mrs. Brook, who was now on her feet, turned away. He watched her a moment more, then checked himself and left her.