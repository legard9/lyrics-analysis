And love's a dangerous game to play
From now on
It's just a lie
It's just a sign
Like stilted conversations that ended
With closed books
Tells you to forget
Something told me yesterday
From now on
It's time to find
That you are not just dreaming
Now it's time for waking up
Tells you to forget

Breakdown
Splinter
A thousand fragments disperse and die

And love's serious game we play
From time to time
We wonder why
Like a sudden realisation
That tells you it's no good
Tells you to forget
But you can't

We play our games
We say we'll change this time
It's for real
We'll avoid those mistakes
But dreams get broken
In the wake and the promises are fake
Breakdown about to come around again
Breakdown

Breakdown comes around and leaves you without a sound