[Verse 1]
Well you didn't wake up this morning
'Cause you didn't go to bed
You were watching the whites of your eyes turn red
The calendar on your wall
Is ticking the days off
You've been reading some old letters
You smile and think how much you've changed
All the money in the world
Couldn't buy back those days

[Chorus]
You pull back the curtains
And the sun burns into your eyes
You watch a plane flying
Across the clear blue sky
This is the day
Your life will surely change
This is the day
When things fall into place

[Verse 2]
You could've done anything
If you'd wanted
And all your friends and family
Think that you're lucky
But the side of you they'll never see
Is when you're left alone with the memories
That hold your life together like
Glue

[Chorus]
You pull back your curtains
And the sun burns into your eyes
You watch a plane flying
Across the clear blue sky
This is the day
Your life will surely change
This is the day
When things fall into place

[Outro]
This is the day (this is the day)
Your life will surely change
This is the day (this is the day)
Your life will surely change
This is the day (this is the day)
Your life will surely change
This is the day (this is the day)
Your life will surely change
This is the day (this is the day)
That your life will surely change
This is the day (this is the day)
Your life will surely change
This is the day (this is the day)
Your life will surely change
This is the day (this is the day)
Your life will surely change
This is the day (this is the day)
Your life will surely change...