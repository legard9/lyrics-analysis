[FLORENCE]
This is an all too familiar scene

[ANATOLY]
Life imperceptibly coming between

[BOTH]
Those whose love is as strong as it could or should be

[ANATOLY]
Nothing has altered --

[FLORENCE]
-- Yet everything's changed

[ANATOLY]
No one stands still

[BOTH]
Still I love you completely
And hope I always will
Each day we get through
Means one less mistake
Left for the making

[ANATOLY]
And there's no return
As we slowly learn
Of the chances we're taking

[FLORENCE]
I'd give the world
To stay just as we are
It's better by far
Not to be too wise

[BOTH]
Not to realize
Where there's truth
There will be lies

[FLORENCE]
You and I
We've seen it all
Been down this road before
Yet we go on believing

[ANATOLY]
You--

[FLORENCE]
You--

[BOTH]
And I
We've seen it all
Chasing our hearts' desire
Yet I still think I'm certain
This time it will be
My happy ending

[ANATOLY, spoken]
Thank you. It's from Molokov -- you were right. She's here, landed from Moscow this morning. Bastard