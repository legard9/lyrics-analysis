The same hut. Winter. Nine months have passed since Act II. Anísya, plainly dressed, sits before a loom weaving. Nan is on the oven.


MÍTRITCH
[an old labourer, enters, and slowly takes off his outdoor things] Oh Lord, have mercy! Well, hasn't the master come home yet?

ANÍSYA.
What?

MÍTRITCH.
Nikíta isn't back from town, is he?

ANÍSYA.
No.

MÍTRITCH.
Must have been on the spree. Oh Lord!

ANÍSYA.
Have you finished in the stackyard?

MÍTRITCH.
What d'you think? Got it all as it should be, and covered everything with straw! I don't like doing things by halves! Oh Lord! holy Nicholas! [Picks at the corns on his hands] But it's time he was back.

ANÍSYA.
What need has he to hurry? He's got money. Merry-making with that girl, I daresay …

MÍTRITCH.
Why shouldn't one make merry if one has the money? And why did Akoulína go to town?

ANÍSYA.
You'd better ask her. How do I know what the devil took her there!

MÍTRITCH.
What! to town? There's all sorts of things to be got in town if one's got the means. Oh Lord!

NAN.
Mother, I heard myself. “I'll get you a little shawl,” he says, blest if he didn't; “you shall choose it yourself,” he says. And she got herself up so fine; she put on her velveteen coat and the French shawl.

ANÍSYA.
Really, a girl's modesty reaches only to the door. Step over the threshold and it's forgotten. She is a shameless creature.

MÍTRITCH.
Oh my! What's the use of being ashamed? While there's plenty of money make merry. Oh Lord! It is too soon to have supper, eh? [Anísya does not answer] I'll go and get warm meanwhile. [Climbs on the stove] Oh Lord! Blessed Virgin Mother! holy Nicholas!

NEIGHBOUR
[enters] Seems your goodman's not back yet?


ANÍSYA.
No.

NEIGHBOUR.
It's time he was. Hasn't he perhaps stopped at our inn? My sister, Thekla, says there's heaps of sledges standing there as have come from the town.

ANÍSYA.
Nan! Nan, I say!

NAN.
Yes?

ANÍSYA.
You run to the inn and see! Mayhap, being drunk, he's gone there.


NAN
[jumps down from the oven and dresses] All right.

NEIGHBOUR.
And he's taken Akoulína with him?

ANÍSYA.
Else he'd not have had any need of going. It's because of her he's unearthed all the business there. “Must go to the bank,” he says; “it's time to receive the payments,” he says. But it's all her fooling.

NEIGHBOUR
[shakes her head] It's a bad look-out. [Silence].

NAN
[at the door] And if he's there, what am I to say?

ANÍSYA.
You only see if he's there.

NAN.
All right. I'll be back in a winking. [Long silence].

MÍTRITCH
[roars] Oh Lord! merciful Nicholas!

NEIGHBOUR
[starting] Oh, how he scared me? Who is it?

ANÍSYA.
Why, Mítritch, our labourer.

NEIGHBOUR.
Oh dear, oh dear, what a fright he did give me! I had quite forgotten. But tell me, dear, I've heard someone's been wooing Akoulína?

ANÍSYA
[gets up from the loom and sits down by the table] There was some one from Dédlovo; but it seems the affair's got wind there too. They made a start, and then stopped; so the thing fell through. Of course, who'd care to?

NEIGHBOUR.
And the Lizounófs from Zoúevo?

ANÍSYA.
They made some steps too, but it didn't come off either. They won't even see us.

NEIGHBOUR.
Yet it's time she was married.

ANÍSYA.
Time and more than time! Ah, my dear, I'm that impatient to get her out of the house; but the matter does not come off. He does not wish it, nor she either. He's not yet had enough of his beauty, you see.

NEIGHBOUR.
Eh, eh, eh, what doings! Only think of it. Why, he's her step-father!

ANÍSYA.
Ah, friend, they've taken me in completely. They've done me so fine it's beyond saying. I, fool that I was, noticed nothing, suspected nothing, and so I married him. I guessed nothing, but they already understood one another.

NEIGHBOUR.
Oh dear, what goings on!

ANÍSYA.
So it went on from bad to worse, and I see they begin hiding from me. Ah, friend, I was that sick—that sick of my life! It's not as if I didn't love him.

NEIGHBOUR.
That goes without saying.

ANÍSYA.
Ah, how hard it is to bear such treatment from him! Oh, how it hurts!

NEIGHBOUR.
Yes, and I've heard say he's becoming too free with his fists?

ANÍSYA.
And that too! There was a time when he was gentle when he'd had a drop. He used to hit out before, but of me he was always fond! But now when he's in a temper he goes for me and is ready to trample me under his feet. The other day he got both hands entangled in my hair so that I could hardly get away. And the girl's worse than a serpent; it's a wonder the earth bears such furies.

NEIGHBOUR.
Ah, ah, my dear, now I look at you, you are a sufferer! To suffer like that is no joke. To have given shelter to a beggar, and he to lead you such a dance! Why don't you pull in the reins?

ANÍSYA.
Ah, but my dear, if it weren't for my heart! Him as is gone was stern enough, still I could twist him about any way I liked; but with this one I can do nothing. As soon as I see him all my anger goes. I haven't a grain of courage before him; I go about like a drowned hen.

NEIGHBOUR.
Ah, neighbour, you must be under a spell. I've heard that Matryóna goes in for that sort of thing. It must be her.

ANÍSYA.
Yes, dear; I think so myself sometimes. Gracious me, how hurt I feel at times! I'd like to tear him to pieces. But when I set eyes on him, my heart won't go against him.

NEIGHBOUR.
It's plain you're bewitched. It don't take long to blight a body. There now, when I look at you, what you have dwindled to!

ANÍSYA.
Growing a regular spindle-shanks. And just look at that fool Akoulína. Wasn't the girl a regular untidy slattern, and just look at her now! Where has it all come from? Yes, he has fitted her out. She's grown so smart, so puffed up, just like a bubble that's ready to burst. And, though she's a fool, she's got it into her head, “I'm the mistress,” she says; “the house is mine; it's me father wanted him to marry.” And she's that vicious! Lord help us, when she gets into a rage she's ready to tear the thatch off the house.

NEIGHBOUR.
Oh dear, what a life yours is, now I come to look at you. And yet there's people envying you: “They're rich,” they say; but it seems that gold don't keep tears from falling.

ANÍSYA.
Much reason for envy indeed! And the riches, too, will soon be made ducks and drakes of. Dear me, how he squanders money!

NEIGHBOUR.
But how's it, dear, you've been so simple to give up the money? It's yours.

ANÍSYA.
Ah, if you knew all! The thing is that I've made one little mistake.

NEIGHBOUR.
Well, if I were you, I'd go straight and have the law of him. The money's yours; how dare he squander it? There's no such rights.

ANÍSYA.
They don't pay heed to that nowadays.

NEIGHBOUR.
Ah, my dear, now I come to look at you, you've got that weak.

ANÍSYA.
Yes, quite weak, dear, quite weak. He's got me into a regular fix. I don't myself know anything. Oh, my poor head!

NEIGHBOUR
[listening] There's someone coming, I think. [The door opens and Akím enters].

AKÍM
[crosses himself, knocks the snow off his feet, and takes off his coat] Peace be to this house! How do you do? Are you well, daughter?

ANÍSYA.
How d'you do, father? Do you come straight from home?

AKÍM.
I've been a-thinking, I'll go and see what's name, go to see my son, I mean,—my son. I didn't start early—had my dinner, I mean; I went, and it's so what d'you call it—so snowy, hard walking, and so there I'm what d'you call it—late, I mean. And my son—is he at home? At home? My son, I mean.

ANÍSYA.
No; he's gone to the town.

AKÍM
[sits down on a bench] I've some business with him, d'you see, some business, I mean. I told him t'other day, told him I was in need—told him, I mean, that our horse was done for, our horse, you see. So we must what d'ye call it, get a horse, I mean, some kind of a horse, I mean. So there, I've come, you see.

ANÍSYA.
Nikíta told me. When he comes back you'll have a talk. [Goes to the oven] Have some supper now, and he'll soon come. Mítritch, eh Mítritch, come have your supper.

MÍTRITCH.
Oh Lord! merciful Nicholas!

ANÍSYA.
Come to supper.

NEIGHBOUR.
I shall go now. Good-night. [Exit].

MÍTRITCH
[gets down from the oven] I never noticed how I fell asleep. Oh Lord! gracious Nicholas! How d'you do, Daddy Akím?

AKÍM.
Ah, Mítritch! What are you, what d'ye call it, I mean?…

MÍTRITCH.
Why, I'm working for your son, Nikíta.

AKÍM.
Dear me! What d'ye call … working for my son, I mean. Dear me!

MÍTRITCH.
I was living with a tradesman in town, but drank all I had there. Now I've come back to the village. I've no home, so I've gone into service. [Gapes] Oh Lord!

AKÍM.
But how's that, what d'you call it, or what's name, Nikíta, what does he do? Has he some business, I mean besides, that he should hire a labourer, a labourer I mean, hire a labourer?

ANÍSYA.
What business should he have? He used to manage, but now he's other things on his mind, so he's hired a labourer.

MÍTRITCH.
Why shouldn't he, seeing he has money?

AKÍM.
Now that's what d'you call it, that's wrong, I mean, quite wrong, I mean. That's spoiling oneself.

ANÍSYA.
Oh, he has got spoilt, that spoilt, it's just awful.

AKÍM.
There now, what d'you call it, one thinks how to make things better, and it gets worse I mean. Riches spoil a man, spoil, I mean.

MÍTRITCH.
Fatness makes even a dog go mad; how's one not to get spoilt by fat living? Myself now; how I went on with fat living. I drank for three weeks without being sober. I drank my last breeches. When I had nothing left, I gave it up. Now I've determined not to. Bother it!

AKÍM.
And where's what d'you call, your old woman?

MÍTRITCH.
My old woman has found her right place, old fellow. She's hanging about the gin-shops in town. She's a swell too; one eye knocked out, and the other black, and her muzzle twisted to one side. And she's never sober; drat her!

AKÍM.
Oh, oh, oh, how's that?

MÍTRITCH.
And where's a soldier's wife to go? She has found her right place. [Silence].

AKÍM
[to Anísya] And Nikíta,—has he what d'you call it, taken anything up to town? I mean, anything to sell?

ANÍSYA
[laying the table and serving up] No, he's taken nothing. He's gone to get money from the bank.

AKÍM
[sitting down to supper] Why? D'you wish to put it to another use, the money I mean?

ANÍSYA.
No, we don't touch it. Only some twenty or thirty roubles as have come due; they must be taken.

AKÍM.
Must be taken. Why take it, the money I mean? You'll take some to-day I mean, and some to-morrow; and so you'll what d'you call it, take it all, I mean.

ANÍSYA.
We get this besides. The money is all safe.

AKÍM.
All safe? How's that, safe? You take it, and it what d'you call it, it's all safe. How's that? You put a heap of meal into a bin, or a barn, I mean, and go on taking meal, will it remain there what d'you call it, all safe I mean? That's, what d'you call it, it's cheating. You'd better find out, or else they'll cheat you. Safe indeed! I mean you what d'ye call … you take it and it remains all safe there?

ANÍSYA.
I know nothing about it. Iván Moséitch advised us at the time. “Put the money in the bank,” he said, “the money will be safe, and you'll get interest,” he said.

MÍTRITCH
[having finished his supper] That's so. I've lived with a tradesman. They all do like that. Put the money in the bank, then lie down on the oven and it will keep coming in.

AKÍM.
That's queer talk. How's that—what d'ye call, coming in, how's that coming in, and they, who do they get it from I mean, the money I mean?

ANÍSYA.
They take the money out of the bank.

MÍTRITCH.
Get along! 'Tain't a thing a woman can understand! You look here, I'll make it all clear to you. Mind and remember. You see, suppose you've got some money, and I, for instance, have spring coming on, my land's idle, I've got no seeds, or I have to pay taxes. So, you see, I go to you. “Akím,” I say, “give us a ten-rouble note, and when I've harvested in autumn I'll return it, and till two acres for you besides, for having obliged me!” And you, seeing I've something to fall back on—a horse say, or a cow—you say, “No, give two or three roubles for the obligation,” and there's an end of it. I'm stuck in the mud, and can't do without. So I say, “All right!” and take a tenner. In the autumn, when I've made my turnover, I bring it back, and you squeeze the extra three roubles out of me.

AKÍM.
Yes, but that's what peasants do when they what d'ye call it, when they forget God. It's not honest, I mean, it's no good, I mean.

MÍTRITCH.
You wait. You'll see it comes just to the same thing. Now don't forget how you've skinned me. And Anísya, say, has got some money lying idle. She does not know what to do with it, besides, she's a woman, and does not know how to use it. She comes to you. “Couldn't you make some profit with my money too?” she says. “Why not?” say you, and you wait. Before the summer I come again and say, “Give me another tenner, and I'll be obliged.” Then you find out if my hide isn't all gone, and if I can be skinned again you give me Anísya's money. But supposing I'm clean shorn,—have nothing to eat,—then you see I can't be fleeced any more, and you say, “Go your way, friend,” and you look out for another, and lend him your own and Anísya's money and skin him. That's what the bank is. So it goes round and round. It's a cute thing, old fellow!

AKÍM
[excitedly] Gracious me, whatever is that like? It's what d'ye call it, it's filthy! The peasants—what d'ye call it, the peasants do so I mean, and know it's, what d'ye call it, a sin! It's what d'you call, not right, not right, I mean. It's filthy! How can people as have learnt … what d'ye call it …

MÍTRITCH.
That, old fellow, is just what they're fond of! And remember, them that are stupid, or the women folk, as can't put their money into use themselves, they take it to the bank, and they there, deuce take 'em, clutch hold of it, and with this money they fleece the people. It's a cute thing!

AKÍM
[sighing] Oh dear, I see, what d'ye call it, without money it's bad, and with money it's worse! How's that? God told us to work, but you, what d'ye call … I mean you put money into the bank and go to sleep, and the money will what d'ye call it, will feed you while you sleep. It's filthy, that's what I call it; it's not right.

MÍTRITCH.
Not right? Eh, old fellow, who cares about that nowadays? And how clean they pluck you, too! That's the fact of the matter.

AKÍM
[sighs] Ah yes, seems the time's what d'ye call it, the time's growing ripe. There, I've had a look at the closets in town. What they've come to! It's all polished and polished I mean, it's fine, it's what d'ye call it, it's like inside an inn. And what's it all for? What's the good of it? Oh, they've forgotten God. Forgotten, I mean. We've forgotten, forgotten God, God I mean! Thank you, my dear, I've had enough. I'm quite satisfied. [Rises. Mítritch climbs on to the oven].

ANÍSYA
[eats, and collects the dishes] If his father would only take him to task! But I'm ashamed to tell him.

AKÍM.
What d'you say?

ANÍSYA.
Oh! it's nothing.

Enter Nan.

AKÍM.
Here's a good girl, always busy! You're cold, I should think?

NAN.
Yes, I am, terribly. How d'you do, grandfather?

ANÍSYA.
Well? Is he there?

NAN.
No. But Andriyán is there. He's been to town, and he says he saw them at an inn in town. He says Dad's as drunk as drunk can be!

ANÍSYA.
Do you want anything to eat? Here you are.

NAN
[goes to the oven] Well, it is cold. My hands are quite numb. [Akím takes off his leg-bands and bast-shoes. Anísya washes up].

ANÍSYA.
Father!

AKÍM.
Well, what is it?

ANÍSYA.
And is Marína living well?

AKÍM.
Yes, she's living all right. The little woman is what d'ye call it, clever and steady; she's living, and what d'ye call it, doing her best. She's all right; the little woman's of the right sort I mean; painstaking and what d'ye call it, submissive; the little woman's all right I mean, all right, you know.

ANÍSYA.
And is there no talk in your village that a relative of Marína's husband thinks of marrying our Akoulína? Have you heard nothing of it?

AKÍM.
Ah; that's Mirónof. Yes, the women did chatter something. But I didn't pay heed, you know. It don't interest me I mean, I don't know anything. Yes, the old women did say something, but I've a bad memory, bad memory, I mean. But the Mirónofs are what d'ye call it, they're all right, I mean they're all right.

ANÍSYA.
I'm that impatient to get her settled.

AKÍM.
And why?

NAN
[listens] They've come!

ANÍSYA.
Well, don't you go bothering them. [Goes on washing the spoons without turning her head].

NIKÍTA
[enters] Anísya! Wife! who has come? [Anísya looks up and turns away in silence].

NIKÍTA
[severely] Who has come? Have you forgotten?

ANÍSYA.
Now don't humbug. Come in!

NIKÍTA
[still more severely] Who's come?

ANÍSYA
[goes up and takes him by the arm] Well then, husband has come. Now then, come in!

NIKÍTA
[holds back] Ah, that's it! Husband! And what's husband called? Speak properly.

ANÍSYA.
Oh bother you! Nikíta!

NIKÍTA.
Where have you learnt manners? The full name.

ANÍSYA.
Nikíta Akímitch! Now then!

NIKÍTA
[still in the doorway] Ah, that's it! But now—the surname?

ANÍSYA
[laughs and pulls him by the arm] Tchilíkin. Dear me, what airs!

NIKÍTA.
Ah, that's it. [Holds on to the door-post] No, now say with which foot Tchilíkin steps into this house!

ANÍSYA.
That's enough! You're letting the cold in!

NIKÍTA.
Say with which foot he steps? You've got to say it,—that's flat.

ANÍSYA
[aside] He'll go on worrying. [To Nikíta] Well then, with the left. Come in!

NIKÍTA.
Ah, that's it.

ANÍSYA.
You look who's in the hut!

NIKÍTA.
Ah, my parent! Well, what of that? I'm not ashamed of my parent. I can pay my respects to my parent. How d'you do, father? [Bows and puts out his hand] My respects to you.


ANÍSYA.
Come in!

NIKÍTA.
Ah, that's it.

ANÍSYA.
You look who's in the hut!

NIKÍTA.
Ah, my parent! Well, what of that? I'm not ashamed of my parent.

AKÍM <
i>[does not answer] Drink, I mean drink, what it does! It's filthy!

NIKÍTA.
Drink, what's that? I've been drinking? I'm to blame, that's flat! I've had a glass with a friend, drank his health.

ANÍSYA.
Go and lie down, I say.

NIKÍTA.
Wife, say where am I standing?

ANÍSYA.
Now then, it's all right, lie down!

NIKÍTA.
No, I'll first drink a samovár with my parent. Go and light the samovár. Akoulína, I say, come here!

Enter Akoulína, smartly dressed and carrying their purchases.

AKOULÍNA.
Why have you thrown everything about? Where's the yarn?

NIKÍTA.
The yarn? The yarn's there. Hullo, Mítritch, where are you? Asleep? Asleep? Go and put the horse up.

AKÍM
[not seeing Akoulína but looking at his son] Dear me, what is he doing? The old man's what d'ye call it, quite done up, I mean,—been thrashing,—and look at him, what d'ye call it, putting on airs! Put up the horse! Faugh, what filth!

MÍTRITCH
[climbs down from the oven, and puts on felt boots] Oh, merciful Lord! Is the horse in the yard? Done it to death, I dare say. Just see how he's been swilling, the deuce take him. Up to his very throat. Oh Lord, holy Nicholas! [Puts on sheepskin, and exit].

NIKÍTA
[sits down] You must forgive me, father. It's true I've had a drop; well, what of that? Even a hen will drink. Ain't it true? So you must forgive me. Never mind Mítritch, he doesn't mind, he'll put it up.

ANÍSYA.
Shall I really light the samovár?

NIKÍTA.
Light it! My parent has come. I wish to talk to him, and shall drink tea with him. [To Akoulína] Have you brought all the parcels?

AKOULÍNA.
The parcels? I've brought mine, the rest's in the sledge. Hi, take this, this isn't mine!

Throws a parcel on the table and puts the others into her box. Nan watches her while she puts them away. Akím does not look at his son, but puts his leg-bands and bast-shoes on the oven.

ANÍSYA
[going out with the samovár] Her box is full as it is, and still he's bought more!


NIKÍTA
Have you brought all the parcels?

AKOULÍNA.
The parcels? I've brought mine, the rest's in the sledge.

ANÍSYA.
Her box is full as it is, and still he's bought more!

NIKÍTA
[pretending to be sober] You must not be cross with me, father. You think I'm drunk? I am all there, that's flat! As they say, “Drink, but keep your wits about you.” I can talk with you at once, father. I can attend to any business. You told me about the money; your horse is worn-out,—I remember! That can all be managed. That's all in our hands. If it was an enormous sum that's wanted, then we might wait; but as it is I can do everything. That's the case.

AKÍM
[goes on fidgeting with the leg-bands] Eh, lad, “It's ill sledging when the thaw has set in.”

NIKÍTA.
What d'you mean by that? “And it's ill talking with one who is drunk”? But don't you worry, let's have some tea. And I can do anything; that's flat! I can put everything to rights.

AKÍM
[shakes his head] Eh, eh, eh!

NIKÍTA.
The money, here it is. [Puts his hand in his pocket, pulls out pocket-book, handles the notes in it and takes out a ten-rouble note] Take this to get a horse; I can't forget my parent. I shan't forsake him, that's flat. Because he's my parent! Here you are, take it! Really now, I don't grudge it. [Comes up and pushes the note towards Akím who won't take it. Nikíta catches hold of his father's hand] Take it, I tell you. I don't grudge it.

AKÍM.
I can't, what d'you call it, I mean, can't take it! And can't what d'ye call it, talk to you, because you're not yourself, I mean.

NIKÍTA.
I'll not let you go! Take it! [Puts the money into Akím's hand].

ANÍSYA
[enters, and stops] You'd better take it, he'll give you no peace!

AKÍM
[takes it, and shakes his head] Oh! that liquor. Not like a man, I mean!

NIKÍTA.
That's better! If you repay it you'll repay it, if not I'll make no bother. That's what I am! [Sees Akoulína] Akoulína, show your presents.

AKOULÍNA.
What?

NIKÍTA.
Show your presents.

AKOULÍNA.
The presents, what's the use of showing 'em? I've put 'em away.

NIKÍTA.
Get them, I tell you. Nan will like to see 'em. Undo the shawl. Give it here.

AKÍM.
Oh, oh! It's sickening! [Climbs on the oven].

AKOULÍNA
[gets out the parcels and puts them on the table] Well, there you are,—what's the good of looking at 'em?

NAN.
Oh how lovely! It's as good as Stepanída's.

AKOULÍNA.
Stepanída's? What's Stepanída's compared to this? [Brightening up and undoing the parcels] Just look here,—see the quality! It's a French one.

NAN.
The print is fine! Mary has a dress like it, only lighter on a blue ground. This is pretty.

NIKÍTA.
Ah, that's it!

Anísya passes angrily into the closet, returns with a tablecloth and the chimney of the samovár, and goes up to the table.

ANÍSYA.
Drat you, littering the table!

NIKÍTA.
You look here!

ANÍSYA.
What am I to look at? Have I never seen anything? Put it away! [Sweeps the shawl on to the floor with her arm].

AKOULÍNA.
What are you pitching things down for? You pitch your own things about! [Picks up the shawl].

NIKÍTA.
Anísya! Look here!

ANÍSYA.
Why am I to look?

NIKÍTA.
You think I have forgotten you? Look here! [Shows her a parcel and sits down on it] It's a present for you. Only you must earn it! Wife, where am I sitting?

ANÍSYA.
Enough of your humbug. I'm not afraid of you. Whose money are you spreeing on and buying your fat wench presents with? Mine!

AKOULÍNA.
Yours indeed? No fear! You wished to steal it, but it did not come off! Get out of the way! [Pushes her while trying to pass].

ANÍSYA.
What are you shoving for? I'll teach you to shove!

AKOULÍNA.
Shove me? You try! [Presses against Anísya].

NIKÍTA.
Now then, now then, you women. Have done now! [Steps between them].

AKOULÍNA.
Comes shoving herself in! You ought to keep quiet and remember your doings! You think no one knows!

ANÍSYA.
Knows what? Out with it, out with it! What do they know?

AKOULÍNA.
I know something about you!

ANÍSYA.
You're a slut who goes with another's husband!

AKOULÍNA.
And you did yours to death!

ANÍSYA
[throwing herself on Akoulína] You're raving!

NIKÍTA
[holding her back] Anísya, you seem to have forgotten!

ANÍSYA.
Want to frighten me! I'm not afraid of you!

NIKÍTA
[turns Anísya round and pushes her out] Be off!

ANÍSYA.
Where am I to go? I'll not go out of my own house!

NIKÍTA.
Be off, I tell you, and don't dare to come in here!

ANÍSYA.
I won't go! [Nikíta pushes her, Anísya cries and screams and clings to the door] What! am I to be turned out of my own house by the scruff of the neck? What are you doing, you scoundrel? Do you think there's no law for you? You wait a bit!

NIKÍTA.
Now then!

ANÍSYA.
I'll go to the Elder! To the policeman!

NIKÍTA.
Off, I tell you! [Pushes her out].

ANÍSYA
[behind the door] I'll hang myself!

NIKÍTA.
No fear!

NAN.
Oh, oh, oh! Mother, dear, darling! [Cries].

NIKÍTA.
Me frightened of her! A likely thing! What are you crying for? She'll come back, no fear. Go and see to the samovár. [Exit Nan].

AKOULÍNA
[collects and folds her presents] The mean wretch, how she's messed it up. But wait a bit, I'll cut up her jacket for her! Sure I will!

NIKÍTA.
I've turned her out, what more do you want?

AKOULÍNA.
She's dirtied my new shawl. If that bitch hadn't gone away, I'd have torn her eyes out!

NIKÍTA.
That's enough. Why should you be angry? Now if I loved her …

AKOULÍNA.
Loved her? She's worth loving, with her fat mug! If you'd have given her up, then nothing would have happened. You should have sent her to the devil. And the house was mine all the same, and the money was mine! Says she is the mistress, but what sort of mistress is she to her husband? She's a murderess, that's what she is! She'll serve you the same way!

NIKÍTA.
Oh dear, how's one to stop a woman's jaw? You don't yourself know what you're jabbering about!

AKOULÍNA.
Yes, I do. I'll not live with her! I'll turn her out of the house! She can't live here with me. The mistress indeed! She's not the mistress,—that jailbird!

NIKÍTA.
That's enough! What have you to do with her? Don't mind her. You look at me! I am the master! I do as I like. I've ceased to love her, and now I love you. I love who I like! The power is mine, she's under me. That's where I keep her. [Points to his feet] A pity we've no concertina. [Sings].

           &nbsp“We have loaves on the stoves,
           &nbspWe have porridge on the shelf.
           &nbspSo we'll live and be gay,
           &nbspMaking merry every day,
           &nbspAnd when death comes,
           &nbspThen we'll die!
           &nbspWe have loaves on the stoves,
           &nbspWe have porridge on the shelf …”

Enter Mítritch. He takes off his outdoor things and climbs on the oven.

MÍTRITCH.
Seems the women have been fighting again! Tearing each other's hair. Oh Lord, gracious Nicholas!

AKÍM
[sitting on the edge of the oven, takes his leg-bands and shoes and begins putting them on] Get in, get into the corner.

MÍTRITCH. S
eems they can't settle matters between them. Oh Lord!

NIKÍTA.
Get out the liquor, we'll have some with our tea.

NAN
[to Akoulína] Sister, the samovár is just boiling over.

NIKÍTA.
And where's your mother?

NAN.
She's standing and crying out there in the passage.

NIKÍTA.
Oh, that's it! Call her, and tell her to bring the samovár. And you, Akoulína, get the tea things.

AKOULÍNA.
The tea things? All right. [Brings the things].

NIKÍTA
[unpacks spirits, rusks, and salt herrings] That's for myself. This is yarn for the wife. The paraffin is out there in the passage, and here's the money. Wait a bit, [takes a counting-frame] I'll add it up. [Adds] Wheat-flour,  kopéykas, oil … Father,  roubles.… Father, come let's have some tea!

Silence. Akím sits on the oven and winds the bands round his legs. Enter Anísya with samovár.

ANÍSYA.
Where shall I put it?

NIKÍTA.
Here on the table. Well! have you been to the Elder? Ah, that's it! Have your say and then eat your words. Now then, that's enough. Don't be cross, sit down and drink this. [Fills a wine-glass for her] And here's your present. [Gives her the parcel he had been sitting on. Anísya takes it silently and shakes her head].

AKÍM
[gets down and puts on his sheepskin, then comes up to the table and puts down the money] Here, take your money back! Put it away.

NIKÍTA
[does not see the money] Why have you put on your things?

AKÍM.
I'm going, going I mean; forgive me for the Lord's sake. [Takes up his cap and belt].

NIKÍTA.
My gracious! Where are you going to at this time of night?

AKÍM.
I can't, I mean what d'ye call 'em, in your house, what d'ye call 'em, can't stay I mean, stay, can't stay, forgive me.

NIKÍTA.
But are you going without having any tea?

AKÍM
[fastens his belt] Going, because, I mean, it's not right in your house, I mean, what d'you call it, not right, Nikíta, in the house, what d'ye call it, not right! I mean, you are living a bad life, Nikíta, bad,—I'll go.

NIKÍTA.
Eh now! Have done talking! Sit down and drink your tea!

ANÍSYA.
Why, father, you'll shame us before the neighbours. What has offended you?

AKÍM.
Nothing what d'ye call it, nothing has offended me, nothing at all! I mean only, I see, what d'you call it, I mean, I see my son, to ruin I mean, to ruin, I mean my son's on the road to ruin, I mean.

NIKÍTA.
What ruin? Just prove it!

AKÍM.
Ruin, ruin; you're in the midst of it! What did I tell you that time?

NIKÍTA.
You said all sorts of things!

AKÍM.
I told you, what d'ye call it, I told you about the orphan lass. That you had wronged an orphan—Marína, I mean, wronged her!

NIKÍTA.
Eh! he's at it again. Let bygones be bygones … All that's past!

AKÍM
[excited] Past! No, lad, it's not past. Sin, I mean, fastens on to sin—drags sin after it, and you've stuck fast, Nikíta, fast in sin! Stuck fast in sin! I see you're fast in sin. Stuck fast, sunk in sin, I mean!

NIKÍTA.
Sit down and drink your tea, and have done with it!

AKÍM. I can't, I mean can't what d'ye call it, can't drink tea. Because of your filth, I mean; I feel what d'ye call it, I feel sick, very sick! I can't what d'ye call it, I can't drink tea with you.


NIKÍTA.
Eh! There he goes rambling! Come to the table.

AKÍM.
You're in your riches same as in a net—you're in a net, I mean. Ah, Nikíta, it's the soul that God needs!

NIKÍTA.
Now really, what right have you to reprove me in my own house? Why do you keep on at me? Am I a child that you can pull by the hair? Nowadays those things have been dropped!

AKÍM.
That's true. I have heard that nowadays, what d'ye call it, that nowadays children pull their fathers' beards, I mean! But that's ruin, that's ruin, I mean!

NIKÍTA
[angrily] We are living without help from you, and it's you who came to us with your wants!

AKÍM.
The money? There's your money! I'll go begging, begging I mean, before I'll take it, I mean.

NIKÍTA.
That's enough! Why be angry and upset the whole company! [Holds him by the arm].

AKÍM
[shrieks] Let go! I'll not stay. I'd rather sleep under some fence than in the midst of your filth! Faugh! God forgive me!

[Exit].

NIKÍTA.
Here's a go!

AKÍM
[reopens the door] Come to your senses, Nikíta! It's the soul that God wants! [Exit].


AKOULÍNA
[takes cups] Well, shall I pour out the tea? [Takes a cup. All are silent].

MÍTRITCH
[roars] Oh Lord, be merciful to me a sinner! [All start].

NIKÍTA
[lies down on the bench] Oh, it's dull, it's dull! [To Akoulína] Where's the concertina?

AKOULÍNA.
The concertina? He's bethought himself of it. Why, you took it to be mended. I've poured out your tea. Drink it!

NIKÍTA.
I don't want it! Put out the light … Oh, how dull I feel, how dull! [Sobs].