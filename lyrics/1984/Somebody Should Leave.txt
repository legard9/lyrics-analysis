[Intro]
Should I chop 'em? Time to cut 'em
Maybe shoot 'em? Kill 'em all!
Use an ax or a gun
A knife or a chainsaw
I'm gonna kill somebody!
I wanna fucking kill somebody!
Should I chop 'em? Time to cut 'em
Maybe shoot 'em? Kill 'em all!
Use an ax or a gun
A knife, a chainsaw

[Hook]
I don't know why everytime I open my eyes
I just wanna kill somebody, I wanna kill somebody
K-k-kill somebody, I wanna kill, kill, kill
I don't know where, how the fuck, or why should I care
I just wanna kill somebody, k-k-k-kill
K-k-k-kill somebody, I wanna kill, kill, kill

[Verse 1: Monoxide]
I can't explain it, cause most of the time I was so faded
I was surprised either one of us made it
I've been walking on the edge for so long the line's faded
Murder-go-round, well I've played it
They never found anyone I used to play with
But they're all afraid to go and make a statement
Cuz I sneak off in the basement in your place when no one's home
And kill the first of many faces until he fucking knows
And I like going to your neighbor's
And dress up all in their clothes
When I destroy you and your family it's really uncomfortable
I know, I know, you're thinking it's a mistake
You're thinking that there's no reason
And you've never seen my face
You're right, a hundred percent I'm alright
There's no reason other than you have a house I wanted to try
So believing in something deeper's a waste of our time
Now on with the crime

[Hook]
I don't know why everytime I open my eyes
I just wanna kill somebody, I wanna kill somebody
K-k-kill somebody, I wanna kill, kill, kill
I don't know where motherfucker why should I care
I just wanna kill somebody, k-k-k-kill
K-k-k-kill somebody, I wanna kill, kill, kill

[Verse 2: Jamie Madrox]
Can I get an A.P.B. on a maniac
Referred on the news as a freak of night
Mehron clown white with a butcher knife
He escaped last night killing who he likes
He picks his victims and hits 'em then picks 'em up
And then loads their unconscious body discreetly inside the trunk
Savor the flavor it'll probably be better to kill them right there
When nobody's looking you're stabbing repeatedly
Get it all over with and nobody cares
But I'm an old-fashioned guy
Gotta get them teeth and both them eyes
Cut off the hands and watch them die
Look 'em in the eye and say "goodbye"
There's must be something wrong with me
I haven't slept in weeks, I'm eating blood, drinking coffee
Back up off me and fuck sleep
Counting sheep, fuck a sheep I kill 'em too
Wear 'em like a nanny goat, lunatic, thought you knew
Now somebody gotta d-i-e, I’m k-i-l-l-i-n-g
Everyone in here 'fore I f-u-c-k-i-n leave
Schizo-crazy I believe a thousand deaths alive in me
They all let go every time I breathe
Then they whisper at night "Murder for me!"

[Hook]
I don't know why everytime I open my eyes
I just wanna kill somebody, I wanna kill somebody
K-k-kill somebody, I wanna kill, kill, kill
I don't know where, how the fuck, or why should I care
I just wanna kill somebody, k-k-k-kill
K-k-k-kill somebody, I wanna kill, kill, kill

[Outro]
Should I chop 'em? Time to cut 'em
Maybe shoot 'em? Kill 'em all!
Use an ax or a gun
A knife or a chainsaw
I'm gonna kill somebody!
I wanna fucking kill somebody!
Should I chop 'em? Time to cut 'em
Maybe shoot 'em? Kill 'em all!
Use an ax or a gun
A knife, a chainsaw
I'm gonna kill somebody!
I wanna fucking kill somebody!

[Hook]
I don't know why everytime I open my eyes
I just wanna kill somebody, I wanna kill somebody
K-k-kill somebody, I wanna kill, kill, kill
I don't know where, how the fuck, or why should I care
I just wanna kill somebody, k-k-k-kill
K-k-k-kill somebody, I wanna kill, kill, kill