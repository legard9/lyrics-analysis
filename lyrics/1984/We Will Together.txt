Little kitty mine, mine won't you sing your song?
Little kitty mine and I'll sing along
We will be together everyday and night
Little kitty mine, mine makes me feel so fine

My kitty is so pretty no one can compete
She is always with me, my kitty is so sweet
But ever I'm sad she will be the one
To cheer me up by singing your song

Little kitty mine, mine won't you sing your song?
Little kitty mine and I'll sing along
We will be together everyday and night
Little kitty mine, mine makes me feel so fine

I like to walk the city but never on my own
With my fine little kitty, I never feel alone
If I'm too far away, and feeling blue
I always find my way back thanks to you

Little kitty mine, mine won't you sing your song?
Little kitty mine and I'll sing along
We will be together everyday and night
Little kitty mine, mine makes me feel so fine

NaNaNaNaNaNa
NaNaNaNaNa
We will be together everyday and night
Little kitty mine, mine makes me feel so fine