[Verse 1]
I was feelin' that, feelin' that breeze
Singin' like a song through the tall oak trees
It was just another summer night
Had to be the last thing on my mind
Yeah, I was all but lost in the moment
I was young and runnin' wide open
It was just another summer night
Had to be the last thing on my mind

[Chorus]
When love broke through
You found me in the darkness
Wanderin' through the desert
I was a hopeless fool
Now I'm hopelessly devoted
My chains are broken
And it all began with You
When love broke through
And it all began with You
When love broke through

[Verse 2]
I did all that I could to undo me
But You loved me enough to pursue me
Yeah, You drew me out of the shadows
Made me believe that I mattered, to You, You
You were there
You heard my prayer in that broke down dusty room
It was the first time I said, "I'm Yours"
The first time I called You Lord

[Chorus]
When love broke through
You found me in the darkness
Wanderin' through the desert
I was a hopeless fool
Now I'm hopelessly devoted
My chains are broken
And it all began with You
When love broke through
And it all began with You
When love broke through

[Verse 3]
Yeah, it was late in the summer when the northeast breeze
Sang like a song through the oak trees
Pennsylvania, she kind of caught my soul
Which had me a little more open than closed
Walls I built, opinions I learned
Covered in the ashes of bridges I burned
Blind to the arrow that was headed to my heart
But You hit the mark

[Chorus]
When love broke through
You found me in the darkness
Wanderin' through the desert
I was a hopeless fool
Now I'm hopelessly devoted
My chains are broken
And it all began with You
When love broke through
And it all began with You
When love broke through

[Outro]
I did all that I could to undo me
But You loved me enough to pursue me