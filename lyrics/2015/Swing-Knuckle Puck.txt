[Verse 1]
I’ve been feeling stagnant and cracked underneath
I try to sleep it off but I can feel it in my dreams
I’ll give them what they wanna see
A kid lost in his twenties
Oh, what a sight to see

[Bridge]
Constant pressure weighing down on me
It gets better, they want me to believe
Constant pressure weighing down on me
It gets better, they want me to believe

[Pre-Chorus]
Not much to show for this time spent alone
I swing but I miss every time

[Chorus]
Not much to show for this time spent alone
I swing but I miss every time
There’s so much fight left inside, and I’ve fought the good fight
But I just can't let this one go

[Verse 2]
Feel the rush of blood beneath the skin
Can’t keep myself from dwelling on this like you did
Cause it’s so dark here, more than it ever is
Close the shades, clear my head, slump back to bed again

[Bridge]
I’ve always wanted to believe that this meant something
We’ve always wanted to believe that this meant something

[Chorus]
Not much to show for this time spent alone
I swing but I miss every time
There’s so much fight inside, and I’ve fought the good fight
But I just can't let this one go

[Chorus]
Not much to show for this time spent alone
I swing but I miss every time
There’s so much fight left inside, and I’ve fought the good fight
But I just can't let this one go

[Outro]
There's so much fight left inside