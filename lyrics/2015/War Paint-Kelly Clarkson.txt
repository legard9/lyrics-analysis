[Verse 1]
Face to face, but miles away
Building trenches out of empty spaces
Lay here, next to me
And close the distance in between

[Refrain]
I'll take off yours if you take off mine
Reveal the things that we're tryna hide
Take off yours if you take off mine
Take off mine, yeah

[Chorus]
'Cause we could be beautiful without our war paint
Our war paint
And we could have it all once we let our walls break
Our walls break
Why in the hell do we fight on the front line
When we both know that we're here on the same side?
We could be beautiful without the mistake
Of our war paint

[Verse 2]
Scar to scar, I wanna know
Every story that you never told
Don't shut me out, don't think too much
Don't keep that barricade up

[Refrain]
I'll take off yours if you take off mine
Reveal the things that we're tryna hide
Take off yours if you take off mine
Take off mine, yeah

[Chorus]
'Cause we could be beautiful without our war paint
Our war paint
And we could have it all once we let our walls break
Our walls break
Why in the hell do we fight on the front line
When we both know that we're here on the same side?
We could be beautiful without the mistake
Of our war paint

[Bridge]
So hold me close and kiss my skin
Don't be afraid, let me in
I'll hold you close and take your hand
To my heart, here I am
Yeah

[Chorus]
'Cause we could be beautiful without our war paint
Our war paint
And we could have it all once we let our walls break
Our walls break
Why in the hell do we fight on the front line (Front line)
When we both know that we're here on the same side? (Same side)
We could be beautiful without the mistake
Of our war paint
'Cause we could be beautiful without our war paint
(With our war paint) Our war paint
And we could have it all once we let our walls break
Our walls break
(We could have it all) Why in the hell do we fight on the front line (The front line)
When we both know that we're here on the same side?
We could be beautiful without the mistake
Of our war paint

[Outro]
Our war paint