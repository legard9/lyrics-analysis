Kanji

[Verse 1: Nayeon, 
Mina
]
君から鳴るベルベル
ごめんまじむり
バッテリー減るの早すぎる
着信が止まらなくって
スマホがパンッ 弾けそうだよ

[Verse 2: Sana, 
Mina
]
なんでなんで私のせい？
って思うだけで胸
キュンするなんて
だけどねみんな
かわいいねって近づくの

[Pre-Chorus 1: Tzuyu, 
Sana
, 
Mina
]
Ooh, さっきの電話ごめんね
友達といて shy shy shy
まだ会えないごめんね
かけなおすから later

[Pre-Chorus 2: Momo, 
Jeongyeon
]
お願いせかさないで前のめりな baby
もう少し我慢してねよそ見させないよ

[Chorus: Jihyo, 
Nayeon
]
Cheer up baby, cheer up baby, 追いかけて
胸の扉を叩いて
今よりももっと大胆に
気がないふりして恋してるの
本当は君が好きだよ
Just get it together, and then baby cheer up!

[Interlude]
(I need you)

[Verse 2: Chaeyoung, 
Dahyun
]
そわそわしてる姿浮かぶし
ドキドキしてるの伝わるけどね
だめだめ軽いと思われるから
メッセージ届いても既読でスルー

[Verse 2: Chaeyoung, 
Dahyun
, 
All
]
(Oh oh oh)
 許してね boy
やり過ぎなのかな胸が痛いよ
(Oh oh oh)
 
どうすればいいの
夢中になっちゃう夢中になってる

[Pre-Chorus 3: Tzuyu, 
Sana
]
Oh whoa, 悩ませてごめんね
嫌いじゃないの shy shy shy
不安にしてごめんね
打ち明けるから later

[Pre-Chorus 4: Momo, 
Jeongyeon
]
こんなに苦しいのは君のせいよ baby
あと少し本気みせて奪いに来てほしい

[Chorus: Jihyo, 
Nayeon
]
Cheer up baby, cheer up baby, 会いに来て
君の気持ちを今すぐ
ありのまま全部届けてよ
これ以上私に近づいたら
恋してるオーラ隠せない
Just get it together, and then baby cheer up!

[Bridge 1: Mina, 
Jihyo
]
ねぇ傷つくの怖いだけよ
臆病な心に気づいて
君を好きな気持ちが
バレちゃう前に聞かせて
迷いをどかしてよ
Just get it together, and then baby cheer up!

[Bridge 2: All]
Be a man, a real man (yeah, yeah)
Gotta see you love me like a real man (uh huh)
Be a man, a real man (yeah, yeah)
Gotta see you love me like a real man

[Chorus: Jeongyeon, 
Jihyo
, 
Nayeon
]
Cheer up baby, cheer up baby, 追いかけて
胸の扉を叩いて
今よりももっと大胆に
気がないふりして恋してるの
本当は君が好きだよ
Just get it together, and then baby cheer up!
English Translation
 
(rough)

[Verse 1: Nayeon, Mina, Sana]
Bell bell ringing from you
I am really quite sorry, but
My battery is decreasing too fast
Incoming calls just don't stop
My smartphone seems to be able to blow
Why is it because of me?
Just thinking I'm crying
But, everyone
I'm cute so getting close

[Pre-Chorus: Tzuyu, Sana, Mina, Momo, Jeongyeon]
Ooh, sorry about my phone earlier
I was chatting to friends shy, shy, shy
Sorry I can not see you yet
Calling me again afterwards
Please do not ask me a front sweet, baby
Please be patient with me, do not let it look

[Chorus: Jihyo, Nayeon]
Cheer up baby, cheer up baby, chase after
Knock the door that is my heart
Be more bold than now
I pretend not to care and I'm in not love
But actually, I like you
Just get it together, and then baby cheer up!
(I need you)

[Verse 2: Chaeyoung, Dahyun]
I appear to be fidgeting
Though it is exciting, my heart is transmit
Because it seems to be light as it is light
Through the already read message
Oh oh oh, forgive me boy
I wonder if I'm overkilling it, My heart hurts
Oh oh oh, what should I do?
I am crazy, about getting messed up

[Pre-Chorus: Tzuyu, Sana, Momo, Jeongyeon]
Oh whoa, sorry for bothering me
I do not dislike you, shy, shy, shy
Sorry for making you uneasy
Since I can confide to you later
It's your fault, it makes me so pain-filled Baby
I hope to see you a little more seriously, come back to take it

[Chorus: Jihyo, Nayeon]
Cheer up baby, cheer up baby, come see me
Your feelings now
Deliver it all as it is
As I approach, me no more
I'm in love, my Aura can not hide it
Just get it together, and then baby cheer up!

[Bridge: Mina, Nayeon, Jihyo]
Hey I'm just scared of getting hurt
Notice my cowardly heart
I feel like loving you
Let me tell you before too long
Let me get lost
Just get it together, and then baby cheer up!

[All]
Be a man, a real man (yeah, yeah)
Gotta see you love me like a real man (uh huh)
Be a man, a real man (yeah, yeah)
Gotta see you love me like a real man

[Chorus: Jeongyeon, Nayeon]
Cheer up baby, cheer up baby, chase after
Knock the door that is my heart
Be more bold than now
I pretend not to care and I'm in not love
But actually, I like you
Just get it together, and then baby cheer up!
Romaji

[Verse 1: Nayeon, Mina, Sana]
Kimi kara naru beru beru beru
Gomen maji muri battery heru no haya sugiru
Chakushin ga tomarana kute
Suma ho ga panhhajike souda yo
Nande nande watashi no sei
Tte omouda kede mune kyun suru nante
Dakedo ne minna kawaii ne tte chikadzuku no

[Pre-Chorus: Tzuyu, Sana, Mina, Momo, Jeongyeon]
Ooh, sakki no denwa gomen ne
Tomodachi to ite shy, shy, shy
Mada aenai gomen ne
Kake nao su kara later
Onegai seka sanai de maeno merina baby
Mou sukoshi gaman shite ne yosomi sa senai yo

[Chorus: Jihyo, Nayeon]
Cheer up baby, cheer up baby, oi kakete
Mune no tobira wo tatai te
Ima yori mo motto daitan ni
Ki ga nai furi shite koishiteru no
Hontou wa kimi ga suki dayo
Just get it together, and then baby cheer up!
(I need you)

[Verse 2: Chaeyoung, Dahyun]
Sowa sowa shi teru sugata ukabushi
Doki doki shi teru no tsuta waru kedo ne
Dame dame karui to omo wa rerukara
Messeeji todoite mo kidoku de suruu
Oh oh oh, yuri shite ne boy
Yari sugina no ka na mune ga itai yo
Oh oh oh, dousureba ii no
Muchuu ni nat chau muchuu ni natteru

[Pre-Chorus: Tzuyu, Sana, Momo, Jeongyeon]
Oh whoa, nayama sete gomen ne
Kirai janai no shy, shy, shy
Fuan ni shite gomen ne
Uchia keru kara later
Konnani kurushii no wa kimi no sei yo baby
Ato sukoshi hinki misete ubai ni kite hoshii

[Chorus: Jihyo, Nayeon]
Cheer up baby, cheer up baby, ai ni kite
Kimi no kimochi o ima sugu
Arino mama zenbu todokete yo
Kore ijou watashi ni chikadzuitara
Koi shiteru ora kaku senai
Just get it together, and then baby cheer up!

[Bridge: Mina, Nayeon, Jihyo]
Nee kizutsuku no kowai dake yo
Oku byouna kokoro ni kidzuite
Kimi o sukina kimochi ga
Bare chau mae ni kikasete
Mayoi wo dokashite yo
Just get it together, and then baby cheer up!

[All]
Be a man, a real man (yeah, yeah)
Gotta see you love me like a real man (uh huh)
Be a man, a real man (yeah, yeah)
Gotta see you love me like a real man

[Chorus: Jeongyeon, Nayeon]
Cheer up baby, cheer up baby, oi kakete
Mune no tobira wo tatai te
Ima yori mo motto daitan ni
Ki ga nai furi shite koishiteru no
Hontou wa kimi ga suki dayo
Just get it together, and then baby cheer up!