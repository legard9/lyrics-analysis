[Verse 1]
Turn the page
Turn the corner
Open the cage
Cut the locks
We're starting over
I don't wanna live this way

[Chorus]
We made it through Hell and back again
We were slipping through the cracks, staring at the end
Oh, and we brave the weather
Hurricane couldn't take you from me
I'm holding on tight and I still believe
Oh, yeah, it just gets better

[Verse 2]
A quiet rage
A screaming silence is all around
Let me in
The world we built is crashing down

[Chorus]
We made it through hell and back again
We were slipping through the cracks, staring at the end
Oh, and we brave the weather
Hurricane, couldn't take you from me
I'm holding on tight and I still believe
Oh, yeah, it just gets better
It just gets better
It just gets better
It just gets better
Oooh
Oooh

[Chorus]
We made it through Hell and back again
We were slipping through the cracks, staring at the end
Oh, and we brave the weather
Hurricane couldn't take you from me
I'm holding on tight and I still believe
Oh, yeah, it just gets better
It just gets better