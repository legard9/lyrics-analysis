Steely and Matthew split up in a corridor and go into different classrooms that are stand-alone trailers.

In one class, a smooth but occasionally glitchy 3-D projection of a teacher greets Steely. The teacher is severe in appearance and manner. Though her projection corresponds to a live human being somewhere else on the planet, she might as well be a robot or at least the ghost of a human once capable of warm emotion.

     Teacher: 'Punctuality appears to be your Achilles' heel, Mr. Grude. Alas, you've cleared H3, admirable. Quiet your ears, then solve the riddle.'

The teacher trails away.

Steely exhales loudly. Takes his bag off and hangs it on a hook. There is a digital 24-hour clock in the top left corner, it's black steel with white numbers. The walls are covered in floral wallpaper, overhead there's a clear paneled mechanical ceiling. The mahogany floors ramp up to class seating, with curves and layers dynamically represented. A wall for projection wrapped in a semi circle lies at the head of the class.

Steely breathes in deeply, dropping his shoulders. The room is so quiet. He closes his eyes. A montage flashing in his head: Shoobie's mouth, himself as a small boy calling up from a pit, fencing, climbing a fence, running. A small quiet click comes into his ears from a clock. The memory of his father first forgetting while giving a lecture at his school. The silence. The stutter. The angry burst at someone touching his arm. He remembers his father not remembering him. His mother throwing things out of focus. The canopied street. The first car he stole on a spinning platter. His father's car.

Steely is mumbling a mantra and slowly his memories whitewash away. He opens his eyes and he is seated on a chair in full lotus. He breaks position and pulls a tray out of a side compartment and now he has a surface with a bowl in its center, a few gauges and a small loupe. It's what seems to be a mini solar system that he's manipulating with the gauges.

The teacher swarms back in and places a decades-old transparency paper on a projector.

The riddle:

     Teacher: 'You poured it hot and you're stirring it up, now what's moving faster, the tea or the cup?'
     Steely: 'The tea.'
     Teacher: What's harder Mr. Grude, a liquid or a solid?'
     Steely: 'Shit'
     Teacher: 'Of course. And what does your understanding of physics inform you of as it relates to softness and hardness of matter, Mr. Grude?'
     Steely: 'That, from a sub-atomic perspective, my shit is composed of particles mostly vibrating at a higher rate than my piss.'
     Teacher: 'Thank you, Mr. Grude. Your deft articulation leaves little to the imagination.'
     Steely: 'You don't have an imagination.'
     Teacher: 'Well, congratulations Mr. Grude. It has been my pleasure watering yours.'

The teacher swarms off. A status bar appears on the projection wall as if loading something. When full, a dozen other desks appear next to Steely each with a student projection seated in them, creating trails with their movements. These projections seem almost real. Unlike the microcosm whose projections are light based, the students' apparitions are hard particle nano projections whose touch have a light weight, thus they can interact with the physical world on a low level. The teacher re-materializes.

     Teacher: 'Grad students, the last movie of your lives here. "Giant"—1956. Enjoy.'

Steely looks down at his desk. His microcosm is developing well, thriving. There are 64 life harboring planets in his system. He looks through his scope. He monitors his gauges, adjusting tension and gravity here and there.

Steely receives a tummy rub via instant messaging from Shoobie, and a pouty face. He replies with a blush face. The movie plays and a couple of the students are watching. The class is 50/50 male to female. A swirl of different races and creeds. Someone's nose starts bleeding. A Japanese student crinkles plastic in a Russian girl's ear to annoy her. Two students are reading palms. Steely extends his hand, taps a blue eyed Korean kid named Kio.

     Steely: 'Howdy'
     Kio: 'Listen. It's out of control. My parents are hiring a load of lifeguards for this weekend. Apparently someone drowned last year at the pool party I've never had. Been on my Kegel exercises since last week.

(Kio's projection spreads hair on Steely's scalp with his swarm fingers and examines it before Steely shakes him away)

     Kio: 'Your hair is so clean?'
     Steely: 'Kio—get off.' (Doesn't move. Focuses his sight through the scope down into the microcosm.)
     Kio: (Speaking out loud) 'Western hygiene has lacked as fuck—these guys look dank.'
     Blonde Girl: 'Close your mouth!'
     Kio: 'Only if you keep yours open.'
     Blonde Girl: You're small.'
     Kio: 'She didn't talk to me like this last night. I'm so confused. This sucks.'
     Blonde Girl: 'Do you know I can hear you? What was last night? I'm in Morocco.'
     Kio: 'Last night, last month—whenever this loud sex was. I forget.' (Kio turns his head and is punched in the nose by Blonde's swarm hand.)
     Blonde Girl: (Warns) 'You are a small feeling. Do not!'

The downpour outside intensifies.

     Steely: 'She didn't like that one, Kio.'
     Kio: 'Bro, whatever, she's a lunatic, and she just got herself un-invited.'
     Steely: (Laughs and shakes his head.)

Dree, the Pacific islander student, taps Steely.

     Steely: 'That's TWO cartridges.'
     Dree: (Says 'okay' without saying anything.)

It thunders soon after a grand flash of lightning. Rain comes down heavier and heavier, then a black out.

The 3-D swarms of students and teacher disappear and the room goes dark. The glow of Steely's experiment is the only light source in the room. His gaze stays stuck on the system, its glow upon his face. Just then a message notice activates.

GROUP TEXT:

     Matthew: 'Anybody seen Robb? I think we lost Robb black ass.'
     Robb: A piece of me with always be inside of you.'
     Shoobie: 'lol' (emoticon)

Steely gets up from his desk, pulls his hoody over hid head and goes to use the urinals outside in the storm.

It's pouring as he walks toward a small shallow tower of urinals—they resemble a merry-go-round. He pisses with the rain falling on him hard. No one is outside. He washes his hands on a faucet above the urinal then heads back toward his class. Sunlight cuts through the rain. He gets back to his seat, soaked, and sits down in the naturally lit room. He looks down at his bowl for a moment, lights a cigarette and stares at it for a moment. Spinning it around and staring at the ember trails. The sunlight creates streaks across the classroom through the ceiling. He pushes everything back into a single point then folds the desktop back into the compartment.

Steely's eyes well up.