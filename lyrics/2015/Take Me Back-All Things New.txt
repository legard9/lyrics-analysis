[Verse 1: Marc E. Bassy]
Against the backdrop of a perfect winter day
It's twenty in NY, eighty in LA
The old me, in the past, got a new bae
When you got a broken heart you got to medicate
I got a-I got a regiment for gettin' over
It includes never sober, always looking over shoulder
It includes never growing up
And turning up in the back house doing coke off a strippers butt
Where you done went baby?
You was the only thing tying me down to this earth
Bought you a wedding ring in my mind
A thousand times over
I never pulled the trigger though
I hope you know the
Grudge I'm holding, that's pushing me to get it in
Think about these hoes, make me wanna hit the gym
Thinkin' bout you that's when I go and grab a pen

[Pre-Chorus: Marc E. Bassy]
Don't make me take back all the things all the I said
Girl make me stand by what I believe
Don't make me take back all the things all the I said
Girl make me stand by what I believe

[Chorus: Marc E. Bassy]
You know you right from top-to-bottom
I don't ever see no problems for you and me
You know you right from top-to-bottom
I don't ever see no problems for you and me

[Verse 2: Skizzy Mars]
Can't say we making love cause I never been
Bad luck with these girls I could never win
It's my complacency or my negligence
But you a blessing girl, you something Heaven sent
Only faithful as my options, I'm a New York resident
But having feelings that I never get
Kiss you on your lips and they tasted like peppermint
We're too young to be hesitant
Trying to get you out your element
Saint Barths every winter, spring South Beach residence
Well-read, Charles Bukowski on the coffee table
Some E. Cummings, Ralph Waldo Emerson
Cruise the Hamptons with the top of, it seems prevalent
Cause every broken heart needs medicine
Not saying that you perfect cause that don't exist
But I think you even better so I wrote you this

[Pre-Chorus: Marc E. Bassy]
Don't make me take back all the things all the I said
Girl make me stand by what I believe
Don't make me take back all the things all the I said
Girl make me stand by what I believe

[Chorus: Marc E. Bassy]
You know you right from top-to-bottom
I don't ever see no problems for you and me
You know you right from top-to-bottom
I don't ever see no problems for you and me