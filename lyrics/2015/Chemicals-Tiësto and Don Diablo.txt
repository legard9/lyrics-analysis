[Pre-Chorus: Thomas Troelsen]
I'm only human
Just what can I do?
'Cause I feel the fusion
When I'm there with you

[Chorus: Thomas Troelsen]
We're all chemicals, 'cals, 'cals
We are, are, are
We're all chemicals, 'cals, 'cals
We are, are, are
We, we, are we chemicals, 'cals, 'cals
We are, are, are
We're all chemicals
We are
We, we, we, are we chemicals, 'cals, 'cals
We are, are, are, are, are, are

Chemicals

[Bridge: Thomas Troelsen]
What's going on inside my brain?
You're somewhere in my DNA
You got me way up in the sky, sky, sky
Just pull me closer in the night, night, night

[Pre-Chorus: Thomas Troelsen]
But I'm only human
Just what can I do?
'Cause I feel the fusion
When I'm there with you

[Chorus: Thomas Troelsen]
We're all chemicals, 'cals, 'cals
We are, are, are
We're all chemicals, 'cals, 'cals
We are, are, are
We, we, are we chemicals, 'cals, 'cals
We are, are, are
We're all chemicals
We are
We, we, we, are we chemicals, 'cals, 'cals
We are, are, are, are, are, are

Chemicals