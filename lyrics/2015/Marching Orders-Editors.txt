[Verse 1]
I can't open my mind
But there's the makings of a dreamer in you
In these desperate times
I'm walking home, walking home to you

[Bridge]
I will fall with the rain
I will flicker with the flame, the fire

[Verse 2]
I used to write down my dreams
Now they're gone when my eyes open on you
Well, even though you fucked up
There's still the makings of a dreamer in you

[Bridge]
I will fall with the rain
I will flicker with the flame
I will fall with the rain
I will flicker with the flame, the fire, fire

[Chorus]
These are the marching orders
These are the rules that we break
These are the doubts we cling to
Trying to give more, trying to give more

These are the marching orders
These are the rules that we break
These are the doubts we cling to
Trying to give more, trying to give more than we take

Trying to give more, trying to give more

[Chorus]
These are the marching orders
These are the rules that we break
These are the doubts we cling to
Trying to give more, trying to give more
(Trying to give more, trying to give more)
Trying to give more
Trying to give more
(Trying to give more, trying to give more)
(Trying to give more, trying to give more)
(Trying to give more, trying to give more)