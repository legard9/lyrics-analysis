[Verse 1]
You were my oppressor
And I, I have been programmed to obey
But now you are my handler
And I, I will execute your demands

[Chorus]
Leave me alone
I must disassociate from you

[Verse 2]
Behold my trance formation
And you are empowered to do as you please
My mind was lost in translation
And my heart has become a cold and impassive machine

[Chorus]
Leave me alone
I must disassociate from you

[Outro]
I won't let you control my feelings anymore
And I will no longer do as I am told
And I am no longer afraid to walk alone
Let me go
Let me be
I am escaping
From your grip
You will never own me again
Yeah