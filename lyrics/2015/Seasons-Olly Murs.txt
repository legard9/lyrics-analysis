[Chorus]
Listen honey, to every word I say
I know that you don't trust me
But I'm better than the stories about me
Everybody messes up somedays
Ain't got no rhyme or reason
All I know is I'm yours, yours, yours, for every season

[Verse 1]
Baby girl with the broken smile
Would you mind if I stayed a while?
And if you're bored, I can light your fire
If that's what you want, if that's what you want
I admit that I've done some wrong
But those wrongs helped me write this song
And through it all, I figured out where I belong
Right by your side, right by your side

[Pre-Chorus]
They say that hearts don't lie
The head might try, but it won't be right
You tell me what you feel inside
Tonight, tonight, tonight, tonight

[Chorus]
Listen honey, to every word I say
I know that you don't trust me
But I'm better than the stories about me
Everybody messes up somedays
Ain't got no rhyme or reason
All I know is I'm yours, yours, yours, for every season

[Verse 2]
Baby girl let your hair hang down
And if we're lost, baby, let's get found
And with the words flipping upside down
Let's make it right, let's make it right

[Pre-Chorus]
They say that hearts don't lie
The head might try, but it won't be right
You tell me what you feel inside
Tonight, tonight, tonight, tonight

[Chorus]
Listen honey, to every word I say
I know that you don't trust me
But I'm better than the stories about me
Everybody messes up somedays
Ain't got no rhyme or reason
All I know is I'm yours, yours, yours, for every season
Yours, yours, for every season

[Bridge]
Winter, summer, spring, or fall
I'll be on the line waiting for your call
Winter, summer, spring, or fall
I'll be on the line waiting for your call
Winter, summer, spring, or fall
I'll be on the line waiting for your call

[Chorus]
Listen honey, to every word I say
I know that you don't trust me
But I'm better than the stories about me
Everybody messes up somedays
Ain't got no rhyme or reason
All I know is I'm yours, yours, yours, for every season
For every season