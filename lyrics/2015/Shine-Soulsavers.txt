[Chorus 1]
Light
There’s light here
And it shines on you
It shines, yeah it shines down
It shines on you

[Verse 1]
When you look around
It’s so profound
What we can do
You and me
All of us
Here we’re coming through
Here we’re coming through
We gonna shine on you

[Chorus 1]
Light
There’s light here
And it shines on you
It shines, yeah it shines down
It shines on you

[Verse 2]
Anyone can see
There’s destiny
For me and you
Our Lord above
Shines down His love
Shines on you
Yeah it shines down
Let it shine on you
Let it shine on you

[Chorus 2]
Light
There’s light here
And it shines on you
It shines, yeah it shines down
It shines on you
Yeah it shines on you
Let it shine on you
Light
There’s light here
And it shines on you
It shines, yeah it shines down
It shines on you
Yeah it shines on you
Let it shine on you