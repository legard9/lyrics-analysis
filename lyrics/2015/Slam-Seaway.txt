I caught you peeking in my mirror
Wondering when I'll look away
My night of vices has me feeling like I never
Left yesterday
So take your steps just like the rest
But you can, you can
You can find me
You can find me here
Everything is cool, man
You wouldn't get it, (No!)
You wouldn't get it, (No!)
You wouldn't get it, (No!)
But everything is cool, man
I caught you peeking in my mirror
Wondering how things got this way
Like when you dreamt that you were falling down forever
Just yesterday
So save your breath, just like the rest
But you can
You can find me
You can find me here
You say you found peace of mind, but that's a lie
I know, I know you're dying
I know you're dying here
But everything is cool, man
You wouldn't get it, (No!)
You wouldn't get it, (No!)
You wouldn't get it
Everything is cool, man
Everything is cool, man