[Verse 1]
Pardon my mind as I’m going back in time
It’s running wild, finding every side
And we’ve been going miles, I guess somebody else should try
Look I’m fine, picked another way to die

[Pre-Chorus]
And I saw you ‘round town
Hanging with a different crowd
Oh, used to frown but I don’t do that now

[Chorus]
When you’re gone
Nanananana
I’ll find someone
Nanananana
To keep me warm until you come back home for more
Cause I am one for all

[Verse 2]
Oh how time flies, feels like yesterday we flew apart
I passed you by, still we see the same stars
(still we see the same stars)
What I miss will show when we see the change
As we sit, fly back and remain the same

[Pre-Chorus]
And it’s so exciting
It makes me scared to be so near
Something distant from here

[Chorus]
When you’re gone
Nanananana
I’ll find someone
Nanananana
To keep me warm until you come back home for more
Cause I am one for all

[Bridge]
On the train on my way to you I see
A crying man sitting next to me
With a vase in his hands, what a change of scene
And how strange when I’m as happy as can be
And if I tell you would you believe
That this man’s on his way to set his brother free?
That could be me

[Chorus]
When you’re gone
Nanananana
I’ll find someone (one)
Nanananana
To keep me warm until you ...

When you’re gone
Nanananana
I’ll find someone
Nanananana
To keep me warm until you come back home for more
Cause I am one for all