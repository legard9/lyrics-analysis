This is unconventional
Only way that we can roll
And if we runnin’ outta coal
I know some people who can pull
I’m talking people who do right
Use their tongues to spark and light
Plan to aim it, push it fight it
Teach it learn it, save, ignite it
This is urgent so best plan on me to overnight it
Might Ehren Watada
My vision is brighter
You’re not tall; just miter
No cloister to hide out
Zoom, zoom, zoom my lighter
With a balaclava
Clink, clink, clink the bottle
Making molotov
Throw it up in the air
Mystic with this we shine just about anywhere
Only ‘cause I care

[Chorus]
I love you
So much you make the nights better
You make them lights hotter
You make the sweat wetter
I love you
So much you make this worth doing
Just keep that truth spilling
Keep your body moving

Get out your seats please don’t be rude
I am not your average dude
Pick up hints and clues I’m shrewd
Rosanna Pan calls me Nancy Drew
Gotta bag of tricks make my words backflip
Trip over metaphors, then I squeeze em like clips
So hold up, focused, armed like soldiers
We’re ready for war, I thought I told ya
Thunderbird, '65, all white, cream inside
Chrome finish, fresh to death
Killed the game so they can testify
Synths analog, kick strong
I keep it in the trunk of the car I’m driving
You can buy all the swag that you can
But you crash, when you try to ride in my lane
Like I learned to ride a bike in one day
Ain’t nothing to hide my bruise
Tears and rain in the concrete
There’s no challenge I refuse
Now I’m black and blue
Ain’t gotta rule you
Just keep you body movin' til it’s
Black and blue
They don’t own it, so they can’t control it

[Chorus]

See the lights go out
Then the knives get drawn
Hear the boots hit the ground
When it all falls down
First the lights go out
Then the knives get drawn
So let this thought ferment:
When did you revoke consent?
Do you need a homestead to defend?
Tear down the fence

[Chorus]