[Intro]
If you defray
You end up prey
Glow through the veil
Is this what you call frail

[Verse 1]
Ignore the edification
Of those whom you admire
We withhold our blessing
We refuse to calm the fire

[Chorus]
Frail
Is this what you call
Is this what you call frail
Is this what you call
Is this what you call frail
Is this what you call
Glow through the veil
Is this what you call frail

[Verse 2]
Knit her a new pea coat
Drape it past the knee
The sinew has withered
Suture as accessory

[Chorus]
Frail
Is this what you call
Is this what you call frail
Is this what you call
Is this what you call frail
Is this what you call
Glow through the veil
Is this what you call frail