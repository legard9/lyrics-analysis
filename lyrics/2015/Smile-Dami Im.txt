[Verse 1]
You're out of touch and over time
You're running and you're running, getting left behind
You've dropped the ball to catch your breath
And is it all there really is
You're thinking but you're thinking

[Pre-Chorus]
'Cause you know the best of things in life are free
And that's all you need

[Chorus]
You gotta live your life
You gotta treat you right
You gotta give into the crazy appetite
You gotta shake it up
You gotta hold it down
You gotta do all of the things that
Make you smile, make you smile
Redefine what it is that really makes you happy
Get excited
Let your world be a world that you build with smile

[Verse 2]
You're beautiful and evergreen
You're dancing on high hopes and you got big dreams
So let them shine, we'll light it up
'Cause now's the time to find your voice
Wear your heart out on your sleeve

[Pre-Chorus]
'Cause you know the best of things in life are free
And that's all you need

[Chorus]
You gotta live your life
You gotta treat you right
You gotta give into the crazy appetite
You gotta shake it up
You gotta hold it down
You gotta do all of the things that
Make you smile, make you smile
Redefine what it is that really makes you happy
Get excited
Let your world be a world that you build with smile

[Bridge]
So live for the nights we won't forget
Live for the memories we haven't made yet
Turn up the love, fall into it
Live for the memories we haven't made yet
This is our life, no regrets

[Chorus]
You gotta live your life
You gotta treat you right
You gotta give into the crazy appetite
You gotta shake it up
You gotta hold it down
You gotta do all of the things that
Make you smile, make you smile
Redefine what it is that really makes you happy
Get excited
Let your world be a world that you build with smile