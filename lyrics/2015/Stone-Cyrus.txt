[Verse 1]
I shoulda known from your voice alone
That you were gonna mess me up
Too much pleasure like a sugar rush
I should've guessed from those cherry lips
That you were gonna leave your mark
Never had a kiss that could hit so hard

[Pre-Chorus]
And I feel you driftin' over me
Yeah, you got this hold on me
Rooo-o-oo-ooow
And you, yeah you make it hard to breathe
But it just don't bother me
But it just don't bother me

[Chorus 1]
I'm a stone in your river
Rolling to wherever
I drown just a little
I drown just a little
I'm a stone in your river
Every night I'm with you
I drown just a little
Deep down in you

[Verse 2]
I shoulda known watching you dance alone
That I could never get enough
Of your body, they can call that love
I coulda guessed from your silhouette
That we were  gonna 'rive real fast
Oh, baby we can make it last

[Pre-Chorus]
And I feel you driftin' over me
Yeah, you got this hold on me
Rooo-o-oo-ooow
And you, yeah you make it hard to breathe
But it just don't bother me
But it just don't bother me, bother me

[Chorus 2]
I'm a stone in your river
Rolling to wherever
I drown just a little
I drown just a little
I'm a stone in your river
Every night I'm with ya
I drown just a little
Deep down in you
Aye-yeah-aye-aye, yeah-yeah
I drown just a little
Deep down in you
Ooh baby, oooh
I drown just a little
Deep down in you

[Chorus 3]
I'm a stone in your river (in your river)
Rolling to wherever
I drown just a little (drown just a little )
I drown just a little
I'm a stone in your river
Every night I'm with ya
I drown just a little
Deep down in you

[Chorus 1]
I'm a stone in your river
Rolling to wherever
I drown just a little
I drown just a little
I'm a stone in your river
Every night I'm with ya
I drown just a little
Deep down in you
Deep down in you deep down in you

[Outro]
I drown just a little
Deep down in you (in you)
I drown just a little
I drown just a little
Deep down in you

[Collision]
Deep down in you
Deep down in you