[Verse 1: JustPierre]
If I'm gon' do it, gotta come correct
We in the building, shout out to the Architect
Big ups to the Director, no movie set
But I call Him Director He order my steps
Yup, I'm talking 'bout God Almighty
Giving praise to the Father, that's my type of party
For our crimes against God, we should be indicted
But then Jesus said, Lord, prepare me a body
The sacrifice He gave for us
The price that He paid for us
Is why I gaze forward, eyes on the Lord
Looking unto Jesus,that's how I endure
Whatever comes, whatever goes
The Lord is teaching me that He's in control
When He's working, His Presence I cannot behold
But when He has tried me, I'll come forth as pure gold

[Chorus: Nanette Smith]
No matter what comes
No matter what goes
Lord, I still got You
Lord, I still got You

[Verse 2: JustPierre]
Glory, glory, hallelu-
All the glory goes to You
Others try, but get denied
'Cause there's none like the Most High
I'm on the Lord side, this is the life that I chose
Living the life that I write in my songs
Whoa, demons be right on my toes
Whoa, but Jesus stay right'ing my wrongs
Whoa, I get elevated when I use Scripture
They see the proof on my face like school pictures
Bro, you smile like everything alright, bruh
Naw, this that type of joy that's in spite of
Whatever comes, whatever goes
The Lord is teaching me that He's in control
When He's working, His Presence I cannot behold
But when He has tried me, I'll come forth as pure gold

[Chorus: Nanette Smith]

[Chorus: Heaven On Earth Music]