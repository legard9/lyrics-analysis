[Verse 1]
The beer was stronger, the summer’s longer
My right knee to your movement’s sweeter that
Blue dress tied up, your stolen lighter
Night spend beside her, when I loved you

[Verse 2]
The music loud on , my heroes prouder
The shadow bigger your mid-night figure
That harbour sunrise
Those tears in her eyes
No way to disguise that I loved you

[Chorus]
A Spanish summer spent under cover
Cast up wood flames between the night
Gets scrawl our own names. Tattoo our new pains
Breath in your ear I can't make it more clear
That I love you
Shan nan na na na hey yey… ohhh
I love you

[Verse 3]
That rain lashed side street where you said we’d meet
That awkward silence the chance of balance
You left me broken
So much unspoken but I keep on hopin’
I still love you

[Verse 4 ]
Before this sunset some tangled regrets
I need to see you talk this thing through
Before the mornin’ and what else darlin’
Don’t leave me wonderin’  I still love you
I still love you  I still love you I still love you