[Verse 1]
It wasn’t quite the story she had hoped for herself
Hers had a different ending but it
Must have been about someone else
There was a time well spent, there was a time well wasted
She never loved him then and it was time to face it

[Chorus]
And she knows, everything will change
Cause one day it was a red striped blouse
And the next a wedding dress
A big deal, high heels tripping over herself
And you know nobody’s impressed

[Verse 2]
Staring into space trying to feel alive
He’s barely out of school already with a dead end nine to five
Shouldn’t he be happy, she’s waiting at home
She keeps on calling, he turns off his phone

[Chorus]
And she knows, everything will change
Cause one day it was a red striped blouse
And the next a wedding dress
A big deal, high heels tripping over herself
And you know nobody’s impressed

[Bridge]
Ever since she read the note on the wall
Saying “Honey, I’ll be leaving tonight”
She’s sleeping with her eyes wide open
Her body trembles with sickly notions
She hopes everything’ll change

[Chorus]
Cause one day it was a red striped blouse
And the next a wedding dress
A big deal, high heels tripping over herself
And you know nobody’s impressed