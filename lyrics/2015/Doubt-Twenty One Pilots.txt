[Verse 1]
Scared of my own image, scared of my own immaturity
Scared of my own ceiling
, 
scared I'll die of uncertainty
Fear might be the death of me, fear leads to anxiety
Don’t know what’s inside of me

[Chorus]
Don't forget abou-bou-bou-bou-bout me
Don't forget abou-bou-bou-bou-bout me
Even when I doubt you (Doubt you)
I'm no good without you, no, no

[Verse 2]
Temperature is dropping, temperature is dropping
I'm not sure if I can see this ever stopping
Shaking hands with the dark parts of my thoughts, no
You are all that I’ve got, no

[Chorus]
Don't forget abou-bou-bou-bou-bout me
Don't forget abou-bou-bou-bou-bout me
Even when I doubt you (Doubt you)
I'm no good without you, no, no, no, no, no

[Bridge]
Gnawing on the bishops, claw our way up their system
Repeating simple phrases, someone holy insisted
Want the markings made on my skin
To mean something to me again
Hope you haven't left without me
Hope you haven't left without me, please

[Chorus]
Don't forget abou-bou-bou-bou-bout me
Don't forget abou-bou-bou-bou-bout me
Even when I doubt you (Doubt you)
I'm no good without you, no
Don't forget abou-bou-bou-bou-bout me
Don't forget abou-bou-bou-bou-bout me
Even when I doubt you
No good without you, no, no, no, no, no

[Outro]
Hey! Hey!
Don't forget about me, no
Hey! Hey!
Don't forget about me, no
Hey! Hey!
Don't forget about me, no
Hey! Hey!
Don't forget about me, no