[Verse 1]
All I needed was the medicine
And you came knocking like a doctor
Gave me the pill to take away the poison
Erase the writing on the wall
You stuck a needle right into that vein
You let me take it like a soldier
Thought for a while I could ignore the pain

[Pre-Chorus 1]
My hands are tied behind my back
I'm paralyzed, my heart attacks
It seemed to me you were the one
Turns out you shot me up for fun

[Chorus]
Thought you got away with murder
Left me at a loss for the words
Just wait until I catch my breath
Wait until I catch my breath
Ya but you thought you got away with murder
Left me at a loss for the words
Just wait until I catch my breath
Wait until I catch my breath

[Verse 2]
I was devastated by the pain
But now I couldn't call the doctor
Gotta stop the forces in my brain
Gotta read the writing on the wall
It isn't easy getting in the vein
When you do it alone
No one to tighten up the tourniquet

[Pre-Chorus 2]
My hands are tied behind my back
I'm paralyzed, my heart attacks
I'm screaming, begging for the one
That won't just shoot me up for fun

[Chorus]
Thought you got away with murder
Left me at a loss for the words
Just wait until I catch my breath
Wait until I catch my breath
Ya but you thought you got away with murder
Left me at a loss for the words
Just wait until I catch my breath
Wait until I catch my breath

[Post-Chorus]
Wait until I catch my
Wait until I catch my
Wait until I catch my (my breath)
Wait until I catch my breath
Wait until I catch my
Wait until I catch my
Wait until I catch my (my breath)
Wait until I catch my breath

[Bridge]
You leave me hanging out here for so long
When will I catch my breath?
You leave me hanging out here for so long
When will I catch my breath?

[Chorus]
Thought you got away with murder
Left me at a loss for the words
Just wait until I catch my breath
Wait until I catch my breath
Ya but you thought you got away with murder
Left me at a loss for the words
Just wait until I catch my breath
Wait until I catch my breath

[Post-Chorus]
Wait until I catch my
Wait until I catch my
Wait until I catch my (my breath)
Wait until I catch my breath
Wait until I catch my
Wait until I catch my
Wait until I catch my (my breath)
Wait until I catch my breath