[Verse 1]
Round up the boys
We're gonna do it right
Come on girls
Put 'em jeans on
Make 'em tight
Leave your 9 to 5 behind
It's an all night kinda night

[Verse 2]
This town we know
How to get it done
With the beer in the back
And the girls in the front
Put on a little Skynyrd
Drive it to the middle of nowhere
And kick it

[Chorus]
(This is our town)
So live it up right here right now
(This is our town)
'Til the sun comes up and the lights go out
(This is our town)
Yeah, it's Friday night, honey let's get lit
Round here last call don't mean
Shh

[Verse 3]
'Cuz this is our town
We roll on down to the place we always go
Pull up the truck right there on the 18th hole
Grab a cold one, tee it off
Tonight we ain't playin' golf

[Verse 4]
They can kick us off
Try to take our beer
They can call the cops
But they're already here
Maybe Bennett
Moonshine sippin'
Those boys in blue
Know how to kick it

[Chorus]
(This is our town)
So live it up right here right now
(This is our town)
'Til the sun comes up and the lights go out
(This is our town)
Yeah, it's Friday night, honey let's get lit
Round here last call don't mean

[Bridge]
So put them boots on the ground, yeah
Tonight is goin' down
Put 'em tall boys in the air
Let's show 'em how we do it down here

[Chorus]
(This is our town)
So live it up right here right now
(This is our town)
'Til the sun comes up and the lights go out
(This is our town)
Yeah, it's Friday night, honey let's get lit
Round here last call don't mean

[Chorus]
(This is our town)
So live it up right here right now
(This is our town)
'Til the sun comes up and the lights go out
(This is our town)
Yeah, it's Friday night, honey let's get lit
Round here last call don't mean
Shh