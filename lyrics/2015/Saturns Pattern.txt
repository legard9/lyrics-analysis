Did you ever think that we would get this far?
To a place where it's comfortable to realize exactly who we really are
How do you know if it's time?
How do you know it's time for you?
Seems as if we're
Seems as if we're
Seems as if we're
Stuck in a pattern of, bum-bana-nanum
Yuh
I came to a fork in the road;
One path said "Take me if you wanna lighten your load"
Other path said "Take me, but use more control"
"I'm the path of most resistance" of which I had no interest, right
I've always tried to take the easy way out
The rotten which I'm always sure my shit'll play out
But keepin one foot out the door will cause internal conflict for sure
Man I'm only tryin to be pure in mind, body and action
But action's only apparent when you're seeking out a result
Or even better, satisfaction
I'm trapped in this headspace with a hand made of lightning bolts
Writing to provoke an interplay between thought, idea and self
Allowing them to rest with one another until they discover
Their own divine interconnectedness
Fear takes an exodus
And I'm only left with this:
Naturally unforced, simple
Of course it's mental
Psyche foldin em up
We space savin
Makin room for new travellers
Within the walls of this here safe haven
We're Naturally unforced, simple
Of course it's mental
Psyche foldin em up
We space savin
Makin room for new travellers
Within the walls of this here safe haven
Did you ever think that we would get this far?
To a place where it's comfortable to realize exactly who we really are
How do you know if it's time?
How do you know?
It's time for you
Seems as if we're
Seems as if we're
Seems as if we're
Stuck in a pattern of, bum-bana-nanum
Yo Wyzsz
It's happening again
It's happening to something from a past life
Rockin the boat until it's capsized
I didn't learn from my first mistake
Heard from my birthing days
Saturn's returned to bring the worst of pain
How many roads must a man travel before releasing
All his demons and his phantasms?
How many lies must a person take
Die for his purpose' sake
Asking god "Why does it hurt this way, god?"
I think that we can break the patterns, chains
And Jacob's ladder to live to update our greyest matter
Face forgive
Fold it in with day breaking
Kick folk music with words for space savin
I've done a lot of mind cleansing but I guess I got some
More to do to build my (??)
Feel the mental fortitude
I can't afford to move three steps back
The clock's tickin like sentence
I'm a witness to that
When on the path you've gotta stay straight while facin the facts
When in doubt, pray for grace to help escape from the past
And if now is the only moment that there is, we've gotta
Stay focused makin sure that hope exists
I'm a soldier on the light side
Older than my lifetimes
Orchestrating sorcery
Coursin through your vital signs
It's the five year crunch to write the final page
Polar shifting, soul ascending tidal wave
Naturally unforced, simple
Of course it's mental
Psyche foldin em up
We space savin
Makin room for new travellers
Within the walls of this here safe haven
We're Naturally unforced, simple
Of course it's mental
Psyche foldin em up
We space savin
Makin room for new travellers
Within the walls of this here safe haven