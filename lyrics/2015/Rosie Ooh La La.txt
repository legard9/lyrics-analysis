[Verse 1: Wonyoung, Minju, Yujin, Sakura, Hyewon]
Ppalgahge muldeuryeo jigeum i sigan
I'll make it red (eh eh eh)
Make it red (eh eh eh)
Eoneusae nae mame ppalgan jangmicheoreom
Ooh-ahh hage (eh eh eh)
Saeropge eh eh (rose)

[Refrain: Chaewon, Hyewon, Hitomi, Chaeyeon]
Ireon neukkimeun ruby boda deo (ruby boda deo)
Naega neukkineun banjjagimcheoreom
Kkeullimyeon ikkeullyeo na na now
Baro jigeum na na now
I don’t wanna make it blue
Sangsanghaebwa neoui la vie en rose

[Pre-Chorus: Eunbi, Yuri]
Deo gipeojin nunbit geu soge bulkeojin
Nae mameul taoreuge hae nareul chumchuge hae
(Ooh) Ijjima yeogi seo itneun rose
(Ooh) Eonjena bitnal su itge

[Chorus: Sakura, Wonyoung, Chaeyeon, Hyewon]
La la la la vie en rose
(Ooh yeah) This is my, my
La la la la vie en rose (rose)
(Ooh yeah) Oh it’s my, my
La la la la vie en rose

[Verse 2: Minju, Yena]
Gidaehaedo joha
Waenji wanbyeokhaejin i neukkim
Gakkaiseo bwado nan joha (red)
Banjjagineun nunbit ruby gati
Modeun siseon oh all eyes on me (hey)
Naega geu nugubodado bitnage
Ppalgahge muldeurilge

[Refrain: Yujin, Hyewon, Nako, Eunbi, Sakura]
Ireon neukkimeun satangboda deo (satangboda deo)
Naega neukkineun dalkomhamcheoreom
Kkeullimyeon ikkeullyeo na na now
Baro jigeum na na now
I don’t wanna make it blue
Mandeureobwa neoui la vie en rose

[Pre-Chorus: Chaeyeon, Chaewon]
Deo gipeojin nunbit geu soge bulkeojin
Nae mameul taoreuge hae nareul chumchuge hae
(Ooh) Ijjima yeogi seo itneun rose
(Ooh) Eonjena bitnal su itge

[Chorus: Hyewon, Minju, Hitomi, Sakura]
La la la la vie en rose
(Ooh yeah) This is my, my
La la la la vie en rose (rose)
(Ooh yeah) Oh it’s my, my
La la la la vie en rose

[Bridge: Wonyoung, Yuri, Minju]
Gamatdeon nuneul tteobwa
Dallajyeo modeun ge da
Amudo moreuneun saeroun sesangeul bwa, oh baby
La la la, la la la, la vie en rose
Jeonbu da muldeuryeo red
La la la, la la la, la vie en rose

[Pre-Chorus: Eunbi, Yuri]
Kkumirado joha ppalgahge chilhaebwa
Eonjedeun kkaeeonal su issge naega bulleo julge
(Ooh) Ijjima yeogi seo itneun rose
(Ooh) Eonjena bitnal su itge

[Chorus: Wonyoung, Yujin, Eunbi, Minju 
(Yuri)
]
La la la la vie en rose 
(la vie en rose, yeah, yeah!)
(Ooh yeah) This is my, my
La la la la vie en rose 
(ooh ooh ooh ooh ooh)
(Ooh yeah) Jangmitbiche muldeulge
La la la la vie en rose 
(oh, la vie en rose, yeah!)

[Outro: Nako, Sakura, Chaeyeon, Yena, Wonyoung 
(Yuri)
 
(Hyewon)
]
Saeppalga-a-a-an my rose 
(my rose, ooh ooh ooh ooh ooh)
Bichi na-a-a-a my rose 
(yeah)
La la la la vie en rose
I sungan teukbyeolhage 
(we’ll make it red)
Oh it’s my, my
La la la la vie en rose