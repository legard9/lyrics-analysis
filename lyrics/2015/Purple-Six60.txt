I've been holding on to nothing
Reaching out to something
Waiting for someone like you
If only there was one thing
Some way of controlling
All these other things I do

Shock me girl electric blue
Standing while the wrecks still true

And so we fall
And through it all
I'm left here with this purple heart of mine
Battles won
The war is done
And the sky is purple, purple, purple

Last night I was dreaming
Falling from the ceiling
Woke up just before the sound
And it got me thinking
Oh I got that feeling
We'd already hit the ground

Shock me girl electric blue
Standing while the wrecks still true

And so we fall
And through it all
I'm left here with this purple heart of mine
Battles won
The war is done
And the sky is purple, purple, purple

Imma keep shining
Imma keep shining all my love
Every battles won
You know this war is never done
But I keep on shining on, shining on, I just want to shine on
Imma keep shining all my love
And so we fall
I'm left here with this purple heart of mine
Battles won
The war is done
And the sky is purple, purple, purple