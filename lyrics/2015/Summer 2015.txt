[Crowd]
(Alestorm, Alestorm, Alestorm)
Pipe down
Are they saying first song?
They want us to play the first song again?
I have no idea-(drumming starts)
No! No don't play
You know, I have no idea what you're saying
So we're gonna play a song about babes (Alestorm, Alestorm, Alestorm)
Sing along if you know the words
If you don't know the words, just go; bloolooloolooroo
Shhh...  (Alestorm, Alestorm, Alestorm)

[Intro, audience clapping]
I know of a tavern not far from here
Where you can get some mighty fine beer
The company's true and the wenches are pretty
It's the greatest damn place in the whole of the city
If you're looking for crewmates, you'll sure find 'em there
Cutthroats, and lowlifes, and worse I should dare
Ol' Nancy don't care who comes to her inn
It's a den of debauchery, violence and sin (and sin)
So come take a drink and drown your sorrows
And all of our fears will be gone 'til tomorrow
We'll have no regrets and live for the day
In Nancy's Harbour Cafe
So come take a drink and drown your sorrows
And all of our fears will be gone 'til tomorrow
We'll have no regrets and live for the day
In Nancy's Harbour Cafe
If you're looking to go on a glorious quest
There's a man there who knows of an old treasure chest
For some pieces of eight, an' a tankard of ale
He'll show ye the map and tell ye it's tale
And then there's Nancy, the lovely barmaiden
She may be old, but her beauty ain't fadin'
Ol' Nancy don't care who comes to her inn
It's a den of debauchery, violence and sin
So come take a drink and drown your sorrows
And all of our fears will be gone 'til tomorrow
We'll have no regrets and live for the day
In Nancy's Harbour Cafe
So!
Come take a drink and drown your sorrows
And all of our fears will be gone 'til tomorrow
We'll have no regrets and live for the day
In Nancy's Harbour Cafe
Mate Bodor on the fucking guitar!

[Solo]
So come take a drink and drown your sorrows
And all of our fears will be gone 'til tomorrow
We'll have no regrets and live for the day
In Nancy's Harbour Cafe
So
Come take a drink and drown your sorrows
And all of our fears will be gone 'til tomorrow
We'll have no regrets and live for the day
In Nancy's Harbour Cafe
Let me see your hooks!