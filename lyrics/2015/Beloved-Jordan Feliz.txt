[Verse 1]
Head full of questions, how can you measure up?
To deserve affection, to ever be enough
For this existence, when did it get so hard?
Your heart is beating, alive and breathing
And there's a reason why
You are essential, not accidental
And you should realize

[Chorus]
You are beloved
I wanted you to know
You are beloved
Let it soak into your soul, oh-oh
Forget the lies you heard, rise above the hurt
And listen to these words
You are beloved
I want you to know
You are beloved

[Post-Chorus]
You-ou-ou, you-ou-ou
You-you are beloved

[Verse 2]
Sometimes a heart can feel like a heavy weight
It pulls you under, and you just fall away
Is anybody gonna hear you call? Oh, oh
But there's a purpose under the surface
And you don't have to drown
Let me remind you, that love will find you
Let it lift you out

[Chorus]
You are beloved
I wanted you to know
You are beloved
Let it soak into your soul, oh-oh
Forget the lies you heard, rise above the hurt
And listen to these words
You are beloved
I want you to know
You are beloved

[Bridge]
Don't be afraid, don't let hope fade
Keep your eyes fixed on the light above
In the heartbreak, in your mistakes
Nothing can separate you from Love
Don't be afraid, don't let hope fade
Keep your eyes fixed on the light above
In the heartbreak, in your mistakes
Nothing can separate you from Love

[Chorus]
You are beloved
I wanted you to know 
(You to know)
You are beloved
Let it soak into your soul, oh-oh
Forget the lies you heard, rise above the hurt
And listen to these words
You are beloved
I want you to know
You are beloved

[Post-Chorus]
You-ou-ou, you-ou-ou
You-you are beloved
You-ou-ou, you-ou-ou
You-you are beloved 
(yea-eh-eh)

[Bridge]
Don't be afraid, don't let hope fade
Keep your eyes fixed on the light above
(You are beloved)
In the heartbreak, in your mistakes
Nothing can separate you from love

[Outro]
You are beloved