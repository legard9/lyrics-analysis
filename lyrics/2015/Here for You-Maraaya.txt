When you're down, down, low
And there's no place you can go...
When you're down, down, low
You know that I am here for you...

You came into my life when I was broken
You heated up my heart when it was frozen
And we got the flow
Now it's up we go (we go, we go, we go, we go)

And now that it's your turn you wanna hide it
A million times already you've denied it
You don't have to fear
You don't have to fight (fight, fight, fight, fight, fight)

When you're down, down, low
Sinking in the undertow
When you're down, down, low
You know that I am here for you (for you)

Cause I know, know baby
All the hurt you never show
When you're down, down, low
You gotta let me heal your aching soul

You came into my life when I was broken
You heated up my heart when it was frozen
And we got the flow
Now it's up we go (we go, we go, we go, we go)

Cause you released my pain, now let me take it
Just leave it at the door and we can make it
You don't have to hide
I am by your side (side, side)

Everybody else sees in black and white
You look at wrong and make it right
Can't I open, your eyes (your eyes, your eyes, your eyes)

When you are down, down, low
Seeking in the undertow
When you are down, down, low
You know that I am here for you (for you)

Cause I know, know baby
All the hurt you never show
When you are down, down, low
You gotta let me heal your aching soul

When you are down, down, low
Seeking in the undertow
When you are down, down, low
You know that I am here for you (for you)

Cause I know, know baby
All the hurt you never show
When you're down, down, low
You gotta let me heal your aching soul

When you are down, down, low
Seeking in the undertow
When you are down, down, low
You know that I am here for you