I’m still alive I’m still alive I’m still alive
Nan jugeoganeun deut boijiman jukji anha
Namdeurui siseoneul pihae gyeolko sumji anha
Eonjena boran deusi kkeutkkaji churakhajiman Im alive
Nan deo isang irheulge eobseo
Gwageoneun dwiro hago jump out
Gipi tteoreojigo inneun nae moseubi areumdawo jigeum
Nae jasineul haneure deonjyeo nan jayurowo
I sunganmankeumeun nan saraisseo Im still alive

Nareul saranghaneun geunyeoga bulhaenghae boyeo
Jachwireul gamchune daejungdeuri duryeowo
Manheun donggyeonggwa hwanho jeongseoburan
Jugeume daehae gonoehae goeroun bam
Nae jeormeumui yupumeun sajinppun
Nae cheongchuneun namdeurui jangsikpum
Yeongjeogin gireul batneun nae gamseong
Nega boji motan mirae nan bwasseo
Nae insaeng mellodineun naega jihwihae
Gojodoeneun Climax reul jeulgine
Naege michin sonyeodeureun ttokttokhae
Jagyeokjisim neoreul gajigo nolgie

Saramdeul da tteonado moduga deungeul dollyeodo
Gayeoundeutan nae moseube neo dongjeonghaedo
Sesangeun andoendago naege malhajiman
Geu narui ulgo itdeon nan jigeum utgo isseo

Eonjena boran deusi kkeutkkaji churakhajiman Im alive
Nan deo isang irheulge eobseo
Gwageoneun dwiro hago jump out
Gipi tteoreojigo inneun nae moseubi areumdawo jigeum
Nae jasineul haneure deonjyeo nan jayurowo
I sunganmankeumeun nan saraisseo Im still alive

I’m still alive I’m still alive I’m still alive
I’m livin’ that I’m livin’ that good life
I’m still alive I’m still alive I’m still alive
We livin’ that we livin’ that good life

Ye sori eobsi sarajin hamseong daesine
Tansik seokkin tanseong
I gamseongsidaeui nae banseongeun chansong
Yangseongboda museoun akseongbangsong
Gakseonghara amugaena a-yo
Danjeong mara amu ttaena
Nan jugeoganeun deut boijiman jukji anha
Namdeurui siseoneul pihae gyeolko sumji anha
Geu songarakjireun naega ajik isyuraneun jeunggeo
Silmanggwa gidae iyuwa geungeo
Ibe oreurangnaerirakhae guseolsu
Nal gidarineun geon yangjibareun mudeomppun

Saramdeul da tteonado moduga deungeul dollyeodo
Gayeoundeutan nae moseube neo dongjeonghaedo
Sesangeun andoendago naege malhajiman
Geu narui ulgo itdeon nan jigeum utgo isseo

Eonjena boran deusi kkeutkkaji churakhajiman Im alive
Nan deo isang irheulge eobseo
Gwageoneun dwiro hago jump out
Gipi tteoreojigo inneun nae moseubi areumdawo jigeum
Nae jasineul haneure deonjyeo nan jayurowo
I sunganmankeumeun nan saraisseo Im still alive

I’m still alive I’m still alive I’m still alive
I’m livin’ that I’m livin’ that good life
I’m still alive I’m still alive I’m still alive
We livin’ that we livin’ that good life



English:


I’m still alive I’m still alive I’m still alive
I may seem like I’m dying but I won’t die
I won’t escape the other’s eyes and hide
I may be obviously falling till the end I’m alive
But I have nothing more to lose
So I put the past behind me and jump out
My deeply falling self is so beautiful right now
I throw myself to the sky and I am free
At this moment, I am alive, I’m still alive

The girl who loves me looks unhappy
I’m covering up my tracks, I’m afraid of the public
The adorations, cheers, and emotional turmoil
I think deeply about death on this miserable night
The only articles that remain of my youth are pictures
My youth is a decoration for others
My senses have received a spiritual energy
I’ve seen a future that you haven’t seen yet
I conduct my own life melody
I enjoy the rising climax
The girls who go crazy over me are smart
Because I play with you out of self-reproach

Even if everyone leaves, even if everyone turns against me
Even if you take pity over my pathetic self
The world tells me that I can’t but
The crying me of the past is now smiling

I may be obviously falling till the end but I’m alive
But I have nothing more to lose
So I put the past behind me and jump out
My deeply falling self is so beautiful right now
I throw myself to the sky and I am free
At this moment, I am alive, I’m still alive

I’m still alive I’m still alive I’m still alive
I’m livin’ that I’m livin’that good life
I’m still alive I’m still alive I’m still alive
We livin’ that we livin’ that’ good life

Yea, instead of the sounds of cheers that disappeared silently
There are sighs of admiration mixed with groans
During this age of sensitivity, my reflection is like praise
A malignant broadcast is scarier than positive reactions
Awaken yourself from everything a-yo
Don’t assume at any time
I may seem like I’m dying but I won’t die
I won’t escape the other’s eyes and hide
Those pointed fingers are proof that I’m still an issue
Disappointment and hope are the reason and basis
Malicious gossip go up and down in people’s mouths
The only thing waiting for me is a tomb filled with sunshine

Even if everyone leaves, even if everyone turns against me
Even if you take pity over my pathetic self
The world tells me that I can’t but
The crying me of the past is now smiling

I may be obviously falling till the end but I’m alive
But I have nothing more to lose
So I put the past behind me and jump out
My deeply falling self is so beautiful right now
I throw myself to the sky and I am free
At this moment, I am alive, I’m still alive

I’m still alive I’m still alive I’m still alive
I’m livin’ that I’m livin’ that good life
I’m still alive I’m still alive I’m still alive
We livin’ that we livin’ that good life