[Hook: Cyrex & Lake]
See I got-
400 Ways that I'm tryna stay awake
400 Ways I could prolly make my way. I got-
400 Ways that I'm tryna get a way
400 Ways I could prolly make my day! I got-
400 Ways that I'm tryna stay awake
400 Ways I could prolly make my way. I got-
400 Ways that I'm tryna get a way
400 Ways I could prolly make my day!

[Verse 1: Cyrex]
Come in wit' at least 1 way to get a come up
Double up my trouble up, when I make it bubble up
Better than a 3-piece with the truffle butter up
8 bar stanzas, 4 letters like "who's Danza?"
10 bucks says you'll keep the bill facing Hamil(ton)
12 bars on this whole verse be the plan, though
15 minute jam session on piano
16 Gigabytes, press it into nano
20 bucks says Tubman on it for her summary
21 years for peers to get drunken, b
24 hours, #24karatfvxkery
25 times 16 is 400, see it's…

[Hook: Cyrex & Lake]
See I got-
400 Ways that I'm tryna stay awake
400 Ways I could prolly make my way. I got-
400 Ways that I'm tryna get a way
400 Ways I could prolly make my day! I got-
400 Ways that I'm tryna stay awake
400 Ways I could prolly make my way. I got-
400 Ways that I'm tryna get a way
400 Ways I could prolly make my day!

[Verse 2: Lake]
I just got four hundred ways... with zero excuses
Around a low time all time high with waves that’s make me translucent
But I know I gotta do this. Put my faith into the music
Don’t rap I just write to express for days that make me lose this... way of–
Feeling real down when I need to stay up never had no option but to keep my pay up
Now I’m feelin way up. The friends that we make probably hold us in place for the feelings that stuck
Damn! Never understood the point of contact
Just young visionary. Spread my word, that’s missionary for the people that lost get on track
Damn! The vision never change
The people the rearrange for the life they wanna make
When I’m stuck up in a haze, I got options, I got plays
I got 1-2-3-400 different ways like, Damn!

[Hook: Cyrex]
400 Ways that I'm tryna stay awake
400 Ways I could prolly make my way. I got-
400 Ways that I'm tryna get a way
400 Ways I could prolly make my day! I got-
400 Ways that I'm tryna stay awake
500, 600 Ways to make my way. I got-
700 Ways that I'm tryna get a way
800 Ways that I'm tryna make my day!
900 Ways that I'm tryna stay awake
1000 Ways that I'm tryna my way. I got-
1100 Ways that I'm tryna get a way
1200 Ways that I'm tryna make my day!