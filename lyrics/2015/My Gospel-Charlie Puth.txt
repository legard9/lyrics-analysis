[Verse 1]
I’d stroll into a bank and put a ski mask on
And walk out with a million bucks
Then I'd burn it in a pile out on your front lawn
Just to prove it didn't mean that much
I'd sell everything I own, take a pile of cash
Walk into the casino, bet it all on black
Then blow all of my winnings on a limousine
To make out with you in the back, hey

[Pre-Chorus]
I’m not afraid to look insane
'Cause I’m crazy about ya

[Chorus]
There's nothing that I won’t do
Just to make you love me, love me, love me
Throw away my pride for you
Just to make you love me, want me, trust me, baby
I know I'm all but shameless
Just need you and I could care less
If everyone knows that I'm your fool
Girl, there's nothing I won’t do
Just to make you love me, love me, love me (Hey)

[Verse 2]
I'd buy a couple red-eye one-way tickets
To Paris to our first class ride
Then I'd carry your body to the top of the tower
To kiss your lips at midnight
Then we’ll wander through the streets and drink champagne
Get drunk and start singing in the pouring rain
I'll acapella karaoke “Sexy Thing”
'Cause baby, I feel no shame
Say this whole world can know you're all I need
'Cause baby, some day, I'll drop and hit one knee and tell ya

[Pre-Chorus]
I'm not afraid to change your name
'Cause I’m crazy about ya

[Chorus]
There's nothing that I won’t do
Just to make you love me, love me, love me
Throw away my pride for you
Just to make you love me, want me, trust me
Baby, I know I'm all but shameless
Just need you and I could care less
If everyone knows that I'm your fool
Girl, there's nothing I won’t do
Just to make you love me, love me, love me (Hey)

[Bridge]
Ah-ah, ah-ah-ah
Throw myself out of a jet air plane
Hundred feet a second, just screaming your name
Ah-ah, ah-ah-ah
I'd steal a cop car, turn the blue lights on
Pick you up and drive it around 'til dawn

[Pre-Chorus]
I'm not afraid to look insane
'Cause I’m crazy about ya
Oh-oh, I'm not afraid to look insane
'Cause I’m crazy, crazy, crazy

[Chorus]
There's nothing that I won't do
Just to make you love me, love me, love me
Throw away my pride for you
Just to make you love me, want me, trust me
Baby, I know I'm all but shameless
Just need you and I could care less
If everyone knows that I’m your fool
Girl, there's nothing I won't do
Just to make you love me, love me, love me, baby