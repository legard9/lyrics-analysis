[Intro: Simple Plan]
Farewell, I didn't mean to let you...

[Verse 1: Simple Plan]
After all these wasted nights
I can't pretend that I'm doing fine
I've played it back a thousand times
But now I see it and I realize

[Pre-Chorus: Simple Plan]
That the damage is done and it's obvious
We can never go back to the way it was
We're drifting apart and it's killing us
It's killing us

[Chorus: Simple Plan]
Farewell
I didn't mean to let you...
Let you down, mess it up
We both knew it couldn't last forever
It's coming down, I've had enough
I guess we crumbled under all the pressure
I did my best for what it's worth
And I gave you all this heart could give
So farewell
I didn't mean to let you down (Down)

[Verse 2: Jordan Pundik]
There was a time you'd scream my name
It used to be my getaway
Now all we do is just complain
Well, maybe I'm the one to blame

[Pre-Chorus: Jordan Pundik]
But the damage is done and it's obvious
We can never go back to the way it was
We're drifting apart and it's killing us
It's killing us

[Chorus: Simple Plan]
Farewell
I didn't mean to let you...
Let you down, mess it up
We both knew it couldn't last forever
It's coming down, I've had enough
I guess we crumbled under all the pressure
I did my best for what it's worth
And I gave you all this heart could give
So farewell
I didn't mean to let you down

[Bridge: Simple Plan & Jordan Pundik]
It's such a shame
How did we end up this way?
We can't go back
And things will never be the same
All hopes faded
Is this what you wanted?
Is this what you wanted?

[Chorus: Simple Plan]
Farewell
I didn't mean to let you...
Let you down, mess it up
We both knew it couldn't last forever
It's coming down and I've had enough
I guess we crumbled under all the pressure
I did my best for what it's worth
And I gave you all this heart could give
So farewell
I didn't mean to let you
Farewell
I didn't mean to let you down (Down)
(Farewell)