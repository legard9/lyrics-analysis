[Intro]
Oh ohohoh
Oh ohohoh

[Verse 1]
I was dreaming of war; you saw that I wouldn't die
Was dreaming of shores that my ship would find
I was dreaming because it made me feel so alive
Was dreaming it all from my bed last night

[Pre-Chorus]
You will be my only one
Hold my hand so we can run
You and I, we're staying young, yeah
We're gonna live, we're gonna live forever

[Chorus]
This time, I want it all
I want it right now, feel the fall
Go, go crazy, go, go, go crazy
We're gonna live, we're gonna live forever
This time, I want it all
I want it right now, wanna feel the fall
Go, go crazy, go, go, go crazy
We're gonna live, we're gonna live forever

[Verse 2]
I was feeling a storm, but it wasn't outside
Was feeling a force; I'm electrified
I was feeling my heart; there was thunder inside
Was feeling it all from my bed last night

[Pre-Chorus]
You will be my only one
Hold my hand so we can run
You and I, we're staying young, yeah
We're gonna live, we're gonna live forever

[Chorus]
This time, I want it all
I want it right now, feel the fall
Go, go crazy, go, go, go crazy
We're gonna live, we're gonna live forever
This time, I want it all
I want it right now, wanna feel the fall
Go, go crazy, go, go, go crazy
We're gonna live, we're gonna live forever
We're gonna live, we're gonna live forever

[Bridge]
All the things that I don't know
All the dreams that I've been shown
All the ways that it could end tonight
Close your eyes
We're gonna live, we're gonna live forever

[Chorus]
This time, I want it all
I want it right now, feel the fall
Go, go crazy, go, go, go crazy
We're gonna live, we're gonna live forever
This time, I want it all
I want it right now, wanna feel the fall
Go, go crazy, go, go, go crazy
We're gonna live, we're gonna live forever
We're gonna live, we're gonna live forever