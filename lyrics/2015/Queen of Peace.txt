[Intro: D Sav, Dappy D & $TeeN]
Yo chale
Yo
I called you earlier but you didn't pick
Chale this Nana Adwoa thing has got me shocked, she was talented in everything she did
I even forgot, it's sad oh
Yeah, R.I.P to her
R.I.P N.A.S.A
(Humming)

[Chorus: $TeeN]
I remember your laugh, it's still ringing in my head
Looking at it now, I can't believe that you dead
I know you gone, but I just wanna tell you
You taught me a lot of shit
Told me some shit I felt too
Told you a lot of shit I had no one to tell to
Such a sweet soul, I can't understand it
Death came around and picked you like a bandit
But you with God now, but it's hard to comprehend it

[Verse 1: BW Baby]
Way back when we used to hang out in the hallway
I'd make you laugh and you'd make me smile all day
You will always be in my heart, and I mean always
You made your mark on the world in a small way
So peaceful, fly to the sky like an eagle
Why do bad things happen to the best people
Regal, queen of her city, Mufasa
Always fly high and that's why they call her N.A.S.A

[Verse 2: N11]
All of this time, I promise you've been on my mind
Been feeling so calm when we standing side by side
Will never forget you, I'm not even gonna lie yeah
Back in a better place you taught me so many times yeah

[Bridge I: $TeeN]
That's just the way it is
That's just the way it is
That's just the way it is
That's just the way it is

[Verse 3: Ras K]
Tears going down my face
When we lose someone that we can't replace
We love you and you know that
That love ain't even going to waste
But one day we'll see you
We'll see you in a better place
But one day we'll see you
We'll see you with your pretty face

[Verse 4: BW Baby]
Yeah we had our problems
But we had to solve them
Thinking of the times spent together like goddamn
You were such a star, about to reach stardom
So close to me
But then now you so far gone
You were such a blessing
You were such a miracle
Strong in your faith
You were spriritual, biblical
Seeing that smile man, it was so typical
R.I.P N.A.S.A, beautiful, original

[Bridge II: $TeeN]
So original, so original
So original

[Chorus: $TeeN]
I remember your laugh, it's still ringing in my head
Looking at it now, I can't believe that you dead
I know you gone, but I just wanna tell you
You taught me a lot of shit
Told me some shit I felt too
Told you a lot of shit I had no one to tell to
Such a sweet soul, I can't understand it
Death came around and sweeped you like a bandit
But you with God now, but it's hard to comprehend it

[Post-Chorus]
Ooh, ooh-ohh

[Tribute I: Augustine Blay/$TeeN]
She had a big impact on my life for real

[Tribute II: AnnMarie Alipui]
I don't even know where to begin
You were my sister, my friend, my motivator, my teacher all wrapped in one
I always looked up to you, you were always inspiring me day by day you were impressing me with something new
You were very educated and knew what was right and wrong
You always stood up for what you believed in
You were very religious and shared your thoughts with us on the daily
Nana Adwoa, you were the life of the party, we both know that
We used to turn up in classes, just dance and let stuff go and release our stress and you taught me peace
You taught me how to have fun and how to work
You taught me balance
I love you Nana Adwoa, you are my sister
Rest in peace, AnnMarie

[Tribute III: Nicole Aidoo]
You taught me the value of love
Loving God, loving family and loving friends
On the 30th of May, I wished you happy birthday and told you that I loved you for life
I meant it
I love you today, tomorrow and forever. I promise to always bring happiness into peoples lives, like you brought happiness into mine though your loud joyful laughter
I love you Nana Adwoa

[Tribute IV: Nana Kwame Minkah/BW Baby]
Nana I know that I never got to say this but I want you to know that I love you and I'll never forget you and I'll keep your memory and legacy in my heart always

[Tribute V: Anastasia Antonelli]
Nana Adwoa I love and I miss you so much
Thank you for being such a pure soul and you'll always be in my heart forever

[Tribute VI: Danielle Mensah]
Nana Adwoa I love you so much, you were so talented and amazing and pretty
I'll never forget any of the good memories we shared together and they will be with me for the rest of my life
I love you

[Tribute VII: Charney Iddrisu]
Nana Adwoa I will never forget how whenever I was annoyed or frustrated or angry you were always there to talk to me, make a-, such a silly joke or do something silly and make me feel better
I love you and rest in peace

[Tribute VIII: Belina Biju]
It honestly breaks my heart knowing that I will never hear your voice again
You were the most talented, energetic, lively and passionate person I've ever met
You always found a way to make me laugh when I was upset
You always entertained me with your fake fights with people, it made me laugh a lot
And I will never forget how you were there for me when I was going through some tough times
Thank you for being such an amazing person and making the last three years of my life very entertaining
You always be in my heart Nana
I love you, rest in peace

[Tribute IX: Ariel Ntim-Addae]
Nana Adwoa, you were the definition of an all-rounder
You were smart, talented, pretty and also sweet
Truly an angel on Earth
Thank you for being there when I needed you and for making me laugh when I didn't want to
Thank you for introducing me to some of the stuff that makes the biggest impact on my life and some of the things that have shaped me as a person
I love you forever, always and the day

[Tribute X: Alan Apraku]
Nana Adwoa was that confident ray of sunshine we all needed in our lives
She was beautiful, she was funny, she was smart and she was caring
Thank you Nana Adwoa for blessing me and thank so much for being there even when I didn't ask
I hope you rest easy and I love you so much
I'll see you in paradise
God bless you

[Tribute XI: Nii Amon Nikoi]
Nana Adwoa, thank you for being such an amazing friend to me, even though in the beginning I wasn't a good friend to you
Thank you for always helping me, whether it was something small or big
You were always there for me and for that I am grateful

[Tribute XII: Shaquille Andah/Shakuzulu]
Dear Nana Adwoa we miss you
You were such a nice person, such a good person and you were an angel sent from the heavens and they have just come to take you back
But we need you, we miss you, thank you for everything you did for us
R.I.P

[Tribute XIII: Nana Banyin Akyianu/Metro Banyin]
We'll always love you Nana
Thank you for what you did on the Earth
Rest easy

[Tribute XIV: Cedric Atitsogbui/C2]
Nana Adwoa you'll be missed by everyone in the whole school
You are an angel and everyone will miss you
You remain in our hearts forever

[Tribute XV: Yula Maruyama]
Nana, it's breaking my heart not knowing that I'm not going to see you again by my side when I go back to school
You were always there on the first day of school waiting so we could hug each other
It's been three days and I'm grieving
I don't know what I'm going to do without you
I need you
I need to hear your voice, your laugh, I need to see your smile and I need to hug you again
I don't think I'll ever be able to let go because I never imagined a life without you
Since day one you have always been there for me no matter what
You have always made me smile, laugh and you just being there made my day
You're the most talented, most gorgeous, creative, smartest and crazy girl I have ever met
The memories we had together were unbelievable and incredible, for the past year and a half was the best thing I could ever imagine
Thank you for coming into my life and thank you for being the best, best friend I could ever ask for
I miss you and I love you so much, forever and always
Rest easy monkey

[Tribute XVI: Krystie Prah]
You were a sister, you were a friend
You were a best friend
There is nothing that I would be able to say that I haven't said to you already
I just want to know... I just want you to know that I will always love you, you will always be in my heart
You were there for each and everyone of your friends
You were there for each and every one of the people that you didn't even know of
Anybody who was in trouble, distressed, sad, you were there for them
That shows something, that shows who you are
And I'm just grateful to have had somebody in my life as beautiful as you are
You are truly an angel

[Tribute XVII: Maureen Gentilezza]
What can I say?
Nana Adwoa was a really good friend of mine
She was really nice to talk to, and although we used to annoy each other all the time, it will always end up in laughing and jokes
All of this that has happened is just so surreal and its still very hard to process and to think that a good friend of mine is gone
But during this I've tried to stay positive and remember what a good person she was and the fun times that we had
She truly had a lot of things that were so good about her
She could sing so well, she had such good communication skills
She tried to do all the activities and at the same time managing her stress levels
She was really someone that I looked up to when it came to the aspects of life and I know that she's happy, I know that she's at peace and I'll just like her to know that I love her a lot and she means so much to me and she was a really good person
I hope that she's happy, all my blessings to and my regards to her family because I can't imagine what they must be going through
So I'd just like to say that I wish her the best up there and I know that the angels will always guide her

[Tribute XVIII: Wilma Ofori Atta & Nana Adwoa S. Agyekum]
Nana
Wilma
I know you can hear me, but I can't hear you and I just want you to know that I love you and all the plans that we said we would fufill, everything we've planned, everything we've talked about, I promise to do them on your behalf
And I want you to know that I love you
I love you to
Sorry
I'm sorry too