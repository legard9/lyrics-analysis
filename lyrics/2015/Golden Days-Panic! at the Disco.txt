[Verse 1]
I found a pile of Polaroids in the crates of a record shop
They were sexy, sexy looking back from a night that time forgot
(Ooh-oh) Boy, he was something, debonair in 1979
And she had Farrah Fawcett hair, carafes of blood red wine

[Pre-Chorus]
In the summertime
In the summertime

[Chorus]
Oh, don't you wonder when the light begins to fade?
And the clock just makes the colors turn to grey
Forever younger, growing older just the same
All the memories that we make will never change
We'll stay drunk, we'll stay tan, let the love remain
And I swear that I'll always paint you

[Refrain]
Golden days
Golden days
Golden days
Golden days

[Verse 2]
I bet they met some diplomats on Bianca Jagger's new yacht
With their caviar and dead cigars, the air was sauna hot
I bet they never even thought about the glitter dancing on the skin
The decades might've washed it out as the flashes popped like pins

[Pre-Chorus]
In the summertime
In the summertime

[Chorus]
Oh, don't you wonder when the light begins to fade?
And the clock just makes the colors turn to grey
Forever younger, growing older just the same
All the memories that we make will never change
We'll stay drunk, we'll stay tan, let the love remain
And I swear that I'll always paint you

[Refrain]
Golden days
Golden days
Golden days
Golden days

[Bridge]
Time can never break your heart, but it'll take the pain away
Right now our future's certain, I won't let it fade away

[Refrain]
Golden days
Golden days
Golden days
Golden days
Golden days
Golden days
Golden days
Golden days