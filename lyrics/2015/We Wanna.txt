We're singing nana yeah yeah

When your life is getting you down
With too much heartache and pain, Oh yeah
Don't loose your faith
Just keep on searching
A heart with one open door
We wanna tell you
Anger can take you and hating can break you
Don't let them take your soul
We're singing

Nana nananana
Don't throw your life away
Nana yeah yeah
Try to make it happy
Nana nananana
This is what we wanna say
Nana yeah yeah
Try to make it happy

We all know that life gives us chances
To make it joyful to live
Oh oh yeah
Don't waste your time waiting for answers
You know you get what you give
We wanna tell you
Don't have to worry
We all gonna make it
Love is finding its way
We're singing

Nana nananana
Don't throw your life away
Nana yeah yeah
Try to make it happy
Nana nananana
This is what we wanna say
Nana yeah yeah
Try to make it happy

We hope that we can make you understand
That life is a beautiful gift
And we will be reminding you again and again
We're singing

Nana nananana
Don't throw your life away
Nana yeah yeah
Try to make it happy
Nana nananana
This is what we wanna say
Nana yeah yeah
Try to make it happy
Nana nananana nananana nana teah yeah yeah yeah