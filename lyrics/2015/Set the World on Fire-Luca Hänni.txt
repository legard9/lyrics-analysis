[Verse 1]
I've broken diamonds
I’ve healed the past
I'm coming faster
Than the train to passed
I'm so delirious
When I need to switch
And when I’m with you baby
You scratch my…

[Pre-Chorus]
Uuh, I'm stronger than before
I said, Uuh
We're livin' in a war
I said, Uuh
I'm steel to the core
Hum, Now it's time for the final encore
Come on

[Chorus]
Heavenly sparks fly while we ignite
We set the world on fire tonight
Trembling stars fall from the sky
We set the world on fire tonight
Oh Oh Oh
Oh Oh Oh
Oh Oh Oh
Set the world on fire tonight (Oh Oh Oh Oh)
Oh Oh Oh
Oh Oh Oh
Oh Oh Oh
Set the world on fire tonight (Oh Oh Oh Oh)

[Verse 2]
Fluid like lava
I'm the highest peak
I'll erupt whenever I want to speak
I’m the highest life form
When I’m in the light
I'm so courageous
Don’t feel no fright

[Pre-Chorus]
Uh, I'm stronger than before
I said, Uuh
We're livin' in a war
I said, Uuh
I’m steel to the core
Yeah, Now it's time for the final encore
Come on

[Chorus]
Heavenly sparks fly while we ignite
We set the world on fire tonight
Trembling stars fall from the sky
We set the world on fire tonight
Oh Oh Oh
Oh Oh Oh
Oh Oh Oh
Set the world on fire tonight (Oh Oh Oh Oh)
Oh Oh Oh
Oh Oh Oh
Oh Oh Oh
Set the world on fire tonight (Oh Oh Oh Oh)

[Bridge]
Oh Oh Oh
You lady
Set the world
Come on baby
Set the world
Yeah
Alright
Let me give it to you

[Outro]
Heavenly sparks fly while we ignite
We set the world on fire tonight
Trembling stars fall from the sky
We set the world on fire tonight
Listen up we're connected by satellite
We set the world on fire tonight
Only love's gonna make this whole thing seem right
We set the world on fire tonight