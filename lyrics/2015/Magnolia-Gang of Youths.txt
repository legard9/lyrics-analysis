[Verse 1]
Quit honking your horns, there's five other lanes
And I am king of them all and faster than light
With my fists in the air, if my body's alive
Then my soul's unaware in braving the last
Of this terrible wine, I've savoured the last
And I've kissed it goodbye, there's no kind of right way
To do what I'll do, but I'm king of the earth
With impossible blood on this 3rd of June

[Pre-Chorus]
Hold on, officer, I know that I'm a danger to myself
And it shows, 'cause I'm on the other side of the law

[Chorus]
There's no way tonight, as far as I know
That heaven will take me, so I'm staggering home
Show me the way, oh, show me the light
Yeah, I'm drunk but I'm ready to kick some ass tonight

[Verse 2]
Still honking those horns, yeah, just drive around
'Cause you're all being ignored, deadpan and bright
As I flip you the bird, I'm a pain in the ass
Yeah, I'm a real piece of work, just waving my arms like
Some terrible mime, and shaking my ass
'Til they kiss it goodbye, just show me the right things
That I didn't do, oh, enlighten me now
With impossible love from the former you

[Pre-Chorus]
Just tell everyone I know what I did, I did for us
And in haste, 'cause I'm terrified of dying in vain

[Chorus]
There's no way tonight, as far as I know
That heaven will take me, so I'm staggering home
Wretched and wild, all glory and trash
Yeah, I'm drunk but I'm ready to kick some fucking ass

[Instrumental Break]

[Chorus]
There's no way tonight, as far as I know
That heaven will take me, so I'm staggering home
Show me the way, oh, show me the light
Yeah, I'm drunk but I'm ready to kick some ass tonight
No way tonight, as far as I know
That heaven will take me, so I'm staggering home
Fire of my loins, oh, light of my life
We’re vastly outnumbered every motherfucking time