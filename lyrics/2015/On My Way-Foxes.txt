[Verse 1]
Colours are fading fast from my memory
But I know you're still haunting everything
And I know someday maybe I'll be okay
But I can't escape, no no

[Refrain]
I've dent myself tonight
I pray I'll be alright, but you let me down I swear
Trying to cut you out
Can't even scream and shout, 'cause you've left me numb, my dear

[Pre-Chorus]
Oh, won't you let me go?
Don't say it's okay
I was on my way to healing, oh

[Chorus]
It's something I just need to learn
Every time I feel alone
Can't keep running back to you again
You turned my golden into dust
Rain on me until I rust
All I do is run to you again

[Verse 2]
Summers are over, lights can shine again
Giving myself to something I can't mend
And I know someday maybe I'll be okay
But am I to blame? Oh no

[Refrain]
I've dent myself tonight
I pray I'll be alright, but you've let me down I swear
Trying to cut you out
Can't even scream and shout, 'cause you've left me numb, my dear

[Pre-Chorus]
Oh, won't you let me go?
Don't say it's okay
I was on my way to healing, oh

[Chorus]
It's something I just need to learn
Every time I feel alone
Can't keep running back to you again
You turned my golden into dust
Rain on me until I rust
All I do is run to you again

[Breakdown]
Baby, I was on my way
I was on my way, I was on my way
Baby, I was on my way
I was on my way, I was on my way
Baby, I was on my way
I was on my way, I was on my way
Baby, I was on my way
I was on my way, I was on my way

[Chorus]
It's something I just need to learn
Every time I feel alone
Can't keep running back to you again
You turned my golden into dust
Rain on me until I rust
All I do is run to you again

[Outro]
Baby, I was on my way
I was on my way, I was on my way