[Verse 1]
I've got this vision, precision, mission impossible
And I make my rhythms unstoppable
Just like my father [?], I'm feeling sophical
I think therefore I'm a little diabolical
Each word in sequence is systematically critical
And I need to feel it in phsyical
I'm like a ticking time bomb, I'm the only one who knows the code
That's why I don't believe in miracles, oh no

[Chorus]
Oh, can we feel things, when we feel things, can we
Oh, can we feel, can we feel, feel
Oh, oh, oh
Can we feel things, when we feel things, can we

[Verse 2]
My OCD has gotten the best of me
And I need medication please
So that may be I can think a little less heavily
This hospital bed is not hospitable
But it's better than being dead in my bed
These sore thoughts, they echo off the walls of my skull
And I can't side, or I can't define if I'm the red or the bull
But I am free, or so they tell
I can run run run, but I'm better off staring at the wall

[Chorus]
Oh, can we feel things, when we feel things, can we
Oh, can we feel, can we feel, feel
Oh, oh, oh
Can we feel things, when we feel things, can we

[Verse 3]
Sometimes I feel emotions like a puppet on strings
My thoughts are magnified by almost everything
Like my emotional attachment to inanimate things
I put my bird up to the sky and I sing
Fuck it

[Outro]
Oh, nanana
Nanana, nanana
Can we feel things, when we feel things, can we
Oh, nanana
Nanana, nanana
Can we feel things, can we feel it
Oh, nanana
Nanana, nanana
Can we feel things, when we feel things, can we
Oh, nanana
Nanana, nanana
Can we feel things, can we feel it
Oh, nanana
Nanana, nanana