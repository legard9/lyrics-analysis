[Verse 1]
A white picket fence on the coast wasn’t ever my style
We’ll have a love that’s worn in ’cause it took us a while
I’ll see my past in the corners and cracks of your smile
And seeing your future in mine’s all you need to survive

[Pre-Chorus]
‘Cause I still believe in love
I’ll be right here when you come
And I will wait
Till I know your name
You make me feel like before you I wasn’t alive
And I know we’ll fall all the time but I still wanna try

[Chorus]
‘Cause I still believe in love
I’ll be right here when you come
And I will wait (I will wait, I will wait)
Till I know your name (I will wait, I will wait)

[Verse 2]
Thought I found you once before
But wherever you are know I’m all yours
‘Cause I still believe in love
I’ll be right here when you come

[Outro]
I will wait
I will wait, yeah…
I will wait
Till I know your name