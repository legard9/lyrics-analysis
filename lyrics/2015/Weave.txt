So close your eyes
For that's a lovely way to be
Aware of things your heart alone was meant to see
The fundamental loneliness goes whenever two can dream a
Dream together
You can't deny don't try to fight the rising sea
Don't fight the moon, the stars above and don't fight me
The fundamental loneliness goes whenever two can dream a
Dream together
When I saw you first the time was halfpast three
When your eyes met mine it was eternity
By now we know the wave is on it's way to be
Just catch that wave don't be afraid of loving me
The fundamental loneliness goes whenever two can dream a
Dream together
When I saw you first the time was halfpast three
When your eyes met mine it was eternity
By now we know the wave is on it's way to be
Just catch that wave don't be afraid of loving me
The fundamental loneliness goes whenever two can dream a
Dream together