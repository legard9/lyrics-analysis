[Verse 1]
To the angels sent from above
We send our love
Pray to God we fulfil our dreams
Pray to God they know what I mean
And if Heaven sends all my love
To the sky above
I hope you can come and see me
One day in harmony
And all the colours in the sky
You can hear the stars collide
Feel the air beneath your feet
As you open up like an ocean
Cause your mother, brother, sister, father, your family loves you
And you'll see that day
Where the sun will rise
And you'll find your love
And look in their eyes
And you'll walk down the aisle
As a bride and he's by your side
And he never leaves
Cause we never die
And you'll stay alive
And you'll never ever have to say goodbye
It'll get better and I'm sure of it
Cause your hard work's being noticed
I just hope my Nana gets to see us shine
And my Poppa lives to the sunrise
On that one beach in Hawaii
While my Mum spends her days riding
On a horseback on the bright side
And my Dad and me never lost time
I just wish it was easy for me to tell him
So I'll say it to all of you
I love you, always

[Verse 2]
And I pray to God for my soul to keep
You can take my love
And spread it 'round the world
Just to see you twirl would fill me up
As we dance into the night for one last time
I hold your hand and we smile
Tears fill our eyes with light

[Bridge]
As we dance among the skies
We can sing into the light
Blue moon, purple night

[Verse 3]
We could walk into the light
We could fly away in the night
We don't have to see more ghosts
Your soul is filled with life
We could send this to the Angels
Tell them everything's alright
So I tell you that I love you
And for everything, I thank you

[Instrumental]

[Spoken]
A lot of people wonder why were they brought into this world?
People stay up at night imagining what their purpose is
Manifesting destinies of great adventure
Conquering even greater darkness and finding the light of life
A light that is already there within their souls
Yet people continue to mistake its place for somewhere out there in the big beautiful world
And I don't blame them, it's hard to find purpose in a universe where things are based on happenstance and coincidence
I spend most of my time lost pacing the floors
Carpet keeping me grounded as I ponder in a daze of confusion
'Cause I don't think any of us actually know where we are in the universe and I know for sure I fucking don't
But I know that there's a beauty in everything, in everyone
I used to think finding that happiness in beauty's form would manifest past all of the trauma
Past the hate, past all of the hurt and despair of the world
But I've learnt very quickly that some of the most gorgeous parts of life are apart of all of that
I've come to accept that the world can be a dark and fucked up place
But we can find love in the darkness still and change because of those experiences for the better
And while I'll always fight for love to be the ethos of each and every character on this earth
I can only help but admire what the universe has to offer through its trials and tribulations
I am at a point in my life where I am very blessed
I am rich with whānau, I have a roof over my head, I can make music and rebel against things
I am in a place where I have power to control my destiny
And I'm well aware of the infinite amounts of sentient beings who don't have that
And while I don't know what to do or how to fix that
I do know this
Everyone has love
It is the sole element of God's creations and all religious beliefs that bring us together
It is the only reason I am talking to you today
And while I treasure my privilege to share love with you as it is in need
To all of those who can't share it
Who can't feel it in their lives
I want to send my message to you
Hopefully putting this out into the universe can bring you some peace somehow because it's the least we can all do
There is a greater purpose we are all apart of
Every single person's life matters
Every single being is connected
And you are the most important part of that
If you give yourself enough respect, enough belief
You can achieve anything, no matter what it is
And you deserve to feel love and to give love
Then maybe I'll meet you on other side
So we can come together again
So, just remember this
Everything's alright
I love you