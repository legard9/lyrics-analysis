[Verse 1]
You ain't even scared
'Cause you know I know you'll conquer it
Yeah, you're finally getting there
I wish I had it all 'cause I'd offer it
And it feels like a last stop
And it feels like an open wound
Whatever it is that you did to me
You didn't have to be so soon
So soon
(But)

[Chorus]
(Go on)
(Go on)
(Go be better than me)
(Go on)
(Go on)
(Go be better than me)
Go be better than me
(Go on)
(Go on)

[Verse 2]
Look beneath your feet
The road rolls out in front of you
And your soul to keep
In the arms of Madison Avenue
And it hits like a running train
And it hurts like being alive
If you really gave up
Then why'd you tell me
I've become the shadow
You the light

[Chorus]
(Go on)
(Go on)
(Go be better than me)
Go on, go be better than me
(Go on)
(Go on)
(Go be better than me)
Go be better
Go be better
(Go on)
(Go on)

[Bridge]
Hey, all the hot lights and the famous streets
It's just the first step in forgetting me

[Chorus]
(Go on)
(Go be better than me)
Go be better
(Go on)
Go on
(Go be better than me)
Go be better
(Go on)