[Intro]
It used to be fun, now it's bringing me down
It’s like everything sucks 'cause you're not around

[Verse 1]
Drive to the beach to watch the sunset
It’s pointless, I don't even like it
Don't wanna watch another movie
My favorite song means nothing to me
I try to medicate, I try to numb the pain
No matter what I do it’s all in vain
My heart was full of love
Now all I do is hate
I fall apart when you're so far away

[Pre-Chorus]
Please come back to me

[Chorus]
It used to be fun, now it's bringing me down
It’s like everything sucks ‘cause you're not around
I never ever should have let you run out of town
Now everything sucks ‘cause you’re not around
All my friends are asking me
Why I’ve been acting crazy
It used to be fun, now it's bringing me down
It’s like everything sucks ‘cause you're not around

[Verse 2]
We used to go out ‘til the morning
But now the party's kinda boring
Went back to where we had our first date
But now I hate the way the food tastes

[Pre-Chorus]
Please come back to me

[Chorus]
It used to be fun, now it's bringing me down
It’s like everything sucks ‘cause you're not around
I never ever should have let you run out of town
Now everything sucks ‘cause you’re not around
All my friends are asking me
Why I’ve been acting crazy
It used to be fun, now it's bringing me down
It’s like everything sucks ‘cause you're not around
Everything sucks ‘cause you're not around

[Bridge]
Please come back to me (Please come back to me)
Please, I’m on my knees

[Chorus]
It used to be fun, now it's bringing me down
It’s like everything sucks ‘cause you're not around
I never ever should have let you run out of town
Now everything sucks ‘cause you’re not around
It used to be fun, now it's bringing me down
It’s like everything sucks ‘cause you're not around
I never ever should have let you run out of town
Now everything sucks ‘cause you’re not around
All my friends are asking me
Why I’ve been acting crazy
It used to be fun, now it's bringing me down
It’s like everything sucks ‘cause you're not around

[Outro]
Everything sucks ‘cause you're not around