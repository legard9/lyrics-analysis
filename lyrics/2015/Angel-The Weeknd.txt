[Intro: The Weeknd]
(
Ooh, ooh
)
(
Ooh, ooh
)
Oh, yeah, I said

[Verse 1: The Weeknd]
Angel, oh-oh-oh-oh, knew you were special from the moment
I saw you (Saw you), I saw you, yeah
I said angel, oh-oh-oh-oh, I feel you closer every time
I call you (Call you), I call you

[Pre-Chorus: The Weeknd]
'Cause all I see are wings
All I can see, your wings, but I know what I am and the life I live
(Life I live) Yeah, the life I live, and even though I sin
Maybe we are born to live, but I know time will tell if we're meant for this
Yeah, if we're meant for this
, and if we're not

[Chorus: The Weeknd]
I hope you find somebody, I hope you find somebody
I hope you find somebody, I hope you find somebody to love
Somebody to love, somebody to love
Yeah, yeah, yeah

[Verse 2: The Weeknd]
Said angel, woah-oh-oh-oh, you'll probably never take me back and I
Know this (Know this,) yeah, I know this, aw man
I said angel, woah-oh-oh-oh, I'm so desensitized to feeling
These emotions (Emotions,) yeah, no emotions, baby

[Pre-Chorus: The Weeknd]
'Cause all I see are wings
All I can see, your wings, but I know what I am and the life I live
(Life I live) Yeah, the life I live, and even though I sin
We all wanna live, but I know time will tell if we're meant for this
Yeah, if we're meant for this
, and if we're not

[Chorus: The Weeknd]
I hope you find somebody (Ooh-ooh-ooh,) I hope you find somebody (Ooh-ooh-ooh)
I hope you find somebody (Ooh-ooh-ooh,) I hope you find somebody to love
Somebody to love, somebody to love
Yeah, yeah, yeah

[Bridge: The Weeknd, 
The Weeknd & 
Maty Noyes
]
And even though we live inside a dangerously empty life
You always seem to bring the light, you always seem to bring the light
And even though we live inside
 a dangerously empty life
You always seem to bring me light
, you always seem to bring me light

[Chorus: The Weeknd, 
Maty Noyes
, 
The Weeknd & Maty Noyes
, 
Children's Choir
]
I hope you find somebody, 
I hope you find somebody
I hope you find somebody, 
I hope you find somebody to love
Somebody to love (
Somebody to love
), 
somebody to love
Yeah, yeah, oh
I hope you find somebody (I hope you’re with somebody, baby,) I hope you find somebody (Ooh-ooh-ooh-ooh-oh-oh)
I hope you find somebody (I hope you find somebody,) I hope you find somebody to love (I hope you find somebody to love)
Somebody to love (Somebody to love,) somebody to love (Somebody to love)
Yeah, yeah, yeah, oh
I hope you find somebody
 (I hope you—,) 
I hope you find somebody
 (I hope you—)
I hope you find somebody
 (I hope you found somebody, baby,) 
I hope you find somebody to love
 (Somebody to love)
Somebody to love, somebody to love
Yeah (
Yeah
,) yeah (
Yeah
,) yeah, ooh, somebody, baby
I hope you find somebody
 (I hope you find somebody) 
I hope you find somebody
 (I hope you find somebody)
I hope you find somebody
 (I hope you find somebody, baby,) 
I hope you find somebody to love
Somebody to love
 (Somebody to love,) 
somebody to love
 (Somebody to love)
Yeah, yeah, yeah