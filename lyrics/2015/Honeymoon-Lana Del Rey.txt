[Verse 1]
We both know that it's not fashionable to love me
But you don't go 'cause, truly, there's nobody for you but me
We could cruise to the blues
Wilshire Boulevard, if we choose
Or whatever you wanna do
We make the rules

[Chorus]
Our honeymoon
Our honeymoon
Our honeymoon
Say you want me too
Say you want me too
Dark blue
Dark blue

[Verse 2]
We both know the history of violence that surrounds you
But I'm not scared, there's nothing to lose now that I've found you
And we could cruise to the news
Pico Boulevard in your used
Little bullet car, if we choose
Mr. "Born to Lose"

[Chorus]
Our honeymoon
Our honeymoon
Our honeymoon
Say you want me too
Say you want me too
Dark blue
Dark blue

[Bridge]
There are violets in your eyes
There are guns that blaze around you
There are roses in between my thighs
Fire that surrounds you
It's no wonder every man in town
Had neither fought nor found you
Everything you do is elusive
To even your honey dew

[Chorus]
Our honeymoon
Our honeymoon
Our honeymoon
Doo, doo, doo, doo, doo, da, da, da, da, da
Doo, doo, doo, doo, doo, da, da, da, da, da
Doo, doo, doo, doo, doo, da, da, da, da, da, da, da

[Outro]
Dreaming away your life
Dreaming away your life
Dreaming away your life
Hmm, mmm
Dreaming away your life
Dreaming away your life
Dreaming away your life
Hmm, hmm, mmm