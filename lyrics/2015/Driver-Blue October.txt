[Justin vocalizing]
I guess we’re standing right where we belong
Wearing life like I have a loose t shirt on
As slowly time plays, I get in the way
Like the things I’d love to do to you can’t be good for the soul

[Chours]
We can go with the top down
We’re never gonna look back, so we just drive
Yeah, we just drive (Drive…)
Yeah, we just drive
So we just drive (Drive…)

[Justin vocalizing]
Winding down the beach: you, me, sunset
Hair thrown back, so wild I could never forget
Like a picture taken, time stand still, can’t look away
Burning bright gold, orange, red so good for the soul

[Chorus]
These are the times to remember
Moments held in our hearts
We’ve been smiling all day
And driving into the stars
So we just drive
Yeah, we just drive
Yeah, we just drive
So we just drive