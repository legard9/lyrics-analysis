Hangul

[Verse 1: Joy, Seulgi]
맨 처음 교복을 입던 날처럼
어색한 기분과 들뜬 마음
어젯밤 갑작스런 네 고백에
나 사실 한 시간도 못 잤어

[Pre-Chorus: Irene, Yeri]
어제완 다른 사이 친구는 이제 그만
근데 말이야 부탁 있어 약속해줘

[Chorus: Wendy]
Kiss kiss kiss 단 하루도 빼먹지 말고
Love love love 잠들기 전 꼭 속삭여줘
네가 원한 만큼 내가 기다린 만큼
마음 다해 안아줘 아껴줘

[Verse 2: Wendy]
그 동안 넌 너무 티가 났지만
그게 날 더욱 설레게 했어

[Pre-Chorus: Yeri, Irene]
처음 잡는 너의 손 촉촉한 떨림 속
나도 몰래 두근거려 더워져요

[Chorus: Seulgi]
Kiss kiss kiss 단 하루도 빼먹지 말고
Love love love 잠들기 전 꼭 속삭여줘
네가 원한 만큼 내가 기다린 만큼
마음 다해 안아줘 Oh yeah

[Bridge: Joy, Seulgi]
어제보다 오늘 오늘보다 내일
마음이 더욱 커져갔음 해 Yeah
혼자 외롭지 않게 둘이 슬프지 않게
네 옆에 붙어있을게

[Chorus: Wendy]
Step by step 둘이서 걷는 길이 좋아
딴딴딴 둘만 아는 노래도 좋아
혹시 오랜 시간 지나도 습관 말고
진심으로 온 맘 다해 아껴줘

[Outro: Joy]
시간이 너무 아까워 내일 또 만나고 싶어
네가 원한 만큼 내가 기다린 만큼
마음 다해 안아줘 사랑해

Romanization

Maen cheoeum gyobogeul ipdeon nalcheoreom
Eosaekhan gibungwa deultteun maeum
Eojetbam gapjakseureon ne gobaege
Na sasil han singando mot jasseo
Eojewan dareun sai chinguneun ije geuman
Geunde mariya butak isseo yaksokhaejwo

Kiss kiss kiss dan harudo ppaemeokji malgo
Love love love jamdeulgi jeon kkok soksagyeojwo
Nega wonhan mankeum naega gidarin mankeum
Maeum dahae anajwo akkyeojwo

Geu dongan neon neomu tiga natjiman
Geuge nal deouk seollege haesseo

Cheoeum jabneun neoui son chokchokhan tteollim sok
Nado mollae dugeungeoryeo deowojyeoyo

Kiss kiss kiss dan harudo ppaemeokji malgo
Love love love jamdeulgi jeon kkok soksagyeojwo
Nega wonhan mankeum naega gidarin mankeum
Maeum dahae anajwo Oh yeah

Eojeboda oneul oneulboda naeil
Maeumi deouk keojyeogasseum hae Yeah
Honja oeropji anhge duri seulpeuji anhge
Ne yeope buteoisseulge

Step by step duriseo geodneun giri joha
Ttanttanttan dulman aneun noraedo joha
Hoksi oraen sigan jinado seupgwan malgo
Jinsimeuro on mam dahae akkyeojwo

Sigani neomu akkawo naeil tto mannago sipeo
Nega wonhan mankeum naega gidarin mankeum
Maeum dahae anajwo saranghae

English Translation

Like the day I first wore my school uniform
I feel awkward and excited
I couldn’t sleep at all because of
Your sudden confession last night
We’re different from yesterday, we’re not friends
But I have a request, promise me

Kiss kiss kiss, don’t leave a day out
Love love love, whisper to me before I sleep
As much as you want it, as much as I waited
Hug me, love me with all your heart

You were so obvious for all this time
But that made my heart flutter even more

I’m holding your hand for the first time
Without knowing, my heart is pounding

Kiss kiss kiss, don’t leave a day out
Love love love, whisper to me before I sleep
As much as you want it, as much as I waited
Hug me with all your heart

More than yesterday, more than today
I hope my heart grows bigger
So I won’t be lonely, so we won’t be sad
I’ll stick by your side

Step by step, I like walking with you
I like this song that only we know
Even if a long time passes
Not as a habit but hug me with all your heart

Time is too precious, I wanna see you again tomorrow
As much as you want it, as much as I waited
Hug me with all your heart, I love you