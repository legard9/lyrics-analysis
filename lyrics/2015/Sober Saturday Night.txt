VERSE 1
I'm here
With a pencil on the right hand
Acting like I don't care
Sober on a Saturday night
I'm here
Too many thoughts in my head
Staring to nowhere
Waiting for the midnight, ye
We're here
Sorry, "we" doesn't exist
Show me where's the exit
This mirror game keeps getting me wrong
PRE CHORUS
And alone I'll walk through this
And alone I'm gonna go places
I'm alone lost in this labyrinth
CHORUS
So help me the tide is making me drowning
And I can feel the inhibition on my body
Thank God that I can hug here now my nanny
I'm sick of your ABCs
Where's my lifeboat, where is it?
Where is it? whe whe where is it?
Can't clutch it
VERSE 2
I'm still here
Thunders on the outside
Lightnings as my flashlights
Lola is already scared
And I'm here
The "prompter" is here nearby
But you're not here why?
I need your silent eyes
PRE CHROUS
And alone I'll walk through this
You're alone but you can not admit
We're alone in this labyrinth
CHORUS
So help me the tide is making me drowning
And I can feel the inhibition on my body
Thank God that I can hug here now my nanny
I'm sick of your ABCs
Where's my lifeboat, where is it?
Where is it? whe whe where is it?
Cause I can't clutch it
Can't clutch it