[Chorus]
Help me polarize, help me polarize, help me down
Those stairs is where I'll be hidin' all my problems
Help me polarize, help me polarize, help me out
My friends and I, we got a lot of problems

[Verse 1]
You know where I'm coming from
Though I am running to you
All I feel is deny, deny, denial

[Pre-Chorus]
I wanted to be a better brother, better son
Wanted to be a better adversary to the evil I have done
I have none to show to the one I love
But deny, deny, denial

[Chorus]
Help me polarize, help me polarize, help me down
Those stairs is where I'll be hidin' all my problems
Help me polarize, help me polarize, help me out
My friends and I, we got a lot of problems

[Verse 2]
Polarize is taking your disguises
Separating 'em, splitting 'em up from wrong and right
It's deciding where to die and deciding where to fight
Deny, deny, denial

[Pre-Chorus]
I wanted to be a better brother, better son
Wanted to be a better adversary to the evil I have done
I have none to show to the one I love
But deny, deny, denial

[Chorus]
Help me polarize, help me polarize, help me down
Those stairs is where I'll be hidin' all my problems
Help me polarize, help me polarize, help me out
My friends and I, we got a lot of problems

[Refrain]
Ah-da-da, da-da, da-da da-da
Ah-da-da, da-da, we have problems
Ah-da-da, da-da, da-da da-da
Ah-da-da, da-da, we have problems

[Bridge]
Domingo en fuego
I think I lost my halo
I don't know where you are
You'll have to come and find me, find me
Domingo en fuego
I think I lost my halo
I don't know where you are
You'll have to come and find me, find me

[Refrain]
Ah-da-da da-da da-da da-da
Ah-da-da da-da, we have problems
Ah-da-da da-da da-da da-da
Ah-da-da da-da, we have problems

[Chorus]
Help me polarize, help me polarize, help me out
My friends and I have problems
Help me polarize, help me polarize, help me out
My friends and I have problems
Help me polarize, help me polarize, help me out
My friends and I have problems
Help me polarize, help me polarize, help me out
My friends and I have problems, yeah
(We have problems)
Singing out, we're singing out, we're singing out
(We have problems)
Yeah

[Outro]
I wanted to be a better brother, better son
I wanted to be a better brother, better son
I wanted to be a better brother, better son