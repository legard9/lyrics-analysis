[Verse 1]
I'll be standing tall
When you call, I'll be at your side
Don't even think of giving in
Know all your fears keep creeping into your mind
Even the darkest hour soon will be over
My friend, it will be over

[Pre-Chorus]
I've been blessed and I've been cursed
I've been the best, I've been the worst
Now I'll take what's mine

[Chorus]
Who wants to celebrate?
And who's just fine to sit and wait?
I gave it everything
'Cause I just wanted you
Who wants to celebrate?
I don't have time to sit and wait
I gave it everything
And I just wanted you to feel the same

[Verse 2]
We've all so often heard it said
There's a road up ahead
That's just beginning
It's hard to see from where I stand
There's a future close at hand
And it's worth living

[Pre-Chorus]
I've been blessed and I've been cursed
I've been the best, I've been the worst
Now I'll take what's mine

[Chorus]
Who wants to celebrate?
And who's just fine to sit and wait?
I gave it everything
'Cause I just wanted you
Who wants to celebrate?
I don't have time to sit and wait
I gave it everything
And I just wanted you to feel the same

[Chorus]
Who wants to celebrate?
And who's just fine to sit and wait?
I gave it everything
'Cause I just wanted you
Who wants to celebrate?
I don't have time to sit and wait
I gave it everything
And I just wanted you to feel the same
Who wants to celebrate?
And who's just fine to sit and wait?
I gave it everything
'Cause I just wanted you to feel the same