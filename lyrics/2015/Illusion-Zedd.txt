[Click here to read about Zedd's production on "Illusion"]

[Verse: Sydney Sierota]
It feels like the fairy tale is over, I really wanted these pages to begin
With "Once upon a time" like all those lullabies
I should have known better, I'll admit it, I thought when I let you in
You were my shining knight from all those stories

[Chorus: Sydney Sierota]
This love is your illusion, this love is your illusion
Take me to a place with real-life love
I need a break from this broken-down fantasy
Show me a place I can realize love, not your illusion

[Verse: Sydney Sierota]
It feels like the fairy tale is over, I really wanted these pages to begin
With "Once upon a time" like all those lullabies
I should have known better, I'll admit it, I thought when I let you in
You were my shining knight from all those stories

[Chorus: Sydney Sierota]
This love is your illusion, this love is your illusion
Take me to a place with real-life love
I need a break from this broken-down fantasy
Show me a place I can realize love, not your illusion