[Verse 1]
Tall and tan and young and handsome
The boy From Ipanema goes walking
And when he passes
Each girl he passes goes ah
When he walks, he's like a samba
That swings so cool and sways so gentle
That when he passes
Each girl he passes goes, ah

[Bridge]
Oh, but I watch him so sadly
How can I tell him I love him
Yes, I would give his heart gladly
But each day when he walks to the sea
He looks straight ahead, not at me

[Verse 2]
Tall and tan and young and handsome
The boy From Ipanema goes walking
And when he passes I smile
But he doesn't see

[Bridge]
Oh, but I watch him so sadly
How can I tell him I love him
Yes, I would give heart gladly
But each day when he walks to the sea
He looks straight ahead, not at me

[Verse 3]
Tall and tan and young and handsome
The boy From Ipanema goes walking
And when he passes I smile
But he doesn't see

[Outro]
He doesn't see me
He doesn't see