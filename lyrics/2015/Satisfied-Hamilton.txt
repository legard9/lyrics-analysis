[Laurens spoken:] Alright, alright, that’s what I’m talkin’ about!
Now, let’s give it up for the maid of honor: Angelica Schuyler!

[Angelica sung:]
A toast to the Groom!
(To the groom, to the groom, to the groom...!)
To the Bride!
(To the bride! To the bride, to the bride!)
From your sister...
(Angelica, Angelica, Angelica!)
...Who is always by your side!
(By your side!)
To the Union!
(To the Union!)
And the hope that you provide! May
You always...
(Always..!)
...Be satisfied-!

[Chorus:]
Rewind! Helpless...! Schuyler...Drownin’ in ‘em...drownin’...rewind!

[Angelica:]
I remember that night I just might (rewind)! I remember that night I just might (rewind)! I remember that night I just might (rewind)! I remember that night I just might (rewind)!
I remember that night I just might regret that night for the rest of my days...I remember those Soldier Boys trippin’ over themselves just to win our praise...! I remember that dream-like candlelight like a dream that you can’t quite place! Oh, but Alexander, I’ll never forget the first time I saw your face! I have never been the same! Intelligent eyes with a hunger-pang frame...and when ya said, “Hi,” I forgot my dang name! You set my heart aflame ev’ry part a flame

[Chorus + Angelica:]
This is NOT a game!

[Hamilton:] You strike me...as a woman who has never been satisfied-

[Angelica:] I’m sure I don’t know what you mean, you forget yourself!

[Hamilton:] You’re like me; I’m never satisfied

[Angelica:] Is that right?

[Hamilton:] I’ve never been satisfied!

[Angelica:] My name is Angelica Schuyler!

[Hamilton:] Alexander Hamilton

[Angelica:] Where’s your fam’ly from?

[Hamilton:] Unimportant; there’s a million things I haven’t done... but you wait, just you wait!

[Angelica:] So, so, so...so is this what it feels like to match wits with someone at your level?! What the h**l is the catch? It's the feeling of freedom, of seein' the light! It's Ben Franklin with a key and a kite! You see it, right?
The conversation lasted two minutes, maybe three minutes ev’rythin’ we said in total agreement; it's a dream and it's a bit of a dance; a bit of a posture, it's a bit of a stance. He’s a bit of a flirt, but I'mma give it a chance! I asked about his fam'ly, did you see his answer?
His hands started fidgeting, he looked askance! He's penniless, he's flying by the seat of his pants!
Handsome, boy, does he know it!
Peach fuzz, and he can't even grow it!
I wanna take him far away from this place
Then I turn and see my sister's face and she is…

[Eliza:] Helpless!

[Angelica:] and I know she is...

[Eliza:] Helpless!

[Angelica:]
And her eyes are just...

[Eliza:]
Helpless!

[Angelica:]
And I realize:

[Chorus + Angelica:]
Three fundamental truths at the exact same time!

[Hamilton:] Where are you taking me?

[Angelica:]
I'm about to change your life

[Hamilton:]
Then by all means, lead the way!

[Chorus:] Number One(!):

[Angelica:] I’m a girl in a world which my only job is to marry rich! My father has no sons so I have social climb for one! And so I’m the oldest and the wittiest and the gossip in New York is insidious
And Alexander is penniless...ha! That doesn’t mean I want him any less!

[Eliza:] Elizabeth Schuyler, it’s a pleasure to meet you

[Hamilton:] Schuyler?

[Angelica:] My sister!

[Chorus:] Number Two(!):

[Angelica:] He's after me 'cause I'm a Schuyler sister that elevates his status; I'd have to be naïve to set that aside, maybe that is why...I introduce him to Eliza; now that's his bride!
Nice going, Angelica, he was right: you will never be satisfied!

[Eliza:] Thank you for all your service!

[Hamilton:] If it takes fighting a war for us to meet, it will have been worth it!

[Angelica:] I'll leave you to it!

[Chorus:] Number Three(!):

[Angelica:] I know my sister like I know my own mind! You will NEVER find anyone as trusting or as kind! If I tell her that I love him she’d be silently resigned! 
He’s
 be mine! She’s say, “I’m fine!” She’d be lying!
But when I fantasize at night it’s Alexander’s eyes...! As I romanticize what might have been if I hadn't sized him up so quickly! At least my dear Eliza's his wife;...at least I keep his eyes in my life…!
To the Groom!
([Chorus:] to the Groom, to the Groom, to the Groom...!)
To the Bride!
([Chorus:] to the Bride, to the Bride, to the Bride...!)
From your sister...
([Chorus:] Angelica, Angelica , Angelica!)
...Who is always by your side! To your Unio-o-o-on!
([Chorus:] Your Union!)
And the hope that you provide! May you ALWAYS...
([Chorus:] Always!)
...Be satisfied!
([Chorus:] Satisfied!)
And I know...she’ll be happy as his bride! And I know...he will never be satisfied, I will never be satisfied...!