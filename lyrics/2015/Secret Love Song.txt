Jasper:
And now, my dears, a toast. Be prepared. To ourselves, a closely united family, and to the dear strangers who have joined us: I allude to you, Jane darling, and Edward, and my dear Charles
Charles:
Does that mean we three may not drink?
Jasper:
Certainly not. Drink to yourselves, to each other and to the happiness of us all
Charles:
Good
Harriet:
Oh, do be quiet Charles
Jasper:
Where was I, where was I, where was I?
Jane:
To the happiness of us all, my love
Jasper:
Thank-you, Jane
Here's a toast to each of us
And all of us together
Here's a toast to happiness and reasonable pride
May our touch on life be lighter
Than a seabird's feather;
May all sorrows as we pass
Politely step aside
Jane:
A commonplace sentiment, my dear Jasper, worthy neither of you, nor of the moment
Jasper:
Moments fly so swiftly, my love
Emily:
I thought what Jasper said was beautiful
Jasper:
Hush, Emily. Jane's chiding merely means that she would have liked to have thought of it herself
Edward:
Get on with the toast, Jasper
Jasper:
Where was I, where was I, where was I?
Jane:
Gasping in the deeps of your own imagination, my love
Jasper:
Thank-you
Now I drink to those of us who, happily united
Ornament our family and share our joy and pain
Charles, my friend, and Edward, too, connubially plighted
Last, my dears, but always best, my own beloved Jane
Jane:
Charmingly put, my dear Jasper, if a trifle pedantic
Jasper:
I do my best, my love, but my best is obviously unworthy
Harriet:
Oh, do stop sparring, you two
Emily:
Sparring? What about their expression?
Jasper:
Where was I, where was I?
Jane:
In command, my love, as always
Jasper:
Harriet married a soldier
A man of pleasant birth
A man of noble worth
And finely tempered steel
Ready to die for the Empire
The sun must never set
Upon his both brave but yet
Ambiguous ideal
So now, dear Charles, I am saluting you
That never setting-sun
Shall call you blest
If far-off natives take to shooting you
You will at least have done
Your level best
All:
Harriet married a soldier
May life be bright for him;
May might be right for him
For ever and for aye
Harriet married a soldier
And in the matrimonial fray
Harriet married a soldier
Despite his glories in the field
He'll have to honour and obey
And be defeated till judgement day!
Harriet:
Oh, but how unfair of you all, I'm as meek as a mouse. Charles rules me with a rod of iron
Jane:
Dear Harriet, we salute your strategy that makes him believe it
Jasper:
Now we come to Emily, whose progress has been steady;
Only married two short years and three fat sons already
Emily:
You make me blush, Jasper. We count the twins as one
Edward:
Never-the-less, my love, they're normal babies, with a mouth each to feed
Jasper:
Emily married a doctor
A sentimental man
A mild and gentle man
Of scientific mind
Doing his best for the nation
Forever dutiful
A really beautiful
Example to the rest of us
A challange to the zest of us
The noblest and the best of us combined
Edward:
I accept your tribute, Jasper, while doubting its complete sincerity, but the surface value is warming enough. I thank you, Jasper
Emily and Jane:
Edward
* * * * *
Harriet:
It played another tune, I remember distinctly, it played another tune
Richard:
You mustn't ask too much of it
Harriet:
It was a waltz
Jane:
Yes, yes, of course it was a waltz. Don't you remember, we danced to it years later at a ball, before we were married — it was this — it was this ——
Hearts and flowers
Dreamy hours
Under skies of blue
Emily: [interjected]
It's remembered. Oh, How clever of it
Richard:
Shh, Emily, That was their love-song
Jane:
Two fond hearts so sweetly beat in tune
'Neath the midnight magic of the moon
Petals falling
Love-birds softly calling
Life begins anew
When Cupid's dart discloses
The secret of the roses
Hearts and flowers and you
Jasper:
The man who wrote those words certainly had a sweet tooth
Emily:
I remember Annie singing that tune when she was doing the stairs
Harriet:
I remember Nanny singing it when she was bathing Emily
Richard:
I remember Father humming it between his teeth when he was whacking me with a slipper
Charles:
An excellent example of two hearts beating to the tune;
Jasper:
A crude joke, Charles, back to the barrack room
Jane:
We found it inspiring enough. Did we not, my love?
Jasper:
Olympian, the loveliest song in the world
Jane & Jasper:
Hearts and flowers
By-gone hours
How the time have flown
Jasper:
You wore white camellias in your hair
Jane:
All you did was hold my hand and stare
Both:
Have we altered
Have our footsteps faltered
Through the years we've known?
When all our days are done, love
There'll still be only one love
You and you alone
Jasper:
Forever, my heart
Jane:
Till death do us part
Burrows:
You rang, Mister Jasper?
Jasper:
Yes, Burrows. Would you like a little Madeira?
Burrows:
I should be honoured, Mr. Jasper
Jasper:
Here
Burrows:
At your service, always
Jasper:
Thank-you
Burrows:
Have I your permission for a moment?
Jasper:
Certainly, Burrows. What is it?
Burrows:
The musical box. There should be a little tune. A little tune from the years that have ended. Allow me. I drink to you all, and to you, sir, and ma'am. This house was happy when there were children in it
All:
Let the angels guide you
Be good and brave and true
Let the angels guide you
Oh do! Oh do! Oh do!
Spurn each vile temptation
Avoid each evil lure
Keep your conversation
Inordinately pure
Good may be rewarded
In some indefinite place
Be always virtuous just in case