[Intro]
Yeah
Yeah
Yeah
Yeah

[Verse]
This ain't about your love
This is about the lies that we've been told
This ain't about your freedom (freedom)
This is about the future that's unknown
This ain't about forgiveness (forgiveness)
This is about the light between my bones
This ain't about your sanity (sanity)
This is about the things you think you know

[Pre-Chorus]
For every time I fell down to my knees
Oh, this is redemption, woohoo
For every time I shed my blood and tears
Oh, this is redemption, woohoo
So Jericho was coming down, woohoo
We're gonna burn it down down down, woohoo

[Chorus]
For every time I lost my voice
Oh lord, oh, this is redemption, woohoo
So this is redemption
So this is redemption, oh oh
So this is redemption (redemption, redemption, oh oh oh)
For every time I lost my voice
Oh lord, oh, this is redemption, woohoo

[Post-Chorus]
Eh hey ey hey yeah
Eh hey ey hey yeah
Woohoohooh
Woohoohooh
Eh hey ey hey yeah
Eh hey ey hey yeah
Woohoohooh
Oh, so this is redemption
Yeah

[Bridge]
This ain't about no history
This is for all our stories to be told
This ain't about no victory
This is about the place that we call home

[Pre-Chorus]
So this is redemption
Oh, this is redemption
And I've heard it all already
They will never stop me
They will never bring us down
If it's war you know I'm ready
There is fire in my belly
So they will never bring us down

[Chorus]
So this is redemption (redemption, redemption, oh oh oh)
So this is redemption (redemption, redemption, oh oh oh)
For every time I lost my voice
Oh lord, oh, this is redemption, woohoo

[Outro]
So we sing
Eh hey ey hey yeah
Eh hey ey hey yeah
Woohoohooh
Woohoohooh
Eh hey ey hey yeah
Eh hey ey hey yeah
Woohoohooh
So is it love? (redemption, redemption, oh oh oh)
For every time I lost my voice
Oh lord, oh, this is redemption, woohoo
So this is redemption