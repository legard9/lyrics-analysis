[Intro]
I got demons, I got demons, I got demons, I got demons

[Verse 1]
I close my eyes and talk to God
And pray that you can save my soul, mmm
I look to you to shine a light
Before the darkness takes a hold

[Pre-Chorus]
I won't break
And I won't run
This time I won't be afraid oooh

[Chorus]
I got demons
I got demons tryna get to me
But they'll never take me down
I'm only human
Underneath my skin the cuts run deep
I just need a little time to work 'em out
(I got demons, I got demons, I got demons)
To work them out
(I got demons, I got demons, I got demons)
I got demons
(I got demons, I got demons, I got demons)
Yeah

[Verse 2]
I can hear them now and then
When I try to make a stand
They try their best to pull me under
That's when I reach for your hand

[Bridge]
Never bow 'cause we're all kings
We are rulers of our minds, yes we are
I know that there's an angel watching over me
I see your wings are open wide

[Pre-Chorus]
I won't break
And I won't run
This time I won't be afraid oooh
We're only saved when we, come undone
It's just the way that we're made oooh

[Chorus]
I got demons
I got demons tryna get to me
But they'll never take me down, no
I'm only human
Underneath my skin the cuts run deep

[Outro]
I just need a little time to work them out
I'm only human
(I got demons, I got demons, I got demons)
I just need a little time to work them out
(I got demons, I got demons, I got demons)
I got demons
(I got demons, I got demons, I got demons)
I got demons
(I got demons)