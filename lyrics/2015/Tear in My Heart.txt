[HINCKLEY, spoken (Broadway only)]
My dearest Jodie,​
I am humiliated by my weakness and my impotence. But Jodie, I can change. I'll prove to you that I can change. With one brave historic act, I will win your love, now and for all eternity.​
Love, John

[HINCKLEY]
I am nothing
You are wind and water and sky
Jodie
Tell me, Jodie
How I can earn your love
I would swim oceans
I would move mountains
I would do anything for you
What do you want me to do?

I am unworthy of your love
Jodie, Jodie
Let me prove worthy of your love
Tell me how I can earn your love
Set me free
How can I turn your love
To me?

[FROMME]
I am nothing
You are wind and devil and God
Charlie
Take my blood and my body
For your love
Let me feel fire
Let me drink poison
Tell me to tear my heart in two
If that's what you want me to do...

I am unworthy of your love
Charlie darlin'
I have done nothing for your love
Let me be worthy of your love
Set you free—

[HINCKLEY]
I would come take you from your life...

[FROMME]
I would come take you from your cell...

[HINCKLEY]
You would be queen to me, not wife...

[FROMME]
I would crawl belly deep through hell...

[HINCKLEY]
Baby, I'd die for you...

[FROMME]
Baby, I'd die for you...

[HINCKLEY]
Even though—

[FROMME]
Even though—

[BOTH]
I will always know:
I am unworthy of
Your love
Jodie darlin' (Charlie darlin')
Let me prove worthy of your love
I'll find a way to earn your love
Wait and see
Then you will turn your love to me
Your love to me...

[HINCKLEY, spoken (Broadway only)]
He died so our love could live.​

[REAGAN, spoken (Broadway only)]
Sorry Nancy, looks like I forgot to duck.​
I sure hope that surgeon's a Republican.​
Where'd that kid learn to shoot, the Russian Army?
There ya' go again.​
There ya' go again.​
There ya' go again.​