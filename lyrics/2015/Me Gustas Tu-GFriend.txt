Original

[Hook]
널 향한 설레임을 오늘부터 우리는
꿈꾸며 기도하는 오늘부터 우리는
저 바람에 노을 빛 내 맘을 실어 보낼게
그리운 마음이 모여서 내리는
Me gustas tu gustas tu
수투두루 좋아해요
Gustas tu 수투루 수투~루

[Verse 1]
한 발짝 뒤에 섰던 우리는
언제쯤 센치 해질까요
서로 부끄러워서 아무 말도 못하는
너에게로 다가가고 싶은데

[Verse 2]
바람에 나풀거리는 꽃잎처럼
미래는 알 수가 없잖아
이제는 용기 내서 고백할게요

[Pre-Hook]
하나보단 둘이서 서로를 느껴봐요
내 마음 모아서 너에게 전하고 싶어

[Hook]
설레임을 오늘부터 우리는
꿈꾸며 기도하는 오늘부터 우리는
저 바람에 노을 빛 내 맘을 실어 보낼게
그리운 마음이 모여서 내리는
Me gustas tu gustas tu
수투두루 좋아해요
Gustas tu 수투루 수투~루

[Verse 3]
한 걸음 앞에 서서 두 손을
놓지 말기로 약속해요
소중해질 기억을 꼭 꼭 담아둘게요
지금보다 더 아껴주세요

[Verse 4]
달빛에 아른거리는 구름처럼
아쉬운 시간만 가는데
이제는 용기 내서 고백할게요

[Pre-Hook]
둘 보단 하나 되어 서로를 느껴봐요
내 마음 모아서 너에게 전하고 싶어

[Hook]
설레임을 오늘부터 우리는
꿈꾸며 기도하는 오늘부터 우리는
저 바람에 노을 빛 내 맘을 실어 보낼게
그리운 마음이 모여서 내리는

[Bridge]
감싸줄게요 그대 언제까지나 언제까지나
사랑이란 말 안 해도 느낄 수 있어요
고마운 마음을 모아서

[Hook]
널 향한 설레임을 오늘부터 우리는
꿈꾸며 기도하는 오늘부터 우리는
저 바람에 노을 빛 내 맘을 실어 보낼게
그리운 마음이 모여서 내리는
Me gustas tu gustas tu
수투두루 좋아해요
Gustas tu 수투루 수투~루
Romanization

[Hook]
Neol hyanghan seolleimeul oneulbuteo urineun
Kkumkkumyeo gidohaneun oneulbuteo urineun
Jeo barame noeul bit nae mameul sireo bonaelge
Geuriun maeumi moyeoseo naerineun
Me gustas tu gustas tu
Sutuduru johahaeyo
Gustas tu suturu sutu~ru

[Verse 1]
Han baljjak dwie seotdeon urineun
Eonjejjeum senchi haejilkkayo
Seoro bukkeureowoseo amu maldo mothaneun
Neoegero dagagago sipeunde

[Verse 2]
Barame napulgeorineun kkochipcheoreom
Miraeneun al suga eobjanha
Ijenuen yonggi naeseo gobaekhalgeyo

[Pre-Hook]
Hanabodan duriseo seororeul neukkyeobwayo
Nae maeum moaseo neoege jeonhago sipeo

[Hook]
Seolleimeul oneulbuteo urineun
Kkumkkumyeo gidohaneun oneulbuteo urineun
Jeo barame noeul bit nae mameul sireo bonaelge
Geuriun maeumi moyeoseo naerineun
Me gustas tu gustas tu
Sutuduru johahaeyo
Gustas tu suturu sutu~ru

[Verse 3]
Han georeum ape seoseo du soneul
Nohji malgiro yaksokhaeyo
Sojunghaejil gieogeul kkok kkok damadulgeyo
Jigeumboda deo akkyeojuseyo

[Verse 4]
Dalbiche areungeorineun gureumcheoreom
Aswiun siganman ganeunde
Ijenuen yonggi naeseo gobaekhalgeyo

[Pre-Hook]
Dul bodan hana dwieo seororeul neukkyeobwayo
Nae maeum moaseo neoege jeonhago sipeo

[Hook]
Seolleimeul oneulbuteo urineun
Kkumkkumyeo gidohaneun oneulbuteo urineun
Jeo barame noeul bit nae mameul sireo bonaelge
Geuriun maeumi moyeoseo naerineun

[Bridge]
Gamssajulgeyo geudae eonjekkajina eonjekkajina
Sarangiran mal an haedo neukkil su isseoyo
Gomaun maeumeul moaseo

[Hook]
Neol hyanghan seolleimeul oneulbuteo urineun
Kkumkkumyeo gidohaneun oneulbuteo urineun
Jeo barame noeul bit nae mameul sireo bonaelge
Geuriun maeumi moyeoseo naerineun
Me gustas tu gustas tu
Sutuduru johahaeyo
Gustas tu suturu sutu~ru
Translation

[Hook]
My heart flutters toward you
Starting from today, us
We’re dreaming and praying
Starting from today, us
I’ll send you my heart with the wind and the sunset
My heart that longs for you falls down
Me gustas tu gustas tu
Su tu tu ru I like you
Gustas tu su tu ru ru

[Verse 1]
We are standing one step behind
When will we become sentimental?
We’re both so shy, we can’t even say anything
But I wanna go closer to you

[Verse 2]
Like the flowers that sway in the wind
I don’t know our future
But I’ll be brave and confess to you

[Pre-Hook]
Two is better than one
Let’s feel each other
I wanna gather my heart
And tell it to you

[Hook]
My heart flutters toward you
Starting from today, us
We’re dreaming and praying
Starting from today, us
I’ll send you my heart with the wind and the sunset
My heart that longs for you falls down
Me gustas tu gustas tu
Su tu tu ru I like you
Gustas tu su tu ru ru

[Verse 3]
Let’s stand facing each other
Let’s not let go of our hands and let’s promise
I’ll give you precious memories

[Verse 4]
Pleases love me more than now
Like the clouds that flicker under the moonlight
Time keeps ticking
Now I’ll be brave and confess to you

[Pre-Hook]
Two is better than one
Let’s feel each other
I wanna gather my heart
And tell it to you

[Hook]
My heart flutters toward you
Starting from today, us
We’re dreaming and praying
Starting from today, us
I’ll send you my heart with the wind and the sunset
My heart that longs for you falls down

[Bridge]
I’ll hold you
Until always
Until always
Even if you don’t say it’s love
I can feel it
Gathering my thankful feelings

[Hook]
My heart flutters toward you
Starting from today, us
We’re dreaming and praying
Starting from today, us
I’ll send you my heart with the wind and the sunset
My heart that longs for you falls down
Me gustas tu gustas tu
Su tu tu ru I like you
Gustas tu su tu ru ru