[VERSE 1]
When the cold wind starts to blow
The night sky shines
On my way back home, after a long day
I suddenly thought of you
Where are you? What are you doing right now?

[Chorus]
Won’t you come look at the stars with me?
Am in front of your house, will you come out for a second?
Just put on something light
We won’t go too far
But I want to hold your hand
Beautiful constellations, though I don’t know their names
Wanna come with me?

[VERSE 2]
When the cold wind starts to blow
I think of you so much
After a long day, I’m walking on the quiet night street
The night sky is so nice
Where are you? What are you doing right now?

[Chorus]
Won’t you come look at the stars with me?
I don’t care where we go, can you come out?
So many things I want to tell you
But I won’t rush
But I want to hold your hand
Beautiful constellations, though I don’t know their names
Wanna go with me?

[VERSE 3]
Though I don’t know the place
Where our footsteps are headed
I used to be alone in the night sky
But if I can walk through it with you
That’s all I need

[OUTRO]
Nanana Nanana
Nanana Nanana