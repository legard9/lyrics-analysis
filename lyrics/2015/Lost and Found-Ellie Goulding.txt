[Verse 1]
Gotta love this field and the cherry sky
Under blossom clouds though it's late July
You don't even try; still, you look so cool
Like a cover boy when you light the moon

[Verse 2]
Near the countryside but I just forgot
All the things that matter I forget a lot
I get so caught up in the city cloud
But this place is still my first love

[Chorus]
Is there anybody out there waiting for me on my way?
If that somebody is you, then baby, I just wanna say
Tonight, nothing will bring us down
Tonight, we're at the lost and found

[Verse 3]
Guess I need to run, take me to the bus
But don't let me go, no, don't let me on
Gotta hold me tight, won't put up a fight
Of course I'll stay, I'll stay the night

[Chorus]
Is there anybody out there waiting for me on my way?
If that somebody is you, then baby, I just wanna say
Tonight, nothing will bring us down
Tonight, we're at the lost and found

[Bridge]
We're at the lost and found
We're at the lost and found

[Chorus]
Is there anybody out there waiting for me on my way?
If that somebody is you, then baby, I just wanna say
Tonight, nothing will bring us down
Tonight, we're at the lost and found

[Outro]
We're at the lost and found
We're at the lost and found