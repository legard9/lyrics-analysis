[Verse 1]
Pain comes, lonely weekend, shoulder, been here before
Locked up, stepped on, so prominent, so loud, I can't belong
Only by my so wide-eyed other side, I can't belong
Rain comes, priceless, do whatever works best, been here before

[Chorus]
If I could be good enough, then I would be blown away
And I could be their everyone, and I could be there every day
If I could be good enough, then I could just glow
If I could let go, then I could change the world
But I can't stop always tearing myself down
I can be good enough, yeah, I will break ground

[Verse 2]
I focus on the dawn, man, 'cause something doesn't feel right
I feel it in my bones like God is throwing stones
Yeah, I'm gonna find the light
And the weight of what you're going through
Is bigger than your will to change
You have to start somewhere; despair won't wear wherever again

[Chorus]
If I could be good enough, then I would be blown away
And I could be their everyone, and I could be there every day
If I could be good enough, then I could just glow
If I could let go, then I could change the world
But I can't stop always tearing myself down
I can be good enough, yeah, I will break ground

[Bridge]
Proud, calm
Loud, strong
Heart, soul
I let go

[Chorus]
If I could let go, then I could change the world
But I can't stop always tearing myself down
I can be good enough, yeah, I will break ground
But I can't stop always tearing myself down
I can be good enough, yeah, I will break ground
Yeah, I will break ground
I can be good enough, yeah, I will break ground