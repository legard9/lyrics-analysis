I live my life like I’m on a hopscotch
Square one, square two, square three and again
I live my life like I’m on a hopscotch
Going for the perfect ten
I live my life like I’m on a hopscotch
Square one, square two, square three and again
I live my life like I’m on a hopscotch
Going for the perfect ten

White lines on the concrete
Feel the ground underneath my feet
I let the wind in front of me
I hear it calling

Had enough of the comfort zone
I breathe in and I touch the stone
Don’t know where this whole thing goes
I jump to find out

They say that I’m insane
But I say it’s just a game

I live my life like I’m on a hopscotch
Square one, square two, square three and again
I live my life like I’m on a hopscotch
Going for the perfect ten
I live my life like I’m on a hopscotch
Square one, square two, square three and again
I live my life like I’m on a hopscotch
Going for the perfect ten

Holding on and I don’t look back
Want to feel like a maniac
Might end up with a broken leg
I know tomorrow
Everything will be washed away
By the rain of yesterday

Taking on a brand new day
I jump to find out
They say that I’m insane
But I say it’s just a game

I live my life like I’m on a hopscotch
Square one, square two, square three and again
I live my life like I’m on a hopscotch
Going for the perfect ten
I live my life like I’m on a hopscotch
Square one, square two, square three and again
I live my life like I’m on a hopscotch
Going for the perfect ten

I won’t go back to zero
The way I feel all
The skies are clear
So I laugh, I live, I love
I won’t go back to zero
The skies are clear so
Right leg, left leg, both legs and hop

I live my life like I’m on a hopscotch
Square one, square two, square three and again
I live my life like I’m on a hopscotch
Going for the perfect ten
I live my life like I’m on a hopscotch
Square one, square two, square three and again
I live my life like I’m on a hopscotch
Going for the perfect ten

The skies are clear so
The way I feel oh
I laugh, I live, I love