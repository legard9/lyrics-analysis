[Verse 1: Sister Margie]
You see I'm just a nun and I'm having some fun
Don't mess with me or I'll slap with my gun
Yeah I'm talking 'bout nuns with guns
If you don't think that's fun, run and tell your mum
Making that funny stuff call me Adrian Bliss
In this modern musical abyss
Now you better believe this
After you've heard this, yeah, you'll reminisce
Some of you rappers more boring than a J-cloth
Chasing the light like a night-time-blind moth
The content still as bland as a tablecloth
Bored of you fellas - I've just had enough

[Hook: Sister Margie]
It's safe to say I'm a bit of a threat
With bars about Jesus and no disrespect
I hope the best for you and for your family
Get used to my name it's Sister Margie

[Verse 2: Sister Margie]
I can only rap on a beat that slaps
I can only Rhyme if it puts me on the map
My competition around me have a big gap
Bringing you Rap, Grime, Pop, don't forget trap
You can't forget what the raps about
Don't be thinking it's just about Clout
Too many rappers just rap about rap
There's a whole world out there, rap about that
So it's me your main Sister, Sister Margie
With the album so disruptive, I call it Margie-Bargy
Rhymezone on this lyrical-lyrical
Bask down in my lyrical glory

[Hook: Sister Margie]
It's safe to say I'm a bit of a threat
With bars about Jesus and no disrespect
I hope the best for you and for your family
Get used to my name it's Sister Margie

[Verse 3: Sister Margie]
People these days are real hella bold:
"Hey check out my SoundCloud!" Nah, it's like mould
How many times do you have to be told
Stop inboxing me, to that I'm opposed
Don't mess with me I'll create a spark
Watch out, I'll shank your Nan in a Greggs car park
Take this album as a lyrical benchmark
Off the wall flows going down like the Bismarck
Now listen, ladies, I've said I'm good
Ever since I started making trouble in your neighbourhood
But do I flow better than the River Clwyd?
Ooh, I think I'm getting up to no good!