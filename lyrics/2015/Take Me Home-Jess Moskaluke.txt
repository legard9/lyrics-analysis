Say you were hoping you'd run into me here
And for old times you'd like to buy me a beer
But you ain't gonna take me home
You reach out and brush the hair from my face
Look in my eyes like nothing's changed
But you ain't gonna take me home

You probably had a fight
Slammed the door and walked out
She's probably waiting up
For you baby right now
You could stay here all night like you just might
Yeah but we both know
You ain't gonna take me home

You move in close like it's too loud in this bar
You say that you and her are falling apart
But you ain't gonna take me home

You probably had a fight
Slammed the door and walked out
She's probably waiting up
For you baby right now
You could stay here all night like you just might
Yeah but we both know
You ain't gonna take me home

I know you had a fight
Slammed the door and walked out
I used to be the one
Waiting for you right now
You can stay here all night like you just might
But we both know
You ain't gonna take me home
Still get me buzzing every time you touch my hand
You're gonna lean in try to kiss me but I'll just pull back
So when she calls again
Go ahead pick up your phone
Cause you ain't gonna take me home
Oh no
Oh, oh
We both know
You ain't gonna take me home