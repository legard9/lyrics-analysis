[Intro, spoken: Nancy]
Like what? Like something great, spectacular, out of this world
I'm practically an expert on deals JoJo and have I got a deal for you

[Verse 1: Nancy]
You've got what I want, so let's get a-swapping
We'll search high and low and find
Something jaw-dropping
Let's find you a steal
Have I got a deal for you

[Verse 2: Nancy]
I'll trade you something great
Stylish and so chic
My old roller-skates slightly used
Just think, uh, antique
Let's find you a steal
Have I got a deal for you

[Verse 3: Nancy]
Ooh, now about this ball of twine here?
A dress that's très devine here
This book's one of a kind, dear
You want something even finer?
Um, a deal is a give and take
A trade of this for that
Ooh, how about this tambourine?
Or a chapeau
That's French for "hat"
It'll be so unreal
Have I got a deal for you

[Verse 4: Nancy]
There's so much to trade
Oh, why can't you be swayed?
Let's walk with zeal
Cause I wanna make a deal
Jojo, I must make a deal with you

[Outro, spoken: Nancy & 
JoJo
]
No, no, no!
Ugh!