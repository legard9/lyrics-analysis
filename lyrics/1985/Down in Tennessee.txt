She's gone, yes she's gone
She's gone, solid gone

Ah, the girl's a valentine, she's fine
Her eyes are green and her hair is brown
Ah, that's a tailor-made woman, that ain't no hand-me-down

Well now, just as soon as I see her I'm gonna say
"Get your fine self right home today!"
And then just one more thing I've gotta know
What in the world did I do to make the woman go?

She's gone, yes she's gone
She's gone, solid gone

Now she might be in Chicago
I thought I saw her down in Tennessee
I traveled round the world to bring my baby on home to me

She's gone, yes she's gone
She's gone, solid gone

And now some people just call her sugar
Although her real name is Nancy Joe
She's the only gal for me
I'm going to be her steady boy

Ah, just as soon as I see her
I'm gonna say "you better get it on home today"
Just one thing I need to know
What in the world did I do to make the woman go?

She's gone, yes she's gone
She's gone, solid gone

Well now she might be in Chicago
I thought I saw her down in Tennessee
I traveled round the world to bring my baby on home to me