[Verse 1]
Please let me make a true confession
I have never been in love before
But since you came in my direction
I've had a change, a change of heart
My girls had come a dime a dozen
I fed them the things they love to hear
I never was wanting for a lover
But I never knew what true love was indeed

[Chorus]
(Who do you love?)
Girl I'm in love with you
(Are you for sure?)
Sure as the sky is blue
(Who do you love?)
Just let me prove to you
(Are you for sure?)
Yes, girl my love is true
(Who do you love?)
Girl I'm in love with you
(Are you for sure?)
Sure as the sky is blue
(Who do you love?)
Just let me prove to you
(Are you for sure?)
Yes, girl my love is true

[Verse 2]
I used to be the great pretender
For the very first time I'm for real
You've got me listening to my heart now
You gotta believe, how I feel
I am ready to be become the giver
Now that I realized it's give and take
Oooh your love has broken my defenses
This time I'm not just on the make
I gotta tell you babe...

[Chorus]
(Who do you love?)
Girl I'm in love with you
(Are you for sure?)
Sure as the sky is blue
(Who do you love?)
Just let me prove to you
(Are you for sure?)
Yes, girl my love is true
(Who do you love?)
Girl I'm in love with you
(Are you for sure?)
Sure as the sky is blue
(Who do you love?)
Just let me prove to you
(Are you for sure?)
Yes, girl my love is true

[Bridge]
(How do I know that you won't go, please tell me)
I don't wanna lose your love
(And if it's true I'll stay with you)
Stay with me
That would make me so very happy
(How do I know that you won't go, please tell me)
I don't wanna lose your love
(And if it's true I'll stay with you)
Stay with me
That would make me so very happy
You got to believe me (How do I know that you won't go, please tell me)
I don't wanna lose your love
(And if it's true I'll stay with you)
Stay with me
That would make me so very happy

[Chorus]
(Who do you love?)
Girl I'm in love with you
(Are you for sure?)
Sure as the sky is blue
(Who do you love?)
Just let me prove to you
(Are you for sure?)
Yes, girl my love is true
(Who do you love?)
Girl I'm in love with you
(Are you for sure?)
Sure as the sky is blue
(Who do you love?)
Just let me prove to you
(Are you for sure?)
Yes, girl my love is true

[Outro]
(Who do you love?)
Girl I'm in love with you
(Are you for sure?)
Sure as the sky is blue
(Who do you love?)
Just let me prove to you
(Are you for sure?)
Yes, girl my love is true