Crickets
 are chirping. We see a shot of the night sky. A caption pops up, “NOVEMBER 6th, 1983/HAWKINS, INDIANA”. The caption fades away. The camera pans down to a government facility. Another caption pops up. “HAWKINS NATIONAL LABORATORY/U.S. DEPARTMENT OF ENERGY’.
CUT TO: INT. HALLWAY
Some lights are flickering. The camera pans down to a door. After a few moments, a man in a lab coat bursts through the door, running. Behind him, an alarm is sounding. Cut to him running through hallways. He eventually reaches an elevator and frantically pushes the button. The elevator begins coming down. He looks back down the hallway and then goes back to pushing the button. The elevator dings and opens. He gets in and pushes the button inside of the elevator. He stands for a moment, breathing heavily. A low growling is heard above him. He looks up and the growling grows louder. As the elevator doors close, he screams and is pulled upwards.
CUT TO: EXT. WHEELER HOUSEHOLD
A sprinkler is on outside. Cut to a wide shot of the house.
MIKE: Something is coming. Something hungry for blood.
Cut to Mike inside the house.
MIKE: A shadow grows on the wall behind you, swallowing you in darkness.
It is almost here.
The camera shows us Dustin, Lucas, and Will, before switching to a shot showing all four boys around a table. They’re playing Dungeons and Dragons.
WILL: What is it?
DUSTIN: What if it's the Demogorgon? Oh, Jesus, we're so screwed if it's the Demogorgon.
LUCAS: It's not the Demogorgon.
MIKE: An army of troglodytes charge into the chamber!
Mike slams a figurine onto the table.
DUSTIN: Troglodytes?
LUCAS: Told ya.
They laugh. Mike suddenly looks nervous and looks around.
MIKE: Wait a minute. Did you hear that? That... that sound? Boom... boom... Boom!
He slams his hands on the table, startling the other boys.
MIKE: That didn't come from the troglodytes, no, that... That came from something else. The Demogorgon!
Mike slams another figurine onto the table.
DUSTIN: We're in deep shit.
MIKE: Will, your action!
WILL: I don't know!
LUCAS: Fireball him!
WILL: I'd have to roll a 13 or higher!
DUSTIN: Too risky. Cast a protection spell.
LUCAS: Don't be a pussy. Fireball him!
DUSTIN: Cast Protection.
MIKE: The Demogorgon is tired of your silly human bickering! It stomps towards you. Boom!
LUCAS: Fireball him, Will!
MIKE: Another stomp, boom!
DUSTIN: Cast Protection.
MIKE: He roars in anger!
WILL: Fireball!
Will throws some dice, but they fall off the table.
WILL: Oh, shit!
LUCAS: Where'd it go? Where is it?
They all get up from the table to search for the die.
WILL: I don't know!
DUSTIN: Is it a 13?
WILL: I don't know!
LUCAS: Where is it?
DUSTIN: Oh, my God! Oh, my God! Oh, my God!
KAREN: Mike! Mike!
LUCAS: Can you find it yet?
WILL: No, I can't find it!
KAREN: Mike!
Karen opens the door to the basement. Mike is at the bottom of the stairs.
DUSTIN: Oh, my God! Oh, my God!
MIKE: Mom, we're in the middle of a campaign!
KAREN: You mean the end? Fifteen after.
She leaves. Mike runs up after her.
LUCAS: Oh, my God! Freaking idiot!
WILL: Why do we have to go?
MIKE: Mom, wait, just 20 more minutes!
Mike walks into the kitchen, where his mom is. His dad fiddles with the TV in the background.
KAREN: It's a school night, Michael. I just put Holly to bed. You can finish next weekend.
MIKE: But that'll ruin the flow!
KAREN: Michael I'm serious.
MIKE: Mom, the campaign took two weeks to plan. How was I supposed to know it was gonna take ten hours?
KAREN: You've been playing for ten hours?
MIKE: Dad, don't you think that 20 more -
TED: I think you should listen to your mother. (
He hits the TV and groans.
) Dang dumb piece of junk.
Cut back to the basement.
WILL: Oh, I got it! Does the seven count?
LUCAS: It was a seven? Did Mike see it? (
Will shakes his head.
) Then it doesn't count.
The boys get ready to leave. Dustin picks up a pizza box.
DUSTIN: Yo, hey, guys. Does anyone want this?
LUCAS & WIL: No.
Dustin goes upstairs to Nancy’s room with the pizza. She’s on the phone with Barbara.
NANCY: Yeah. No, I don't think... Yeah, he's cute. Barb, no, I don't think so. Barb, you're not -
DUSTIN: Hey, Nancy. There's a slice left if you want it. Sausage and pepperoni!
NANCY: Hold on.
She puts the phone down and walks over to the door. Dustin smiles. She smiles and closes the door in his face. Cut to Dustin joining the other boys outside.
DUSTIN: There's something wrong with your sister.
MIKE: What are you talking about?
DUSTIN: She's got a stick up her butt.
LUCAS: Yeah. It's because she's been dating that douchebag, Steve Harrington.
DUSTIN: Yeah, she's turning into a real jerk.
MIKE: She's always been a real jerk.
DUSTIN: Nuh-uh, she used to be cool.
Lucas, Dustin, and Will begin riding away on their bikes.
DUSTIN: Like that time she dressed up as an elf for our Elder tree campaign.
MIKE: Four years ago!
DUSTIN: Just saying.
LUCAS: Later.
Will stays behind for a moment.
WILL: It was a seven.
MIKE: Huh?
WILL: The roll, it was a seven. The Demogorgon, it got me. See you tomorrow.
As Will catches up with Lucas and Dustin, the garage lights flicker. Mike turns off the lights and heads back inside. Cut to the other boys riding home. Lucas turns into his driveway.
LUCAS: Good night, ladies.
DUSTIN: Kiss your mom 'night for me. (
To Will
) Race you back to my place? Winner gets a comic.
WILL: Any comic?
DUSTIN: Yeah.
Will speeds ahead of Dustin.
DUSTIN: Hey! Hey! I didn't say "go"! Get back here! I'm gonna kill you!
WILL: (
As he passes Dustin’s house
) I'll take your X-Men 134!
Will continues to speed away. Dustin stops in front of his house, panting.
DUSTIN: Son of a bitch.
Cut to Will continuing to ride. The camera pans to some signs on a fence he passes. One reads, “hnL/HAWKINS NATIONAL LABORATORY/U.S. DEPARTMENT OF ENERGY”. A second reads, “RESTRICTED/AREA/NO TRESPASSING/U.S. GOVERNMENT/PROPERTY”. The light on Will’s bike flickers off. It flickers back on just in time for Will to spot a figure in the road. Will gasps and veers off the road, crashing.
He looks up at the road to see the figure turning towards him, with unnaturally long arms. He abandons his bike and runs into the forest. Cut to him reaching his house. He quickly runs inside and locks the door behind him. His dog barks. He looks for his family.
WILL: Mom? Jonathan? Mom?
He looks out a window and sees the figure approaching the house. He tries dialing 911, but all he receives on the other end is static.
WILL: Hello? Hello?
He sees the figure approach the front door. His dog continues barking, but backs away. The lock unlocks. Will runs to the shed behind the house, leaving the phone off the hook. He retrieves a shotgun from the wall of the shed and frantically begins to load it. He points it at the door, but after a few moments, hears growling from the back of the shed. He turns around and his eyes widen. The growling turns into screeching. The light in the shed gets brighter and brighter, until it suddenly stops, as does the shrieking. The camera cuts to a shot of the entire shed, now empty. The screen fades to black.
OPENING CREDITS
CUT TO: INT. HOPPER’S HOUSE
We see a child’s drawing of a kid and two adults next to a house. The camera slowly pans over various empty cans and trash to a TV, which is on.
DONNA: And that's it for News Center this morning. Thanks for joining us. Let's hand off now to Liz at the news desk.
LIZ: All right, thank you, Donna. Turning now to local news, we're getting reports of surges and power outages all across the county. Last night, hundreds of homes in East Hawkins were affected, leaving many residents in the dark. The cause of the outage is still unknown. We reached out to Roane County Water and Electric, and a spokesperson says that they are confident power will be restored to all remaining homes within the next several hours.
The camera continues panning to Hopper, who is asleep on his couch. Somewhere, a dog barks, waking Hopper. He sighs and looks at his watch. Cut to a shot of the lake, which then pans over to Hopper smoking on his porch. Back inside, he takes a shower, brushes his teeth, smokes some more, takes some pills, drinks some beer, and smokes even more. Cut to him putting on his police uniform, including his holster, his badge, and his hat. As he leaves, the camera pans back to the TV.
LIZ: In other news, you might wanna stay home tonight or at least pack an umbrella. We turn to everybody's favorite morning weather guy, Charles.
CUT TO: EXT. BYER’S HOUSEHOLD
JOYCE: Where the hell are they?
Cut to inside the house. Jonathan is making breakfast.
JOYCE: Jonathan?
JONATHAN: Check the couch!
Cut to Joyce searching the couch.
JOYCE: Ugh, I did. (
She finds them
) Oh, got them.
She enters the kitchen.
JOYCE: Okay, sweetie, I will see you tonight.
JONATHAN: Yeah, see you later.
JOYCE: Where's Will?
JONATHAN: Oh, I didn't get him up yet. He's probably still sleeping.
JOYCE: Jonathan, you have to make sure he's up!
JONATHAN: Mom, I'm making breakfast.
Joyce goes to wake up Will.
JOYCE: I told you this a thousand times. Will! Come on, honey. It's time to get up. (
She finds that Will isn't in his room
) He came home last night, right?
JONATHAN: He's not in his room?
JOYCE: Did he come home or not?
JONATHAN: I don't know.
JOYCE: You don't know?
JONATHAN: No. I got home late. I was working.
JOYCE: You were working?
JONATHAN: Eric asked if I could cover. I said yeah. I just thought we could use the extra cash.
JOYCE: Jonathan, we've talked about this.
JONATHAN: I know...
JOYCE: You can't take shifts when I'm working.
JONATHAN: Mom, it's not a big deal. Look, he was at the Wheelers' all day. I'm sure he just stayed over.
JOYCE: I can't believe you. I can't believe you sometimes.
Joyce picks up the phone and calls the Wheelers. Cut to the Wheelers eating breakfast, with the phone ringing in the background. Nancy watches Mike pour syrup on his food.
NANCY: That's disgusting.
MIKE: You're disgusting.
Karen picks up the phone.
KAREN: Hello?
JOYCE: Hi, Karen. It's Joyce.
KAREN: Oh, Joyce, hi.
Mike squirts syrup on Nancy's plate.
NANCY: What the hell, Mike?
TED: Hey!
KAREN: Quiet!
TED: Language.
NANCY: Are you kidding?
JOYCE: Was that Will I heard back there?
KAREN: Will? No, no, no, it's just Mike.
JOYCE: Will didn't spend the night?
KAREN: No, he left here a little bit after 8:00. Why? He's not home?
JOYCE: Um, you know what? I think he just left early for... for school. Thank you so much.
KAREN: Okay.
JOYCE: Bye.
KAREN: Bye.
Joyce and Jonathan exchange glances.
CUT TO: EXT. HAWKINS MIDDLE SCHOOL
Mike, Lucas, and Dustin bike to school. They lock their bikes up.
MIKE: That's weird. I don't see him.
LUCAS: I'm telling you, his mom's right. He probably just went to class early again.
DUSTIN: Yeah, he's always paranoid Gursky's gonna give him another pop quiz.
Two school bullies, Troy and James, approach them.
TROY: Step right up, ladies and gentlemen. Step right up and get your tickets for the freak show. Who do you think would make more money in a freak show? Midnight, Frogface, or Toothless?
As he says this, he pushes the boys one by one. James looks pensive for a moment.
JAMES: I'd go with Toothless.
DUSTIN: I told you a million times, my teeth are coming in. It's called cleidocranial dysplasia.
JAMES: (
In a mocking voice
) "I told you a million times."
TROY: Do the arm thing.
JAMES: Do it, freak!
Dustin cracks his arms. James and Troy groan and recoil.
TROY: God, it gets me every time.
Troy and James walk away, pushing the boys as they go.
LUCAS: Assholes.
MIKE: I think it's kinda cool. It's like you have superpowers or something. Like Mr. Fantastic.
DUSTIN: Yeah, except I can't fight evil with it.
CUT TO: INT. HALLWAY AT HIGH SCHOOL
BARB: So, did he call?
NANCY: Keep your voice down.
BARB: Did he?
NANCY: I told you, it's not like that. Okay, I mean, yes, he likes me, but not like that. We just made out a couple times.
BARB: "We just made out a couple times." Nance, seriously, you're gonna be so cool now, it's ridiculous.
NANCY: No, I'm not.
BARB: You better still hang out with me, that's all I'm saying. If you become friends with Tommy H. or Carol -
NANCY: Oh, that's gross! Okay, I'm telling you, it was a one-time... Two-time thing.
Inside Nancy's locker is a note "MEET ME IN BATHROOM. STEVE".
BARB: You were saying?
CUT TO: INT. BATHROOM AT HIGH SCHOOL
Nancy and Steve are having a making out session.
NANCY: Steve
STEVE: Mmm-hmm?
NANCY:  I have to go.
STEVE: In a minute.
The school bell rings.
NANCY: Steve. I really, like seriously, I have to go.
STEVE:Wait, wait, wait. Let's... Come on, let's do something tonight, yeah?
NANCY:Uh No, I can't. I have to study for Kaminsky's test.
STEVE:Oh, come on. What's your GPA again? 3.999...
NANCY:Kaminsky's tests are impossible.
STEVE: Well, then, just let me help.
NANCY: You failed chem.
STEVE: C-minus.
NANCY: Well, in that case...
STEVE:So I'll be over around, say, like, 8:00?
NANCY: Are you crazy? My mom would not...
STEVE: I'll climb through your window. She won't even know I'm there. I'm stealthy, like a ninja.
NANCY: You are crazy.
STEVE: Wait, wait, wait. Just... Okay, forget about that. We can just We can just, like, chill in my car. We can find a nice quiet place to park, and.
NANCY: Steve, I have to study. I'm not kidding.
STEVE: Well, why do you think I want it to be nice and quiet?
NANCY: You're an idiot, Steve Harrington.
She's leaving but then turns around...
NANCY: Meet me at Dearborn and Maple at 8:00. To study.
She leaves as the bell rings, Steve picks up his stuff.
Cut to: EXT, POLICE STATION
A van arrives at the police...
CUT TO: INT. POLICE STATION
...and its driver, Chief of Police Jim Hopper , enters the place.
FLO: Good of you to show.
After passing by receptionist Flo, he comes near the table where officers Powell and Callahan are playing cards.
JIM:Oh, hey, morning, Flo. Morning, everybody.
POWELL: Hey, Chief.
CALLAHAN: Damn! You look like hell, Chief.
JIM: Oh, yeah?
CALLAHAN:  Yeah.
JIM:Well, I looked better than your wife when I left her this morning.
FLO: While you were drinking or sleeping, or whatever it is you deemed so necessary on Monday morning, Phil Larson called. Said some kids are stealing the gnomes out of his garden again.
JIM: Oh, those garden gnomes again. Well, I'll tell you what, I'm gonna get right on that.
Jim picks up a donut and messes with the cards in Powell's hand.
FLO: On a more pressing matter, Joyce Byers can't find her son this morning.
JIM:Mmm. Okay, I'm gonna get on that. - Just give me a minute.
FLO: Joyce is very upset.
JIM: Well, Flo, Flo, we've discussed this. Mornings are for coffee and contemplation.
FLO:  Chief, she's already in your....
JIM: Coffee and contemplation, Flo!
He enters the room, and, due to not paying attention, is surprised by Joyce already waiting.
Chief is now typing the file on Joyce's charges as she, clearly nervous, smokes for relief.
JOYCE: I have been waiting here over an hour, Hopper.
JIM: And I apologize again.
JOYCE:I'm going out of my mind!
JIM: Look, boy his age, he's probably just playing hookie, okay?
JOYCE:No, not my Will. He's not like that. - He wouldn't do that.
JIM:Well, you never know. I mean, my mom thought I was on the debate team, when really I was just screwing Chrissy Carpenter in the back of my dad's Oldsmobile, so...
JOYCE:Look, he's not like you, Hopper. He's not like me. He's not like most. He has a couple of friends, but, you know, the kids, they're mean. They make fun of him. They call him names. They laugh at him, his clothes...
JIM:His clothes? What's wrong with his clothes?
JOYCE: I don't know.Does that matter?
JIM:Maybe.
JOYCE: Look, he's... He's a sensitive kid. Lonnie... Lonnie used to say he was queer. Called him a fag.
JIM: Is he?
JOYCE:He's missing! Is what he is.
JIM: When was the last time you heard from Lonnie?
JOYCE:Uh, last I heard, he was in Indianapolis. That was about a year ago. But he has nothing to do with this.
JIM:  Why don't you give me his number?
JOYCE: You know, Hopper, he has nothing to do with this. Trust me.
JIM: Joyce, 99 out of 100 times, kid goes missing, the kid is with a parent or relative.
JOYCE:What about the other time?
JIM:  What?
JOYCE: You said, "99 out of 100." What about the other time, the one?
JIM: Joyce.
JOYCE: The one!
JIM: Joyce, this is Hawkins, okay? You wanna know the worst thing that's ever happened here in the four years I've been working here? Do you wanna know the worst thing? It was when an owl attacked Eleanor Gillespie's head because it thought that her hair was a nest.
JOYCE: Okay, fine. I will call Lonnie. He will talk to me before he talks to...
JIM: What, a pig?
JOYCE: A cop! Just find my son, Hop. Find him!
Cut to HAWKINS LAB, EXT
Cars arrive at the research facility. From them, emerge a team of scientists carrying briefcases, including...
SCIENTIST: Dr. Brenner.
...who shakes this scientist's hand.
Cut to HAWKINS LAB, INT
BRENNER: This way, gentlemen. The entire east wing will be evacuated within the hour. We've sealed off this area following quarantine protocol.
The team of scientists, including Dr. Brenner, suit up in protective suits, grab guns and flashlights, and go down an elevator, where they proceed to the quarantined underground subsystem. There, they find strange biologic growth spreading - originating from a mysterious fracture in the wall.
SCIENTIST: This is where it came from?
BRENNAN: Yes.
SCIENTIST: And the girl?
BRENNAN:She can't have gone far.
A growl is heard from the growth as we...
Cut to FOREST
...see two bare feet walking along the dry leaves. As they stop, the camera pans up to a young girl dressed in a hospital gown, with a completely shaven head. Then we cut to her point of view.
Cut to BENNY'S BURGERS, EXT
She's watching Benny Hammond, owner of the local burger joint, leaving through the back door to put garbage in the can outside. As he walks back, the girl is pensive.
Cut to BENNY'S BURGERS, INT
The girl enters the back door, passes by the pantry, and gets to see Benny serving a costumer.
BENNY: All right, and one more.
Hey, Ben. What do you think about that, uh
BENNY: Hey, I don't know.
-I don't know.
I don't know, 37 points per game average - Thirty-seven now, but
- Mr. Basket.
The girl enters the kitchen, eventually appearing by a basket of fries, which she starts to gobble. Benny eventually notices her...
BENNY: Hey! Come here!
...and she grabs the basket and tries running away, before Benny gets her just as she's about to leave through the door she came in.
BENNY:Hey, come here! You think you can steal from me, boy?
He then takes a look at the kid he just caught
BENNY: the girl,What in the hell?
Remember, finish chapter 12 and answer 12.
3 on the difference between an experiment and other forms of science investigation.
This will be on the test, which will cover chapters 10 through 12.
It will be multiple choice with an essay section.
So, did it come? Sorry, boys.
I hate to be the bearer of bad news, but it came.
Yes!
The Heathkit ham shack.
- Whoa.
- Ain't she a beaut?
I bet you can talk to New York on this thing.
Think bigger.
- California?
- Bigger.
Australia? Oh, man! When Will sees this, he's totally gonna blow his shit.
Lucas!
Sorry.
Hello, this is Mike Wheeler, president of Hawkins Middle AV Club.
What are you doing?
Hello, this is Dustin, and this is the secretary and treasurer of Hawkins Middle AV Club. Do you eat kangaroos for breakfast?
Sorry to interrupt, but, uh, may I borrow Michael, Lucas and Dustin?
JIM: Okay, okay, okay. One at a time, all right? [points to Mike] You. You said he takes what?
MIKE: Mirkwood.
JIM:  Mirkwood?
MIKE: Yeah.
JIM:  Have you ever heard of Mirkwood?
CALLAHAN: I have not. That sounds made up to me.
LUCAS: No, it's from Lord of the Rings.
DUSTIN: Well, The Hobbit.
LUCAS: Who cares?
DUSTIN:  He asked!
LUCAS: [mocking]"He asked!"
MIKE:  Shut up, guys!
JIM:  Hey, hey, hey!  What'd I just say?  Shut up. One at a damn time. [to Mike] You.
MIKE:  Mirkwood, it's a real road. It's just the name that's made up. It's where Cornwallis and Kerley meet.
JIM:  Yeah, all right, I think I know that.
MIKE: We can show you, if you want.
JIM: I said that I know it!
MIKE: We can help look.
DUSTIN: Yeah.
JIM: No.

[the boys protest]
JIM: No. After school, you are all to go home. Immediately. That means no biking around looking for your friend, no investigating, no nonsense. This isn't some Lord of the Rings book.
DUSTIN: The Hobbit.
LUCAS: Shut up!
- Hey!
JIM: Stop it! Do I make myself clear? Do I make myself clear?
MIKE: Yes, sir.
- Yeah.
Ring-a-ding-ding! Anybody home?
Password?
Uh, Rada Radaga Radagast?
Yeah.
You may enter.
Thank you, sir.
So, guess what? I got off early and Ta-da! Poltergeist.
I I thought I wasn't allowed to see it.
I changed my mind.
As long as you don't have nightmares for a week.
No, I won't.
I don't get scared like that anymore.
Oh, yeah? Not even of clowns?
No.
What about my witch?
- No.
Mom
- Ooh
- I'm not five anymore.
- But Will Byers
- I'm going to cook you up in my
- Stop.
That's so stupid.
Mom!
Will?
- Will?
- Will? Will?
- Will?
- Will!
- Will!
- Will? Will! Where are you?
Geez.
Your parents forget to feed you? Is that why you ran away? They, uh They hurt you? You went to the hospital, you got scared, you ran off, you wound up here, is that it? All right.
I'll give this back, all right? And you can have as much as you want.
All right? Maybe even some ice cream.
But you gotta answer a few of my questions first, all right? We got a deal? All right, let's start with the easy stuff.
All right? My name's Benny.
Benny Hammond.
See? Like this.
Here.
I got you.
Don't worry.
It's okay.
Nice to meet you, yeah.
And you are? Eleven? What's that mean? What's it mean? -
No.
- Well, I'll be damned.
She speaks.
"No"? No, what? All right.
I guess no more food, then.
Eleven.
Yeah.
What's it mean?
Eleven.
All right, then.
Here you go.
Take it easy, take it easy.
Yeah, look, all I know is that she's scared to death.
Yeah, I think maybe she's been abused or kidnapped or something.
Yeah, it'd be great if someone would come by.
Yeah, we're at 4819 Randolph Lane.
Yeah, Randolph.
Will Byers? Will! Will Byers? Come on, kid! Will? Hey! I got something.
- That his bike?
- Yeah, he must have crashed.
You think he got hurt in the fall?
Not so hurt he couldn't walk away.
Bike like this is like a Cadillac to these kids.
He would've walked it home.
get off work if I win.
Oh, Friday I'm supposed to but we will cover whatever the damages are to your property.
- It's just, um - That's for sure.
Is Lonnie there? - Lonnie isn't here right now.
- Can you please - I told you, Lonnie's not here.
- Who is this? - His girlfriend, Cynthia.
- Cynthia.
- Who the hell is this? - Cynthia, this is Joyce.
- Who? - Lonnie's ex-wife.
- I really need to speak to him - Lonnie's not here.
- Can you please put
No!
- Why don't you call back later?
No, not later. Now!
Can
- Bitch!
- Mom.
- What?
- You have to stay calm.
Hey, you've reached Lonnie.
Leave a message and I'll holler right back at ya.
Lonnie, some teenager just hung up on me.
Will is is missing.
I don't know where he is.
I need I just need you to call me back, please, just Damn it! Damn it! Mom? What? Cops.
- It was just lying there? - Yeah.
Cal? - Did it have any blood on it, or - No, no, no, no, no Phil? If you found the bike out there, why are you here? - Well, he had a key to the house, right? - Yeah.
So maybe he came home.
You think I didn't check my own house? I'm not saying that.
- Has this always been here? - What? I don't know.
Probably.
I mean, I have two boys.
Look at this place.
You're not sure? Hey.
Hey, what's up with this guy, huh? Nothing, he's probably just hungry.
Come on.
- Hey!
- Jesus! What are you, deaf? I've been calling you.
What's going on? Hello?
- Are you sure you're okay, Chief?
- Listen, I want you to call Flo.
I want to get a search party together, all right? All the volunteers she can muster.
Bring flashlights, too.
Hey, you think we got a problem here? We should be out there right now.
We should be helping look for him.
We've been over this, Mike.
The chief says - I don't care what the chief said.
- Michael! We have to do something.
Will can be in danger.
More reason to stay put.
- Mom!
- End of discussion.
So me and Barbara are gonna study at her house tonight.
- That's cool, right?
- No, not cool.
What? Why not?
Why do you think? Am I speaking Chinese in this house? Until we know Will is okay, no one leaves.
- This is such bullshit.
- Language.
So we're under house arrest? Just because Mike's friend got lost on the way home from
- Wait, this is Will's fault?
- Nancy, take that back.
- No!
- You're just pissed off 'cause you wanna hang out with Steve.
- Steve?
- Who is Steve?
- Her new boyfriend.
- You are such a douchebag, Mike!
Language! Nancy, come back.
Come back! It's okay.
It's okay, Holly.
Here, have some juice, okay? You see, Michael?
- You see what happens?
- What happens when what?
I'm the only one acting normal here! I'm the only one that cares about Will!
That is really unfair, son.
We care.
- Mike! - Let him go.
I hope you're enjoying your chicken, Ted.
What did I do? Hey! What'd I do?
- Will!
- Will Byers! Will! Will, we're here for you, bud! He's a good student.
- What?
- Will.
He's a good student.
Great one, actually.
I don't think we've met.
Scott Clarke.
Teacher, Hawkins Middle.
Earth and biology.
I always had a distaste for science.
Well, maybe you had a bad teacher.
Yeah, Ms.
Ratliff was a piece of work.
Ratliff? You bet.
She's still kicking around, believe it or not.
Oh, I believe it.
Mummies never die, so they tell me.
Sarah, my daughter Galaxies, the universe, whatnot She always understood all that stuff.
I always figured there was enough going on down here, I never needed to look elsewhere.
Your daughter, what grade is she? - Maybe I'll get her in my class.
- No, she, uh She lives with her mom in the city.
Thanks for coming out, Teach.
We really appreciate it.
- She died a few years back.
- Sorry? His kid.
Lucas, do you copy? It's Mike.
- Lucas? - Hey, it's Lucas.
I know it's you.
And say "over" when you're done talking so I know when you're done.
Over.
I'm done.
Over.
I'm worried about Will.
Over.
Yeah.
This is crazy.
Over.
I was thinking Will could've cast Protection last night, but he didn't.
He cast Fireball.
- Over.
- What's your point? Over.
My point is he could've played it safe, but he didn't.
He put himself in danger to help the party.
Over.
Meet me in ten.
Over and out.
What are you doing here? I told you on the phone, I'm under house arrest now.
- I figured we'd just study here.
- No.
No way.
Oh, come on.
I can't have you failing this test.
So just bear with me.
What'd I tell you? Ninja.
You like that ice cream, huh? Smile looks good on you.
You know, smile? All right.
You just sit tight.
Whoever it is, I'll tell 'em to go away real quick, all right? Yeah, yeah, yeah - Hey, can I help you?
- Hi, you must be Benny Hammond.
I'm afraid I am.
I'm afraid we're closed for the evening, too.
- So try back tomorrow morning.
- Connie Frazier.
Social Services.
Ah, Social Services.
My apologies.
I didn't expect you so soon.
- That's a heck of a drive.
- Not too bad this time of night.
Hey, listen, I I haven't told her that you're coming yet.
I didn't want her running off again.
She's a tad skittish.
- Children I work with usually are.
- Right, right.
- So, where is she? - Right.
She's in the kitchen.
- Come on up.
I'll introduce you.
- Thank you.
Sorry again for trying to turn you away there.
- It's fine.
- You know, it's funny.
Your, uh, voice sounds different on the Ah, man.
This is it.
Hey, guys.
You feel that? - I think maybe we should go back.
- No.
We're not going back.
Just stay close.
Come on.
Just stay on channel six.
Don't do anything stupid.
Hey, guys, wait up.
Wait up! "Which polymers occur naturally?" Starch and cellulose.
Mmm.
"In a molecule of CH4, the hydrogen atoms are spatially oriented - towards the centers of" - Tetrahedrons.
Wow.
Jesus, how many of these did you make? You said you wanted to help.
How about this? How about How about every time that you get something right, I have to take off an item of clothing.
But every time that you get something wrong - Uh, pass.
- Oh, come on.
- Come on.
- No.
- Come on.
It'll be fun.
- No.
"During fractional distillation, hydrocarbons are separated according to their" Melting point.
Ooh, it's boiling points.
That's what I meant.
Yeah, that's not what you said.
- No.
- No? Oh, do you need Do you need help, or No.
Steve.
Steve, come on.
What? Are you crazy? My parents are here.
That's weird, I don't see them.
Was this your plan all along? To to get in my room and then - get another notch on your belt.
- No.
Nancy, no.
I'm not Laurie, or Amy, or Becky.
You mean, you're not a slut.
That's not what I'm saying.
You know, you're so cute when you lie.
Shut up.
Bad Steve.
Bad.
Don't do that to Miss Nancy.
NANCY: You're an idiot, Steve Harrington.
STEVE: You are beautiful, Nancy Wheeler.
"Compared to the rate of inorganic reactions, the rate of organic reactions is generally" Jonathan, wow.
You took these? These are great.
Wow, they really are.
- I know I haven't been there for you.
- I've been working so hard and I I just feel bad.
I don't even barely know what's going on with you.
All right? I am sorry about that.
Hey, what is it? What is it, honey? - Nothing.
- Tell me.
Tell me.
- Come on.
You can - No.
It's just I should've been there for him.
No.
Oh, no.
You can't do that to yourself.
This was not your fault.
Do you hear me? - He is close.
- I know it.
I I feel it in my heart.
You just have to You have to trust me on this, okay?
- Yeah.
- Oh, look at this.
Look at this one.
Look at this one.
- I mean, that's it, right? - Yeah.
That's it.
That's the one.
Hello? Hello? Lonnie?
- Dad?
- Hopper? Who is this? Will?
- Will?
- It's Will? Mom, it's Will? Who is this?
- What have you done to my boy?
- What? What? Give me back my son! Hello? Hello, who is this? Hello? Who is this? Mom, who was it? Who was it, Mom? - It was him.
- Look at me, Mom.
Was it Will? - Yes.
- What did he say? He just breathed.
He just breathed.
- And was someone else there?
- I
- Mom, who was there? Who was it?
- It was him.I know it was his breathing.
I know it was his breathing.
Will!
- Will!
- Byers! I've got your X-Men 134!
DUSTIN: Guys, I really think we should turn back.
LUCAS: Seriously, Dustin? You wanna be a baby, then go home already!
DUSTIN: I'm just being realistic, Lucas!
LUCAS: No, you're just being a big sissy!
DUSTIN: Did you ever think Will went missing because he ran into something bad? And we're going to the exact same spot where he was last seen? - And we have no weapons or anything?
MIKE: Dustin, shut up.
DUSTIN: I'm just saying, does that seem smart to you?
MIKE: Shut up. Shut up. Did you guys hear that?