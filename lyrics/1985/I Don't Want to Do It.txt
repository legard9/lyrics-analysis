CHAPTER ONE
<<<<<<< HEAD

‘There’s two people waiting outside, Daddy. Can we open the door now?’ asked young Nancy Walker.

Urging his eleven-year-old daughter to come away from the window, Donald smiled as Nancy skipped towards him. Nancy was like a miniature version of her mother: petite, blonde, with blue eyes and a cute button nose.

‘How’re we doing for time, Donald?’ Mary shouted out.

Holding his daughter’s hand, Donald led her into the kitchen. ‘We have twenty minutes until our business officially opens, my dear,’ he said, proudly. He had worked two jobs for many years to secure his and Mary’s aim of a better life for themselves and their family. He had even worked at weekends while Mary brought the children up nigh-on single-handedly, but it had been worth it now they had achieved their dream.

‘Look, Dad. I buttered all that,’ Christopher said, pointing towards a stack of bread.

Donald ruffled the hair of his eight-year-old son. Christopher looked nothing like his mother and sister. He took after his dad with his brown hair and his chocolate-coloured eyes.

‘Can you put that cake in the display cabinet for me, Donald? Oh, and turn the jukebox on as well,’ Mary ordered.

Donald raised his eyebrows to the ceiling at the mention of the jukebox. He had been totally against purchasing such an object. He had finally relented when Mary explained her exact reasons for wanting one. ‘I don’t want ours to be like some grotty old transport café, Donald. I want it to be vibrant and modern. If we buy the jukebox outright, just think of the extra income we will earn with people putting all their pennies in. We don’t want a café full of old-age pensioners, do we? We want to attract a younger crowd that have money to spend, and music is the best attraction of all. That new band, the Rolling Stones, would liven up a graveyard,’ Mary insisted.

Donald sat down on one of the posh plastic shiny red chairs that his wife had fallen in love with. She had an eye for décor, did his Mary, and Donald had to admit she had done a bloody good job. Red and white had been her colour theme and apart from the picture of James Dean that sat proudly on the wall opposite the jukebox, everything was a mixture of those two colours. Thinking how trendy and also how very American it all looked, Donald smiled, stood up, and walked into the kitchen. ‘I can’t wait any longer. Let’s open the door now, shall we?’

‘Can I open it?’ Christopher shouted, grabbing his father’s arm.

‘No, I want to do it,’ Nancy said obstinately, pushing her brother out of the way.

‘Behave yourselves, please. Seeing as your mother designed this and buying a café was all her idea in the first place, it will be her that opens the door to the public.’

Eyes shining with excitement, Mary picked up the scissors. Donald had put a piece of red ribbon across the outside of the door this morning and once that was cut, their wonderful café was open for the whole wide world to see. ‘To happiness and success,’ Mary said.

Queenie Butler stared at her mother’s grave and crouched down next to her sister. ‘We’ve tidied you up, Mum, and we’re off now. Love you. God bless,’ Queenie said, kissing her fingers and placing her right hand against her mother’s headstone.

‘Yep. God bless, sweetheart,’ Vivian added, solemnly.

‘Don’t our flowers look beautiful?’ Queenie commented, linking arms with her sister.

Vivian nodded. ‘Best-looking grave over here by miles. At least we have respect for the dead, unlike some people,’ she said loudly, as Old Mother Taylor walked past.

‘Stop it, Viv,’ Queenie laughed.

‘Well, her old man’s grave is an eyesore. How the hell can she visit him regular and stare at those weeds? It ain’t bleedin’ normal. Lazy old cow,’ Vivian said, loudly.

‘Whatever is your Lenny doing?’ Queenie enquired.

Marching over to her nine-year-old son, Vivian clipped him around the ear. ‘What have I told you about pissing over ’ere, eh? If you wanna do a wee-wee, you ask me and I’ll take you to a toilet. You don’t get your dingle-dangle out in public, understand? It’s naughty.’

‘Sorry, Mummy,’ Lenny said, grinning.

As her nephew skipped on ahead of them, Queenie chuckled. ‘I’m sure he only does it to wind you up, Viv. He laughs every time you have a go at him. Sod all wrong with his brain. Smart as a button, he is.’

Vivian batted her eyelids. Lenny was her only son, she adored him, but it wasn’t easy bringing up a child with disabilities. Lenny had nearly died when she’d given birth to him. She had gone into labour at home and when the doctor finally arrived, he hadn’t been able to get her son out at first. Lenny had been in the breech position and it seemed like an eternity before he finally entered the world. Queenie had been with her throughout, holding her hand while she screamed blue murder and both of them had thought little Lenny was a goner. He lay motionless on the bed for a good few minutes before the doctor managed to find signs of life. The relief she felt when she heard that first cry come from his lips, Vivian would remember till her dying day.

Lenny’s dad was an East End Jack-the-lad called Bill Harris. Bill was working his way up the criminal ladder and had felt humiliated being associated with a son who wasn’t born perfect. It was common knowledge locally that Bill was knocking off the tarty barmaid in the Blind Beggar and when Vivian finally learned of his betrayal, she had packed his clothes in a couple of sacks, marched inside the pub with Queenie alongside her, chucked them at the barmaid, and told her she was welcome to her no-good husband. That was over three years ago now, and Vivian had never clapped eyes on Bill Harris since. Rumour had it that he’d moved to Barking and set up home with his new tart. Viv hoped this wasn’t true, as she would much prefer the bastard to be six feet under and covered by earth.

‘Your Lenny looks more and more like my Michael every day. Wouldn’t Mum have loved him now?’ Queenie said, wistfully.

Vivian nodded. Their poor old mum had died last year and had adored young Lenny. She’d had a stroke and was found dead in her armchair with her knitting on her lap. She was only fifty-seven, no age at all.

‘Shall we pop in and see the boys in the club on the way back?’ Queenie asked. She was dead proud of her two eldest sons for recently setting up their own business. It was officially a snooker club, but it was common knowledge locally that it was really an illegal drinking and gambling den.

Queenie wasn’t daft. She knew Vinny and Roy had pulled off a robbery or two to afford such an establishment, but it didn’t bother her. She wanted her sons to live the good life and if that meant swindling the odd person or company along the way, then so be it.

‘Yeah, why not. They might find Lenny some jobs to do this afternoon which will give me a break. Old Jack’s café re-opens today, you know. Fat Beryl saw one of them jukeboxes being delivered there, so it sounds posh. Let’s be nosy and have a cuppa there first, then we’ll pop in to see the boys.’

‘Oh, I dunno, Viv. Say all Old Jack’s customers are back in there? He was ever so popular, was Jack. My Vinny swears blind he had nothing to do with young Peter’s murder, but I don’t fancy walking into a furnace. Perhaps we should pop in there when it’s been open a couple of weeks? Bound to be packed today.’

‘People are always gonna spread bloody rumours. Your Vinny’s got good morals, you know that. Peter was a pervert, that’s a fact, and if it was Vinny that topped him for touching up that poor child, then he deserves a bleedin’ medal. Let’s walk in there with our heads held high. You are Queenie Butler, no bastard would have the guts to say anything bad to you, would they? Or me, for that matter, and if somebody did get a bit lippy, we’ll just walk round the corner and tell the boys.’

Knowing how feared her two eldest sons were, Queenie couldn’t help but chuckle. ‘Sod it then. Let’s go and be nosy.’

Donald and Mary had been rushed off their feet all morning. Donald was in charge of the cooking in the kitchen and Mary was out at the front taking customers’ orders and making teas, coffees, sandwiches and rolls. Even young Nancy and Christopher had worked flat-out. They were in charge of the washing and drying up, with the promise of some extra pocket money.

At the first lull in the day’s activities, Donald left the kitchen to join his wife at the counter.

‘The interest that jukebox has caused, you would not believe. Everybody who has come in has had a look at it. Told you it would be good for business, didn’t I?’ Mary said, treating Donald to a smug expression. Her husband could be a domineering man at times, but Mary had learned to stick up for herself over the years. Nancy always sided with her, and Christopher with his father, but overall they were a happy, well-balanced family.

‘You most certainly did, my dear. I can’t believe how busy we have been, can you? Perhaps it’s a bit of a novelty as it’s our first day, but if things continue in the same vein, we will have to look at taking on a member of staff, Mary. The kids start their new school next week, remember? And there is no way I would have had time to wash and dry up this morning as well as cook all the food.’

‘Let’s see how things pan out. Perhaps we can take someone on part-time to help us out in our busiest period?’ Mary replied. She and Donald had planned to open from seven in the morning to seven in the evening to begin with.

Donald didn’t answer. He was too busy watching the reaction of his seated customers to the two bleached-blonde women and dark-haired child who had just walked in. They were being treated like royalty. Because there were no spare tables, at least three different people had leapt up to give them theirs and one man had even offered to buy them their food and drink.

‘This has to be members of that Butler family that Mad Freda warned us about,’ Donald hissed in his wife’s ear.

Mary smiled broadly as the young boy ran over to the jukebox and the two women approached the counter. ‘Hello, I’m Mary Walker and this is my husband, Donald. We are the new owners of this establishment,’ Mary said, for about the fiftieth time that day.

‘Music, Mummy. I wanna dance,’ young Lenny shouted at the top of his voice.

‘’Ere you go, boy,’ an old man in a flat cap said, handing Lenny some coins.

Queenie held out her right hand. ‘Pleased to meet you. I’m Queenie Butler and this is my sister, Vivian. My sons own the snooker club just around the corner and that young man over there is Lenny, Vivvy’s boy.’

The volume on the jukebox wasn’t overly loud, but as Lenny started singing and bopping away to the sound of Buddy Holly’s ‘That’ll be the Day’, most of the people in the café fawned over him, then clapped in unison.

Mary joined in with the applause.

‘Rock and roll mad, he is. Dunno where he gets it from. Nobody else in the bleedin’ family likes it,’ Vivian informed Mary.

‘Right, we’ll have two cups of rosy and I’ll have one of them scones with thick butter on it,’ Queenie said.

‘I’ll have a scone too and a can of pop and an iced bun for my Lenny,’ Vivian added.

Remembering Mad Freda’s warning about the Butlers not paying, Mary was relieved when Queenie handed her a pound note, then thanked her as she gave her her change.

Mary followed Donald out into the kitchen. ‘Well, that Freda was obviously mad. She said the boy was a mongol and even though you can tell he is a bit backward, he certainly isn’t one of those. And even though the women seem a bit rough and ready, they were polite enough and paid for their order.’

Donald kissed his wife on the forehead. ‘I told you everything would be fine.’ There was no way Donald would worry his Mary, but he really hadn’t liked the look of those Butler women. The atmosphere in the café had been normal before they’d walked in and he could tell people were only offering them their tables, fawning over the child, and generally falling over backwards out of some kind of fear. Donald wasn’t stupid. Those Butlers had danger stamped all over them.

Vinny and Roy Butler grinned as they divided up the previous evening’s takings.

‘Blinding! And another good night was had by all,’ Vinny said, as he handed his brother a pile of notes.

Roy chuckled. ‘You sticking the other pile back in the kitty?’

‘Yep. We gotta speculate to accumulate,’ Vinny replied, in a sensible manner.

At twenty, Vinny was two years older than his brother Roy, and between them they were on their way to becoming a force to be reckoned with. A container-load of TVs they had stolen had paid for them to buy the rundown snooker club, and even though it had taken six months to save enough money to refurbish it to their lavish taste, it now looked very classy.

Unlike a lot of young East End wannabes, Vinny and Roy had gone down the clever route of keeping themselves to themselves. Their father Albie was an arsehole. He was also an alcoholic, and watching him make a complete prick of himself over the years had put the boys off ever frequenting pubs.

Neither Vinny nor Roy was a complete teetotaller. Both lads enjoyed the odd Scotch on the rocks here and there, but they only ever drank in front of friends and family, or on their own premises. In their line of business, both lads always liked to have their wits about them. Being clever was part of their image.

One of the reasons Vinny and Roy had decided to buy the club and turn it into the headquarters of their empire was that they hadn’t wanted to tread on anybody else’s toes. The East End was littered with villains, with the two most frightening families being the Mitchells and the Krays.

The Mitchells were based in Canning Town and were heavily into pub protection. They were a family firm, run by the old man, Harry. He pulled the strings while his three sons, Ronny, Paulie and Eddie, terrorized people into handing over their hard-earned cash.

Then there were the Krays. They were local lads who had made a real name for themselves. They were virtually beyond the reach of the law now. Earlier this year they had escaped conviction for nightclub extortion. They’d even been given an interview on TV after that and hung out with film stars and celebrities.

Vinny didn’t know if being that famous was a good thing or not, but he was determined to be feared, well-respected and rich. As a lad, he had idolized both the Mitchells and the Krays for what they had achieved in life and he was hell-bent on topping their glory. Wanting to be the best was part of Vinny’s nature.

‘Who is it?’ Roy shouted, as he heard a knock on the door.

‘It’s the bleedin’ woman who gave birth to the pair of ya,’ Queenie yelled.

Vinny grinned as Roy unlocked the metal door and Lenny ran towards him. ‘All right, Champ? What you been up to?’ Vinny asked, lifting the boy off the ground and swinging him around in the air. Vinny adored his cousin, all the family did.

‘Been Nanny’s grave, then I went dancing in the café,’ Lenny replied, sporting a big grin.

‘Dancing in what poxy café?’

‘Old Jack’s café. It’s re-opened today. New people have taken it over and it’s got one of them jukeboxes in there. I wouldn’t swing him around too much. Three iced cakes the greedy little sod has eaten and he’s bound to be Tom Dick at some point,’ Vivian explained.

Not wanting sick over his brand-new shirt, Vinny sat Lenny on a chair. ‘So, what do you think of the décor, Auntie Viv? You haven’t seen the leather chairs and sofas yet, have you?’

Vivian grinned. She loved her nephews. Unlike a lot of young men these days, Vinny and Roy had impeccable manners. They still referred to her as ‘Auntie’ and probably always would. Viv sat down on one of the burgundy sofas and stroked the quality leather. ‘Oh, it’s beautiful, boys. Looks like a palace now, eh, Queenie?’

Queenie felt as proud as a peacock as she nodded her head in agreement.

Roy stood up. ‘I’ll get you and Auntie Viv a glass of sherry,’ he said, gesturing for Vinny to follow him.

‘What’s up?’ Vinny asked.

‘Why don’t we tell her now? Seems as good a time as any,’ Roy whispered, when his brother joined him behind the bar.

‘Nah. Not in front of Champ,’ Vinny replied.

‘Well, we gotta tell her soon. I hate seeing Dad take the piss out of her like this. He’s such a bastard.’

Vinny nodded in agreement. Breaking the bad news to his mother was not going to be easy, but it had to be done. ‘We’ll find a way to tell her in the next couple of days. And don’t worry about Dad. That treacherous piece of shit will be dealt with, I promise.’

Noticing the dangerous glint in his brother’s eyes that he had seen many times before, Roy felt his stomach knotting. ‘What do you mean by dealt with? I know he’s a prick, Vin, but we can’t do anything bad to him, he’s still our dad.’

=======
‘There’s two people waiting outside, Daddy. Can we open the door now?’ asked young Nancy Walker.
Urging his eleven-year-old daughter to come away from the window, Donald smiled as Nancy skipped towards him. Nancy was like a miniature version of her mother: petite, blonde, with blue eyes and a cute button nose.
‘How’re we doing for time, Donald?’ Mary shouted out.
Holding his daughter’s hand, Donald led her into the kitchen. ‘We have twenty minutes until our business officially opens, my dear,’ he said, proudly. He had worked two jobs for many years to secure his and Mary’s aim of a better life for themselves and their family. He had even worked at weekends while Mary brought the children up nigh-on single-handedly, but it had been worth it now they had achieved their dream.
‘Look, Dad. I buttered all that,’ Christopher said, pointing towards a stack of bread.
Donald ruffled the hair of his eight-year-old son. Christopher looked nothing like his mother and sister. He took after his dad with his brown hair and his chocolate-coloured eyes.
‘Can you put that cake in the display cabinet for me, Donald? Oh, and turn the jukebox on as well,’ Mary ordered.
Donald raised his eyebrows to the ceiling at the mention of the jukebox. He had been totally against purchasing such an object. He had finally relented when Mary explained her exact reasons for wanting one. ‘I don’t want ours to be like some grotty old transport café, Donald. I want it to be vibrant and modern. If we buy the jukebox outright, just think of the extra income we will earn with people putting all their pennies in. We don’t want a café full of old-age pensioners, do we? We want to attract a younger crowd that have money to spend, and music is the best attraction of all. That new band, the Rolling Stones, would liven up a graveyard,’ Mary insisted.
Donald sat down on one of the posh plastic shiny red chairs that his wife had fallen in love with. She had an eye for décor, did his Mary, and Donald had to admit she had done a bloody good job. Red and white had been her colour theme and apart from the picture of James Dean that sat proudly on the wall opposite the jukebox, everything was a mixture of those two colours. Thinking how trendy and also how very American it all looked, Donald smiled, stood up, and walked into the kitchen. ‘I can’t wait any longer. Let’s open the door now, shall we?’
‘Can I open it?’ Christopher shouted, grabbing his father’s arm.
‘No, I want to do it,’ Nancy said obstinately, pushing her brother out of the way.
‘Behave yourselves, please. Seeing as your mother designed this and buying a café was all her idea in the first place, it will be her that opens the door to the public.’
Eyes shining with excitement, Mary picked up the scissors. Donald had put a piece of red ribbon across the outside of the door this morning and once that was cut, their wonderful café was open for the whole wide world to see. ‘To happiness and success,’ Mary said.
Queenie Butler stared at her mother’s grave and crouched down next to her sister. ‘We’ve tidied you up, Mum, and we’re off now. Love you. God bless,’ Queenie said, kissing her fingers and placing her right hand against her mother’s headstone.
‘Yep. God bless, sweetheart,’ Vivian added, solemnly.
‘Don’t our flowers look beautiful?’ Queenie commented, linking arms with her sister.
Vivian nodded. ‘Best-looking grave over here by miles. At least we have respect for the dead, unlike some people,’ she said loudly, as Old Mother Taylor walked past.
‘Stop it, Viv,’ Queenie laughed.
‘Well, her old man’s grave is an eyesore. How the hell can she visit him regular and stare at those weeds? It ain’t bleedin’ normal. Lazy old cow,’ Vivian said, loudly.
‘Whatever is your Lenny doing?’ Queenie enquired.
Marching over to her nine-year-old son, Vivian clipped him around the ear. ‘What have I told you about pissing over ’ere, eh? If you wanna do a wee-wee, you ask me and I’ll take you to a toilet. You don’t get your dingle-dangle out in public, understand? It’s naughty.’
‘Sorry, Mummy,’ Lenny said, grinning.
As her nephew skipped on ahead of them, Queenie chuckled. ‘I’m sure he only does it to wind you up, Viv. He laughs every time you have a go at him. Sod all wrong with his brain. Smart as a button, he is.’
Vivian batted her eyelids. Lenny was her only son, she adored him, but it wasn’t easy bringing up a child with disabilities. Lenny had nearly died when she’d given birth to him. She had gone into labour at home and when the doctor finally arrived, he hadn’t been able to get her son out at first. Lenny had been in the breech position and it seemed like an eternity before he finally entered the world. Queenie had been with her throughout, holding her hand while she screamed blue murder and both of them had thought little Lenny was a goner. He lay motionless on the bed for a good few minutes before the doctor managed to find signs of life. The relief she felt when she heard that first cry come from his lips, Vivian would remember till her dying day.
Lenny’s dad was an East End Jack-the-lad called Bill Harris. Bill was working his way up the criminal ladder and had felt humiliated being associated with a son who wasn’t born perfect. It was common knowledge locally that Bill was knocking off the tarty barmaid in the Blind Beggar and when Vivian finally learned of his betrayal, she had packed his clothes in a couple of sacks, marched inside the pub with Queenie alongside her, chucked them at the barmaid, and told her she was welcome to her no-good husband. That was over three years ago now, and Vivian had never clapped eyes on Bill Harris since. Rumour had it that he’d moved to Barking and set up home with his new tart. Viv hoped this wasn’t true, as she would much prefer the bastard to be six feet under and covered by earth.
‘Your Lenny looks more and more like my Michael every day. Wouldn’t Mum have loved him now?’ Queenie said, wistfully.
Vivian nodded. Their poor old mum had died last year and had adored young Lenny. She’d had a stroke and was found dead in her armchair with her knitting on her lap. She was only fifty-seven, no age at all.
‘Shall we pop in and see the boys in the club on the way back?’ Queenie asked. She was dead proud of her two eldest sons for recently setting up their own business. It was officially a snooker club, but it was common knowledge locally that it was really an illegal drinking and gambling den.
Queenie wasn’t daft. She knew Vinny and Roy had pulled off a robbery or two to afford such an establishment, but it didn’t bother her. She wanted her sons to live the good life and if that meant swindling the odd person or company along the way, then so be it.
‘Yeah, why not. They might find Lenny some jobs to do this afternoon which will give me a break. Old Jack’s café re-opens today, you know. Fat Beryl saw one of them jukeboxes being delivered there, so it sounds posh. Let’s be nosy and have a cuppa there first, then we’ll pop in to see the boys.’
‘Oh, I dunno, Viv. Say all Old Jack’s customers are back in there? He was ever so popular, was Jack. My Vinny swears blind he had nothing to do with young Peter’s murder, but I don’t fancy walking into a furnace. Perhaps we should pop in there when it’s been open a couple of weeks? Bound to be packed today.’
‘People are always gonna spread bloody rumours. Your Vinny’s got good morals, you know that. Peter was a pervert, that’s a fact, and if it was Vinny that topped him for touching up that poor child, then he deserves a bleedin’ medal. Let’s walk in there with our heads held high. You are Queenie Butler, no bastard would have the guts to say anything bad to you, would they? Or me, for that matter, and if somebody did get a bit lippy, we’ll just walk round the corner and tell the boys.’
Knowing how feared her two eldest sons were, Queenie couldn’t help but chuckle. ‘Sod it then. Let’s go and be nosy.’
Donald and Mary had been rushed off their feet all morning. Donald was in charge of the cooking in the kitchen and Mary was out at the front taking customers’ orders and making teas, coffees, sandwiches and rolls. Even young Nancy and Christopher had worked flat-out. They were in charge of the washing and drying up, with the promise of some extra pocket money.
At the first lull in the day’s activities, Donald left the kitchen to join his wife at the counter.
‘The interest that jukebox has caused, you would not believe. Everybody who has come in has had a look at it. Told you it would be good for business, didn’t I?’ Mary said, treating Donald to a smug expression. Her husband could be a domineering man at times, but Mary had learned to stick up for herself over the years. Nancy always sided with her, and Christopher with his father, but overall they were a happy, well-balanced family.
‘You most certainly did, my dear. I can’t believe how busy we have been, can you? Perhaps it’s a bit of a novelty as it’s our first day, but if things continue in the same vein, we will have to look at taking on a member of staff, Mary. The kids start their new school next week, remember? And there is no way I would have had time to wash and dry up this morning as well as cook all the food.’
‘Let’s see how things pan out. Perhaps we can take someone on part-time to help us out in our busiest period?’ Mary replied. She and Donald had planned to open from seven in the morning to seven in the evening to begin with.
Donald didn’t answer. He was too busy watching the reaction of his seated customers to the two bleached-blonde women and dark-haired child who had just walked in. They were being treated like royalty. Because there were no spare tables, at least three different people had leapt up to give them theirs and one man had even offered to buy them their food and drink.
‘This has to be members of that Butler family that Mad Freda warned us about,’ Donald hissed in his wife’s ear.
Mary smiled broadly as the young boy ran over to the jukebox and the two women approached the counter. ‘Hello, I’m Mary Walker and this is my husband, Donald. We are the new owners of this establishment,’ Mary said, for about the fiftieth time that day.
‘Music, Mummy. I wanna dance,’ young Lenny shouted at the top of his voice.
‘’Ere you go, boy,’ an old man in a flat cap said, handing Lenny some coins.
Queenie held out her right hand. ‘Pleased to meet you. I’m Queenie Butler and this is my sister, Vivian. My sons own the snooker club just around the corner and that young man over there is Lenny, Vivvy’s boy.’
The volume on the jukebox wasn’t overly loud, but as Lenny started singing and bopping away to the sound of Buddy Holly’s ‘That’ll be the Day’, most of the people in the café fawned over him, then clapped in unison.
Mary joined in with the applause.
‘Rock and roll mad, he is. Dunno where he gets it from. Nobody else in the bleedin’ family likes it,’ Vivian informed Mary.
‘Right, we’ll have two cups of rosy and I’ll have one of them scones with thick butter on it,’ Queenie said.
‘I’ll have a scone too and a can of pop and an iced bun for my Lenny,’ Vivian added.
Remembering Mad Freda’s warning about the Butlers not paying, Mary was relieved when Queenie handed her a pound note, then thanked her as she gave her her change.
Mary followed Donald out into the kitchen. ‘Well, that Freda was obviously mad. She said the boy was a mongol and even though you can tell he is a bit backward, he certainly isn’t one of those. And even though the women seem a bit rough and ready, they were polite enough and paid for their order.’
Donald kissed his wife on the forehead. ‘I told you everything would be fine.’ There was no way Donald would worry his Mary, but he really hadn’t liked the look of those Butler women. The atmosphere in the café had been normal before they’d walked in and he could tell people were only offering them their tables, fawning over the child, and generally falling over backwards out of some kind of fear. Donald wasn’t stupid. Those Butlers had danger stamped all over them.
Vinny and Roy Butler grinned as they divided up the previous evening’s takings.
‘Blinding! And another good night was had by all,’ Vinny said, as he handed his brother a pile of notes.
Roy chuckled. ‘You sticking the other pile back in the kitty?’
‘Yep. We gotta speculate to accumulate,’ Vinny replied, in a sensible manner.
At twenty, Vinny was two years older than his brother Roy, and between them they were on their way to becoming a force to be reckoned with. A container-load of TVs they had stolen had paid for them to buy the rundown snooker club, and even though it had taken six months to save enough money to refurbish it to their lavish taste, it now looked very classy.
Unlike a lot of young East End wannabes, Vinny and Roy had gone down the clever route of keeping themselves to themselves. Their father Albie was an arsehole. He was also an alcoholic, and watching him make a complete prick of himself over the years had put the boys off ever frequenting pubs.
Neither Vinny nor Roy was a complete teetotaller. Both lads enjoyed the odd Scotch on the rocks here and there, but they only ever drank in front of friends and family, or on their own premises. In their line of business, both lads always liked to have their wits about them. Being clever was part of their image.
One of the reasons Vinny and Roy had decided to buy the club and turn it into the headquarters of their empire was that they hadn’t wanted to tread on anybody else’s toes. The East End was littered with villains, with the two most frightening families being the Mitchells and the Krays.
The Mitchells were based in Canning Town and were heavily into pub protection. They were a family firm, run by the old man, Harry. He pulled the strings while his three sons, Ronny, Paulie and Eddie, terrorized people into handing over their hard-earned cash.
Then there were the Krays. They were local lads who had made a real name for themselves. They were virtually beyond the reach of the law now. Earlier this year they had escaped conviction for nightclub extortion. They’d even been given an interview on TV after that and hung out with film stars and celebrities.
Vinny didn’t know if being that famous was a good thing or not, but he was determined to be feared, well-respected and rich. As a lad, he had idolized both the Mitchells and the Krays for what they had achieved in life and he was hell-bent on topping their glory. Wanting to be the best was part of Vinny’s nature.
‘Who is it?’ Roy shouted, as he heard a knock on the door.
‘It’s the bleedin’ woman who gave birth to the pair of ya,’ Queenie yelled.
Vinny grinned as Roy unlocked the metal door and Lenny ran towards him. ‘All right, Champ? What you been up to?’ Vinny asked, lifting the boy off the ground and swinging him around in the air. Vinny adored his cousin, all the family did.
‘Been Nanny’s grave, then I went dancing in the café,’ Lenny replied, sporting a big grin.
‘Dancing in what poxy café?’
‘Old Jack’s café. It’s re-opened today. New people have taken it over and it’s got one of them jukeboxes in there. I wouldn’t swing him around too much. Three iced cakes the greedy little sod has eaten and he’s bound to be Tom Dick at some point,’ Vivian explained.
Not wanting sick over his brand-new shirt, Vinny sat Lenny on a chair. ‘So, what do you think of the décor, Auntie Viv? You haven’t seen the leather chairs and sofas yet, have you?’
Vivian grinned. She loved her nephews. Unlike a lot of young men these days, Vinny and Roy had impeccable manners. They still referred to her as ‘Auntie’ and probably always would. Viv sat down on one of the burgundy sofas and stroked the quality leather. ‘Oh, it’s beautiful, boys. Looks like a palace now, eh, Queenie?’
Queenie felt as proud as a peacock as she nodded her head in agreement.
Roy stood up. ‘I’ll get you and Auntie Viv a glass of sherry,’ he said, gesturing for Vinny to follow him.
‘What’s up?’ Vinny asked.
‘Why don’t we tell her now? Seems as good a time as any,’ Roy whispered, when his brother joined him behind the bar.
‘Nah. Not in front of Champ,’ Vinny replied.
‘Well, we gotta tell her soon. I hate seeing Dad take the piss out of her like this. He’s such a bastard.’
Vinny nodded in agreement. Breaking the bad news to his mother was not going to be easy, but it had to be done. ‘We’ll find a way to tell her in the next couple of days. And don’t worry about Dad. That treacherous piece of shit will be dealt with, I promise.’
Noticing the dangerous glint in his brother’s eyes that he had seen many times before, Roy felt his stomach knotting. ‘What do you mean by dealt with? I know he’s a prick, Vin, but we can’t do anything bad to him, he’s still our dad.’
>>>>>>> master
Leaning towards his brother’s ear, Vinny spoke loudly and clearly. ‘I wouldn’t care if he was the King of England. Nobody makes a fool out of our mum and I mean fucking nobody. Our dad will pay for the liberty he has taken. Trust me on that one.’