[EVA]
What's new, Buenos Aires?
I'm new!
I wanna say I'm just a little stuck on you
You'll be on me too
I get out here, Buenos Aires
Stand back!
You oughta know what you're gonna get in me
Just a little touch of star quality
Fill me up with your heat, with your noise
With your dirt, overdo me
Let me dance to your beat, make it loud
Let it hurt running through me
Don't hold back, you are certain to impress
Tell the driver this is where I'm staying
Hello, Buenos Aires!
Get this!
Just look at me, dressed up, somewhere to go
We'll put on a show
Take me in at your flood, give me speed
Give me light, set me humming
Shoot me up with your blood
Wine me up with your nights, watch me coming
All I want is a whole lot of excess
Tell the singer this is where I'm playing
Stand back, Buenos Aires
Because!
You oughta know what you're gonna get in me
Just a little touch of star quality
And if ever I go too far
It's because of the things you are
Beautiful town, I love you
And if I need a moment's rest
Give your lover the very best
Real eiderdown and silence

[CHÉ [spoken]]
On the 9th February 1935 in Buenos Aires, a polo match between a team of leading Argentine players and the touring British side. The British ambassador said he had never seen a social occasion quite like it. Even by the standards of Buenos Aires society, the gathering at the polo ground glittered. The Rolls and the Daimlers, the hampers from Harrods, the clothes, the diamonds, the crystal, the wines, the procession of nannies from England and France. The result of the match, oh yes, the home team won! But, as the British ambassador pointed out, that did not reflect badly on British horsemanship; three of the Argentine players were educated at Eton!

[EVA]
You're a tramp, you're a treat
You will shine to the death, you are shoddy
But you're flesh, you are meat
You shall have every breath in my body
Put me down for a lifetime of success
Give me credit, I'll find ways of paying
Rio de la Plata!
Florida, Corrientes, Nueve de Julio
All I want to know
Stand back, Buenos Aires!
Because!
You oughta know what you're gonna get in me
Just a little touch of...
Just a little touch of...
Just a little touch of star quality