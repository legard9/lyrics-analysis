Wait- sorry, sorry, sorry, stop the music, sorry

What happened there? That song is just so emot- I just met that guy! Huh... Alright, I'm just gonna have to compose myself, the show must go on here. Uh.. [Clears throat]

Students, faculty, parents, grandparents, uncles that weren't invited but showed up anyway, handsome young janitors who are secretly math geniuses and the homeless guy from With Honors, my name is Andy Samberg and I am as honored to be here today as I am unqualified.

There’s a storied history of incredible Class Day speakers here at Harvard: Nobel Prize laureate Mother Teresa, former U.S. president Bill Clinton and now me, the fake rap wiener songs guy.

I'm also just over the moon to be receiving an honorary degree here today, I mean never in my wildest dreams did I- what’s that? No degree? So what, I’m just like, just like a professor or… Oh nothing? Well, then why am I here? Dean Hammonds... (yes?) You lied to me! Alright well it’s just this crappy speech then. I flew my folks in! Here we go.

Class of 2012, you are graduating from college that means this is the first day of the last day of your life. No, that’s wrong. This is the last day of the first day of school. Nope that’s worse. This is a day.

I too turned to Webster’s dictionary and it defined Harvard as “the season for gathering crops.” Admittedly that’s actually a definition of ‘harvest’ but it was the closest word I could find to Harvard that wasn’t a proper noun. In the end isn’t that what Harvard is really about though? It’s about planting the seeds of knowledge that eventually produce crops, A.K.A money, in order to satisfy the farmers, your parents, who pay like 45 thousand crops a year to send you to harvest so you could major in women’s agriculture. You see what I’m doing.

Before I move on, the world outside of Harvard has asked me to make a quick announcement. The following majors are apparently useless as of tomorrow: history, literature, all things related to art, social studies, East Asian studies, pretty much anything that ends with studies, romance languages, and finally, folklore and mythology. C’mon guys. Just study something useful and play World of Warcraft in your free time ok?

Anyhow all those majors are now useless unless you can somehow turn them into an iPhone app. Math and science majors you guys are cool, finally.

2012 is a great time to be graduating from college. Sure the job market is a little slow. Sure our health care and social security systems are going to evaporate in five years. Sure you will have to work until you’re 80 to support your 110 year old parents who live forever because of nanotechnology. Sure the concept of love will soon disappear leaving us all lonely robots ready to kill our best friend for a lukewarm cup microchip soup, but that doesn’t matter because tomorrow you graduate from harvest- from Harvard, from Harvard is where you will graduate.

I’m sure a lot of you’re looking up here and thinking “What makes this guy so special? What has he accomplished? He didn’t even go to Harvard.” To you I say this; I didn’t even apply to Harvard because I knew I wouldn’t get in. Boom! Suck on that. I don’t accept you, esteemed college. I break up first. I move on and you see me with my hot new girlfriend. She’s riding shotgun in my convertible Sebring - the one that Harvard was always begging me to rent to drive up the coast. I’m just laughing and looking really fit like, “Have you been going to the gym?” “Nope, just eating right and making positive choices.”

Man, I really wanted that honorary degree! Well I guess the old saying is true: never trust Dean Hammonds.

Regardless, Harvard remains iconic in our culture. One thing that sticks out of my mind is the central role this campus played in one of the most important films ever made about social connections and how we communicate. I’m referring of course to 1986 whimsical blackface romp, Soul Man, starring C. Thomas Howell as a white student posing as an African-American in order to exploit affirmative action. He was at Harvard law in that movie and that movie exists.

Most of you don’t know this yet, but Harvard is one of the few schools you can attend that can also eventually become your workplace nickname. “Whose edamame is this in the break room? Probably Harvard’s. Whose Vespa is in my parking spot? I’m going with Harvard’s.” In fact once a graduate you can never wear your Harvard sweatshirt in public again without looking like a world-class a-hole. I honestly think the Coop should sell University of Michigan t-shirts that you can wear just to blend in once you’re out of here. And to clarify when I say the Coop I mean your campus bookstore, and not famous film actor Bradley Cooper, whom I also refer to as the Coop, and who also sells books and sweatshirts in his free time.

Speaking of fame, Harvard has many famous alumni: Mark Zuckerburg, Bill Gates, just a few ex-students that started successful businesses after dropping out, which means if you’re here in this crowd today and graduating you’re destined to be a massive failure.

Sorry those are just the facts. Also a fact, Class Day is a terrible name for a day when you don’t have to go to class, like ever again. It’s pretty much like calling New Year’s Eve ‘Sobriety Night.’ “Hey, you going out for Sobriety Night?” “Yeah it’s gonna suck”.

And now on a more literary note, I’d like to read a poem by the great W.B. Yeats, which is actually pronounced "Yeets". Lot of people don’t know that, thanks for the heads up Barney Frank. Anyways, this is a truly beautiful and poignant passage from the 1929 collection ‘The Winding Stair and other Poems’ and I think it’s especially applicable to today’s ceremonies. It goes like this:

This is how we do it
This is how we do it
It’s Friday night and I feel alright
Hit the shore because I’m faded
Honeys in the streets say money, yeah we made it

There’s more but you get it, classic "Yeets", an important poet.

While I am really excited to be here today, I’ll be honest. At 33 years of age I haven’t endured or lived that much more than you guys so in order to give you a broader scope of what’s to come, I reached out and asked for some words of wisdom from some people that I thought were relevant to your experience here.

The aforementioned Mark Zuckerberg, who was a Harvard student, was kind enough to send me some remarks that I will relay to you now.

[Imitating Zuckerberg]
Uh, hey guys, it’s me Mark, or as my friend [?] calls me, Zuckleberry Finn. He thought of that! I just wanted to give a quick ‘congrats’ to you all but really more of a ‘congrats’ to me. You know since I left things have gone so good you guys. Like a six-year-old’s fantasy of the future good. In fact I recently completed the Harvard trifecta. Start your own company, have a movie be made about you and marry an Asian doctor. Trifecta! So everyone out there be sure to upgrade to timeline and lay off the Pinocchio’s pizza. Haha, I went to Harvard!

That’s what he had to say. I also asked, for the local experience, Massachusetts native Mark Wahlberg to send over some thoughts for you guys. Here’s what he had to say.

[Imitating Wahlberg]
Hey Harvard, how’s it going? So you guys are graduating huh? I think that’s great. Hey we should do a film together. What do you think? You guys are super smart right? I used a prosthetic penis at boogie nights. Just think about it. Say hi to your mother for me, okay?

He asked me to say that to you guys. Then finally I asked blockbuster superstar Nick Cage for some remarks. Now, I realize he didn’t go to Harvard and he’s not from Boston but he has a special connection to the place that I’ll let him explain. Here’s what he wrote.

[Imitating Nick Cage]
Good afternoon. As I write to you I’m currently digging a tunnel into the bowels of the Widener Library. When I finally breach its mighty walls I will steal the legendary Gutenberg Bible and return it to its rightful owner, Steve Gutenberg. You know I’ve seen some weird stuff in my day. In Istanbul I saw a small child swallow a pelican whole. In the Sahara desert I saw a herd of oxen fly into a portal and disappear from our world forever. But no matter what I’ve seen there’s been one thing I’ve held to be true. Love is the most powerful force this universe has to offer and we should show kindness to all around us with the exception of Dean Hammonds, who is a FILTHY LIAR! And that, my friends, is the true meaning of Hanukkah. I’d love to keep writing but now the time is come for me to ride on to my next adventure. ”What’s that?” you ask. Simple. I’m going to have sex with the statue of John Harvard.

And those are my three impressions. Thank you, you guys. Late night television led me straight here. Now we’ve been paying a lot of attention to the students here today but I want to take a moment I mean acknowledge all the parents. In particular I want to give a shout out to all the moms in the house. Give it up. Absolutely. Absolutely. Our moms put up with so much I mean they ask for so little and as I look out at all the beautiful mothers here today I can’t help but be filled with an overwhelming sense of horniness. Oh yes. You’re a fine crop indeed. And I likes me some older ladies, they know how to do stuff, if you know what I mean. To all the moms, open invitation, nobody gots to know about it.

Before all the dads get upset, and no disrespect, really. You’ve got to be something special if you’ve got such fine ladies on your arms. In fact, as I look at all these strong loyal men I can’t help but be filled with an overwhelming sense of horniness. Oh yes. I see a lot of silver foxes out there today, and Harvard isn’t cheap. Where are my sugar daddies at? Yeah, I see you, you don’t have to raise your hand. Open invitation gentlemen, nobody gots to know.

Now I’d like to get a little serious. As you move forward in the world there will be obstacles but every challenge is a chance for success. You know what, I’m sorry; I had a whole inspirational section to this prepared but now it feels so phony. So I’m going to scrap these scripted words and just speak to you guys from the heart.

The things I’m about to say to you aren’t to make any friends. They’re not for some cheap applause. It’s real talk and it comes from my soul, so listen up. Yale sucks balls! Am I right? Cheer if I’m right! They’re the worst. Yale asked me to do their Class Day speech, but I couldn’t make it to the stage because I kept slipping in all their drool. It’s like a second-tier safety school in the worst city in America. I’m kidding, New Haven is nicer now…than Rwanda.

A little known fact about Yale, it was built on top of an ancient Native American toilet. Really it’s no wonder they’re called the bulldogs, they’re a bunch of big headed inbreds with breathing problems. That comes with my apologies to any inbreds here today. Don’t let anyone compare you to a Yalie. This all might sound harsh, but in truth Yale is basically a sewer filled with mold people, only replace the word people with stinky dried up dog turds that hate laughter and puppies. And that’s my heart stuff you guys, from my soul.

For some of you it might have been hard to hear but I felt it was my duty to give it to you straight. Also, quick confession, I know literally nothing about Yale. But I will say this, Dartmouth can burn in hell!
Ah, Class Day. You know it’s hard to know where life will take you from here; what adventures you’ll have, which sitcoms you’ll write for, but my advice to you is simple. Relax dude! You just finished college at Harvard. You worked so hard. Trust me; you’re going to kill it. I went to Santa Cruz and then I transferred to film school and I’m rich and I don’t mean spiritually rich or any hippie crap like that. I’m talking about racks on racks. Believe it. I’m being a little hyperbolic to seem cool but I am up against Mother Teresa on this thing OK? Have you guys Youtubed her Class Day speech? She was like crumping and throwing bags of money into the crowd. I’m going to take some liberties.

But in the days ahead a lot of people will tell you to trust your instincts and don’t be afraid to take chances, and I am definitely one of those people. But I would also say this: don’t rush into the next phase of your life whether it’s grad school at Harvard or grad school at MIT or massively disappointing your parents by exploring your art made out of garbage thing. Whatever it is you try, make sure it’s what you really want to do because the only person who knows what that is, is you. If all else fails just remember these beautiful words from the film Dead Poet’s Society:

“NEIL! MY NEIL IS DEAD! MY BOY!”

Which now that I’ve said out loud did not quite drive home my point as much as I had hoped. In fact I’m realizing that only like seven percent of what I’ve said today has been at all helpful or even passable as English, but in the end I feel I’m only truly qualified to give you three simple tips on how to succeed in life.
One, cut a hole in a box. Two, put your junk in said box. Three, make her or him open the box. And that’s the way you do it.
Also I can’t believe I’m about to say this, but Dean Hammonds, I forgive you. Bygones be bygones. I’ve already got that sweet degree from Santa Cruz and film school anyway.
So thank you graduates, Godspeed, and congratulations! Play the Yeets!