It was night, and Ulysses saw himself rising above the lake waters brighter than the full moon seen from Old Earth. A rippling path of un-moonlight reached toward the horizon, as if a road to the stars were offered him. Because he (Ulysses the planet) was covered with ocean, he had a brighter albedo than Old Luna, as well as being larger and closer. The light from Eta Carina (subdued to merely solar vehemence by an intervening parasol, whose location he deduced by occlusion of stars and streamers of bright nebulae) formed a pinpoint of reflection on his oceans, dazzling bright, and painted his visible crescent silver. Between the horns of the moon could be seen with the unaided eye (or, at least, with such unaided eyes as Ulysses the man possessed), lights from the floating cities of the Ulyssian tenants.

Because he (Ulysses the relay) was below the Earth’s horizon at the moment, he (Ulysses the man) was momentarily out of communication with himself (Ulysses the space station).

So he seated himself (let us assume with a slight, purse-lipped smile) in a grove not far from the shore, perhaps on a tree stump or perhaps on a stone, and let us imagine that Penelope has placed a picturesque ruin, perhaps a circular colonnade, nearby, with marble Doric pillars rising ghostly in the un-moonlight, and their connecting architraves ornamented with a frieze of nymphs fleeing satyrs, a frozen footrace endlessly circling the grassy space embraced by the pillars. Here and there were tall, slim poplars, sacred to Heracles, or pharmaceutical trees whose bioengineered balms cured numberless diseases in a form of mankind long-extinct, but which were still kept for the fragrance of their leaves, or for the sentiment of things past.

His skin thickened on his soles when he walked, or his buttocks when he sat, or perhaps he had a long, loose mantle of thinking-cloth to act as cushion to his rustic throne, or to wrap his muscled limbs against the cold, rather than simply increasing his skin-heat levels. He could adjust his eyes to the night vision of a fox, of course, but let us assume he is in an old-fashioned mood, and merely has his thinking-cloth emit a cloud of floating sparks, a type of controlled St. Elmo’s Fire, tiny as lightning bugs, and balanced by magnetic monopoles so as to form no danger to him (the man). Or perhaps he is feeling even more old-fashioned and dashes the little sparks against some kindling he has gathered, and now there is a cheery campfire near his large, jet-black feet.

He sang in Portuguese, of course. There were other languages, to be sure, but in Portuguese coracao (heart) rhymes with violao (guitar) and can-cao (song), making it particularly easy to versify about singing on a guitar to win a lady’s heart (which was indeed what he ended up doing, as was perhaps his plan from the first). He wanted to use a long-dead language, considering the surroundings. (He had not selected English, lest he be reduced to singing about the stars above shining on the dove perching on the glove he was dreaming-of, ending the lyric on a preposition. He could not tell from the historical records whether this was a strict rule for this language, or something he did not need to put up with.)

Melody haunted the moonlit glade. Sparks flew up from his campfire or from his electrostatic aura, as the case may be. A few deer stepped silently from the forest, long ears twitching. In theory, the individual deer did not comprehend the meaning of music, but as five or six of them came within earshot, the logic chips scattered through their nervous systems were within intercommunication range. His eyes were dialed down, not night-adapted, and so the rustle in the nocturnal woods might have been another doe or two.

The gathering of deer, kingliest stags and fawns most shy, song-enchanted, all knelt couchant near a guitar-strumming man, was an event sufficiently odd to trigger responses from local-area ecological subsystems, what we may call the unconscious mind, the midbrain, of Penelope.

At this point, imagine her like a maiden reclined on a couch in a bower, who hears a distant measure but does not wake; and yet the song was found in her dreams, threading its way like elusive smoke upward through nocturnal thoughts.

The system sent a blinking owl or two to investigate, and when the simple on-board thought-codes in the owls could not resolve the puzzle, diurnal birds woke up unexpectedly, to flock to the area and land on nearby branches, finches yellow as gold, cardinals red as blood.

(Imagine that the maiden in the bower opens lavender eyes, and sees the woven emerald leaves forming the roof of her wigwam, and smells the blooms threading through its sides, but still thinks the song is an echo from sleep.)

Ulysses increased the light output from his mantle, so then the fabric was shining bright as day; this comforted the birds, who settled close to hear his guitar, one landing on his knee or shoulder; but his splendor provoked a day-night query cycle from some of the plants not far away, which called a swan (huge in the gloom of the lake, and as serene as a ghost), which in turn stirred up a chorus of frogs, and, finally, these in turn disturbed the thick, colorless, and odorless gel at the bottom of the lake.

Between the swan and the frogs and the gel, a healthy amount of Penelope was now concentrated in the area, enough for self-awareness. The lake ooze surfaced as liquid, like an oil-slick, and sublimated immediately to a fine mist.

He saw the vapor rising like a Brocken Specter from the lake, and knew she was here.

She was in the mist, and in the swan, and the owls and songbirds, and through many eyes, she saw herself, in the form of does and vixens, all nuzzling this strange man. She was the serpent coiled around his ankle, the nightingale on his wrist.

Penelope (in the mist) looked on, each droplet gathering light into microspore receptors. Perhaps she wondered why she (in her pets) was nuzzling this man.

The mist settled and condensed, and soon clear liquid was dripping from the leaves and threading its way, microscopic rivers, down channels in the poplar bark. The warmth of the air was gone, the stillness was clammy: Ulysses interpreted this behavior through a series of conventions called the Green Symphony Aesthetic; as if in his mind’s eye, he saw an olive-skinned dark-haired Mediterranean beauty giving him a cold, unfriendly glance.

And she dripped a cold drop of herself, or two, down his neck, making him wince. One need not employ a foreign aesthetic to understand this gesture.

She addressed him. Of course, Penelope probably sent a radio signal from some nearby poplar tree or local node of her information system, but we are allowed a certain poetic license, so let us say a magpie, or some other Quaternary biotic of Old Earth, whose vocal passages can be used for this purpose, flapped near and landed and addressed him in human speech. One is tempted to say it is a gay-plumed parrot, but they are not normal to the temperate zone, and poetic license can only reach so far.

So, then, a bird that was certainly not a parrot spoke.

Cerebellines are famous for their composure, and the complex inscrutability of their thought, which is said to be able to regard every point of view at once: but let us assume for the sake of drama that Penelope addressed him with arch exasperation or coldly worded coyness. Romances are always sweeter if the girl and the planet do not at first get along. Let us take the opening lines from Ao Aerolith Wolfemind One- Nine’s famous sonnet-cycle on the topic:

“Strange sir, I am neither in any wise proud, nor do I scorn you, nor yet am I too greatly amazed, but right well do I know what manner of man you seemed, in days past, when you went forth without fanfare from our glad company at Canopus. Far ahead of the Diaspora you sailed; and were given, in a casket, such thoughts, and such a soul, as could endure the endless solitude uncaring. Now is this, the clamor to disturb my nestling’s slumber, and beguile the sleeping trees with dreams of day, a sign of such an uncaring heart? No doubt you opened your memory casket, changed your personae, and are now an Eremite, habituated to hear no voice but your own.”

This elegant speech provoked indignation from Ulysses. Had his satellite been above the horizon, perhaps, the wiser and swift brain there, Ulysses Three, would have seen the hidden jest of the comment, but the slower Ulysses Four, the human, reacted in a human fashion: “Madame! A mere hundred years would not try my patience even as deeply as your untruthful words. I have opened no casket, and loaded no isolationist template.”

But then he caught himself, or perhaps (after a three-second delay) his posthuman space-station self, Ulysses Two, found a dogleg signal path to put him in contact with his higher versions. If so, let us imagine (as Alexander Scriabin, Hypothetical Revenant, imagined in his composition for color-clavier “The Blush of Ulysses”), a burst of reddish gold in D major, to symbolize the wry resignation with which Ulysses the Planet recognized that blush of anger as his own, and, with a green shrug in A minor, the planet wrote the corresponding memory-emotion of the man into his own logs and mind.

Ulysses Four continued: “Indeed, it was on an errand of sympathy I came, proud woman, to praise this fairest world of yours; for I mean to make of mine own world in miniature the seas of Earth, or whatever of them your wisdom might counsel can be made again in the oceans of Ulysses: to help the cause of Earth I come, and praise your beauty to all ears fit to hear me. Why should all this be forgotten?”

The leaves rustled as if in the night-breeze, but there was no breeze. As countless information points floating in leaf capillaries warmed to the task of transferring a large volume of information, the slight temperature variant, amplified a million times, produced a dilation of stem and twigs. A single leaf waving is inaudible; a forest of
such leaves waving is an audible sensation, as unmistakable as an ocean roll, or the breathy sigh of a hidden nymph, or the blush on the cheek of a fair-skinned woman.

This made him pause. He (Ulysses Four, the man) was not familiar with the expressions and nonverbal telltales of Cerebelline neuroforms, but he (Ulysses One, the planet) certainly was. Ulysses One would later tell him, or, rather, implant within his memory as if he knew it at the time, what this sigh of leaves might mean.

It was the idea of forgetfulness she feared. This melancholy world was perhaps afraid of losing her self-identity, once the Twenty-first Earth was no longer enough loved by the Chrysopoeian Oecumene to bear the expense of so fragile a museum.

While he could not have deduced that at this point, his later memories back on Ulysses One would have this information backdated and edited into the record. So let us suppose, as the poets do, that Ulysses Four had some unspoken intuition on the point: Earth fears she is going mad. And so his heart was moved with pity.

Ulysses then vowed one of those vows that is at once deadly serious and not serious at all: “If it is within my power, I will preserve the memory of Earth until the Eschaton! All the worlds that men have made, from Demeter to Dyson Alpha, not one is half so fair as what Dame Nature, blind and cruel and lovely, made for us in this blue and imperfect globe. I will adorn my world with Earth-life, and put real cetaceans to sport and play within the waves, the chatter of dolphins and the songs of whales to echo in the deep; and the modern space-dolphinoids will see and know in what shallow places they swim! I promise you a renaissance of Earthly aesthetics, and every man in the Oecumene will grow an arbor, or wear an anadem of blossoms.”

“Why such a vow? How can you bind the Chrysopoeian Oecumene to your will, when you are dressed in sickly oceans of improperly fed algae, ragged as a beggar? Are you some king in disguise who will throw off his robes and shout commands?”

“I speak as I must, even if the least wise part of me so speaks: I love this Earth, as all men who do not forget the past must do.”

She answered and said, “Do you? So you say. Put these airy words to the test. Would you walk to every land upon this globe, that you might come to love the rocks where snowy owls find nests, as well as the close and steaming jungles where insects as bright as jewels, and poisoned asps patterned with sparkling beads, do hive and crawl? The pale red-golds of the desert canyons in the dawn you must embrace, and learn to see the angry beauty of the cactus trees, yet also swim with arctic penguins and long-toothed walruses, and behold the blue and enchanted midnight beneath the aurora borealis crown. You will run with awkward ostriches as well as rearing stallions. Any fool can see the beauty in a tropic fish; if you mean your words, you will, for me, love the scowling hermit crab, the deadly shark, the dun,
lopsided flounder.”

He laughed. “Madame, I will do as I will when I will. Why do you seek to command me to do what my nature inclines? I have stared at whirls of cosmic dust and roaring near-nova stars for far too long. These mad suns and eccentric scalded Jupiters were mine long before the new Oecumene settled here. Your cactus pricks and teeth of sharks will not affright me, and the hermit crab is a wonder compared even to the most complex dancing nebula of space. All unliving things are simplistic systems, after all, items without inner value.” And then, prompted by he knew not what, he said: “Unliving things have no passions, and no memory: mere matter is the amnesia of the universe.”

“Perhaps there is much we should forget,” she mused. “Do you know the spot where you stand?”

“Cannae,” he said. “Not far from here grim Hannibal encircled the unwary legions of the Republic, who drove their shouting centurions to defeat, had they but known it, when the Punic center ranks gave way, or seemed to. Seventy thousand troopers lost their lives before the blood-red sunset. Of wars, few cost more. I would not forget those deeds, abhorrent as they seem to us.”

“Are we not vowed to peace, all of us at Eta Carina?”

“But not vowed to thoughtlessness. Those soldiers were as brave as any quiet martyr who does not raise a hand against his slayers. There was no noumenal mathematics in the time of the Second Mental Structure. They are gone beyond recall; and all their thoughts are silent to us now, unrecoverable, irretrievable, and lost. By honoring the dead, I defy that silence.”

Another bird, perhaps a magpie, with a voice as keen as the piping of a flute, called out: “Tell me! What is it like to know there is a casket you can open, which will at once alter you beyond what you could grow into, and make you anew into a new man? Is it not death, that one thing we have forbidden and left behind? Is it not as cruel as war?”

He put aside the guitar. “Madame, these are strange questions. The casket of loneliness was given me in case I should otherwise go mad, for at that time, I was certain that Eta Carina would be mine alone, and forever.”

The leaves rustled again. He wondered why she was agitated. The birds circled him, first the nightingale, then the night-jars, owls, but also the sleepy finches, blue jays, and cardinals.

The birds sang, “Freely will I aid you, and revive the dying, scum-choked seas of your little lunar world; but freely you must give to me what in  older days, by feminine wile, or glamour, or unknown sympathies of the heart, I would have had to win from you. The science of the mind, in these last days of the Seventh Mental Structure, is all discovered: each trembling and uncertain wisp of unconscious fancy, each fleeting thought, can be numbered and known. And so no mystery can obtain in these matters.”

“Which matters?” he cried out. “We are but strangers to each other! How could I agree to this?” (Scriabin thinks this protest insincere, and symbolizes it with a diminished seventh; whereas Aerolith expresses this outcry in a memorable sonnet in words of honest surprise.)

But the birds had all taken wing, and the deer, leaping startled to their narrow feet, tails white with panic, fled. The croaking frogs fell silent, and the bright-eyed foxes slunk away.

His cloak, Ulysses snuffed, so that instant dark was around him, and now he tuned his eyes to their most sensitive register, and woke special sensory cells planted along his skull, to view the hidden wavelengths. He detected heat in the bottom of the lake, and electronic signals indicated a confluence of nanomachines in rapid-assembly mode.

A moment later, the swan reared back and flapped with snow-white wings to dry the figure being lifted to the surface. Like that of a naiad, her head first crested. Water spilled from raven-hued tresses of hair and ran in little trickles from the delicate, feminine curves, full breasts, flat stomach, rounded hips, and long legs, of the slender form dimly seen by un-moonlight. By the time she raised her hands in a gesture more graceful than any ballet, elbows high, back arched, to wring her long and heavy hair, he was lost in admiration.

Closer he came, as if drawn by a lodestone. Her eyes were steady, mysterious, half-lidded, and little drops of water clung to the lashes, bright as diamonds.

When her lips parted, he saw how red and full they were, how white her teeth. “Call me Penelope,” she said. “And I for you have created a woman of my own substance, and poured all my virtues and authority into her.”

Of course he sang to her. What else could he do?

Their first kiss was not then, but, for the sake of drama, let us pretend that he won it from her with words both wild and solemn, playfully serious, sweet in the way all love is sweet, but bitter with a hidden bitterness.

By the time Ulysses Three cleared the horizon, and reestablished signal flow with his small-brained partial, it was too late. With only the hint of wry resignation in his thoughts, satellite Ulysses Three sent messages and memory downloads (through himself at Ulysses Two) back to himself at the planet Ulysses One, the news that they were to be married.

Many a man discovers he has agreed to something before he knows it.