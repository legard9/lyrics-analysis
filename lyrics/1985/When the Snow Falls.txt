Korean (Original)

[Verse 1]
계절에 흩날려 떨어진 꽃잎은
홀로 남아 외로워 슬프도록

[Verse 2]
별들이 떠나간 하늘은 서글퍼
내리는 빗물처럼 울고만 있네

[Chorus]
꽃들은 피고 또 지듯 보내는 마음
바람에 전해주오

[Verse 3]
첫눈이 내리면 이뤄지는 소원
그 말을 난 믿어요 간절한 소원

[Verse 4]
애달픈 나의 마음을 그대 안다면
다시 꼭 돌아오리
<<<<<<< HEAD
=======


>>>>>>> master
English (Translated)

[Verse 1]
The fallen flowers
Scattered by the season
Remains alone, lonely and sad

[Verse 2]
The sky without the stars is sad
Like the falling rain, it is crying

[Chorus]
Flowers bloom and wither
Just like my heart
Tell me with the wind

[Verse 3]
When the first snow falls
My wish will come true
I believe in that, an earnest wish

[Verse 4]
If you know my longing heart
Please come back to me
<<<<<<< HEAD
Romanized
Gyejeore heunnallyeo tteoreojin kkochipeun
Hollo nama oerowo seulpeudorok
Byeoldeuri tteonagan haneureun seogeulpeo
Naerineun binmulcheoreom ulgoman inne
Kkotdeureun pigo tto jideut bonaeneun maeum
Barame jeonhaejuo
Cheonnuni naerimyeon irwojineun sowon
Geu mareul nan mideoyo ganjeolhan sowon
=======

Romanized
Gyejeore heunnallyeo tteoreojin kkochipeun
Hollo nama oerowo seulpeudorok

Byeoldeuri tteonagan haneureun seogeulpeo
Naerineun binmulcheoreom ulgoman inne

Kkotdeureun pigo tto jideut bonaeneun maeum
Barame jeonhaejuo

Cheonnuni naerimyeon irwojineun sowon
Geu mareul nan mideoyo ganjeolhan sowon

>>>>>>> master
Aedalpeun naui maeumeul geudae andamyeon
Dasi kkok doraori