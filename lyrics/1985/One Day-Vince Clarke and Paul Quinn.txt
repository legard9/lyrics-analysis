[Verse 1: Paul Quinn]
This is my story through an open door
I'd like to trust you but I can't be sure
It's in your eyes, it's in the way you sing
Your lies to me, they don't mean anything

[Chorus: Paul Quinn]
Maybe one day we're gonna find some place
Maybe one day put on a different face

[Verse 2: Paul Quinn]
Watch from a window, see an old man cry
He's all alone and you just walk on by
There's no expression, there's no life here now
She's gone for good, this time he takes his bow

[Chorus: Paul Quinn]
Maybe one day we're gonna find somе place
Maybe one day put on a diffеrent face

[Post-Chorus: Paul Quinn]
Our life is an exodus
God knows what's set for us
Good if we make it some day

[Interlude]

[Chorus: Paul Quinn]
Maybe one day we're gonna find some place
Maybe one day put on a different face

[Verse 3: Paul Quinn]
They crossed the border, found a new land there (New land there)
Ten thousand marching, it's the new Red Square
We pray in silence and our hands between
We hide our faces, keep our hats unseen

[Chorus: Paul Quinn]
Maybe one day we gonna find some place
Maybe one day put on a different face

[Post-Chorus: Paul Quinn]
Our life is an exodus
God knows what's set for us
Good if we make it some day

[Chorus: Paul Quinn]
Maybe one day we gonna find some place (Maybe one day)
Maybe one day put on another face

[Outro: Paul Quinn]
Maybe one day
Maybe one day
Maybe one day
Maybe one day
(So it keeps on foot, huh?)