(Konishi)
Translators: Andrei Cunha

KISS
Kagami no mae de
Chiisaku
Tsubuyaite miru

KISS
Kon'ya no
Kiss no koto
Kangaete
Watashi wa sukoshi
Tokimeku

Koi no hajimari ga
Chikazuiteru no
Iki wo kirashite
Kakeashi de
Yatte kuru

Atarashii kuchibeni
Atarashii koibito ni
Yoku niau iro wa
Nani iro
Pink to purple to
Paaru and orange to

KISS
Mo ichido
Kagami no mae de
Chiisaku
Tsubuyaite
Miru

Kitto kore ga
Ai no tema
Kotoshi no
Ai no tema
Amai amai
Ai no tema
Watashitachi no
Ai no tema

KISS
Kono natsu wa
Atarashii koibito to
Vacances
Sukoshi tokimeku

Chiisana bag ni wa
Chiisana camera to
Noto to
Iroenpitsu to

Pink to purple to
Paaru to orange to
Atarashii sunglass
Atarashii sandal to
Atarashii mizugi mo
Kau no

Jet-ki de yukimasho
First class de
Futari
Shibaraku doko ka e
Oshinobi de dekakeru

Kitto kore ga
Ai no tema
Kotoshi no
Ai no tema
Amai amai
Ai no tema
Watashitachi no
Ai no tema

--------------------------
KISS
In front of the mirror
I whisper
Softly

KISS
When I think of
Tonight's kiss
My heart
Throbs
A little

The beginning of a new love
Is getting closer
Gets you out of breath
And rushes
To catch you

A new lipstick
What colour
Goes with
My new boyfriend?
Pink and purple
Pearl and orange?

KISS
Once again
In front of
The mirror
I whisper
Softly

This has to be
Love's theme
This year's
Love's theme
Sweet sweet
Love's theme
Our
Love's theme

KISS
This summer
I'll be going on holyday
With my new boyfriend
My heart throbs a little

Inside the small bag
A small camera
A notebook
And colour pencils

Pink and purple
Pearl and orange
Gotta buy new sunglasses
New sandals
And a new
Bathing suit

Let's go on a jetplane
The two of us
First class
Let's go ingonito
Somewhere for a while

This has to be
Love's theme
This year's
Love theme
Sweet sweet
Love theme
Our own
Love theme