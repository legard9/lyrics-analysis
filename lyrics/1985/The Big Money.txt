[Chorus]
Sweet mother, I no go forget you
For the suffer wey you suffer for me yeah
Sweet mother, I no go forget you
For the suffer wey you suffer for me yeah
Sweet mother, I no go forget you
For the suffer wey you suffer for me yeah
Sweet mother, I no go forget you
For the suffer wey you suffer for me yeah

[Verse 1]
Mum, mum, mum
You know you're my number one
Mum, you know you're my number one
And I know actions speak louder than words too, but I'm telling you now, I love you mum
I love you mum
For all the big things and all the little little stuff you've done
And I know actions speak loud, but they can't speak louder than the crowd
When I say you will get an action replay and a rollin' with the nines if you get rude to my mumsie
I'm a nice guy but I can get ugly
I'm a grime guy but I can get funky
And me without mum is like cornflakes without the milk
I love my mum like garnet silk
But I don't wanna get locked up like Shyne
So God forgive me if I bust my-

[Chorus]
Sweet mother, I no go forget you (I never forget you)
For the suffer wey you suffer for me yeah (Oi dad, am I reppin')
Sweet mother, I no go forget you (Music please)
For the suffer wey you suffer for me yeah (Oi, oi, go on then)

[Verse 2]
Listen to this mum
This song's worth much more than a kiss mum
Worth more than a bracelet on the wrist mum
And you know me dad, I'm your first son
And I will never let nobody diss mum (Never)
I don't know another love like this mum
How can your love make me happy as Larry
But still sometimes make cry like Swiss mum
(Mum better know) That I will always take care of my two younger brothers and my sis mum
I know we argue, but it's all crisp mum
Me and Jme are like Ace and Vis mum
It's okay, leave that, sit down
You just made the food, let me wash up your dish mum
You deserve it, you gave birth to two of the biggest grime artists in the bizz mum

[Chorus]
Sweet mother, I no go forget you (Yeah, forget you)
For the suffer wey you suffer for me yeah (For the suffer wey you suffer for me)
Sweet mother, I no go forget you (It's Junior, [?])
For the suffer wey you suffer for me yeah (Oi mum, let me break it down for you so you understand)

[Verse 3]
I love you mum, it's the deepest love
The deepest feeling
And if you love your mum
Let me see your middle finger and index finger touching the ceiling
And say "brap", "brap"
'Cause if it wasn't for your mum, you wouldn't be breathing
And I know I don't normally MC like this
But, you know what, it's not an MC ting
I'm not a prodigal son
That's not me don
I love my sweet mum
She brought me up properly
She never let me come home at all hours of the evening
'Cause when I was bad, she made me stand in the corner
And put my hands up
It was either that or get a beating (Go on then)

[Chorus]
Sweet mother, I no go forget you (Forget you)
For the suffer wey you suffer for me yeah (I love you mum)
Sweet mother, I no go forget you (It's not just Mothers Day, it's not just on your Birthday)
For the suffer wey you suffer for me yeah (It's everyday, I love you)

[Outro]
This one's for you
Actually, mum, can I borrow phone card
Let me phone up nan and granddad and let them know, that two of their grandsons, are in the big money sound
'Cause, grandma and granddad, better know