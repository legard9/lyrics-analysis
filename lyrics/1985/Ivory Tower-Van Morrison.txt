When you come down
From your Ivory Tower
You will see how it really must be
To be like me, to see like me
To feel like me
Take a look at me, I'm a poor man's son
I never did no harm to no one
You've got money in the bank
And I don't have none
When you come down
From your Ivory Tower
You will see how it really must be
To be like me, to see like me
To feel like me
You can see through your rose colored glasses
In a world that seems like glamor to you
You've got opinions and judgments about
All kinds of things
That you don't know anything about
Don't you know the price that I have to pay
Just to do everything I have to do
Do you think that there's nothing to it
You should try it sometime
When you come down
From your Ivory Tower
You will see how it really must be
To be like me, to see like me
To feel like me