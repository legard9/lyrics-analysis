[Verse 1]
There's a moment in this journey that I gave up
My boots just couldn't walk another mile
And that cloud above me had no silver linin'
I couldn't buy a break with my last dime
Oh, but when I saw you standin' in the corner
I'da never thought that you would have my back
But then we rolled in like the thunder and the lightenin'
Threw some punches then we had a laugh

[Chorus]
Just some roughed up desperadoes
Hangin' tough through thick and thin
Kickin' up dust wherever we go
I can see that you and me are gonna be friends

[Verse 2]
Who'da thought we'd wind up here together?
It's crazy that we're standin' side by side
Fighting just like two birds of a feather
Who's gonna tell us now that we can't fly?

[Chorus]
Just some roughed up desperadoes
Hangin' tough through thick and thin
Kickin' up dust where ever we go
I can see that you and me are gonna be friends
To the end you and me are gonna be friends

[Bridge]
Yeah, here we go
Hey, hey you and me
Different as different can be
You like to rock, I like to roll
You take the high and I'll take the low
Woah, woah-oh, woah, woah-oh

[Chorus]
Just some roughed up desperadoes
Hangin' tough through thick and thin
Kickin' up dust wherever we go
I can see that you and me are gonna be friends
To the end you and me are gonna be friends

[Outro]
I can see that you and me are gonna be friends, yeah