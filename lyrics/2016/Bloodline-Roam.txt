[Intro]
You didn’t mean it when you said that you’d try
To stay away from the things that you had to hide
You didn’t mean it when you said that your vice was external
You’re walking in circles

[Verse 1]
Do you feel anything when you hide behind yourself?
Or did you even know you did that and it affected someone else?
Do you tell anyone, that you can’t sleep at night?
Or is there no one to confide in when it’s in your bloodline?

[Chorus]
It’s not the same this time, you said it but I know it’s just a lie
I hope it was enough to be dishonest with
The people you took comfort in
It’s not the same this time, you said it but I know it’s just a lie
I hope it was enough to be the way you are
When everything’s falling apart

[Verse 2]
Do you feel anything? Do you feel anything?
Do you feel anything; does it come to mind?
When you lay in bed and try to sleep at night?
Do you find any shelter behind closed eyes; is it on your mind?

[Chorus]
It’s not the same this time, you said it but I know it’s just a lie
I hope it was enough to be dishonest with
The people you took comfort in
It’s not the same this time, you said it but I know it’s just a lie
I hope it was enough to be the way you are
When everything’s falling apart

[Outro]
You didn’t mean it when you said that you’d try
To stay away from the things that you had to hide
You didn’t mean it when you said that your vice was external
You’re walking in circles
You didn’t mean it when you said that you’d try
To stay away from the things that you had to hide
You didn’t mean it when you said that your vice was external
You’re walking in circles