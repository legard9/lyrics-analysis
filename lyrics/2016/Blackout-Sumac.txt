Words of summons
Long since blown
Into dust, beyond memory
What remains
Sting of dirt in eyes
Clotted on wriggling tongue

Under bent cedar
Low branches hang
Silent witness
To birth spasms
Bristling beast
Burning hot breath
Odor of violence
Stammering wrath
Ceremony of terrible emergence
Known without lesson
(Knelt to pray)
Liberated in worship
Devout in practice
Delivering the sermon
To a subservient
Congregation of one
Submission, all must bend
Facade is broken