[Verse 1]
How many lives do I have to wait?
How many hearts does it take to break this body?
'Til I feel like somebody?
How many lives do I have to wait?
How many hearts does it take to break this body?
'Til I feel like somebody?

[Chorus]
I wanna live, wanna die on a silver lining
I wanna ride the wings of love
When I die, don't you cry, I'll be flying by you
I'll be riding wings of love

[Verse 2]
How many lies do I have to shake?
How many nights should I lie awake on fire?
Someone take me higher
How many lies do I have to shake?
How many nights should I lie awake on fire?
Someone take me higher

[Chorus]
I wanna live, wanna die on a silver lining
I wanna ride the wings of love
When I die, don't you cry, I'll be flying by you
I'll be riding wings of love

[Post-Chorus]
Wings of love
Wings of love
Wings of love
Wings of love

[Chorus]
I wanna live, wanna die on a silver lining
I wanna ride the wings of love
When I die, don't you cry, I'll be flying by you
I'll be riding wings of love

[Post-Chorus]
Wings of love
Wings of love
Wings of love
Wings of love