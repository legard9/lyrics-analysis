There is this park near Floyd's place.
Where nature smiles without a trace...
Of chaos wrought in our world today.
Where good is good and bad has no way...
Of entering the mind and causing grief.
For this was Floyd's found inner peace.

He'd sit and fish...contemplate the sky...
He'd wish for hope that by and by...
His ashes to sprinkle with Mary, his bride.
Then grace this world with a wonderful sigh.
For Nancytown is where he'd go.
To bide his time and then...to show...

His neighbors Buck and Mitchell too,
His 'catch' that day was theirs...it's true.
Floyd's dreams for them their families dear,
While fishing with his thoughts and fears,
For lonely he wasn't and friend's love, he was.
Secure from anger...for God above

Gave Floyd this heart so pure and chaste...
A spirit of kindness and none of haste.
For this genuine man who loved this world...
And gave so much...my thoughts are curled,
Inside...for he was special to me...
And the fishing we shared has come to be...

The thoughts I will cherish from my heart to his.
And carry these thoughts forever within.
My soul, for the richness he shared with this world.
For these spoken words, "I love ya', my girl".
He was my friend and uncle, as well...
Above that this goodness...he shared it so well.

He came to this world, a grin not a frown...
And found his peace fishing...at Nancytown.
My soul is with him as he walks this last mile,
My wish for a wink and a grin and a smile...
Tell my Dad that I love him, give Mary a hug,
Get the pole and the worms and your little red jug.

For...I'm told that the fishing in heaven to be...
The greatest of sport with a deep ,blue sea.
Full of Rainbows just waiting for fishermen who...
Have come for the 'Big One', this trophy for two,
Brothers who graced this world with a crown...
Kings of the "Big Ones", at old Nancytown.

written, with love for Uncle Floyd de Vegter
By Susan K. de Vegter Rowse