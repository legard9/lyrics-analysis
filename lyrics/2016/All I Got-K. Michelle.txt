[Verse 1]
I'll give you this dance, and I'll give you hope
I'll sing you a song, its something I wrote
I'll trace you in ink, connect all the dots
Its not much, but its all that I've got
I'll give you my heart, and some of my time
And if you do wrong, a piece of my mind
I'll give you some space, if that's what you want
It's not much, but it's all that I've got

[Pre-Chorus]
Letting it all out
Breaking these walls down
No where to hide now
It's not much, but it's all that I've got

[Chorus]
If all I got is the love I was promised
I'm good, even if it's all I got
If all I got is the touch of your body
I'm good, even if it's all I got

[Verse 2]
Lets do it again, don't want it to end
So baby just keep your hands on my skin
I love how it feels to lay here and talk
Uh! It's not much, but its all that I've got

[Pre-Chorus]
Letting it all out
Breaking these walls down
No where to hide now
It's not much, but it's all that I've got

[Chorus]
If all I got is the love I was promised
I'm good, even if it's all I got
If all I got is the touch of your body
I'm good, even if it's all I got

[Hook]
You're all I got, I wanna be solid as a rock
Im never gonna stop, loving you outside the box
Huh, oooh!

[Chorus]
If all I got is the love I was promised
I'm good (Baby It's all good, no) Even if it's all I got
If all I got is the touch of your body
I'm good, even if it's all I got
If all I got is the love I was promised
I'm good, even if it's all I got
If all I got is the touch of your body
I'm good, even if it's all I got