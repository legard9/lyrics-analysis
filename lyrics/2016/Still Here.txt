[Intro]
Yo, Eskiboy, Tunnel Vision Volume 3
I just wanna say like, thanks to my mum innit like (Thanks Mum)
She gave birth to the greatest grime artist of all time
Shut ya mout, Boy Better Know

[Verse 1]
Thanks Mum it's been 27 years, I thought of you when I was having beef with 27 brehs
I know how you feel about me being streetwise, I'm so sorry I like facing fears
But, all the work paid off 'cause I'm looking at the sunset and racing clear
Still wasting brehs in a battle on the mic
But don't worry 'cause I got it covered when I'm 'ere
I've had a bad day, but I realise, back then I tried to not care
But a mother's love is too strong so don't play with it
Plus the family's still here and me, I'm still here
Nanny rang me, and she told me she still cares
Plus I'm round Jerome's and he still cares
What about Daddy? He still cares
Mum it's so cold when I roam on the roadside
If you got a name, yeah, everybody knows mine
I try and stay cool on a low vibe
Then I gotta switch and get real on a show guy
Show guys there are MC's in my game who try and get a few stripes off of my name
But Mum know like since Junior got killed
I've had it in me to go out and rain on a guy
And when I get aggy, they wanna ask why I just celebrate, you know why
I wouldn't do it for nothing 'cause I'm a humble guy
And my uncles, they're nightmares
They'll come to your ends, walk right where any badman can say, "I live right here"
Can't chat shite here, I'm the aggiest Lord of the Mic here

[Bridge]
Thanks Mum (Thank you)
You gave birth (Thanks mother) to the greatest grime artist of all time (Thank's mum, thank you)
Hol' on tight (Hol' on tight, I appreciate it, trust me)
(Eski) Eskiboy
Tunnel Vision Volume 3 (Tunnel Vision Volume 3)
Boy Better Know, shut your mout'

[Verse 2]
There's not a woman on earth who could love me more than Mum
So why am I an idiot, letting this girl run my life? (Run)
It's so hard trying to be number one in the game (One)
Have a girl who ain't on a hype ting
You don't wanna see me cooching with your ting
I don't wanna see you cooching with my ting
I don't spit just for the sake of rhyming
I do practice flowing and timing
Tightness, in the ranks I'm climbing up
And I won't stop writing
The legend of Wiley won't die king
He's been a warrior, he won't stop fighting
Slap your wifey up if she likes him
And that will stop you and him from fighting
Even though I'm on a hype ting, my advice to you will be do the right thing

[Outro]
Thanks Mum (Thanks mum)
Tunnel Vision Volume 3, shut your mout (Eski, Eski, Eski)
Boy Better Know, in the place (Thanks Mum, in the place)
Eskiboy (Eskiboy, thanks Mum)