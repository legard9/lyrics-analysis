[Verse 1]
War is all around
I'm growing tired of fighting
I've been drained and I can't hide it
But I have strength for you, you're all that's real anymore
I am coming home now, I need your comfort

[Chorus]
From this moment
From this moment
You will never be alone
We're bound together
Now and forever
The loneliness has gone

[Verse 2]
States are crumbling, and walls are rising high again
It's no place for the faint-hearted
But my heart is strong because now I know where I belong
It's you and I against the world
And we are free

[Chorus]
From this moment
From this moment
You will never be alone
We're bound together
Now and forever
The loneliness has gone

[Bridge]
We've gone against the tide
All we have is each other now
I am coming home now
I need your comfort

[Chorus]
From this moment
From this moment
You will never be alone
We're bound together
Now and forever
The loneliness has gone

[Chorus]
From this moment
From this moment
You will never be alone
We're bound together
Now and forever
The loneliness has gone

[Chorus]
From this moment
From this moment
You will never be alone
We're bound together
Now and forever
The loneliness has gone

[Outro]
We're bound together
Now and forever
The loneliness has gone