[Verse 1]
Pressure's on, pressure's high
Pressure's heavy on my mind
Weight is here, weight is right
Weight is heavy on my spine
Truth has holes; truth, it swells
Sometimes truth can feel like hell
And it's full, and it swells
In the end, we'll all be well

[Chorus]
How did expectations get so high?
Got a wicked thirst to feel alive
How did expectations get so high?
Now I have nowhere to run and hide
Run and hide

[Verse 2]
Ideas move, ideas pull
Can ideas be controlled?
Fear has grown, fear so old
Fear is pulsing in my skull
Hope below, hope on high
Hope in ocean, hope in sky
People come, people try
People gone in the blink of an eye

[Chorus]
How did expectations get so high?
Got a wicked thirst to feel alive
How did expectations get so high?
Now I have nowhere to run and hide
Run and hide

[Bridge]
Who's in control, who's in control, who's in control, who's in control?
Who's in control, who's in control, who's in control, who's in control?
Who's in control, who's in control, who's in control, who's in control?
Who's in control, who's in control, who's in control, who's in control?
Who's in control, who's in control, who's in control, who's in control?
Who's in control, who's in control, who's in control, who's in control?
Who's in control, who's in control, who's in control, who's in control?
Who's in control, who's in control, who's in control, who's in control?

[Chorus]
How did expectations get so high?
Got a wicked thirst to feel alive
How did expectations get so high?

[Outro]
How did expectations get so high?