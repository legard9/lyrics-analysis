[Verse 1: Graham Nash]
I used to be in a band
Made up of my friends
We played across the land
When music had no end
And so beginning
We sang with all our hearts
And everything we had
Everything we gave
Came back to everyone
In the olden days

[Chorus: Graham Nash]
Songs with soul
And words with so much hope
For a brighter day
Oh, I know
That people hurt
But they tried to find a way
Through those broken days

[Verse 2: Graham Nash]
But nowadays it seems
That we all need to care
And follow all our dreams
And answer all our prayers
What happened to "All You Need Is Love"?
And time will always pass
There goes another day
So slowly yet so fast
That you can lose your way
To these golden days

[Chorus: Graham Nash]
Songs with soul
And words with so much hope
For a brighter day
Oh, I know
That people hurt
But they tried to find a better way
To these golden days

[Outro: Graham Nash]
Olden days
Broken days
Golden days