[Verse 1]
Keeping my thoughts under wraps tonight
Keeping my feelings to myself
I'm already broken hearted
So let's get this party started

The darkness comes with you
With longing and desire
Your anger burns inside your eyes
They flash like burning coal

[Chorus]
Get out your head, get out your head
Try not to think, be cool, be calm, be fake
Dumb yourself down, numb yourself out
Fake it till you make it break
Make the world black out

[Verse 2]
All the hurt you nurse inside
You'd better cough it up
Beautiful like shards of glass
In your throat they'll cut you up

[Chorus]
Get out your head, get out your head
Try not to think, be cool, be calm, be fake
Dumb yourself down, numb yourself out
Fake it till you make it break
Make the world black out

[Bridge]
That was such a wicked thing to say
That was such a spiteful thing to do
You smile as the words spew out your mouth
So I laugh in your face right back at you
Oh-whoah

[Verse 3]
There's no reward for men who weep
No medals given out
But better be the one who speaks
Than be the man who won't
(You, and you, and you, and you)

[Chorus]
Get out your head, get out your head
Try not to think, be cool, be calm, be fake
Dumb yourself down, numb yourself out
Fake it till you make it break
Make the world black out

[Outro]
We all blackout
We all blackout
We all blackout
We all blackout
We all blackout