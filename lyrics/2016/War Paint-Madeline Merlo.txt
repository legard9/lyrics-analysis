[Verse 1]
It's okay to not be okay
When even the air you breathe
Is just too much for your lungs to take
And all the words that you don't say
I hear your SOS on the radio
As you're driving away
C'mon pick up your phone
Don't have to face it alone
Gonna fight with you and for you
Yeah I want you to know

[Chorus]
I will stand your ground
I'll kiss your battle scars
And leave my XO mark till you can feel it
Can you feel it?
You can call my name
I'll hold your hand grenade
Keep all your secrets safe 'til you can see me
Can you see me coming?
Running for you dead of night
Can you hear me, hola, you're alright
I'll take your fears and wipe your eyes and wear it all like war paint
Wear it all like war paint, wear it all like war paint

[Verse 2]
I see gold, when you're black and blue
I love the colours burning beautiful that you wanna use
When all you want is to get out alive
You don't have to cry for me to hear your battle cry
C'mon turn on your light
I've been waiting all night
To fight with you and for you
When all your flags are white

[Chorus]
I will stand your ground
I'll kiss your battle scars
And leave my XO mark till you can feel it
Can you feel it?
You can call my name
I'll hold your hand grenade
Keep all your secrets safe 'til you can see me
Can you see me coming?
Running for you dead of night
Can you hear me, hola, you're alright
I'll take your fears and wipe your eyes and wear it all like war paint
Wear it all like war paint, wear it all like war paint

Wear it all like war paint, wear it all like war paint

I will stand your ground
I'll kiss your battle scars
And leave my XO mark till you can feel it
Can you feel it?
You can call my name
I'll hold your hand grenade
Keep all your secrets safe 'til you can see me
Can you see me coming?
Running for you dead of night
Can you hear me, hola, you're alright
I'll take your fears and wipe your eyes and wear it all like war paint
Wear it all like war paint, wear it all like war paint

Wear it all like war paint, wear it all like war paint