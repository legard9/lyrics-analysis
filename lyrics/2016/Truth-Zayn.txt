[Verse 1]
My luck's on the game, can't find a way through
Don't know how many times I had to say this to you
This ain't my scene, this wasn't my dream
It was all yours, of course
I got caught up in this game and you know I won't say names of who's to blame

[Pre-Chorus]
I won’t point any fingers, I won’t say it was you
I'll let life take its time, and in time, you'll see the truth
I won't ask any questions, I won't say it was you
I'll let life take its time, and in time, you'll see the truth

[Chorus]
See the truth, see the truth
See the truth, see the truth
See the truth, see the truth
See the truth, see the truth

[Verse 2]
My own mind was in the way, front seat, new view
Don't know how many times I've had to talk you through
My dreams, new scenes with enemies
I got caught up in this game, a game

[Pre-Chorus]
I won’t point any fingers, I won’t say it was you
I'll let life take it's time, and in time, you'll see the truth

[Chorus]
See the truth, see the truth
See the truth, see the truth
See the truth, see the truth
See the truth, see the truth

[Bridge]
I tried, hide it all
Don't try, don't try
Don't try to hide it all
Don't try to hide it all
Don't try, don't try
Don't try to hide it all

[Pre-Chorus]
I won’t point any fingers, I won’t say it was you
I'll let life take it's time, and in time, you'll see the truth

[Chorus]
See the truth, see the truth
See the truth, see the truth
See the truth, see the truth
See the truth, see the truth