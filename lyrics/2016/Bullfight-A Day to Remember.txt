[Verse 1]
What a waste, I see you now and then
But mostly we pretend we're not more strangers than we're friends
Then we say, what we need to say, to try and get away from the tension we create

[Pre-Chorus 1]
Why can't we say the things, the things we wish we would?
Why can't we laugh the way, the way we know we should?
Fall under the weight of all the pressure that's misplaced
Flaws that I embrace

[Chorus]
Everybody sees your head’s hung low
They don't ask, they don't wanna know us
I'll be the one, I'll be your spark
I'll be your light led through the darkness
Everybody sees your head’s hung low
They don't ask, they don't wanna know us
I'll be the one, I'll be your spark
I'll be your light led through the darkness

[Post-Chorus]
I get
I get
I get so misunderstood

[Verse 2]
Something changed the way we interact
Now we can't get it back
Another relic of the past (Relic of the past)
Makes you think, did I ever know you at all?
Follow until I fall
Vultures circling in the sun, the sun, the sun, the sun, the sun

[Pre-Chorus 1]
Why can't we say the things, the things we wish we would?
Why can't we laugh the way, the way we know we should?
Fall under the weight of all the pressure that's misplaced
Flaws that I embrace

[Chorus]
Everybody sees your head’s hung low
They don't ask, they don't wanna know us
I'll be the one, I'll be your spark
I'll be your light led through the darkness
Everybody sees your head’s hung low
They don't ask, they don't wanna know us
I'll be the one, I'll be your spark
I'll be your light led through the darkness

[Post-Chorus]
I get
I get
I get so misunderstood

[Breakdown]
You can't just avoid the issue
You should've learned from the things we've been through
You don't know what you have until it’s lost
Till the hammer drops
Brace, fall
Brace, fall
Brace, fall
Brace, fall
Brace, fall
Brace, fall
Brace, fall
Brace, fall

[Pre-Chorus 2]
Fall under the weight of all the pressure that's misplaced
Flaws that I embrace

[Chorus]
Everybody sees your head’s hung low
They don't ask, they don't wanna know us
I'll be the one, I'll be your spark
I'll be your light led through the darkness
Everybody sees your head’s hung low
They don't ask, they don't wanna know us
I'll be the one, I'll be your spark
I'll be your light led through the darkness

[Post-Chorus]
I get
I get
I get so misunderstood