[Verse 1]
Where are we now?
Both home alone in the same house
Would you even know me if I wasn’t old me?
If I wasn’t afraid to say what I mean?

[Chorus]
We’ve been here before
But I don’t remember a lock on the door
Is it keeping me out or you in?

[Verse 2]
Strange how these city streets have changed faces
Like we did
Would you even know me if time hadn’t stole me?
If I wasn’t afraid to say what I need?

[Chorus]
We’ve been here before
We’ll figure it out like that goddamn door
We just need a skeleton key

[Outro]
Leave me or don’t, I don’t care
Just let me know when you finally drag your body out of bed and I’ll get my things