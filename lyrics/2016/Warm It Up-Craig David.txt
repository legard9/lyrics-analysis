[Verse 1]
I've seen some girls in my time, but you're simply amazing
Can't keep my eyes off of you since you stepped in the room
Maybe it's all of these drinks, got me feeling so wavy
But all I know, all I know, tonight I'm with you, yeah

[Pre-Chorus]
She had me from the go, go, had me from the start
She told me that she wanted some love
She hit me with that oh, oh, wasn't holding back
She knew that what she wanted was love
We ain't got no time to waste
So why don't we do it right now?
Before you go walking away
Let me show you how we can get down

[Chorus]
Warm it up, that's all you gotta do for me baby
Warm it up, that's all you gotta do for me girl
I love it when you push it really close to me
The feeling when the music takes control of me
The way you move your body from the back
Like that, like that, when you move it like that
I love it when you push it really close to me
The feeling when the music takes control of me
The way you move your body from the back
Like that, like that, when you move it like that

[Verse 2]
Temperature rising, it's so hot, the music is blazing
The bass got us grinding our bodies in sync with the groove
My hands are exploring the places that drive you so crazy
And all I know, all I know, tonight I'm with you, yeah

[Pre-Chorus]
She had me from the go, go, had me from the start
She told me that she wanted some love
She hit me with that oh, oh, wasn't holding back
She knew that what she wanted was love
We ain't got no time to waste
So why don't we do it right now?
Before you go walking away
Let me show you how we can get down

[Chorus]
Warm it up, that's all you gotta do for me baby
Warm it up, that's all you gotta do for me girl
I love it when you push it really close to me
The feeling when the music takes control of me
The way you move your body from the back
Like that, like that, when you move it like that
I love it when you push it really close to me
The feeling when the music takes control of me
The way you move your body from the back
Like that, like that, when you move it like that

[Bridge]
Work what your mama gave you
Don't be shy, don't be shy
Lessons your father taught you
Not tonight, not tonight
Right now, you ain't gotta hold back
They're flaying your favourite throwback
The things that you wanna do
It's alright, it's alright, yeah
It's alright, it's alright, yeah
It's alright, it's alright, yeah