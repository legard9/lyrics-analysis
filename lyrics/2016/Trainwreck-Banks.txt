[Chorus]
Hey, I heard it from the state
They told me you were never gonna let me get away
And if you took me fishing, you would never give me bait
I had to get away, I had to get away, ay-ay
Hey, you try to compensate
For thinking with your one brain I should decapitate
You showed me all your letters that I should've confiscated
Both of my eyes were weighted and I had to get away

[Verse 1]
Talking to ears that have been deaf for as long as I can remember
A self-medicated handicap, so I speak to myself
And I try so hard to get his stupid deaf ears to hear
That I've become illiterate, I've become dumb

[Pre-Chorus 1]
When I come through, you were dark blue
And I saved you from your darker days
Born to take care of you, or I thought so
Maybe it was just a phase

[Chorus]
Hey, I heard it from the state
They told me you were never gonna let me get away
And if you took me fishing, you would never give me bait
I had to get away, I had to get away, I had to get away
Hey, you try to compensate
For thinking with your one brain I should decapitate
You showed me all your letters that I should've confiscated
Both of my eyes were weighted, I had to get away

[Verse 2]
Shot down by a guy I never wanted to kiss
And I can hear the singing of his ringing triumphing
And I'm chugging along in a train
And I'm heading the wrong way, and I'm a trainwreck
And my heart goes "Beat, beat, beat"
To the music of this sad, same song
It's quite depressing
There is no fixing to the problem when you're talking to an idiot

[Pre-Chorus 2]
When I come through, you were dark blue
And I saved you, from your darker days
Born to take care of you, or I thought so
Maybe it was just a phase
Baby, only maybe
Just a dreamer, but I soon found out
That the train tracks were behind me
Tried to warn me, but both of my ears went out

[Interlude]
(But then my ears went out, out, out...)

[Pre-Chorus 1]
When I come through, you were dark blue
And I saved you, from your darker days
Born to take care of you, or I thought so
Maybe it was just a phase

[Chorus]
Hey, I heard it from the state
They told me you were never gonna let me get away
And if you took me fishing, you would never give me bait
I had to get away, I had to get away, I had to get away
Hey, you try to compensate
For thinking with your one brain I should decapitate
You showed me all your letters that I should've confiscated
Both of my eyes were weighted, I had to get away