[Verse 1]
This is my time with you, I'm not giving it back
They could break our arms but we will remain intact
You’d never had anyone make you feel so good
I can see it in your eyes when I look at you

[Chorus]
It's in our eyes but we never tell
It's in our eyes but we never tell
It's in our eyes but we never tell
That I want you, ooh
I want you, I want you
I know you want me too

[Verse 2]
We're on the outside always looking in
You don't trust humans and I feel the same

[Chorus]
It's in our eyes but we never tell
It's in our eyes but we never tell
It's in our eyes but we never tell
That I want you, ooh
I want you, I want you
I know you want me too

[Bridge]
Let's give 'em something to remember
Something to talk about
On their telephones
On their couch at home
Make me shake, make me tremble
We can be animals
Our secret universe, a place to be us

[Verse 3]
This is my time with you, I'm not giving it back
They could break our arms but we will remain intact

[Chorus]
It's in our eyes but we never tell
It's in our eyes but we never tell
It's in our eyes but we never tell
It's in our eyes but we never tell
It's in our eyes but we never tell
It's in our eyes but we never tell
It's in our eyes but we never tell

[Outro]
Let's give 'em something to remember
Something to talk about
Let's give 'em something to remember
Something to talk about