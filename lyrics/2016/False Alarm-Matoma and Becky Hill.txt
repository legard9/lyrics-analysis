[Pre-Chorus]
I heard sirens in my head
From the first time that we met
Thought it was a false alarm
Yeah, we started as a spark
Didn't think we'd come this far
But here we are, oh

[Chorus]
I'm dancing in flames
I'm dancing in flames
I ain't scared of the blaze
Don't rescue me
And now I'm burning in your arms
Endless fire in my heart
No, it's not a false alarm
No, it's not a false alarm

[Verse 1]
But here we are
I never get my hopes up
Cause then I'll never get let down
But you were something special
I didn't notice until now

[Pre-Chorus]
I heard sirens in my head
From the first time that we met
Thought it was a false alarm
Yeah, we started as a spark
Didn't think we'd come this far
But here we are, oh

[Chorus]
I'm dancing in flames
I'm dancing in flames
I ain't scared of the blaze
Don't rescue me
And now I'm burning in your arms
Endless fire in my heart
No, it's not a false alarm
No, it's not a false alarm
No, it's not a false alarm
No, no
No, it's not a false alarm
No, no
No, it's not a false alarm

[Bridge]
Maybe I'm too careful
Didn't trust the signs
Didn't wanna jump
Too scared of the height

[Chorus]
And now I'm burning in your arms
Endless fire in my heart
No, it's not a false alarm
No, it's not a false alarm
Oh, no, it's not a false alarm
No, no
No, it's not a false alarm
No, it's not a false alarm
No, it's not a false alarm