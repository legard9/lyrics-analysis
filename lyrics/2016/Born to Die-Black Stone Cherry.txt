[Verse 1]
Moma was a moon, Daddy was a Sun
Of a man shot down by a law man's gun
On his dying day he looked my way
He said listen to me son while you still can
You gotta take life's cards with a loving hand
And you can live to fight another day

[Chorus]
Because we're born to die
In this life, when its time
Take a bow
What you leave behind
You've got a love for life
Take your time, make it right
Before you go, you can shine a light
Guard your promise, cross my heart
Told my secret to the stars
Now I know the reason why
We're born to die

[Verse 2]
I was fifteen, I knew it all
I tried to run before I knew how to crawl
And couldn't nobody seem to make me understand
But in this life, you've gotta pick your fights
And even if you loose you gotta know you tried
Sometimes you've got to fall before you dance

[Chorus]
Because we're born to die
In this life, when its time
Take a bow
What you leave behind
You've got a love for life
Take your time, make it right
Before you go you can shine a light
Guard your promise, cross my heart
Told my secret to the stars
Now I know the reason why
We're born to die

[Interlude]

[Chorus]
Because we're born to die
In this life, when its time
Take a bow
What you leave behind
You've got a love for life
Take your time, make it right
Before you go you can shine a light
Guard your promise, cross my heart
Told my secret to the stars
Now I know the reason why
We're born to die
Born to die