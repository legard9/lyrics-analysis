[Verse]
Anywhere you are and you'll go, I follow
Mountain high and river low, I follow
Take me to the bottom, drive me in low
Wherever you go
Mountain high and river low, I follow

[Chorus]
It's a beautiful, beautiful life
Beautiful, beautiful life
Beautiful, beautiful life
With you

[Verse]
Anywhere you are and you'll go, I follow
Mountain high and river low, I follow
Take me to the bottom, drive me in low
Wherever you go
Mountain high and river low, I follow

[Chorus]
Beautiful, beautiful life
Beautiful, beautiful life
Beautiful, beautiful life
With you

[Outro]
Anywhere she wants to go