[In front of Cartman's house. Jimbo, Ned, and the boys are waiting for Cartman]
Liane:	Now you be careful, Eric. The woods can be very dangerous.
Cartman:	Okay, Mom.
Kyle:	Ready to go hunting, Cartman?
Stan:	Yeah, my Uncle Jimbo says we gotta get up there early. Right, Uncle Jimbo?
Jimbo:	That's right, Stanley. Animals are much easier to shoot in the morning.
Liane:	Here hon, I packed you some cheesy poofs and happy tarts.
Jimbo:	Don't worry Mrs. Cartman, we'll take good care of him. I brought my old war buddy Ned to keep things safe.
Ned:	Hello, Mrs. Cartman. How are you today?
Liane:	Be sure to use lots of bug spray, and if you have to poo-poo, don't wipe with poison ivy. [The other boys laugh]
Cartman:	Dude, that's sick mom.
Liane:	And I know it can get scary up in those woods, but just remember, mommy's not far away. [They laugh some more]
Cartman:	Drive, Drive!
Liane:	You give your mommy a kissy.
Cartman:	Drive the car damn-it, drive!

[In the Humvee]
Kyle:	[in a slight falsetto] Don't get scared up in the mountains Cartman.
Cartman:	Shut up, I'm not scared of nothing.
Stan:	Maybe your mom can give me a kiss too, Cartman.
Kenny:	(Maybe she'll suck my dick)
Jimbo:	Oh, ho, that's disgusting.
Cartman:	You piece of crap, I'll kill you! [starts fighting with Kenny]
Jimbo:	That's the spirit boys, let's get that testosterone flowin'.
Cartman:	Eeeh! [about to strike Kenny again]
Jimbo:	Now boys, boys, ah-I need to get serious for a minute. I want you to understand a few basic rules of hunting, since this is your first time. First, don't ever walk with your gun unless the safety's on. Second, don't shoot anything that looks human, and third, never spill your beer in the bullet chamber.
Stan:	Uh, Uncle Jimbo, we don't drink beer.
Jimbo:	You what?!?
Ned:	Moh Yeah, that's right, I don't think eight year old kids drink beer, mmm.
Kyle:	I like chocolate milk.
Jimbo:	Well, we'll be doing plenty of drinking on this trip; After all, hunting sober is like … fishing … sober. It sure will be nice to get out of the city for a while, away from civilization.

[On the mountain]
Jimbo:	Well, here we are. Okay, each of you young 'uns take a gun, a beer and some smokes.
Cartman:	Hey, I didn't get a gun. [Jimbo hands him one] Sweet. This is like the gun I used in 'Nam
Stan:	You weren't in Vietnam, Cartman.
Ned:	Were you stationed in De Nang?
Stan:	Cartman always makes stuff up, Ned. You can't believe anything he says.
Cartman:	Hey, I'll blow your friggin' head off.
Jimbo:	[rushing up] Hey, look out son, that's dangerous. You're gonna spill your beer.
Stan:	My Uncle Jimbo says that after this, he's gonna take me hunting in Africa.
Kyle:	Wow, that'd be cool.
Cartman:	My mom says there's a lot of black people in Africa.
Jimbo:	Boys, looky there. That there's a Rocky Mountain black bear …one of the few remaining of its kind. Isn't it beautiful… By God, it's coming right for us! [Shoots it. It is hit and flips off the mound it was on.]
Stan:	Hey, it wasn't coming right for us. It was just sitting there.
Jimbo:	Shhh, not so loud. Now that there's just a technicality.
Kyle:	What do you mean?
Jimbo:	You see boys, the Democrats have passed a lot of laws trying to stop us from hunting.
Cartman:	Democrats piss me off!
Jimbo:	They say we can't shoot certain animals anymore, unless they're posing an immediate threat. Therefore, before we shoot somethin', we have to say 'It's coming right for us.'
Stan:	Wow, you're smart Uncle Jimbo.
Ned:	Mmm, Jimbo look, mm.
Jimbo:	Ohho, it's a deer. Looks like about a .46 gauge, Ned. It's coming right for us! [pulls out a bazooka, mounts it on his shoulder, and fires a rocket. All that's left of the deer is a hind leg, which falls over]
Cartman:	Kickass!! [everyone is in creep position now, except Jimbo]
Jimbo:	Did you see that?! I was imperiled by that ferocious, charging buck.
Ned:	Moohh, rabbit, rabbit, 5 o'clock. [a rabbit hops blissfully into view]
Jimbo:	[drops to the ground] Let's move, moove! [hustles off to the right. Ned follows]
Stan:	Is this hunting?
Kyle:	I guess so.
Cartman:	Ah, dude! I'm starting to have flashbacks.
Kyle:	What?!
Cartman:	Danforth, pull up flank! Look out for Charlies- up in the trees!

[On the ridge]
Jimbo:	This one's yours Stan. [Stan aims at the bunny, but it just blinks at him. Jimbo coaxes] It's coming right for us.
Ned:	It's coming right for us. [Stan focuses. The bunny blinks]
Kyle:	Shoot it Stan.
Cartman:	I got your back, soldier.
Stan:	[lowers his gun] I can't. [the bunny hops away]
Jimbo:	What the? What's wrong with you?
Stan:	I don't want to shoot the bunny.
Jimbo:	What the hell are you talking about, you don't want to shoot the bunny? You're babbling, you're not making any sense, you're hysterical.
Stan:	I'm not hysterical, I just don't want to shoot the bunny.
Jimbo:	No nephew of mine is gonna be a tree hugger.
Cartman:	Yeah hippie, go back to Woodstock if you can't shoot anything.
Stan:	I can shoot you, fat ass!
Cartman:	I can shoot you too!
Stan:	I'll kill you!
Cartman:	I'll fill you full of lead!
Kyle:	[the mountain rumbles] Hey, what's that?

[South Park Center for Seismic Activity, Randy is humming: another day and all is quiet. A rumble is heard in the distance, but Randy is dozing off. A stronger rumble rolls through]
Randy:	[checks the seismograph] What the heck is this? [places a call] Yeah, Frank, it's uh Randy, Uh huh, good, good. Yeah, listen, the uh, the little needle's moving. Yeah, it's going back and forth really fast-what does that mean? … Uh huh … Uh huh … Let me check [looks at the volcano, which shows a column of steam] Yeah, it's smoking. Uh huh. Oh really? Really? Oh my God! A volcano! [Dramatic music builds, then Randy … sips coffee]

[Night on the mountain]
Cartman:	My weenies won't cook.
Ned:	Mmm, this wood won't burn, umm.
Jimbo:	Well, Ned, looks like we'll have to use the old Indian fire trick.
Ned:	Mmm, yepper. [pours gas on the fire. The flames run up the stream and set Ned aflame] Mmm, aahh, oh.
Jimbo:	Hehehaahaa, hey, stop-drop-and roll Ned. Haaahaaaaahaha!! [Ned drops and rolls. He knocks the gas can over, and the flaming gas blows up the Hummer]
Stan, Kyle:	Whoa!
Jimbo:	Goddamnit Ned, I just got that van. How the hell are we supposed to get home?
Ned:	Mmm, oh, it hurts, it hurts, mmm.
Cartman:	[now cooking his weenies over Ned's burning body] Hey you guys, this works pretty good right now.

[City Hall, The following morning]
Chef:	But you see Mr. Mayor, you can't stop serving Salisbury steak in our public schools. What's next, meatloaf?
Mayor:	We are quite aware of your concerns Chef, but… [the intercom beeps. She answers]
Johnson:	Mayor, the geologist is here to see you.
Mayor:	My geologist? Now? Tell him the infection is fine and I don't need another check-up.
Johnson:	No mayor, that's a gynecologist. A geologist studies the earth.
Mayor:	Don't you think I know that? How dare you insult my intellect, I went to Princeton for God's sake! You get out of my office!
Johnson:	I'm not in your office mayor, I'm talking to you through a speaker.
Mayor:	[long pause] Just send in the geometrist.
Johnson:	Geologist…
Mayor:	You are fired, buddy!
Johnson:	Thank you mayor, it's been great working for you.
Randy:	[entering] Mayor, we have got a very big problem. Mt. Evanston is about to erupt.
Mayor:	What does this mean to the town?
Randy:	Well, this graph shows everything from normal to bad. [he points out relative damage levels on a chart] Right now South Park is here.
Mayor:	My God!
Chef:	Mayor, some of the school children, are up camping, on that mountain, right now!
Mayor:	Ooh, this is big! Johnson, Johnson, are you there? [Beeps out]
Ted:	Uhh, you just fired Johnson, Mayor. I'm his replacement, Ted.
Mayor:	Ted, we have got a major crisis here. I want you to get on the phone and call Inside Edition, Rescue 911-uh, and Entertainment Tonight-eh. [thump] Better get my stylist on the phone too. Don't worry, things are under control.

[Back on the Mountain. Jimbo is telling stories]
Jimbo:	You see… Ned picked up the grenade and… BOOM, blasted his arm clear off. We spent three hours looking for that damn arm, but it was never to be found. Some say it's still crawling around to this day.
Ned:	[extends his prosthetic arm towards Cartman as if to show that shot-off arm is now at the campsite, climbing on Cartman] Mmmowwwh.

[Cartman flinches, and the others laugh]
Jimbo:	Hah, got ya.
Cartman:	Heh, that's not scary.
Kyle:	You were scared Cartman! you almost peed in your pants!
Cartman:	Shut up, I didn't pee in my pants!
Jimbo:	Hey Ned, hand me that gin. [Ned opens up the icebox and tosses a bottle to him] You boys want to tie one on?
Kyle:	No, No thanks, that stuff tastes like pee.
Stan:	Yeah, Cartman's pee.
Cartman:	Hey, you would taste my pee!
Jimbo:	What the hell's wrong with you? Can't you have a little alcohol? [Kenny reaches for the gas can and chugs away] Christ, look at that little bastard go! [Kenny hiccups] Now you see that Stan? Now, now that is a dirty little bastard!
Stan:	Hey, I'm a dirty little bastard too.
Cartman:	Hey you guys. I know a scary story.
Kyle:	Shut up, Cartman! You can't scare anybody!
Cartman:	[softly, slowly, flashlight ready] Oh yeah? Have you guys ever heard of [flashlight on] Scuzzlebutt?
Stan:	Whatabutt?
Cartman:	Scuzzlebutt is a creature that lives up on this very mountain, and kills anybody who dares climb to the top.
Stan:	Why?
Cartman:	Because, it loves the taste of blood, and likes to add pieces to its deformed body.
Kyle:	Deformed how?
Cartman:	Well, on his left arm, instead of a hand, he has…
Stan:	A hook.
Kyle:	A knife.
Cartman:	No! A piece of celery.
Stan:	Celery?
Cartman:	Yeah, and he walks with a limp. Because one of his legs is missing. And where his leg should be, there's nothing but…Patrick Duffy.
Kyle:	Patrick Duffy? Damn it Cartman, that's not scary!
Cartman:	What do you mean? Have you ever seen Step By Step? [silence] So he lives alone on this mountain, and weaves baskets, and other assorted crafts. They say that on quiet nights you can hear him weaving his baskets. Tahink … tahink … tahink.
Stan:	Cartman, you suck at telling scary stories.
Kyle:	Yeah, give me that flashlight. [takes it and turns it off]
Kenny:	[Volcano rumbles] (Hey, what's that?)
Kyle:	What is that?
Stan:	Maybe it's Scuzzlebutt coming to weave us into wicker baskets.
Cartman:	Hey, it might be!
Kyle:	Gosh, I hope he doesn't cut me with his celery hand. [the others laugh]
Cartman:	You guys! … Go to Hell!
Jimbo:	[now playing his guitar] Hey Ned, why don't you whip out the ol' cancer kazoo? Let's do a little song.
Ned:
Abumbayah my lord, kumbayah.
Kumbayah my lord, kumbayah.
Uumbayah my lord, kumbayah. [Uncle Jimbo cries]
Moh lord, Kumbayah.
Emsomeone's crying my lord, Kumbayah
Someone's crying my lord, Kumbayah...
Cartman:	They don't think Scuzzlebutt is scary huh? Let's see how they like it when they actually see Scuzzlebutt! I'll scare the hell out of them tomorrow!
Ned:
… Someone's crying my lord, Kumbayah.
Moh lord, Kumbayah.

[Next morning. A rooster crows]
Kyle:	Stan, Stan, wake up!
Stan:	What, dude?
Kyle:	I don't know where Cartman is. I think something took him away.
Stan:	Where's my Uncle Jimbo and Ned?
Kyle:	They're out fishing with Kenny.
Stan:	With Kenny? But, but this is supposed to be my camping trip. Why do they like Kenny so much? Doesn't he like me anymore?
Kyle:	Well Stan, you want to know what I think?
Stan:	What? [Kyle farts. Stan does nothing]

[In the fishing boat]
Jimbo:	What a beautiful morning for fishing. There's one, there's a fish right there! [throws grenade, killing a fish.]
Ned:	Mmm, got it. [Kenny throws grenade, killing four fish.]
Jimbo:	Great instincts boy.
Stan:	[runs to the side of the lake] Uncle Jimbo, Cartman's missing!
Jimbo:	Who? The fat kid?
Stan:	Yeah.
Jimbo:	Ah hell, I guess we better go look for him. Ned, we got to cut it short. Fire out the 12-20! [Ned fires the load and kills all the fish in the lake. The boat lands a second later] Well, I think that's about the limit for our fishing permit.
Ned:	[rowing to shore] Moh man it smells like dead fish here.
Kenny:	(A little like a vagina.)
Ned:	Em-moh man, that is nasty.
Jimbo:	Heh, I don't think I've ever seen a kid as cool as you Kenny. I'm making you... my honorary nephew.
Kenny:	(Thanks.) [Stan's head drops]

[In front of city hall. The media is now present]
Newscaster:	The people of South Park are humble and friendly. But now, a ticking timebomb of hot lava waits to engulf these people and end their miserable lives with one last fleeting moment of excruciatingly painful burning agony.
Crowd:	Yeahhh. [they sound more excited than agonized]
Individual in crowd:	Hey, I'm on TV! I'm on TV!
Newscaster:	Mayor, what are you doing to prepare for this inevitable catastrophe?
Mayor:	All we know right now is that some of our children [sob-sob] are camping on that mountain and... Oh, I'm sorry, can I start over?
Newscaster:	Huh?
Mayor:	You can edit this right? Ready, 3,2,1 [Melodramatically] All we know right now is that some of our children are up camping on that mountain. We can't do anything until we get them. Okay people, let's go get those kids.
Chef:	Come on everybody. You got to help the children.

[On the mountain]
Jimbo:	Uh, well, he couldn't have gone far, unless something drug him off.
Ned:	There's not many animals out today, Jimbo, mmm.
Jimbo:	Yeah, it's almost like something funny's going on. [The volcano rumbles.] Christ, Ned, what'd you have for breakfast.
Ned:	Mm I don't know man. I've got some bad gas.
Jimbo:	Wait, there's a ram! It's coming right for us! [Fires at the ram. Kenny takes over] Nice shootin' Kenny. Here, you need a bigger gun.
Kyle:	Look!
Cartman:	I am Scuzzlebutt, Lord of the Mountains. Behold my Patrick Duffy leg.
Ned:	Mmm, what is it?
Kyle:	Dude, it's Scuzzlebutt! Cartman wasn't lying.
Jimbo:	Holy crow! We could make a mint killing this thing.
Ned:	We'll be on the cover of Guns & Ammo.
Jimbo:	This calls for some HJ-14.
Cartman:	Heheh. Those guys are totally scared.
Jimbo:	[armed with twin shoulder rockets] Fire in the hole!
Cartman:	Holy crap! [jumps out of the way. The rockets hit the mound and debris goes in all directions]
Jimbo:	Damn it, I think I missed
Cartman:	What the hell is wrong with you people?!
Jimbo:	C'mon, let's move, move!
Cartman:	[sees them coming and backs away] Hey, wait, aah!

[Rescue Center]
Mayor:	Is, is it on? Okay. Okay people, form groups and search the mountain. Report back here every hour! You got that?!
Randy:	Mayor, I might have an idea.
Mayor:	Huh, what?
Randy:	If we can dig a very large trench, we can divert the lava into a canyon, and then it would bypass South Park, pretty much completely.
Mayor:	And, that would be good? Alright?
Randy:	Uh - pretty sure.
Mayor:	Well, what are we waiting for. Okay, people, change of plans! Half of you grab shovels!

[On the moutain. Jimbo and the others have lost sight of Cartman]
Jimbo:	These look like its tracks. He must have gone this way. Ned, prepare some HK-12 and some plasticine. I'll bet that sucker's headed for a higher elevation. The higher up it- BIRD!! [shoots a bald eagle] The higher up it goes, the better it can breathe.
Kyle:	[sees Cartman] Look, up there.
Cartman:	You guys, it's just me.
Jimbo:	Are we sure it's Scuzzlebutt?
Stan:	Does it have Patrick Duffy for a leg
Jimbo:	I can't tell; let's kill it!
Cartman:	Eh, I gotta get out of this stupid costume.
Jimbo:	Kenny, you take the front.
Stan:	No! [taking the rifle from Kenny] I can do it Uncle Jimbo. I want to bag that animal!
Jimbo:	That's the spirit, kiddo. Let's hunt!
Cartman:	You guys, you guys, I was just kidding!

[Townspeople digging trench]
Newscaster:	As some people of South Park try desperately to save their mountain town, others look for the missing townspeople. But all must take every precaution necessary.

[Rescue Center]
Officer Barbrady:	Okay people, listen up. As we near the top of the mountain, the chances of our encountering some lava becomes great. Therefore, I have special-ordered this training film to assist us in volcano safety. Mr. Garrison, if you would please. [Mr. Garrison starts the film]
Host:	Harbringers of sorrow, natural disasters can be the cause of troubling and undesirable stress, and the volcano is no exception. But what should you do if a volcano erupts near you or your family? Here we see the Stevens family enjoying a May picnic, but suddenly daughter hears a noise. It's a volcano. Junior seems worried. But have no fear Junior, Jane learned in school what to do when you hear a volcano erupt. [Jane backs up, pulls out the blanket from under the food, and throws it over her family] That's right Jane, duck and cover. [She ducks under the blanket herself. The lava rolls over the family, causing no harm. The blanket flies off, revealing a sparkling family] So what will you-do when you hear a volcano erupting? [Two boys on bikes look alarmed. They leap off their bikes and hit the street, duck, and cover their heads with their arms] That's right, duck and cover. [The lava rolls over the two, causing no harm] Looks like you got the idea.
Thank you and goodbye.
Officer Barbrady:	OK, any questions?
Chef:	That has got to be the most ridiculous load of pig crap I have ever seen!
Officer Barbrady:	That's enough out of you!

[On the mountain. Cartman moves further up on the slope and turns around]
Stan:	I'm gonna bag Scuzzlebutt. Then we'll see who's the little bastard.
Cartman:	Hey, seriously you guys!
Kyle:	[hushed] Kill it, Stanley. Kill it. [normally] Come on Stan, kill it.
Stan:	Ah, damn it, I can't do it!
Jimbo:	[swatting Stan behind the head] You pansy! give me that gun.
Cartman:	Hey!
Stan:	Cartman?
Cartman:	Goddammit, don't shoot me!
Jimbo:	What in Sam Hell?
Cartman:	I was just trying to scare you guys, you can put the guns down now.
Ned:	Mmm, so much for the cover of Guns & Ammo.
Jimbo:	Yepper, but I think we've learned some important lessons Ned I think that- [Boom! Lava begins to bubble out and flow]
Kyle:	Whoa!
Ned:	Mmm, holy crap.
Jimbo:	The mountain! It's blown it's top! [A boulder of lava lands in front of Kenny]
Kyle:	Oh my God, they killed Kenny!
Kenny:	[steps out from behind the boulder] (Nope, I'm okay.) [see his arm on fire] (It-aah. It hurts! IT HURTS!) [The boulder rolls onto Kenny] (Ugh.)

[On the side of the mountain]
Townsman 1:	Look, the volcano.
Townsman 2:	Quick, duck and cover. [They do. The lava sweeps them down the mountainside, burnt to the bone]

[At the base of the mountain. Jimbo and the others are moving quickly]
Kyle, Stan, Cartman:	Aaaaah!
Jimbo:	That lava's coming right for us!! [behind them a river of lava charges down the mountain]

[Looking on from the edge of town]
Chef:	Oh no, look.

[At the trench. Jimbo and the others have made it out of the mountain, but their way into town has been cut off]
Jimbo:	What the hell is this trench doing here?!? We can't get across!
Newscaster:	It now looks as if the missing children are trapped in the path of hot, nasty lava.
Mayor:	God, please deliver those darling kids from... Wait, wait wait wait. 3,2, and 1. God, please deliv-
Cartman:	Help! [Scuzzlebutt appears and scratches his belly]
Jimbo:	Jimminy Hope, it's the real Scuzzlebutt!
Cartman:	What?! Scuzzlebutt's real?!
Kyle:	Oh my God! Look at his leg!
Patrick Duffy:	Hi kids, I'm TV's Patrick Duffy.
Jimbo:	Quick Ned, shoot it!
Ned:	[aims, but no shot is heard] Mmmoh no, out of ammo.
Scuzzlebutt:	Grrrr.
Mayor:	What... is that thing?
Chef:	That's Scuzzlebutt.
Mr. Garrison:	Yeah, he has Patrick Duffy for a leg, and weaves baskets.
Officer Barbrady:	This isn't happening. Everyone look away please. Nothing to see here.
Jimbo:	Well boys, I'm sorry I got you all killed. [Scuzzlebutt rips a tree from the ground, bites off a limb...]
Kyle:	Aaaaah! [...and molds the tree into a-]
Stan:	Whoa, he built a wicker basket
Jimbo:	Hey, he's saving us.
Scuzzlebutt:	[puts the group in the basket and lifts them over the trench] Grrr.
Liane:	Scuzzlebutt saved the day.
Randy:	And my calculations worked: the lava is following the trench into the canyon.
Mayor:	Hmmm. Where exactly does the canyon go?
Randy:	Uhh...
Denver citizens:	Aaaaah.

[South Park. The ordeal is over]
Mr. Garrison:	South Park is saved. [Cheers go up]
Kyle:	Hey look, Kenny's okay.
Kenny:	[appearing on a mound nearby] (Hey guys, come here.)
Newscaster:	And now these humble people can rejoice and celebrate their jovial victory over nature. I'm getting word that the chef of the school cafeteria wants to sing a song about this thrilling struggle of humanity.
Chef:
Mmmm
Baby, every time that we kiss
Hot Lava!
Every time that we make love, hot lava
Hot Lava!
Lava so hot it makes me sweat
Lava so warm and red and wet
Lava!
Brrrrrrrrrrrr.
Newscaster:	Mayor, what do you have to say about this wonderful outcome?
Mayor:	Scuzzlebutt.
Scuzzlebutt:	Friend. [presents flowers to the Mayor]
Mayor:	Oh, how sweet.
Stan:	[shoots Scuzzlebutt] I did it, I did it, I finally killed something. [smiles, proud of himself. Unfortunately, this took place during a live report]
Newscaster:	Oh my God! What has he done?
Mayor:	Turn off the cameras!
Stan:	Hey, that was easy!
Patrick Duffy:	Noooo! Why God? Why?!
Jimbo:	Damn it Stan, you shouldn't have done that!
Stan:	What?! Why?!
Kyle:	Yeah, make up your mind, dude!
Jimbo:	Stan, some things you kill, and some things you don't. See?
Stan:	No.
Ned:	Moh, only now in this late hour do I see the folly of guns. Mmm, I'll never use a gun again, mmm. [Drops his gun, which fires one last shot as it hits the snow. Kenny is hit]
Kenny:	(Oh, no!) [Rats are hot on the trail]
Stan:	But I just wanted you to be proud of me, like you were with Kenny.
Jimbo:	But Kenny's dead now, Stan, and you're always going to be my nephew. And you can't just kill anything. You understand?
Kyle:	Dude, I don't understand hunting at all.
Stan:	Yeah, [drops his gun] it's stupid. Let's go watch cartoons.
Cartman:	Yeah, cartoons kick ass! [all three leave]