Romanization
One, two, three, four
Eonjebuteoinji nado al suneun eopjiman
Seongtanjeore nuni oji anki shijakaesseo
Maenyeon geuttaega dwemyeon urin gidaehaji
Eorinaigachi seolleineun maeumeuro
Ilgiyeboeseodo nuni ol kkeora hajiman
Beolsseo myeot nyeonjae teullineunji jeongmal moreugesseo
Sesang saramdeul moduga barago isseul kkeoya
Olhae seongtanjeoreneun hayan nuneul bol kkeorago
Eojjeomyeon an oljido molla
Neomu jobashimnaehamyeon haneureun
Chakage saneun goseman
Chukbogeul naeryeo julkkeora haji
Chang bakkeul bwa nuni wa
Geureoke gidarideon hayan nuni wa
On sesangeul hayake deopeojuneun hayan nuni wa
Eonjebuteoinji nado al suneun eopjiman
Seongtanjeore nuni oji anki shijakaesseo
Sesangsaramdeul moduga barago isseul kkeoya
Olhae seongtanjeoreneun hayan nuneul bol kkeorago

[Lee/Kyu] eojjeomyeon an oljido molla

[Lee/Kyu] neomu jobashimnaehamyeon haneureun

[Shin/Sung] chakage saneun goseman

[Shin/Sung] chukbogeul naeryeo julkkeora haji
Chang bakkeul bwa nuni wa
Geureoke gidarideon hayan nuni wa
On sesangeul hayake deopeojuneun hayan nuni wa
Sangsanghada boni modeun ge
Eoneusaenga hyeonshiri dwae
Santa harabeoji oshineun
Giren nuni naeryeoya dwae
Pomnage jjuk mikkeureojige
Sseolmae kkeureojullae
Rudolpeu saseumko OK
Iwang ol ttae seonmureun
Myeongpumeuro eottae
Maeil nan sangsangeuro bamsae
Manjokhae ireoke gidarigo gidarideon
Hin nunman naeryeojumyeon OK
Chang bakkeul bwa nuni wa
Geureoke gidarideon hayan nuni wa
On sesangeul hayake deopeojuneun hayan nuni wa
Chang bakkeul bwa (chang bakkeul bwa)
Geureoke gidarideon hayan nuni wa
Jigeum wa on sesangeul (on sesangeul)
Hayake deopeojuneun hayan nuni wa
Hangul
One, two, three, four
언제부터인지 나도 알 수는 없지만
성탄절에 눈이 오지 않기 시작했어
매년 그때가 되면 우린 기대하지
어린아이같이 설레이는 마음으로
일기예보에서도 눈이 올 거라 하지만
벌써 몇 년째 틀리는지 정말 모르겠어
세상 사람들 모두가 바라고 있을 거야
올해 성탄절에는 하얀 눈을 볼 거라고
어쩌면 안 올지도 몰라
너무 조바심내하면 하늘은
착하게 사는 곳에만
축복을 내려 줄거라 하지
창 밖을 봐 눈이 와
그렇게 기다리던 하얀 눈이 와
온 세상을 하얗게 덮어주는 하얀 눈이 와
언제부터인지 나도 알 수는 없지만
성탄절에 눈이 오지 않기 시작했어
세상사람들 모두가 바라고 있을 거야
올해 성탄절에는 하얀 눈을 볼 거라고

[이/규] 어쩌면 안 올지도 몰라

[이/규] 너무 조바심내하면 하늘은

[신/성] 착하게 사는 곳에만

[신/성] 축복을 내려 줄거라 하지
창 밖을 봐 눈이 와
그렇게 기다리던 하얀 눈이 와
온 세상을 하얗게 덮어주는 하얀 눈이 와
상상하다 보니 모든 게
어느샌가 현실이 돼
산타 할아버지
오시는 길엔 눈이 내려야 돼
폼나게 쭉 미끄러지게
썰매 끌어줄래
루돌프 사슴코 OK
이왕 올 때 선물은
명품으로 어때
매일 난 상상으로 밤새
만족해 이렇게 기다리고 기다리던
흰 눈만 내려주면 OK
창 밖을 봐 눈이 와
그렇게 기다리던 하얀 눈이 와
온 세상을 하얗게 덮어주는 하얀 눈이 와
창 밖을 봐 (창 밖을 봐)
그렇게 기다리던 하얀 눈이 와
지금 와 온 세상을 (온 세상을)
하얗게 덮어주는 하얀 눈이 와
English
One, two, three, four
I don’t know since when but
It started not to snow on Christmas
Every time that time of year comes around
We are so excited, just like kids
The weather report says it will snow
But it’s been wrong for so many years
The whole world will be expecting it
Expecting snow on Christmas Day
It might not snow
If we worry too much
The skies only bless places
That have good people
Look out the window, it’s snowing
The white snow that we’ve all been waiting for is coming
The white snow that is covering the whole world is coming
I don’t know since when but
It started not to snow on Christmas
The whole world will be expecting it
Expecting snow on Christmas Day
It might not snow
If we worry too much
The skies only bless places
That have good people
Look out the window, it’s snowing
The white snow that we’ve all been waiting for is coming
The white snow that is covering the whole world is coming
After imagining it
Everything became reality
The path that Santa will take
Needs to have snow
Will you pull my sled
So that it will
Smoothly glide, Rudolph, OK
Now that you’re coming
Why not bring some designer brand presents?
I spend all night imagining
I’ll be satisfied
If the long-awaited snow falls, OK
Look out the window, it’s snowing
The white snow that we’ve all been waiting for is coming
The white snow that is covering the whole world is coming
Look out the window, it’s snowing
The white snow that we’ve all been waiting for is coming
The white snow that is covering
The whole world is coming