[Verse 1]
So what, what do I do?
How to move on
How to forget about you?
You gave me your word
Get up off your knees
Stop begging me please

[Chorus]
Well, tell it to my heart
Cause it's trashed once again
And your sorry's cannot stop
All the hurt, all the pain

[Verse 2]
If I meant so much to you
Why would you risk
All that we had, all we've made?
I don't know who you are
Just saying you're wrong
Does not change a thing, not anything

[Chorus 2]
Well, tell it to my heart
Cause it's smashed up again
Oh, your sorry's cannot stop
All the hurt, all the pain
No, no, no

[Bridge]
And I don't wanna hear
These excuses again
This life that we live
Is not a game

[Chorus 3]
Well tell it to my heart
Cause you trashed it again
Oh yes you
And your sorry's cannot stop
All the hurt and the pain
No, no, no

[Chorus 2]
Well tell it to my heart
Cause you smashed it again
Yes you did
And your sorry's cannot stop
All the hurt and the pain
No, no, no