"Perseas' love for Andromeda, their wedding ceremony and the unexpected surprises..."
He who goes to seek the Gorgon is unlikely to return
His mother will cry in sorrow in a funeral he will burn
But he is the one exception hero like the burning sun
Oh Persea Eurymedon be the ruler of the land
Oh the beast has fallen back to where it belongs
And I see a princess tied in chains to a rock
I call upon the Northern Wind, the Nereids and The Venus Queen
To ask the king for her hand in marriage
And though it wasn't meant to be, it seems that it's our destiny
To have her in my chambers before sunset
She is the healer of my forsaken soul
Cause you are the princess of my life
The queen of my everlasting toll
How can you save me?
From olden times, for you I will die young
But Phineus he came along
To interrupt our wedding song
And Cepheus what a treachery, he vanished
And now what does that stupid fool want
To give up all I ever wanted?
I swear that he will dwell in the death silence
I take the serpent head and turn them into stone
The falling rain, the fall with the flame
Oh King Polydectes now its your turn....