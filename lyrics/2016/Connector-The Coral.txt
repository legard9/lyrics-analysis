[Verse 1]
You are not the only one
To open up your heart
We come together then we come apart
I am not the plans you made
That lie broken in two
I am just the reflection of you

[Chorus]
I'm the connector, you're the receiver
You're the projector, I'm the believer

[Verse 2]
You danced out the window like
A child drawn to a flame
Bow down to the masters of your shame
I am but the vision of
The image that you see
I am here and you are here in me

[Chorus]
I'm the connector, you're the receiver
You're the projector, I'm the believer

[Instrumental]

[Verse 3]
You are like the ghost that I have
Passed along the way
An illusion of a memory that fades
I am like the wish you dropped
Into the slide machine
Disappeared behind the coloured screen

[Chorus]
I'm the connector, you're the receiver
You're the projector, I'm the believer