Roger Creager

Sometimes I sit in my backyard, I kick back and drink iced tea
I stare all the way to the ocean and dream of how it would be
If I were a ship captain somewhere or an old fashioned sailor at sea
Or a stowed away pirate just down below hiding and praying the law dont find me
Maybe I'd sail from Nantucket chasing the great white whale
Oh without a sound, I'd run him aground then I'd bring old Ahab the tail
Yeah I'd bring old Ahab his tail

Chorus:
But I'm not a (sailor, outlaw, runaway)
I'm just a man stuck here in the promise land
Living hard and living free
I'm a dreamer that's what I got, Oh but here goes one last shot
I hope someday they're dreaming about me

Sometimes I dream Im a cowboy around 1949
Id cross the border on horseback with a real close buddy of mine
Id know we'd run from trouble but Im sure its what we'd find
When you're out of the frying pan into the fire, who cares what you leave behind
I know i'd fall in love down there and I'd probably end up in jail
When you fall in love with a rich man's daughter, 'who's gonna go your bail
Ohhh i hate them Mexican jails---

Chorus

I'd love to go rafting the waters. riding the mighty Mississippi
I'd float around from town to town causing trouble then I'd give'em the slip
That water could take me back to the days of old Huck Finn
I'd sleep all day and smoke all night and play tricks on old Jim
Yeah I'd think I'd like old Jim