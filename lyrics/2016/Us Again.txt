*Out of "Vocab! prélude"*

[Intro]
Hocus Pocus in the place to be
Procussions... check it out...
One time... T-Love

[Couplet 1 : 20Syl]
J'voudrais qu'mes mots sonnent comme Check it out !
Que quand t'écoutes tu dises You know what it's all about ou bien No doubt
Ces phases qui claquent sur basse qui t'bloquent au sol
Kickent fat, qui craquent et niquent la console (Come on !)
Ma langue a l'histoire, les racines et le sens
La leur a les stars, le fashion et le slang (No shit on me)
Putain d'accent, j'veux l'même dans mes textes
Mais quand j'rappe cain-ri, tout l'monde me dit Who's next ?
Ils me disent tous: " Le yaourt, abandonne "
Alors, en scred, j'fais tourner la MPC et j'improvise Danone
Sur mon coin d'hexagone, j'examine la donne
Entre Shakespeare, Stallone, j'expire, tâtonne
Pour trouver mon truc, assumer mon langage
Même si chez moi les "R" sonnent et que eux les mâchent
On a leurs WC, leurs sandwiches et leurs chewing-gums verts
Et eux ont notre savoir-faire

[Refrain]
J'veux pas d'tes "Fuck !", pas d'tes "Shit !", pas d'tes "Motherfucker !"
J'veux pas d'tes "Nigga !", pas d'tes "Bitch !", pas d'tes "Sucker !"
J'veux pas d'tes "Fuck !", pas d'tes "Shit !", pas d'tes "Motherfucker !"
J'veux pas d'tes "Nigga !", pas d'tes "Bitch !", pas d'tes "Sucker !"

[Couplet 2 : Mr. J. Medeiros]
Know what I'm asking for ? Maximum de bruit !
I'm actually asking if you see a passionate MC
I'm past masking who I be, I paid my dues
It's like: Tout le monde dit: Yeah !
If they're playing y'all like déjà-vu
I'm sayin', dude, they givin us the same one liners
Money, drugs, guns, honeys, thugs mining for gold, cash
Diamonds in the ears of dead soldiers
Yo, I'm holding fast to rhyming for the years they left over

[Couplet 3 : T-Love]
Non, oui-oui, be still mon coeur
Sometimes when I be chilling I can understand a word
Or two, maybe three, good day six
But most of the time, French rhyme shines in bits
Many times it's the point that I miss
But if I like it, I bump it but I won't know the gist
I'll humalong a riff mais j'sais pas c'est quoi
I never know the meaning but the feeling be raw

[Couplet 4 : Stro the 89th Key]
Yo, on the regular, not a church girl she was secular
Not about the money, somebody redirected her
I know I met her at a earlier age
But she don't look the same way on a page
Got a brother confused and I ain't willing to lose
You can believe me a fool and it's cool
Cause after all, it's music, I tell her
Je ne parle pas français
But when the rhythm is cool
It doesn't matter if together we move, come on

[Refrain]
J'veux pas d'tes "Fuck !", pas d'tes "Shit !", pas d'tes "Motherfucker !"
J'veux pas d'tes "Nigga !", pas d'tes "Bitch !", pas d'tes "Sucker !"
J'veux pas d'tes "Fuck !", pas d'tes "Shit !", pas d'tes "Motherfucker !"
J'veux pas d'tes "Nigga !", pas d'tes "Bitch !", pas d'tes "Sucker !"

[Couplet 5 : 20Syl]
J'leur ai piqué des mots de ci de là
Et mon champ lexical vaut pas plus d'6 dollars
J'me prends pas pour J. Dilla, loin d'là
Je reste fan, bref, j'idolâtre plein de gars (One love)
Parce que trop d'classiques me mettent presque en transe
Mais parfois j'tombe de haut quand j'en saisis le sens
On s'moque du fond tant qu'on atteint la cible
Résultat : plus de bips que de mots intelligibles:
Let's **** that motherf***** in the ***, Pardon ?
Once again ? Nan, nan, nan, passons...
J'aime ma langue natale mais j'dirai fatalement
Qu'elle a la musicalité d'un régiment allemand
Je suis né ici, pas à N.Y.C et parfois plutôt fier
Quand j'arrive à faire chanter c'foutu vocabulaire
On a leurs sodas, le best of du business de Bush fils
Et eux ont notre French kiss

[Refrain]
J'veux pas d'tes "Fuck !", pas d'tes "Shit !", pas d'tes "Motherfucker !"
J'veux pas d'tes "Nigga !", pas d'tes "Bitch !", pas d'tes "Sucker !"
J'veux pas d'tes "Fuck !", pas d'tes "Shit !", pas d'tes "Motherfucker !"
J'veux pas d'tes "Nigga !", pas d'tes "Bitch !", pas d'tes "Sucker !"

[Outro]
I like your Peace, like your Unity, like your Love
I like your Jazz, like your Soul, like your Hip-Hop
I like your Peace, like your Unity, like your Love
I like your Jazz, like your Soul, like your Hip-Hop