[Verse 1]
Money rains from the sky above
But keep the change 'cause I've got enough
A little time and some tenderness
You'll never buy my love
No other thing that's as precious to
No other! There's no other!
Than a heart that's real and a heart that's true
Something that you got to know

[Chorus]
This girl don't need diamonds so bright
This girl don't need money, all right
This girl just got to hold me all night
You'll never buy my love

[Verse]
These treasures don't really come for free
Your paychecks don't mean that much to me
Just take my hand and hold me tight
You'll never buy my love
You buy me this and you buy me that
To win over! Win me over!
You got me wrong and that's a fact
Something that you got to know

[Chorus]
This girl don't need diamonds so bright
This girl don't need money, all right
This girl just got to hold me all night
You'll never buy my love

[Bridge]
Will you realise when I'm gone
That I dance to a different song
It's a shame but I've got to go
Something that you never know

[Chorus]
This girl don't need diamonds so bright
This girl don't need money, all right
This girl just got to hold me all night
You'll never buy my love

[Chorus]
This girl don't need diamonds so bright
This girl don't need money, all right
This girl just got to hold me all night
You'll never buy my love

[Outro]
...don't need diamonds so bright
...don't need money, all right
...just got to hold me all night
You'll never buy my love