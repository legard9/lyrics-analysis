[Intro]
Get it lifted
Whoop, whoop, whoop, whoop
Fire truck

[Verse 1]
어딜 봐 Mr. Fireman on the floor (On the floor)
불 지펴봐 이 열기를 식혀줄게 (Hot)
불만이 가득찬 것 더 화끈해줘
저 사이렌소리에 끼는 내 기분은
Mm, 애써빼는 척은 곤란해 (곤란해)
미지근한 분위기는 no thanks (No thanks)
흔들리는 내 달궈진 backseat (Backseat)
태운 뒤 빈틈없이, we get lit (We get lit)

[Refrain]
Ayy, ayy, ayy, ayy-ayy
Hands up if you feeling the vibe now
Ayy, ayy, ayy, ayy-ayy
One step, two steps

[Pre-Chorus]
오늘밤 너와 나 모두 다 빠져들지
언제든 달려가 소방차, you can call me
몸을 움직여, pick it up
리듬 느끼는대로 흔들어
뜨거워지는 순간 크게 소리쳐

[Chorus]
Whoop, whoop, whoop, whoop
Fire truck
Whoop, whoop
Whoop, whoop, whoop, whoop

[Verse 2]
Yeah
Be anywhere, everywhere, 부르기만 하면 돼
Look at, look at how 그냥 불장난같애
Blink하면 나타나, just blink for me
시원하게 니 화를 풀려줄테니
Just hold up
자꾸만 밀면 다쳐, get higher
열기는 마치 처음 뜨거워
이 공간은 폭발하기 십초전 (Ah!)

[Refrain]
Ayy, ayy, ayy, ayy-ayy
Hands up if you feeling the vibe now
Ayy, ayy, ayy, ayy-ayy
One step, two steps

[Pre-Chorus]
짜릿한 이 음악 니 맘 확 불태울까지
외쳐봐 소방차 어디든 달려가지
Maximum으로 turn it up
리듬에 맞춰 흔들어
전율 느끼는 순간 크게 소리쳐

[Chorus]
Whoop, whoop, whoop, whoop
Fire truck
(Ayy, ayy, ayy, ayy, ayy, ayy, ayy)
Whoop, whoop
(Ayy, ayy, ayy, ayy, ayy, ayy)
Whoop, whoop, whoop, whoop
Fire truck
Whoop, whoop
Whoop, whoop, whoop, whoop
Fire truck

[Bridge]
멈추지 마 밤이 새도록
Maximum으로 turn it up
(Maximum으로 turn it up)
Alright, 이 음악속에 모든 걸 던져
Dance, my party people

[Outro]
Whoop, whoop, whoop, whoop
소방차, 소방차, 소방차, 소방차
Whoop, whoop, whoop, whoop
소방차, 소방차