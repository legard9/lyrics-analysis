[Verse 1]
I wanna see what you see in me
And never let you down
Can you still feel my love?
I walked away from the piece of me
A dying ghost in an old machine
Oh please, don't cry, my love

[Pre-Chorus]
I was a blind man chasing shadows
It was a cold hair man I'd known
Wherever you go I will follow
Like an orphan running home

[Chorus]
You're the flood, you're the flood, you're the flood that opened my heart
You're the flood, you're the flood, you're the flood that opened my heart
You're the flood, you're the flood, you're the flood that opened my heart
You're the flood, you're the flood, you're the flood that opened my heart

[Verse 2]
I had a dream you were next to me
I woke up calling your name
Can you still feel my love?
I wanna be something you can touch
You're moving fast, but I'm catching up
No don't slow down, my love

[Pre-Chorus]
I was a blind man chasing shadows
It was a cold hair man I'd known
Wherever you go I will follow
Like an orphan running home

[Chorus]
You're the flood, you're the flood, you're the flood that opened my heart
You're the flood, you're the flood, you're the flood that opened my heart
You're the flood, you're the flood, you're the flood that opened my heart
You're the flood, you're the flood, you're the flood that opened my heart

[Bridge]
Let the water rise
Let the water rise
Let the water take me under
Let the water rise
Let the water rise
Let the water take me under
I believe in the flood you opened
I believe it can save me now
I believe when the door was closing
You broke it down
You broke it down

[Chorus]
You're the flood, you're the flood, you're the flood that opened my heart
You're the flood, you're the flood, you're the flood that opened my heart
You're the flood, you're the flood, you're the flood that opened my heart
You're the flood, you're the flood, you're the flood that opened my heart
You're the flood, you're the flood, you're the flood that opened my heart