I held it in close like a cigarette
Tucked in the folds of her pretty lips
Keep the shit together
Gonna make it stick
In the heart of a Saturday night
Keep the records spinning with them slow sad songs
Me, the boys, we're drinking and we'll sing along
Keep the drinks coming and we'll go 'til dawn with her
Honey, with her
Because if you call me
I would answer
Won't you take my body
Before the cancer does
We can dance along the light of day
Tread fast and make these memories
Of the way it felt before we both got old
Yeah, before we both got old
So I tried to save some leather for the walkin 'home
There's nowhere left to leave it but all alone
Try to find something I can call my own tonight
But I got nothing left but all the slow sad songs
My grandad used to sing them now I sing along
Try to find me something I can bring back home for her
Honey, for her
Because if you call me
I would answer
Won't you take my body before the cancer does
We could dance along the light of day
Tread fast and make these memories
Of the way it felt before we both got old
'Cause you am I are doomed to die
Yeah well both get old
But not tonight
So hit it, boys
Because if you call me
I would answer
Won't you take my body before the cancer does
We could dance along the light of day
Tread fast and make these memories
Of the way it felt before we both got old