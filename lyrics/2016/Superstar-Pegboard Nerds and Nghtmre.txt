[Verse]
You're the superstar above me when the sun goes down
The one and only who could light this town
And when you're feeling like you're burning out
I will remind you

[Chorus]
That you're a superstar and you don't need a golden crown
'Cause everybody needs a hero now
And when the weight is like a million pounds
I'll stand behind you
'Cause you're a superstar, a superstar
'Cause you're a superstar, a superstar
'Cause you're a superstar, a superstar
'Cause you're a superstar, a superstar

[Chorus]
You're the superstar and you don't need a golden crown
'Cause everybody needs a hero now
And when the weight is like a million pounds
I'll stand behind you
'Cause you're a superstar, a superstar
'Cause you're a superstar, a superstar
'Cause you're a superstar, a superstar
'Cause you're a superstar, a superstar

[Chorus]
I will remind you
That you're a superstar and you don't need a golden crown
'Cause everybody needs a hero now
And when the weight is like a million pounds
I'll stand behind you
'Cause you're a superstar, a superstar
'Cause you're a superstar, a superstar