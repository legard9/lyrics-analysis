(Bear goddess of wild life and a protector)
Robouas uet magnei
Noue pepoisas criðiion imon
Noue pepoisas geliiin
Supritiia tiresos sondi
Noue pepoisas abalon
Blatus suadiius areuoclouiuspe
Noue pepoisas clounis Nantaroriias
Blatusagiiet samali sepont
A boua uer magnei
Etic pepoisa criðiion tuon
A pepoisa geliiin
Supritiia tiresos tui
A pepoisa Brenoduron senon
Uolugatus suadiius geliiuspe
Etic pepoisa criðiion ansron
Etic blatusagiiet samali sepont
Robouas uet magnei
Noue pepoisas criðiion imon
Noue pepoisas geliiin
Supritiia tiresos sondi
Noue pepoisas abalon
Blatus suadiius areuoclouiuspe
Noue pepoisas clounis Nantaroriias
Blatusagiiet samali sepont

[English Translation]
Were you at the rock
Or did you see my love
Or did you see a brightness
The beauty of this land
Or did you see the apple
The sweetest and most fragant blossom
Or did you see the meadows of Nantarora
Are they blossoming as they say?
Oh I was at the rock
And I saw your love
Oh I saw your brightness
The beauty of your land
Oh I did see the old town of Brenoduron
The sweetest and brightest beacon
And I saw our love
And she is blossoming as they say
Were you at the rock
Or did you see my love
Or did you see a brightness
The beauty of this land
Or did you see the apple
The sweetest and most fragant blossom
Or did you see the meadows of Nantarora
Are they blossoming as they say?