[Verse]
Salt is in the water
Your heavy body sink
Tides will pull me over
But never let you in
Salt is in the water
Your ocean with no end
Meet my rivers sweeter
But our waters won't ever

[Chorus]
Blend
But our waters won't ever
Blend

[Verse]
Salt got in the river
Dynamic currents form
Differences however
Would make us bend and bounce
Salt is in your water
The currents wouldn't help
Knowing all your layers
I'll will never learn to swim in

[Chorus]
Them
I'll will never learn to swim in
Them

[Bridge]
Pressure of light
The moon pulls heavy
All night
Tugging on strings
Gently but strong

[Outro]
But our waters won't blend (Oo-oo-oo-oo-oo)
(Ba-pa-pa-pa-pa, pa-pa-pa-pa-pa-pa)
But our waters won't blend (Oo-oo-oo-oo)
(Ba-pa-pa-pa-pa, pa-pa-pa-pa-pa-pa)
But our waters won't blend (Oo-oo-oo-oo-oo)
Ba-pa-pa-pa-pa
Ba-pa-pa-pa-pa, pa-pa-pa-pa-pa-pa
Oo-oo-oo-oo, blend
Oo-oo-oo-oo, blend
Oo-oo-oo-oo, blend