[Verse 1]
South of your eyes
North of your chin
Yeah, I hear what you're saying
I'm taking it in
Every single twisted word is
All part of the dance
The sanctuary of broken bird
When nothing happens by chance

[Chorus]
I'm a liar too
Yeah, I'm a liar too
But then again
So are you

[Verse 2]
The story's told
In so many ways
It never really gets old but
Let's just see how it flies
There will never be a second first touch, no
Saturday night
Was all I really wanted, all I really asked for
Now I'm killing the light

[Chorus]
'Cause I'm a liar too
Yeah, 'cause I'm a liar too
But then again
So are you

[Bridge]
Don't go, please stay with me
Don't go, don't go
Please stay, please stay
Don't go, no
'Cause I'm a liar too
But then again
Oh, I'm a liar too
Don't go
Please stay, please stay
Don't go
'Cause I'm a liar too
Don't go
Please stay, please stay
'Cause I'm a liar too

[Outro]
But then again
So are you