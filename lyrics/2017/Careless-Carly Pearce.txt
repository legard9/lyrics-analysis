[Verse 1]
Don't call me
I've heard it all before the last time you said you were sorry
And all this back and forth is getting old
You're just the boy who cries love
Then you turn around and run
Yeah, you're the boy who cries love
You do it just for fun

[Chorus]
And baby, you were careless with my heart
Said forever then you let it fall apart
You left me crying there on the floor, walked out the door
Like it was nothing
And baby, you were reckless with my love
I gave you all I've got, you just gave it up
So it don't matter how much you've changed or the look on your face
When you say you want me, you miss me tonight
Baby, I could care less

[Verse 2]
And I know you
And all the empty breath of your promises
No, it ain't nothing new
The only thing different is me
'Cause you're just the boy who cries love
And I took you back every time
Yeah, you're the boy who cries love
Oh well, not tonight

[Chorus]
Baby, you were careless with my heart
Said forever then you let it fall apart
You left me crying there on the floor, walked out the door
Like it was nothing
And baby, you were reckless with my love
I gave you all I've got, you just gave it up
So it don't matter how much you've changed or the look on your face
When you say you want me, you miss me tonight
Yeah, baby, I could care less
Yeah, baby, I could care less

[Bridge]
I'm calm, cool, collected
Keeping my heart locked up, protected
Now all your begging and pleading is bouncing back to you
An echo in the cold hard truth
You're just the boy who cries love
You're just the boy who cries love
Well, I hope you have fun

[Corus]
'Cause baby, you were careless with my heart
Said forever then you let it fall apart
You left me crying there on the floor, walked out the door
Like it was nothing
Yeah, baby, you were reckless with my love
I gave you all I've got, you just gave it up
So it don't matter how much you've changed or the look on your face
When you say you want me, you miss me tonight
Yeah, baby, I could care less
Yeah, baby, I could care less

[Outro]
You're just the boy who cries love