[Intro]
Whoa nana na
Ooh ooh
Oh yeah
Yeah nana na
Ooh ooh
Oh

[Chorus]
I need a Gideon boot and a khaki suit
Fi stand out inna Babylon and represent the truth
Me nah go stay a road, and see bad things, keep mi mouth pon mute
I got a strong Reggae beat with a mic inna mi hand
Fi lead out the youths dem outta destruction
And tell the people they should know...right from wrong

[Verse 1]
We got the strength, we got the power...
To help each other up the lander
Don't be selfish to help your brothers today
We can make it if we try
Just remember to hold your head high
It doesn't make sense when we fuss and fight

[Chorus]
I need a Gideon boot and a khaki suit
Fi stand out inna Babylon and defend the truth
Me nah go sit aside, and watch Babylon, and keep mi mouth pon mute
I've got a strong Reggae beat with a mic inna mi hand
Fi lead out the youths dem outta destruction
And let the people know...right from wrong

[Hook]
How you believe in love...
Will get Jah blessing from above
How you believe in love
Ooh ooh ooh
How you believe in love...
Will get Jah blessing from above
How you believe in love
Ooh ooh ooh

[Verse 2]
Calling all the leaders of the world today
Yeah
Give a hand to the poorer class I said, yeah
Got to let the people know...the right from the wrong
They are depending on you to lead the way, yeah
People, just be wise
Believe in yourself
The future generation depends on you, hey
Show a little love in your heart
And let the people know there's more to life, mhm hm

[Chorus]
I need a Gideon boot and a khaki suit
Fi stand out inna Babylon and defend the truth
Me nah go sit aside, and watch Babylon, and keep mi mouth pon mute
I've got a strong Reggae beat with a mic inna mi hand
Fi lead out the youths dem outta destruction
And let the people know...right from wrong

[Hook]
How you believe in love...
Will get Jah blessing from above
How you believe in love
Ooh ooh ooh
How you believe in love...
Will get Jah blessing from above
How you believe in love
Ooh ooh ooh

[Verse 3]
Hey, think about the hungry, think about the naked ones
Hey, think about the fatherless and the motherless, ha
Think about the gun you send out coming back at you, woah yeah
Hey, think about the next generation to come, mhm hm hm

[Outro]
I need a Gideon boot and a khaki suit
Fi stand up inna Babylon and defend the truth
'Cause, right yah now, me nah go stand alone and keep mi mouth pon mute
I need a king Reggae beat with a mic inna mi hand
Fi lead out the - bumboclaat...