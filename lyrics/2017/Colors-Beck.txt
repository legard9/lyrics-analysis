[Verse 1]
I'll find you
And go right through
Walls we made, eh-ay
I see you
I need you
Every day
It's nothing
It's your life
I don't know why, eh-ay
I can't get
What I want
I keep, I keep trying

[Pre-Chorus]
Found our way through the lost years
Now the day brings it all here

[Chorus]
All the colors, see the colors, make the colors, feel the colors
She says
See it in your eyes
All the colors, see the colors, make the colors, feel the colors
Tell me, do you feel alive?
All the colors, see the colors, make the colors, feel the colors
She says
Nothing's in your eyes
All the colors, see the colors, make the colors, feel the colors
Tell me, do you feel alive?

[Post-Chorus]
Do you feel alive?
Do you feel alive?

[Verse 2]
I got all the love you need
I got all the love you need
I keep it with you
I don't have the time to wait
I don't have the time to wait
I need to see you
Got it all under control
Got it all under control
You can't hear me
Now it only hurts to know
Now it would only hurt to know
When you don't need me
You don't need me

[Pre-Chorus]
Found our way through the lost years
Now the day brings it all here

[Chorus]
All the colors, see the colors, make the colors, feel the colors
She says
See it in your eyes
All the colors, see the colors, make the colors, feel the colors
Tell me, do you feel alive?

[Post-Chorus]
Feel alive?
Do you feel alive?

[Bridge]
Na na na na na na na na
Na na na na na na na na
Na na na na na na na na
Na na na na

[Chorus]
All the colors, see the colors, make the colors, feel the colors
She says
See it in your eyes
All the colors, see the colors, make the colors, feel the colors
Tell me, do you feel alive?

All the colors, see the colors, make the colors, feel the colors
She says
Nothing's in your mind
All the colors, see the colors, make the colors, feel the colors
Tell me, do you feel alive?

[Post-Chorus]
Do you feel alive?