[Verse 1]
Looks like the sky is breaking open
Looks like the sky is falling down
Feels like fear is taking over
Is this the slipping of the crown
If we run for cover
Stick our heads in the ground
What is that to say for honor
How will that save us now

[Chorus]
Seems like everywhere we hear the warning
They all say a storm is coming
We are the storm, we are the storm
We can be the change that brings the lightning down
Be the thunder
We are the storm, we are the storm

[Verse 2]
If we gather all our raindrops
If we form into a wave
And take control of angry oceans
There is so much we could save
Be our own salvation
Be our own higher ground
If we recognize the power
That is what will save us now

[Chorus]
Seems like everywhere we hear the warning
They all say a storm is coming
We are the storm, we are the storm
We can be the change that brings the lightning down
Be the thunder
We are the storm, we are the storm

[Bridge]
Whenever the devil whispers
Whenever the devil whispers
Laugh in his face, laugh in his face (2x)

[Chorus]
We can be the change that brings the lightning down
Be the thunder
We are the storm, we are the storm