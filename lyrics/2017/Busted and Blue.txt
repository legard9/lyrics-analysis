[INTRO: Nib Roc]
Yo, this is Nib Roc
Answering to 
Crank Lucas's #AlphabetChallenge
But I'm about to 
do something really special
 with it
So check it. Here we go

[INTRO: Crank Lucas]
Crank Lucas!
Air Strike
, 
absolutely angry
 with no morals
I’m an 
aerial assassin
, 
and my aim is abnormal
One second, 
amateurs
 approach, and then I go amok
Ascending through the sky, afflicting ammo with my 
Avian bud
Antagonizing us is across the line of awful
Yes, acknowledge my awesomeness
 when I 
bust out a rhyme with blazing fire
Have the boombox burning with begazing desire
I bring the heat so you better go and back up
Before I blow my breezy flame breath and leave you bruised up
There’s no one bronzed and brighter
, I’m the blazer to beat
Bringing myself in the zone, becoming more of a beast
Chompy in the casa
, I’m the 
ceremony master
Crazy in the cranium, but a crafty commander
Undisputed champ of the chomp, creating crazes
Chompies go and reek havoc, I chuckle in cowards’ faces
Cool but not not cantankerous
, 
dang I’m more than dangerous
Dazzlin’ with my rangs and it’s done-zo for the knaves and ‘em
Call me a dragon and I’ll put you in a drastic daze
I damage dumb fools with nothing less of a disaster, 
mate
You better ding dong ditch your bad ways
Cuz I epically erupt, go ecstatic without effort
Do I think when I spew lava, it’s excessive? (Never ever!)
Everybody’s bound to know 
I’m not elegant or elusive
But when evil comes my way I get egregious and lose it
Emotional but easy-going
, I’m gonna make this eloquent
When it comes to Eruptor, no one’s more eager in my element
(Freeze!) It’s the fine feline
, fast and genuine
Faster than a flock of roadrunners makin’ a beeline
Never fumble the rock
, I’m a force to be reckoned with
A frenzy of fun with a certain fundamental wit
Fakers can flex, they’re full of foolery
, compared to me
I’m funky, unadulterated, 
fighting to the fullest peak
Gill’s
 gone wild with the guile and style to 
gut
Grumpy Greeble gangs
 and leave them all in a pile
No guts, no glory, I’m gilded with the greatest moral code
My will is ginormous, 
this guy is golden with the goal to go
Harder than a hypocrite, hotter than a microwave
Haunting haters left and right, a headless specter’s hay day
Hustlin’ heavily in a haze ain’t nothin’
But you don’t have to like it, ‘cuz the Hood surely loves it
No one can hex me
, I’m a havoc-serving hazard
The highest of my class, nobody’s hammered what I’ve mastered
Ignoramuses can get incinerated
I ignite, I’m the illest, always illuminating
I’ll inflict a lot of royal pain on you, don’t impeach me
Don’t say I didn’t inquire you when I give you an injury
Immune to the ignorance, Ignitor’s on a win streak
I’m iconic, you’re ironic, no fire spirit’s more iller
It’s just Jet-Vac
, 
jackin’ up jobbers with plain justice
Jammin’ to the jive, 
with the juice to leave ya busted
Your petty jabs won’t ever bring me down, I’ll just jounce back
No joke, lad
, I justify my skills and have ya jet black
And blue (Ooh!) when I play you like a jester
I’m the air element jock, simply no one does it better
Guess who’s kickin’ off the verse? it’s the bear-knuckle King Pen
I’m a killer combatant with not one kink in my arsenal
Click clack, nick, knack, knockin’ out my victims
Never such a thing as kryptonite in my kingdom
I’m ferocious like a kraken, you better use Hara-Kiri
Keepin’ it cool and frosty while my foes keep it dreary
Lighting up the sky with luminous liberal rapping
The Rod never spares, here’s a lesson for you lackeys
I’m liable, never lazy, laugh at lames who try to faze me
I’m a lord of striking lightning, no lice intimidate me
When I’m making a mystical mastery with my lyrics
A mystery’s nothin’ to me, a maestro when I do this
Mysticat is my moniker, rockin’ like Metallica
Mathematically no one’s equivalent to my caliber
A muse of magical prowess is anything but mythical
I’m massive toward the masses and my mad flow’s critical
Night Shift, never underestimate the numbers one-two
Nothing’s better than the nimble and naughty
Vampire in navy blue who’s gotta tussle with nincompoops
To nibble on necks and nuke the noobs like it’s nothing new
I’ll have you straight up on the nylons in a nanosec
Deckin’ nickle-worth, numbskulls and have ‘em chin-checked
Okay, it’s the overpowered, over-the-top
Obligated, operational dragon who never stops
Oath to keep the world strong with the strength of many oxen
Ain’t no ounce of quit in me and fighting is my only option
Often times I’m on point, overzealous to the max
Optic vision’s very clear when Spyro’s ought to attack
With punchlines so poisonous, potentially perfected
Potions with the pwnage to keep the power persisted
Pop Fizz, I’m a psycho, a pretty passionate fellow
All about the pizzazz and my belly’s far from yellow
I project, I’m a physician who is pumped to pummel evil
Pack a powerful punch, pierced to the point like a needle
Quiver and quake before the Queen of the gold
I qualify in the ranks and quiet those who oppose
Never in a quarrel ‘cuz I’ll win, they’ll go and quit too quick
I’m a quintessential quasar, there’s no question about it
Real vibes in real time with the extraordinary rhymes
Rip Tide, raw and wild, really reckless in my prime
Rockin’ and rejoicin’, remixin’ with a sick role
When I Whale on rooks, I leave ‘em startled like they’ve seen a Rickroll
Ready to rival rejects, ready to rumble ripoffs
Ready to raise havoc, I’m a rocket ready for liftoff
Spitfire, funny that my name is my succession
‘Cuz I scorch and sizzle posers, school ‘em all in this session
Literally a speed demon with the nerves and will of steel
Started as the star Supercharger, sparks from this spirit were real
I’m just a savage, I squash folks with my Hot Streak
Spittin’ sick sermons, skills are hotter than a sunny beach
I’m terrifyin’, a tenacious thrivin’ tyrant
You’re no threat to me, you’re tedious, your time’s up, stop tryin’
Terrafin, the terror’s in, I’ll trash the comp until his end is near
So tell the presses, I’m tempted to thrash this turkey right here!
Take it underground, any place, any terrain
I’m insane and I’ll go through ya like a two-ton train, mane
Wind-Upchucks at utter disappointments
Universal rhymes, utmost uniqueness to the point, yes
Unattached suckers utilize their skill sloppily
While I unlock potential, unify my thoughts solemnly
Urban kicks, using wits, upsetting those who fear me
And usually they’re under me, undone in theory
Back with a villainous verbal-assaulting valor
Vocally I am a vulture, preying in a vicious manner
Victory’s always valid, and never vague to Voodood
Swing valiantly with my ax and I vanquish all who pursue
I can verify that I’m very venomous
When it comes to volleying magic, no one else can do it best
Comin’ in like a wrecking ball, comin’ here to break your walls
This worm’s a wizard with his words, others are whack and all
Wham folks with a power that’s as whopping as my wordplay
Always known as a winner, even on my worst day (Whoo!)
Won’t whine or whimper when the time comes to whoop my foes
I bet my wit will have you wishin’ for a better flow
The extreme Tree Rex, expect excellence, don’t try and go
Xerox my existence, I can beat you like a xylophone
I’m exponential and exhibit total strength
Don’t need an X-ray to check that I’m always in a great state
Yo it’s the yes man, Pain-Yatta, young and restless
But yawn at all my enemies, the yonkers can’t touch this
Yin Yang, harmonious yahoo with the yummy flows
Yielding you to stay outta my yard, this is my abode
Year after year, I go through slack, so just stop the yacks
I’d hate to get very yucky and have to smash you flat
Containing zero tolerance, zipping through my enemies
Zooming through so fast it’s like they’re all a bunch of zombies
A zestful of explosives is sure to keep me going
The king of the bazooka’s in the zone and never slowing
Zilch’s the amount a fatigue I show, I’m going ziz zag
Zinger with my lines like it’s nothing and that’s a fact
I’m just a zealot with lots sturrin’ in my boiling pot
Alphabet Challenge, from A to Zook, man I can’t be stopped
Whoo!