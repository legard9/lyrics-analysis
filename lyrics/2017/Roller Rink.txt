18
When I left the skating rink I felt sort of hungry, so I went in this drugstore and
had a Swiss cheese sandwich and a 
malted
, and 
then I went in a phone booth
. I thought
maybe I might give old 
Jane
 another buzz and see if she was home yet. I mean I had the
whole evening free, and I thought I'd give her a 
buzz
 and, if she was home yet, take her
dancing or something somewhere. I never danced with her or anything the whole time I
knew her. I saw her 
dancing
 once, though. She looked like a very good dancer. It was at
this Fourth of July dance at the club. 
I didn't know her too well then, and I didn't think I
ought to cut in on her date
. 
She was dating this terrible guy
, 
Al Pike
, that went to 
Choate
.
I didn't know him too well, but he was always hanging around the swimming pool. He
wore those white 
Lastex
 kind o
f swimmin
g trunks, and he was always going off the 
high
dive
. He did the same lousy old half 
gainer
 all day long. It was the only dive he could do,
but he thought he was very hot stuff. 
All muscles and no brains.
 Anyway, that's who Jane
dated that night. I couldn't understand it. I swear I couldn't. After we started going around
together, I asked her how come she could date a showoff bastard like Al Pike. Jane said
he wasn't a show-off. She said he had an 
inferiority complex.
 She acted like she felt sorry
for him or something, and she wasn't just putting it on. She meant it. It's a funny thing
about girls. Every time you mention some guy that's strictly a bastard--very mean, or very
conceited and all--and when you mention it to the girl, she'll tell you he has an 
inferiority
complex
. Maybe he has, but that still doesn't keep him from being a 
bastard
, in my
opinion. Girls. You never know what they're going to think. I once got this girl 
Roberta
Walsh's roommate a date with a friend of mine. His name was Bob Robinson and he
really had an 
inferiority complex
. You could tell he was very ashamed of his parents and
all, because they said "he don't" and "she don't" and stuff like that and they weren't very
wealthy. But he wasn't a 
bastard
 or anything. He was a very nice guy. But this Roberta
Walsh's roommate didn't like him at all. She told Roberta he was too 
conceited
--and the
reason she thought he was conceited was because he happened to mention to her that he
was captain of the 
debating team
. A little thing like that, and she thought he was
conceited! 
The trouble with girls is
, if they like a boy, no matter how big a bastard he is,
they'll say he has an inferiority complex, and if they don't like him, no matter how nice a
guy he is, or how big an 
inferiority complex
 he has, they'll say he's conceited. Even smart
girls do it.
Anyway, I gave old Jane a buzz again, but her phone didn't answer, so I had to
hang up. Then I had to look through my 
address book
 to see who the hell might be
available for the evening. The trouble was, though, my address book only has about three
people in it. Jane, and this man, 
Mr. Antolini
, that was my teacher at 
Elkton Hills
, and my
father's office number. I keep forgetting to put people's names in. So what I did finally, I
gave old 
Carl Luce a buzz.
 He 
graduated
 from the 
Whooton Schoo
l after I left. He was
about three years older than I was, and I didn't like him too much, but he was one of these
very intellectual guys-- he had the highest 
I.Q.
 of any boy at Whooton--and I thought he
might want to have dinner with me somewhere and have a slightly 
intellectual
conversation. He was very enlightening sometimes. So I gave him a 
buzz
. He went to
Columbia now, but he lived on 65th Street and all, and I knew he'd be home. When I got
him on the phone, he said he couldn't make it for dinner but that he'd meet me for a drink
at ten o'clock at the Wicker Bar, on 54th. I think he was pretty surprised to hear from me.
I once called him a fat-assed 
phony
.
I had quite a bit of time to kill till ten o'clock, so what I did, 
I went to the movies
at Radio City
. It was probably the worst thing I could've done, but it was near, and I
couldn't think of anything else.
I came in when the goddam stage show was on. 
The Rockettes
 were kicking their
heads off, the way they do when they're all in line with their arms around each other's
waist. The 
audience
 applauded like mad, and some guy behind me kept saying to his
wife, "You know what that is? That's 
precision
." He killed me. Then, after the Rockettes,
a guy came out in a 
tuxedo
 and roller skates on, and started skating under a bunch of little
tables, and telling jokes while he did it. He was a very good skater and all, but I couldn't
enjoy it much because I kept picturing him practicing to be a guy that roller-skates on the
stage. It seemed so stupid. I guess I just wasn't in the right mood. Then, after him, they
had this 
Christmas
 thing they have at Radio City every year. All these angels start coming
out of the boxes and everywhere, guys carrying 
crucifixes
 and stuff all over the place,
and the whole bunch of them--thousands of them--singing 
"Come All Ye Faithful!"
 like
mad. Big deal. It's supposed to be religious as hell, I know, and very pretty and all, but I
can't see anything religious or pretty, for 
God
's sake, about a bunch of actors carrying
crucifixes all over the stage. When they were all finished and started going out the boxes
again, you could tell they could hardly wait to get a 
cigarette
 or something. I saw it with
old 
Sally Hayes
 the year before, and she kept saying how beautiful it was, the costumes
and all. I said old Jesus probably would've puked if He could see it--all those fancy
costumes and all. Sally said I was a 
sacrilegious atheist
. I probably am. The thing Jesus
really would've liked would be the guy that plays the kettle drums in the 
orchestra
. I've
watched that guy since I was about eight years old. My brother 
Allie
 and I, if we were
with our parents and all, we used to move our seats and go way down so we could watch
him. 
He's the best drummer
 I ever saw. He only gets a chance to bang them a couple of
times during a whole piece, but he never looks bored when he isn't doing it. Then when
he does bang them, he does it so nice and sweet, with this nervous expression on his face.
One time when we went to Washington with my father, Allie sent him a postcard, but I'll
bet he never got it. We weren't too sure how to address it.
After the 
Christmas
 thing was over, the goddam picture started. It was so putrid I
couldn't take my eyes off it. It was about this English guy, Alec something, that was in
the war and loses his memory in the hospital and all. He comes out of the hospital
carrying a cane and 
limping
 all over the place, all over London, not knowing who the hell
he is. He's really a duke, but he doesn't know it. Then he meets this nice, 
homey
, sincere
girl getting on a bus. Her goddam hat blows off and he catches it, and then they go
upstairs and sit down and start talking about Charles Dickens. He's both their favorite
author and all. He's carrying this copy of Oliver Twist and so's she. I could've puked.
Anyway, they fell in love right away, on account of they're both so nuts about Charles
Dickens and all, and he helps her run her publishing business. She's a publisher, the girl.
Only, she's not doing so hot, because her brother's a drunkard and he spends all their
dough. He's a very bitter guy, the brother, because he was a doctor in the war and now he
can't operate any more because his nerves are shot, so he boozes all the time, but he's
pretty witty and all. Anyway, old Alec writes a book, and this girl publishes it, and they
both make a hatful of dough on it. They're all set to get married when this other girl, old
Marcia, shows up. Marcia was Alec's fiancée before he lost his memory, and she
recognizes him when he's in this store autographing books. She tells old Alec he's really a
duke and all, but he doesn't believe her and doesn't want to go with her to visit his mother
and all. His mother's blind as a bat. But the other girl, the 
homey
 one, makes him go.
She's very 
noble
 and all. So he goes. But he still doesn't get his memory back, even when
his great Dane jumps all over him and his mother sticks her fingers all over his face and
brings him this teddy bear he used to slobber around with when he was a kid. But then,
one day, some kids are playing cricket on the lawn and he gets smacked in the head with
a cricket ball. Then right away he gets his goddam memory back and he goes in and
kisses his mother on the forehead and all. Then he starts being a regular duke again, and
he forgets all about the homey babe that has the publishing business. I'd tell you the rest
of the story, but I might puke if I did. It isn't that I'd spoil it for you or anything. There
isn't anything to spoil for 
Chrissake
. Anyway, it ends up with Alec and the homey babe
getting married, and the brother that's a drunkard gets his nerves back and operates on
Alec's mother so she can see again, and then the drunken brother and old Marcia go for
each other. It ends up with everybody at this long dinner table laughing their asses off
because the great Dane comes in with a bunch of puppies. Everybody thought it was a
male, I suppose, or some goddam thing. All I can say is, don't see it if you don't want to
puke all over yourself.
The part that got me was, there was a lady sitting next to me that cried all through
the goddam picture. The phonier it got, the more she cried. You'd have thought she did it
because she was kindhearted as hell, but I was sitting right next to her, and she wasn't.
She had this little kid with her that was bored as hell and had to go to the bathroom, but
she wouldn't take him. She kept telling him to sit still and behave himself. She was about
as kindhearted as a goddam wolf. You take somebody that cries their goddam eyes out
over phony stuff in the movies, and nine times out of ten they're mean bastards at heart.
I'm not kidding.
After the movie was over, I started walking down to the Wicker Bar, where I was
supposed to meet old 
Carl Luce
, and while I walked I sort of thought about war and all.
Those war movies always do that to me. I don't think I could stand it if I had to go to war.
I really couldn't. It wouldn't be too bad if they'd just take you out and shoot you or
something, but you have to stay in the Army so goddam long. That's the whole trouble.
My brother D.B. was in the Army for four goddam years. 
He was in the war, too--he
landed on D-Day and all
--but I really think he hated the Army worse than the war. I was
practically a child at the time, but I remember when he used to come home on furlough
and all, all he did was lie on his bed, practically. He hardly ever even came in the living
room. Later, when he went overseas and was in the war and all, he didn't get wounded or
anything and he didn't have to shoot anybody. All he had to do was drive some cowboy
general around all day in a command car. He once told Allie and I that if he'd had to
shoot anybody, he wouldn't've known which direction to shoot in. He said the Army was
practically as full of bastards as the Nazis were. I remember Allie once asked him wasn't
it sort of good that he was in the war because he was a writer and it gave him a lot to
write about and all. He made Allie go get his baseball mitt and then he asked him who
was the best war poet, 
Rupert Brooke
 or Emily Dickinson. 
Allie said Emily Dickinson
. I
don't know too much about it myself, because I don't read much poetry, but I do know it'd
drive me crazy if I had to be in the Army and be with a bunch of guys like Ackley and
Stradlater and old Maurice all the time, marching with them and all. I was in the 
Boy
Scouts
 once, for about a week, and I couldn't even stand looking at the back of the guy's
neck in front of me. 
They kept telling you to look at the back of the guy's neck in front of
you.
 I swear if there's ever another war, they better just take me out and stick me in front
of a firing squad. I wouldn't object. What gets me about D.B., though, he hated the war so
much, and yet he got me to read this book A Farewell to Arms last summer. He said it
was so terrific. That's what I can't understand. It had this guy in it named 
Lieutenant
Henry that was supposed to be a nice guy and all. I don't see how D.B. could hate the
Army and war and all so much and still like a 
phony
 like that. I mean, for instance, I don't
see how he could like a phony book like that and still like that one by 
Ring Lardner
, or
that other one he's so crazy about, The Great Gatsby. D.B. got sore when I said that, and
said I was too young and all to appreciate it, but I don't think so. I told him I liked Ring
Lardner and 
The Great Gatsby
 and all. I did, too. I was crazy about The Great Gatsby.
Old Gatsby. 
Old sport.
 That killed me. Anyway, I'm sort of glad they've got the atomic
bomb invented. If there's ever another war, I'm going to sit right the hell on top of it. I'll
volunteer for it, I swear to God I will.
19
In case you don't live in New York, the Wicker Bar is in this sort of 
swanky
 hotel,
the Seton Hotel. I used to go there quite a lot, but I don't any more. I 
gradually
 cut it out.
It's one of those places that are supposed to be very sophisticated and all, and the 
phonies
are coming in the window. They used to have these two French babes, Tina and Janine,
come out and play the 
piano
 and sing about three times every night. One of them played
the piano--
strictly lousy
--and the other one sang, and most of the songs were either pretty
dirty or in French. The one that sang, old Janine, was always whispering into the goddam
microphone before she sang. She'd say, "And now we like to geeve you our impression of
Vooly Voo Fransay. Eet ees the story of a leetle Fransh girl who comes to a beeg ceety,
just like New York, and falls een love wees a leetle boy from Brookleen. We hope you
like eet." Then, when she was all done whispering and being cute as hell, she'd sing some
dopey song, half in English and half in French, and drive all the 
phonies
 in the place mad
with joy. If you sat around there long enough and heard all the 
phonies
 applauding and
all, 
you got to hate everybody in the world, I swear you did
. The bartender was a louse,
too. 
He was a big snob
. He didn't talk to you at all hardly unless you were a big shot or a
celebrity or something. If you were a big shot or a celebrity or something, then he was
even more nauseating. He'd go up to you and say, with this big charming smile, like he
was a helluva swell guy if you knew him, "Well! How's Connecticut?" or "How's
Florida?" It was a terrible place, I'm not kidding. I cut out going there entirely, 
gradually
.
It was pretty early when I got there. I sat down at the 
bar
--it was pretty crowded--
and had a couple of Scotch and sodas before old Luce even showed up. 
I stood up when I
ordered them so they could see how tall I was and all and not think I was a goddam
minor.
 
Then I watched the phonies for a while.
 Some guy next to me was snowing hell
out of the babe he was with. He kept telling her she had 
aristocratic
 hands. That killed
me. The other end of the bar was full of flits. They weren't too flitty-looking--I mean they
didn't have their hair too long or anything--but you could tell they were flits anyway.
Finally old Luce showed up.
Old Luce. What a guy. He was supposed to be my Student Adviser when I was at
Whooton. The only thing he ever did, though, was give these sex talks and all, late at
night when there was a bunch of guys in his room. He knew quite a bit about sex,
especially 
perverts
 and all. He was always telling us about a lot of creepy guys that go
around having affairs with sheep, and guys that go around with girls' pants sewed in the
lining of their hats and all. And flits and Lesbians. Old Luce knew who every flit and
Lesbian
 in the United States was. All you had to do was mention somebody--anybody--
and old Luce'd tell you if he was a flit or not. Sometimes it was hard to believe, the
people he said were flits and Lesbians and all, movie actors and like that. Some of the
ones he said were flits were even married, for God's sake. You'd keep saying to him,
"You mean Joe Blow's a flit? Joe Blow? That big, tough guy that plays gangsters and
cowboys all the time?" Old Luce'd say, "Certainly." He was always saying "Certainly."
He said it didn't matter if a guy was married or not. He said half the married guys in the
world were flits and didn't even know it. He said you could turn into one practically
overnight, if you had all the traits and all. He used to scare the hell out of us. I kept
waiting to turn into a flit or something. The funny thing about old Luce, I used to think he
was sort of flitty himself, in a way. He was always saying, "Try this for size," and then
he'd goose the hell out of you while you were going down the corridor. And whenever he
went to the can, he always left the goddam door open and talked to you while you were
brushing your teeth or something. That stuff's sort of flitty. It really is. I've known quite a
few real flits, at schools and all, and they're always doing stuff like that, and that's why I
always had my doubts about old Luce. He was a pretty intelligent guy, though. He really
was.
He never said hello or anything when he met you. The first thing he said when he
sat down was that he could only stay a couple of minutes. He said he had a date. Then he
ordered a dry 
Martini
. He told the bartender to make it very dry, and no olive.
"Hey, I got a flit for you," I told him. "At the end of the bar. Don't look now. I
been saving him for ya."
"Very funny," he said. "Same old Caulfield. When are you going to grow up?"
I bored him a lot. I really did. He amused me, though. He was one of those guys
that sort of amuse me a lot.
"How's your sex life?" I asked him.
 He hated you to ask him stuff like that.
"Relax," he said. 
"Just sit back and relax, for Chrissake."
"I'm relaxed," I said. "How's Columbia? Ya like it?"
"Certainly I like it. If I didn't like it I wouldn't have gone there," he said. He could
be pretty boring himself sometimes.
"What're you majoring in?" I asked him. "Perverts?" I was only 
horsing around
.
"What're you trying to be--funny?"
"No. I'm only kidding," I said. "Listen, hey, Luce. You're one of these intellectual
guys. I need your advice. I'm in a terrific--"
He let out this big groan on me. "Listen, Caulfield. If you want to sit here and
have a quiet, peaceful drink and a quiet, peaceful conver--"
"All right, all right," I said. "Relax." You could tell he didn't feel like discussing
anything serious with me. That's the trouble with these intellectual guys. They never want
to discuss anything serious unless they feel like it. So all I did was, I started discussing
topics in general with him. "No kidding, how's your sex life?" I asked him. "You still
going around with that same babe you used to at Whooton? The one with the terrffic--"
"Good God, no," he said.
"How come? What happened to her?"
"I haven't the 
faintest
 idea. For all I know, since you ask, she's probably the
Whore of New Hampshire by this time."
"That isn't nice. If she was decent enough to let you get sexy with her all the time,
you at least shouldn't talk about her that way."
"Oh, God!" old Luce said. "Is this going to be a typical Caulfield conversation? I
want to know right now."
"No," I said, "but it isn't nice anyway. If she was decent and nice enough to let
you--"
"Must we pursue this horrible 
trend of thought
?"
I didn't say anything. I was sort of afraid he'd get up and leave on me if I didn't
shut up. So all I did was, I ordered another drink. I felt like getting stinking drunk.
"Who're you going around with now?" I asked him. "You feel like telling me?"
"Nobody you know."
"Yeah, but who? I might know her."
"Girl lives in the Village. Sculptress. If you must know."
"Yeah? No kidding? How old is she?"
"I've never asked her, for God's sake."
"Well, around how old?"
"I should imagine she's in her late thirties," old Luce said.
"In her late thirties? Yeah? You like that?" I asked him. "You like 'em that old?"
The reason I was asking was because he really knew quite a bit about sex and all. He was
one of the few guys I knew that did. He lost his virginity when he was only fourteen, in
Nantucket
. He really did.
"I like a mature person, if that's what you mean. Certainly."
"You do? Why? No kidding, they better for sex and all?"
"Listen. Let's get one thing straight. I refuse to answer any typical Caulfield
questions tonight. When in hell are you going to grow up?"
I didn't say anything for a while. I let it drop for a while.
 Then old Luce ordered
another Martini and told the bartender to make it a lot dryer.
"Listen. How long you been going around with her, this 
sculpture
 babe?" I asked
him. I was really interested. "Did you know her when you were at Whooton?"
"Hardly. She just arrived in this country a few months ago."
"She did? Where's she from?"
"She happens to be from 
Shanghai
."
"No kidding! She Chinese, for Chrissake?"
"Obviously."
"No kidding! Do you like that? Her being Chinese?"
"Obviously."
"Why? I'd be interested to know--I really would."
"I simply happen to find Eastern philosophy more satisfactory than Western.
Since you ask."
"You do? Wuddaya mean '
philosophy
'? Ya mean sex and all? You mean it's better
in China? That what you mean?"
"Not necessarily in China, for God's sake. The East I said. Must we go on with
this inane conversation?"
"Listen, I'm serious," I said. "No kidding. Why's it better in the East?"
"It's too involved to go into, for God's sake," old Luce said. "They simply happen
to regard sex as both a physical and a spiritual experience. If you think I'm--"
"So do I! So do I regard it as a wuddayacallit--a physical and spiritual experience
and all. I really do. But it depends on who the hell I'm doing it with. If I'm doing it with
somebody I don't even--"
"Not so loud, for God's sake, Caulfield. If you can't manage to keep your voice
down, let's drop the whole--"
"All right, but listen," I said. I was getting excited and I was talking a little too
loud. Sometimes I talk a little loud when I get excited. "This is what I mean, though," I
said. 
"I know it's supposed to be physical and spiritual, and artistic and all. But what I
mean is, you can't do it with everybody--every girl you neck with and all--and make it
come out that way. Can you?"
"Let's drop it," old Luce said. "Do you mind?"
"All right, but listen. Take you and this Chinese babe. What's so good about you
two?"
"Drop it, I said."
I was getting a little too personal. I realize that. But that was one of the annoying
things about Luce. When we were at Whooton, he'd make you describe the most personal
stuff that happened to you, but if you started asking him questions about himself, he got
sore. These 
intellectual
 guys don't like to have an intellectual conversation with you
unless they're running the whole thing. They always want you to shut up when they shut
up, and go back to your room when they go back to their room. When I was at Whooton
old Luce used to hate it--you really could tell he did--when after he was finished giving
his sex talk to a bunch of us in his room we stuck around and chewed the fat by ourselves
for a while. I mean the other guys and myself. In somebody else's room. Old Luce hated
that. He always wanted everybody to go back to their own room and shut up when he was
finished being the big shot. The thing he was afraid of, he was afraid somebody'd say
something smarter than he had. He really amused me.
"Maybe I'll go to China. My sex life is lousy," I said.
"Naturally. Your mind is immature."
"It is. It really is. I know it," I said. "You know what the trouble with me is? I can
never get really sexy--I mean really sexy--with a girl I don't like a lot. I mean I have to
like her a lot. If I don't, I sort of lose my goddam desire for her and all. Boy, it really
screws up my sex life something awful. My sex life stinks."
"Naturally it does, for God's sake. I told you the last time I saw you what you
need."
"You mean to go to a psychoanalyst and all?" I said. That's what he'd told me I
ought to do. His father was a 
psychoanalyst
 and all.
"It's up to you, for God's 
sake
. It's none of my goddam business what you do with
your life."
I didn't say anything for a while. I was thinking.
"Supposing I went to your father and had him 
psychoanalyze
 me and all," I said.
"What would he do to me? I mean what would he do to me?"
"He wouldn't do a goddam thing to you. He'd simply talk to you, and you'd talk to
him, for God's sake. For one thing, he'd help you to recognize the patterns of your mind."
"The what?"
"The patterns of your mind. Your mind runs in-- Listen. I'm not giving an
elementary course in 
psychoanalysis
. If you're interested, call him up and make an
appointment. If you're not, don't. I couldn't care less, frankly."
I put my hand on his shoulder. Boy, he amused me. "You're a real friendly
bastard," I told him. "You know that?"
He was looking at his wrist watch. "I have to tear," he said, and stood up. "Nice
seeing you." He got the bartender and told him to bring him his check.
"Hey," I said, just before he beat it. "Did your father ever psychoanalyze you?"
"Me? Why do you ask?"
"No reason. Did he, though? Has he?"
"Not exactly. He's helped me to adjust myself to a certain extent, but an extensive
analysis hasn't been necessary. Why do you ask?"
"No reason. I was just wondering."
"Well. Take it easy," he said. He was leaving his tip and all and he was starting to
go.
"Have just one more drink," I told him. "Please. I'm lonesome as hell. No
kidding."
He said he couldn't do it, though. He said he was late now, and then he left.
Old 
Luce
. He was strictly a pain in the ass, but he certainly had a good
vocabulary. He had the largest vocabulary of any boy at Whooton when I was there. They
gave us a test.
20
I kept sitting there getting drunk and waiting for old Tina and Janine to come out
and do their stuff, but they weren't there. A 
flitty-looking
 guy with wavy hair came out
and played the piano, and then this new babe, 
Valencia
, came out and sang. She wasn't
any good, but she was better than old Tina and Janine, and at least she sang good songs.
The piano was right next to the bar where I was sitting and all, and old Valencia was
standing practically right next to me. I sort of gave her the old eye, but she pretended she
didn't even see me. I probably wouldn't have done it, but I was getting drunk as hell.
When she was finished, she beat it out of the room so fast I didn't even get a chance to
invite her to join me for a drink, so I called the 
headwaiter
 over. I told him to ask old
Valencia if she'd care to join me for a drink. He said he would, but he probably didn't
even give her my message. People never give your message to anybody.
Boy, 
I sat at that goddam bar till around one o'clock or so, getting drunk as a
bastard
. I could hardly see straight. The one thing I did, though, I was careful as hell not
to get 
boisterous
 or anything. I didn't want anybody to notice me or anything or ask how
old I was. 
But, boy, I could hardly see straight. When I was really drunk, I started that
stupid business with the bullet in my guts again. I was the only guy at the bar with a
bullet in their guts. I kept putting my hand under my jacket, on my stomach and all, to
keep the blood from dripping all over the place. I didn't want anybody to know I was
even wounded. I was concealing the fact that I was a wounded sonuvabitch.
 Finally what
I felt like, I felt like giving old Jane a buzz and see if she was home yet. So I paid my
check and all. Then I left the bar and went out where the telephones were. I kept keeping
my hand under my jacket to keep the blood from dripping. Boy, was I drunk.
But when I got inside this phone booth, I wasn't much in the mood any more to
give old Jane a buzz.
 I was too drunk, I guess. So what I did, I gave old 
Sally Hayes
 a
buzz.
I had to dial about twenty numbers before I got the right one. Boy, was I blind.
"Hello," I said when somebody answered the goddam phone. I sort of yelled it, I
was so drunk.
"Who is this?" this very cold lady's voice said.
"This is me. Holden Caulfield. Lemme speaka Sally, please."
"Sally's asleep. This is Sally's grandmother. Why are you calling at this hour,
Holden? 
Do you know what time it is?"
"Yeah. Wanna talka Sally. Very important. Put her on."
"Sally's asleep, young man. Call her tomorrow. Good night."
"Wake 'er up! Wake 'er up, hey. 
Attaboy
."
Then there was a different voice. "Holden, this is me." It was old Sally. "What's
the big idea?"
"Sally? That you?"
"Yes--stop screaming. Are you drunk?"
"Yeah. Listen. Listen, hey. I'll come over Christmas Eve. Okay? Trimma goddarn
tree for ya. Okay? Okay, hey, Sally?"
"Yes. You're drunk. Go to bed now. Where are you? Who's with you?"
"Sally? I'll come over and trimma tree for ya, okay? Okay, hey?"
"Yes. Go to bed now. Where are you? Who's with you?"
"Nobody. Me, myself and I." Boy was I drunk! I was even still holding onto my
guts. "They got me. Rocky's mob got me. You know that? Sally, you know that?"
"I can't hear you. Go to bed now. I have to go. Call me tomorrow."
"Hey, Sally! You want me trimma tree for ya? Ya want me to? Huh?"
"Yes. Good night. Go home and go to bed."
She hung up on me.
"G'night. G'night, Sally baby. Sally sweetheart darling," I said. Can you imagine
how drunk I was? I hung up too, then. I figured she probably just came home from a date.
I pictured her out with the 
Lunts
 and all somewhere, and that Andover jerk. All of them
swimming around in a goddam pot of tea and saying 
sophisticated
 stuff to each other and
being charming and phony. I wished to God I hadn't even phoned her. When I'm drunk,
I'm a 
madman
.
I stayed in the damn phone booth for quite a while. I kept holding onto the phone,
sort of, so I wouldn't pass out. I wasn't feeling too marvelous, to tell you the truth.
Finally, though, I came out and went in the men's room, 
staggering
 around like a moron,
and filled one of the washbowls with cold water. Then I dunked my head in it, right up to
the ears. I didn't even bother to dry it or anything. I just let the sonuvabitch drip. Then I
walked over to this 
radiator
 by the window and sat down on it. It was nice and warm. It
felt good because I was shivering like a bastard. It's a funny thing, I always shiver like
hell when I'm drunk.
I didn't have anything else to do, so I kept sitting on the radiator and counting
these little white squares on the floor. I was getting soaked. About a gallon of water was
dripping down my neck, getting all over my collar and tie and all, but I didn't give a
damn. I was too drunk to give a damn. Then, pretty soon, the guy that played the piano
for old Valencia, this very wavyhaired, flitty-looking guy, came in to comb his golden
locks. We sort of struck up a conversation while he was combing it, except that he wasn't
too goddam friendly.
"Hey. You gonna see that Valencia babe when you go back in the bar?" I asked
him.
"It's highly probable," he said. Witty bastard. All I ever meet is witty bastards.
"Listen. Give her my compliments. Ask her if that goddam waiter gave her my
message, willya?"
"Why don't you go home, Mac? How old are you, anyway?"
"Eighty-six. Listen. Give her my compliments. Okay?"
"Why don't you go home, Mac?"
"Not me. Boy, you can play that goddam piano." I told him. I was just flattering
him. He played the piano stinking, if you want to know the truth. "You oughta go on the
radio," I said. "Handsome chap like you. All those goddam golden locks. Ya need a
manager?"
"Go home, Mac, like a good guy. Go home and hit the sack."
"No home to go to. No kidding--you need a manager?"
He didn't answer me. He just went out. He was all through combing his hair and
patting it and all, so he left. Like Stradlater. All these handsome guys are the same. When
they're done combing their goddam hair, they beat it on you.
When I finally got down off the radiator and went out to the hat-check room, I
was crying and all. I don't know why, but I was. I guess it was because 
I was feeling so
damn depressed and lonesome
. Then, when I went out to the checkroom, I couldn't find
my goddam check. The hat-check girl was very nice about it, though. She gave me my
coat anyway. And my 
"Little Shirley Beans"
 record--I still had it with me and all. I gave
her a buck for being so nice, but she wouldn't take it. She kept telling me to go home and
go to bed. I sort of tried to make a date with her for when she got through working, but
she wouldn't do it. She said she was old enough to be my mother and all. I showed her
my goddam gray hair and told her I was forty-two--I was only horsing around, naturally.
She was nice, though. I showed her my goddam 
red hunting hat
, and she liked it. She
made me put it on before I went out, because my hair was still pretty wet. She was all
right.
I didn't feel too drunk any more when I went outside, but it was getting very cold
out again, and my teeth started chattering like hell. I couldn't make them stop. I walked
over to Madison Avenue and started to wait around for a bus because 
I didn't have hardly
any money left
 and I had to start 
economizing
 on cabs and all. But I didn't feel like
getting on a damn bus. And besides, I didn't even know where I was supposed to go. So
what I did, I started walking over to the park. I figured I'd go by that little lake and see
what the hell the ducks were doing, see if they were around or not, I still didn't know if
they were around or not. It wasn't far over to the park, and I didn't have anyplace else
special to go to--I didn't even know where I was going to sleep yet--so I went. I wasn't
tired or anything. I just felt blue as hell.
Then something terrible happened just as I got in the park. I dropped old 
Phoebe
's
record. It broke-into about fifty pieces. It was in a big envelope and all, but it broke
anyway. I damn near cried, it made me feel so terrible, but all I did was, I took the pieces
out of the envelope and put them in my coat pocket. They weren't any good for anything,
but I didn't feel like just throwing them away. Then I went in the park. Boy, was it dark.
I've lived in New York all my life, and I know Central Park like the back of my
hand, because I used to roller-skate there all the time and ride my bike when I was a kid,
but I had the most terrific trouble finding that lagoon that night. I knew right where it
was--it was right near Central Park South and all--but I still couldn't find it. I must've
been drunker than I thought. I kept walking and walking, and it kept getting darker and
darker and spookier and spookier. I didn't see one person the whole time I was in the
park. I'm just as glad. I probably would've jumped about a mile if I had. Then, finally, I
found it. What it was, it was partly frozen and partly not frozen. But I didn't see any
ducks around. I walked all around the whole damn lake--I damn near fell in once, in fact-
-but I didn't see a single duck. I thought maybe if there were any around, they might be
asleep or something near the edge of the water, near the grass and all. That's how I nearly
fell in. But I couldn't find any.
Finally I sat down on this bench, where it wasn't so goddam dark. 
Boy, I was still
shivering like a bastard, and the back of my hair, even though I had my hunting hat on,
was sort of full of little hunks of ice. That worried me. I thought probably I'd get
pneumonia and die
. I started picturing millions of jerks coming to my funeral and all. My
grandfather from Detroit, that keeps calling out the numbers of the streets when you ride
on a goddam bus with him, and my aunts--I have about fifty aunts--and all my lousy
cousins. What a mob'd be there. They all came when Allie died, the whole goddam stupid
bunch of them. I have this one stupid aunt with 
halitosis
 that kept saying how peaceful he
looked lying there, D.B. told me. I wasn't there. I was still in the hospital. I had to go to
the hospital and all after I hurt my hand. Anyway, I kept worrying that I was getting
pneumonia
, with all those hunks of ice in my hair, and that I was going to die. I felt sorry
as hell for my mother and father. Especially my mother, because she still isn't over my
brother Allie yet. I kept picturing her not knowing what to do with all my suits and
athletic equipment and all. The only good thing, I knew she wouldn't let old Phoebe come
to my goddam 
funeral
 because she was only a little kid. That was the only good part.
Then I thought about the whole bunch of them sticking me in a goddam cemetery and all,
with my name on this tombstone and all. Surrounded by dead guys. Boy, when you're
dead, they really fix you up. I hope to hell when I do die somebody has sense enough to
just dump me in the river or something. Anything except sticking me in a goddam
cemetery. People coming and putting a bunch of flowers on your stomach on Sunday, and
all that crap. Who wants flowers when you're dead? Nobody.
When the weather's nice, my parents go out quite frequently and stick a bunch of
flowers on old Allie's grave. I went with them a couple of times, but I cut it out. In the
first place, I certainly don't enjoy seeing him in that crazy cemetery. Surrounded by dead
guys and tombstones and all. It wasn't too bad when the sun was out, but twice--twice--
we were there when it started to rain. It was awful. It rained on his lousy tombstone, and
it rained on the grass on his stomach. It rained all over the place. All the visitors that were
visiting the cemetery started running like hell over to their cars. That's what nearly drove
me crazy. All the visitors could get in their cars and turn on their radios and all and then
go someplace nice for dinner--everybody except Allie. I couldn't stand it. I know it's only
his body and all that's in the cemetery, and his soul's in Heaven and all that crap, but I
couldn't stand it anyway. I just wish he wasn't there. You didn't know him. If you'd
known him, you'd know what I mean. It's not too bad when the sun's out, but the sun only
comes out when it feels like coming out.
After a while, just to get my mind off getting 
pneumonia
 and all, I took out my
dough and tried to count it in the lousy light from the street lamp. All I had was three
singles and five quarters and a nickel left--boy, I spent a fortune since I left Pencey. Then
what I did, I went down near the 
lagoon
 and I sort of skipped the quarters and the nickel
across it, where it wasn't frozen. I don't know why I did it, but I did it. I guess I thought
it'd take my mind off getting pneumonia and dying. It didn't, though.
I started thinking how old Phoebe would feel if I got 
pneumonia
 and died. It was a
childish way to think, but I couldn't stop myself. She'd feel pretty bad if something like
that happened. She likes me a lot. I mean she's quite fond of me. She really is. Anyway, I
couldn't get that off my mind, so finally what I figured I'd do, I figured I'd better sneak
home and see her, in case I died and all. I had my door key with me and all, and I figured
what I'd do, I'd sneak in the apartment, very quiet and all, and just sort of 
chew the fat
with her for a while. The only thing that worried me was our front door. It creaks like a
bastard. It's a pretty old apartment house, and the 
superintendent's
 a lazy bastard, and
everything creaks and squeaks. I was afraid my parents might hear me sneaking in. But I
decided I'd try it anyhow.
So I got the hell out of the park, and went home. I walked all the way. It wasn't
too far, and 
I wasn't tired or even drunk any more. It was just very cold and nobody
around anywhere.