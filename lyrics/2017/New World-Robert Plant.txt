With songs we praise a happy landing
On yet another virgin shore
Escape the old world
Embrace the new world
Out here the immigrant takes all
Across the plains and over mountains
Put plight to all who came before
They’re barely human
It’s time to move them
To let them kneel before the sword
Uneducate the noble savage
The great white father’s word is law
Subjugate them
To liberate them
The poison pen, the bloody sword