[Verse 1: Exuma]
I came down on a lightning bolt
Nine months in my Mama's belly
When I was born the midwife screamed and shout
I had fire and brimstone coming out of my mouth

[Chorus]
I'm Exuma, I'm the obeah man
(Exuma, obeah man)
Nananana nanana, nanana nana
Nananana nanana, nanana nana

[Verse 2]
Exuma was my name when I lived in the stars
Exuma was a planet that once lit Mars
I've got the voice of many in my throat
The teeth of a frog and the tail of a goat

[Chorus]
I'm Exuma, I'm the obeah man
(Exuma, obeah man)
Yeah, nananana nanana, nanana nanana
Nananana nanana, nanana nanana

[Verse 3]
When I've got my big hat on my head
You know that I can raise the dead
And when I got my stick in my hand
You know that I am The Obeah Man
If you got a woman and she ain't happy
Come see me for camalame
Take that camalame and you make her some tea
And she will love you all the time
And when she got you running like a train on a track
Take some flour and you make some pap
That ought to give you strength in your back

[Chorus]
I'm Exuma, I'm the obeah man
(Exuma, obeah man)
Nananana nanana, nanana nanana
Nanananana nanana, nanana nanana

Yeah, Yeah

[Verse 4]
I've sailed with Charon, day and night
I've walked with Houngaman, Hector Hyppolite
Obeah, Obeah, Obeah, Obeah's in me
(Obeah, obeah, obeah)
I drank the water from the fiery sea

[Chorus]
I'm Exuma, I'm the obeah man
(Exuma, obeah man)
Nananana nanana, nanana nanana
Nanananana nanana, nanana nanana

Yeah, hey, hey hey
Hahaha

[Verse 5]
Tony McKay was my given name
Given on Cat island when my mama felt the pain
Creatures of the earth, space, sea, and land
I'm Exuma, I'm the obeah man

[Chorus]
I'm Exuma, I'm the obeah man
(Exuma, obeah man)
Hey, hey, nanananana
Nanananana, hey, hey, hey

[Verse 6]
Think about a fixture, I'll fix your hand
If you got a woman, oh, she'll have a man
And if you have a man, I'll get you a woman
Because I am the obeah man
You know I am the obeah man
Hey, hey, hey
Hey, hey, hey
Hey, hey, oh yeah
I'm the obeah man, I'm the obeah man
I'm the obeah man, I'm the oh--
Woo, hoo, hoo, aw, uh-huh
I can make the sun fall from the sky
I can make man that wants to live again
I'm the obeah man, I'm the obeah man, I'm the oh--
Hey, hey, hey, hey, hey