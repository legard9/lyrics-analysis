[Verse 1]
I've been thinking 'bout the future
And I've been thinking 'bout the now
I know we're gonna be together
I just don't know how

[Pre-Chorus]
You know when we get close
Can't deny the tension between us both
And I don't wanna pressure you, but I think you need to make a move

[Chorus]
I've been waiting (I've been waiting)
And I'll keep waiting (I'll keep waiting)
Only forever, only forever, only forever, only forever
Only forever, only forever, only forever, only forever

[Verse 2]
Yeah, I can tell that you're terrified to take a shot this strong
Should I wait up for you day and night?
Just let me know how long

[Pre-Chorus]
You know when we get close
Can't deny the tension between us both
And I don't wanna pressure you, but I think you need to make a move

[Chorus]
'Cause I've been waiting (I've been waiting)
And I'll keep waiting (I'll keep waiting)
Only forever, only forever, only forever, only forever
Only forever, only forever, only forever, only forever

[Bridge]
What if I told you it's too late?
What if I say that I can't wait?
What if I meet somebody else who doesn't leave me on a shelf?
I'll give you one more chance, but it only lasts

[Chorus]
Only forever, only forever, only forever, only forever
Only forever, only forever, only forever, only forever