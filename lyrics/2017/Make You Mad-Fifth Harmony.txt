[Verse 1: Normani]
It's in my blood, it’s in my veins
The way I work it, baby, you know what's on my mind
It's in my heart, it’s in the way I move that body
And every time I leave, my image on your mind

[Pre-Chorus: Ally]
No other like me, yeah, baby, you'll see
Won't ya come my way?
I'll lock ya down and I'll throw out the key
Won't ya come my way?

[Chorus: Normani, Lauren, Dinah & Ally]
I'm gonna make you miss me
I'm gonna make you go mad
I’m gonna make sure I’m the best you ever had (No)
I'm gonna make you miss me (No)
I’m gonna make you so mad (No)
I'm gonna make sure I'm the best you ever had

[Verse 2: Dinah & (Lauren)]
It's in the night, I hear you call in the midnight hour
That’s when I come alive
Turn out the light, now you're gon' feel my power
Can't take the heat, coming from the inside out (Whoa)

[Pre-Chorus: Lauren, Ally, Both]
No other like me, yeah, baby, you'll see
Won't ya come my way?
I'll lock ya down and I'll throw out the key
Won't ya come my way? (Whoa)

[Chorus: Normani, Lauren, Dinah & Ally]
I'm gonna make you miss me
I'm gonna make you go mad
I'm gonna make sure I'm the best you ever had (Woah, woah)
(No)
I'm gonna make you miss me (No)
I'm gonna make you so mad (No) (I'mma make you miss me)
I'm gonna make sure I'm the best you ever had

[Bridge: Ally, Dinah & Normani]
Ayy, won't ya come my way?
Ayy, won't ya come my way?
Ayy, won't ya come my, won't you come my
Won't you come my way? (What you say now?)
Ayy, won't ya come my way? (Ooh)
Ayy, won't ya come my way? (What you say now?)
Ayy, won't ya come my, won't you come my
Won't you come my way?

[Chorus: Normani, Lauren, Dinah & Ally]
I'm gonna make you miss me (I'ma make you miss me)
I'm gonna make you go mad (Woah, yeah)
I'm gonna make sure I'm the best you ever had (Best you ever had, baby)
I'm gonna make you miss me (Gonna make you love me, yeah)
I'm gonna make you so mad
I'm gonna make sure I'm the best you ever had