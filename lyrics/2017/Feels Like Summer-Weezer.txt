[Verse 1]
Climbing up the tower, just a boy and his computer
I'm still in my bathrobe, hiding in the shadows
I'm not used to losing, bye, bye, sugar blue eyes
You're home with the angels, thank you for being so kind

[Pre-Chorus]
I'm holding on and I don't want to let you go, whoa-oh-oh

[Chorus]
Yeah it feels like summer, yeah it feels like summer to me
Yeah it feels like summer, yeah it feels like summer
And she was a lover to me, to me, to me, to me

[Verse 2]
Which way is the graveyard? I'm an iceberg with a warm heart
I'm spiritual, not religious, I'm a Libra, if it matters
Shattered by an email, your words will fade away
Castle built in the sand will only last one day

[Pre-Chorus]
I'm holding on and I don't want to let you go, whoa-oh-oh

[Chorus]
Yeah it feels like summer, yeah it feels like summer to me
Yeah it feels like summer, yeah it feels like summer
When she was a lover to me

[Bridge]
June bride, shine so bright
Flowers in her hair, but it just ain't right
June bride, shine so bright
Flowers in her hair
We look good together, aw yeah
We look good together, aw yeah

[Chorus]
Yeah it feels like summer, yeah it feels like summer
When she was a lover to me

[Outro]
Let me see the smile, stay with me awhile
I cried for you, you were the song in my life
Let me see the smile, stay with me awhile
I cry for you, you were the song in my life