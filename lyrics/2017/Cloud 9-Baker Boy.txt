[Intro: Baker Boy]
Baker Boy in the town
(Yolngu Matha – English translation)
I'm on cloud 9

[Verse 1: Baker Boy]
(Yolngu Matha – English translation)
They see you really humble, don't ever change, you might crumble
(Yolngu Matha – English translation)
Don't ever turn back on God
(Yolngu Matha – English translation)
We gonna fly high like an eagle, don't wanna see no ego
Don't think about it, let go
Just hold on to my hand and let's go
Gotta do what Baker Boy, young man, not a boy
You can't control me like a toy
I'm a human being, just like you and me
We are family, stop the jealousy
Come with me now, break it down
North side of town, stop being the clown
Be the person that chases the crown
'Cause at the end of the day, you'll be found
So never back down (Back down, back down)

[Pre-Chorus: Baker Boy]
You wanna be as good as me? Boy, you better practice
Step back, feel the power of my blackness
You wanna be as good as me? Boy, you better practice
Step back, feel the power of my blackness

[Chorus: Kian & 
Baker Boy
]
Can't stop me now 
(Can't stop me)
And you can't bring me down 
(Bring me down)
I'm on cloud 9
And I'm not coming down 
(I'm on cloud 9)
Can't stop me now
And you can't bring me down
I'm on cloud 9 
(Bring me down)
And I'm not coming down

[Verse 2: Baker Boy]
(Yolngu Matha – English translation)
Whenever the truth comes out, your heart is ripping out
You gotta stay strong, no doubt
The bigger the crowd, the bigger we are
'Cause the internet changes the line
'Cause they they don't want us too divine
From the human race, always on the case
Ending up getting chased, now we're standing in chains
Flash back, being black, reality hits back
Your system is whack
Trying to find a way to change the track because
(Yolngu Matha – English translation)

[Pre-Chorus: Baker Boy]
You wanna be as good as me? Boy, you better practice
Step back, feel the power of my blackness
You wanna be as good as me? Boy, you better practice
Step back, feel the power of my blackness

[Chorus: Kian & 
Baker Boy
]
Can't stop me now 
(Can't stop me)
And you can't bring me down 
(Bring me down)
I'm on cloud 9 
(I'm on cloud 9)
And I'm not coming down
Can't stop me now
And you can't bring me down
I'm on cloud 9
And I'm not coming down
Can't stop me now 
(Can't stop me)
And you can't bring me down 
(Bring me down)
I'm on cloud 9
And I'm not coming down 
(I'm on cloud 9)
Can't stop me now
And you can't bring me down
I'm on cloud 9
And I'm not coming down 
(I'm not coming down)
Can't stop me now 
(Can't stop me)
And you can't bring me down 
(You can't bring me down)
I'm on cloud 9 
(I'm on cloud 9)
And I'm not coming down 
(I'm not, I'm not coming down)
Can't stop me now 
(Can't stop me)
And you can't bring me down 
(You can't bring me down)
I'm on cloud 9 
(I'm on cloud 9)
And I'm not coming down 
(I'm not coming down)

[Outro: Baker Boy]
You can't bring me down
I'm not coming down
Can't stop me