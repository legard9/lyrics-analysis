From the very very first time I saw you
You were my home, you were home, you were my home
Now that you are falling, let me be your calling, be your one prayer, keep away the bad things
In this moment let me give to you what you’ve given to me, that’s the least I can do
Baby it’s you
Every time I close my eyes
It’s always been you
Heaven knows that I’ll be waiting always by your side
Baby it’s you
Every time that I was down
You were there to fix me up
You were there to pick me up
Now it’s my turn
From the billion hearts to choose between
Oh I was your choice, I was your choice, so let me be your salvation and joy
You know that I love it when you call, just to say hello, oh oh
Will you let me hear it, once more