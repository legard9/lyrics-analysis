Miglior film
La forma dell'acqua - The Shape of Water
 (The Shape of Water)
Chiamami col tuo nome (Call Me by Your Name)
Dunkirk
Il filo nascosto (Phantom Thread)
Lady Bird
L'ora più buia (Darkest Hour)
The Post
Scappa - Get Out (Get Out)
Tre manifesti a Ebbing, Missouri (Three Billboards Outside Ebbing, Missouri)
Miglior regia
Guillermo del Toro
, The Shape of Water – La forma dell’acqua
Christopher Nolan, 
Dunkirk
Jordan Peele, Get Out – Scappa
Greta Gerwig, Lady Bird
Paul Thomas Anderson, Il filo nascosto
Miglior attrice
Sally Hawkins - 
La forma dell'acqua - The Shape of Water (The Shape of Water)
Frances McDormand
 - Tre manifesti a Ebbing, Missouri (Three Billboards Outside Ebbing, Missouri)
Margot Robbie - I, Tonya
Saoirse Ronan - Lady Bird
Meryl Streep - The Post
Miglior attore
Gary Oldman
 - L'ora più buia (Darkest Hour)
Timothée Chalamet - Chiamami col tuo nome (Call Me by Your Name)
Daniel Day-Lewis - Il filo nascosto (Phantom Thread)
Daniel Kaluuya - Scappa - Get Out (Get Out)
Denzel Washington - Roman J. Israel, Esq.
Miglior attrice non protagonista
Mary J. Blige - Mudbound
Allison Janney
 - I, Tonya
Lesley Manville - Il filo nascosto (Phantom Thread)
Laurie Metcalf - Lady Bird
Octavia Spencer - 
La forma dell'acqua - The Shape of Water (The Shape of Water)
Miglior attore non protagonista
Willem Dafoe - The Florida Project
Woody Harrelson - Tre manifesti a Ebbing, Missouri (Three Billboards Outside Ebbing, Missouri)
Richard Jenkins - 
La forma dell'acqua - The Shape of Water (The Shape of Water)
Christopher Plummer - Tutti i soldi del mondo (All the Money in the World)
Sam Rockwell
 - Tre manifesti a Ebbing, Missouri (Three Billboards Outside Ebbing, Missouri)
Miglior film straniero
Una Donna Fantastica
 (Una mujer fantástica) (Cile)
L’insulto (L'insulte) (Libano)
Loveless (Neljubov) (Russia)
Corpo e anima (Testről és lélekről) (Ungheria)
The Square (Svezia)
Miglior film d’animazione
Baby Boss (The Boss Baby)
The Breadwinner
Coco
Ferdinand
Loving Vincent
Miglior sceneggiatura originale
Guillermo del Toro e Vanessa Taylor - 
La forma dell'acqua - The Shape of Water (The Shape of Water)
Greta Gerwig - Lady Bird
Emily V. Gordon e Kumail Nanjiani - The Big Sick - Il matrimonio si può evitare... l'amore no (The Big Sick)
Martin McDonagh - Tre manifesti a Ebbing, Missouri (Three Billboards Outside Ebbing, Missouri)
Jordan Peele
 - Scappa - Get Out (Get Out)
Miglior sceneggiatura non originale
Scott Frank, James Mangold e Michael Green - 
Logan - The Wolverine (Logan)
James Ivory
 - Chiamami col tuo nome (Call Me by Your Name)
Scott Neustadter e Michael H. Weber - The Disaster Artist
Dee Rees e Virgil Williams - Mudbound
Aaron Sorkin - Molly's Game
Miglior fotografia
Roger A. Deakins
 - 
Blade Runner 2049
Bruno Delbonnel - L'ora più buia (Darkest Hour)
Hoyte Van Hoytema - 
Dunkirk
Rachel Morrison - Mudbound
Dan Laustsen - 
La forma dell'acqua - The Shape of Water (The Shape of Water)
Miglior montaggio
Jon Gregory - Tre manifesti a Ebbing, Missouri (Three Billboards Outside Ebbing, Missouri)
Paul Machliss e Jonathan Amos - Baby Driver - Il genio della fuga (Baby Driver)
Tatiana S. Riegel - I, Tonya
Lee Smith
 - 
Dunkirk
Sidney Wolinsky - 
La forma dell'acqua - The Shape of Water (The Shape of Water)
Migliore scenografia
Paul Denham Austerberry, Shane Vieau e Jeff Melvin
 - 
La forma dell'acqua - The Shape of Water (The Shape of Water)
Nathan Crowley e Gary Fettis - 
Dunkirk
Dennis Gassner e Alessandra Querzola - 
Blade Runner 2049
Sarah Greenwood e Katie Spencer - La bella e la bestia (Beauty and the Beast)
Sarah Greenwood e Katie Spencer - L'ora più buia (Darkest Hour)
Migliore colonna sonora
Carter Burwell - Tre manifesti a Ebbing, Missouri (Three Billboards Outside Ebbing, Missouri)
Alexandre Desplat
 - 
La forma dell'acqua - The Shape of Water (The Shape of Water)
Jonny Greenwood - Il filo nascosto (Phantom Thread)
John Williams - 
Star Wars: Gli ultimi Jedi (Star Wars: The Last Jedi)
Hans Zimmer - 
Dunkirk
Migliore canzone
Mighty River (musica e testi di Mary J. Blige, Raphael Saadiq e Taura Stinson) - Mudbound
Mystery of Love (musica e testi di Sufjan Stevens) - Chiamami col tuo nome (Call Me By Your Name)
Remember Me
 (musica e testi di Kristen Anderson-Lopez e Robert Lopez) - Coco
Stand Up For Something (musica di Diane Warren, testi di Diane Warren e Lonnie Lynn) - Marshall
This is Me (musica e testi di Benj Pasek e Justin Paul) - 
The Greatest Showman
Migliori effetti speciali
Joe Letteri, Daniel Barrett, Dan Lemmon e Joel Whist - The War - Il pianeta delle scimmie (War for the Planet of the Apes)
Ben Morris, Mike Mulholland, Neal Scanlan e Chris Corbould - 
Star Wars: Gli ultimi Jedi (Star Wars: The Last Jedi)
John Nelson, Gerd Nefzer, Paul Lambert e Richard R. Hoover
 - 
Blade Runner 2049
Stephen Rosenbaum, Jeff White, Scott Benza e Mike Meinardus - Kong: Skull Island
Christopher Townsend, Guy Williams, Jonathan Fawkner e Dan Sudick - Guardiani della Galassia Vol. 2 (Guardians of the Galaxy Vol. 2)
Miglior sonoro
Ron Bartlett, Doug Hemphill e Mac Ruth - 
Blade Runner 2049
Christian Cooke, Brad Zoern e Glen Gauthier - 
La forma dell'acqua - The Shape of Water (The Shape of Water)
David Parker, Michael Semanick, Ren Klyce e Stuart Wilson - 
Star Wars: Gli ultimi Jedi (Star Wars: The Last Jedi)
Julian Slater, Tim Cavagin e Mary H. Ellis - Baby Driver - Il genio della fuga (Baby Driver)
Mark Weingarten, Gregg Landaker e Gary A. Rizzo
 - 
Dunkirk
Miglior montaggio sonoro
Richard King e Alex Gibson
 - 
Dunkirk
Mark Mangini e Theo Green - 
Blade Runner 2049
Nathan Robitaille e Nelson Ferreira - 
La forma dell'acqua - The Shape of Water (The Shape of Water)
Julian Slater - Baby Driver - Il genio della fuga (Baby Driver)
Matthew Wood e Ren Klyce - 
Star Wars: Gli ultimi Jedi (Star Wars: The Last Jedi)
Migliori costumi
Consolata Boyle - Vittoria e Abdul (Victoria & Abdul)
Mark Bridges
 - Il filo nascosto (Phantom Thread)
Jacqueline Durran - La bella e la bestia (Beauty and the Beast)
Jacqueline Durran - L'ora più buia (Darkest Hour)
Luis Sequeira - 
La forma dell'acqua - The Shape of Water (The Shape of Water)
Miglior trucco e acconciatura
Daniel Phillips e Lou Sheppard - Vittoria e Abdul (Victoria & Abdul)
Arjen Tuiten - Wonder
Kazuhiro Tsuji, David Malinowski e Lucy Sibbick
 - L'ora più buia (Darkest Hour)
Miglior documentario
Abacus: Small Enough to Jail, regia di Steve James
Icarus
, regia di Bryan Fogel
Last Man in Aleppo, regia di Firas Fayyad
Strong Island, regia di Yance Ford
Visages Villages, regia di Agnès Varda e JR
Miglior cortometraggio
DeKalb Elementary, regia di Reed Van Dyk
The Eleven o'Clock, regia di Derin Seale e Josh Lawson
The Silent Child
, regia di Chris Overton e Rachel Shenton
Watu Wote/All of Us, regia di Katja Benrath e Tobias Rosen
Miglior cortometraggio documentario
Edith+Eddie, regia di Laura Checkoway e Thomas Lee Wright
Heaven is a Traffic Jam on the 405
, regia di Frank Stiefel
Heroin(e), regia di Elaine McMillion Sheldon e Kerrin Sheldon
Knife Skills, regia di Thomas Lennon
Traffic Stop, regia di Kate Davis e David Heilbroner
Miglior cortometraggio animato
Dear Basketball
, regia di Glen Keane e Kobe Bryant
Garden Party, regia di Victor Caire e Gabriel Grapperon
Lou, regia di Dave Mullins e Dana Murray
Negative Space, regia di Max Portner e Ru Kuwahata
Revolting Rhymes, regia di Jakob Schuh e Jan Lachauer