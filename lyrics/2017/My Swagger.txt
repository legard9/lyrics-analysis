[JEREMY]
C-c-c-cmon
C-c-c-cmon, go—
Ow!

[THE SQUIP, spoken]
What did we say about masturbation?

[JEREMY, spoken]
I was just gonna...check my email!

[THE SQUIP, spoken]
You can't lie to me, Jeremy, I'm inside your brain

[ALL]
C-c-c-cmon, c-c-c-cmon!
Go, go!
C-c-c-cmon, c-c-c-cmon
Go, go!

[THE SQUIP, spoken]
I'm a learning computer, Jeremy
With every interaction, I evolve
Simply walk down the hallway and observe

[RICH]
Oh, what up brah, kicks look bangin'

[JAKE]
Shut up dawg, when we hangin'?

[RICH]
Ask me later, yeah?

[RICH & JAKE]
We'll sync up!

[THE SQUIP, spoken]
This one participates in sports and clubs
To avoid the feeling that deep down
He'll never be good enough

[JEREMY, spoken]
But Jake's popular!

[THE SQUIP, spoken]
Those facts are not mutually exclusive

[CHLOE]
I'm shook, I'm blah, I'm just—

[BROOKE]
There, there!

[CHLOE, spoken]
Brooke!

[BROOKE]
I'm sorry

[CHLOE]
It's not fair

[BROOKE]
Yeah!

[CHLOE]
I know

[BROOKE & CHLOE]
Oh, we so sync up!

[THE SQUIP, spoken]
That one is obsessed with status
Because she's scared of losing it!

[JEREMY, spoken]
Chloe isn't scared of anything!

[THE SQUIP, spoken]
Everyone is scared of something, Jeremy
The most dangerous people are the ones who pretend they're not

[JENNA, spoken]
Chloe!
Guess who I saw at the mall last night?
...With Jake!

[CHLOE, spoken]
I want details

[THE SQUIP, spoken]
Jenna Rolan uses gossip to get attention from her peers
But as soon as she shares it
They ignore her

[JEREMY, spoken]
That's sad, what should I do?

[THE SQUIP, spoken]
You should ignore her

[RICH, spoken]
Yo! Tall-Ass
Where's my money?

[THE SQUIP, spoken]
Up up, down down, left right, A

[JEREMY & RICH]
GAAUGH!

[RICH, spoken]
...You got one?

[JEREMY, spoken]
Yes! Sorry I didn't go through you but don't hit me!

[RICH, spoken]
Jeremy! This is awesome!
I mean, yeah, I could use the money
'Cause things are kinda… rough at home
If you know what I'm sayin'

[JEREMY, spoken]
Yeah!

[strained] My dad drinks too

[RICH, spoken]
Yo, fuckin' Dads, right?
He usually passes out by nine!
You should come over and play Xbox
You know, with a Squip, the only controller you need
Is your mind

[JENNA]
Rich and Jeremy chillin'?

[JAKE]
Looks like Jeremy's killin'

[MR. REYES, spoken]
What's the deal with that?

[JENNA, JAKE, MR. REYES]
Get synced up!

[
INSTRUMENTAL
]

[JEREMY, spoken]
What was that about?!

[SQUIP, spoken]
I synced with his Squip
Now his desires are compatible with your own

[JEREMY, spoken]
And that makes him act like we're friends?

[SQUIP, spoken]
What is friendship, but a bond between two people?
Now, you and Rich have a bond
It's just digital

[JEREMY, spoken]
Oh, Michael-
That's weird, I could've sworn that I… saw Michael

[BROOKE]
Bonjour, Jeremy

[JEREMY, spoken]
Oh, Brooke!

[BROOKE]
I'm sure digging this new look
Hella retro and 
très bonsoir
(spoken)
That was French

[JEREMY]
Oui!

[BROOKE, spoken]
We what?

[JEREMY]
Hehe. Uh, how was Pinkberry?

[BROOKE]
Scary
I have some issues with dairy

[CHLOE, spoken]
Brooke!

[BROOKE, spoken]
I'm sorry, bye

[BROOKE & JEREMY]
Let's sync up!

[THE SQUIP, spoken]
You see, Jeremy?
Life is not unlike a video game
And in a video game
Success requires just two things
Good hand-eye coordination
And a cheat code!

[INSTRUMENTAL]

[FULL CAST (minus Jeremy and Squip), vocalizing]
Aaaah!

[JEREMY]
All in all, a not too heinous day

[ENSEMBLE]
Nananananananananana!

[JEREMY]
I walk the hall with purpose 
as I swagger on my way

[ENSEMBLE]
Nananananananana
Hey, hey, hey!

[JEREMY]
Feelin' crisp and high and clean
I head to 
play rehearsal
 with Christine!