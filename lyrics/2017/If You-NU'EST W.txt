Korean Original

[Intro: Ren]
있다면 말할 수 있다면
머리카락처럼 자라버린 그리움을
있다면 또 볼 수 있다면
저 밤하늘보다 까만 두 눈을

[Verse 1: JR]
있다면 지금까지 놓친 필름 속에
있다면 지나갔던 우리 추억 속에
빠져 헤엄쳐 향기는 진하게 배여서
그런지 허전함은 내게 배가 되었어

[Verse 2: JR]
떠나 보내고 나서 널 생각해 봤자
미련하게도 아직 난 꿈인 것 같아
너의 빈자리 그 자린 계속 비워둘게
안녕이란 말은 잠시 넣어둘래

[Pre-Chorus: Aron]
I wanna wanna turn back the time we had
별처럼 쏟아지는 기억들
그 반짝반짝 거리는 시간 속을
헤매다 지쳐 휘청거리는 날

[Chorus: Baekho]
닿을 수 있다면 얼마나 좋을까
꿈에서라도 if i could stay one more night
소설처럼 아니 거짓말처럼 널
다시 만나 활짝 웃을 수 있다면

[Verse 3: JR]
다른 길을 걸어도 시간은 같이 가
다른 풍경을 봐도 하늘은 같이 봐
함께 그려졌었던 도화지에 너란 색
채워지지 않았어 지금 이 그림에

[Verse 4: JR]
널 다시 볼 수 있다면 너와 웃을 수가 있다면
이게 꿈이라서 손 잡을 수가 있다면
얼마나 좋을까 놓지 않을 텐데
안녕이란 말은 잠시 넣어둘래

[Pre-Chorus: Aron, Ren]
I wanna wanna go back to you tonight
숨처럼 참기 힘든 추억들
또 어느샌가 피어난 연기 되어
내 눈을 가려 휘청거리는 날

[Refrain: JR]
널 그때에 널 다시 만날 수만 있다면

[Chorus: Baekho]
닿을 수 있다면 얼마나 좋을까
꿈에서라도 if i could stay one more night
소설처럼 아니 거짓말처럼 널
다시 만나 활짝 웃을 수 있다면

[Hook: Ren]
있다면 있다면 있다면

[Bridge: JR]
다시 한번 같이 걸어갈 수 있다면
아름답던 너의 목소리가 잊혀지지 않았어
웃는 너의 얼굴이 내 기억 속에 생생하게 남아 있어
I miss you I miss you 네가 정말 보고 싶어

[Outro: Ren, Aron]
있다면 돌릴 수 있다면
마치 영화처럼 다시 처음 그 자리로
있다면 내 옆에 있다면
마치 이 모든 게 아픈 꿈처럼
Romanization

[Intro: Ren]
Issdamyeon malhal su issdamyeon
Meorikaragcheoreom jarabeorin geuriumeul
Issdamyeon tto bol su issdamyeon
Jeo bamhaneulboda kkaman du nuneul

[Verse 1: JR]
Issdamyeon jigeumkkaji nohchin pilleum soge
Issdamyeon jinagassdeon uri chueok soge
Ppajyeo heeomchyeo hyanggineun jinhage baeyeoseo
Geureonji heojeonhameun naege baega doeeosseo

[Verse 2: JR]
Tteona bonaego naseo neol saenggakhae bwassja
Milyeonhagedo ajik nan kkumin geot gata
Neoui binjari geu jarin gyesok biwodulge
Annyeongiran mareun jamsi neoheodullae

[Pre-Chorus: Aron]
I wanna wanna turn back the time we had
Byeolcheoreom ssodajineun gieokdeul
Geu banjjakbanjjak georineun sigan sogeul
Hemaeda jichyeo hwicheonggeorineun nal

[Chorus: Baekho]
Daheul su issdamyeon eolmana joheulkka
Kkumeseorado if i could stay one more night
Soseorcheoleom ani geojitmalcheoreom neol
Dasi manna hwaljjak useul su issdamyeon

[Verse 3: JR]
Dareun gireul georeodo siganeun gati ga
Dareun punggyeongeul bwado haneureun gati bwa
Hamkke geuryeojyeosseossdeon dohwajie neoran saek
Chaewojiji anhasseo jigeum i geurime

[Verse 4: JR]
Neol dasi bol su issdamyeon neowa useul suga issdamyeon
Ige kkumiraseo son jabeul suga issdamyeon
Eolmana joheulkka nohji anheul tende
Annyeongiran mareun jamsi neoheodullae

[Pre-Chorus: Aron, Ren]
I wanna wanna go back to you tonight
Sumcheoreom chamgi himdeun chueokdeul
Tto eoneusaenga pieonan yeongi doeeo
Nae nuneul garyeo hwicheonggeorineun nal

[Refrain: JR]
Neol geuttaee neol dasi mannal suman issdamyeon

[Chorus: Baekho]
Daheul su issdamyeon eolmana joheulkka
Kkumeseorado if i could stay one more night
Soseolcheoreom ani geojitmalcheoreom neol
Dasi manna hwaljjak useul su issdamyeon

[Hook: Ren]
Issdamyeon issdamyeon issdamyeon

[Bridge: JR]
Dasi hanbeon gati georeogal su issdamyeon
Areumdabdeon neoui moksoriga ijhyeojiji anhasseo
Usneun neoui eolguri nae gieok soge saengsaenghage nama isseo
I miss you I miss you naega jeongmal bogo sipeo

[Outro: Ren, Aron]
Issdamyeon dollil su issdamyeon
Machi yeonghwacheoreom dasi cheoeum geu jaliro
Issdamyeon nae yeope issdamyeon
Machi i modeun ge apeun kkumcheoreom
English Translation

[Intro: Ren]
If you can say
I like the hair that grows old
If you can see it again
It's black than the night sky

[Verse 1: JR]
If you missed the film
If we were in our memories
I'll swim out and smell the fragrance deeply
I was embarrassed by such a vacancy

[Verse 2: JR]
Let's think about you after we leave
Foolishly, I still feel like a dream
I'll keep you empty
I want to put the word "goodbye" for a while

[Pre-Chorus: Aron]
I wanna wanna turn back the time we had
Memories pouring like stars
In the sparkling hour
I'm so sick and tired

[Chorus: Baekho]
How good would it be if I could reach?
If i could stay one more night
Like a novel, like a lie
If you can meet again and smile

[Verse 3: JR]
Even if you walk the other way
Even if you look at other landscapes, look at the sky
Your drawing on the colored paper, you were painting together
It's not filled

[Verse 4: JR]
If I can see you again, If I can laugh with you
If this is my dream
I wouldn't let go
I want to put the word "goodbye" for a while

[Pre-Chorus: Aron, Ren]
I wanna go back to you tonight
Memories that are hard to bear like breath
I have been delayed
The day I wiggle my eyes

[Refrain: JR]
If I could meet you again at this time

[Chorus: Baekho]
How good would it be if I could reach?
If i could stay one more night
Like a novel, like a lie
If you can meet again and smile

[Hook: Ren]
If you, If you, If you

[Bridge: JR]
If I can walk together again
Your beautiful voice has not been forgotten
Your face is still alive in my memory
I miss you, I miss you

[Outro: Ren, Aron]
If you can turn it
It's like the movie
If you are next to me
Like all these sick dreams