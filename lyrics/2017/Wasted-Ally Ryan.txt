[Verse 1]
I can't go home
But I can't stay here
Something just broke inside
My dear
I'm caught in the branches
Of an old dead tree
I'm in a boat in the desert
Far from the sea

[Chorus]
All I wanted
Was one more chance
Before closing time
All I wanted
Was one more chance

[Verse 2]
Don't put your heart
In the hands of imbeciles
You could end up worse
Than dead or killed
Yeah it's bittersweet
The smiles we knew
Now we’re both creeping around
Like Nosferatu

[Chorus]
All I wanted
Was one more chance
Before closing time
All I wanted
Was one more chance... Oh oh oh
All I wanted
Was one more chance
Before closing time
All I wanted
Was one more chance... Oh oh oh

[Verse 3]
Put the chairs on the tables
And turn down the lights
Let's play our song
And sit as quiet as mice
You see people are cars
And they recall every wreck
And they’ll even the score
On that you can bet

[Chorus]
All I wanted
Was one more chance
Before closing time
All I wanted
Was one more chance… Oh oh oh
All I wanted
Was one more chance
Before closing time
All I wanted
Was one more chance... Oh oh oh
All I wanted
Was one more chance
Before closing time
All I wanted
Was one more chance… Oh oh oh… Oh oh oh