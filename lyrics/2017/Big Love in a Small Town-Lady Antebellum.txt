[Verse 1]
Yeah, we were 16 and teenage broke
Blowin' stop signs and cigarette smoke
Just some bad boys and some good girls
Yeah, summer nights, spent the whole world
In your green eyes, said "Ready, set, go"

[Chorus]
Yeah, we had big love in a small town
What I'd give just to get a little bit back now
Yeah, we fell in just as fast as we fell out
We were too young to settle down and a big love in a small town
In a small town, yeah

[Verse 2]
We were Thunderbirds and Pontiac wild
Flyin' in your front seat, at least for a little while
Baby, tell me, do you ever feel like
Our love was drowned out by real life?
Well, I've been reminiscing 'bout you and I, you and I, oh, you and I

[Chorus]
Yeah, we had big love in a small town
What I'd give just to get a little bit back now
Yeah, we fell in just as fast as we fell out
We were too young to settle down and a big love in a small town
In a small town, baby

[Bridge]
Do you ever think about me
When you close your eyes at night?
Do you ever think about me?
Lost in a dream
In that small town
In a small town

[Chorus]
We had big love in a small town
What I'd give just to get a little bit back now
Yeah, we fell in just as fast as we fell out
We were too young to settle down, always dead set on getting out
Ain't that the thing about big love in a small town?
In a small town, yeah