Hangul

힐끗 쳐다보니 turn on green light
이 자릴 난 떠나긴 싫어
렌즈에 비친 너를 lock on babe
I wanna get you babe
담아볼래 잠시 또 웃어 봐
손 안에 널 찍어둘 good chance
아무도 몰래 ring my heart
너를 바라볼래 secret time (hello)
눈이 마주치면 아무 일 없듯
괜히 딴짓하며 또 한 번
내게 담을래

I fall in love 너는 알까
흔들리는 나의 표정
I can't touch 내 맘이 들킬까 봐
왜 이러니 또 두근거려
Endless time

I'm a real flirt but 네 앞에선
꼼짝도 못해 난 어린애
훈훈한 얼굴 왜 가리니
좀 비켜 줘 언니 quickly
부끄러움은 상승해 또 마주치는
눈 피해 좀 빠르게
끝없는 상상을 만들어내
그 다음은 미뤄 (I'm) not ready yet

좀 더 다가갈까 secret move (hello)
이제 너랑 나랑 둘만 나오게
나를 바라봐 줘 한 번만
내 맘에 찍을래

I fall in love 너는 알까
흔들리는 나의 표정
I can't touch 내 맘이 들킬까 봐
왜 이러니 또 두근거려

Fall in you Focusing you
어디에 넌 있을까
(그대를 향하는 spotlight)
나에겐 너만 보여

아찔하게 내게로 zoom
또 다른 널 알고 싶어
Touch 이런 나 처음이야
I'll be thinking of you

Every day and night
I wanna get you one more time and this love

Romanization

Hilkkeut chyeodaboni turn on green light
I jaril nan tteonagin sirheo
Renjeue bichin neoreul lock on babe
I wanna get you babe
Damabollae jamsi tto useo bwa
Son ane neol jjigeodul good chance
Amudo mollae ring my heart
Neoreul barabollae secret time (hello)
Nuni majuchimyeon amu il eobtdeut
Gwaenhi ttanjithamyeo tto han beon
Naege dameullae

I fall in love neoneun alkka
Heundeullineun naui pyojeong
I can't touch nae mami deulkilkka bwa
Wae ireoni tto dugeungeoryeo
Endless time

I'm a real flirt but ne apeseon
Kkomjjakdo mothae nan eorinae
Hunhunhan eolgul wae garini
Jom bikyeo jwo eonni quickly
Bukkeureoumeun sangseunghae tto
Majuchineun nun pihae jom ppareuge
Kkeuteobtneun sangsangeul mandeureonae
Geu daeumeun mirwo (I'm) not ready yet

Jom deo dagagalkka secret move (hello)
Ije neorang narang dulman naoge
Nareul barabwa jwo han beonman
Nae mame jjigeullae

I fall in love neoneun alkka
Heundeullineun naui pyojeong
I can't touch nae mami deulkilkka bwa
Wae ireoni tto dugeungeoryeo

Fall in you Focusing you
Eodie neon isseulkka
(geudaereul hyanghaneun spotlight)
Naegen neoman boyeo

Ajjilhage naegero zoom
Tto dareun neol algo sipeo
Touch ireon na cheoeumiya
I'll be thinking of you

Every day and night
I wanna get you one more time and this love

English Translation

I get a glimpse of you, turn on green light
I don’t want to leave here now
Your reflection through the lens, lock on babe
I wanna get you babe
I want to capture it, smile once more please
A moment to capture you in my hands, good chance
Without anyone noticing, ring my heart
I want to only gaze at you, secret time (hello)
If our eyes meet, I’ll pretend like nothing happened
Look busy then casually once more
I’ll capture you

I fall in love. Will you notice
My shaking expression?
I can’t touch. What if you catch my feelings for you?
What’s happening to me, my heart is fluttering again
Endless time

I’m a real flirt but in front of you
I can’t even budge – I’m like a little child
Why are you covering your beautiful face
Get out of the view, unnie, quickly
My shyness becomes more apparent
We make eye contact again – I dodge my eyes
And look away quickly – Endlessly, I create fantasies about you
Other things can wait. I’m not ready yet

Should I get closer, secret move (hello)
Now only you and I will be in the image
Look over at me just once
I’ll capture an image of you in my heart

I fall in love. Will you notice
My shaking expression?
I can’t touch. What if you catch my feelings for you?
What’s happening to me, my heart is fluttering

Fall in you, focusing you
Where can you be
Directed towards you – spotlight
You are all I see

Dizzily, towards me, zoom
I want to get to know another side of you
Touch. This is a new side of me
I’ll be thinking of you

Every day and night
I wanna get you one more time and this love