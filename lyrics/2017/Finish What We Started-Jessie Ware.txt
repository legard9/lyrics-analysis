[Verse 1]
Wanna be closer than close
Baby, you already know
I want you, I want you
Tell me the door isn't closed
I'm telling you nobody knows
I want you, I want you

[Pre-Chorus]
I've been waitin' here
I deserve this kiss

[Chorus]
Are we gonna finish what we started?
Only one thing left here to do
Are we gonna finish what we started?
Oh, me and you
Are we gonna finish what we started?
Only one thing left here to do
Are we gonna finish what we started?
Oh, me and you

[Verse 2]
When you look at me that way
I run out of reasons to say, "I don't want you"
But I want you
Words from my mouth can't explain
What the rest of my body is sayin'
I want you, I want you

[Pre-Chorus]
I've been waitin' here
I deserve this kiss

[Chorus]
Are we gonna finish what we started?
Only one thing left here to do
Are we gonna finish what we started?
Oh, me and you
Are we gonna finish what we started?
Only one thing left here to do
Are we gonna finish what we started?
Oh, me and you

[Outro]
Are we gonna finish what we started?
Only one thing left here to do
Are we gonna finish what we started?
Oh, me and you
Are we gonna finish what we started?
Only one thing left here to do
Are we gonna finish what we started?
Oh, me and you