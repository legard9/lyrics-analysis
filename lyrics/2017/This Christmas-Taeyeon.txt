Hangul
왠지 따뜻했던 그날 밤
눈꽃이 내려앉은 두 볼에
스치듯 가벼운 너의 입술이
난 아직 잊혀지지가 않아
Beautiful night 그날의 널 기억해
마치 시간을 되돌린 듯 날 찾아와주길
Christmas 꿈결같던 Christmas
기억하고 있어 네가 남긴
달콤한 속삭임 커져간 떨림 Oh
Christmas 네가 있던 Merry Christmas
영원할 것 같았던 간절한 이 마음을 전해
I pray on This Christmas
문득 생각이나 그날 밤
유난히 따뜻했던 두 손이
얼어붙은 내 맘을 녹여주던
난 왠지 어제처럼 느껴져
Beautiful night 그날의 널 기억해
마치 운명이 우릴 채운 듯 날 찾아와주길
Christmas 꿈결같던 Christmas
기억하고 있어 네가 남긴
달콤한 속삭임 커져간 떨림 Oh
Christmas 네가 있던 Merry Christmas
영원할 것 같았던 간절한 이 마음을 전해
I pray on This Christmas
찬란했던 그 겨울 앞에 내가 서있어
다시 찾아온 계절이 너를 반기고 함께 와주길
Christmas 꿈결같던 Christmas
기억하고 있어 네가 남긴
달콤한 속삭임 커져간 떨림 Oh
Christmas 네가 있던 Merry Christmas
영원할 것 같았던 간절한 이 마음을 전해
I pray on This Christmas
간절한 이 마음을 전해
I pray on This Christmas
Romanization
Waenji ttatteushaessdeon geunal bam
Nunkkochi naelyeoanjeun du bore
Seuchideut gabyeoun neoui ibsuri
Nan ajik ijhyeojijiga anha
Beautiful night geunarui neol gieokhae
Machi siganeul doedollin deut nal chajawajugil
Christmas kkumgyeolgatdeon Christmas
Gieokhago isseo nega namgin
Dalkomhan soksagim keojyeogan tteollim Oh
Christmas nega issdeon Merry Christmas
Yeongwonhal geot gatassdeon ganjeolhan i maeumeul jeonhae
I pray on This Christmas
Mundeuk saenggagina geunal bam
Yunanhi ttatteushaessdeon du sori
Eoreobuteun nae mameul nokyeojudeon
Nan waenji eojecheoreom neukkyeojyeo
Beautiful night geunarui neol gieokhae
Machi unmyeongi uril chaeun deut nal chajawajugil
Christmas kkumgyeolgatdeon Christmas
Gieokhago isseo nega namgin
Dalkomhan soksagim keojyeogan tteollim Oh
Christmas nega issdeon Merry Christmas
Yeongwonhal geos gatassdeon ganjeolhan i maeumeul jeonhae
I pray on This Christmas
Chanranhaessdeon geu gyeoul ape naega seoisseo
Dasi chajaon gyejeori neoreul bangigo hamkke wajugil
Christmas kkumgyeolgatdeon Christmas
Gieokhago isseo nega namgin
Dalkomhan soksagim keojyeogan tteollim Oh
Christmas nega issdeon Merry Christmas
Yeongwonhal geos gatassdeon ganjeolhan i maeumeul jeonhae
I pray on This Christmas
Ganjeolhan i maeumeul jeonhae
I pray on This Christmas
English Translation
That night was so warm for some reason
Snowflakes fell on my cheeks
And your lips lightly brushed against them
I still can’t forget
Beautiful night, I remember you that day
I hope you come to me like turning time back
Christmas, a dream-like Christmas
I want to remember
The growing trembling at your sweet whispers
Christmas, when you were here, Merry Christmas
I’ll give you earnest heart that I thought would last forever
I pray on This Christmas
I suddenly remember that night
Your especially warm hands
Melted my frozen heart
Feels like it was only yesterday
Beautiful night, I remember you that day
I hope you come to me like destiny filling us up
Christmas, a dream-like Christmas
I want to remember
The growing trembling at your sweet whispers
Christmas, when you were here, Merry Christmas
I’ll give you earnest heart that I thought would last forever
I pray on This Christmas
I’m standing before that brilliant winter
I hope this season will greet you and come together
Christmas, a dream-like Christmas
I want to remember
The growing trembling at your sweet whispers
Christmas, when you were here, Merry Christmas
I’ll give you earnest heart that I thought would last forever
I pray on This Christmas
I’ll give you earnest heart
I pray on This Christmas