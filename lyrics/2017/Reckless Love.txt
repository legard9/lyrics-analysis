Lindy & The Circuit Riders - Beautiful and Glorious (Live)
Michael W. Smith - Healing Rain
Aline Barros - Use My Life
Dan Bremnes - Weakness
Steven Curtis Chapman - Dive (feat. Ricky Skaggs)
For All Seasons - Light & Momentary
Lauren Daigle - Look Up Child (Recorded at Sound Stage Studios Nashville)
DJ PV - Tu Amor (feat. Kike Pavón & Melissa Hermosillo)
UPPERROOM - To the One (Live)
Michael W. Smith - Washed Away / Nothing but the Blood
Sarah Kroger - Pieces (feat. Audrey Assad)
Local Sound - Never Far
Tobe Nwigwe - CAGED BIRDS (feat. LaNell Grant)
Marcos Witt - Un Millón (feat. Kelly Spyker)
London Gatch - Be My Heart
Madison Hughes - Tossed at Sea (feat. Yongrush)
Influencers Worship - † (just the cross) (Live)
Dj Em D, Shope - Away
Lauren Daigle - Don't Dream It's Over (Recorded at Sound Stage Studios Nashville)
Lindy & The Circuit Riders - Mark My Hands (Live)
Wimberley - Eyes to Zion
Jadi Torres - Contigo (feat. Ariel Kelly)
Deraj - Everything
HGHTS - Somebody (feat. Kristen Hicks)
Manny Montes, Mic Kid, Jaydan - Te Siento
Dru Bex - Compass (feat. Ada Betsabe)
Uzuhan - God Be My Witness (feat. Chai)
Will Ruck - No Weapon
Lacy O'Connor - Home
Mike & Mel Gabriel - No Greater Love
Plumb - Somebody Loves You (Live From Ocean Way)
Danilo Montero - Te Alabaré Bueno es Alabar Salmo 84 Popurrí
Dera Val - I Trust in You
The Vicious Vic - Brave
Beacon Light - Waste No Time
Keith & Kristyn Getty - The Lord Is My Shepherd (Psalm 23) (Live)
Empty Isles - Hope Finds Me
HillaryJane - Deja Vu
Efrain - Where Would I Go
The Creak Music - Reckless Love
Whys of the Wise - Okay
Joshua Leventhal - L E P E R S (feat. Atmos One)
SoulBox - Boundaries
Sarah Farias - O Rosto de Cristo**
Homegrown Worship - We're Alive
Momentum Music - I Need You
Jacob Stanifer - Mary Kate
Evan and Eris - Juzz Do It
Roy Tosh - Close (feat. Evan & Eris)
Malik Nichols - God My All In All
Levi Parker - Dues (feat. Kyle Travis, Vic Sage, Harrisxn)
Den svenska björnstammen - När jag blundar vill jag va nån annan
Of the Land - Anchor
James Gardin, TheyCallMeHeat - World (feat. Add-2)
Marcos Freire - Na Terra Como No Céu (Here As In Heaven) (feat. Fernanda Brum)
Yemi Ayeni - Isaiah Song
The O'Neill Brothers Group - Gentle Wind
Melania Pacheco - Love Is Wonder
Beyond The Walls - God of Breakthrough
Lindy & The Circuit Riders - Stand in Awe (Live)