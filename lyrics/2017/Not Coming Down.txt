It goes on and on and on
TiK ToK on the clock, Don't stop
Remind me of everything tonight

Zone on
Yes, I'm in the zone I used to know
I get a good feeling
Dreaming about the things that we could be

So it's gonna be forever
You already know
That's the real me when I'm in the spot
Don't believe me just watch

Oh, is it too late now to say "I need a one dance"?
And then we start to dance, and now I'm singing like
"Oh, I want something just like this"
Might be over now, but I feel it still
I always had a feeling...

(Duh)

I want you to be happier
Because I'm happy when I'm wiser and I'm older
'Cause you make me feel like it goes on and on and on, yeah (Yeah)

(Nananananana)

Thinking out loud
You're not coming down
Keep it on a high note
Tonight you got me holdin' on to somethin'
I know I'm not the only one

If you feel like, like a rockstar (star)
This is what you came for
It feels right when I'm in my zone
Oh, doesn't mean I'm over the way that things have been, ooh

Wish we could turn back time
Ten years older but look what you taught me

I want you to stay, stay with me
I need more hours with you
I can't forget you (Bom bom bi bom bi dum)

She said oh oh oh I love it (Ooh-ooh-ooh)
Lightning then the thunder, whoa
Don't let me down, whoa oh
What can I say?

Say you won't let go
You watch me never let go
Just say you won't let go 'til I can't no more
Are you ready for, ready for the rest of my life (Yes, please)
I think your love would be too much
I tell myself

We've come a long way from where we began
This is crazy!
After all these years, I'm on your magical mystery ride
We've come too far
What do you mean?
Can we go back to the stars?

But if you close your eyes
Time flies like nothing changed at all?
And if you close your eyes
I don't know why, it feel like you've been here before I got 'em sayin', "Wow"...

Oh my gosh, I like it like that
Tell me that you've had enough
I got this music I just can't deny

We ain't ever getting older

You know I'm all about that dream or a genie or a wish
I'm all about that place much simpler than this, and you know me
Just hold on, we're going home
(Turn down for what)

I'm all about that glitz and glam and the fashion
I'm all about that pandemonium and all the madness
Won't let you forget me
Baby, what are you waiting for? (ah, hey!)