[Intro]
Stars are just wishes come true
Yeah

[Chorus 1]
I used to worship at your altar
Underneath the covers
I used to worship at your altar
I used to think I was your lover

[Post-Chorus]
So what did I do?
Was it my fault?
Could I have changed it?
And why did you go?
It's not like you're blameless
You should be ashamed of how you behaved
Making that thing some kind of game
Holding my hand, telling me meet me in five years
(I use to worship, at your altar)
Five years

[Verse 1]
But you're a different kind of person
Not the person that I once knew
Not the girl I fell in love with
Not the god I made you into

[Bridge]
You do what you want
Sleep with who you want
I can't stop you
Even if I try, the whole time, you will lie
Then you give me one more line about doing lines
You say that you're just living your life
That I should do my time, but I already did my time

[Chorus 2]
I used to worship at your altar
I thought you'd wash away my pain
Thought your name made you a river
After all it's just a name

[Post-Chorus 2}
So what did I do?
Was it my fault?
Could I have changed it?
And why did you go?
It's not like you're blameless
Was it my fault?
Could I have changed it?

[Bridge]
You do what you want
Sleep with who you want
I can't stop you
Even if I try, the whole time, you will lie
Then you give me one more line about doing lines
You're on my mind

[Breakdown]
Oh, yeah, that's right, but I am fine
Yeah, I'm alive, I'm alive
I won't worship at your shrine again
And no I do not want to be your friend
I feel relieved to know that one day all of this will end
Such a weight off of my shoulders
Momma told us, "Remember, you're not infinite
It's better not to speak and maybe you should get to listening
Don't you worry about the money, honey, or the internet
Cause you're alive, cause you're alive
Baby, you're alive."
No need to worship at an altar
No need to worship at an altar