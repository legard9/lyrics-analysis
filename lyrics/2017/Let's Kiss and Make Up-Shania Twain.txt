[Verse 1]
Let's sit down and talk it over
Take my shoulder tonight
Look at me and tell me what is
On your mind this time

[Pre-Chorus]
Gonna stay here til you got a smile on your face
I'm here no matter what
Everything else can wait

[Chorus]
Let's kiss and make up now
Let's kiss and make love
Don't wait another minute, let's go with it
Kiss me and let's make up now

[Verse 2]
Let's be honest, let's be open
We're not broken, not yet
It's not easy tryin' to please me
I'm not perfect, I know that

[Pre-Chorus]
Gonna stay here til you got a smile on your face
I'm here no matter what
Everything else can wait

[Chorus]
Let's kiss and make up now
Let's kiss and make love
Don't wait another minute, let's go with it
Kiss me and let's make up now

[Bridge]
Talk to me, it's alright, I'm open
Whoa, don't try, we're not broken, not broken
I know that we can do this
Come, baby, do it right now (Make up now)
(Come, baby, do it right now)
(Make up now)

[Chorus]
Let's kiss and make up now
Let's kiss and make love
We'll do it like we meant it
Let's go with it
Kiss me and let's make up now
Kiss me and let's make up now
Kiss me and let's make up now