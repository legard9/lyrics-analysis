[Verse 1]
Life is like a circus wheel
And I never know how to feel
You’re still living in the way back then
But I never meant to hurt you or to lose a friend
'Cause time's gone by on a runaway train
And there's nothing we can do to bring it back again

[Pre-Chorus 1]
But you
You’re looking at me
Just say what you want
Just say what you need
'Cause I
Was meant to be free
And come as I go
And go as I please

[Chorus]
Did you think I’d stay?
Well, you should turn the other way
'Cause we’re never going back again
All you did was stay
Holding on to yesterday
Well, it’s never coming back again

[Verse 2]
Life, a couple hundred miles away
And I, remember you like it was yesterday

[Pre-Chorus 2]
So, why
Why you looking at me?
It's not what you want
It’s not what you need

[Chorus]
Did you think I’d stay?
Well, you should turn the other way
'Cause we’re never going back again
All you did was stay
Holding on to yesterday
Well, it’s never coming back again

[Post-Chorus]
You can stay in the past
You can live a lie
Say I’ve changed, call me cold
Call me what you like
And never know what it’s like
On the other side, oh
You can stay in the past
You can live a lie
Say I’ve changed, call me cold
Call me what you like
And never know what it’s like
On the other side

[Chorus]
Did you think I’d stay?
Well, you should turn the other way
'Cause we’re never going back again
All you did was stay
Holding on to yesterday
Well, it’s never coming again

[Post-Chorus]
You can stay in the past
You can live a lie
Say I’ve changed, call me cold
Call me what you like
And never know what it’s like
On the other side
Never know what it’s like
On the other side, oh

[Outro]
You can stay in the past
You can live a lie
Say I’ve changed, call me cold
Call me what you like
And never know what it’s like
On the other side
Never know what it’s like
On the other side