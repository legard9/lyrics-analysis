[Verse 1]
Every night before I, go to sleep
I listen long to the, bustling streets
And I wonder, how long, I’m gonna settle
Oh I’m gonna settle

[Verse 2]
They raised me up, on brick and bread
Concrete jungle living, in your head
And I wonder, how long
I’m gonna settle
I’m gonna settle

[Verse 3]
My mama, was told by them
And my daddy, he was told by them
Oh man, won’t you be the one
To help me understand?

[Bridge 1]
No answer, just waiting
Anticipating why

[Interlude & Instrumental]
Hmm --
Ooh --
Hmm --
Hmm --

[Verse 4 & Outro]
A little patchway up, in the sky
It says “you can leave here, any time you like”
And I wonder, how long, I’m gonna settle
How long I’m gonna settle
How long
I’m gonna settle