[Intro - Lysian]
Jetlag
Sometimes people just can't understand that
I'm just focused, I ain't tryna be like niggas wa-up?
It's just like I'm in my zone
Me and my microphone
(Yeah)

[Verse 1 - Lysian]
Sometimes I feel like a foreigner
I got my feet on the ground but, my mindset on the cloud and
 
The gap is getting bigger as I encounter
A lot of nonsense
Lead by my absence
I'm the type person that makes statements
Instead of making arguments
I gets busy to make it look easy
May all my brothers in LBV know that this ain't easy
Believe me I got a test of chemistry next week
So I'm tryna get the mix and master done by next week
And find some sleep
Nanh, til' I'm done I won't sleep
It's 5am in Houston, 6 in Washington
As I picture myself rising like the sun
Sparkle like the sun
I remain in the shade for the month to get it done
I live, create, sleep in the booth
This is my life nothing else but the truth

[Hook -  Lysian]
This is 'bout me not you
This is my life, not yours
So please would you please would you
Not tell me what to do
(X 2)

[Verse 2 - Lysian]
Sometimes I feel like I'm too careless
People telling me let's go party at the No Stress
But I guess I'm too focused
God is my witness
T.H.E. Box is my right hand
My partner for the master plan
We in America, yes can! Hell yeah, yes we can!
Cause we put our mind into it
Matter fact we masterminding to it
I'm dedicated to it and to be honest I'm love with music
I love listening to it, but I adore makin it
And I know dreams become reality when you make it
It's all fake when you make it
That's when enemies become besties
Besties become enemies, ladies become groupies
Groupies want babies
The change change, the peep change
Your hole life change
I forsee it all, so I'll never change
Unless you act all strange
Cause your life hasn't change
If not I'll never change

[Hook -  Lysian]
This is 'bout me not you
This is my life, not yours
So please would you please would you
Not tell me what to do
(X 2)

[Verse 3 - Lysian]
Sometimes I feel like I write cause I have to
I feel like I'm doing my own therapy
I don't mind being lonely but sometimes
I wish I had people to talk to, but what would be the point
Since I'm
Misunderstood by my people
Perceived as dismal, it's all paradoxal
Greedy and thankful
My will's beyong success, I won't my songs to be timeless
Tell the devil that my soul's priceless
I'm not tryna be famous, big dreams like Micromegas
May I leave immaterial things as my legacy
May I be immortal
May I and my vocal be known as special
May I be on a pedestal
Cause I'm not average, yeah I'm not average
I don't go out for a beverage
I'm savage, I go out to look at surroundings, buildings
Paintings, all the things
Inspiring enough to inspire the boarders
Enough to inspire the others
Enough for them to cross the border
And to become boarders of the movement
We the new artists
This the Genesis
Of the movement