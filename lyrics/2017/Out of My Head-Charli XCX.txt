[Intro]
Eh-eh-X-C-X

[Verse 1: Tove Lo]
Glitter in my sheets
Dancin' on, no sleep
I don't learn, wanna burn, wanna turn all the way up, yeah
I don't learn, wanna burn in the dirt 'til I'm out of luck, yeah

[Pre-Chorus: Tove Lo]
Pills and potions and terrible things
Heart on the floor when the telephone rings
All of the lies, I just wanna believe
Drop all my morals, I just wanna sin

[Chorus: Alma]
I'm out of my, out of my head
Out of my head
Please, get out of my, out of my, out of my
Out of my head
Please, get out of my, out of my, out of my head
Out of my head
Please, get out of my, out of my, out of my head
Out of my head, out of my head

[Post-Chorus: Tove Lo]
Need you, need you, need you out
Need you, need you, need you out

[Verse 2: Charli XCX]
Party with my tears
Swayin' with my fears, yeah, yeah, yeah, yeah
Let 'em dry, get me high, I'll get by if I'm gettin' love, yeah
Let 'em dry, get me high, I get by with a little love
Now give me

[Pre-Chorus: Tove Lo & Charli XCX]
Pills and potions and terrible things
Heart on the floor when the telephone rings
All of the lies, I just wanna believe
Drop all my morals, I just wanna sin

[Chorus: Alma]
I'm out of my, out of my head
Out of my head
Please, get out of my, out of my, out of my
Out of my head
Please, get out of my, out of my, out of my head
Out of my head
Please, get out of my, out of my, out of my
Out of my head, out of my head
I'm out of my, out of my head
Out of my head
Please, get out of my, out of my, out of my (head)
Out of my head
Please, get out of my, out of my, out of my head
Out of my head
Please, get out of my, out of my, out of my (head)
Out of my head, out of my head
(I'm out of my, out of my)

[Bridge: Tove Lo, Alma & Charli XCX]
You got me doin' all this stupid shit
You fuck me up like this
Secretly, I'm into it though
You got me doin' all this stupid shit
You fuck me up like this
Secretly, I'm kinda into it though
(You fuck me up like this)
You got me doin' all this stupid shit
You fuck me up like this
Secretly, I'm into it though
Got me doin' all this stupid shit
You fuck me up like this
Secretly, I'm kind of into it
I'm out of my, out of my head

[Chorus: Alma]
Out of my head
Please, get out of my, out of my, out of my head
Out of my head
Please, get out of my, out of my, out of my head
Out of my head
Please, get out of my, out of my, out of my head
Out of my head, out of my head
I'm out of my, out of my head
Out of my head
Please, get out of my, out of my, out of my (head)
Out of my head
Please, get out of my, out of my, out of my head
Out of my head
Please, get out of my, out of my, out of my head
Out of my head, out of my head

[Outro: Tove Lo & Alma]
Need you, need you, need you out
I'm out of my, out of my head
Need you, need you, need you out
I'm out of my, out of my head
Out of my head
Please, get out of my, out of my, out of my head
Out of my head, out of my head
Please, get out of my, out of my, out of my head
Out of my head, out of my head