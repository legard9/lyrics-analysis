This girl, Carmen, my friend
Born safe, dead end

She used to talk a lot of, girl stuff
Her eager bluff, wasn't quite enough

So she defied the night, like a shooting star
Only to fall, with her unlucky charm

Giving away pieces, of her complete body
Little by little, no grace no glory
Giving away pieces, of her complete body
Little by little

Mary was born a virgin, and died a whore
Seeking solace to her soul

Comfort and money from men
Men who’d manage, to ignore

So she fell in love with that one son
Who already gave away everything

Giving away pieces, of her complete body
Little by little, no grace no glory
Giving away pieces, of her complete body
Little by little, no grace no glory

My friend Carmen, godsend
Born safe, dead end

She lost her challenge, she lost her charm
Lost hope, for her heart to mend

She's in love with a dead man
Yeah she's in love with a dead man