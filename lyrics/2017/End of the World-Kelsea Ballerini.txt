[Verse 1]
Dark as midnight and lost in it
Insecure and just done with it
Love gave up, so I stop believing
I don’t know how to get through feelings

[Chorus]
I thought it was the end of the world
But it kept spinning round, round, round
And then the sun came up again
And I stopped looking down, down, down
I thought the stars in the sky and in my eyes had lost their shine
But just like a lifeline right on time
I met you at the end of the world

[Verse 2]
The sky kept falling, but we danced in it
I was done with love, but second chance did it
You cleared the smoke and picked up the wreckage
Got to go through Hell to get to Heaven

[Chorus]
I thought it was the end of the world
But it kept spinning round, round, round
And then the sun came up again
And I stopped looking down, down, down
I thought the stars in the sky and in my eyes had lost their shine
But just like a lifeline right on time
I met you at the end of the world, the world, the world

[Bridge]
I thought the cold would last forever
I didn’t know that there was something better
Waiting at the end of the world, the world, the world

[Chorus]
I thought it was the end of the world
But it kept spinning round, round, round
And then the sun came up again
And I stopped looking down, down, down
I thought the stars in the sky and in my eyes had lost their shine
But just like a lifeline right on time
I met you at the end of the world, the world, the world

[Outro]
I thought it was the end of the world
I thought it was
Dark as midnight and lost in it
The sky kept falling, but we danced in it