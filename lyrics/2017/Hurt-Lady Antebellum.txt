[Verse 1]
If my memory gets the best of me
Then I'll always find an excuse
Yeah, I'll make believe, re-write history
Ignite a spark I can't undo
It always starts out simple like a conversation
Before I know it, I'm lost in your illumination

[Chorus]
If you catch my eye across a crowded room
I'll fall into the atmosphere surrounding you
If you pull me close just to disappear
The chances are, I'd wait for you a thousand years
If you light the fuse, you know that I'll react
If you're reckless with your love just to take it back
You could hurt somebody like that

[Verse 2]
You're a carnival on a summer night
Gone too soon every time
Yeah, it's beautiful how you burn so bright
In the waste land you leave behind

[Chorus]
If you catch my eye across a crowded room
I'll fall into the atmosphere surrounding you
If you pull me close just to disappear
The chances are, I'd wait for you a thousand years
If you light the fuse, you know that I'll react
If you're reckless with your love just to take it back
You could hurt somebody like that

[Bridge]
If you call me up at 3AM
I'll run to the rescue time and time and time again

[Chorus]
If you pull me close just to disappear
The chances are, I'd wait for you a thousand years
If you light the fuse, you know that I'll react
If you're reckless with your love just to take it back
You could hurt somebody like that
You could hurt somebody like that
You could hurt somebody like that