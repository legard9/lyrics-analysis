[Verse 1: Katie]
Hello little baby
You're a princess just like me
Bet you're thinking maybe
It's a pretty cool thing to be
But soon you'll see that everyone expects a lot from you
They say that there a princess should and shouldn't do

[Chorus: Katie]
But you and me
We, we know better

[2x]
1, 2, 3 together, clap together, snap together
You and me together, knees together, freeze together
Up or down together, princess crowned together
Always be together, you and me

[Verse 2: Kristen]
They say a princess is full of charm and grace
They say she always knows her place
They say a princess wears pink and frilly clothes
They say she never laughs, and snorts milk out her nose

They say she's calm, they say she's kind
They say she never speaks her mind
Or freezes Nanny's big behind!

[Chorus: Together]
But you and me
We, we know better
(Nanny: You girls are in so much trouble, when I tell your father...)
Anna: How come you can do that and I can't?
Elsa: I don't know. I wish you could though.[Verse 3]
They say a princess is super duper sweet
She doesn't fight, she doesn't sweat, and you never see her eat
They say a princess doesn't climb and scrape her knee
They say a princess wouldn't freeze her tutor's tea
They say she poised, they say she's fair
She never mentions underwear!
Or longs to see the world out there...

[Chorus]
But you and me
We...

[Bridge]
Have big ideas of our own
For the distant someday when we're grown
When I'm queen (And I'm your right hand)
You'll get to travel throughout the land!
(I'll them of my sister, and the magic things she can do)
We'll take care of our people
And they will love me and you

[Outro]
No one can tell us what a princess should be
As long as we're together, you and me...