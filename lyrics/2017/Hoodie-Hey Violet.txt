[Verse 1]
You'd probably think I was psychotic (If you knew)
What I still got in my closet (Sad but true)
I slip it all over my shoulders
Something I'll never get over
It makes me feel a little bit closer to you

[Pre-Chorus]
I can't keep your love, I can't keep your kiss
Gave you everything and all I got was this

[Chorus]
I'm still rocking your hoodie and chewing on the strings
It makes me think about you, so I wear it when I sleep
I kept the broken zipper and cigarette burns
Still rocking your hoodie, baby, even though it hurts
Still rocking your...

[Verse 2]
I used to put my hand in your pockets (Holding on)
The smell of your cologne still on it (But you're still gone)
Slip it on over my shoulders
Someone I'll never get over
Makes me feel a little bit closer to you

[Pre-Chorus]
I can't keep your love, I can't keep your kiss
Gave you everything and all I got was this

[Chorus]
I'm still rocking your hoodie and chewing on the strings
It makes me think about you, so I wear it when I sleep
I kept the broken zipper and cigarette burns
Still rocking your hoodie, baby, even though it hurts
Still rocking your hoodie and chewing on the strings
It makes me think about you, so I wear it when I sleep
I kept the broken zipper and cigarette burns
Still rocking your hoodie, baby, even though it hurts
Still rocking your...

[Bridge]
If you want it back, if you want it back
I'm here waiting
Come take it back, come take it back
If you want it back, if you want it back
I'm here waiting
Come take it back, come take it back

[Chorus]
I'm still rocking your hoodie and chewing on the strings
It makes me think about you, so I wear it when I sleep
I kept the broken zipper and cigarette burns
Still rocking your hoodie, baby, even though it hurts
I'm still rocking your hoodie and chewing on the strings
It makes me think about you, so I wear it when I sleep
I kept the broken zipper and cigarette burns
Still rocking your hoodie, baby, even though it hurts
I'm still rocking your hoodie and chewing on the strings
It makes me think about you, so I wear it when I sleep (Every time I sleep)
I kept the broken zipper and cigarette burns
Still rocking your hoodie, baby, even though it hurts
I'm still rocking your hoodie (Hoodie, hoodie, hoodie)