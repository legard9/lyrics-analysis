(Na na nanana nana, na na nanana nana na) Eden
(Na na nanana nana, na na nanana nana na) yeah
Too far gone to turn around, as I make it into town
I said driver, take my suitcase, where's that good time goin' down
Hit the freeway to the skyline, need some faster ecstasy
If she's got a prize, then I've got the money
She said for you the first one's free
And I thought I saw Jesus in the hotel room
As we made love on the balcony
With a drink in my hand, like they said it would be, yeah
So this is Eden, so this is Eden
And baby I knew it, that you'd lead me to it, I don't wanna get out
(Na na nanana nana, na na nanana nana na) Eden
Feel the pressure rise in the elevator, make our way down to the street
Soul sister, sing a song for me, drunks and junkies shuffle their feet
It's a flea rang circus, down town Mavelone
We're crashing and burning, for twenty-four hours the party goes on, yeah
So this is Eden, so this is Eden
And baby I knew it, that you'd lead me to it, nobody gets out
(Na na nanana nana, na na nanana nana na) Eden, nobody gets out
(Na na nanana nana, na na nanana nana na) of Eden, no no

Breathe in, breathe out, check in, check out
It's a wild weekend, ghost train ride to the other side
It's a funhouse suicide, it's making me feel more dead than alive, yeah
So this is Eden, so this is Eden
And baby I knew it, that you'd lead me to it
So this is Eden, so this is Eden
And baby I knew it, that you'd lead me to it, yeah
(Na na nanana nana, na na nanana nana na) Eden, nobody gets out
(Na na nanana nana, na na nanana nana na) of Eden, no no no no...
(Na na nanana nana, na na nanana nana na)
(Na na nanana nana, na na nanana nana na)
Check in, you won't check out