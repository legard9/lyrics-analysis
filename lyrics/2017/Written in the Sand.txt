I'm a long list with no time
Sunset panic on the street
Sugar and light bulbs
The milk of kindness is behind us now

With all those stones in your coat
Did you think they wouldn't know?

The tea leaves of trashed sheets
Dirty needles and sweets
Zero to heaven in seven
A lifetime, a nanosecond

All the sand in your glass
Is going by so fast

The radio is playing our tune
I love it, could you turn it down?
The thought of you crying in my room
I miss you, could you come around sometime?

When the night comes down
The world becomes a room
Under a microscope
With a lab coat and glue

I'm fixing this hole
With everything I knew

The music is making my head split
I love it, could you turn it off?
The thought of you is tearing me in two
I miss you, could you come around sometime, sometime?

This list is what went right
Your name is written twice
'Cause we live like astronauts
And our missions never cross

The stakes are high
We're standing by

There used to be a hundred ways to put my arms around you
Every one seemed new, natural and true
Perfecting loneliness 'til nothing's holding us
Consider Earth, we could be far worse