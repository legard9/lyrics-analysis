[Verse 1]
Let winter break
Let it burn 'til I see you again
I will be here with you
Just like I told you I would
I'd love to always love you
But I'm scared of loneliness
When I'm, when I'm alone with you

[Chorus]
I know it's hard
Only you and I
Is it all for me?
Because I know it's all for you
And I guess, I guess
It is only, you are the only thing I've ever truly known
So, I hesitate, if I can act the same for you
And my darlin', I'll be rooting for you
And my darlin', I'll be rooting for you

[Verse 2]
And where did she go?
Truth left us long ago
And I need her tonight because I'm scared of loneliness with you, baby
And I should let it go
But all that is left is my perspective, broken and so left behind again

[Chorus]
I know it's hard
Only you and I
Is it all for me?
Because I know it's all for you
And I guess, I guess
It is only, you are the only thing I've ever truly known
So, I hesitate, if I can act the same for you
And my darlin', I'll be rooting for you
And my darlin', I'll be rooting for you