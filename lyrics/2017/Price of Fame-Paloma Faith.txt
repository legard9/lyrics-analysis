[Verse 1]
It doesn't matter what you give
It's never enough
You can never have a bad day girl
Cause that would be tough for me
It doesn't mater what you say
We'll never believe it
So pour another whiskey babe
You look great

[Pre-Chorus]
She was only woman
Not built to be a superstar
She carried a burden
A gift to cause a thousand scars
Too broken down and frail to carry such a heavy load
She walked into darkness and we stood
To watch her go

[Chorus]
Didn't see you fade away
But that's the price you paid
Singing through your hurt and pain
Should have been another way
Take it just a bit too late
Don't believe it
Oh lord, it was the price of fame

[Verse 2]
So can I get a photograph?
Even if you don't want it
I don't care if you got troubles girl
Just please don't be honest with me
Oh, can you be my laughing stock?
Don't take it so personal
Just pour another whiskey babe
You look great

[Pre-Chorus]
She was only woman
Not built to be a superstar
She carried a burden
A gift to cause a thousand scars
Too broken down and frail to carry such a heavy load
She walked in to darkness and we stood
To watch her go

[Chorus]
Didn't see you fade away
But that's the price you paid
Singing through your hurt and pain
Should have been another way
Take it just a bit too late
Don't believe it
Oh lord, it was the price of fame

[Outro]
Didn't see you fade away
But that's the price you paid
Singing through your hurt and pain
Should have been another way
Take it just a bit too late
Don't believe it
Oh lord, it was the price of fame
Yeah lord, it was the price of fame
Sing lord, price of fame
It was the price of fame