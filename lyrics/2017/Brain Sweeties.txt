(If you find this page on any site other than Genius.com, you are viewing a mirror site. Be aware that the page may be outdated, and mirror sites will not correctly mirror the annotations that explain the slang)
Is this guide helpful? Have any comments, suggestions? Leave a comment in the thread.
Template Annotation - Simply copy/paste the annotation into yours - makes doing the formatting easier.
NavigationThe above annotation has quick links that'll take you to a specific part of the page! You may also press `ctrl+f` on your keyboard, which'll enable you to search specific words (mobile browsers will have a 'search text' option in a menu).

Slang Types:• Cockney Rhyming Slang
• MLE / Multicultural London English / UBE / Urban British English (aka Nang Slang / Nang Teen or Jafaican / Blinglish / Tikkiny / Blockney)
• Polari (aka Palare / Palyaree / Palary)

#:• 1 man up / One man up
• 2 man step / Two man step
• 3 man step / Three man step
• 4-door / Four door
• 5-0 / Five O / Five-O
• .9 / .9mil / Nine / Nina
• 10 Toes / 10 Toe / Ten toes / Ten toe
• 12
• .22
• .32
• .357 / 3-5
• .38
• .40
• .44 / Four four / 4's
• .45 / 4's / Four fifth
• .50
• 110s
• 125 / 1-2's

A:• A-Town / A Town / The A
• Act up
• Aggro
• Aggy / Aggiest
• Ain't
• Air / Airing
• Akhi / Akh
• All mouth and trousers
• Alie / Ahlie
• Allow / 'Low / Lowe
• AM / M
• Ammi / Am / Amm / Ammy / Amnesia
• Amp
• Armshouse / Arms
• Arse Bandit
• Arse
• Arsehole / Arseholes
• Arselick / Arse lickers
• Athens of the North
• Auld Reekie
• Awoh

B:• Babylon
• Bad B
• Badman / Badmans
• Badness
• Baccy
• Bacon
• Back
• Back it / Back / Backed it / Backed out / Back out
• Backshot
• Bae
• Bag of / Bagga / Baggaman / Bagga man
• Bag of sand
• Bagged
• Baghdad
• Bags
• Baiders / Baider / Bade / Bading
• Bait
• Bally
• Bam
• Bando
• Bang
• Banging
• Barbie
• Bare
• Bark / Barking
• Bars
• Bash / Bashing / Bashed
• Batty / Batty boy
• Beam
• Beanie / Beanies / Beans
• Beat / Beater
• Beef / Beefing / Beefin'
• Begfriend / Beg friend
• Bell
• Bellend / Bellends
• Bells
• Belly
• Benz / Ben / Benner / Benners
• Big mac / Big macs / Big M
• Big man ting / BMT / Big man tings
• Big up
• Bill / Billing
• Bill / Bills
• Billy no mates
• Bin
• Bingo wings
• Bird / Birds
• Birded
• Birms / Birmz
• Biscuit
• Bitch / Bitches / Bitching / Bitch boy
• Bits
• Bizzies
• Black Maria
• Blag / Blagging
• Blam / Blammed / Blammer
• Blench
• Blicky
• Bloke
• Blootered
• Blud
• Blue Borough
• Blues and twos
• Bly
• Bob
• Bobbies
• Bocat / Bocats / Bow cat / Bowcat
• Bog / Bog roll
• Bogtrotters
• Booky / Bookie / Buki / Bookey
• Bollock naked
• Bollocking
• Bollocks
• Bollocksed
• Boo
• Boomting / Boom ting / Boom-ting
• Bonkaz / Bonkers
• Bore / Bored / Boring
• Borer / Bora
• Boss / Bossman
• Boydem / Boysdem
• Boyed
• Boyment
• Bradistan
• Brain
• Brap / Blap
• Bread
• Breading
• Bredrin / Bredrins
• Breeze off
• Breh / Brer / Bre
• Brick / Bricks
• Bricky / Brix
• Bro
• Brodie
• Brooklyn
• Broom
• Broski
• Brown
• Bruck / Bruk
• Bruck-back / Brucky / Bruckshot
• Brudda / Bruddas / Bredda / Breddas / Bruddah / Breddah
• Brum
• Brummie
• Bruv
• Bubble / Bubblin'
• Buck / Bucky / Bucked
• Buff / Buff ting
• Bugger all / Bugger-all
• Bugger off / Buggered off
• Buggered
• Buggery
• Builder's bum
• Bujj / Buj / Bujju / Booj
• Bully van / Bully vans
• Bumfluff
• Bummy / Bum
• Bun
• Bunda
• Burger
• Burner / Burna
• Burst / Bursting
• Buss / Bussed
• Bust / Bustin'
• Butters

C:• Cabbaged
• Cack-handed
• Cah / Cuh
• Cake
• Cannon
• Can't be arsed / CBA / Can't be asked
• Case / Cases
• Cat / Cats / Catty / Kat / Kit Kat / Katty
• Catfish
• Chatty / Chatty Patty / Chatty-chatty
• Charlie
• Charver / Charvers / Charva / Charvas
• Chav / Chavs / Chavvy
• Cheese
• Cheesed off
• Cheddar
• Cheff / Cheffer / Cheffed / Cheffing / Cheffers / Cheffy
• 'Chette / 'Chette's
• Chib / Chibs / Chibbed
• Chief
• Chiefed
• Ching
• Chinged
• Chinger / Chingers
• Chinging / Chingings
• Chip
• Chips
• Chirps / Chirpsing
• Chocolate starfish
• Choong / Choong ting / Chung / Chungting / Choong tings / Choongest / Choongers
• Chuntering
• Civilian / Civilians / Civ
• Clanger
• Clap / Clappin' / Clapping / Clappaholic / The clap
• Clapper / Clappers
• Clap Town
• Clapped
• Clart / Clartin' / Clarting / Clarter
• Clock / Clocked
• Clout
• Cockblock
• Codswallop
• Cold
• Cop / Coppa / Copper / Cops / Coppers
• Cor blimey / Corblimey / Gorblimey / Gor blimey
• Corn
• Cotch / Cotched / Cotching / Kotch / Kotched / Kotching
• Crackhead
• Crash / Crashed / Crashing / Crashing corn
• Crasher / Crashers
• Creasing / Creasing up
• Crep / Creps
• Crib
• Criss
• Chrome
• Crow / Cro / Chro
• Crud / Cruddy
• C Town
• Cunch / Country
• Cut / Cutting

D:• Dank
• Darg / Dog / Dawg
• Dark
• Dash / Dasheen / Dashing
• Dead / Dead ting / Deadout
• Dead up
• Dear Green Place
• Dem
• Deyah
• Dibble
• Dinger / Ding-Dong / Ding
• Dinner / Dinners
• Dip / Dipper / Dippers / Dipped
• Do road
• Dog's bollocks
• Don / Dons / Donness / Donette / Donny
• Don dada / Don gorgon
• Dosh
• Dot / Dots
• Dotty / Dotties
• Done / Dun
• Done out / Dun out / Done out here / Dun out here
• Double-Tap
• Dough
• Doughnut / Donut
• Draw
• Drop
• Drop top
• Drum
• Dumpies
• Dunkno / Dunknow / Dun know / Dun kno / Done know
• Duppy / Duppying
• Dutty
• Drill / Drills / Drilling / Drillings
• Driller / Drillers
• Drip

E:• Eat
• Eating
• Ediat / Eediat / Eediyat
• Elizabeth
• Ends

F:• Faff / Faffer
• Fag / Fags
• Fam
• Famalam
• Fanny
• Fassy / Fassyhole / Fassies / Fass
• Fed / Feds / Federals
• Feen / Fiend
• Fence
• Field
• Fishing
• Fit / Fitter
• Five O / Five-O / 5-0
• Fiver
• Flat roofin'
• Flicky / Flick / Flicks
• Food
• Four door / 4-door
• Four Fizzy / Fizzy
• Four four / .44 / 4's
• Fries
• From time
• Fry
• Fryer
• Funds
• Fuzz

G:• G / G's / OG
• G-Check
• Gaff
• Gammon
• Gangdem / Gang dem
• Garm / Garms
• Gash
• Gassed
• Gauge
• Gavvers
• Gaza / Gaza Strip
• Geezer
• Gelato
• Gem
• Glide / Glides
• Glizzy
• Got-got / Got got
• Gorblimey / Gor blimey / Cor blimey / Corblimey
• Gov / Govs / Govy
• Grand
• Granite City
• Greaze / Greazy
• Green
• Grind / Grinder
• Groupie
• Grub
• Gully
• Gullyside / Illyside
• Gunchester
• Gwop
• Gyalchester
• Gyaldem / Gyal dem / Girl dem / Girls dem / Gyallie / Gyal

H:• Had up
• Hammer / Hammers
• Hammered
• Handting / Hand ting
• Hang tight
• Harlem / Harlem City
• Hat / Hatterz
• Heater / Heat
• Head
• Headtop
• Headtopped
• Hella
• HMP
• Hench
• Home V's
• Hold tight
• Holla / Holla'd
• Hooptie / Hoopties / Hoopty / Hoop
• Hoover
• Hot
• Hotted
• Hudds
• Hunna
• Hype / Hyping / Hyped

I:• I and I
• IC3
• Illyside / Gullyside
• Innit
• Iron
• Itch / Itchy
• Izzy

J:• Jack Jones / Jacks
• Jakes
• Jam sandwich
• Jarring
• Jawn / Jawns
• Jeet
• Jezzy / Jezzie / Jezebel
• Jook / Jooked / Jooks / Jooking / Jookings
• Joint / Joints
• Jugg / Juggin'
• Juice / Juice
• Junk

K:• K / K / K's / K'd
• Kat / Kit Kat / Katty / Cat / Cats / Catty
• Ken
• Ketchup
• Khalas
• Kicks
• Killy
• Kiss my arse
• Kitchen
• KMT ("Kiss / Kissing my teeth" or "Kiss / Kissing your teeth")
• Kotch / Kotched / Kotching / Cotch / Cotched / Cotching
• Kway
• Kweff / Queff / Kweffer / Queffer / Kweffed / Queffed / Kweffings / Queffing
• Kweng / Kwenger / Kwengers / Kwengings / Kwenged

L:• L
• Lack / Lacking / Slipping / Slip
• Landing / Landings
• Leg it / Legged it / Legging it / Legs it
• Leng
• Lengman
• Lengting / Lengtings
• Lick
• Lighty
• Lightwork
• Likkle
• Line
• Link / Linking
• Live corn
• Long / Long ting / Longage
• Loo / Loo roll
• Low flyer
• 'Low / Lowe / Allow
• Lughole / Lugholes
• Lunch
• Lurk / Lurking / Lurky

M:• M / AM
• Mac / M10 / M11
• Macaroni
• Machine
• Mad
• Mains
• Mali / Malis
• Mali Strip
• Man
• Man step / Step
• Mandem
• Manky
• Manny
• Manor
• Mash / Masheen
• Mash man
• Mash work
• Matic
• Maud
• Mazza / Mazzaleen / Mazzaleeni
• MCM / Man Crush Monday
• Melt
• 'Ments / 'Mense
• Merk / Murk / Merking / Murking / Merked / Murked / Murker / Merker
• Mill'
• Minge
• Minging / Minger
• Moola / Moolah / Mola / Mulla / Mula / Mulah / Mulla / Mullah
• Moist
• Monkey
• Moscow
• Mop
• More Time
• Mr Plod / Plod / Plods / The plod / PC Plod
• Mug / Muggin' / Mugging / Mugged
• Muppet / Muppets
• Murderzone / MZ
• M-Way

N:• Nang
• Nank / Nank's / Nanked / Nanking
• Nizzy
• 'Narm / Nizzy / Nizzy Narm / Pecky / Pecknarm / Peckyside / Pecknizzy / Pecks / Peckz
• Ned / Neds
• Neek / Neeks / Neeky
• NFA
• Nitty
• Nick / Nicked
• Nine / Nina / .9 / .9mil
• Niz
• No Cap
• No Face, No Case
• No long ting / Nothing long
• Noddy
• Nonce
• North Weezy / Norf Weezy / Weezy

O:• Obbo
• Old bill / The bill
• Older / Olders
• On Job / OJ
• On road
• One man up / 1 man up
• Oner
• Ooter / Ooters
• Opp / Opps / Oppers
• Opp Block
• Opping
• On Tag
• Ones
• OT / Outer Town

P:• P / P's
• Pack / Packs
• Packed
• Paigon / Paigons / Pagan / Pagans
• Panda Car / Panda cars
• Paper / Papes
• Par / Parred / Parring
• Passa
• Patty
• Pave
• Peak
• Pecknarm / 'Narm / Pecky / Peckyside / Pecknizzy / Nizzy / Nizzy Narm / Pecks / Peckz
• PCD / Pull Up, Crash, Dash
• Peelers
• Peb / Pebs / Pebbles
• Ped
• Pen
• Peng / Pengers / Pengest
• Pengaleng / Peng-a-leng / Peng-aleng
• Pengting / Peng ting / Peng tings
• Pickney / Pickneys
• Piff / Piffting / Piff ting
• Pig / Pigs / Piglets
• Pillock / Pillocks
• Pinky
• Piss
• Pissed / Pissed off
• Plod / Plods / The plod / Mr Plod / PC Plod
• Plonker / Plonkers
• Plug / Plugging
• Poke
• Pole
• Ponce / Ponces / Poncy / Poncing
• Poomplex
• Pork / Porkies / Porky / Porked
• Pree / Preeing / Preed
• Proper
• Pum pum
• Pump / Pumpy
• Punani / Punanis
• Pussio / Pussy'ole / Pussy'oles / Pussyoles / Pussyole / Pussy hole / Pussyhole / Pussyholes / Pussy / Pussies

Q:• Queens face / Queen heads
• Quid

R:• Rack / Racks
• Racket
• Rah / Rahs
• Rahtid / Raatid / Rhaatid / Rarted / Ratid
• 'Rales
• Rambo / Rambz / Rambzy / Rammy / Rambizzy / Ramsey
• Ratchet / Ratch'
• Rass / Rarse / Raas
• Readies
• Reh teh teh
• Reload / Re-up
• Rental
• Ride out / ride
• Ride or Die
• Ringer
• Road / Roads
• Roadman / Roadmen
• Rozzer / Rozzers
• Rubbish
• Russian

S:• Sammy / Sammi
• Sauce
• Score
• Scram
• Second City of the Empire
• Seen / Skeen
• Shank / Shanks / Shanked / Shanking / Shankings
• Shave
• Shell / Shellings / Shelling
• Shh
• Shift / Shifted / Shiffed
• Shook
• Shot / Shots
• Shotting
• Shotty
• Showerman / Shower man
• Sing
• Siraq
• Skank
• Skate / Skating / Skeet
• Skeng / Skengs / Skengaleng / Skeng-a-leng
• Skengman
• Sket / Skets / Sketty
• Slag / Slags / Slaggy
• Slag off / Slagging / Slagged
• Slang / Slanging
• Slapper / Slapper
• Slew / Slewing / Slewed
• Slipping / Slip / Lack / Lacking
• Smackers
• Smoke / Smokes / Smoker
• Snake
• Snap
• Sneeze
• Snitch
• Spanner
• Spannered
• Sparko
• Spin / Spinner / Spinners / Spin ting
• Splash / Splashed / Splashy
• Spliff
• Spondulicks / Spondoolicks / Spondulics / Spondulacks
• Splurt
• Stain
• Standard
• Star
• Step / Man step
• Stick / Sticks
• Still / Styll
• Stiver
• Strally
• Strap / Straps / Strapped
• Strip
• Swammy / Swammies
• Swear down
• Sweets / Sweeties
• Swim / Swimming
• Swiss
• Sword

T:• Tanned
• Tark
• Tapped
• Teeth
• Tekkers
• Tenner
• Ten toes / Ten toe / 10 Toes / 10 toe
• Tester
• The bill / Old bill
• The plod / Mr Plod / Plod / Plods / PC Plod
• The Seven / The 7
• The Six / The 6
• The Sweeney
• Three man step / 3 man step
• Three-ha'pence
• [on] Tick
• Ting / Tings
• Tips
• Tit wank
• Toff
• Touched
• Top / Toppers
• Top boy
• Tosser / Tossers
• Trap
• Trap House
• Trapping / Trapper
• Tre Pound / Trey
• Trident
• Trip
• TSG
• Tug
• Tum-tum
• Twang
• Twat / Twatting
• Two man step / 2 man step

U:• Uck / Uckers / Ucky
• UDN
• Undies
• Upsah

V:• Verbal
• Vex / Vexed
• Vietnarm
• Volts

W:• Wagwan
• Waigon / Waigons
• Wallad
• Wano Road
• Wank / Wanks / Wanker / Wankers / Wanking / Wanked
• Wank bank
• Wank mag
• Wankered
• Wankstain / Wankstains
• Wap / Waps
• Wass / Wassy
• Wasteman / Wastemen
• Wet wipe
• Wet / Wetty / Wet up / Wetted
• Wetter / Wetters
• Whip
• Whistle / Whistles
• White
• Whizz / Whiz
• Wing
• Woi-oi
• Wooly Road / Wooly Hood
• Woosh
• Woosher
• Wonga
• Wrap / Wrapped

X:• Xanny

Y:• Yard
• Yellow Brick
• Ying / Yinged
• Yob / Yobbo
• Yute / Yutes / Yout' / Youts
• YG / Young G

Z:• Z / Zed / Zeds
• Zoobie
• Zoop
• Zoot