[Intro: YG]
Ladies and Gentlemen...

[Hook: YG & (Mariah Carey)]
I know you love me
You just don't trust me
You don't understand me
But you love it when I call you Ms. Carey (I don't, I don't)
I know you love me
You just don't trust me
You need to understand me
But you love it when I call you Ms. Carey (I don't, I don't)

[Verse 1: Mariah Carey]
Somewhere in another life
We stole a moment in time
Gave you everything that you needed
I was even down to repeat
Said you would always be mine
Feeding me nothing but lies
I was so gone, I admit it
Had me messed up for a minute

[Pre-Chorus: Mariah Carey]
But I'm tired of crying
No more tears
Pity party of the year
Cold at night cause you're not here
Leaving you was my worst fear
I was caught up, I was blind
You kept playing with my mind
Told you I'm finished
Gassed up the whip and
Rear view, bye

[Chorus: Mariah Carey]
'Cause when you love someone
You just don't treat them bad
You messed up all we had
Probably think I'm coming back
But I don't (I don't, I don't)
Boy I was bugging
Thinking somehow I could trust you
See I used to love you
But I don't, I don't (I don't, I don't)
Boy I was bugging
Thinking somehow I could trust you
But you don't mean nothing
'Cause I don't, I don't

[Verse 2: Mariah Carey]
I tried to make it work
No matter how much it hurt
You had to make it all about you
Tell me why you go and do me like you do
I went from me and you
To walking right out on you
I know you want love, in your feelings
Fronting on me like your love was the realest

[Pre-Chorus: Mariah Carey]
And I'm tired of crying
No more tears
Pity party of the year
Cold at night cause you're not here
Leaving you was my worst fear
I was caught up, I was blind
You kept playing with my mind
Told you I'm finished
Gassed up the whip and
Rear view, bye

[Chorus: Mariah Carey]
'Cause when you love someone
You just don't treat them bad
You messed up all we had
Probably think I'm coming back
But I don't (I don't, I don't)
Boy I was bugging
Thinking somehow I could trust you
See I used to love you
But I don't, I don't (I don't, I don't)
Boy I was bugging
Thinking somehow I could trust you
But you don't mean nothing
'Cause I don't, I don't

[Hook: YG]
I know you love me
You just don't trust me
You don't understand me
But you love it when I call you Ms. Carey
I know you love me
You just don't trust me
You need to understand me
But you love it when I call you Ms. Carey

[Verse 3: YG]
Hold up, hold up, hold up, hold up
Hold up, I just got you ring, for what?
I put you in the game, now it's game over
Every time you went to the Louis store you got chauffeured
Hold up, how you gon' leave that?
Hold up, give me my ring back
Never mind, you can keep that
'Cause every time you look at your ring, me, you gon' dream that

[Hook: YG]
I know you love me
You just don't trust me
You don't understand me
But you love it when I call you Ms. Carey
I know you love me
You just don't trust me
You need to understand me
But you love it when I call you Ms. Carey

[Chorus: Mariah Carey]
'Cause when you love someone
You just don't treat them bad
You messed up all we had
Probably think I'm coming back
But I don't (I don't, I don't)
Boy I was bugging
Thinking somehow I could trust you
See I used to love you
But I don't, I don't (I don't, I don't)
Boy I was bugging
Thinking somehow I could trust you
But you don't mean nothing
'Cause I don't, I don't

[Outro: Mariah Carey]
Don't
I don't, I don't