[Verse 1]
How could we be so close but miles apart?
'Cause something tells me that's not who we are
Baby, nobody else could know my heart like you do
Oh, like you do
It could be easy just to give it up (don't give it up)
Baby, we both know that's not who we are (who we are)
So let's just fall back in, back to the start like it's new
(Oh, like it's new)

[Pre-Chorus]
All I want from you is devotion
Tired of going through these emotions
Running all the red lights just to see you tonight
Make it right

[Chorus]
Make it feel like it's the first time we fell in love
Nothing like the first time that we touched
Everything we've lost I need it the most
Baby, now's the time to pull me in close
Don't hold me like you already know me
I'm not leaving 'til I get you to show me
We could fall in love just like
Just like the first time, the first time

[Verse 2]
Got my attention, now we're eye to eye
Love and affection never wants to die
Remember how we kept this love alive
Float on a kiss, baby, straight to the sky

[Pre-Chorus]
All I want from you is devotion
Tired of going through these emotions
Running all the red lights just to see you tonight
Make it right

[Chorus]
Make it feel like it's the first time we fell in love
Nothing like the first time that we touched
Everything we've lost I need it the most
Baby, now's the time to pull me in close
Don't hold me like you already know me
I'm not leaving 'til I get you to show me
We could fall in love just like
Just like the first time, the first time

[Bridge]
All I want from you is devotion
Tired of going through these emotions
How could we be so close but miles apart?

[Chorus]
Make it feel like it's the first time we fell in love
Nothing like the first time that we touched
Everything we've lost I need it the most
Baby, now's the time to pull me in close
Don't hold me like you already know me
I'm not leaving 'til I get you to show me
We could fall in love just like
Just like the first time, the first time