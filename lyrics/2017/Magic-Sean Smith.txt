Ranking has ended, so this page is no longer in use. To keep track of the finalized lists and the rest of the EOY process, check for updates in the 
Year End List Timeline 2017
 thread.
This is the ranking page for the 2017 Year End Lists. 
The
 initial ordering for albums and songs is based on the results of a Google form voting process—
click here to learn more
. Please do not edit the lyrics of this page. If you would like something changed, discuss it in the suggestions or message 
@Empath
.
Example Annotation:
Artist - Album/Song
Top 65 Albums:
1. 
Kendrick Lamar - 
DAMN.
2. 
Tyler, the Creator - 
Flower Boy
3. 
SZA - 
Ctrl
4. 
JAY-Z - 
4:44
5. 
Sampha - 
Process
6. 
Lorde - 
Melodrama
7. 
Migos - 
Culture
8. 
BROCKHAMPTON - 
Saturation I & II
9. 
Vince Staples - 
Big Fish Theory
10. Joey Bada$$ - 
ALL AMERIKKKAN BADA$$
11. 
Kelela - 
Take Me Apart
12. 
Harry Styles - 
Harry Styles
13. 
Daniel Caesar - 
Freudian
14. 
Rapsody - 
Laila's Wisdom
15. 
Big K.R.I.T. - 
4Eva Is A Mighty Long Time
16. J Hus - 
Common Sense
17. 
Lana Del Rey - 
Lust for Life
18. 
Paramore - 
After Laughter
19. 
Young Thug - 
Beautiful Thugger Girls
20. 
Lecrae - 
All Things Work Together
21. 
Run the Jewels - 
Run the Jewels 3
22. 
Wiley - 
Godfather
23. 
Lil Uzi Vert - 
Luv Is Rage 2
24. 
J.I.D - 
The Never Story
25. 
2 Chainz - 
Pretty Girls Like Trap Music
26. 
Drake - 
More Life
27. 
Björk - 
Utopia
28. 
Gorillaz - 
Humanz
29. 
Thundercat - 
Drunk
30. 
Halsey - 
hopeless fountain kingdom
31. 
Gucci Mane - 
Droptopwop
32. 
Father John Misty - 
Pure Comedy
33. 
Moses Sumney - 
Aromanticism
34. 
Fleet Foxes - 
Crack Up
35. 
NF - 
Perception
36. 
Dvsn - 
Morning After
37. 
The xx - 
I See You
38. 
Jaden Smith - 
SYRE
39. 
Casper - 
Lang Lebe Der Tod
40. 
Meek Mill - 
Wins & Losses
41. 
Big Sean - 
I Decided
42. 
Khalid - 
American Teen
43. 
Perfume Genius - 
No Shape
44. 
Corbin - 
Mourn
45. 
Childish Gambino - 
Awaken, My Love!
46. 
Future - 
FUTURE
47. 
Logic - 
Everybody
48. 
Andy Mineo & Wordsplayed - 
Magic & Bird
49. 
J. Cole - 
4 Your Eyez Only
50. 
Kesha - 
Rainbow
51. 
Sam Smith - 
The Thrill of It All
52. 
IDK - 
IWasVeryBad
53. 
Mac Demarco - 
This Old Dog
54. 
King Krule - 
The OOZ
55. 
Ty Dolla $ign - 
Beach House 3
56. 
21 Savage, Offset, & Metro Boomin - 
Without Warning
57. 
A Boogie Wit Da Hoodie - 
The Bigger Artist
58. Calvin Harris - 
Funk Wav Bounces Vol. 1
59. 
milo - 
who told you to think??!!?!?!
60. 
Amine - 
Good For You
61. 
Witt Lowry - 
I Could Not Plan This
62. 
Joji - 
In Tongues
63. 
Future - 
HNDRXX
64. 
Future & Young Thug - 
SUPER SLIMEY
65. 
Kehlani - 
SweetSexySavage
Notable albums released after voting closed but before Dec. 4:
YGG - 
World Domination
TBD
TBD
TBD
TBD
Critically acclaimed albums not on the list:
Rick Ross - 
Rather You Than Me
Syd - 
Fin
Stormzy - 
Gang Signs & Prayer
William Patrick Corgan - 
Ogilala
CyHi The Prynce - 
No Dope On Sundays
Joyner Lucas - 
508-507-2209
Talib Kweli - 
Radio Silence
Lil Peep - 
Come Over When You're Sober, Pt. I
Choker - 
PEAK
Neck Deep - 
The Peace And The Panic
Brent Faiyaz – 
Sonder Son
Jidenna - 
The Chief
Ibeyi - 
Ash
Benjamin Booker - 
Witness
St. Vincent - 
MASSEDUCTION
The War On Drugs - 
A Deeper Understanding
Post Malone - 
Stoney
TBD
TBD
Album Graveyard:
Macklemore - 
Gemini
DJ Khaled - 
Grateful
Marilyn Manson - 
Heaven Upside Down
XXXTENTACION - 
17
Ed Sheeran - 
÷
Katy Perry - 
Witness
Linkin Park - 
One More Light
Playboi Carti - 
Playboi Carti
Taylor Swift - 
reputation
Brand New - 
Science Fiction
TBD
Top 65 Songs:
1. 
Frank Ocean - “Chanel”
2. 
Lil Uzi Vert - "XO Tour Llife"
3. 
Kendrick Lamar - “DNA.”
4. 
JAY-Z - "The Story of O.J"
5. 
Calvin Harris - “Slide” feat. Frank Ocean & Migos
6. 
Kendrick Lamar - “XXX” feat. U2
7. 
Vince Staples - “Yeah Right” feat. Kendrick Lamar
8. 
Future - “Mask Off”
9. 
Tyler, the Creator - “See You Again” feat. Kali Uchis
10. 
Harry Styles - “Sign Of The Times”
11. 
Khalid - “Location”
12. 
Cardi B - “Bodak Yellow”
13. 
NF - "Let You Down"
14. 
Frank Ocean - “Biking” feat. Tyler, the Creator & JAY-Z
15. 
Kendrick Lamar - "LOVE."
16. 
Migos - "T-Shirt"
17. 
Logic - “1-800-273-8255”
18. 
SZA - “Love Galore” feat. Travis Scott
19. 
Goldlink - “Crew”
20. 
Tyler, the Creator - "Boredom" feat. Anna of the North, Corinne Bailey Rae & Rex Orange County
21. 
Lorde - “Green Light”
22. 
Lil Uzi Vert - “Neon Guts” feat. Pharrell Williams
23. 
Drake - “Teenage Fever”
24. 
SZA - “The Weekend”
25. 
Travis Scott - “Butterfly Effect”
26. 
Kesha - “Praying”
27. 
Joey Bada$$ - "Rockabye Baby" feat. ScHoolboy Q
28. 
French Montana - "Unforgettable"
29. 
Sampha - “(No One Knows Me) Like The Piano”
30. 
Kelela - "LMK"
31. 
Playboi Carti - “Magnolia”
32. 
Lana Del Rey - “Love”
33. 
Post Malone - "rockstar" feat. 21 Savage
34. 
Frank Ocean - “Provider”
35. 
Camila Cabello- "Havana" feat. Young Thug
36. 
A$AP Mob - “RAF” feat. Quavo, A$AP Rocky, Lil Uzi Vert, Playboi Carti & Frank Ocean
37. 
Portugal. The Man - "Feel It Still"
38. 
Lil Uzi Vert - “The Way Life Goes”
39. Mac Demarco - “My Old Man”
40. 
N.E.R.D - "Lemon" feat. RIhanna
41. 
Tyler, the Creator - “Who Dat Boy?” feat. A$AP Rocky
42. 
DJ Khaled - “Wild Thoughts” feat. Rihanna & Bryson Tiller
43. 
Lecrae - "I'll Find You"
44. 
A Boogie Wit Da Hoodie - "Drowning"
45. 
Marshmello - “Silence” feat. Khalid
46. 
Young Thug & Carnage - "Homie" feat. Meek Mill
47. 
Selena Gomez - "Fetish"
48. 
Freddie Gibbs - "Crushed Glass"
49. 
JAY-Z - "Family Feud"
50. 
Ski Mask The Slump God - “Catch Me Outside”
51. 
Meek Mill - "1942 Flows"
52. 
2 Chainz - "4 AM" feat. Travis Scott
53. 
A$AP Ferg - “Plain Jane”
54. 
Big Sean - "Sacrifices" feat. Migos
55. 
Jaden Smith - "Icon"
56. 
Khalid - "Young Dumb & Broke"
57. 
Lil Peep - "Awful Things"
58. 
BROCKHAMPTON - “SWAMP”
59. 
Lorde - "Homemade Dynamite"
60. 
Tyler, the Creator - “I Ain’t Got Time!”
61. 
Tyler, the Creator - "911 / Mr. Lonely"
62. 
BROCKHAMPTON - "SWEET"
63. 
Lil Pump - “Gucci Gang”
64. 
21 Savage - “Bank Account”
65. 
Miguel - "Sky Walker"
Notable songs released after voting closed but before Dec. 4:
TBD
TBD
TBD
Critically acclaimed songs not on the list:
Futuristic - "Epiphany"
Corbin - "ICEBOY"
Mabel - "Finders' Keepers"
Big Sean - "No Favors" feat. Eminem
Kendrick Lamar - "DUCKWORTH."
Moses Sumney - "Quarrel"
2 Chainz - "Good Drank" feat. Gucci Mane & Quavo
Halsey - "Now or Never"
Halsey - "Eyes Closed"
Vince Staples - "Big Fish"
The xx - “Replica”
The xx - “Dangerous”
Offset & Metro Boomin - “Ric Flair Drip”
Migos - “Slippery” feat. Gucci Mane
Young Thug - “Relationship”
Calvin Harris - “Rollin” feat. Future & Khalid
Dua Lipa - “New Rules”
Kodak Black - “Tunnel Vision”
Kyle - “iSpy” feat. Lil Yachty
Marian Hill - “Down”
Carly Rae Jepsen - “Cut to the Feeling”
Luis Fonsi & Daddy Yankee - “Despacito (Remix)" feat. Justin Bieber
Kendrick Lamar - "FEEL."
Kali Uchis - "Tyrant" feat. Jorja Smith
A.CHAL - "To the Light"
J.I.D - "NEVER"
SZA - "Doves In The Wind" feat. Kendrick Lamar
Gorillaz - "Sleeping Powder"
IDK - "Maryland Ass Nigga" feat. Swizz Beatz
Baka - Live Up To My Name
21 Savage - Nothing New
Bad Bunny - Soy Peor
Drake - Portland ft. Travis Scott & Quavo
YBN Nahmir - "Rubbin Off The Paint"
Drake - "Ice Melts" feat. Young Thug
TBD
Rich Chigga - "Glow Like Dat"
Fabolous & Jadakiss - "Theme Music"
TBD
TBD
TBD
TBD
TBD
TBD
TBD
Song Graveyard:
Kendrick Lamar - “LUST.”
Kendrick Lamar - “FEAR.”
Kendrick Lamar - “ELEMENT."
Playboi Carti - “wokeuplikethis*” feat. Lil Uzi Vert
Selena Gomez - "Bad Liar"
XXXTENTACION - "Jocelyn Flores"
Migos - "Bad and Boujee" feat. Lil Uzi Vert
Kendrick Lamar - “HUMBLE.”
JAY-Z - "Marcy Me"
Ed Sheeran - "Shape of You"
CyHi The Prynce - "Dat Side" feat. Kanye West
Eminem - “Walk On Water” feat. Beyonce
Drake - “Gyalchester”
Taylor Swift - “Getaway Car”
Drake - “Passionfruit”
Daniel Caesar - "Get You'" feat. Kali Uchis
Childish Gambino - “Redbone”
Big Sean - "Bounce Back"
TBD
TBD
TBD
TBD