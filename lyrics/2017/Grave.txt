Here we stand by Karin Boye's grave
I'm singing while Mattias plays his funky machines
Tiger, have you reached a brighter day?
Won't you give this world just another chance
'Cause we could drive in our car
From Alingsas to the stars

'Cause we would have loved your style
I would have loved to be alive
We could have gone to Kvarnen every night
I could have held your hands
You could have played in our band
We could have spread revolution through this land

Mattias, play the omnichord for her

Ooh, that's a machine I've never heard
So don't think it's time that you came back and saw
What's new in this world
You could be our girl (I could be your girl)

'Cause we would have loved your style
I would have loved to be alive
We could have gone to Kvarnen every night
I could have held your hands
You could have played in our band
We could have spread revolution through this land
We could have reached the skies
You wouldn't even have to try
We would love to hear you sing
Bring on the vibes and I'll bring the ring
Come back to life and we'll show you everything(show me everything)