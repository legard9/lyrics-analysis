I see the moonlight
I'm fighting my tired eyes
Open the closed doors
And bury your dark times
We're all walking a delicate line
It's not easy, love won't break me
We're all walking in delicate times
That won't drag me down
All I know
Is I could use a friend
Or something to believe in
All I hope
Is that better days are coming
Better days are coming
Better days are coming
Better days are coming
Oh better days are coming
Better days are coming
Oh what a long night
Swear I could lose my mind
So I open the window
And let in the good life
We all float on a delicate line
It's not easy, love won't break me
We're all floating in delicate times
That won't drag me down
All I know
Is I could use a friend
Or something to believe in
All I hope
Is that better days are coming
Better days are coming
Better days are coming
Better days are coming
Oh better days are coming
Better days are coming
All I know
Better days are coming
Better days are coming
All I hope
Oh better days are coming
Better days are coming
All I know
Is I could use a friend
Or something to believe in
All I hope
Is that better days are coming
Better days are coming
All I know
Better days are coming
Better days are coming
All I hope
Is that better days are coming
Better days are coming