Nancy?
Yes, dear
Now be honest. At my age, do you think I should be doing something with this?
Ha ha, believe me Ronny, every once in awhile it's healthy to do this sort of thing. It isn't just for young people you know. A lot of people our age do it. I hear even George Burns did it
But not while wearing roller skates
Probably not
And why shorty pajamas? Why can't I just wear the normal lounge type?
Oh Ronny, you look good dear. I like the way you look
And the water wings?
Mmm, I like those too
Listen, can I take off the water wings and the bunny ears?
Oh, I wish you wouldn't
Nancy, I'm really surprised at you. I mean this is strange. I wouldn't think you'd go in for this and I've known you an awful long time
I suppose you want me to take off the pirate boots and throw away the pogo stick and the Dumbo ears?
Can I remind of you something? I am the President of the United States
Ooh hoo ha ha ha, that's what makes it so wild
Now Ronny, Ronny dear, just grit your teeth and say "I'm going to jump in with both feet"
Alright I'll do it. But I just wish I could take off the beaver tail and get rid of the skates
Oh, it's too late now dear *kiss* Good luck
I'll probably be sorry in the morning
(We're standby Mr. President. You're on and cue the President)
Live from New York, it's Saturday Night!
Nancy, look at that boy over there. He must have gotten separated from the tour
Oh, you mean the little one standing there, staring at the picture of George Washington, dear?
Yes, I'll just go over and stand behind him and tell him what a great country this is
Excuse me little boy, that's the father of our country you're looking at. His name was George Washington. He was also the first President of the United States and a great president he was too. He fought for our country and made it the great country it is today. And after you finish school you can grow up to be President. Just like George Washington. And you can live here in the White House, just like me

[Tattoo from Fantasy Island]
(What? Me leave Fantasy Island?)
(Leave it. The plane! The plane!)
Excuse me, Mr. President, there's someone very important here to see you
Brezhnev? Begin? Prince Charles?
No sir, it's Mr. Bill
Mr. who?
Mr. Bill, sir. You know the little man who always gets stepped on and smashed on Saturday Night Live?
Well you know I can't stay up that late, but Ed Meese often tells me about the show the next morning. So, send him in
(Hoo-hoo, Mr. President, hee-hee, oh boy, it sure is great to meet you. Ah, you know, I just wanted to tell you that I think you're doing a great job and I really like your movies a lot, yeah!)
Well uh, think you Mr. Bill. Which movies do you like the best?
(Oh boy, I-I like those westerns where you do all those dangerous stunts, wee!)
Well, Mr. Bill, I had highly trained stuntmen to do those. But tell me, I mean, who does your stunts?
(Oh, gee, I have to do all my own stunts, and uh, and you know, in fact that's why I wanted to get into something a little safer. Like, like politics, uh, say, could you give me some pointers about how you broke in?)
Well I'd be glad to Mr. Bill. Listen, why don't we discuss this out on the Rose Garden?
(The Rose Garden?)
You've never seen the Rose Garden? Well listen, hop up here on the window sill and I'll show it to you
(Well uh, could you give me a hand? Now that's a little high for me to hop)
Sure, Mr. Bill
(Oh, but be careful there, watch out)
Well, there's the Rose Garden down there and- (Watch out!)
(No! No! Ooh! Oooh, the thorns, oh no
Hey, hey, say what's that big plane coming, huh?)
Oh, that's Secretary of State Sluggo in his new B-1 Bomber
(Sluggo, he's gonna be [?] oh, oh, oh, oooh!)
Ah, the poor little fella should've stuck to show business
Roger, Atlanta Control, this is Air Force One, departing you now, request additional air check, over
You know I really have to hand it to the President. The way he handled those air traffic controllers, he taught them a thing or two
Would you mind if I sat in with you?
Oh, good morning sir. We'd enjoy very much having you sit up here with us. We were just remarking what a good job you did with the air traffic controllers
Well Dennis Morgan and I used to fly these things together
Is that so sir?
Oh yes, yes. In Painting the Clouds with Sunshine and Hellcats of the Navy, we logged over a hundred thousand hours
That's incredible sir
You know I shot down four Japanese fighter planes in one afternoon in 1944
That was in the Pacific?
No, that was in Lassie Comes Home
I'm going to [?] our congratulations on the, on your handling of the air traffic controller problem
Right, like I was saying sir, you taught them a thing or two
Oh, that's for sure
Tallahassee Center, this is Air Force One, request your clearance for current heading and altitude, over
Thank you for calling Tallahassee Center Control. We're not in right now. But if you leave your flight number and altitude at the sound of the beep, we'll get right back to you, right back to you, right back to you