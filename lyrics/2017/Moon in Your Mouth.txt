Hangul

[Intro: Skit]
"에헤이예!
"아 그럼, 오늘 같이 신경 많이 썼지"
"호이 컴온! 저기 아줌마 좀 봐"
"뭔데"
"우와"
"와, 여기 보는 거 맞지?"
"와, 나랑 눈 못 마주치는 것 같지"
"나 보는 것 같은데"
"야 오늘은 나거든요"
"아니거든요"
"그러니까"

[Verse 1]
봐봐 마술같이  한순간 빨려 들어 마치
모두가 짠 듯이 그녈 보네  머리를 쓸어 넘기니
모두 놀래 The truth is 다 같은 맘일걸
한순간 남자라는 이름 아래 Rival돼
치명적인 저 미소는 Dangerous
(She’s so dangerous)

[Verse 2]
Oh 온몸이 짜릿해져 ‘아이야’ 오네
Oh, Girl 두 눈을 뗄 수 없어 난
멀리서도 다 느껴져

[Verse 3]
도도한 그 표정 뒤에
숨겨놓은 미소가  나를 향한 것 같은데
확실해 자꾸만 마주치는
너의 예쁜 두 눈이 나를 자꾸 애타게 해
(Come on)

[Hook]
네 앞에 날 데려가
Just you and me alone
맘 가는 대로 그냥
널 내게 맡겨봐
I promise I will be the one

[Verse 4]
달콤한 네 목소린 예쁜 Melody
나를 들뜨게 만드는 걸
모든 걸 다 가진 듯한 기분 Nobody knows
너에게로 점점 더 빠져들어

[Verse 5]
꿈인 걸까 지금 내 심장이
입안에 톡톡 튀는 사탕처럼 뛰네
오감에 퍼진 짜릿한 느낌 이 감정이
모두 사라져 버리기 전에
먼저 내민 내 손을 잡아주겠니?

[Verse 6]
코끝으로 스쳐가는
너의 샴푸 향기가
나를 잠시 멈추게 해
그 순간 살며시 스쳐간
네 부드런 손끝은
나를 정말 미치게 해

[Hook]
네 앞에 날 데려가
Just you and me alone
맘 가는 대로 그냥
널 내게 맡겨봐

[Verse 7]
나 남자답게 네게 다가갈게 Baby
다른 건 모두 지워버려
지금 이곳에
단 둘이서 Just me and you

[Hook]
네 앞에 날 데려가
Just you and me alone
(You and me, you and me, yeah)
맘 가는 대로 그냥
널 내게 맡겨봐
(내게 너를 맡겨)

[Outro]
Baby let me be the one
Baby let me be the one
(Baby let me be the one)
질투 어린 시선들 속에
마주 서있어
꿈이 아니길 바래
(Baby let me)

Romanization

Bwabwa masulgachi
Hansungan ppallyeo deureo machi
Moduga jjan deushi geunyeol bone
Meorireul sseureo neomgini
Modu nollae The truth is

Da gateun mamilkkeol
Hansungan namjaraneun ireum arae Rivaldwae
Chimyeongjeogin jeo misoneun Dangerous
(She’s so dangerous)

Oh onmomi jjaritaejeo ‘aiya’ one
Oh, girl du nuneul ttel su eopseo nan
Meolliseodo da neukkyeojeo

Dodohan geu pyojeong dwie
Sumgyeonoeun misoga
Nareul hyanghan geot gateunde
Hwakshilhae jakkuman majuchineun
Neoye yeppeun du nuni
Nareul jakku aetage hae
(Come on)

Ni ape nal deryeoga
Just you and me alone
Mam ganeun daero geunyang
Neol naege matgyeobwa
I promise I will be the one

Dalkomhan ni moksorin yeppeun Melody
Nareul deultteuge mandeuneun geol
Modeun geol da gajin deutan gibun Nobody knows
Neoyegero jeomjeom deo ppajeodeureo

Kkumin geolkka
Jigeum nae shimjangi
Ibane toktok twineun
Satangcheoreom ttwine

Ogame peojin
Jjaritan neukkim i gamjeongi
Modu sarajeo beorigi jeone
Meonjeo naemin nae soneul jabajugenni?

Kokkeuteuro seucheoganeun
Neoye shampu hyanggiga
Nareul jamshi meomchuge hae
Geu sungan salmyeoshi seucheogan
Ni budeureon sonkkeuteun
Nareul jeongmal michige hae

Ni ape nal deryeoga
Just you and me alone
Mam ganeun daero geunyang
Neol naege matgyeobwa

Na namjadapge nege dagagalkke Baby
Dareun geon modu jiweobeoryeo
Jigeum igose
Dan duriseo Just me and you

Ni ape nal deryeoga
Just you and me alone
(You and me, you and me, yeah)
Mam ganeun daero geunyang
Neol naege matgyeobwa
(naege neoreul matgyeo)

Baby, let me be the one
Baby, let me be the one
(Baby, let me be the one)
Jiltu eorin shiseondeul soge
Maju seoisseo
Kkumi anigil barae
(Baby, let me)

English

Look, it’s like magic
The way everyone’s drawn to her
It looks planned, the way everyone looks at once
When she flicks her hair back
Everyone is surprised, the truth is

We all feel the same
In that moment we all become rivals as men
Her fatal smile is dangerous
(She’s so dangerous)

Oh, my whole body is electrified like “ow”
Oh girl, I can’t take my eyes off you
I can feel it from afar

The smile hidden
Behind your bold expression
Seems like it’s meant for me
I’m sure, my eyes keep meeting
Your pretty eyes
And they’re making me more impatient
(Come on)

Bring me over to you
Just you and me alone
However you like
Just leave it all to me
I promise I will be the one

Your sweet voice is a pretty melody
It puts me over the moon
I feel like I’ve got everything, nobody knows
I fall for you more and more

Is this a dream?
My heart is pounding
Like candy that pops
In your mouth

This electrifying feeling
Spreads through all of my senses
Before this feeling leaves me
Could you take my hand?

The smell of your shampoo
Brushing past my nose
Makes me stop for a moment
In that moment, the way your fingers
Brush lightly past me
Really drives me crazy

Bring me over to you
Just you and me alone
However you like
Just leave it all to me

I’ll approach you like a man, baby
Forget everything else
Right now, right here
The two of us, just me and you

Bring me over to you
Just you and me alone
(You and me, you and me, yeah)
However you like
Just leave it all to me
(Leave it all to me)

Baby, let me be the one
Baby, let me be the one
(Baby, let me be the one)
We’re facing each other
Surrounded by looks of jealousy
I hope it’s not a dream
(Baby, let me)