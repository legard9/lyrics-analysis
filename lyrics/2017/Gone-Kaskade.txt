[Verse 1]
Stay, dance, put me on
Inside out, I'm gone
Secrets or a song
I keep my lips drawn

[Pre-Chorus 1]
Once I might have thought that
Everybody wants what I want
Everyone will know my name
Now that I've heard the chorus
They've been here before us
Turn, don't throw it away

[Chorus]
Don't throw it away
Don't throw it away
Don't throw it away
Now, don't throw it away
Don't throw it away

[Verse 2]
One chance, here me out
Whispering a shout
I'm gone in a song
I keep my cover on

[Pre-Chorus 2]
Once I might have thought that
Everybody wants what I want
Everyone will feel this way
It’s what I want more of
It's right here before us
Don't, don't walk away

[Chorus]
Don't throw it away
Don't throw it away
Don't throw it away
Now, don't throw it away
Don't throw it away

[Bridge]
I can't sing what I can say
I can't face you, look away
It's the only way you know
Where my head goes

[Outro]
(Don't throw it away)
 I can't sing what I can say
(Don't throw it away)
 I can't face you, look away
(Now, don't throw it away)
 It's the only way you know
(Don't throw it away)
 Where my head goes