[Intro]
I'd rather go blind, blind, blind
Than to see you with her tonight
I'm done with your lies
But I can't say goodbye
So, I'd rather go blind

[Verse 1]
See I remember, I remember that night
Til the mornin’ I just cried and cried
In the bed, no pillow talk
The way you left was cold and rough
It's ok cause you'll be back and you'll be mine
But where you go, where you go, where you go
I will never know
You just leave me here confused
Down, depressed and feeling blue
I’ve convinced myself that you’ll be back in due time

[Pre-Chorus]
I’ll be praying, wishing, hoping that you notice me
But now you're surrounded by new company
I play it cool, but truth is that's a lie

[Chorus]
I’d rather go blind, blind, blind
Than to see you with her, tonight
I'm done with your lies
But I can't say goodbye
So I’d rather go blind
Cause you was all mine, all, mine
And we had the time of our lives
Then I lost my sight
When I saw you that night
So I’d rather go blind
I’d rather go blind

[Verse 2]
Save your talkin', I know all the facts
Tell her it's over and imma take you back
Look me right here in my face
Cross your heart and say you've changed
But we both know that's not what you want right now
Now where you go, where you go, where you go
I will never ever, never know
You just left me here confused
Down, depressed and feeling blue
I just hope that you’ll come back home in due time

[Pre-Chorus]
I’ll be praying, wishing, hoping that you notice me
But now you surrounded by new company
I play it cool, but truth is that's a lie

[Chorus]
I’d rather go blind, blind, blind
Than to see you with her, tonight
I'm done with your lies
But I can't say goodbye
So I’d rather go blind
Cause you was all mine, all, mine
And we had the time of our lives
Then I lost my sight
When I saw you that night
So I’d rather go blind
I’d rather go blind

[Bridge]
Baby, what the hell am I supposed to do without ya?
I’ll turn a blind eye just to solidify ya
Cause I need you back in my life

[Chorus]
I’d rather go blind
Than to see you with her, tonight
I'm done with your lies
I can't say goodbye
So I’d rather go blind
You were all mine
And we had the time of our lives
But I lost my sight
When I seen yo ass that night
So, I’d rather go blind
I'd rather go blind