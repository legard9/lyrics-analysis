[Intro]
I'm a day late
I'm in jeopardy
I'm a namesake
I'm a memory
If I came straight would you remember me?
When the day breaks
Let it be

[Verse 1]
I can't change who I am now
Couldn't change who I was then
I been known to break the gates of faith down
I been known to skate for face to face kin
I been known for few friends, small family
Bruised skin, through tragedy
My true youth been through a travesty
And made it happily to look back and be here
Welcome my people to Benjamin's regress
Looking for regrets
Inside of a place that I hope I can egress
I'm trying to be best
All the while I been here trying to de-stress
Looking for defects
Maybe I'll find them before I go eject
Huh, my grandmother been through a hellish endeavor
Made it through weather
That many would not have been sure to do better
Her husband would hit her
Beat her until she could not see the lever
To pull her and sever
From everything that she had come to know ever
But she took my father and she took my unc'
Fled the cape from that fucking punk
Single mother working in them London hubs
Never begged, hustled for that lunch
Scrapping scraping, never giving up
She gave life to Dad who gave life to me
Look what we've become yeah

[Chorus]
And you may not remember me
Tomorrow when you wake up
I'll just wait to see
If you call for my name
You may not remember me
Tomorrow when you wake up
I'll just wait to see
If you call for my name
If you call for my name

[Verse 2]
They say my nan's got memory loss
Body working better than her mind
When I call nowadays she barely talks
Sometimes I think she really knows it's me
But then her tone of voice will change
And then she mumbles something only she
Can understand and try to frame
I was on long-distance, speaking through to Spain
Swore that she was calling out my name
But when I asked her to repeat again
She was riding on a truly different train
I don't know
When the mind's not connected, what perspective do we know?
Is she trapped, cold, walking through the snow?
Is she sittin' on a beach with every soul that she's ever known?
Or is she all alone?
I just can't really find out through the phone
But my O says a visit won't be known
So I sit and wonder if she's here or no?
What a joke, this whole thing is simply blown
I just pace back and forth, try to go
Forward through the things I just can't control
Huh, her body is here but her mind I can't find
Huh, I just really wish I could've gotten the time
For giving that queen her proper reply
We love you Valerie Ryan, goodbye

[Chorus]
And you may not remember me
Tomorrow when you wake up
I'll just wait to see
If you call for my name
You may not remember me
Tomorrow when you wake up
I'll just wait to see
If you call for my name
If you call for my name