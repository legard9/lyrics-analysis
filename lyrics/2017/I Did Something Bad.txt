[Chris]
Guess what, ever since the world began
Same plot, everyone's been dumping on a fellow man
Pounding people they feel better than
I hope you're taking notes, cause
You feel everyone deserves shit
Get real, some of us have got it, girl, and some got squat
That's the truth, oh honey, like it or not
I hate to break it to you
My daddy
 taught me you get nowhere being nice
So now I'm sharing his advice
The world according to Chris is
Better to strike than get struck
Better to screw than get screwed
You'd probably think it's bizarre
But that's the way things are!

[Sue] (spoken)
But that doesn't mean it has to be that way. What does it cost to be kind?

[Chris] (spoken)
Ew. What have you done with my best friend? Billy, so tell me! Am I right or am I right?

[Billy] (spoken)
Trust me

[Billy]
I swear, there's a dick in every class
Not fair, each and every time I fail, those losers pass
Hey, every one of them can kiss my ass

[Chris]
You wonder why I love him

[Billy]
One year, there was this good looking guy
So queer,
 and on top of that he had a wandering eye
One day he looks at me 
a -huh!-
 bye-bye

[Chris]
I bet he got the message

[Billy/Chris]
We're here to tell you how this whole damn freak show works
If you don't listen, then you're jerks

[Billy/Chris/Ensemble]
The world according to Chris is
Better to punch than get punched
Better to burn than get burned
Learn that and you're gonna go far

[Billy]
Cause that's the way things are!

[Chris]
Hananana Nanananana
Hananana Nananana

[Ensemble]
Hananana Nanananana
Hananana Nananana

[Sue] (spoken)
Do you believe her?!

[Tommy] (spoken)
Come on. She's just being Chris

[Sue] (spoken)
You weren't there! It was awful, 
we were hurting Carrie!

[Tommy] (spoken)
I'm sure it wasn't that bad

[Sue]
Tommy, you don't understand
What was just a joke got out of hand
We kept on screaming 'til she hit the floor

[Tommy] (spoken)
Come on, everybody was doing it

[Sue]
Hey, I was in there, too
What came over me was something new
I did things I've never done before!
And now I wish there's something I could do or say
I've never ever felt this way

[Tommy]
Look, Sue
Don't be so hard on yourself
You can tell me to keep my mouth shut
But, wanna know what I'd advise?

[Sue] (spoken)
What?

[Tommy]
Apologize

[Sue] (spoken)
Apologize…! Oh, Tommy, that's genius!

[Chris]
Ew, Sue, I can tell you're feeling sad

[Chris/Ensemble]
Boo-hoo! So we clobbered Carrie and it's too damn bad

[Chris]
This is why you gotta love my dad

[Ensemble]
He's got the right idea!

[Chris]
My daddy taught me who's on top and who's below

[Ensemble]
Who's below

[Chris]
And now it's time I let you know

[Chris/Ensemble]
The world according to...

[Chris]
Chris!

[Sue] (spoken)
You're joking, right? You can't possibly mean all this!

[Chris] (spoken)
Why are you being such a buzzkill?

[Sue] (spoken)
Chris, grow up!

[Students]
Hananana Nananana
Hananana Nanananana
Hananana Nananana
Hananana Nanananana

[Billy]
Let's party, people!

[Ensemble]
According to Chris!

[Billy] (Ensemble)
The world according to Chris! (The world!)
The world according to Chris! (According to Chris!)
The world according to Chris! (The world!)
The world according to Chris! (The world!)

[Chris]
The world according to Chris is
Better to whip than get whipped
Even if somebody bleeds
Please, nobody dies from a scar
And that's the way things are