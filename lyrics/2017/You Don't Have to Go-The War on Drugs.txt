[Verse 1]
Love is a bird I can't even see
Even in the darkness right in front of me
And you were shining bright through the window
In the morning
There's a tone that rises gently
With the wind

[Verse 2]
You can be free, sometimes brave
Sometimes all you wanna do is run away
I don't need the curse, the hurt is much too real
Are we even lost?
I'm silently waiting
For how the room will be painted

[Chorus]
Yeah, I know
It’s hard
This time
Yeah, I know
That you know
This time

[Verse 3]
How could I wait 'til you recognized me
When you were there inside my dreams?
No, you don't have to go, I wanna make you stay
Goodbye
Anyway
I heard the news today

[Verse 4]
I've been up since the break of dawn
I lost my mind today
I'm at the sea, and I can hear the trains
Winds of change so new
Blowin' right through me and blowin' back through you
Then pull you into the light, light, yeah

[Instrumental Break]

[Verse 5]
Now I'm home and it's clear you're gone
Lost my edge today
Singing all the songs in the pouring rain
Pushed and pulled apart
At the seams, I can feel the chains
The winds of love blow few
Let it move through me, let it blow through you
And take you into the night, yeah, ooh