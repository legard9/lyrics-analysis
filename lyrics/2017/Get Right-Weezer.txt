[Verse 1]
I've been spending time alone
Thinking 'bout the things that could've been
It's harder on my own
I didn't think it would turn out like this
I made a shrine with pictures of you
I tried and tried to get to the truth
But I'm afraid to look
And choosing one means losing all the rest

[Chorus]
I know you're out there waiting for me there
But I don't know if I can get right
I know you're out there waiting for me there
But I don't know if I can get by

[Verse 2]
Raise your candle high
I would never turn my back on you
I'm your tiger burning bright
Even for just a day or two
There's something you should probably know
I hate to be the man in your phone
With alligator shoes
'Cause I really am in love with you

[Chorus]
I know you're out there waiting for me there
But I don't know if I can get right
I know you're out there waiting for me there
But I don't know if I can get by
Singing to my door, keep your fingers crossed
Don't evaporate like a beautiful mirage
I know you're out there waiting for me there
But I don't know if I can get right

[Bridge]
Why won't you tell me, I already know the answer
Why won't you tell me it's over, it's over
Why won't you tell me, I already know the answer
Why won't you tell me it's over, it's over

[Chorus]
I know you're out there (out there) waiting for me there
But I don't know if I can get right
I know you're out there (out there) waiting for me there
But I don't know if I can get by
Singing to my door, keep your fingers crossed
Don't evaporate like a beautiful mirage
I know you're out there (out there) waiting for me there
But I don't know if I can get right