[Verse 1]
Bird species never seen or heard before
The first flute carved from the first fauna

[Refrain 1]
Utopia
It’s not elsewhere
Let's purify

[Verse 2]
You assigned me to protect our lantern
To be intentional about the light

[Refrain 2]
Utopia
It isn't elsewhere
It’s here

[Verse 3]
My instinct has been shouting at me for years
Saying, "Let’s get out of here!"
Huge toxic tumour bulging underneath the ground here
Purify, purify, purify, purify toxicity