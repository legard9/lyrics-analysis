[NANKI-POO]
A wandering minstrel I —
A thing of shreds and patches
Of ballads, songs and snatches
And dreamy lullaby!
My catalogue is long
Through every passion ranging
And to your humours changing
I tune my supple song!
I tune my supple song!
Are you in sentimental mood?
I'll sigh with you
Oh, sorrow, sorrow!
On maiden's coldness do you brood?
I'll do so, too —
Oh, sorrow, sorrow!
I'll charm your willing ears
With songs of lovers' fears
While sympathetic tears
My cheeks bedew —
Oh, sorrow, sorrow!
But if patriotic sentiment is wanted
I've patriotic ballads cut and dried;
For where'er our country's banner may be planted
All other local banners are defied!
Our warriors, in serried ranks assembled
Never quail — or they conceal it if they do —
And I shouldn't be surprised if nations trembled
Before the mighty troops of Titipu!

[Chorus]
We shouldn't be surprised if nations trembled
Trembled with alarm
Before the mighty troops
The troops of Titipu!

[NANKI-POO]
And if you call for a song of the sea
We'll heave the 
capstan
 round
With a yeo heave ho, for the wind is free
Her anchor's a-trip and her helm's a-lee
Hurrah for the homeward bound!

[Chorus]
Yeo-ho — heave ho —
Hurrah for the homeward bound!

[NANKI-POO]
To lay aloft in a howling breeze
May tickle a landsman's taste
But the happiest hour a sailor sees
Is when he's down
At an inland town
With his Nancy on his knees, yeo ho!
And his arm around her waist!

[Chorus]
Then man the capstan — off we go
As the fiddler swings us round
With a yeo heave ho
And a rum below
Hurrah for the homeward bound!
With a yeo heave ho
And a rum below
Yeo-ho, heave ho
Yeo-ho, heave ho
Heave ho, heave ho, yeo-ho!

[NANKI-POO]
A wandering minstrel I —
A thing of shreds and patches
Of ballads, songs and snatches
And dreamy lullaby!

[Chorus]
And dreamy lulla-lullaby
Of dreamy lullaby
Lullaby! Lullaby!

[PISH-TUSH, spoken]
And what may be your business with Yum-Yum?

[NANKI-POO, spoken]
I'll tell you. A year ago I was a member of the Titipu town band. It was my duty to take the cap round for contributions. While discharging this delicate office, I saw Yum-Yum. We loved each other at once, but she was betrothed to her guardian Ko-Ko, 
a cheap tailor, and I saw at once that my suit was hopeless.
 Overwhelmed with despair, I quitted the town. Judge of my delight when I heard, a month ago that Ko-Ko had been condemned to death for flirting! I hurried back at once, in hope of finding Yum-Yum at liberty to listen to my protestations

[PISH-TUSH, spoken]
It is true that Ko-Ko was condemned to death for flirting, but he was reprieved at the last moment, and raised to the exalted rank of Lord High Executioner under the following remarkable circumstances: