[Chorus]
You know we're just working on it
No leaderboard keeping score
Long as you woke up this morning
And see you're working on it
It might be taking forever, but it's better than never

[Verse 2]
Smoking two packs a day, down to one
It don't sound like much, but for him, that's a revelation
You shoulda seen the pride in his eyes
When he said how far he's come from where he was

[Pre-Chorus]
I'm like, oh yeah, I get it, it's alright
We all got our monsters that don't see the day light
I'm like, oh yeah, I get it, it's alright
Skeletons you're hiding ain't gon' leave overnight

[Chorus]
You know we're just working on it
No leaderboard keeping score
Long as you woke up this morning
And see you're working on it
It might be taking forever, but it's better than never
So go easy, easy
Everybody got ugly, ugly
You know we're just working on it
We're working on it, working on it

[Verse 2]
Last night, you left the bar all alone
First time in a while you slept at home
And now you're back to chasing that watered-down love
You already know that ain't what you want

[Pre-Chorus]
And I'm like, oh yeah, I get it, it's alright
We all got our monsters that don't see the day light
I'm like, oh yeah, I get it, it's alright
Skeletons you're hiding ain't gon' leave overnight

[Chorus]
You know we're just working on it
No leaderboard keeping score
Long as you woke up this morning
And see you're working on it
It might be taking forever, but it's better than never
So go easy, easy
Everybody got ugly, ugly
You know we're just working on it
We're working on it, working on it

[Post-Chorus]
You know we're just working on it
We're working on it, working on it

[Bridge]
Put a magnifying glass to my life
Believe me, you will find some shit
Your eyes should unsee, ears unhear
Let's be clear, we all shady
Day by day, we fight the good fight
Maybe once in a while, we find the strength to be okay
And find some patience for ourselves
Don't be so hard upon yourself
Show some love to yourself

[Chorus]
You know we're just working on it
No leaderboard keeping score
Long as you woke up this morning
And see you're working on it
It might be taking forever, but it's better than never
So go easy, easy
Everybody got ugly, ugly
You know we're just working on it
We're working on it, working on it

[Post-Chorus]
Go easy, easy
Everybody got ugly, ugly
You know we're just working on it
We're working on it, working on it