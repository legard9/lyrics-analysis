NCT
2016
2017
2018
2019
U
The 7th Sense
, 
Without You
EMPATHY
127
NCT #127
LIMITLESS
, 
Cherry Bomb
Regular-Irregular
, 
Chain
Awaken
DREAM
CHEWING GUM
The First
, 
We Young
We Go Up
We Boom
WayV
The Vision
Please read my comment down below if you find something is missing.
 - Kkeudeopshi
2016
2016-04-08

[
The 7th Sense
 - Single]
1. 
The 7th Sense (일곱 번째 감각)
2016-04-10

[
Without You
 - Single]
1. 
Without You
2. 
Without You
 (Chinese Ver.)
2016-07-10

[
NCT #127
 - Mini Album]
1. 
Fire Truck (소방차)
2. 
Once Again (여름 방학)
3. 
Wake Up
4. 
Another World
5. 
Paradise
6. 
Mad City
7. 
Switch (Feat. SR15B)
2016-07-28

[
Taste The Feeling
 - Single]
1. 
Taste the Feeling
2016-08-24

[
CHEWING GUM
 - Single]
1. 
Chewing Gum
2. 
Chewing Gum
 (Chinese Ver.)
2017
2017-01-06

[
NCT #127 LIMITLESS
 - Mini Album]
1. 
無限的我 (무한적아;Limitless)
2. 
Good Thing
3. 
Back 2 U (AM 01:27)
4. 
롤러코스터 (Heartbreaker)
5. 
Baby Don't Like It (나쁜 짓)
6. 
Angel
2017-01-09

[
The First
 - The 1st Single Album]
1. 
마지막 첫사랑 (My First and Last)
2. 
最後的初戀 (My First and Last)
3. 
덩크슛 (Dunk Shot)
4. 
Chewing Gum
5. 
Chewing Gum (泡泡糖)
2017-06-14

[NCT #127 CHERRY BOMB
 - The 3rd Mini Album]
1. 
CHERRY BOMB
2. 
Running 2 U
3. 
0 Mile
4. 
Sun & Moon
5. 
Whiplash
6. 
Summer 127
7. 
CHERRY BOMB (Performance Ver.)
2017-08-07

[School 2017 (Original Television Soundtrack), Pt. 4]
1. 
Stay In My Life
2017-08-17

[We Young - The 1st Mini Album]
1. 
We Young
2. 
La La Love
3. 
같은 시간 같은 자리 (Walk You Home)
4. 
My Page
5. 
We Young
 (Chinese Ver.)
6. 
Trigger The Fever
2017-12-15

[Joy - Single]
1. 
Joy
2018
2018-01-12

[텐데… (Timeless) - Single]
1. 
텐데… (Timeless)
2018-01-30

[Radio Romance OST Pt. 1]
1. 
Radio Romance
2018-03-14

[NCT 2018: Empathy]
1. 
Intro: Neo Got My Back
2. 
TOUCH
3. 
BOSS
4. 
Baby Don't Stop
5. 
YESTODAY
6. 
GO
7. 
Black on Black
8. 
일곱 번째 감각 (The 7th Sense)
9. 
WITHOUT YOU
10. 
WITHOUT YOU
 (Chinese Ver.)
11. 
텐데… (Timeless)
12. 
夢中夢 (몽중몽); Dream In A Dream
13. 
OUTRO: VISION
14. 
YESTODAY (Extended Ver.)
 (Bonus Track)
2018-04-06

[New Heroes - Single]
1. 
New Heroes
2018-05-23

[Chain - EP]
1. 
Dreaming
2. 
Chain
3. 
Limitless
 (Japanese Ver.)
4. 
Come Back
5. 
100
2018-06-11

[Hard for me]
1. 
Hard for me
2018-09-03

[We Go Up - The 2nd Mini Album]
1. 
We Go Up
2. 
1, 2, 3
3. 
너와 나 (Beautiful Time)
4. 
Drippin'
5. 
Dear DREAM
6. 
We Go Up
 (Chinese Ver.)
2018-09-07

[New Dream - Dokgo Rewind OST]
1. 
New Dream
2018-10-12

[NCT #127 Regular-Irregular]
1. 
지금 우리 (City 127)
2. 
Regular
 (Korean Ver.)
3. 
Replay (PM 01:27)
4. 
Knock On
5. 
나의 모든 순간 (No Longer)
6. 
Interlude: Regular To Irregular
7. 
내 Van (My Van)
8. 
악몽 (Come Back)
 (Korean Ver.)
9. 
신기루 (Fly Away With Me)
10. 
Regular
 (English Ver.)
11. 
Run Back 2 U
 (Bonus Track)
2018-10-21

[Up Next Session: NCT 127]
1. 
CHERRY BOMB
 (English Version)
2. 
Regular (English Ver.)
 [Domdada Remix]
3. 
What We Talkin' About
4. 
소방차 (Fire Truck)
 [Kago Pengchi Remix]
2018-11-23

[NCT #127 Regulate]
1. 
지금 우리 (City 127)
2. 
Regular
  (Korean Ver.)
3. 
Replay (PM 01:27)
4. 
Welcome To My Playground
5. 
Knock On
6. 
나의 모든 순간 (No Longer)
7. 
내 Van (My Van)
8. 
Interlude: Regular To Irregular
9. 
Simon Says
10. 
악몽 (Come Back)
 (Korean Ver.)
11. 
신기루 (Fly Away With Me)
12. 
Chain
 (Korean Ver.)
13. 
Regular
 (English Ver.)
14. 
Run Back 2 U
 (Bonus Track)
2018-12-13

[DREAMWORKS x S.M STATION]
1. 
Hair in the Air
2. 
Best Day Ever
2018-12-27

[사랑한단 뜻이야 (Candle Light) - Single]
1. 
사랑한단 뜻이야 (Candle Light)
2019
2019-01-01

[See the V]
1. 
See the V
2019-01-17

[The Vision]
1. 
理所当然 (Regular)
2. 
噩梦 (Come Back)
3. 
梦想发射计划 (Dream Launch)
2019-02-22

[Let's Shut Up & Dance]
1. 
Let's Shut Up & Dance
2019-04-18

[Awaken]
1. 
Lips
2. 
Wakey-Wakey
3. 
Chain
4. 
Regular
 (English Ver.)
5. 
Touch
  (Japanese Ver.)
6. 
Blow My Mind
7. 
Limitless
 (Japanese Ver.)
8. 
Long Slow Distance
9. 
Kitchen Beat
10. 
CHERRY BOMB
11. 
소방차 (Fire Truck)
12. 
End to Start
2019-05-09

[无翼而飞(Take Off)]
1. 
无翼而飞 (Take Off)
2. 
理所当然 (Regular)
3. 
真实谎言 (Say It)
4. 
噩梦 (Come Back)
5. 
爱不释手 (Let Me Love U)
6. 
梦想发射计划 (Dream Launch)
2019-05-24

[We Are Superhuman]
1. 
Superhuman
2. 
Highway to Heaven
3. 
아 깜짝이야 (FOOL)
4. 
시차 (Jet Lag)
5. 
종이비행기 (Paper Plane)
6. 
OUTRO: WE ARE 127
2019-06-06

[Don’t Need Your Love]
1. 
Don’t Need Your Love
2019-07-03

[So Am I (Ava Max Collab)]
1. 
So Am I (Remix)
2019-07-15

[Fireflies - THE OFFICIAL SONG OF THE WORLD SCOUT FOUNDATION]
1. 
Fireflies
2019-07-19

[Highway to Heaven (English Ver.)]
1. 
Highway to Heaven
 (English Ver.)
2019-07-26

[We Boom]
1. 
BOOM
2. 
STRONGER
3. 
119
4. 
Bye My First. . .
5. 
Best Friend
6. 
Dream Run
2019-08-09

[일진에게 찍혔을 때 OST Part.1]
1. 
NEW LOVE
2019-10-01

[Baby Only You - The Tale of Nokdu OST]
1. 
Baby Only You
2019-10-29

[Take Over the Moon - The 2nd Mini Album]
1. 
天选之城 (Moonwalk)
2. 
黑夜日出 (Yeah Yeah Yeah)
3. 
秘语 (Love Talk)
4. 
心心相瘾 (King of Hearts)
5. 
面对面 (Face to Face)
6. 
幸福遇见 (We go nanana)
2019-11-22

[Up to You]
1.
Up to You
2019-12-13

[Coming Home]
1. 
Coming Home

[2020]
2020-01-22

[The Dream]
1. 
Chewing Gum
2. 
마지막 첫사랑 (My First and Last)
3. 
We Young
4. 
GO
5. 
We Go Up
6. 
BOOM
7. 
사랑한단 뜻이야 (Candle Light)
2020-03-06

[NCT #127 Neo Zone - The 2nd Album]
1. 
Elevator (127F)
2. 
영웅 (英雄; Kick It)
3. 
꿈 (Boom)
4. 
낮잠 (Pandora's Box)
5. 
Day Dream (白日夢)
6. 
Interlude: Neo Zone
7. 
뿔 (MAD DOG)
8. 
Sit Down!
9. 
메아리 (Love Me Now)
10. 
우산 (Love Song)
11. 
백야 (White Night)
12. 
Not Alone
13. 
Dreams Come True
2020-04-29

[RELOAD]
1. 
Ridin'
2. 
Quiet Down
3. 
내게 말해줘 (7 Days)
4. 
사랑은 또다시 (Love Again)
5. 
너의자리 (Puzzle Piece)
2020-05-19

[NCT #127 Neo Zone: The Final Round]
1. 
Punch
2. 
NonStop
3. 
Prelude
4. 
Kick It
5. 
Boom
6. 
Pandora's Box
7. 
Day Dream
8. 
너의 하루 (Make Your Day)
9. 
Interlude: Neo Zone
10. 
MAD DOG
11. 
Sit Down!
12. 
Love Me Now
13. 
우산 (Love Song)
14. 
백야 (White Night)
15. 
Not Alone
16. 
Dream Comes True
17. 
Elevator 127F
2020-06-09

[Awaken The World]
1. 
超越时空 (Turn Back Time)
2. 
Bad Alive
3. 
执迷 (Unbreakable)
4. 
After Midnight
5. 
Interlude: Awaken the World
6.
Only Human
7. 
多米诺 (Domino)
8.  
浪漫发酵 (Up From Here)
9. 
Electric Hearts
10. 
Stand By Me
2020-10-12

[RESONANCE Pt. 1]
1. 
Make A Wish (Birthday Song)
2. 
Misfit
3. 
Volcano
4. 
백열등 (Light Bulb)
5. 
Dancing In The Rain
6. 
Interlude: Past to Present
7. 
무대로 (Déjà Vu; 舞代路)
8. 
月之迷 (Nectar)
9. 
Music, Dance
10. 
피아노 (Faded In My Last Song)
11. 
From Home
12. 
From Home
 (Korean Ver.)
13. 
Make A Wish (Birthday Song)
 [English Ver.]
NCT 2020 : The Past & Future - Ether
RESONANCE
2020-11-23

[RESONANCE Pt. 2]
1. 
90's Love
2. 
Misfit
3. 
Raise the Roof
4. 
Volcano
5. 
백열등 (Light Bulb)
6. 
Dancing In The Rain
7. 
My Everything
8. 
Interlude: Past to Present
9. 
Make A Wish (Birthday Song)
10. 
무대로 (Déjà Vu; 舞代路)
11. 
月之迷 (Nectar)
12. 
Music, Dance
13. 
피아노 (Faded In My Last Song)
14. 
From Home
15. 
From Home
 (Korean Ver.)
16. 
Make A Wish (Birthday Song)
 [English Ver.]
17. 
피아노 (Faded In My Last Song)
18. 
Work It
19. 
단잠 (All About You)
20. 
I.O.U
21. 
Outro: Dream Routine
RESONANCE
2020-12-04

[RESONANCE]
1. 
RESONANCE
2021
2021-02-17

[Loveholic]
1. 
gimme gimme
2. 
Lipstick
3. 
First Love
4. 
Chica Bom Bom
5. 
영웅 (英雄; Kick It)
6. 
Right Now