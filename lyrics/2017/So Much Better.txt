[Intro]
The bottom feels so much better than the top!
So much better

[Chorus]
Nan, you're a window shopper
Taking a look, but you never buy
Nan, you're a window shopper
You won't pick it up if it's over a fiver
Nan, you're a window shopper
Get on the bus 'cause you still can't drive
Nan, you're a window shopper
Mad as fuck, only just alive

[Verse 1]
Get up in the morning and you like your tea milky
You fumble for your glasses 'cause without them, you can't see
It's funny how I come round your house and I'm 20
And I still have to wear all the presents you sent me
I walk into your kitchen, everything's got a label
You done your Christmas shopping and we're only in April
And you won't leave the house unless you're wearing your thermals
You're covered all in cat hair and you're stinking like Strepsils
You're heading down the Bowl's Club
Have another orange squash
Balls are rollin', rollin', rollin'
You can't walk right 'cause things aren't what they were
Your ankles are swollen, swollen, swollen

[Chorus]
Nan, you're a window shopper
Taking a look, but you never buy
Nan, you're a window shopper
You won't pick it up if it's over a fiver
Nan, you're a window shopper
Get on the bus 'cause you still can't drive
Nan, you're a window shopper
Mad as fuck, only just alive

[Verse 2]
You're walking down the post office to pick up your pension
And then you're off to Bingo, it's become an obsession
So weary of the kids when they're wearing their hoods up
And even if they smile at you, you think it's a stick-up
You only buy the paper just to cut out the coupons
You're saving 50p, but what do you want with tampons?
You're always at the doctor picking up your prescription
And they throw in some K-Y just to ease up the friction
You got a leak in your colostomy bag
Yeah, it's got a hole in, hole in, hole in
At the weekend you're shopping with your trolley
It's sad how you're rollin', rollin', rollin'

[Chorus]
Nan, you're a window shopper
Taking a look, but you never buy
Nan, you're a window shopper
You won't pick it up if it's over a fiver
Nan, you're a window shopper
Get on the bus 'cause you still can't drive
Nan, you're a window shopper
Mad as fuck, only just alive