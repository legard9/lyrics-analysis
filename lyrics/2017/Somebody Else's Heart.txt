[Peter]
Nants ingonyama bagithi Baba
Sithi uhm ingonyama
Nants ingonyama bagithi baba
Sithi uhhmm ingonyama
Ingonyama
Siyo Nqoba
Ingonyama
Ingonyama nengw' enamabala x9
I can show you the world
Shining shimmering splendid
Tell me princess now when did you last
Let your heart decide?

[Alex]
I can open your eyes
Take you wonder by wonder
Over sideways and under
On a magic carpet ride

[Peter]
A whole new world
A new fantastic point of view
No one to tell us no, or where to go
I'd say you're only dreaming

[Alex]
A whole new world
A dazzling place I never knew
But when i'm way up here, it's crystal clear
Now i'm in a whole new world with you
La da da da da, La da da da da
The rainstorm and the river are my brothers
The heron and the otter are my friends

[Peter & Alex]
And we are all connected to each other

[Alex]
In a circle, in a hoop, that never ends

[Peter & Alex]
Have you ever heard the wolf cry to the blue corn moon
Or asked the grinning bobcat why he grinned?

[Alex]
Can you sing with all the voices of the mountain?
Can you paint with all the colors of the wind?

[Peter]
The seaweed is always greener
In somebody else's lake
You think about going up there
But that is a big mistake
Just look at the world around you
Right here on the ocean floor
Such wonderful things surround you
What more is you looking for?

[Alex]
Under the sea
Under the sea
Darling its better, down where it's wetter
Take it from me
Up on the shore they work all day
Out in the sun they slave away
While we devoting, full time of floating
Under the sea

[Peter & Alex]
Ever just the same

[Peter]
Ever a suprise

[Peter & Alex]
Ever as before, ever just as sure
As the sun will rise

[Peter]
Oh oh oh ohh
Certain as the sun (certain as the sun)
Rising in the east

[Alex]
Tale as old as time

[Peter & Alex]
Song as old as rhyme
Beauty and the beast

[Peter & Alex]
Til' we find our place
On the path unwinding
In the circle
The circle of life!