[Verse 1]
Lately I ain't been the same man
You knew me to be
Knew me to be
I've been so caught up, so thoughtless
You know that ain't me
Know that ain't me

[Pre-Chorus]
But even in my darkest hour
Yeah you were a light I could see
But all of us fall
You know when you call
Yeah you can count on me

[Chorus]
When the roof falls in
When the windows break
It's all you can take
Yeah you can count on me
When you come undone
When it all goes wrong
And you need someone
Yeah you can count on me

[Verse 2]
I know some mornings
It's hard just to roll out of bed
Roll out of bed
I know I've been there
How crazy and cold life can get
Cold life can get

[Pre-Chorus]
But even in my darkest hour
Yeah you were a light I could see
But all of us fall
You know when you call
Yeah you can count on me

[Chorus]
When the roof falls in
When the windows break
It's all you can take
Yeah you can count on me
When you come undone
When it all goes wrong
When you need someone
Yeah you can count on me

[Bridge]
Like I counted on you
When your love dragged me through
I'm still wearing these bruises
But you set me free
Oh darling you'll see
Yeah you can count on me

[Chorus x2]
When the roof falls in
When the windows break
It's all you can take
Yeah you can count on me
When you come undone
When it all goes wrong
When you need someone
Yeah you can count on me

When the roof falls in
When the windows break
It's all you can take
Yeah you can count on me
When you come undone
When it all goes wrong
And you need someone
Yeah you can count on me