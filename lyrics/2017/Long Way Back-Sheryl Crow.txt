[Verse 1]
I got an up-close glimpse of the outside world
It's an awful big place, well that's for damn sure
Seen more of this life than most have seen
And it's taken a mighty big toll on me
It sure feels good to be free

[Chorus]
Some days I feel alright
Some days I can't wait until it's night
Sometimes you gotta face the light
Get back in the ring, put on your gloves and fight
I think it's gonna take some time
To find a way to ease my mind
It's gonna take a long way back home
So I'm gonna take the long way back home
Back home

[Verse 2]
Did you ever see a man have a heart attack?
It'll open your eyes and stop you in your tracks
So I threw away my last cigarette
Get it right this time and clean up my act
That's a hard cold fact

[Chorus]
Some days I feel alright
Some days I can't wait until it's night
Some days you gotta face the light
Get back in the ring, put on your gloves and fight
I think it's gonna take some time
To find a way to ease my mind
I'm gonna take the long way back home
I'm gonna take the long way back home
Oh, back home

[Bridge]
So many things I ain't done yet
So many things I've done that I forget
I just need a place to rest my head

[Chorus]
Some days I feel alright
Some days I can't wait until it's night
Some days you gotta face the light
Get back in the ring, put on your gloves and fight
I think it's gonna take some time
To find a way to ease my mind
I'm gonna take the long way back home

[Chorus]
Some days I feel alright
Some days I can't wait until it's night
Some days you gotta face the light
Get back in the ring, put on your gloves and fight
I think it's gonna take some time
To find a way to ease my mind
I'm gonna take the long way back home
So I'm gonna take the long way back home
So I'm gonna take the long way back home
So I'm gonna take the long way back home
Back home