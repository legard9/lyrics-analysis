Hangul

손가락을 짚어서 세어봐
울지 않았던 날을
너를 만나고 행복했냐고?
정말로 사랑은 했냐고?

그 사람보다 큰 침대 밑에서
왜 이리도 아기처럼 울고
있는지 있는지
너를 못 잊는지 (I feel you girl)

빨래 바구니를 들고 태연한 척
하루가 어떻게 가는지 모르고
매일을 바쁘게 보내야 할 거야
넌 내가 늘 강한 줄 알거야 (하지만)

나도 여자예요 baby
나도 여자예요 baby
너 땜에 아침에 눈을 뜨면 울고
바람에 스쳐도 다시 울고
나도 여자예요 baby

짹깍 거리는 시계
나도 모르게 너를 기다려
티슈 한 박스 는 쓴 거 같아
눈물이 정말 마를지 몰라
(I feel you girl)

때 지난 드라마를 보고 태연한 척
하루가 어떻게 가는지 모르고
매일을 바쁘게 보내야 할 거야
넌 내가 늘 강한 줄 알거야 (하지만..)

나도 여자예요 baby
나도 여자예요 baby
너 땜에 아침에 눈을 뜨면 울고
바람에 스쳐도 다시 울고
나도 여자예요 baby
Sing it with me now!

I love you love you love you
I love you love you
I love you love you love you
약해지고 있어 난

I love you love you love you
I love you love you
I love you love you love you
너 없인 못 사는데

나도 여자예요 baby
나도 여자예요 baby
어서 미안하다고 말해
어서 sing it with me now!

I love you love you love you
I love you love you
I love you love you love you
약해지고 있어 난

I love you love you love you
I love you love you
I love you love you love you
너 없인 못 사는데

고픈데 보고픈데
그대 사랑이 난 고픈데
(I wanna feel the love)
아픈데 너무 아픈데
그댈 좋은 것만 기억해
(you’re gonna be ok)

고픈데 보고픈데
그대 사랑이 난 고픈데
(I wanna feel the love)
아픈데 너무 아픈데
(but what you say)

I love you love you love you

Romanization

Songarageul jipeoseo seeobwa
Ulji anatdeon nareul
Neoreul mannago haengbokaennyago?
Jeongmallo sarangeun haennyago?

Geu saramboda keun chimdae miteseo
Wae irido agicheoreom ulgo
Inneunji inneunji
Neoreul mot inneunji (i feel you girl)

Ppallae bagunireul deulgo taeyeonhan cheok
Haruga eotteoke ganeunji moreugo
Maeireul bappeuge bonaeya hal geoya
Neon naega neul ganghan jul algeoya (hajiman)

Nado yeojayeyo baby
Nado yeojayeyo baby
Neo ttaeme achime nuneul tteumyeon ulgo
Barame seuchyeodo dasi ulgo
Nado yeojayeyo baby

Jjaekkkak georineun sigye
Nado moreuge neoreul gidaryeo
Tisyu han bakseu neun sseun geo gata
Nunmuri jeongmal mareulji molla
(i feel you girl)

Ttae jinan deuramareul bogo taeyeonhan cheok
Haruga eotteoke ganeunji moreugo
Maeireul bappeuge bonaeya hal geoya
Neon naega neul ganghan jul algeoya (hajiman..)

Nado yeojayeyo baby
Nado yeojayeyo baby
Neo ttaeme achime nuneul tteumyeon ulgo
Barame seuchyeodo dasi ulgo
Nado yeojayeyo baby
Sing it with me now!

I love you love you love you
I love you love you
I love you love you love you
Yakaejigo isseo nan

I love you love you love you
I love you love you
I love you love you love you
Neo eopsin mot saneunde

Nado yeojayeyo baby
Nado yeojayeyo baby
Eoseo mianhadago malhae
Eoseo sing it with me now!

I love you love you love you
I love you love you
I love you love you love you
Yakaejigo isseo nan

I love you love you love you
I love you love you
I love you love you love you
Neo eopsin mot saneunde

Gopeunde bogopeunde
Geudae sarangi nan gopeunde
(i wanna feel the love)
Apeunde neomu apeunde
Geudael joeun geonman gieokae
(you’re gonna be ok)

Gopeunde bogopeunde
Geudae sarangi nan gopeunde
(i wanna feel the love)
Apeunde neomu apeunde
(but what you say)

I love you love you love you