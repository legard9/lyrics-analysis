[Verse 1: Mikill Pane]
I'm not pulling out real guns to use
But I've got a good caliber of love for you
So if we enter something, I'll protect it too
Cause I wouldn't wanna leave any exit wounds
Some dude popped a gap and that reached your heart
But he was shot down like Nancy Sinatra
That means your armour put him to the test
But that geezer fucked up your bulletproof vest
I'll save you girl, so stop and I'll shoot
Then I'll make the world revolver round you
I've got my weapon, if I pop some lead in
I'll give a new meaning to a shotgun wedding
Screw pumps, I hunt to find a Mrs
With two guns of love and neither misses
When I walk around, I hold them in each hand
Like a taller, blacker Yosemite Sam

[Hook: Jodie Verses Everyone]
I've got a full clip baby, of love I wanna shoot to your heart
(Shoot the love to your heart, you can't run away)
I've got a full clip baby, of love I wanna shoot to your heart
(Shoot the love to your heart, you can't get away)

[Verse 2: Mikill Pane]
It's no surprise that a kid from Hackney
Grew up to become so trigger-happy
I bust shots of love, oh so specific
So Scotland Yard don't go ballistic
You know what Mikill Pane intends though
Think Golden Eye, do you play Nintendo?
Rules never apply in this gamer's head
So I use friendly fire to escape the friend zone
Every line is true, got to this spot
Cause I tell it like it is, shoot from the hip hop
But you're one step ahead and I'm feeling quite silly
Cause you're most def a ten and I deal with nine millies
But you know there can be one outcome
Wake up, smell the roses and the gunpowder
Use your head, this is no bullshit
And your heart for the whole full clip

[Hook x2]

[Bridge: Mikill Pane x2]
Pow, pow, the whole tune bangs
But it's not a sex shooter and I don't shoot blanks
I let the slugs fly cause they love to travel
I aim to make our names double-barreled

[Hook x4]