[Verse 1]
I can see you sitting on the hood of my car
Salty cigarette blowing East Coast stars
And that dress made a mess out of my heart
I could feel the tears of that beachside town
Sound of that roller-coaster slowing down
And the smell of Chanel on your neck

[Pre-Chorus]
Feel the heat of the sand
The sandals in your hand
Well, you played air guitar in the bar
Showed up the cover band, hey!

[Chorus]
Well, maybe I was crazy
Falling for you baby
Must have stayed in the rays too long
'Cause I still feel you darling
Long after the sunburn's gone

[Verse 2]
Well, you never gave a damn about being discreet
You'd climb over the console and recline my seat
And we'd lay in that sweat you drew hearts on my chest

[Pre-Chorus]
Feel the heat of the sand
The sandals in your hand
Well, you played air guitar in the bar
Showed up the cover band
Yeah, baby!

[Chorus]
Well, maybe I was crazy
Fallin' for you baby
Must have stayed in the rays too long
'Cause I still feel you darling
Long after the sunburn's gone

[Pre-Chorus]
Feel the heat of the sand
Baby, your sandals in your hand
Well, you played air guitar in the bar
Showed up the cover band

[Chorus]
Well, maybe I was crazy
Fallin' for you baby
Must have stayed in the rays too long
Cause I still feel you darling
Long after the sunburn's gone