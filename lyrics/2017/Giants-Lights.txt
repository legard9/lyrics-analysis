[Verse 1]
Are you getting tired of hanging around here?
Is it bringing you down?
We've been talking about how we could have no fear
If we got out of town

[Pre-Chorus]
Oh, the city towers over us
And all our problems make us powerless
Let's get somewhere where the both of us come rising up
Come rising up

[Chorus]
Where we could be giants
Bigger than the walls that hide us
Breaking all the laws of science
Looking at a sea of diamonds
If we could be, we could be giants, oh
If we could be giants, oh

[Verse 2]
I don't wanna be the one you can push by
At the back of the room
I would rather ride with you to the big sky
At the heart of the moon

[Pre-Chorus]
Oh, the city towers over us
And all our problems make us powerless
Let's get somewhere where the both of us can come rising up

[Chorus]
Where we could be giants
Bigger than the walls that hide us
Breaking all the laws of science
Looking at a sea of diamonds
If we could be, we could be giants, oh
If we could be giants, oh
If we could be, if we could be giants, oh, giants
If we could be giants, oh, giants

[Bridge]
If we could come up, no one ever would defy us
Top of the world, we'll be sitting at the highest
We could come up, no one ever would defy us
Top of the world, we'll be sitting at the highest

[Breakdown]
Where we could be giants (
we could be giants
)
Bigger than the walls that hide us (
bigger than walls that hide us
)
Breaking all the laws of science (
we could be giants
)
Looking at a sea of diamonds (
bigger than walls that hide us
)

[Chorus]
If we could be, we could be giants
Bigger than the walls that hide us
Breaking all the laws of science
Looking at a sea of diamonds
If we could be, if we could be giants, oh
If we could be giants, if we could be giants
If we could be, if we could be giants, oh
If we could be giants, if we could be giants
If we could be giants