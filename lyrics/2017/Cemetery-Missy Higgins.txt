[Verse 1]
I believe I did
Everything I could
I believe I did
Everything by the book
I believe I did
Lose the fight, lose the fight
Cause it wasn't enough
To give you everything
Was not enough
To let you deeply in
I ran through dark city streets to hold you
Through the night

[Chorus]
But where'd you end up now
Do we even live in the same town?
Do you ever think about that time
We snuck into the cemetery at night?
Oh where'd you end up, tell
Do you have a hole in your heart as well?
And do you ever think about that time
We snuck into the cemetery at night
Get out get out get out of my mind
Get out get out get out of my mind
I want to forget that night
Cause laying upon the dead with you
Laying upon the dead with you, my love
I've never felt so alive
Never felt so so alive
I've never felt so alive

[Verse 2]
I believe I gave everything I had
I believe I lost what you never gave back
I believe I did lose my mind
Lose my mind
Cause it wasn't enough
To let you call the shots
Was not enough just letting you be lost
I fell through hell every time you disappeared
Cause I loved you still

[Chorus]
But where'd you end up now
Do we even live in the same town?
Do you ever think about that time
We snuck into the cemetery at night?
Oh where'd you end up, tell
Do you have a hole in your heart as well?
And do you ever think about that time
We snuck into the cemetery at night
Get out get out get out of my mind
Get out get out get out of my mind
I want to forget that night
Cause laying upon the dead with you
Laying upon the dead with you, my love
I've never felt so alive
Never felt so so alive
I've never felt so alive
I've never felt so alive
I've never felt so alive

[Outro]
Now all the scars have finally faded, I realise
Since then I've never felt so alive