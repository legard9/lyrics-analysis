[Verse 1]
I was saving you from drowning
You were silent as a rain cloud
The ceiling was imploding
The walls were closing in
I was feeling a prisoner
Was running so fast
I was speeding through the red lights
Speeding past

[Chorus]
Still got mad love for you, baby
Still got mad love for you, baby
Every day you find ways to drive me crazy
Still got mad love for you, baby
Still got mad love for you, baby
Every day you find ways to drive me crazy

[Refrain]
Oh, oh
Oh, oh
Oh, oh
Oh, oh

[Verse 2]
So restless while waiting
You've been gone so long
I fell in your slipstream
Fell in my arms
And I want you to dance now
To dance just for me
It's all out of focus
You make me so free

[Chorus]
Still got mad love for you, baby
Still got mad love for you, baby
Every day you find ways to drive me crazy
Still got mad love for you, baby
Still got mad love for you, baby
Every day you find ways to drive me crazy

[Refrain]
Oh, oh
Oh, oh
Oh, oh
Oh, oh

[Chorus]
Still got mad love for you, baby
Still got mad love for you, baby
Every day you find ways to drive me crazy
Still got mad love for you, baby
Still got mad love for you, baby
Every day you find ways to drive me crazy

[Outro]
Oh, oh
Oh, oh
Mad love, mad love