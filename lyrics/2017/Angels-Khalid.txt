[Verse 1]
I've been seeing angels
In my living room
That have walked the sun
And have slept on the moon
Covered in the fragrance
Of their own perfume
Telling me the stories
Stories coming true
Well you see these angels
These angels see the light
Yeah I had my troubles
Troubles, all right
I've been seeing angels
Oh no

[Verse 2]
They'll hold onto their secrets
And torn up memories
We float above horizons
And sail across the seas
I hope for better days
And lightly times are tough
The angels give me strength
And I'm not giving up
So I wipe away my tears
I unveil my pain
They're brushing off my shoulders
And I hold on to their stain
I've been seeing angels

[Outro]
Angels
Angels
Angels
Angels
Angels
Angels, ayy
Angels