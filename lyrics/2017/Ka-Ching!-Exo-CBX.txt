Kanji
Ka-CHING!
Yeah, I got the money
You see the money?
Come and get it now
世界の全て　この手に掴め
欲しいものは全部
有り触れてる Fake 捨て　本物だけ
見せてやるよ Show time
I got it, you know it, I’m on it
金で買えない物　無い物ねだり
Ka-CHING Ka-CHING-CHING
Ka-CHING Ka-CHING Ka-CHING-CHING
価値を見極めろ　時は金なり
最上級の瞬間を
Entertain you baby
Life is way too short
Don’t waste your time and
Stop the music and listen
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING We’re on a payroll
足りない　足りない　叫ぼう欲望
最大限に楽しめよう
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING We’re on a payroll
Money’s good, money’s bad
使い方次第
Are you rich, are you poor?
そのハート次第
ダイヤに　ドレスに　車に
切り無い What do you choose?
Do what you do
I got it, you know it, I’m on it
金で買えない物　無い物ばかり
Ka-CHING Ka-CHING-CHING
Ka-CHING Ka-CHING Ka-CHING-CHING
急がば回れだ　時は金なり
最上級の瞬間を
Entertain you baby
Life is way too short
Don’t waste your time and
Stop the music
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING We’re on a payroll
足りない　足りない　叫ぼう欲望
最大限に楽しめよう
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING We’re on a payroll
Money money あるほど
Eenie meenie miney moe
But 意味ない物など要らないから Go
Boom boom 鳴らせよ
Let’s go party, let’s go baby
今しかないとき Oh
What you paying for?
What you paying for?
夢を手にするための代償
What you waiting for?

[ベ/チェ] What you waiting for?

[シ/チェ] 自ら手に入れろよ Payroll
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING We’re on a payroll
足りない　足りない　叫ぼう欲望
最大限に楽しめよう
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING We’re on a payroll
Ka-CHING
Romaji
Ka-CHING!
Yeah, I got the money
You see the money?
Come and get it now
Sekai no subete kono te ni tsukame
Hoshii mono wa zenbu
Arifureteru Fake sute honmono dake
Misete yaru yo Show time
I got it, you know it, I’m on it
Kane de kaenai mono nai mono nedari
Ka-CHING Ka-CHING-CHING
Ka-CHING Ka-CHING Ka-CHING-CHING
Kachi wo mikiwamero toki wa kane nari
Saijoukyuu no shunkan wo
Entertain you baby
Life is way too short
Don’t waste your time and
Stop the music and listen
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING We’re on a payroll
Tarinai tarinai sakebou yokubou
Saidaigen ni tanoshimeyou
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING We’re on a payroll
Money’s good, money’s bad
Tsukai kata shidai
Are you rich, are you poor?
Sono haato shidai
Daiya ni doresu ni kuruma ni
Kiri nai What do you choose?
Do what you do
I got it, you know it, I’m on it
Kane de kaenai mono nai mono bakari
Ka-CHING Ka-CHING-CHING
Ka-CHING Ka-CHING Ka-CHING-CHING
Isogaba maware da toki wa kane nari
Saijoukyuu no shunkan wo
Entertain you baby
Life is way too short
Don’t waste your time and
Stop the music
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING We’re on a payroll
Tarinai tarinai sakebou yokubou
Saidaigen ni tanoshimeyou
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING We’re on a payroll
Money money aru hodo
Eenie meenie miney moe
But imi nai mono nado iranai kara Go
Boom boom narase yo
Let’s go party, let’s go baby
Ima shikanai toki Oh
What you paying for?
What you paying for?
Yume wo te ni suru tame no daishou
What you waiting for?

[Baek/Chen] What you waiting for?

[Xiu/Chen] mizukara te ni irero yo Payroll
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING We’re on a payroll
Tarinai tarinai sakebou yokubou
Saidaigen ni tanoshimeyou
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING We’re on a payroll
Ka-CHING
English
Ka-CHING!
Yeah, I got the money
You see the money?
Come and get it now
Everything in the world, I will get
Everything I want
Everything’s usually fake I will show you
Only the real thing, show time
I got it, you know it, I’m on it
Begging for things you can’t buy or don’t have
Ka-CHING Ka-CHING-CHING
Ka-CHING Ka-CHING Ka-CHING-CHING
Let’s determine the price, time is money
At the most exciting moment
Entertain you baby
Life is way too short
Don’t waste your time and
Stop the music and listen
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING We’re on a payroll
Not enough, not enough shout out your desires
Let’s enjoy ourselves as much as we can
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING We’re on a payroll
Money’s good, money’s bad
It depends on how you use it
Are you rich, are you poor?
It depends on your heart
Diamonds, dresses, cars
No limit, what do you choose?
Do what you do
I got it, you know it, I’m on it
Is there nothing you can’t buy with money
Ka-CHING Ka-CHING-CHING
Ka-CHING Ka-CHING Ka-CHING-CHING
Haste makes waste, time is money
At the most exciting moment
Entertain you baby
Life is way too short
Don’t waste your time and
Stop the music
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING We’re on a payroll
Not enough, not enough shout out your desires
Let’s enjoy ourselves as much as we can
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING We’re on a payroll
As long as you have money money
Eenie meenie miney moe
But I don’t need meaningless things, gotta go
Boom boom it’s beating
Let’s go party, let’s go baby
There is no time but now
What you paying for?
What you paying for?
The price to obtain your dream
What you waiting for?
What you waiting for?
Let’s obtain by our own payroll
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING We’re on a payroll
Not enough, not enough shout out your desires
Let’s enjoy ourselves as much as we can
Ka-CHING Ka-CHING Ka-CHING Ka-CHING
Ka-CHING Ka-CHING We’re on a payroll
Ka-CHING