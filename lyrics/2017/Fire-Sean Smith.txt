After the work you took to the storm
"Get on your two feet." You yelled back through the store
And hand in hand we kept to the shade
And I never knew this God would be so brave
And after the storm we took to the south
Now that you're here you’ll try to help me out
There's nothing of interest nothing worth my time
I was losing my feet, now I'm losing my smile
I haven’t heard what you said in this town for a while
Now I'm losing my head in this drought and you brought back the fire
I'm so sick of this, man I'm taking it back
I want more meaning then a television gone black
And you kept me kind, yeah you kept me calm
What can I say? I love your voice
But I haven't heard what you said in this town for a while
Now I'm losing my head in this drought and you brought back the fire
Like a temple on fire you held me tight
Your soft spoken words they're the birthing of light
And my hands will stop shaking once and for all
Once and for all
But I haven't heard what you said in this town for a while
Now I’m losing my head in this drought and you brought back the fire
No, I cannot breath, man I cannot believe, I’ve looking at it the wrong way since the day I could see
Cuz I haven't heard what you said in this town for a while