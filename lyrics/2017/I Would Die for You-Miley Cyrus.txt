[Verse 1]
You are everything to me
And I, I would die for you
There've been times where I'm up all night
Crying in the dark, so I sleep with the light on
I've heard I've got words like a knife
That I don't know how to choose just so wisely
But I see trees and their colored leaves
When I think about all that we could be

[Chorus]
When you're gone, time moves so slow
Like the grass, I've watched us grow
I've heard you reap only what you'll sow
How could I never let you know?

[Verse 2]
That you are everything to me
You're sweeter than candy and better than any childhood dreams
I am yours and you are mine
I have your heart, I don't even need a ring
I'd give up all I have in exchange for who I love more than anything

[Chorus]
When you're gone, time moves so slow
Like the grass, I've watched us grow
I've heard you reap only what you'll sow
How could I ever let you know?

[Bridge]
'Cause you, you're everything to me
And I, I would die for you
Yeah, I would die for you

[Chorus]
When you're gone time, moves so slow (I would die)
Like the grass, I've watched us grow (I would die)
I've heard you reap only what you'll sow (I would die)
How could I never let you know?
I would die for you