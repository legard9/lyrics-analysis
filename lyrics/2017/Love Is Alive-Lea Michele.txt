[Verse 1]
It wakes me up at night
It needs to share its light
It doesn't get how dark it is outside
It warms the rising sun
It kisses everyone
It doesn't remember the hate of yesterday

[Chorus]
Oh, can't you hear it in my voice?
Oh, can't you see it in my eyes?
Love, love is alive in me

[Verse 2]
And when my golden crown
Becomes a cup of doubt
I try to remember all I need is all around

[Chorus]
Oh, can't you hear it in my voice?
Oh, can't you see it in my eyes?
Love, love is alive in me (oh, oh, oh)

[Outro]
Oh, can't you hear it in my voice?
Oh, can't you see it in my eyes?
Love, love is alive
Oh, can't you feel it in my touch?
Know that I'll always have enough
Love, love is alive in me (oh, oh, oh)