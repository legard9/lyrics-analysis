8/1
Caskey - Generation - 19/19
Courier - Courier - 10/10
furino - from the i miss you department - 2/2
GFRIEND - Parallel - 7/7
wifisfuneral - Boy Who Cried Wolf - 16/16

8/2
Blacc Zacc & Hoodrich Pablo Juan - Dirty Money Power Respect - 6/6

8/3
Gus Dapperton - Yellow and Such - 4/4
Loaf Muzik - Knowledge Born - 1/16
SALIO - Studio Sessions EP - 4/4

8/4
30 Glizzy - YC Wit Me - 1/8
Accept - The Rise of Chaos - 10/10
Andy Mineo & Wordsplayed -  Andy Mineo and Wordsplayed Present Magic & Bird - 13/13
A$AP Twelvyy - 12 - 14/14
Brett Eldredge - Brett Eldredge - 12/12
Briana Marela - Call It Love - 10/10
Ciaran McMeeken - Ciaran McMeeken - 12/12
Cursed Earth - Cycles of Grief, Vol. 1: Growth - 5/5
Damar Jackson - Unfaithful - 8/8
Dan Wilson - Re-Covered - 13/13
Dead Cross - Dead Cross - 10/10
Dylan Scott - Dylan Scott (Deluxe Edition) - 16/16
FAIRCHILD - So Long And Thank You - 8/11
Get The Shot - Infinite Punishment - 2/12
Girls' Generation - Holiday Night - 10/10
Imagine Dragons - Live At AllSaints Studios - 4/4
Jeff Beal - An Inconvenient Sequel: Truth To Power (Music From the Motion Picture) - 28/28
Justin Abisror - Swag Money - 12/12
Lakeshore - 41 - 8/8
Mondo Cozmo - Plastic Soul - 10/10
Neil Young - Hitchhiker - 10/10
Primary - www.shininryu.com - 8/8
Randy Newman - Dark Matter - 9/9
RiFF RAFF - The White West - 14/14
Sincerely, Me - Aesthetically Pleasing - 5/5
Slushii - Out of Light - 13/13
Tagtraeumer - Unendlich Eins - 15/15
Third Day - Revival - 15/15
Tyler Childers - Purgatory - 10/10
Ugly God  - The Booty Tape - 10/10
Various Artists - NOW That's What I Call Music! 63 - 22/22
Wage War - Deadweight - 12/12
William Bolton - Glow - 4/4
YoungBoy Never Broke Again - AI YoungBoy - 15/15

8/6
Trilla Kid - HELLION - EP - 2/3
YRN Lingo - Strawberry - 9/9

8/7
Half-A-Mil - EP3 - 2/7
Jake Hill - Meme King - 11/11

8/8
Carti Bankx - Young & Heartless - 11/11
Fickle Friends - Glue - EP - 4/4
Mannequin Mishap - Acatalepsy - 5/5
Sean Price - Imperius Rex - 16/16

8/9
DramaB2R - Born2Rule - 0/6
TLC - Spotify Singles - 2/2
With Confidence - Better Weather B-Sides - 2/2

8/10
Avicii - Avīci (01) - 6/6
Soldier Kidd - All Eyez On Me - 12/12
The Wise Man's Fear - The Lost City - 11/11

8/11
5 Billion In Diamonds - 5 Billion In Diamonds - 10/10
Aesop Rock & Homeboy Sandman - Triple Fat Lice - 5/5
Bebe Rexha - All Your Fault, Pt. 2 - 6/6
Billie Eilish - dont smile at me - 8/8
Brother Sundance - Honey EP - 3/5
Caloncho - Bálsamo - 13/13
Chip - League of My Own II - 17/17
David Rawlings - Poor David's Almanack - 10/10
Dizzy Wright - The Golden Age 2 - 18/18
dodie - You EP - 6/6
Downtown Boys - Cost Of Living - 8/12
Fashawn - Manna - 8/9
Frankie Rose - Cage Tropical - 6/10
Hundred Suns - The Prestaliis - 11/11
Jesus Culture - Love Has a Name (Deluxe) - 16/16
Justice for the Damned - Dragged Through the Dirt - 5/11
Kesha - Rainbow - 15/15
Like Ghosts - To Feel Like You're Drowning - 6/6
Lil Peep - Come Over When You're Sober (Part One) - 7/7
Lindsay Ell - The Project - 12/12
Marina City - Terminal - 5/5
Mark Bryan - Songs Of The Fortnight - 4/11
Milo - who told you to think?​?​!​!​?​!​?​!​?​! - 15/15
Mocky - How To Hit What And How Hard (The Moxtape Vol. IV) - 12/12
Moneybagg Yo - Federal 3X - 15/15
Motograter - Desolation - 11/11
MSW - Hell - 3/7
NEEDTOBREATHE - HARD CUTS: Songs from the H A R D L O V E Sessions EP - 6/6
Oneohtrix Point Never - Good Time Motion Picture Soundtrack - 13/13
Paul Kelly - Life Is Fine - 12/12
Phantoms - Screaming on the Inside - 7/7
RAT BOY - SCUM - 24/25
So Much Light - Oh, Yuck - 13/13
SonReal - One Long Dream - 13/13
The Cribs - 24-7 Rock Star Shit - 10/10
The Prosecution - The Unfollowing - 2/12
Varials - Pain Again - 11/11

8/15
Jamila Woods - HEAVN - 20/20

8/16
NanaBcool - 2amCruise EP - 6/6
TAEYANG - WHITE NIGHT - 7/7

8/17
Lil B - Black Ken - 27/27
Marqo 2 Fresh - Rockstar/Trapstar 2 - 14/14
NCT DREAM - We Young - The 1st Mini Album - 6/6
PeeWee Longway - The Blue M&M 3 - 9/15
Ramriddlz - Sweeter Dreams - 12/12

8/18
A$AP Ferg - Still Striving - 14/14
Alice Glass - Alice Glass - 6/6
Chase & Status - Tribe - 15/17
Cold Black - Circles - 5/8
Dave East - Paranoia: A True Story - 13/13
Eluveitie - Evocation II: Pantheon - 18/18
Everything Everything - A Fever Dream - 11/11
Felly - Wild Strawberries - 15/15
Ghostpoet - Dark Days + Canapés - 12/12
Grizzly Bear - Painted Ruins - 11/11
Josh Abbott Band - Until My Voice Goes Out - 13/14
King Gizzard & The Lizard Wizard With The Mild High Club - Sketches of Brunswick East - 13/13
Kings Kaleidoscope - The Beauty Between - 10/10
Kodak Black - Project Baby 2 - 19/19
Kriminals - Kriminals - 2/11
Leaf - Trinity - 13/13
Lil Debbie - OG In My System - 11/11
Mozzy - 1 Up Top Ahk - 15/16
Neck Deep - The Peace and the Panic - 13/13
No Malice - Let the Dead Bury the Dead - 10/10
No Plug - Culinary Arts 101 - 4/14
Outasight - Richie - 4/11
Phora - Yours Truly Forever - 16/16
Q Money - Win - 2/3
Rainer Maria - S/T - 9/9
Ray Wylie Hubbard - Tell The Devil I'm Gettin' There As Fast As I Can - 0/11
Reconcile - Streets Don't Love You - 16/16
Riley Green - Outlaws Like Us EP - 6/6
Rockabye Baby! - Lullaby Renditions of Justin Timberlake - 12/12
Ryan Follese - Ryan Follese - 12/12
Shelby Lynne & Allison Moorer - Not Dark Yet - 0/10
Stargazer - Tui La - 4/9
Steve Wilson - To The Bone - 11/11
The Accidentals - Odyssey - 13/13
The Homeless Gospel Choir - Normal - 11/11
Thy Art Is Murder - Dear Desolation - 10/10
UNKLE - The Road Part 1 - 11/15

8/19
Brand New - Science Fiction - 12/12
PRXJEK - AKUMA - EP - 2/7

8/20
ITSOKTOCRY - BEAUTIFUL BLOODSUCKERR [ SCENE 4 ] - 9/9

8/21
Adore Delano - Whatever - 11/11
Derez De'Shon - Pain - 18/18

8/23
Lil Yachty - Birthday Mix 2.0 - 7/7
Valleyheart - Nowadays - 6/6

8/24
Walter Etc. - Gloom Cruise - 10/10

8/25
Action Bronson - Blue Chips 7000 - 13/13
Angelo De Augustine - Swim Inside The Moon - 9/9
Apollo Brown & Planet Asia - Anchovies - 13/15
A$AP Mob - Cozy Tapes Vol. 2: Too Cozy - 17/17
Beatnick & K-Salaam x Junior Reid - Give Love EP - 0/11
Berner & Young Dolph - Tracking Numbers - 8/8
Brian McKnight - Genesis - 5/12
BROCKHAMPTON - SATURATION II - 16/16
Buddy - Magnolia - 5/5
Christian Nodal - Me Dejé Llevar - 13/13
Cymbals - Light In Your Mind - 6/11
Daniel Caesar - Freudian - 10/10
Der Weg Einer Freiheit - Finisterre - 5/5
DrugRixh Hect - Hector Vol. 1 - 11/11
Elijah Blake - Audiology - 12/14
EMA - Exile In the Outer Ring - 11/11
Eskimo Callboy - The Scene - 14/14
Fifth Harmony - Fifth Harmony - 10/10
Filthy Friends - Invitation - 1/12
For The Win - Heavy Thoughts - 10/10
Forever Starts Today - Always Hope - 3/11
Gogol Bordello - Seekers and Finders - 11/11
Grieves - Running Wild - 15/15
Hype Williams - Rainbow - 20/20
Ian Felice - In The Kingdom Of Dreams - 10/10
Iron & Wine - Beast Epic - 11/13
Ivan B - Forgive Me For My Honesty - 12/12
Jack Cooper - Sandgrown - 3/9
Joseph Shabason - Aytche - 9/9
Karen & the Sorrows - The Narrow Place - 11/11
Kitty - Miami Garden Club - 13/13
Liars - TFCF (Themes From Crying Fountain) - 11/11
Lil Uzi Vert - Luv Is Rage 2 - 16/16
MadeinTYO - True's World EP - 6/6
Nadine Shah - Holiday Destination - 10/10
New Kingston - A Kingston Story: Come from Far - 0/9
Oh Sees - Orc - 10/10
Old Dominion - Happy Ending - 12/12
Our Ceasing Voice - Free Like Tonight - 11/11
Ozuna - Odisea - 16/16
PVRIS - All We Know Of Heaven, All We Need Of Hell - 10/10
Queens of the Stone Age - Villains - 9/9
Ramin Djawadi - Game of Thrones: Season 7 (Music from the HBO Series) - 24/24
Ripp Flamez - Project Melodies 2 - 2/13
Savoy Brown - Witchy Feelin' - 0/11
Tasha Cobbs Leonard - Heart. Passion. Pursuit. (Deluxe) - 16/16
The Cadillac Three - Legacy - 11/11
The Fresh & Onlys - Wolf Lie Down - 1/8
The War On Drugs - A Deeper Understanding - 10/10
Turnover - Good Nature - 11/11
Widowspeak - Expect The Best - 3/9
Wiki - No Mountains in Manhatten - 16/16
XXXSENSATION - BEFRIEND - 7/7
XXXTENTACION - 17 - 11/11

8/27
Krimelife Ca$$ - BABYFACE - 0/5

8/29
Swoape - Swoape - Demo - 4/4
Trippie Redd - A Love Letter You'll Never Get... - 2/2
Xanman - Xan Laflare - 1/8
Yhung T.O. - Before The Fame - 7/7

8/30
BLACKPINK - BLACKPINK - 12/12
Dead Ties - Beauty Is Everywhere - 5/5
Stan Matthews - Truck Driving Man - 8/8

8/31
5K HD ‎- And To In A - 0/10
Daboii - Young Wild Nigga - 6/10
Steve Hong - The Prelude - 4/4