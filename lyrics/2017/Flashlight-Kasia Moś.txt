[Verse 1]
Like a bullet from a smoking gun
They try to tell us that we don't belong
Creeping 'round at the depth of night
Just two shadows in love, we were ghosts
Two hearts, we're invincible
When we're together we're untouchable
Haunted by if it's wrong or right
Shadows in love, we were ghosts

[Pre-Chorus]
Fire
Like a burning desire
Taking me higher
Walk in a wire
We will never come down

[Chorus]
It's like a flashlight burning in our eyes
You call the dogs off, I got them hypnotized
You never catch us, take us by suprise
Running faster at the speed of light
Falling deeper, got you in the sight
Bringing down your eyes in the sky

[Verse 2]
Like two animals on the run
Not afraid to fly into the sun
Invisible, we don't leave a trace
We're shadows in love, we were ghosts

[Pre-Chorus]
Fire
Like a burning desire
Taking me higher
Walk in a wire
We will never come down

[Chorus]
It's like a flashlight burning in our eyes
You call the dogs off, I got them hypnotized
You never catch us, take us by suprise
Running faster at the speed of light
Falling deeper, got you in a sight
Bringing down your eyes in the sky

[Instrumental]