INT. UNKNOWN SPACE

Close on THEODORE’S face (30s). We hold on him for a long time. He’s looking at something off camera, deep in thought. He starts quietly dictating a love letter into a small microphone.

THEODORE
To my Chris, I have been thinking
about how I could possibly tell you
how much you mean to me. I remember
when I first started to fall in
love with you like it was last
night. Lying naked beside you in
that tiny apartment, it suddenly
hit me that I was part of this
whole larger thing, just like our
parents, and our parents’ parents.
Before that I was just living my
life like I knew everything, and
suddenly this bright light hit me
and woke me up. That light was you.

Theodore, searching for the right words, quietly enjoys writing the letter. As he continues, he is moved by the memories he’s describing.

THEODORE (CONT’D)
I can’t believe it’s already been
50 years since you married me. And
still to this day, every day, you
make me feel like the girl I was
when you first turned on the lights
and woke me up and we started this
adventure together. Happy Anniversary,
my love and my friend til the end. Loretta. Print.

Cut out to reveal a computer monitor he’s sitting at. On the screen we see the letter he’s been dictating, transcribed into a handwritten letter on blue stationery.

As he says “Loretta,” we see “Loretta” being handwritten at the bottom of the letter. He proofreads his letter. Also on the screen are photos of a couple in their 80s. The couple is tagged “Chris” and “Me - Loretta.” Underneath is a bullet point email from Loretta: anniversary letter to husband Chris, married fifty years, love of my life, met right after college, have had the greatest life together.

Theodore pushes print and the letter comes out on a beautiful robin’s egg blue piece of stationery, with ball point pen handwritten older-female cursive. He looks at it, not happy.

He puts the printed letter on a stack of other printed letters to Chris and starts a new one. There is also a stack of finished letters in their envelopes - an assortment of beautiful stationery in all shapes and sizes.

THEODORE (CONT’D)
Chris, my best friend. How lucky am
I that I met you fifty years ago?
How lucky are we...

We track off of Theodore, down a line of cubicles, hearing bits of letters being written and seeing photos of who they’re being written to on the screens.

MIDDLE-AGED WOMAN LETTER WRITER
Dear Nana, Thank you so much for my
truck. I love the color and I play with it
every day. It’s the best truck I’ve ever seen.
Love, Tommy.

We see photos of Tommy and Nana on the screen, and five-year old hand writing. Moving off of her, we find another letter writer.

LETTER WRITER 2
What a beautiful wedding and what a
gorgeous bride. There wasn’t a dry
eye in the house, especially mine.
Your aunt and I are so proud of you.
I hope you and your lovely new
wife will come visit us in Florida.

LETTER WRITER 3
He served our country with honor
and dignity. I’m grateful I was able
to fight along side him. He will
live always in my heart.

We continue tracking, revealing dozens and dozens of cubicles full of letter writers. We hear someone answer the phone.

RECEPTIONIST (O.S.)
Beautifulhandwrittenletters.com,
please hold.

LETTER WRITER 2
Love, Uncle Doug.

INT. THEODORE’S OFFICE - EARLY EVENING

Theodore walks through the reception area. The office is almost empty except for him and the receptionist, PAUL. Theodore begins to scan each letter through a scanner on the front desk, then puts them in the outgoing mailbox.

Paul is sitting at a desk across the room, reading handwritten letters on a computer monitor.

PAUL
Theodore! Letter Writer 612.

THEODORE
Hey, Paul.

PAUL
Even more mesmerizing stuff today.
(re: letter on his screen)
Who knew you could rhyme so many
words with the name Penelope? Badass.

THEODORE
Thanks, Paul, but they’re just letters.
(beat)
Hey, that’s a nice shirt.

Paul is wearing a bright yellow button down shirt.

PAUL
(lighting up)
Oh, thank you. I just got it. It reminded
me of someone suave.

THEODORE
Well, now it reminds me of someone suave.
Have a good night, Paul.

PAUL
Buh-bye.

INT. THEODORE’S OFFICE ELEVATOR - CONTINUOUS

Theodore enters an oversized, corporate elevator. He puts a hands-free device in his ear. There are a few other people in the elevator with the same devices in their ears.

THEODORE
Play melancholy song.

Melancholy song starts. Long beat.

THEODORE (CONT’D)
Play different melancholy song.

Different melancholy song starts. Hold on everyone in the elevator, they’re all murmuring inaudibly into their own devices.

EXT. LOS ANGELES STREET - DUSK

Slightly in the future, the city's been developed even more with massive office, apartment and mall complexes. It's a city designed for comfort and ease. The LA basin is more crowded and dense, resembling Shanghai, with buildings as far as the eye can see. Construction cranes loom overhead. Close on Theodore walking through the commuter crowd.

THEODORE
Check emails.

An awkward text voice reads to him. It accents wrong syllables, making everything it says sound a little off.

TEXT VOICE
Email from Best Buy: Check out all your
favorite new --

THEODORE
Delete.

TEXT VOICE
Email from Amy: Hey Theodore, Lewman's
having a bunch of people over this weekend.
Let's all go together. I miss you. I mean, not
the sad, mopey you - the old, fun you. Let's
get him out. Gimme a shout back. Love, Amy.

THEODORE
Respond later.

TEXT VOICE
Email from Los Angeles Times weather. Your
seven day forecast is partly--

THEODORE
Delete.

TEXT VOICE
No new emails.

INT. SUBWAY - EVENING

Theodore sits in a crowded subway. Everyone on the train murmurs to themselves, occupied with their small devices. He plays a futuristic puzzle game on his handheld device as he listens to news headlines.

THEODORE
Next.

TEXT VOICE
China/India merger headed for regulatory
approval--

THEODORE
Next.

TEXT VOICE
World trade deals stalled as talks break
down betw--

THEODORE
Next.

TEXT VOICE
Sexy daytime star Kimberly Ashford reveals
provocative pregnancy photos.

He scrolls through titillating but tasteful pregnant woman photos.

INT. MALL - NIGHT

Theodore walks through a mall and enters an apartment lobby, nestled in between stores.

INT. THEODORE'S APARTMENT BUILDING HALLWAY - NIGHT

Theodore walks through the hallway.

INT. THEODORE'S APARTMENT - NIGHT

Theodore enters his apartment.