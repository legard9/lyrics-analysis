[Verse 1]
Rhymes electrify, get your fucking hand's high
On the beat-side is my Slantize
I been killing mic's since '96, and I
Can't die, steady I rise, a deadly design
Of the organic mixed with electronics, blended
Within ligaments fitted with robotic systems
Consisting of mechanisms, hand-tested
To sense rhythm ticks in a nano-second
I'm the man, I reckon, I got a plan to step in
And strangle the fucking neck of every dang executive
In the game who ever slang the gayest records
In an effort, just to make a little name to sell 'em
I'mma bang the hell out of any major veteran's
In my way, till I'm made a legend
So any gang who tell it, just may embellish it
Saying "Hey, this fella musta fell under the weight of an angry elephant"

[Hook]
Anybody talking that shit, better quit it
Keep on walkin or get, what I'm givin
When I kick this skill on the battlefield premises
Mission is: kill every Shadowville nemesis
Leave no rival of mine alive
If it bleeds or it breathes, then it needs to die
It's the fact yall mad at my raps, it's automatic
Attack and slaughter maggots, get back!

[Verse 2]
Lean on the beat, drillin the drums
Speed freakin machine-gun syllable tongue
I'm a demon who happens to speak in English
When I'm rappin, I'm casting a jinx, not singin'
I'm bringing the meanest, audible voo-doo
See, every thing I do's too much hotter than you'd do
Disc jock my record like it's pop, the groove's soon
Locked in your head like "dot-dot-dotta-da-doo"
Cuz when I spit it, it's sick as fuck
Notice the kids'll come
Over and pick it up
And you know I'll never give it up
So, listen up, you can zip it up, homey
Every joe on the street wanna show me
How they flow speech over a cloned beat
One step closer, be choking on your teeth

[Hook]x2

[Verse 3]
Quit searching this instant, turn your attention
To witness, a person with a perfect rhythm
Spinnin, surging, picture serpents twisting
Merging, kissin, squirming, witness
A surgeon in terms of precision
The work I deliver is surely efficient
You never heard of wordsmith on Earth, living
As proficient as this, deserving a position with
The industry, till they mentioned me, DZ
Coming from the sunny VA streets
He's so hypnotic, seems he's robotic
Breathes as often as sea-crawlin aquatic
Creatures, no lungs, he features gills on his
Parts like a shark has, hardened cartilage
He's a monster he's far from human, haunting
Gruesome thoughts, spew from this ruthless author

[Hook]x3

[Outro]:
I'm a soldier, told that I had to kill...
I was trained on Shadowville's Battlefields
I was chained and beaten, till the pain of being
Free was seeming greater than the pain of bleeding