[Verse 1: Ally & 
Dinah
]
Can't spend time in rewind, I'm sorry
Many times I remind myself
We've come way too far in our stories, oh
To build these walls and to blind ourselves
And for every action, there's a reaction

[Pre-Chorus: Dinah]
And I know the world can be cold, we can't let it divide us
There's something inside us, a power that grows
There's something beautiful in the flaws in all we are
Something calling all of us and it says

[Chorus: Normani & 
Dinah
]
We build bridges
Oh, we build bridges
No, we won't separate
We know love can conquer hate
So we build bridges
Bridges, not walls
Bridges, not walls
(Bridges, not walls)

[Verse 2: Lauren & 
Normani
]
I believe in the beauty of love (
Hey
)
I believe that we really are one
I believe every woman is a fighter
And I believe every man can stand beside her

[Pre-Chorus: Lauren + Ally]
And I know the world can be cold, we can't let it divide us
There's something inside us, a power that grows
There's something beautiful in the flaws in all we are
Something calling all of us and it says

[Chorus: Normani & 
Dinah
]
We build bridges
Oh, we build bridges
No, we won't separate
We know love can conquer hate
So we build bridges
Bridges, not walls
Bridges, not walls
(Bridges, not walls)

[Bridge: Dinah, 
Ally
 & 
Lauren
]
All I pray is we break our chains
Because love's worth fighting for
Shed your light oh, oh, oh, oh
All I pray is we break our chains
Because love's worth fighting for
Shed your light oh, oh, oh, oh
All I pray is we break our chains
Because love's worth fighting for
Shed your light oh, oh, oh, oh
All I pray is we break our chains (
All I pray is we break
)
Because love's worth fighting for
Shed your light

[Post-Bridge: Dinah & (Ally)]
So we build bridges (All I pray is we break our chains)
So we build bridges (Because love's worth fighting for, shed your light oh)
So we build bridges (All I pray is we break our chains)
So we build bridges (Because love's worth fighting for, shed your light oh)

[Chorus: Normani, 
Dinah
 & 
Ally
]
We build bridges (
We build bridges, we build bridges
)
Oh, we build bridges (
We build bridges, we build bridges
)
No, we won't separate
 (
We build bridges, we build bridges
)
We know love can conquer hate
 (
We build bridges
)
So we build bridges (
We build bridges
)
Bridges, not walls
 (
We build bridges, we build bridges
)
Bridges, not walls