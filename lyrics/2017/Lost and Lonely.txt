They were lost and lonely
Thinkin' bout those sweet days that
They spend together
She made his dream a reality
He turns her gray sky blue
Too much time passed
Over the ocean
But... they will be...
They will be always together

Haruka tooku hanarete
Dakedo itsuka nareteta

Kumo ga nagareteku
Sora ga hirogatte
Kakurete yuku
Fuan na ame

Kyouri ga nani wo toozake
Ai wo dare to kurabete

Imi wo sagashiteru
Ano hi okita koto
Dare ni mo aru
Sasayaka na yume

We'll be always together
Karada juu kakemeguru
Anata e no emotion
We'll be always together
Kawaranai omoi wo
Dakishimete iru

Moshimo ashita tooku ni
Tabi tatsu koto ga dekitara

Hareta aozora ni
Egao miseru koto
Ima nara mou
Furue mo nai

We'll be always together
Sora wo kake meguru
Futari no emotion
We'll be always together
Niji wo koe mirai wo
Dakishimetai yo

We'll be always together
Karada juu kakemeguru
Anata e no emotion
We'll be always together
Kawaranai omoi wo
Dakishimete iru

We'll be always together
Sora wo kake meguru
Futari no emotion
We'll be always together