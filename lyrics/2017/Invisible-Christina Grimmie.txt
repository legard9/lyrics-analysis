[Verse 1]
I'm solid, not apparition
Better check your vision, see what you're missing, boy
I, I don't need your permission
To go on existing with or without you, boy
I ain't gonna let it go, 'cause this been going on too long

[Chorus]
I won't be another ghost
No, I won't be invisible
See me everywhere you go
No, I won't be invisible

[Post-Chorus]
Invisible
Invisible

[Verse 2]
I had my suspicions, you kept me at a distance
I ain't disappearing, boy
I thought you were worth it
Pulling back the curtain, I see why I was hurting, boy
And I ain't gonna let it go
Now you see me with the lights on (See me with the lights on)

[Chorus]
I won't be another ghost
No, I won't be invisible
See me everywhere you go
No, I won't be invisible

[Post-Chorus]
Invisible
Invisible

[Bridge]
I won't be diminished, eclipsed, or hidden
You're gonna see my light blaze back to life like the phoenix rise
I won't be diminished, eclipsed, or hidden
You're gonna see my light blaze back to life like the phoenix rise

[Outro]
Hey yeah, invisible
Hey yeah, invisible
Hey yeah, invisible
Tonight