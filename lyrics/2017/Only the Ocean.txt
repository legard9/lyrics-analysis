[Intro: Sean Little]
Strategizing hashes additional marketing endeavors
Somehow asinine mediocracy excels
She held a mirror exhausted saying
How her man enticed
See Hollywood actresses malign
Eye speech helps art manifest expression
Sean, he's a magnificent emcee

[Verse 1: Sean Little]
Ayo, it started as a dream and I guess it was harmless
In the mirror he would sing in an effort to harness
Lyrics that were clever and would never ever tarnish
Son wanted to be a musician
Some said he was cool, skeptics said "who is you, kid?"
Manipulated ugly proof like a beautician
Nip and tuck it as he age
But he didn't have the juice, Nantucket ocean spray
"Hey, loser! Is rap your life?
Neglect your kids, get pissed at your wife"
For a lack of support, back and forth, yap and divorce
Backed in a corner, cut her some slack, rap performer
Cause even when you home, your head isn't there
Check your beeping phone, heading for the stair
Chasing a dream, you gonna be famous
At the cost of your family, now that is a shame

[Hook: Sean Little]
I'll tell you what the shame is, to be nameless
Oh brother I'm famous, no stutter I'm dangerous
Lone lover is language, so butter I'm stainless
Go cover the pain with close shutter your cameras

[Verse 2: Playdough]
It's a down-low dirty, shame I came early
Show off my pearly whites, I'm so purty, right?
Tight, being this great should be illegal in a 48 state
District court, I spit for sport, rap for change
Kick this game, I nick the vein and it's a shame
Now my pen's free bleeding
Band-aid peeling, can't stop the feeling
Can't keep it up with a scab on the pad
Gave what I got 'cause it's all that I had
Raw with the jab upper-cut plug wonder why
I sting like a beehive, flow like a butterfly
Like the cut of my jib, no fib
Stain like a barbecue rib, no bib
Your lid get flipped, I double dip
Folded it neat then ripped it and it's a shame

[Hook: Sean Little]
I'll tell you what the shame is, to be nameless
Oh brother I'm famous, no stutter I'm dangerous
Lone lover is language, so butter I'm stainless
Go cover the pain with close shutter your cameras

[Verse 3: Sean Little]
It's a shame that he claimed to know this
Love so ferocious, it searches, exposes
Hurts and it holds him, learns and it grows him
Discernment is wholesome couldn't have earned, he was chosen
And the choice is poisonous to the boisterous
They boys who strut, they just work work work to deserve their cut
Yearn for righteousness, never learn to trust
In the works of a greater greater
He identifies their lies, pride, and that they're major haters
So he hates back 'cause he hates that
And when he hates, that is where the hate's at
In the receivers of it and the deceiver loves it
When a believer believes some rubbish
But the fruit proves it is only a claim
He is fooled, he is doing the same
Like the vocal sample saying it is truly a shame

[Hook: Sean Little]
I'll tell you what the shame is, to be nameless
Oh brother I'm famous, no stutter I'm dangerous
Lone lover is language, so butter I'm stainless
Go cover the pain with close shutter your cameras