[Verse 1]
You think you're ready
But you've got too much on your mind
Too much left to prove
No easy answers
You've got to work it out yourself
You won't hear anyone but you

[Chorus]
The moment you stop looking
Wherever you go, you'll be in the right place
You'll never know the difference it makes
When you let go, and give up your chance
I'll come find you one of these days

[Verse 2]
We've got potential
But it's the future you and me
When the coast is cleared
You see the sunrise
I see your soul shine through to your eyes
When you're here

[Chorus]
The moment you stop looking
Wherever you go, you'll be in the right place
You'll never know the difference it makes
When you let go, and give up your chance
I'll come find you one of these days

[Instrumental]

[Chorus]
The moment you stop looking
Yeah, the moment you stop looking
Wherever you go, you'll be in the right place
You'll never know the difference it makes
When you let go, and give up your chance
I'll come find you one of these days
I'll come find you one of these days
I'll come find you one of these days