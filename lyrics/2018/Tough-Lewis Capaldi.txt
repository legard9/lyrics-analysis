[Verse 1]
Every little part of me is holding on to every little piece of you
Is holding on to every drop of blood you drew
Is holding onto you
And every waking hour I spend
Holding out for reasons not to go to bed
I'm holding out for someone else to hold instead
If every hope of you is dead

[Pre-Chorus]
'Cause every night since you cut and run
I feel like the only one who's ever been the lonely one
Trying to mend a heart that keeps breaking
With every step that you're taking

[Chorus]
'Cause you've been running circles 'round
My mind, turning me inside out
And I fell for you but hit the ground
'Cause the only love I've known has let me
Down and I need lifting up
Now you ain't here I'm sleeping rough
And I'm praying I can pray enough
So waking up without you ain’t so tough

[Verse 2]
Find it hard to find my feet
When I keep on stumbling over you and me
But I keep on trying 'cause I know I need
To outrun the memories

[Pre-Chorus]
And every day I'm reminded of
The way I let it come undone
I feel like the lonely one, the only one with time worth wasting
Well, I still can't stop chasing

[Chorus]
'Cause you've been running circles 'round
My mind, turning me inside out
And I fell for you but hit the ground
'Cause the only love I've known has let me
Down and I need lifting up
Now you ain't here I'm sleeping rough
And I'm praying I can pray enough
So waking up without you ain’t so tough, oh
Tough, oh, tough
So waking up without you

[Bridge]
Say I
Thought I was talking to you
Say I
I guess I'm talking to myself
Me and nobody else

[Chorus]
'Cause you've been running circles 'round
My mind, turning me inside out
And I fell for you but hit the ground
'Cause the only love I've known has let me
Down and I need lifting up
Now you ain't here I'm sleeping rough
And I'm praying I can pray enough
So waking up without you ain’t so tough