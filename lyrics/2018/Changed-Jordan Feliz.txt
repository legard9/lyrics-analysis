[Verse 1]
Let me tell you, all my friends
About this joy I'm living in

[Pre-Chorus]
Let me take the mic, go on and testify
How I was dead and then I came to life
No more living in the dark of night
Now everything's alright

[Chorus]
I've been changed, I've been saved
Brand new day
I've been changed, I've been changed
Tell me why would I turn back now?
There's no end to the love I've found
Future's bright and there ain't no doubt
I've been changed, I've been changed

[Verse 2]
All my heartbreak fades away
Like a book when you turn the page

[Pre-Chorus]
Let me take the mic, go on and testify
How I was dead and then I came to life
No more living in the dark of night
Now everything's alright

[Chorus]
I've been changed, I've been saved
Brand new day
I've been changed, I've been changed
Tell me why would I turn back now?
There's no end to the love I've found
Future's bright and there ain't no doubt
I've been changed, I've been changed

[Interlude]
Woo-oh, woo-oh
Woo-oh, I've been changed
Woo-oh, woo-oh
Woo-oh

[Bridge]
I put my hands in the air
'Cause I know You're there (Your love, it's something magical)
I put my hands in the air
'Cause You heard my prayer (A transformation radical)
I put my hands in the air
'Cause I know You're there (I know it's supernatural)
I put my hands in the air
'Cause You heard my prayer (wooh wooh wooh)

[Chorus]
I've been changed, I've been saved (I can feel it now)
Brand new day (I believe it)
I've been changed, I've been changed (Oh, I believe)
Tell me why would I turn back now? (I can feel it now)
There's no end to the love I've found (wooh wooh wooh)
Future's bright and there ain't no doubt
I've been changed, I've been changed

[Outro]
Let me tell you now
Woo-oh, woo-oh
Woo-oh, I've been changed
Woo-oh, woo-oh
Woo-oh