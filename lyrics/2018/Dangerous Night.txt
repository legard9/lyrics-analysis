[Verse 1: Baekhyun]
Aiming at each other toward the point of freezing
I hear the sharpness in your voice
I'm only filled with thoughts that leave me breathless
Oh oh oh oh oh
Aye ye

[Refrain: Chen]
With your eyes covered
Each of our hearts firmly shut
We turn our backs towards one another

[Pre-Chorus: Kai, Xiumin]
Ah, I'm burning
I can't breathe, it's like I'll split in half
I'm thirsting
And with this one glass
It's like I will overflow
On this dangerous night

[Chorus: Chen, All, Kai]
It’s the love shot
Na nanana nananana
Na nanana nanana
Na nanana nananana
Oh oh oh oh oh
It’s the love shot
Na nanana nananana
Na nanana nanana
Na nanana nananana
Oh oh oh oh oh
It’s the love shot

[Verse 2: Suho]
It's getting twisted with love and hate
Our beautiful memories
Are dyed in white
Fades bit by bit

[Refrain: D.O]
They get deeper everyday, calm down
Wounds of words
And my heart burnt black, where is love?
Yeah yeah yeah yeah

[Pre-Chorus: Chanyeol, Sehun, Both]
Even if I cover my eyes and ears
And force myself to wander, in the end the answer is love
My empty stomach is filled with ego
An empty glass of compassion we left behind
Now I fill it again, let me hear it everyone

[Chorus: Suho, All, Xiumin]
It’s the love shot
Na nanana nananana
Na nanana nanana
Na nanana nananana
Oh oh oh oh oh
It’s the love shot
Na nanana nananana
Na nanana nanana
Na nanana nananana
Oh oh oh oh oh
It’s the love shot

[Bridge 1: D.O, Suho]
People come and people go
But you and I are still frozen in this world
Gradually adjusting to these dulled emotions

[Bridge 2: Baekhyun/Sehun, Chanyeol/Sehun]
My heart is burning up
I'm parched
With trust  I filling this void with you
You light a fire in my lifeless heart
Yeah

[Chorus: Chen, All, Kai]
It’s the love shot
Na nanana nananana
Na nanana nanana
Na nanana nananana
Oh oh oh oh oh
It’s the love shot
Na nanana nananana
Na nanana nanana
Na nanana nananana
Oh oh oh oh oh
It’s the love shot