[Intro]
Oh, oh, oh
Oh, oh, oh, oh, oh, oh
Oh, oh, oh, oh, oh, oh
Oh, oh, oh

[Verse 1]
Where do I go from here?
How do I disappear?
Here beyond the looking glass
Somewhere off the beaten path
Heartache never seems to pass, just lasts
And I'd go

[Chorus]
Yet I know that tomorrow comes, so I'll be here when you rise
Stay real close and the moment will subside
Look the other way as I bottle myself up inside
I won't let the teardrops spill tonight
Just conceal myself and hide
A portrait of my life
La da da da da
La da da da da

[Verse 2]
Somewhat desensitized
Still the same hopeful child
Haunted by those severed ties
Pushing past the parasites
Down but not demoralized
Unconfined, but don't let go

[Chorus]
Yes I know that tomorrow comes
So I'll be here when you rise (Rise)
Stay here close and the moment will subside
Look the other way as I bottle myself up inside
I won't let the teardrops spill tonight
Just conceal myself and hide
A portrait of my life

[Bridge]
And for the finale, she can float around effortlessly
And dream away the hours in her mind
And I let go

[Chorus]
Yet I know that tomorrow comes, so I'll be here when you rise
Stay here close and the moment will subside
Look the other way as I bottle myself up inside
I won't let the teardrops spill tonight
Just conceal myself and hide
This portrait of my life