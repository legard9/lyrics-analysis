English Translation

[Rap Monster]
Ah.. I’m 20 now
Now that I’m actually 20.. It’s nothing special
So what am I supposed to do now?

[Chorus: Jin]
I’m 20 (now) and it’s a good day
I’ve become a free body
But what is this, there’s nothing to it I’ve only become an adult
I’ve only become an adult, I’ve only become an adult
I’ve only become an average adult, The average adult is what I’ve become
An adult

[Verse 1: Rap Monster]
Some have gone to whichever college, some have gone off to military service
Some are repeating a grade and some are looking for work
The tender touch of our families, that sweet romance has come at its end
Haste and clumsiness are all what’s left
The weight of being 20, I thought would be like a feather
Now it’s a boulder that pins me down, the regret at age 20
Oh no! All the Peter Pans who’ve freshly jumped out of fairytale books
It must’ve never existed from the start, this Neverland they dream about
A little sense of liberation, a little sense of regret (for an unfulfilled wish or goal)
A little sense of hostility towards this world that’s strange to me
Because I’m now an adult I want to keep a straight face and play it cool
Youth and naivete, they’re both in vain, really
Eyah..Would you look at my lyrics, I’ve matured
A friend who’s been hired is lifting files (T/N: ‘seoryucheol’) instead of being in season (T/N: 'cheol’, literal translation of colloquial phrase for 'growing up’)
I was once a young adult but now I’m an adult child
Mom’s not around to take care of me, I’m skipping meals

[Chorus: Jin]
I’m 20 (now) and it’s a good day
I’ve become a free body
But what is this, there’s nothing to it I’ve only become an adult
I’ve only become an adult, I’ve only become an adult
I’ve only become an average adult, The average adult is what I’ve become
An adult

[Verse 2: Suga]
What every men in their 20s experience is a deep sense of remorse
Fearing military draft notes, the things that once felt so far away
Become reality. They said “[North and South Korea] would be unified by the time you’ve grown up”
That’s what mom said and I had completely believed her
But the reality is that [I’m] an able-bodied first-rate soldier material
This friend who’s perfectly healthy is a fourth-rate so why am I first-rate
How could I be so uselessly healthy
Civil duties? it’s a juvenile death sentence
I envy and envy the nation’s lowest 10%
It blows away this flowery adolescence, I’m evading military service
The roaring 20s, time when everything is luscious
They try to set fire to flower-blossomed youth
The moment I graduated after the hellish exams
I attend another school, its name is Society
Applause for courage to malesin their 20s who were forced to break from Society
And a handshake of gratitude to the nation’s 60,000 officers and men of the armed forces

[Outro]
Turned 20 and started attending a school, its name is Life
But there were no teachers there
Some will repeat a grade and some will be drafted
But do not forget this time of blissful ignorance
Adulthood, it’s nothing, really
Adulthood, it’s nothing, really
It’s nothing like how it seems, really
Really, it’s nothing like how it seems
Hangul

[Rap Monster]
아.. 이제 스무살인데
막상 돼 보니까.. 별것도 없고
저 이제 어떻게 해야 돼요?

[Chorus: 진]
스무살이고 좋은 날이고
자유의 몸이 된 거야
근데 이게 뭐니 아무것도 아니잖아 어른이 된 거야
그냥 어른이 된 거야, 그냥 어른이 된 너야
평범한 어른이 된 거야, 평범한 어른이 된 나야
어른이

[Verse 1: Rap Monster]
누군 무슨 대를 가고, 누군 군대를 가고
누군 재수를 하고 누군 일자리를 찾네
가족들의 어루만짐, 그 달콤한 로맨스는 끝
남은 건 그저 서두름과 서투름뿐
깃털같을 줄로만 알았던 스무살의 무게
이젠 바위가 되어 짓눌러, 스무살의 후회
Oh no! 이제 막 동화책을 뛰쳐나온 피터팬들
처음부터 없었나봐 네버랜드는
약간의 해방감, 약간의 안타까움
낯선 세상에 드는 약간의 반감
이미 성인이니 난 시치미떼고 싶지
젊음이니 청춘이니 다 부질이 없어 really
이야.. 내 가사 봐봐 철들었어
취직한 친구는 철 대신 서류철 들었어
난 애어른이었지만 이제는 어른아이
챙겨주는 엄마도 없어, 끼니를 거른다

[Chorus: 진]
스무살이고 좋은 날이고
자유의 몸이 된 거야
근데 이게 뭐니 아무것도 아니잖아 어른이 된 거야
그냥 어른이 된 거야, 그냥 어른이 된 너야
평범한 어른이 된 거야, 평범한 어른이 된 나야
어른이

[Verse 2: Suga]
20대 남성은 필히 받게 되는 것 중 심히
두려운 군입대 영장, 멀게 느꼈었던 일이
현실이 돼 어릴 적에 크면 통일될 거래
라던 엄마 말만 철썩같이 믿고 믿었네
But 현실은 건실한 1급에 현역 대상자
건강이 멀쩡한 친구도 4급인데요 난 왜
쓸데없이 건강하기만 한 거야
국방의 의무, 그건 청춘 사망선고야
국가가 인정한 하위 10프로를 시기해 시기
꽃다울 시기 날리니 군대를 기피해 기피
20대, 뭘 해도 화려할 때
꽃이 핀 청춘을 불태우려 하네
지겨운 입시를 마치고 졸업한 순간
사회란 학교에 입학 후 강제 휴학
당하게 된 20대 남성에게 용기의 박수를
60만 국군 장병에겐 감사의 악수를

[Outro]
스무살이 되고 세상이라는 학교
에 입학했지만 거긴 선생님도 없더라고
누군 재수에 누군 군댈 가겠지만
마냥 순수했던 이 때를 잊지 마
어른, 그거 별 거 아니니까
어른, 그거 별 거 아니니까
그거 별 거 아니니까
진짜 별 거 아니니까
Romanized

[Rap Monster]
A.. ije seumusarinde
Maksang dwae bonikka.. byeolgeosdo eopsgo
Jeo ije eotteohge haeya dwaeyo?

[Chorus: jin]
Seumusarigo joheun narigo
Jayuui momi doen geoya
Geunde ige mwoni amugeosdo anijanha eoreuni doen geoya
Geunyang eoreuni doen geoya, geunyang eoreuni doen neoya
Pyeongbeomhan eoreuni doen geoya, pyeongbeomhan eoreuni doen naya
Eoreuni

[Verse 1: Rap Monster]
Nugun museun daereul gago, nugun gundaereul gago
Nugun jaesureul hago nugun iljarireul chajne
Gajokdeurui eorumanjim, geu dalkomhan romaenseuneun kkeut
Nameun geon geujeo seodureumgwa seotureumppun
Gisteolgateul julloman arassdeon seumusarui muge
Ijen bawiga doeeo jisnulleo, seumusarui huhoe
Oh no! ije mak donghwachaegeul ttwichyeonaon piteopaendeul
Cheoeumbuteo eopseossnabwa nebeoraendeuneun
Yakganui haebanggam, yakganui antakkaum
Naccseon sesange deuneun yakganui bangam
Imi seonginini nan sichimittego sipji
Jeolmeumini cheongchunini da bujiri eopseo really
Iya.. nae gasa bwabwa cheoldeureosseo
Chwijikhan chinguneun cheol daesin seoryucheol deureosseo
Nan aeeoreunieossjiman ijeneun eoreunai
Chaenggyeojuneun eommado eopseo, kkinireul georeunda

[Chorus: jin]
Seumusarigo joheun narigo
Jayuui momi doen geoya
Geunde ige mwoni amugeosdo anijanha eoreuni doen geoya
Geunyang eoreuni doen geoya, geunyang eoreuni doen neoya
Pyeongbeomhan eoreuni doen geoya, pyeongbeomhan eoreuni doen naya
Eoreuni

[Verse 2: Suga]
20dae namseongeun pilhi batge doeneun geot jung simhi
Duryeoun gunipdae yeongjang, meolge neukkyeosseossdeon iri
Hyeonsiri dwae eoril jeoge keumyeon tongildoel georae
Radeon eomma malman cheolsseokgati mitgo mideossne
But hyeonsireun geonsilhan 1geube hyeonyeok daesangja
Geongangi meoljjeonghan chingudo 4geubindeyo nan wae
Sseuldeeopsi geonganghagiman han geoya
Gukbangui uimu, geugeon cheongchun samangseongoya
Gukgaga injeonghan hawi 10peuroreul sigihae sigi
Kkoccdaul sigi nallini gundaereul gipihae gipi
20dae, mwol haedo hwaryeohal ttae
Kkocci pin cheongchuneul bultaeuryeo hane
Jigyeoun ipsireul machigo joreophan sungan
Sahoeran hakgyoe iphak hu gangje hyuhak
Danghage doen 20dae namseongege yonggiui baksureul
60man gukgun jangbyeongegen gamsaui aksureul

[Outro]
Seumusari doego sesangiraneun hakgyo
E iphakhaessjiman geogin seonsaengnimdo eopsdeorago
Nugun jaesue nugun gundael gagessjiman
Manyang sunsuhaessdeon i ttaereul ijji ma
Eoreun, geugeo byeol geo aninikka
Eoreun, geugeo byeol geo aninikka
Geugeo byeol geo aninikka
Jinjja byeol geo aninikka