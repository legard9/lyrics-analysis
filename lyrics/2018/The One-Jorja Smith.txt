[Verse 1]
Never had to work for love
Don't need you to show me how
Don't want to be falling in
When I'm falling out
Didn't think I'd give for love
Every time I hold it back
Now there’s lust in my head
I'm tryna find who I am

[Pre-Chorus]
There's choosers, there's takers
There's beggin' heartbreakers
I don't wanna be that way
You will never hear me say
"Come hold me, console me"
When, really, I'm lonely
Even if I feel this way
I don't wanna feel this way
When I—

[Chorus]
Meet  someone
I don't want to need no one
I'm not tryna let you in
Even if I've found the one

[Verse 2]
Never had to wait for love
Always thought it'd come around
You come for me
But I'm nowhere to be found
Cancellations for conversations
I don't need right now
I'm afraid of these relations
I can't be tied down

[Pre-Chorus]
There's choosers, there's takers
There's beggin' heartbreakers
I don't wanna be that way
I don't wanna feel this way
When I—

[Chorus]
Meet someone
I don't wanna need no one
I'm not tryna let you in
Even if I've found the one

[Pre-Chorus]
I don't wanna feel this way
When I—

[Chorus]
Meet someone
I don't wanna need no one
I'm not tryna let you in
Even if I've found the one