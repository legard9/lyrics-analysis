[Verse 1]
A part of me (a part of me)
Is feeling weak (is feeling weak)
I took a chance but now it feels so hard to breathe (hard to breathe)
A leap of faith (a leap of faith)
Now haunting me (now haunting me)
What happen to the words it's always you and me

[Pre-Chorus]
I wanna feel alive
When I'm running through my life
So help me to ignite
This spark I feel inside
So help me now

[Chorus]
Light me up, light me up my babe
Light me up, tell me where to go
Light me up, light me up my babe
Light me up and tell me where to go-ohoh
Oooh-oh-oh, oooh-oh
Tell me where to go-ohoh
Oooh-oh-oh, oooh

[Pre-Drop]
And tell me where to go

[Drop]
Tell me where to go
Tell me where to go-ohoh

[Verse 2]
I am fading now
Of real life
Misguided by the way they say I have to live
So bring me back
Back to the place
Where my heart can finally reveal its face

[Pre-Chorus]
I wanna feel alive
When I'm running through my life
So help me to ignite
This spark I feel inside
So help me now

[Chorus]
Light me up, light me up my babe
Light me up, tell me where to go
Light me up, light me up my babe
Light me up and tell me where to go-ohoh
Oooh-oh-oh, oooh-oh
Tell me where to go
Oooh-oh-oh, oooh

[Pre-Drop]
And tell me where to go-oh

[Drop]
Tell me where to go
Tell me where to go-ohoh

[Outro]
Light me up, light me up my babe
Light me up, tell me where to go
Light me up, light me up my babe
Light me up and tell me where to go-ohoh

[Ending]
Oooh-oh-oh