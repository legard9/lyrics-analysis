Turn to the end of this and you will discover, to your surprise, amusement and perhaps even mild dismay, that William Eggleston has chosen to answer a series of questions put to him by curators, photographers, critics and fans in as maddeningly deadpan and laconic a way as could ever be imagined. It is as if he has stuffed up his ears because he simply cannot tolerate the clamour. Should we blame him for this impatience with post-facto chit-chat?

Of course not. That level of dismissiveness is entirely consistent with everything we think that we know about this great photographer from the American South. J D Salinger would have done the same. This man is not in the business of talking through his work. He has been, lifelong, in the business of making it, in all its gloriously tense inscrutability. Why spoil it by reductive explanation? People often talk such nonsense anyway. You can't follow up photography with words. It doesn't make any sense. Those words in italics were from the man himself, written a while ago.

Eggleston was born in Memphis, Tennessee, in 1939, and he grew up on the very fringe of the Mississippi Delta, where he continues to work to this day. His family had been cotton farmers, though his father was an engineer and his mother the daughter of an important local judge. He bought his first camera, a Canon Rangefinder, in 1957, and after a brief period of work in monochrome, he switched to colour in 1965.

His way of making proved controversial from the start, and what we forget is that Eggleston has had to put up with a lot of pompous and ill-informed ignorance during his long life. If his work had not been so important, that level of criticism might not have mattered so much, but the fact is that his work has been an education for all of us. The way we do photography now would not have been the same without Eggleston. Martin Parr, Nan Goldin and Jeff Wall would not have been granted the permission to be themselves without Eggleston's example. It was he and not, say, Cartier-Bresson, who was the true revolutionary, which means that he has caused a lot of trouble in his time merely by being, quite unflinchingly, who he has been.

He has had many detractors, and many of those critics spoke up when his work was shown at MoMa, New York, in 1976, in a retrospective that helped to define the nature of photography in our time. Forty years ago, his photographs were dismissed as banal, inconsequential and ramshackle in the extreme. The New York Times called it "the most hated show of the year", and Hilton Kramer, loftily countering the curator's assertion that the show was in fact perfect, wrote "perfectly bad, perhaps… perfectly boring, certainly".

What didn't they get that we, having absorbed Eggleston's influence, can now see with such clarity?

They wanted a subject, a message, a neatly-framed box into which content was poured. Eggleston didn't deal in such easy certitudes. He found his early subject-matter in the American South, his homeland, but the American South that he saw and felt on his pulses could not have been more different from the American South of Walker Evans or Bruce Davidson. There is no political perspective in Eggleston's work. This is not a photography of protest or social engagement. He does not seek out a story or a subject-matter. The subjects – a rusting street light, a heap of planks ranged against a wall, a ceiling fixture – are barely subjects at all. They are most often nothing but lone objects, often seen at an uncustomary angle to the vertical or the horizontal, so that we begin to feel vertiginous as we stare and stare at them.

And then there is his use of colour. Photography didn't use colour seriously until Eggleston came along. Colour was the prerogative of the slick advertising man, that dealer in cliché and banality. Eggleston saw a use for heightened colour; in fact, his colours can be shrill to the point of near hysteria. So he shows us objects that are both ordinary and very particularised, and then ratchets up the tension that surrounds those objects by infecting their atmosphere with shrill colours. He is besotted by the imaginative possibilities of the ordinary. He wants us to rinse our eyes until we see, without prejudice, the exquisite poignancy of the seeming banalities of the everyday.

Some of his early work reminds us of the greatness of Raymond Carver, who had a way of describing how the look of a refrigerator seems to a drunken man, that glacial, detached control of the stupefied gaze. So we cannot expect storytelling from Eggleston, but we do find a high degree of calculated painterliness, a form of abstraction, if you like – in fact, he has painted and drawn all his life.

Most of all, you must resist seeing through the photograph to the bald image of a recognisable object too quickly, too readily. Instead, begin by looking at the form and the tight framing of the piece, the angle of view, the playing off of colour against shadow – that sort of thing. Otherwise, you will exhaust the imaginative possibilities of Eggleston's work before you even begin.

'Dear Bill': photographers, curators and fans ask questions of William Eggleston...

Simon Baker, curator of photography, Tate Modern: What was the first photograph that was important to you (by you or anyone else), and why?

A picture I took of some prisoners at the state penitentiary. I'm guessing I was about 20 at the time.

Brett Rogers, director, The Photographers' Gallery: When we recently showed an Eggleston image at the Gallery, we wrote on the accompanying caption that you photograph scenes of everyday life with a 'snapshot style'. When Nan Goldin visited in January, she took exception to this, saying yours was definitely not a 'snapshot' approach. What is your view on this description of your approach?

Thank you, Nan.

Nan Goldin, photographer, New York: Remember our times in Paris? Are you still gonna marry me?

Yes, no question about it.

Michael Glover, art critic, The Independent: You seem to have both loved and loathed the American landscape. How much pain has the holding of such contradictory impulses caused you?

I don't remember loathing any of it.

Chris Dercon, director, Tate Modern: As we are about to show some of your beautiful dye-transfer prints at Tate Modern, I have been wondering how you decide on the size of the prints you make.

I have currently settled on two sizes: smaller dye transfers and large-format pigment prints.

Alice Jones, deputy arts editor, The Independent: What do you think of Instagram?

I don't know what they are.

Martin Parr, photographer, Bristol: What is the difference between your current shooting and that of the 1970s?

The subject-matter is different.

Jason Evans, photographer, Brighton: What's the difference between a photographer who makes art, and an artist who makes photographs?

Not sure there is any difference.

Nina Berman, photographer, New York: Is there a place you've never been that you would like to photograph?

I can't think offhand of any particular place.

Penny Martin, curator and editor-in-chief, The Gentlewoman: What building would you like to blow up?

I'm not in that business.

Alec Soth, photographer, Minneapolis: A few years ago Robert Frank said, "There are too many images, too many cameras now. We're all being watched. It gets sillier and sillier. As if all action is meaningful. Nothing is really all that special. It's just life. If all moments are recorded, then nothing is beautiful and maybe photography isn't an art any more. Maybe it never was." What do you think about this?

I don't disagree with any part of that statement.

Alice Hawkins, photographer, Essex: I know you were interested in Elvis, but have you met your fellow Tennessean Dolly Parton? Would you like to take her picture?

No comment.

Bobby Gillespie, singer, Primal Scream, London: Did you really give the 12-year-old Alex Chilton [the late singer with Big Star] LSD/acid at a party in Memphis in the 1960s?

No.

Nick Hall, picture editor, The Independent Magazine: What's your favourite colour?

It used be green when I was young. Now I don't have a favourite.

Michael Benson, curator, Candlestar, London: Novelist Donna Tartt claims to recognise "a sparkle of menace" in your most powerful photographs. Do you agree?

No.

Polly Borland, photographer, London: What are your feelings about death?

I haven't been there yet.

Adam Broomberg and Oliver Chanarin, photographers, London: You are on a train from Memphis to Manhattan. It's a 1,102-mile journey and the train is travelling at 80mph. What is the train-driver's name?

I call him "someone I think I trust".

Philip Hensher, novelist and art critic: What should a photographer do with symmetry?

I have no idea.

Lewis Blackwell, creative director, Getty Images, London: Did Garry Winogrand really say to you, "Bill, you can take a good picture of anything"?

Yes.

Peter Dench, photographer, London: Do you fancy a pint; my round?

Why not, of course it depends on what it's a pint of…