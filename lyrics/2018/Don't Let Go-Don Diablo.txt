[Verse 1: Don Diablo]
One touch, one kiss won't be enough
I'm on empty, fill me up
Closer, baby I want you
I'm hooked, I'm struck, I'm at your door
Desperate for a little more
Closer, baby I need you

[Chorus: Don Diablo]
If we could only slow the time
We'd have forever, every night
Oh, don't let go
If we could only run away
We'd have forever, every day
Oh, don't let go
Don't let go

[Post-Chorus]
Don't let go
Don't let go
Don't let go
Don't let go
Don't let go
Don't let go
Don't let go
Don't let go

[Verse 2: Holly Winter]
Fearless with you by my side
Stronger with your hand in mine
Better, just to be near you
Nothing standing in my way
Cross the distance, here to stay
Closer, I'll never leave you

[Chorus: Holly Winter]
If we could only slow the time
We'd have forever, every night
Oh, don't let go
If we could only run away
We'd have forever, every day
Oh, don't let go
Don't let go

[Post-Chorus]
Don't let go
Don't let go
Don't let go
Don't let go
Don't let go
Don't let go
Don't let go
Don't let go

[Chorus: Don Diablo]
If we could only slow the time
We'd have forever, every night
Oh, don't let go

[Post-Chorus]
Don't let go
Don't let go
Don't let go
Don't let go
Don't let go
Don't let go
Don't let go
Don't let go

[Chorus: Holly Winter]
If we could only slow the time
We'd have forever, every night
Oh, don't let go
If we could only run away
We'd have forever, every day
Oh, don't let go