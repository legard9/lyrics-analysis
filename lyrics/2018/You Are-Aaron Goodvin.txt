[Verse 1]
I'm a real black-and-whiter
I'm a half-glass-empty negative-sider
I'm a jerk when I'm mad
I'm good at running my mouth
But I don't like to brag about that
Ha, but about that, I ain't worth it
Lord knows I ain't perfect

[Chorus]
But you are the best decision that I ever made
You are everything to everything I ain't
You're one is seven billion
Girl I've been near and far
Ain't nobody like who you are
And I love who you are
'Cause you know who you are
Can't believe that you're mine, all mine
But you are

[Verse 2]
Sometimes I wonder why you put up with me
I'm in over my head, girl, I'm out of my league
I'm a hell of a man, I'm hell on you girl
If I could I'd tell the whole world

[Chorus]
You are the best decision that I ever made
You are everything to everything I ain't
You're one is seven billion
Girl I've been near and far
Ain't nobody like who you are
And I love who you are
'Cause you know who you are
Can't believe that you're mine, all mine
But you are

[Chorus]
You are the best decision that I ever made
You are everything to everything I ain't
You're one is seven billion
Girl I've been near and far
Ain't nobody like who you are
And I love who you are
'Cause you know who you are
Can't believe that you're mine, all mine
But you are

[Outro]
Can't believe, can't believe that you're mine
But you are
Yeah you are
Yeah you are
Yeah you are
Can't believe that you're mine all mine
But you are