Whoa, Mama Nantucket, she tried to kick the habit
But she couldn't and she got stung
I tried to relieve her
But she wasn't a believer
Until she found another one

Da-do-da-li-da-lo-da-la-hee I love it here on the range
Oh-my-ty-yi-yippee, I would love it more if it changed
Da-do-da-li-da-lo-da-la-hee I love it here on the range
Oh-my-ty-yi-yippee, I would love it more if it changed

Oh, Bobby with a dollar, he was sure he found the answer
But it might take a little time
Playing follow the leader
He was sure would be a winner
But I had other things on my mind

Da-do-da-li-da-lo-da-la-hee I love it here on the range
Oh-my-ty-yi-yippee, I would love it more if it changed
Da-do-da-li-da-lo-da-la-hee I love it here on the range
Oh-my-ty-yi-yippee, I would love it more if it changed

Oh, I see all the people running
Coming from miles around
Everybody's singing a different tune
See them all fall down

Well I don't like to quarrel but what about tomorrow
Will it be the same as the past
I keep on hoping that something will happen
And I hope that it happens fast

Da-do-da-li-da-lo-da-la-hee I love it here on the range
Oh-my-ty-yi-yippee, I would love it more if it changed
Da-do-da-li-da-lo-da-la-hee I love it here on the range
Oh-my-ty-yi-yippee, I would love it more if it changed

Whoa, li-da-lo-da-la-hee, I love it here on the range
A' oh-my-ty-yi-yippee, I would love it more if it changed
Da-do-da-li-da-lo-da-la-hee I love it here on the range
Oh-my-ty-yi-yippee, I would love it more if it changed

Yes, I'd love it more if it changed!