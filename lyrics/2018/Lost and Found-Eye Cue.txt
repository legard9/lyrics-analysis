[Intro]
Come and take me, I can show you how it's done
Come and take me when no one is around
Come and take me, I can show you how it's done
We'll be lost and we'll be

[Verse 1]
If you want me, I can tell you what it's all about
You're standing in the shadow, I'm wondering why
You're in the dark
I want a deeper, deeper love
I want you by my side
Just let me deeper, go deeper

[Pre-Chorus]
Come and take me, I can show you how it's done
Come and take me when no one is around
Come and take me, I can show you how it's done
We'll be lost and we'll be found
We'll be lost and we'll be found, found

[Chorus]
Have you ever thought about it?
We went too far, no there's no way back
No way back
Have you ever thought about it?
We went too far, no there's no way back
No way back

[Verse 2]
I wish you trust your intitution and let the journey start
You're afraid that you might suffer
But trust me what you'll find will blow your mind
I want a deeper, deeper love
I want you by my side
Just let me deeper, go deeper, go deeper

[Pre-Chorus]
Come and take me, I can show you how it's done
Come and take me when no one is around
Come and take me, I can show you how it's done
We'll be lost and we'll be found
We'll be lost and we'll be
Baby, you and I we'll be lost and found

[Chorus]
Have you ever thought about it?
We went too far, no there's no way back
No way back
Have you ever thought about it?
We went too far, no there's no way back
No way back

[Outro]
We'll be lost and we'll be