I
Hoops
Blue and pink sashes,
Criss-cross shoes,
Minna and Stella run out into the garden
To play at hoop.
Up and down the garden-paths they race,
In the yellow sunshine,
Each with a big round hoop
White as a stripped willow-wand.
Round and round turn the hoops,
Their diamond whiteness cleaving the yellow sunshine.
The gravel crunches and squeaks beneath them,
And a large pebble springs them into the air
To go whirling for a foot or two
Before they touch the earth again
In a series of little jumps.
Spring, Hoops!
Spit out a shower of blue and white brightness.
The little criss-cross shoes twinkle behind you,
The pink and blue sashes flutter like flags,
The hoop-sticks are ready to beat you.
Turn, turn, Hoops!  In the yellow sunshine.
Turn your stripped willow whiteness
Along the smooth paths.
Stella sings:
        "Round and round, rolls my hoop,
         Scarcely touching the ground,
           With a swoop,
         And a bound,
         Round and round.
         With a bumpety, crunching, scattering sound,
         Down the garden it flies;
         In our eyes
         The sun lies.
         See it spin
         Out and in;
         Through the paths it goes whirling,
         About the beds curling.
         Sway now to the loop,
         Faster, faster, my hoop.
         Round you come,
         Up you come,
         Quick and straight as before.
         Run, run, my hoop, run,
         Away from the sun."
And the great hoop bounds along the path,
Leaping into the wind-bright air.
Minna sings:
          "Turn, hoop,
         Burn hoop,
         Twist and twine
          Hoop of mine.
         Flash along,
         Leap along,
         Right at the sun.
         Run, hoop, run.
         Faster and faster,
         Whirl, twirl.
          Wheel like fire,
          And spin like glass;
          Fire's no whiter
         Glass is no brighter.
          Dance,
          Prance,
          Over and over,
          About and about,
          With the top of you under,
         And the bottom at top,
         But never a stop.
         Turn about, hoop, to the tap of my stick,
          I follow behind you
          To touch and remind you.
          Burn and glitter, so white and quick,
           Round and round, to the tap of a stick."
The hoop flies along between the flower-beds,
Swaying the flowers with the wind of its passing.
Beside the foxglove-border roll the hoops,
And the little pink and white bells shake and jingle
Up and down their tall spires;
They roll under the snow-ball bush,
And the ground behind them is strewn with white petals;
They swirl round a corner,
And jar a bee out of a Canterbury bell;
They cast their shadows for an instant
Over a bed of pansies,
Catch against the spurs of a columbine,
Jostle the quietness from a cluster of monk's-hood.
Pat! Pat! behind them come the little criss-cross shoes,
And the blue and pink sashes stream out in flappings of colour.
Stella sings:
         "Hoop, hoop,
                  Roll along,
         Faster bowl along,
          Hoop.
          Slow, to the turning,
          Now go!—Go!
          Quick!
          Here's the stick.
          Rat-a-tap-tap it,
          Pat it, flap it.
           Fly like a bird or a yellow-backed bee,
          See how soon you can reach that tree.
           Here is a path that is perfectly straight.
          Roll along, hoop, or we shall be late."
Minna sings:
          "Trip about, slip about, whip about
          Hoop.
          Wheel like a top at its quickest spin,
          Then, dear hoop, we shall surely win.
          First to the greenhouse and then to the wall
           Circle and circle,
           And let the wind push you,
           Poke you,
           Brush you,
           And not let you fall.
           Whirring you round like a wreath of mist.
           Hoopety hoop,
           Twist,
           Twist."
Tap! Tap! go the hoop-sticks,
And the hoops bowl along under a grape arbour.
For an instant their willow whiteness is green,
Pale white-green.
Then they are out in the sunshine,
Leaving the half-formed grape clusters
A-tremble under their big leaves.
"I will beat you, Minna," cries Stella,
Hitting her hoop smartly with her stick.
"Stella, Stella, we are winning," calls Minna,
As her hoop curves round a bed of clove-pinks.
A humming-bird whizzes past Stella's ear,
And two or three yellow-and-black butterflies
Flutter, startled, out of a pillar rose.
Round and round race the little girls
After their great white hoops.
Suddenly Minna stops.
Her hoop wavers an instant,
But she catches it up on her stick.
"Listen, Stella!"
Both the little girls are listening;
And the scents of the garden rise up quietly about them.
"It's the chaise!  It's Father!
Perhaps he's brought us a book from Boston."
Twinkle, twinkle, the little criss-cross shoes
Up the garden path.
Blue—pink—an instant, against the syringa hedge.
But the hoops, white as stripped willow-wands,
Lie in the grass,
And the grasshoppers jump back and forth
Over them.
                                  II
                                    Battledore and Shuttlecock
The shuttlecock soars upward
In a parabola of whiteness,
Turns,
And sinks to a perfect arc.
Plat! the battledore strikes it,
And it rises again,
Without haste,
Winged and curving,
Tracing its white flight
Against the clipped hemlock-trees.
Plat!
Up again,
Orange and sparkling with sun,
Rounding under the blue sky,
Dropping,
Fading to grey-green
In the shadow of the coned hemlocks.
"Ninety-one."  "Ninety-two."  "Ninety-three."
The arms of the little girls
Come up—and up—
Precisely,
Like mechanical toys.
The battledores beat at nothing,
And toss the dazzle of snow
Off their parchment drums.
"Ninety-four."  Plat!
"Ninety-five."  Plat!
Back and forth
Goes the shuttlecock,
Icicle-white,
Leaping at the sharp-edged clouds,
Overturning,
Falling,
Down,
And down,
Tinctured with pink
From the upthrusting shine
Of Oriental poppies.
The little girls sway to the counting rhythm;
Left foot,
Right foot.
Plat!  Plat!
Yellow heat twines round the handles of the battledores,
The parchment cracks with dryness;
But the shuttlecock
Swings slowly into the ice-blue sky,
Heaving up on the warm air
Like a foam-bubble on a wave,
With feathers slanted and sustaining.
Higher,
Until the earth turns beneath it;
Poised and swinging,
With all the garden flowing beneath it,
Scarlet, and blue, and purple, and white—
Blurred colour reflections in rippled water—
Changing—streaming—
For the moment that Stella takes to lift her arm.
Then the shuttlecock relinquishes,
Bows,
Descends;
And the sharp blue spears of the air
Thrust it to earth.
Again it mounts,
Stepping up on the rising scents of flowers,
Buoyed up and under by the shining heat.
Above the foxgloves,
Above the guelder-roses,
Above the greenhouse glitter,
Till the shafts of cooler air
Meet it,
Deflect it,
Reject it,
Then down,
Down,
Past the greenhouse,
Past the guelder-rose bush,
Past the foxgloves.
"Ninety-nine," Stella's battledore springs to the impact.
Plunk!  Like the snap of a taut string.
"Oh!  Minna!"
The shuttlecock drops zigzagedly,
Out of orbit,
Hits the path,
And rolls over quite still.
Dead white feathers,
With a weight at the end.
                                 III
                                   Garden Games
The tall clock is striking twelve;
And the little girls stop in the hall to watch it,
And the big ships rocking in a half-circle
Above the dial.
Twelve o'clock!
Down the side steps
Go the little girls,
Under their big round straw hats.
Minna's has a pink ribbon,
Stella's a blue,
That is the way they know which is which.
Twelve o'clock!
An hour yet before dinner.
Mother is busy in the still-room,
And Hannah is making gingerbread.
Slowly, with lagging steps,
They follow the garden-path,
Crushing a leaf of box for its acrid smell,
Discussing what they shall do,
And doing nothing.
"Stella, see that grasshopper
Climbing up the bank!
What a jump!
Almost as long as my arm."
Run, children, run.
For the grasshopper is leaping away,
In half-circle curves,
Shuttlecock curves,
Over the grasses.
Hand in hand, the little girls call to him:
           "Grandfather, grandfather gray,
           Give me molasses, or I'll throw you away."
The grasshopper leaps into the sunlight,
Golden-green,
And is gone.
"Let's catch a bee."
Round whirl the little girls,
And up the garden.
Two heads are thrust among the Canterbury bells,
Listening,
And fingers clasp and unclasp behind backs
In a strain of silence.
White bells,
Blue bells,
Hollow and reflexed.
Deep tunnels of blue and white dimness,
Cool wine-tunnels for bees.
There is a floundering and buzzing over Minna's head.
"Bend it down, Stella.  Quick!  Quick!"
The wide mouth of a blossom
Is pressed together in Minna's fingers.
The stem flies up, jiggling its flower-bells,
And Minna holds the dark blue cup in her hand,
With the bee
Imprisoned in it.
Whirr! Buzz! Bump!
Bump! Whiz! Bang!
BANG!!
The blue flower tears across like paper,
And a gold-black bee darts away in the sunshine.
"If we could fly, we could catch him."
The sunshine is hot on Stella's upturned face,
As she stares after the bee.
"We'll follow him in a dove chariot.
Come on, Stella."
Run, children,
Along the red gravel paths,
For a bee is hard to catch,
Even with a chariot of doves.
Tall, still, and cowled,
Stand the monk's-hoods;
Taller than the heads of the little girls.
A blossom for Minna.
A blossom for Stella.
Off comes the cowl,
And there is a purple-painted chariot;
Off comes the forward petal,
And there are two little green doves,
With green traces tying them to the chariot.
"Now we will get in, and fly right up to the clouds.
           Fly, Doves, up in the sky,
           With Minna and me,
           After the bee."
Up one path,
Down another,
Run the little girls,
Holding their dove chariots in front of them;
But the bee is hidden in the trumpet of a honeysuckle,
With his wings folded along his back.
The dove chariots are thrown away,
And the little girls wander slowly through the garden,
Sucking the salvia tips,
And squeezing the snapdragons
To make them gape.
"I'm so hot,
Let's pick a pansy
And see the little man in his bath,
And play we're he."
A royal bath-tub,
Hung with purple stuffs and yellow.
The great purple-yellow wings
Rise up behind the little red and green man;
The purple-yellow wings fan him,
He dabbles his feet in cool green.
Off with the green sheath,
And there are two spindly legs.
"Heigho!" sighs Minna.
"Heigho!" sighs Stella.
There is not a flutter of wind,
And the sun is directly overhead.
Along the edge of the garden
Walk the little girls.
Their hats, round and yellow like cheeses,
Are dangling by the ribbons.
The grass is a tumult of buttercups and daisies;
Buttercups and daisies streaming away
Up the hill.
The garden is purple, and pink, and orange, and scarlet;
The garden is hot with colours.
But the meadow is only yellow, and white, and green,
Cool, and long, and quiet.
The little girls pick buttercups
And hold them under each other's chins.
"You're as gold as Grandfather's snuff-box.
You're going to be very rich, Minna."
"Oh-o-o!  Then I'll ask my husband to give me a pair of garnet earrings
Just like Aunt Nancy's.
I wonder if he will.
I know.  We'll tell fortunes.
That's what we'll do."
Plump down in the meadow grass,
Stella and Minna,
With their round yellow hats,
Like cheeses,
Beside them.
Drop,
Drop,
Daisy petals.
           "One I love,
           Two I love,
           Three I love I say..."
The ground is peppered with daisy petals,
And the little girls nibble the golden centres,
And play it is cake.
A bell rings.
Dinner-time;
And after dinner there are lessons.