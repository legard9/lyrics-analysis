[Intro: Christina Aguilera]
Hide it 'til we feel it, then we feel it, 'til we fight it, yeah
Hold it 'til we need it, never leave it, didn't want it, yeah

[Verse 1: Christina Aguilera & Ty Dolla $ign]
New York, worldwide (L.A., worldwide)
Boy, this my city (Girl, that's my hood)
Just pulled up to the hotel (Hotel, hotel)
All my day ones here with me (Ah, yeah)
We got moola, power, we on fire tonight
Gonna get it how we want it, 'cause we 'bout it
'Bout that life

[Pre-Chorus: Christina Aguilera & Ty Dolla $ign]
Baby, it's alright (Baby, it's alright)
Baby, it's OK (Baby, it's OK)
Smile, cry later (Ya Ya Ya Ya)
Don't worry 'bout tomorrow
I'll be with my ladies you can find me there
Try to play us, we gon' start a riot up in here

[Chorus: Christina Aguilera & Ty Dolla $ign]
Accelerate, c'mon babe, pick up your speed
Stamina, fill me up, that's what I need
Another shot, you comin' home with me
Fuck all these drugs, fuck all these clubs
What's wrong with me? (Oh, ya ya)

[Verse 2: Christina Aguilera & Ty Dolla $ign]
All my boss ladies (Independent, yeah)
Go get your Mercedes (skrrt, skrrt, skrrt, skrrt)
No matter, long as you get there (Get there)
Just don't let it drive you crazy (ah-ha-ah)
Get that moola, power, you on fire tonight
You can get it how you want it, that's it, go tonight

[Pre-Chorus: Christina Aguilera & Ty Dolla $ign]
Baby, it's alright, (Baby, it's alright)
Baby, it's OK, (Baby, it's OK)
Smile, cry later
Don't worry 'bout tomorrow (Nah, nah, nah, nah)
I'll be with my ladies, you can find me there (Find me)
Try to play us, we gon' start a riot up in here

[Chorus: Christina Aguilera, Ty Dolla $ign & 2 Chainz]
Accelerate, c'mon babe, pick up your speed
Stamina, fill me up, that's what I need
Another shot, you comin' home with me (Yeah)
Fuck all these drugs, fuck all these clubs (2 Chainz)
What's wrong with me?

[Verse 3: 2 Chainz, with Christina Aguilera]
Right, left, mic left, mic check, trap check
Sex drive, NASCAR, crash it like the Nasdaq
More than you can expect, everything except
Jealousy and envy, we gon' move on past that
Joint strong, pass that, ooh, girl, bad, bad
Where the, where the cash at? Don't forget the hashtag
Pretty, pretty, so saditty, work it out, muscle memory
Get the money, my ability, until the end, 2000, infinity
I put it in, now that you're feelin' me (feelin' me)
Look how I'm killin' it (killin' it), leavin' the dealership (yeah)
You ain't got internet? (you ain't heard?)
Just left the Benedict (uh), I got them benefits (uh)
Did it deliberate, guilty till proven innocent

[Bridge: Christina Aguilera & Ty Dolla $ign]
Hide it 'til we feel it, then we feel it, 'til we fight it, yeah
Hold it 'til we need it, never leave it, didn't want it, yeah
Fuck all these drugs, fuck all these clubs
What's wrong with me?

[Chorus: Christina Aguilera, Ty Dolla $ign & Both]
Accelerate, c'mon babe, pick up your speed
(Pick up your speed, babe)
Stamina, fill me up, that's what I need (Oh, yeah)
Another shot, you comin' home with me
Fuck all these drugs, fuck all these clubs
What's wrong with me?

[Outro: Christina Aguilera & Ty Dolla $ign]
Ooh-ooh, ooh-ooh, woo
Ooh-ooh, yeah, ah, yeah
Ooh-ooh, ooh-ooh, ah, yeah
Ooh-ooh, oh yeah
Ooh-ooh, ooh-ooh
Ooh-ooh
Ooh-ooh, ooh-ooh
Ooh-ooh
Yeah, yeah
Ooh