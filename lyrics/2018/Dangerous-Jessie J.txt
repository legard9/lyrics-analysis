[Verse 1]
Dressed like James Dean
Sound like Marvin Gaye when you sing to me
I'm in Hollywood but I wanna go downtown to you
I love when you're around, it's true
We talk music
You get melo-high, so therapeutic
Even though I should, won't try to leave your side tonight
I want to feel your love inside
Of me, me
(Love it when it's you and) Me, me
(Only just you and) Me, me
(Love it when it's you and) Me, me

[Pre-Chorus]
Your silence drives me crazy
Talking with your hands gets me naked
You're hot in my mouth, feels amazing
I love it because it's easy to lust, ooh

[Chorus]
You are dangerous
So dangerous
You're my dangerous
So dangerous
And I love it, ooh
And I love it, ooh

[Verse 2]
I'm addicted to ya
You're trouble but the sex got me screaming, "Hallelujah"
Slide on it, ride
Slowly put your hands in mine
I can feel it's almost time
For me, me
Love it when it's you and me, me
Only just you and me, me
Love it when it's you and me, me

[Pre-Chorus]
Your silence drives me crazy
Talking with your hands gets me naked
You're hot in my mouth, feels amazing
I love it because it's easy to lust, ooh

[Chorus]
You are so dangerous
So dangerous
You're my dangerous
So dangerous
And I love it (Woo)
And I love it (Yeah, woo, ah, yes)
And I love it

[Outro]
Sexy, sexy (Woo), you are, you are (And I love it)
Sexy, sexy (Woo), you are, you are
You are so sexy
You are so sexy
So sexy to me, mm
Da-da-da-da
Da-da-da-da-da-da
Da-da-da-da-da-da-da-da
Ba-da-ba-ba-la, ooh