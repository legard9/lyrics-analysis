[Intro]
Can I be honest with you?
Can I tell you the truth?

[Verse 1]
I never did mind the storm
Even when my heart was torn
Thunder in the night sky
Running from a town like this
Crashing with a stolen kiss
We're all glitter and tears in the moonlight
And it came to me in a taxi ride
Hit me like a blinding light
And my heart was beating so alive
Wanna find myself out in the wild
Oh I, oh I

[Chorus]
I wanna get lost without you
(In the dark, in the night)
I wanna get lost without you
(Get it wrong, make it right)
Ooh, ooh
Tell me is that cool?
I wanna get lost without you

[Verse 2]
Ain't gonna be afraid of fun
Gonna let my makeup run
Oh, see every sunrise
Facing up to all my fears
Gonna find a way outta here
We're all glitter and tears in the moonlight

[Chorus]
I wanna get lost without you
(In the dark, in the night)
I wanna get lost without you
(Get it wrong, make it right)
Ooh, ooh
Tell me is that cool?
I wanna get lost without you
I wanna get lost without you

[Bridge]
Before the sky was broken
I could get in and out of trouble on my own
Just don't leave me totally alone
Baby, the world can see me dance
The universe was ours and then it disappeared
But the skies are still blue
With or without you
And now I'm standing out here, in the crowd
And I'm dancing

[Chorus]
I wanna get lost without you
(In the dark, in the night)
I wanna get lost without you
(Get it wrong, make it right)
Ooh, ooh
Tell me is that cool?
I wanna get lost without you
I wanna get lost without you