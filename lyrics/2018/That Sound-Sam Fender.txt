[Verse 1]
Serotonin stole the moment
The best of me was left under
The bed-sheets on that sombre morning
Dying for some REM
My mind is always troubled with
Where have I been and where am I going

[Chorus]
When my head comes crashing down
They're all waiting at the bottom
Tryna claw me down beneath it all
Every night, I beg that sound
It's the greatest revelation
It’s the only thing that keeps me grounded

[Post-Chorus]
I need to hear that sound
I need to hear that

[Verse 2]
Loaded vampires butter me up
Drop names and sniff up residue
While boasting 'bout their revenue
At home, I face these green-eyed beasts
Everybody wants to leave
But no one wants to see you do it

[Chorus]
When my head comes crashing down
They're all waiting at the bottom
Tryna claw me down beneath it all
Every night, I beg that sound
It's the greatest revelation
It's the only thing that keeps me grounded

[Post-Chorus]
I need to hear that sound
I need to hear that

[Chorus]
When my head comes crashing down
They're all waiting at the bottom
Tryna claw me down beneath it all
Every night, I beg that sound
It's the greatest revelation
It’s the only thing that keeps me grounded

[Post-Chorus]
I need to hear that sound
I need to hear that sound