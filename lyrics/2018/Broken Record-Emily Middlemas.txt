[Verse 1]
I know you and I know your heart
You just scared to tear mine apart
Feeling like you have to stay
'Cause I enjoy your company
You ain't staying for the real love
I know you and you know that it's wrong
You pick up the phone, but you're so sick of me calling
But I do it anyway, hoping that you'll love me

[Pre-Chorus]
One day, I need you to need me
To remind me that you love me
But you don't tell me it's a lie

[Chorus]
Am I caught up with confusion, babe?
An illusion to stay, so dark and ashamed
[?]
I've been digging in the snow
Making fire, making flames
I never really thought you cared though
Thought we were running out of time
I never really thought I cared to you
A broken record in mind

[Verse 2]
You know me and you know that I'm lost
Got no guidance from you, baby, just stop
I'm more than pictures, I'm more than words

[Pre-Chorus]
Ooh, I need you to need me
To remind me that you love me
But you don't tell me it's a lie

[Chorus]
Am I caught up with confusion, babe?
An illusion to stay, so dark and ashamed
[?]
I've been digging in the snow
Making fire, making flames
I never really thought you cared though
Thought we were running out of time
I never really thought I cared to you
A broken record in mind

[Bridge]
I'm broken, I'm broken
I broke apart, aah
I'm broken (I'm broken), I'm broken (I'm broken)
I broke apart, aah

[Chorus]
Am I caught up with confusion, babe?
An illusion to stay, so dark and ashamed
[?]
I've been digging in the snow
Making fire, making flames
I never really thought you cared though
Thought we were running out of time
I never really thought I cared to you
A broken record in mind