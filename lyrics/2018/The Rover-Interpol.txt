[Verse 1]
Come and see me and maybe you'll die
But I can keep you in artwork, the fluid kind
That's enough for excitements today
Prostrated faded, it's pay-to-play

[Verse 2]
Come and see me, yeah, maybe you'll try
I've been holding these pyros til they could fly
Open up and enlighten again
Enjoy the skyline, it's an incremental end

[Chorus]
"Walk in on your own feet"
Says the rover
"It's my way or they all leave"
Says the rover
(Ooooh, Ooooh)
The rover

[Verse 3]
Ça suffit, hell yeah, maybe it's time
You can't stick to the highways, it's suicide
I'm welling up with excitements again
The apex resolves, you need to tell your friends

[Chorus]
"Walk in on your own feet"
Says the rover
"It's my way or they all leave"
Says the rover
(Ooooh, Ooooh)
The rover
(Ooooh, Ooooh)
The rover

[Verse 4]
Come and see me, yeah baby, let's cry
Satin face in some worlds we’d be too kind
Nature’s subjected to fires again
Falling for my independence

[Chorus]
"Walk in on your own feet"
Says the rover
You were high and on the wrong street
Till the rover

[Outro]
Said "Hop in, all in"
(Ooooh, Ooooh)
The rover
"Hop in, all in"
(Ooooh, Ooooh)
The rover
(Ooooh, Ooooh)
The rover
"All in, all in"
(Ooooh, Ooooh)
The rover
He barely has to seek repentants