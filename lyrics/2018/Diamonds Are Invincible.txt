[Psycho Girl, spoken]
Girls, we're gonna kill it today! And I'm feeling crazy good!

[Lilly and Ruby, spoken]
Uh oh!

[Psycho Girl, spoken]
Like I'm invincible! Are you thinking what I'm thinking?

[Psycho Girl]
Come on, girls
Let's play some PvP
We'll have some fun
Kill everyone we see
No one can stop us
We've got that girl power
They'll crown us champions
Women of the hour

[Lilly and Ruby]
Invincible
We're unstoppable
We're incredible
Nanananananana
Invincible
We're unstoppable
We're incredible
Nanananananana
Yeah

[Psycho Girl]
We've got style
We've got the fancy moves
We've got more diamonds
So no way we can lose
Those silly boys
They always think they're better
We'll sneak behind you
And then we'll come and get ya

[Lilly and Ruby]
Invincible
We're unstoppable
We're incredible
Nanananananana
Invincible
We're unstoppable
We're incredible
Nanananananana
Yeah

[Lilly, Spoken]
What was that?

[Ruby, Spoken]
Girl, you're all glitchy

[Psycho Girl, Spoken]
I know, it's been driving me nuts

[Lilly, Spoken]
That was weird

[Ruby, Spoken]
Okay?

[Psycho Girl, Spoken]
Guys
I don't know why
But I have to confess

[Psycho Girl]
I feel new strange powers from within now
Nobody's gonna bully me
Because they know I am

[Lilly and Ruby]
Invincible
We're unstoppable
We're incredible
Nanananananana
Invincible
We're unstoppable
We're incredible
Nanananananana
Yeah

[Psycho Girl]
Let's show my new powers within
There's so much more beneath this skin

[Psycho Girl, Spoken]
Why can't I turn them on?

[Psycho Girl]
What's happening?

[Psycho Girl, Spoken]
Ow!
No!
No!

[Psycho Girl]
Let me go!