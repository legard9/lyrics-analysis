PITCH FOR A MOVIE: LION KING IN THE HOOD



1. cast list

Mufasa & his absence played by every father ever

Simba played by the first boy you know who died too young

Sarabi played by the woman in church who has forgot the taste of praise
in favor of the earth that hold her boy captive

Nala played by the girl crying on the swing for her valentine who now date the dirt

Timon & Pumbaa played by Ray-Ray & Man-Man, the joy of not-dead friends

Zazu played by the ghost of James Baldwin

Rafiki played by a good uncle with a bad habit, his lust for rocks on his lips

Scar played by the world, the police, the law & its makers, the rope-colored hands

The endless army of hyenas played by a gust
choked tight with bullet shells, the bullets themselves
now dressed in a boy







2. Opening Credits

        brought to you by Disney & dead aunts

brought to you on a platter, an apple in the lion-boy’s mouth

        brought to you on a terrible boat, reeking of shit & an unnamed sick

brought to you on a tree branch heavy with tree-colored man







3. Opening Scene: The Circle of [interrupted]Life

Nants ingonyama bagithi baba
Sithi uhhmm [BANG] ingonyama

Nants ingonyama [a mother calls for her boy] bagithi baba
Sithi [BANG BANG] uhhmm ingonyama

Ingonyama Siyo [the sound of blood leaving a boy] Nqoba

Ingonyama  [a mother’s knees fall into a puddle
of the blood she made herself]
Ingonyama [the slow song of a spirit rising]nengw’ enamabala
[the spirit is confused about where its body went]







4. Song: Oh I just can’t wait to be king

this is the part where they realize that black people dream
& our blood is indeed blood & our teeth, teeth
& the music is loud because the field was wide & the field was long
& we dreamed on simple things: shoes, our children back

this is the part where the racist cuts off his tongue, his wet, pink repent
he gives his eyes & his hands & anything that has ever assumed
a black body a black & burnable thing. He gives himself to the lions
& the lions feast & the lions are still a metaphor for black boys
& the boys, full of fear turned into dinner, fall asleep & they dream

yes, yes, they really do dream.







5. Song: Be Prepared

        for the stampede of tiny lead beast

for the jury not to flinch

        for the hands, wild in your wildless hair

for the darkest toll, your double down to get half

        for the man, ecstatic with triggers

to spread your legs while they search for drugs there

        for the drug there, for their mouths to ask that big question

to hear your history told to you over & over & over

        for it never to change.








6. Scene:  Mufasa Dies at the hands of his brother Scar

What did you expect to be different?
The hood (according to whiteness & the sadness of this film) is any jungle:

a brother kills a brotha

        the left behind body a forgettable meal
        for the dark, white birds circling above








7. Montage: Timon & Pumbaa teach Simba a music other than the blues

clip 1: the boy getting older in spite of everything

clip 2: the boy & the boy-friends smoking blunts
for once something else brown & on fire

clip 3: the boy who would be king with his mouth
in another man’s throne

clip 4: Timon & Simba singing
down each other’s throat

clip 5: Timba calling Pumbaa a faggot & they all laugh

clip 6: murals of all the dead friends’ faces

clip 7: funeral songs. small caskets

clip 8: red, blue, periwinkle, yellow, black, & blood-maroon rags

clip 9: flowers & picture frames on the side walk

clip 10: shot the boys laughing anyway

clip 11: shot the boys laughing in the sun

clip 12: shot the boys laughing in the rain

clip 13: shot of the boys not being shot







8. Scene: Simba comes home to kill his uncle

& even here, a black man
kills a black man, this awful cartoon, all the children cry
blood until their bodies are just bone on skin.

roll the credits, I must go weep. Why does Disney remind us
what we have learned:

        - one black light swallows another so easy
        - killing is unavoidable as death
        - the king’s throne is wet with his brother’s blood
        - the queen suffers too but gets no name








9. closing credits

say the name
of the first boy
you love
who died.
say it
& don’t cry.
say it
& love
the air
around your tongue.
say it
& watch
the fire come.
say it
& watch the son rise.