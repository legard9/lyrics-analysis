[Verse 1: Yunho, San, Seonghwa, Yeosang, (Hongjoong)]
Neo geureoda huhoehandago
Jogeum deo keumyeon alge doendago
Maebeon dasi ttokgateun malman
I'm sick and tired of it
I'm sick and tired of it, yeah (Nobody knows!)
"Hago sipeun ge mwonyago?"
"Neoneun keoseo mwoga doel geonyago?" (Oh yeah, yeah, yeah)
Moreugenneyo cheoeumira
Deo sarabwaya al geot gateunde

[Verse 2: Hongjoong]
Ayy, ayy, ayy
Ayy, iraerajeoraera ebebebe
Ssibeojumyeon nan deo jeulgyeo pop, pop, pop
Jaejiri wanjeon pungseonkkeom teojyeo pum, pum, pum
Ah-yeah. kkirikkirideul moyeo jjokjjokjjok
Ipdeuri an swieo cham, geunyang baksu chyeo jjakjjakjjak

[Pre-Chorus: Mingi, Jongho, San, Wooyoung]
Cheonggi deureo baekgi deureo
Eo? neoneun wae mal an deureo?
Na ttaeneun eum neo ttaega mwo
Egumeoniya gadeon gil gasyeo
Nae mari da matdago
Haebwaseo da andago
Yes sir, yes sir, yes sir
Gracias

[Chorus: Yunho, San, Jongho]
Geokjeongeun no, thanks, I'm okay (Oh)
Nan geujeo nail ppuningeol (Hey, hey)
Nal wihaeseora haji ma
Naebeoryeo dwo nan nae rideume

[Post-Chorus: Jongho, Wooyoung, Mingi & Yeosang]
Chumeul chwo chumeul chwo chumeul chwo
Umjigyeo umjigyeo umjigyeo
Chumeul chwo chumeul chwo chumeul chwo (Let's go, let's go)
Nae siganeun naega wonhaneun daero

[Verse 3: Mingi]
Fix on! (Yeah)
Ssonda gireul bikyeora naega naganda beonjjeok
Hanadul hamyeon jeongjeok
Jinachin gwansimeun dogini janeul beorigo bless up
Seonbongui udumeori baegimyeon baegeul japji
Junbireul hasigo jaba nae kkorie kkorireul multtaekkaji

[Pre-Chorus: Hongjoong, Jongho, San, Wooyoung]
Cheonggi deureo baekgi deureo
Eo? neoneun wae mal an deureo?
Na ttaeneun eum neo ttaega mwo
Egumeoniya gadeon gil gasyeo
Nae mari da matdago
Haebwaseo da andago
Yes sir, yes sir, yes sir
Gracias

[Chorus: Seonghwa, San, Jongho]
Geokjeongeun no, thanks, I'm okay (Oh)
Nan geujeo nail ppuningeol (Hey, hey)
Nal wihaeseora haji ma
Naebeoryeo dwo nan nae rideume

[Post-Chorus: Jongho, San, Mingi & Yeosang]
Chumeul chwo chumeul chwo chumeul chwo
Umjigyeo umjigyeo umjigyeo
Chumeul chwo chumeul chwo chumeul chwo (Let's go, let's go)
Nae siganeun naega wonhaneun daero

[Bridge A: Seonghwa, Yunho & Jongho]
Haji ma haji ma ne mamdaero
Naneun nal naneun nal jal ara
Mwodeun da mwodeun da nae sikdaero
Hage naebeoryeo dwo

[Bridge B: Mingi, Hongjoong, Both, San & (Jongho)]
We are another type, we are another, baby
Gakjaui sokdoneun da dalla, we are all another, baby
Nae mal jom deureurago, haebomyeon an doenyago
(Yes sir, yes sir, yes sir)
(I'm ok, it's all right)

[Chorus: Yunho, San, Jongho]
Geokjeongeun no, thanks, I'm okay (Ooh-ooh-ooh-ooh; Oh-ooh)
Nan geujeo nail ppuningeol (nail ppuningeol; Hey, hey)
Nal wihaeseora haji ma
Naebeoryeo dwo nan nae rideume

[Post-Chorus: San, Wooyoung, Jongho & Yeosang]
Chumeul chwo chumeul chwo chumeul chwo (chumeul chumeul chwo, yeah)
Umjigyeo umjigyeo umjigyeo (momeul umjigyeo, yeah)
Chumeul chwo chumeul chwo chumeul chwo (Let's go, let's go)
Nae siganeun naega wonhaneun daero