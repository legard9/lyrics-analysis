[Verse 1]
You know I want it
Thing got me up on it (On it)
Boy, look in my, look in my eyes
I'm coming, I’m coming
Down to the lobby
Got the key to my body
I'm with it, I'm with it tonight
I’m coming, I'm coming (Yeah)

[Chorus]
Wearing nothing but you
High on the seventeenth floor
Gimme some, gimme some more
I knew just what to do (Oh yeah)
Dropping my clothes on the floor
Put that "Do Not Disturb" on the door

[Post-Chorus]
Do not, do not disturb
Do not, do not disturb
(Come on, come on)
Do not, do not disturb
Do not, do not disturb
(Come on, come on)

[Verse 2]
Pull me in closer
Do it over and over (Oh, oh)
I'm working that overtime
I love it, I love it (Love it, I love it)
I'm going in deeper
Marvin Gaye on the speaker (Yeah, word)
I'm getting it, getting it on
I love it, I love it (Oh)

[Chorus]
Wearing nothing but you
High on the seventeenth floor
Gimme some, gimme some more
I knew just what to do (Let's go)
Dropping my clothes on the floor
Put that "Do Not Disturb" on the door

[Post-Chorus]
Do not, do not disturb
Do not, do not disturb
(Come on, come on)
Do not, do not disturb
Do not, do not disturb
(Come on, come on)

[Outro]
Do not, do not disturb
Do not, do not disturb
(Come on, come on)
Do not, do not disturb
Do not, do not disturb
(Come on, come on)
Do not, do not disturb
Do not, do not disturb
(Come on, come on)