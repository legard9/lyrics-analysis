I don't wanna waste it
You give me a taste of
Who you are, who you are
You should leave your lights on
'Cause I can be a love song
That feeds your heart
That feeds your heart

All I need's an open door
That you'll hold open once you're sure
For me

If I could make you mine
If I could make you mine
I would make us shine, shine, shine forever
If I could make you mine
If I could make you mine
I would make us shine, shine, shine forever

If I could be your feather, be your feather
I would find a way to fall into your heart
And I would make you mine
And I would make us shine
I would make us shine, shine, shine forever

I wanna hear your heartbeat
I wanna have it closer
You and me, next to me
Would you spare a little time
I wanna show you what it's like
To be with me, be with me, yeah

All I need's an open door
That you'll hold open once you're sure
For me

If I could make you mine
If I could make you mine
I would make us shine, shine, shine forever
If I could make you mine
If I could make you mine
I would make us shine, shine, shine forever

If I could be your feather, be your feather
I would find a way to fall into your heart
And I would make you mine
And I would make us shine
I would make us shine, shine, shine forever


And I'll keep holding on
I know that you're the one
I will be patient till I figure out
And all you need is time
I know we'll be alright
Somehow I'll be just fine

If I could make you mine
If I could make you mine
I would make us shine, shine, shine forever
If I could make you mine
If I could make you mine
I would make us shine, shine, shine forever

If I could be your feather, be your feather
I would find a way to fall into your heart
And I would make you mine
And I would make us shine
I would make us shine, shine, shine forever