[Intro: Nick Jonas]
Yeah, yeah, yeah, yeah
Yeah, yeah, yeah, yeah

[Verse 1: Nick Jonas]
You are my water, my sun, my moon and stars
Your heart is all I need
It starts when you come
I want to be where you are, where you are (na, na)

[Pre-Chorus: Nick Jonas]
Every time you go away, I'm always tryna fight
How bad I want you (bad I want you), bad I want you (ooh)
I could try to fill the space with someone else tonight
But I don't want to (I don't want to), I don't want to

[Chorus: Nick Jonas]
Right now, you know I miss your body
So I won't kiss nobody until you come back home
And I swear, the next time that I hold you
I won't let you go nowhere
You'll never be alone, I'll never let you go

[Verse 2: Nick Jonas]
You lead, I follow, no sleep
That's what you said (that's what you said)
That's what you said (that's what you said)
And you should know
You leave, I'm hollow
I need you in my bed (I need you in my)
You in my bed (I need you in my)

[Pre-Chorus: Nick Jonas]
Every time you go away, I'm always tryna fight
How bad I want you (ay), bad I want you (bad I want you)
I could try to fill the space with someone else tonight
But I don't want to (I don't want to), I don't want to (oh)

[Chorus: Nick Jonas]
Right now, you know I miss your body
So I won't kiss nobody (won't kiss nobody, baby)
Until you come back home (oh)
And I swear, the next time that I hold you
I won't let you go nowhere (let go)
You'll never be alone (never be alone), I'll never let you go

[Post-Chorus: Nick Jonas]
Yeah, oh, yeah, oh, no
I will never let you go, no, no
Yeah, oh, no

[Chorus: Nick Jonas]
Right now, you know I miss your body
So I won't kiss nobody until you come back home
I swear, the next time that I hold you
I won't let you go nowhere
You'll never be alone
Right now, you know I miss your body (I miss your body, baby)
So I won't kiss nobody (nobody) until you come back home (oh)
And I swear, the next time that I hold you (ooh)
I won't let you go nowhere (I won't let you go)
You'll never be alone (oh), I'll never let you go

[Outro: Nick Jonas]
Oh, you'll never be alone, ooh, yeah
I'll be there, I'll never let you go