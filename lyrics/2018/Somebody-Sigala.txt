[Verse 1: HRVY]
We painted a picture
Broke the rules in the summer rain
Twenty years later
We'd have a big house in L.A
Seems like our future is on the line for my mistake
I can't let this fade away

[Pre-Chorus: HRVY]
And now we find it hard to sleep
And though your body's next to me
I still got far to go
I'm hanging on to hope
Say those words you don't believe
You turn around and look at me
I still got far to go
You're hanging on to hope

[Chorus: HRVY]
So good that I don't deserve it
So close that it's almost perfect
To be lying in your arms
Even though I broke your heart
So high for the chance to fix this
I’m wrapped up in your forgiveness
And without you I am nothing
But I feel like somebody with you
Somebody with you
Somebody with you
Somebody with you
(Somebody)
And without you I am nothing
But I feel like somebody with you

[Verse 2: Nina Nesbitt]
So we're searching
Looking for ways we can repair
Out of the certain
Cause you know deep down I care
You gotta listen
When I tell you I'll be there
Oh please don't let us disappear

[Pre-Chorus: Nina Nesbitt]
Say those words you don't believe
You turn around and look at me
I still got far to go
You're hanging on to hope

[Chorus: HRVY and Nina Nesbitt]
So good that I don't deserve it
So close that it's almost perfect
To be lying in your arms
Even though I broke your heart
So high for the chance to fix this
I’m wrapped up in your forgiveness
And without you I am nothing
But I feel like somebody with you
Somebody with you
Na na na na, yeah yeah
Na na na na, yeah yeah
Somebody with you
Na na na na, yeah yeah
Na na na na, yeah yeah
Na na na na, yeah yeah

So good that I don't deserve it
So close that it's almost perfect
To be lying in your arms
Even though I broke your heart
So high for the chance to fix this
I’m wrapped up in your forgiveness
And without you I am nothing
But I feel like somebody with you

So good that I don't deserve it
So close that it's almost perfect
To be lying in your arms
Even though I broke your heart
So high for the chance to fix this
I’m wrapped up in your forgiveness
And without you I am nothing
But I feel like somebody with you
Somebody with you
Na na na na, yeah yeah
Na na na na, yeah yeah
Somebody with you
Na na na na, yeah yeah
Na na na na, yeah yeah
Na na na na, yeah yeah
Somebody with you

[Outro]
Na na na na, yeah yeah
Na na na na, yeah yeah
Na na na na, yeah yeah
Somebody with you
Na na na na, yeah yeah
Na na na na, yeah yeah
Na na na na, yeah yeah