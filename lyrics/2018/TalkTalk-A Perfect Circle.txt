[Verse 1]
You're waiting
On miracles
We're bleeding out
Thoughts
And prayers
Adorable (Crisis)
Like cake in a crisis
We're bleeding out

[Pre-Chorus]
While you deliberate
Bodies accumulate

[Chorus 1]
Sit and talk like Jesus
Try walkin' like Jesus
Sit and talk like Jesus
Talk like Jesus
Talk, talk, talk, talk
Get the fuck out of my way

[Bridge]
Don’t be the problem, be the solution
Don’t be the problem, be the solution
Don’t be the problem, be the solution
Problem, problem, problem, problem
Faith without works is
Talk without works is
Faith without works is
Dead, dead, dead, dead

[Chorus 2]
Sit and talk like Jesus
Try walkin' like Jesus
Sit and talk like Jesus
Try walkin' like Jesus
Try braving the rain
Try lifting the stone
Try extending a hand
Try walkin' your talk or get the fuck out of my way