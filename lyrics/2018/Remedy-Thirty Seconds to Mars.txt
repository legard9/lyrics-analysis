[Verse 1]
Hey man, where'd you go today?
Did you find some place to stay?
Far, far, far away, yeah
Hey man, there you go again
On your own again
You're breaking hearts again, yeah

[Chorus]
Alright, okay
Do you hear what I got to say?
Do you hear what I got to say?
I'm searching for a remedy
Alright, okay
Do you hear what I got to say?
Do you hear what I got to say?
I'm searching for a remedy

[Verse 2]
Hey man, everywhere you roam
Your demons gonna go
But at least you're not alone, yeah
Hey man, you're hanging by a thread
You're standing at the edge
It's one more chance to take my hand

[Chorus]
Alright, okay
Do you hear what I got to say?
Do you hear what I got to say?
I'm searching for a remedy
Alright, okay
Do you hear what I got to say?
Do you hear what I got to say?
I'm searching for a remedy
I'm searching for a remedy
I'm searching for a remedy

[Bridge]
Talk about it
Scream about it
Laugh about it
With anyone
Talk about it
Scream about it
Laugh about it
With anyone

[Chorus]
Alright, okay
Do you hear what I got to say?
Do you hear what I got to say?
I'm searching for a remedy
Alright, okay
Do you hear what I got to say?
Do you hear what I got to say?
I'm searching for a remedy
I'm searching for a remedy
I'm searching for a remedy
Talk about it
I'm searching for a remedy
Scream about it
I'm searching for a remedy