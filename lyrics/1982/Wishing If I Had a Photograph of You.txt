Poor Mercantile Jack



Is the sweet little cherub who sits smiling aloft and keeps watch on life of poor Jack, commissioned to take charge of Mercantile Jack, as well as Jack of the national navy?  If not, who is?  What is the cherub about, and what are we all about, when poor

Mercantile Jack is having his brains slowly knocked out by penny-weights, aboard the brig Beelzebub, or the barque Bowie-knife—when he looks his last at that infernal craft, with the first officer’s iron boot-heel in his remaining eye, or with his dying body towed overboard in the ship’s wake, while the cruel wounds in it do ‘the multitudinous seas incarnadine’?

Is it unreasonable to entertain a belief that if, aboard the brig Beelzebub or the barque Bowie-knife, the first officer did half the damage to cotton that he does to men, there would presently arise from both sides of the Atlantic so vociferous an invocation of the sweet little cherub who sits calculating aloft, keeping watch on the markets that pay, that such vigilant cherub would, with a winged sword, have that gallant officer’s organ of destructiveness out of his head in the space of a flash of lightning?

If it be unreasonable, then am I the most unreasonable of men, for I believe it with all my soul.

This was my thought as I walked the dock-quays at Liverpool, keeping watch on poor Mercantile Jack.  Alas for me!  I have long outgrown the state of sweet little cherub; but there I was, and there Mercantile Jack was, and very busy he was, and very cold he was: the snow yet lying in the frozen furrows of the land, and the north-east winds snipping off the tops of the little waves in the Mersey, and rolling them into hailstones to pelt him with.  Mercantile Jack was hard at it, in the hard weather: as he mostly is in all weathers, poor Jack.  He was girded to ships’ masts and funnels of steamers, like a forester to a great oak, scraping and painting; he was lying out on yards, furling sails that tried to beat him off; he was dimly discernible up in a world of giant cobwebs, reefing and splicing; he was faintly audible down in holds, stowing and unshipping cargo; he was winding round and round at capstans melodious, monotonous, and drunk; he was of a diabolical aspect, with coaling for the Antipodes; he was washing decks barefoot, with the breast of his red shirt open to the blast, though it was sharper than the knife in his leathern girdle; he was looking over bulwarks, all eyes and hair; he was standing by at the shoot of the Cunard steamer, off to-morrow, as the stocks in trade of several butchers, poulterers, and fishmongers, poured down into the ice-house; he was coming aboard of other vessels, with his kit in a tarpaulin bag, attended by plunderers to the very last moment of his shore-going existence.  As though his senses, when released from the uproar of the elements, were under obligation to be confused by other turmoil, there was a rattling of wheels, a clattering of hoofs, a clashing of iron, a jolting of cotton and hides and casks and timber, an incessant deafening disturbance on the quays, that was the very madness of sound.  And as, in the midst of it, he stood swaying about, with his hair blown all manner of wild ways, rather crazedly taking leave of his plunderers, all the rigging in the docks was shrill in the wind, and every little steamer coming and going across the Mersey was sharp in its blowing off, and every buoy in the river bobbed spitefully up and down, as if there were a general taunting chorus of ‘Come along, Mercantile Jack!  Ill-lodged, ill-fed, ill-used, hocussed, entrapped, anticipated, cleaned out.  Come along, Poor Mercantile Jack, and be tempest-tossed till you are drowned!’

The uncommercial transaction which had brought me and Jack together, was this:- I had entered the Liverpool police force, that I might have a look at the various unlawful traps which are every night set for Jack.  As my term of service in that distinguished corps was short, and as my personal bias in the capacity of one of its members has ceased, no suspicion will attach to my evidence that it is an admirable force.  Besides that it is composed, without favour, of the best men that can be picked, it is directed by an unusual intelligence.  Its organisation against Fires, I take to be much better than the metropolitan system, and in all respects it tempers its remarkable vigilance with a still more remarkable discretion.

Jack had knocked off work in the docks some hours, and I had taken, for purposes of identification, a photograph-likeness of a thief, in the portrait-room at our head police office (on the whole, he seemed rather complimented by the proceeding), and I had been on police parade, and the small hand of the clock was moving on to ten, when I took up my lantern to follow Mr. Superintendent to the traps that were set for Jack.  In Mr. Superintendent I saw, as anybody might, a tall, well-looking, well-set-up man of a soldierly bearing, with a cavalry air, a good chest, and a resolute but not by any means ungentle face.  He carried in his hand a plain black walking-stick of hard wood; and whenever and wherever, at any after-time of the night, he struck it on the pavement with a ringing sound, it instantly produced a whistle out of the darkness, and a policeman.  To this remarkable stick, I refer an air of mystery and magic which pervaded the whole of my perquisition among the traps that were set for Jack.

We began by diving into the obscurest streets and lanes of the port.  Suddenly pausing in a flow of cheerful discourse, before a dead wall, apparently some ten miles long, Mr. Superintendent struck upon the ground, and the wall opened and shot out, with military salute of hand to temple, two policemen—not in the least surprised themselves, not in the least surprising Mr. Superintendent.

‘All right, Sharpeye?’

‘All right, sir.’

‘All right, Trampfoot?’

‘All right, sir.’

‘Is Quickear there?’

‘Here am I, sir.’

‘Come with us.’

‘Yes, sir.’

So, Sharpeye went before, and Mr. Superintendent and I went next, and Trampfoot and Quickear marched as rear-guard.  Sharp-eye, I soon had occasion to remark, had a skilful and quite professional way of opening doors—touched latches delicately, as if they were keys of musical instruments—opened every door he touched, as if he were perfectly confident that there was stolen property behind it—instantly insinuated himself, to prevent its being shut.

Sharpeye opened several doors of traps that were set for Jack, but Jack did not happen to be in any of them.  They were all such miserable places that really, Jack, if I were you, I would give them a wider berth.  In every trap, somebody was sitting over a fire, waiting for Jack.  Now, it was a crouching old woman, like the picture of the Norwood Gipsy in the old sixpenny dream-books; now, it was a crimp of the male sex, in a checked shirt and without a coat, reading a newspaper; now, it was a man crimp and a woman crimp, who always introduced themselves as united in holy matrimony; now, it was Jack’s delight, his (un)lovely Nan; but they were all waiting for Jack, and were all frightfully disappointed to see us.

‘Who have you got up-stairs here?’ says Sharpeye, generally.  (In the Move-on tone.)

‘Nobody, surr; sure not a blessed sowl!’  (Irish feminine reply.)

‘What do you mean by nobody?  Didn’t I hear a woman’s step go up-stairs when my hand was on the latch?’

‘Ah! sure thin you’re right, surr, I forgot her!  ’Tis on’y Betsy White, surr.  Ah! you know Betsy, surr.  Come down, Betsy darlin’, and say the gintlemin.’

Generally, Betsy looks over the banisters (the steep staircase is in the room) with a forcible expression in her protesting face, of an intention to compensate herself for the present trial by grinding Jack finer than usual when he does come.  Generally, Sharpeye turns to Mr. Superintendent, and says, as if the subjects of his remarks were wax-work:

‘One of the worst, sir, this house is.  This woman has been indicted three times.  This man’s a regular bad one likewise.  His real name is Pegg.  Gives himself out as Waterhouse.’

‘Never had sitch a name as Pegg near me back, thin, since I was in this house, bee the good Lard!’ says the woman.

Generally, the man says nothing at all, but becomes exceedingly round-shouldered, and pretends to read his paper with rapt attention.  Generally, Sharpeye directs our observation with a look, to the prints and pictures that are invariably numerous on the walls.  Always, Trampfoot and Quickear are taking notice on the doorstep.  In default of Sharpeye being acquainted with the exact individuality of any gentleman encountered, one of these two is sure to proclaim from the outer air, like a gruff spectre, that Jackson is not Jackson, but knows himself to be Fogle; or that Canlon is Walker’s brother, against whom there was not sufficient evidence; or that the man who says he never was at sea since he was a boy, came ashore from a voyage last Thursday, or sails tomorrow morning.  ‘And that is a bad class of man, you see,’ says Mr. Superintendent, when he got out into the dark again, ‘and very difficult to deal with, who, when he has made this place too hot to hold him, enters himself for a voyage as steward or cook, and is out of knowledge for months, and then turns up again worse than ever.’

When we had gone into many such houses, and had come out (always leaving everybody relapsing into waiting for Jack), we started off to a singing-house where Jack was expected to muster strong.

The vocalisation was taking place in a long low room up-stairs; at one end, an orchestra of two performers, and a small platform; across the room, a series of open pews for Jack, with an aisle down the middle; at the other end a larger pew than the rest, entitled SNUG, and reserved for mates and similar good company.  About the room, some amazing coffee-coloured pictures varnished an inch deep, and some stuffed creatures in cases; dotted among the audience, in Sung and out of Snug, the ‘Professionals;’ among them, the celebrated comic favourite Mr. Banjo Bones, looking very hideous with his blackened face and limp sugar-loaf hat; beside him, sipping rum-and-water, Mrs. Banjo Bones, in her natural colours—a little heightened.

It was a Friday night, and Friday night was considered not a good night for Jack.  At any rate, Jack did not show in very great force even here, though the house was one to which he much resorts, and where a good deal of money is taken.  There was British Jack, a little maudlin and sleepy, lolling over his empty glass, as if he were trying to read his fortune at the bottom; there was Loafing Jack of the Stars and Stripes, rather an unpromising customer, with his long nose, lank cheek, high cheek-bones, and nothing soft about him but his cabbage-leaf hat; there was Spanish Jack, with curls of black hair, rings in his ears, and a knife not far from his hand, if you got into trouble with him; there were Maltese Jack, and Jack of Sweden, and Jack the Finn, looming through the smoke of their pipes, and turning faces that looked as if they were carved out of dark wood, towards the young lady dancing the hornpipe: who found the platform so exceedingly small for it, that I had a nervous expectation of seeing her, in the backward steps, disappear through the window.  Still, if all hands had been got together, they would not have more than half-filled the room.  Observe, however, said Mr. Licensed Victualler, the host, that it was Friday night, and, besides, it was getting on for twelve, and Jack had gone aboard.  A sharp and watchful man, Mr. Licensed Victualler, the host, with tight lips and a complete edition of Cocker’s arithmetic in each eye.  Attended to his business himself, he said.  Always on the spot.  When he heard of talent, trusted nobody’s account of it, but went off by rail to see it.  If true talent, engaged it.  Pounds a week for talent—four pound—five pound.  Banjo Bones was undoubted talent.  Hear this instrument that was going to play—it was real talent!  In truth it was very good; a kind of piano-accordion, played by a young girl of a delicate prettiness of face, figure, and dress, that made the audience look coarser.  She sang to the instrument, too; first, a song about village bells, and how they chimed; then a song about how I went to sea; winding up with an imitation of the bagpipes, which Mercantile Jack seemed to understand much the best.  A good girl, said Mr. Licensed Victualler.  Kept herself select.  Sat in Snug, not listening to the blandishments of Mates.  Lived with mother.  Father dead.  Once a merchant well to do, but over-speculated himself.  On delicate inquiry as to salary paid for item of talent under consideration, Mr. Victualler’s pounds dropped suddenly to shillings—still it was a very comfortable thing for a young person like that, you know; she only went on six times a night, and was only required to be there from six at night to twelve.  What was more conclusive was, Mr. Victualler’s assurance that he ‘never allowed any language, and never suffered any disturbance.’  Sharpeye confirmed the statement, and the order that prevailed was the best proof of it that could have been cited.  So, I came to the conclusion that poor Mercantile Jack might do (as I am afraid he does) much worse than trust himself to Mr. Victualler, and pass his evenings here.

But we had not yet looked, Mr. Superintendent—said Trampfoot, receiving us in the street again with military salute—for Dark Jack.  True, Trampfoot.  Ring the wonderful stick, rub the wonderful lantern, and cause the spirits of the stick and lantern to convey us to the Darkies.

There was no disappointment in the matter of Dark Jack; he was producible.  The Genii set us down in the little first floor of a little public-house, and there, in a stiflingly close atmosphere, were Dark Jack, and Dark Jack’s delight, his white unlovely Nan, sitting against the wall all round the room.  More than that: Dark Jack’s delight was the least unlovely Nan, both morally and physically, that I saw that night.

As a fiddle and tambourine band were sitting among the company, Quickear suggested why not strike up?  ‘Ah, la’ads!’ said a negro sitting by the door, ‘gib the jebblem a darnse.  Tak’ yah pardlers, jebblem, for ’um QUAD-rill.’

This was the landlord, in a Greek cap, and a dress half Greek and half English.  As master of the ceremonies, he called all the figures, and occasionally addressed himself parenthetically—after this manner.  When he was very loud, I use capitals.

‘Now den!  Hoy!  ONE.  Right and left.  (Put a steam on, gib ’um powder.)  LA-dies’ chail.  BAL-loon say.  Lemonade!  TWO.  AD-warnse and go back (gib ’ell a breakdown, shake it out o’ yerselbs, keep a movil).  SWING-corners, BAL-loon say, and Lemonade!  (Hoy!)  THREE.  GENT come for’ard with a lady and go back, hoppersite come for’ard and do what yer can.  (Aeiohoy!)  BAL-loon say, and leetle lemonade.  (Dat hair nigger by ’um fireplace ’hind a’ time, shake it out o’ yerselbs, gib ’ell a breakdown.)  Now den!  Hoy!  FOUR!  Lemonade.  BAL-loon say, and swing.  FOUR ladies meet in ’um middle, FOUR gents goes round ’um ladies, FOUR gents passes out under ’um ladies’ arms, SWING—and Lemonade till ‘a moosic can’t play no more!  (Hoy, Hoy!)’

The male dancers were all blacks, and one was an unusually powerful man of six feet three or four.  The sound of their flat feet on the floor was as unlike the sound of white feet as their faces were unlike white faces.  They toed and heeled, shuffled, double-shuffled, double-double-shuffled, covered the buckle, and beat the time out, rarely, dancing with a great show of teeth, and with a childish good-humoured enjoyment that was very prepossessing.  They generally kept together, these poor fellows, said Mr. Superintendent, because they were at a disadvantage singly, and liable to slights in the neighbouring streets.  But, if I were Light Jack, I should be very slow to interfere oppressively with Dark Jack, for, whenever I have had to do with him I have found him a simple and a gentle fellow.  Bearing this in mind, I asked his friendly permission to leave him restoration of beer, in wishing him good night, and thus it fell out that the last words I heard him say as I blundered down the worn stairs, were, ‘Jebblem’s elth!  Ladies drinks fust!’

The night was now well on into the morning, but, for miles and hours we explored a strange world, where nobody ever goes to bed, but everybody is eternally sitting up, waiting for Jack.  This exploration was among a labyrinth of dismal courts and blind alleys, called Entries, kept in wonderful order by the police, and in much better order than by the corporation: the want of gaslight in the most dangerous and infamous of these places being quite unworthy of so spirited a town.  I need describe but two or three of the houses in which Jack was waited for as specimens of the rest.  Many we attained by noisome passages so profoundly dark that we felt our way with our hands.  Not one of the whole number we visited, was without its show of prints and ornamental crockery; the quantity of the latter set forth on little shelves and in little cases, in otherwise wretched rooms, indicating that Mercantile Jack must have an extraordinary fondness for crockery, to necessitate so much of that bait in his traps.

Among such garniture, in one front parlour in the dead of the night, four women were sitting by a fire.  One of them had a male child in her arms.  On a stool among them was a swarthy youth with a guitar, who had evidently stopped playing when our footsteps were heard.

‘Well I how do you do?’ says Mr. Superintendent, looking about him.

‘Pretty well, sir, and hope you gentlemen are going to treat us ladies, now you have come to see us.’

‘Order there!’ says Sharpeye.

‘None of that!’ says Quickear.

Trampfoot, outside, is heard to confide to himself, ‘Meggisson’s lot this is.  And a bad ’un!’

‘Well!’ says Mr. Superintendent, laying his hand on the shoulder of the swarthy youth, ‘and who’s this?’

‘Antonio, sir.’

‘And what does he do here?’

‘Come to give us a bit of music.  No harm in that, I suppose?’

‘A young foreign sailor?’

‘Yes.  He’s a Spaniard.  You’re a Spaniard, ain’t you, Antonio?’

‘Me Spanish.’

‘And he don’t know a word you say, not he; not if you was to talk to him till doomsday.’  (Triumphantly, as if it redounded to the credit of the house.)

‘Will he play something?’

‘Oh, yes, if you like.  Play something, Antonio.  You ain’t ashamed to play something; are you?’

The cracked guitar raises the feeblest ghost of a tune, and three of the women keep time to it with their heads, and the fourth with the child.  If Antonio has brought any money in with him, I am afraid he will never take it out, and it even strikes me that his jacket and guitar may be in a bad way.  But, the look of the young man and the tinkling of the instrument so change the place in a moment to a leaf out of Don Quixote, that I wonder where his mule is stabled, until he leaves off.

I am bound to acknowledge (as it tends rather to my uncommercial confusion), that I occasioned a difficulty in this establishment, by having taken the child in my arms.  For, on my offering to restore it to a ferocious joker not unstimulated by rum, who claimed to be its mother, that unnatural parent put her hands behind her, and declined to accept it; backing into the fireplace, and very shrilly declaring, regardless of remonstrance from her friends, that she knowed it to be Law, that whoever took a child from its mother of his own will, was bound to stick to it.  The uncommercial sense of being in a rather ridiculous position with the poor little child beginning to be frightened, was relieved by my worthy friend and fellow-constable, Trampfoot; who, laying hands on the article as if it were a Bottle, passed it on to the nearest woman, and bade her ‘take hold of that.’  As we came out the Bottle was passed to the ferocious joker, and they all sat down as before, including Antonio and the guitar.  It was clear that there was no such thing as a nightcap to this baby’s head, and that even he never went to bed, but was always kept up—and would grow up, kept up—waiting for Jack.

Later still in the night, we came (by the court ‘where the man was murdered,’ and by the other court across the street, into which his body was dragged) to another parlour in another Entry, where several people were sitting round a fire in just the same way.  It was a dirty and offensive place, with some ragged clothes drying in it; but there was a high shelf over the entrance-door (to be out of the reach of marauding hands, possibly) with two large white loaves on it, and a great piece of Cheshire cheese.

‘Well!’ says Mr. Superintendent, with a comprehensive look all round.  ‘How do you do?’

‘Not much to boast of, sir.’  From the curtseying woman of the house.  ‘This is my good man, sir.’

‘You are not registered as a common Lodging House?’

‘No, sir.’

Sharpeye (in the Move-on tone) puts in the pertinent inquiry, ‘Then why ain’t you?’

‘Ain’t got no one here, Mr. Sharpeye,’ rejoin the woman and my good man together, ‘but our own family.’

‘How many are you in family?’

The woman takes time to count, under pretence of coughing, and adds, as one scant of breath, ‘Seven, sir.’

But she has missed one, so Sharpeye, who knows all about it, says:

‘Here’s a young man here makes eight, who ain’t of your family?’

‘No, Mr. Sharpeye, he’s a weekly lodger.’

‘What does he do for a living?’

The young man here, takes the reply upon himself, and shortly answers, ‘Ain’t got nothing to do.’

The young man here, is modestly brooding behind a damp apron pendent from a clothes-line.  As I glance at him I become—but I don’t know why—vaguely reminded of Woolwich, Chatham, Portsmouth, and Dover.  When we get out, my respected fellow-constable Sharpeye, addressing Mr. Superintendent, says:

‘You noticed that young man, sir, in at Darby’s?’

‘Yes.  What is he?’

‘Deserter, sir.’

Mr. Sharpeye further intimates that when we have done with his services, he will step back and take that young man.  Which in course of time he does: feeling at perfect ease about finding him, and knowing for a moral certainty that nobody in that region will be gone to bed.

Later still in the night, we came to another parlour up a step or two from the street, which was very cleanly, neatly, even tastefully, kept, and in which, set forth on a draped chest of drawers masking the staircase, was such a profusion of ornamental crockery, that it would have furnished forth a handsome sale-booth at a fair.  It backed up a stout old lady—HOGARTH drew her exact likeness more than once—and a boy who was carefully writing a copy in a copy-book.

‘Well, ma’am, how do you do?’

Sweetly, she can assure the dear gentlemen, sweetly.  Charmingly, charmingly.  And overjoyed to see us!

‘Why, this is a strange time for this boy to be writing his copy.  In the middle of the night!’

‘So it is, dear gentlemen, Heaven bless your welcome faces and send ye prosperous, but he has been to the Play with a young friend for his diversion, and he combinates his improvement with entertainment, by doing his school-writing afterwards, God be good to ye!’

The copy admonished human nature to subjugate the fire of every fierce desire.  One might have thought it recommended stirring the fire, the old lady so approved it.  There she sat, rosily beaming at the copy-book and the boy, and invoking showers of blessings on our heads, when we left her in the middle of the night, waiting for Jack.

Later still in the night, we came to a nauseous room with an earth floor, into which the refuse scum of an alley trickled.  The stench of this habitation was abominable; the seeming poverty of it, diseased and dire.  Yet, here again, was visitor or lodger—a man sitting before the fire, like the rest of them elsewhere, and apparently not distasteful to the mistress’s niece, who was also before the fire.  The mistress herself had the misfortune of being in jail.

Three weird old women of transcendent ghastliness, were at needlework at a table in this room.  Says Trampfoot to First Witch, ‘What are you making?’  Says she, ‘Money-bags.’

‘What are you making?’ retorts Trampfoot, a little off his balance.

‘Bags to hold your money,’ says the witch, shaking her head, and setting her teeth; ‘you as has got it.’

She holds up a common cash-bag, and on the table is a heap of such bags.  Witch Two laughs at us.  Witch Three scowls at us.  Witch sisterhood all, stitch, stitch.  First Witch has a circle round each eye.  I fancy it like the beginning of the development of a perverted diabolical halo, and that when it spreads all round her head, she will die in the odour of devilry.

Trampfoot wishes to be informed what First Witch has got behind the table, down by the side of her, there?  Witches Two and Three croak angrily, ‘Show him the child!’

She drags out a skinny little arm from a brown dustheap on the ground.  Adjured not to disturb the child, she lets it drop again.  Thus we find at last that there is one child in the world of Entries who goes to bed—if this be bed.

Mr. Superintendent asks how long are they going to work at those bags?

How long?  First Witch repeats.  Going to have supper presently.  See the cups and saucers, and the plates.

‘Late?  Ay!  But we has to ’arn our supper afore we eats it!’  Both the other witches repeat this after First Witch, and take the Uncommercial measurement with their eyes, as for a charmed winding-sheet.  Some grim discourse ensues, referring to the mistress of the cave, who will be released from jail to-morrow.  Witches pronounce Trampfoot ‘right there,’ when he deems it a trying distance for the old lady to walk; she shall be fetched by niece in a spring-cart.

As I took a parting look at First Witch in turning away, the red marks round her eyes seemed to have already grown larger, and she hungrily and thirstily looked out beyond me into the dark doorway, to see if Jack was there.  For, Jack came even here, and the mistress had got into jail through deluding Jack.

When I at last ended this night of travel and got to bed, I failed to keep my mind on comfortable thoughts of Seaman’s Homes (not overdone with strictness), and improved dock regulations giving Jack greater benefit of fire and candle aboard ship, through my mind’s wandering among the vermin I had seen.  Afterwards the same vermin ran all over my sleep.  Evermore, when on a breezy day I see Poor Mercantile Jack running into port with a fair wind under all sail, I shall think of the unsleeping host of devourers who never go to bed, and are always in their set traps waiting for him.