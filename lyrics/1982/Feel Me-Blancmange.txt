Feel me now
Feel the pain
Take the blame
Feel the strain

Before a word was spoken
My heart was broken

Feel me now
Feel the pain
Take the blame

Feel me now
Feel the pain
Take the blame
Feel the same

Before a word was spoken
My heart was broken

Feel me now
Feel the pain
Take the blame

Feel me now
Feel the pain

Wait a minute
Stop! Stop!
Here comes a love song
There goes the banister!

Feel me now (x2)

Find a plate glass window waiting for you
And you and you
Ohhh

Feel me now (x2)

Wait a minute
I'm going to start again
One! Two! Three! Four!

Put your hands in the pocket
The pocket of a friend
What do you feel?
Tell me now

Before a word was spoken
My heart was broken
Won't you feel me now?
Won't you feel the pain?

Here comes a love song
There goes a banister
(repeated 'til the song fades)