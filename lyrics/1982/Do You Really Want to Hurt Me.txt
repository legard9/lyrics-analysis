[Hook]
This ain't a good time
But when is it ever
I know the perfect time
And baby that's never
So don't you dare leave me now
Throw my heart on the ground
Cause tonight ain't the night for sorrow
But you can hurt me tomorrow

Ok, it's on you
Aha, ok

[Verse 1]
I used to be a strange fruit, Billy Holiday
Then you got me by my roots, took the pain away
I tried to question our direction, that was my mistake
I had to ask you where we going baby, Marvin Gaye
I used to do it like Sinatra, do it my way
Now I'm the Fugee of my Lauren Hill that got away
I though we had an at last love, Etta James
But now I'm wondering...

If what I heard it's true
Then I know what you came to do
Love may be blind but I'm looking at you
So before you pull the trigger, did you ever consider

[Hook]
This ain't a good time
But when is it ever
I know the perfect time
And baby that's never
So don't you dare leave me now
Throw my heart on the ground
Cause tonight ain't the night for sorrow
But you can hurt me tomorrow

You can hurt me...
You can hurt me tomorrow...
You can hurt me tomorrow girl
Save it for tomorrow

[Verse 2]
If you can take a rain check on a stormy night
Then I will love you till you're old, like Betty White
You can hurt me any other day, pick a fight
But not on Monday, Tuesday, Wednesday, Thursday, Friday night
And not the weekend either cause I got a song to write
I promise I'mma hear you out when the time is right
Let's have a talk, August 7th, 2099
At your place or mine?

[Hook]
This ain't a good time
But when is it ever
I know the perfect time
And baby that's never
So don't you dare leave me now
Throw my heart on the ground
Cause tonight ain't the night for sorrow
But you can hurt me tomorrow

Hey why you turn around and walk away
I know you got a lot to say
And I really want to talk about it
Just not today
Just not today


[Verse 3]
Is there an instrument to measure all the heart ache in
A looking glass so we can see where all the magic went
I need a button, I can push so we can start again
Cause girl you bring me to my knees, Nancy Kerrigan
This ain't a good time
But when is it ever

[Hook]
This ain't a good time
But when is it ever
I know the perfect time
And baby that's never
So don't you dare leave me now
Throw my heart on the ground
Cause tonight ain't the night for sorrow
But you can hurt me tomorrow

You can hurt me...
You can hurt me tomorrow...
You can hurt me tomorrow girl
Save it for tomorrow