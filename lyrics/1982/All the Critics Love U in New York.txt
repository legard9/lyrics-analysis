This timeline is organized around the contrast between hot and cold. Hot and cold language, hot and cold writing style, the idea of hot and cold conflict and authors' hot and/or cold confrontation of anger and injustice. In some cases, the author explicitly references hot/cold, "there are cold holes and warm holes. Mine is a warm hole" (Invisible Man). Other authors evoke a mood of hot or cold stylistically, for example, with the use of repetition (for emotional emphasis (hot) or to stress a kind of complacency (cold)), and the use of shocking language (hot). Some writers use the contrast of hot and cold to talk about outward conflict versus inner conflict. Overall, cold tends to be characterized as negative, while hot or warm is represented as positive. However, there are a some exceptions (when pieces seem to disdain rushing to a particular judgement. Ex: The narrator of Black Boy thinks the Negro Communists are too hot, too quick to pass judgement and move to action. So much so that they have actually been "frozen").
Understanding the dichotomy between hot and cold found during this period can help give readers a framework to talk about the ways two very stylistically different authors can both be radical. It can also give readers a broader understanding of how the canon of African-American literature (during this time period, but ostensibly as a whole) treated the subjects of conflict, anger and injustice.

*It is interesting to note that during this time period (1940-1980), America was always involved in at least one war, whether cold or hot. From World War II (1941-1945), the Korean War (1950-1953), and the Vietnam War (1965-1973), to the Cold War (1945-1989).


1940 - Langston Hughes: The Big Sea, Harlem Literati

"During the summer of 1926, Wallace Thurman, Zora Neale Hurston, Aaron Douglas, John P. Davis, Bruce Nugent, Gwendolyn Bennett, and I decided to publish 'a Negro quarterly of the arts' to be called Fire—the idea being that it would burn up a lot of the old, dead conventional Negro-white ideas of the past, épater le bourgeois into a realization of the existence of the younger Negro writers and artists....

None of the older Negro intellectuals would have anything to do with Fire. Dr. DuBois in the Crisis roasted it. The Negro press called it all sorts of bad names....Rean Graves, the critic for the Baltimore Afro-American, began his reviews by saying: 'I have just tossed the first issue of Fire into the fire.' Commenting upon various of our contributors, he said: 'Aaron Douglas who, in spite of himself and the meaningless grotesqueness of his creations, has gained a reputation as an artist, is permitted to spoil three perfectly good pages and a cover with his pen and ink hudge pudge....Langston Hughes displays his usually ability to say nothing in many words.'
So Fire had plenty of cold water thrown on it by the colored critics."

1942 - Margaret Walker: For My People

For my people everywhere singing their slave songs
repeatedly: their dirges and their ditties and their blues
and jubilees, praying their prayers nightly to an
unknown god, bending their knees humbly to an
unseen power;

For my people lending their strength to the years, to the
gone years and the now years and the maybe years,
washing ironing cooking scrubbing sewing mending
hoeing plowing digging planting pruning patching
dragging along never gaining never reaping never
knowing and never understanding;

For my playmates in the clay and dust and sand of Alabama
backyards playing baptizing and preaching and doctor
and jail and soldier and school and mama and cooking
and playhouse and concert and store and hair and
Miss Choomby and company;

For the cramped bewildered years we went to school to learn
to know the reasons why and the answers to and the
people who and the places where and the days when, in
memory of the bitter hours when we discovered we
were black and poor and small and different and nobody
cared and nobody wondered and nobody understood;

For the boys and girls who grew in spite of these things to
be man and woman, to laugh and dance and sing and
play and drink their wine and religion and success, to
marry their playmates and bear children and then die
of consumption and anemia and lynching;

For my people thronging 47th Street in Chicago and Lenox
Avenue in New York and Rampart Street in New
Orleans, lost disinherited dispossessed and happy
people filling the cabarets and taverns and other
people’s pockets and needing bread and shoes and milk and
land and money and something—something all our own;

For my people walking blindly spreading joy, losing time
being lazy, sleeping when hungry, shouting when
burdened, drinking when hopeless, tied, and shackled
and tangled among ourselves by the unseen creatures
who tower over us omnisciently and laugh;

For my people blundering and groping and floundering in
the dark of churches and schools and clubs
and societies, associations and councils and committees and
conventions, distressed and disturbed and deceived and
devoured by money-hungry glory-craving leeches,
preyed on by facile force of state and fad and novelty, by
false prophet and holy believer;

For my people standing staring trying to fashion a better way
from confusion, from hypocrisy and misunderstanding,
trying to fashion a world that will hold all the people,
all the faces, all the adams and eves and their countless generations;

Let a new earth rise. Let another world be born. Let a
bloody peace be written in the sky. Let a second
generation full of courage issue forth; let a people
loving freedom come to growth. Let a beauty full of
healing and a strength of final clenching be the pulsing
in our spirits and our blood. Let the martial songs
be written, let the dirges disappear. Let a race of men now
rise and take control.

-------Melvin B. Tolson: Dark Symphony

"They tell us to forget
The Golgotha we tread. . .
We show are scourged with hate,
A price upon our head.
They who have shackled us
Require of us a song,
They who have wasted us
Bid us condone the wrong.

They tell us to forget
Democracy is spurned.
They tell is to forget
The Bill of Rights is burned.
Three hundred years we slaved,
We slave and suffer yet:
Though flesh and bone rebel,
They tell us to forget!

Oh, how can we forget
Our human rights denied?
Oh, how can we forget
Our manhood crucified?
When Justice is profaned
And plea with curse is met,
When Freedom's gates are barred,
Oh, how can we forget?....

The New Negro
Breaks the icons of his detractors,
Wipes out the conspiracy of silence,
Speaks to his America....

The New Negro,
Hard-muscled, Fascist-hating, Democracy ensouled,
Strides in seen-league boots
Along the Highway of Today
Toward the Promised Land of Tomorrow!...

VI. Tempo di Marcia
Out f abysses of Illiteracy,
Through the labyrinths of Lies,
Across the waste lands of Disease . . .
We advance!

Out of the dead-ends of Poverty,
Through the wildernesses of Superstition,
Across the barricades of Jim Crowism . . .
We advance!

With the Peoples of the World . . .
We advance!

1945 - Richard Wright: Black Boy

"H. L. Mencken. I knew by hearsay that he was the editor of the American Mercury, but aside from that I knew nothing about his. The article was a furious denunication  of Mencken, concluding with one, hot, short sentence: Mencken is a fool....

I was jarred and shocked by the style, the clear, clean, sweeping sentences. Why did he write like that? And how did one write like that? I pictured the man as a raging demon, slashing with his pen, consumed with hate, denouncing everything American, extolling everything European or German, laughing at the weaknesses of people, mocking God, authority. What was this? I stood up, trying to realize what reality lay behind the meaning of the words . . . Yes, this man was fighting, fighting with words. He was using words as a weapon, using them as one would use a club. Could words be weapons? Well, yes, for there they were. Then, maybe, perhaps, I could use them as a weapon? No. It frightened me. I read on and what amazed me was not what he said, but how on earth anybody had the courage to say it....

I had once tried to write, had once reveled in feeling, had let my crude imagination roam, but the impulse to dream had been slowly beaten out of me by experience. Now it surged up again and I hungered for books, new ways of looking and seeing....

I forged more notes and my trips to the library became frequent. Reading grew into a passion....

I read Dreiser's Jenner Gerhardt and Sister Carrie and they revived in me a vivid sense of my mother's suffering; I was overwhelmed....

I now knew what being a Negro meant. I could endure the hunger. I had learned to live with hate. But to feel that there were feelings denied me, that the very breath of life itself was beyond my reach, that more than anything else hurt, wounded me. I had a new hunger.
In bouying me up, reading also cast me down, made me see what was possible, what I had missed. My tension returned, new, terrible, bitter, surging, almost too great to be contained. I no longer felt that the world about me was hostile, killing; I knew it.....

I smiled each day, fighting desperately to maintain my old behavior, to keep my disposition sunny. But some of the white men discerned that I had begun to brood....

I knew of no Negroes who read the books I liked and I wondered if any Negroes ever thought of them. I knew that there were Negro doctors, lawyers, newspapermen, but I never saw any of them. When I read a Negro newspaper I never caught the faintest echo of my preoccupation in its pages. I felt trapped and occasionally, for a few days, I would stop reading. But a vague hunger would come over me for books, books that opened up new avenues of feeling and seeing, and again I would forge another note to the white librarian. Again I would read and wonder as only the naive and unlettered can read and wonder, feeling that I carried a secret, criminal burden about with me each day....
I told none of the white men on the job that I was planning to go north; I knew that the moment they felt I was thinking of the North they would change toward me. I would have made them feel that I did not like the life I was living, and because my life was completely conditioned by what they said or did, it would have been tantamount to challenging them....

I could drain off my restlessness by fighting with Shorty and Harrison. I had seen many Negroes solve the problem of being black by transferring their hatred of themselves to others with a black skin and fighting them. I would have to be cold to do that, and I was not cold and I could never be....

It was when the Garveyites spoke fervently of building there own country, of someday living within the boundaries of a culture of their own making, that I sensed the passionate hunger of their lives, that I caught a glimpse of the potential strength of the American Negro....

Their [Negro Communists'] emotional certainty seemed buttressed by access to a fund of knowledge denied to ordinary men, but a day's observation of their activities was sufficient to reveal all their thought processes....Communism, instead of making them leap forward with fire in their hearts to become masters of ideas and life, had frozen them at an even lower level of ignorance than had been theirs before they met Communism."

1952 - Ralph Ellison: Invisible Man

"The point now is that I found a home--or a hole in the ground, as you will. Now don't jump to the conclusion that because I call my home a "hole" it is damp and cold like a grave; there are cold holes and warm holes. Mine is a warm hole. And remember, a bear retires to his hole for the winter and lives until spring; then he comes strolling out like the Easter chick breaking from its shell. I say all this to assure you that it is incorrect to assume that because I'm invisible and live in a hole, I am dead. I am neither dead nor in a state of suspended animation. all me Jack-the-Bear, for I am in a state of hibernation."


1969 - Amiri Baraka:  Black Art

Poems are bullshit unless they are
teeth or trees or lemons piled
on a step. Or black ladies dying
of men leaving nickel hearts
beating them down. Fuck poems
and they are useful, wd they shoot
come at you, love what you are,
breathe like wrestlers, or shudder
strangely after pissing. We want live
words of the hip world live flesh &
coursing blood. Hearts Brains
Souls splintering fire. We want poems
like fists beating niggers out of Jocks
or dagger poems in the slimy bellies
of the owner-jews. Black poems to
smear on girdlemamma mulatto bitches
whose brains are red jelly stuck
between 'lizabeth taylor's toes. Stinking
Whores! we want "poems that kill."
Assassin poems, Poems that shoot
guns. Poems that wrestle cops into alleys
and take their weapons leaving them dead
with tongues pulled out and sent to Ireland. Knockoff
poems for dope selling wops or slick halfwhite
politicians Airplane poems, rrrrrrrrrrrrrrrr
rrrrrrrrrrrrrrr . . .tuhtuhtuhtuhtuhtuhtuhtuhtuhtuh
. . .rrrrrrrrrrrrrrrr . . . Setting fire and death to
whities ass. Look at the Liberal
Spokesman for the jews clutch his throat
& puke himself into eternity . . . rrrrrrrr
There's a negroleader pinned to
a bar stool in Sardi's eyeballs melting
in hot flame Another negroleader
on the steps of the white house one
kneeling between the sheriff's thighs
negotiating coolly for his people.
Aggh . . . stumbles across the room . . .
Put it on him, poem. Strip him naked
to the world! Another bad poem cracking
steel knuckles in a jewlady's mouth
Poem scream poison gas on beasts in green berets
Clean out the world for virtue and love,
Let there be no love poems written
until love can exist freely and
cleanly. Let Black people understand
that they are the lovers and the sons
of warriors and sons
of warriors Are poems & poets &
all the loveliness here in the world

We want a black poem. And a
Black World.
Let the world be a Black Poem
And Let All Black People Speak This Poem
Silently
or LOUD

------Haki R. Madhubuti: a poem to complement other poems

"change.
life if u were a match i wd light i unto something beautiful.
change.
change.
for the better into a realreal together thing. change, from a
make believe
nothing on corn meal and water. change.
change. from the last drop to the first, maxwellhouse
did. change.
change from a programmer for IBM, thought him was a
brown computer. change.
colored is something written on southern out-houses.
change.
greyhound did., i mean they got rest rooms on buses.
change.
change.
change nigger.
saw a nigger hippy, him wanted to be different. changed.
saw a nigger liberal, him wanted to be different.
changed.
saw a nigger conservative, him wanted to be different.
changed.
niggers don't u know that niggers are different. change.
a doublechange. nigger wanted a double zero in front of
his name; a license to kill,
niggers are licensed to be killed. change. a negro: some-
thing pigs eat.
change. i say change into a realblack righteous aim. like i
don't play
saxophone but that doesn't mean i don't dig 'trane.'
change.
change.
hear u coming but yr / steps are too loud. change. even a
lamp post changes nigger.
change, stop being an instant yes machine. change.
niggers don't change they just grow. that's a change;
bigger and better niggers.
change, into a necessary blackself.
change, like a gas meter gets higher.
change, like a blues song talking about a righteous to-
morrow.
change, like a tax bill getting higher.
change, like a good sister getting better.
change, like knowing wood will burn, change.
know the realenemy.
change,
change nigger, standing on the corner, thought him was
cool. him still
standing there. it's winter time, him cool.
change,
know the realenemy.
change: him wanted to be a TV star. him is. ten o'clock
news.
wanted, wanted. nigger stole some lemon & lime
popsicles,
thought them were diamonds.
change nigger change.
know the realenemy.
change: is u is or is u aint. change. now  now change. for
the better change.
read a change. live a change. read a blackpoem.
change. be the realpeople.
change. blackpoems
will change:
know the realenemy. change. know the realenemy. change
yr / enemy change know the real
change know the realenemy change, change, know the
realenemy, the realenemy, the real
realenemy change  your the enemies / change your change
your change your enemy change
your enemy. know the realenemy, the world's enemy.
know them know them know them the
realenemy    change your enemy    change your change
change change your enemy change change
change change your change change change,
your
mind nigger.

------Mari Evans: I Am a Black Woman

I am a black woman
the music of my song
some sweet arpeggio of tears
is written in a minor key
and I
can be heard humming in the night

Can be heard
humming
in the night.

I saw my mate leap screaming to the sea
and I / with these hands / cupped the lifebreath
from my issue in the canebrake
I lost Nat's swinging body in a rain of tears
and heard my son scream all the way from Anzio
for Peace he never knew . . . . I
learned Da Nang and Pork Chop Hill
in anguish
Now my nostrils know the gas
and these trigger tire / d fingers
seek the softness in my warrior's beard

I
am a black woman
tall as a cypress
strong
beyond all definition still
defying place
and time
and circumstance
assailed
impervious
indestructible
Look
on me and be
renewed

1970 - Robert Hayden: El-Hajj Malik El-Shabazz
(Malcolm X)

The icy evil that struck his father down
and ravished his mother into madness
trapped him in violence of a punished self
struggling to break free....

Sometimes the dark that gave his life
its cold satanic sheen would shift
a little, and he saw himself
floodlit and eloquent:

yet how could he, "Satan" in The Hole.
guess what the waking dream foretold?...

He X'd his name, became his people's anger,
echoed them to vengeance for their past;
rebuked, admonished them,

their scourger who
would shame the, drive them from
the lush ice gardens of their servitude....

Time. "The martyr's time," he said.
Time and the karate killer,
knifer, gunman. Time that brought
ironic trophies as his faith

twined sparking round the bole,
the fruit of neo-Islam.
"The martyr's time."...

He fell upon his face before
Allah the faceless in whose blazing Oneness all
were one. He rose renewed renamed, became
much more than there was time for him to be.

1978 - Maya Angelou: Still I Rise

You may write me down in history
With your bitter, twisted lies,
You may trod me down in the very dirt
But still, like dust, I'll rise.

Does my sassiness upset you?
Why are you beset with gloom?
'Cause I walk like I've got oil wells
Pumping in my living room.

Just like moons and like suns,
With the certainty of tides,
Just like hopes springing high,
Still I'll rise.

Did you want to see me broken?
Bowed head and lowered eyes?
Shoulders falling down like teardrops,
Weakened by my soulful cries.

Does my haughtiness offend you?
Don't take it awful hard
'Cause I laugh like I've got gold mines
Diggin' in my own back yard.

You may shoot me with your words,
You may cut me with your eyes,
You may kill me with your hatefulness,
But still, like air, I'll rise.

Does my sexiness upset you?
Does it come as a surprise
That I dance like I've got diamonds
At the meeting of my thighs?

Out of the huts of history's shame
I rise
Up from a past that's rooted in pain
I rise
I'm a black ocean leaping and wide,
Welling and swelling I bear in the tide.
Leaving behind nights of terror and fear
I rise
Into daybreak that's wondrously clear
I rise
Bringing the gifts that my ancestors gave,
I am the dream and the hope of the slave.
I rise
I rise
I rise.

1980 -June Jordan: Poem about My Rights

Even tonight and I need to take a walk and clear
my head about this poem about why I can't
go out without changing my clothes my shoes
my body posture my gender identity my age
my status as a woman alone in the evening /
alone on the streets / alone not being the point /
the point being that I can't do what I want
to do with my own body because I am the wrong
sex the wrong age the wrong skin and
suppose it was not here in the city but down on the beach /
or far into the woods and I wanted to go
there by myself thinking about God / or thinking
about children or thinking about the world / all of it
disclosed by the stars and the silence:
I could not go and I could not think and I could not
stay there
alone
as I need to be
alone because I can't do what I want to do with my own
body and
who in the hell set things up
like this
and in France they say if the guy penetrates
but does not ejaculate then he did not rape me
and if after stabbing him if after screams if
after begging the bastard and if even after smashing
a hammer to his head if even after that if he
and his buddies fuck me after that
then I consented and there was
no rape because finally you understand finally
they fucked me over because I was wrong I was
wrong again to be me being me where I was / wrong
to be who I am
which is exactly like South Africa
penetrating into Namibia penetrating into
Angola and does that mean I mean how do you know if
Pretoria ejaculates what will the evidence look like the
proof of the monster jackboot ejaculation on Backland
and if
after Namibia and if after Angola and if after Zimbabwe
and if after all of my kinsmen and women resist even to
self-immolation of the villages and if after that
we lose nevertheless what will the big boys say will they
claim my consent:
Do You Follow Me: We are the wrong people of the wrong skin on the wrong continent and what
in the hell is everybody being reasonable about
and according to the Times this week
back in 1966 the C.I.A. decided that they had this problem
and the problem was a man named Nkrumah so they
killled hime and before that it was Patrice Lumumba
and before that it was my father on the campus
of my Ivy League school and my father afraid
to walk into the cafeteria because he said he
was wrong the wrong age the wrong skin the wrong
gender identity and he was paying my tuition and
before that
it was my father saying I was wrong saying that
I should have been a boy because he wanted one / a
boy and that I should have been lighter skinned and
that I should have had straighter hair and that
I should not be so boy crazy but instead I should
just be one / a boy and before that
it was my mother pleading plastic surgery for
my nose and braces for my teeth and telling me
to let the books loose to let them loose in other
words
I am very familiar with the problems of the C.I.A.
and the problems of South Africa and the problems
of Exxon Corporation and the problems of white
America in general and the problems of the teachers
and the preachers and the F.B.I. and the social
workers and my particular Mom and Dad / I am very
familiar with the problems because the problems
turn out to be
me
I am the history of rape
I am the history of rejection of who I am
I am the history of the terrorized incarceration of
my self
I am the history of battery assault and limitless
armies against whatever I want to do with my mind
and my body and my soul and
whether it's about walking out at night
or whether it's about the love that feel or
whether it's about the sanctity of my vagina or
the sanctity of my national boundaries
or the sanctity of my leaders or the sanctity
of each and every desire
that I know from my personal and idiosyncratic
and indisputably single and singular heart
I have been raped
be-
cause I have been wrong the wrong sex the wrong age
the wrong skin the wrong nose the wrong hair the
wrong need the wrong dream the wrong geographic
the wrong sartorial I
I have been the meaning of rape
I have been the problem everyone seeks to
eliminate by forced
penetration with or without the evidence of slime and /
but let this be unmistakable this poem
is not consent I do not consent
to my mother to my father to the teachers to
the F.B.I. to South Africa to Bedford-Stuy
to Park Avenue to American Airlines to the pardon
idlers on the corners to the sneaky creeps in
cars
I am not wrong: Wrong is not my name
My name it my own my own my own
and I can't tell you who the hell set things up like this
but I can tell you that from now on my resistance
my simple and daily and nightly self-determination
may very well cost you your life