Well I gambled, played the game
Took my chances once again
Yes I'm always a fool for love when I'm feeling lonely
And I know he's caused me pain
But I need him just the same
'Cos I ain't got the strenght to say no when the nights are colder

Another heart made of stone
And a love I cannot own
And he cuts me to the bone
Another heart of stone

You know the fools who love and run
And I know I've been fooled by some
All those promises made disappear in the haze of the morning sun
I wanna take you in my arms
Wanna drink in all your charms
Even though it'd tear me apart when the night is over

Another heart made of stone
And a love I cannot own
And he cuts me to the bone
Another heart of stone
No matter what do I say
If I give it all away
Still won't make me stay
Another heart of stone
And if I wield the axe
Don't make him realise
Will he ever know the reason why I'm blue
Another heart made of stone
And a love I cannot own
And he cuts me to the bone
Another heart of stone

They tell me lovin' rules the night
But you know love ain't right
If it's given to one whose affection has no meaning
Still I gamble like before
And I'll take just a little bit more
'Cos your arms hold a thousand reasons to keep me dreaming

Another heart made of stone
And a love I cannot own
And he cuts me to the bone
Another heart of stone
And if I wield the axe
Don't make him realise
Will he ever know the reason why I'm blue

Another heart of stone
Another heart of stone