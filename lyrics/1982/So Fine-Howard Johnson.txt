[Intro]
Ooh…so fine
So fine, blow my mind
Ooh…so fine
So fine, blow my mind

[Verse 1]
When I saw you come inside
I couldn't help but notice you were alone tonight
Oh, you’re so fine
Can we dance, can we talk, can we spend some time?

[Pre-Chorus]
The way you strut (Strut)
Your (Your) stuff across the floor
Baby, I (I)
Can't (Can't)
Have you to myself

[Chorus]
Baby baby baby
So fine
So fine, blow my mind
Baby baby baby, I wanna make you mine
So fine
So fine, blow my mind
Baby baby baby
So fine
So fine, blow my mind
Baby baby baby, I wanna make you mine
So fine
So fine, blow my mind

[Verse 2]
Can we sit down and discuss
Possibilities for love tonight
Concentrate on just us
And releasing what we feel inside

[Pre-Chorus]
And when night (Night)
Ends (Ends)
Find a quiet place to be together
We'll take our (Our) time (Time)
Maybe it will last forever

[Chorus]
Baby baby baby
So fine
So fine, blow my mind
Baby baby baby, I wanna make you mine
So fine
So fine, blow my mind
Baby baby baby
So fine
So fine, blow my mind
Baby baby baby, I wanna make you mine
So fine
So fine, blow my mind

[Bridge]
Hold your head back
Lay it to the side
Hey girls, tell me that you’re fine
Hold your head back
Now close your eyes
Hey girls, tell me that you’re fine
Hold your head back
Lean it to the side
Hey fellas (Yeah), ain't she fine? (She fine)
Hold your head back back back back
Lean it to the side
Hey fellas (Yeah), ain't she fine? (She fine)

[Pre-Chorus]
The way you strut (Strut)
Your (Your) stuff across the floor
I really like it, baby
I (I)
Can't (Can't)
Have you to myself

[Outro Chorus]
Baby baby baby
So fine
So fine, blow my mind
Baby baby baby, I wanna make you mine
So fine
So fine, blow my mind
Baby baby baby
So fine
So fine, blow my mind
Ooh, na na na, na na na, na na na
So fine
So fine, blow my mind
Baby baby baby, I wanna make you mine
So fine
So fine, blow my mind
Baby baby baby
So fine
So fine, blow my mind
Baby baby baby, I wanna make you mine
So fine
So fine, blow my mind
Baby baby baby, I wanna make you mine
So fine
So fine, blow my mind
Baby baby baby
So fine
So fine, blow my mind
Baby baby baby, I wanna make you mine
So fine
So fine, blow my mind
Baby baby baby, I wanna make you mine
So fine
So fine, blow my mind
Baby baby baby
So fine
So fine, blow my mind