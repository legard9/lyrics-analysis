[Verse 1]
It's like a thousand trumpets Spottieottiedopa-dopa-liscious
It's like a million bass drums beating out bum-ba-baby-bum-bum
It's like the voice of angels, not too fast and not too slow
Hi-hats humming happiness, where my passion is
Now that's the sound of my soul
The MC orchestra going Bone Thugs all on the track, ah, ah huh
It's some of Aunt Nancy's voice in New York every time I come back
If Jimmy Paige would've played in a group with Snoop and it came out dope
Little this, little that, this is that, get the gat, do the track

[Chorus]
Now that's the sound of my soul
That's the sound of my soul
That's the sound of my soul
Baby, it sounds like love
So listen close to my soul
That's the sound of my soul

[Verse 2]
A little of my mom mixed up doing harmony over my pop's
I feel a lil' Piccolo player, she's echoing off of tree tops
A little J. Dilla, a little Sam Cook, that's when you sing in high notes

[Chorus]
That's the sound of my soul
That's the sound of my soul
That's the sound of my soul
Baby, it sounds like love
So listen close to my soul
That's the sound of my soul
(To the bridge)

[Bridge]
I could do anything I want to
My gift is my song, for you
Rumbling halos in this
If the universe had a singing voice
It would ride the beat like this
Like what?
Like this
Like what?
Like this
Like what?

[Chorus]
That's the sound of my soul
That's the sound of my soul
That's the sound of my soul
Baby, it sounds like love
So listen close to my soul
That's the sound of my soul