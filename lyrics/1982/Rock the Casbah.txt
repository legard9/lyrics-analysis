[Andy Cooper]
Don't drink, don't smoke, but what I do
Is wash and shampoo like Selsun Blue
Then I find a clue with Nancy Drew
And go oompa, loompa, doom-pa-dee-do (Yeah)
Tighter than Magnum P.I.'s oldest pair of Levi's
Throwing banana cream pies that splatter between eyes
Of any competitor, home skillet
You can feel it from your perm to your pedicure

[Dizzy Dustin] + (Andy Cooper)
Now you can be a housewife or a bachelorette (Yeah)
Or a little baby girl in a basinette (Uh-huh)
Maybe you're an underprivileged foster kid
Or a rich kid eatin with a lobster bib
We'll get your body moving till your feet start throbbin (Uh-huh)
And have you noggin bobbin like you're riding a toboggan (What?)
Sockin it to you as if I knew kung fu
On cue, to serve hors d'oeuvres and fondue

[Andy Cooper]
Add a little cherry cola and a bowl of tapioca
"We rock the Casbah," and sometimes the Copa
Young Einstein mix the mocha
And give them a taste of almond rocha

[Chorus: Andy Cooper]
Almond rocha, that's the cat's meow
Almond rocha, that's the cat's meon now
I said, almond rocha, that's the cat's meow
Almond rocha, that's the cat's meon, now
I said, almond rocha, that's the cat's meow
Almond rocha, that's the cat's meon, now
I said, almond rocha, that's the cat's meow
Almond rocha, that's the cat's meon, now
(Way out) you get down, you get down, you get down
(Way out) you get down, you get down, you get down

[Dizzy Dustin] + (Andy Cooper)
Sucker alligators get dropped in the water pot
Cooked on the spot till they're hot like a lava rock
I prepare a culinary delight
So you better mind your manners when I carry the mic (What?)
If you shoot the shilack, I put a boot in your back
You'll eat your words like alphabet soup or a snack
I'm the Love Bug Herbie at a demolition derby
With my rhyme, so break it down Einstein

[Andy Cooper]
Boba Fett had jets to outrace space cadets
Back when CD's replaced tape cassettes
I bought the hip hop not cheesy pop
Now I'm ready to rock like ZZ Top
So I blast raps until my mouth is full of gauze packs
Breathin harder than your mama did in her Lamaze class
To the jiggasaurus hidin the truth
Ugly Duckling is about to put the fly in your soup

[Dizzy Dustin]
I got a crocodile smile on the chocolate Nile
Actin like a jerk, that's not my style
I'm a joker, I'm a smoker, I'm a midnight toker
Why, oh why, did I eat almond rocha?

[Chorus: Andy Cooper]
Almond rocha, that's the cat's meow
Almond rocha, that's the cat's meon, now
I said, almond rocha, that's the cat's meow
Almond rocha, that's the cat's meon, now
I said, almond rocha, that's the cat's meow
Almond rocha, that's the cat's meon, now
I said, almond rocha, that's the cat's meow
Almond rocha, that's the cat's meon, now
(Way out) you get down, you get down, you get down
(Way out) you get down, you get down, you get down
(Way out) you get down, you get down, you get down
(Way out) you get down, you get down, you get down
It's like that y'all, and you do stop