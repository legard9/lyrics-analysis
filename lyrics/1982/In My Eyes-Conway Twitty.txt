Conway Twitty & Loretta Lynn

See that mountain over there
I'll move it for you
Aw I know you would
You know I'd move it for you

See the color of my hair
I'd change it for you
Hmmm, you'd do that for me
You know I'd change it for you

Baby, it's true love
And I've never had this feeling
It's true love
And I can't believe it's real

No more broken hearts for you
Girl I feel the same way too
'Cause it's true love
Brand new love with you

--- Instrumental ---

See that star up in the sky
I'll go get it for you
Aw honey, you don't have to do that
You know I'll get it for you

See these tears in my eyes
You're making me cry
Hey, did I do something wrong
'Cause I'm so happy inside

Baby, it's true love
It's a super-natural feeling
It's true love
And this time I know it's real

No more broken hearts for you
Girl, I feel the same way too

Baby, it's true love
And I've never had this feelin'
It's true love
And I can't believe it's real

No more broken hearts, for you
Girl, I feel the same way too

'Cause it's true love
It's a super-natural feeling
It's true love
And I can't believe it's real

It's true love
It's a super-natural feeling
It's true love
And this time I know it's real...