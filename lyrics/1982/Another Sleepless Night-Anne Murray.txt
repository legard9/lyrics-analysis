[Verse 1]
The nights have been so lonely since you went away
I could not get to sleep, try as I may
But now you're back and you're here to stay

[Pre-Chorus]
Still it looks like another sleepless night
Oh, but darling, that's alright
Just as long as you are holding me tight

[Bridge]
Making love 'til the early morning light
Only whets our appetite
This will be another sleepless night

[Chorus]
Hold me
While the moon shines in through the window
Let your love flow
Hold me
We can orchestrate love's scenario with the radio

[Pre-Chorus]
And it looks like another sleepless night
Oh, but darling, that's alright
Just as long as you are holding me tight

[Chorus]
Hold me
While the moon shines in through the window
Let your love flow
Hold me
We can orchestrate love's scenario with the radio

[Pre-Chorus]
And it looks like another sleepless night
Oh, but darling, that's alright
Just as long as you are holding me tight

[Outro]
Making love 'til the early morning light
Only whets our appetite
This will be another sleepless night