The shape was rectangular
The color was purple
And I was a kid
Spinning a globe around
Yeah, picking out places that I wanted to live
You grew up in a fairy tale world
But you can't spin the earth
And you can't figure it out
You're at home
You can tell when you see the bridges and bell
Pennsylvania's the home that you know that you can always come back to
Yeah, yeah, yeah, yeah . Woah
Belle Vernon!
Allentown and nanty glow, Red lion, Edinboro
We have a California and a jersey shore
Is Bethlehem where Jesus' was born?
Oh man, in nature at each other's necks
Birds in the east, and steel in the west
The lion on the mountain and the panthers pit
But it's friendly competition and that's just it
You're at home
You can tell when you see the bridges and bell
Pennsylvania's the home that you know that you can always come back to
We have 67 counties, and no dominant political party
The declaration of indy was signed out in Philly
And Pittsburgh has more bridges than any other city
Not counting Venice
You've got a friend in Pennsylvania
(I'm coming home)
You've got a friend in Pennsylvania
(Come back home, I'm coming home)
You've got a friend in Pennsylvania
(I'm coming home)
You've got a friend in Pennsylvania
(Come back home, I'm coming home)
You're at home
You can tell when you hear the liberty
Pennsylvania's the home that you know that you can always come back to
You've got a friend in Pennsylvania
You've got a friend in Pennsylvania