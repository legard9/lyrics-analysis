Her visitors had been gone half an hour, but she was still in the drawing-room when Nanda came back. The girl found her, on the sofa, in a posture that might have represented restful oblivion, but that, after a glance, our young lady appeared to interpret as mere intensity of thought. It was a condition from which at all events Mrs. Brook was quickly roused by her daughter's presence: she opened her eyes and put down her feet, so that the two were confronted as closely as persons may be when it is only one of them who looks at the other. Nanda, gazing vaguely about and not seeking a seat, slowly drew off her gloves while her mother's sad eyes considered her from top to toe. "Tea's gone," Mrs. Brook then said as if there were something in the loss peculiarly irretrievable. "But I suppose," she added, "he gave you all you want."
"Oh dear yes, thank you—I've had lots."

Nanda hovered there slim and charming, feathered and ribboned, dressed in thin fresh fabrics and faint colours, with something in the effect of it all to which the sweeter deeper melancholy in her mother's eyes seemed happily to testify. "Just turn round, dear." The girl immediately obeyed, and Mrs. Brook once more took everything in. "The back's best—only she didn't do what she said she would. How they do lie!" she gently quavered.

"Yes, but we lie so to THEM." Nanda had swung round again, producing evidently on her mother's part, by the admirable "hang" of her light skirts, a still deeper peace. "Do you mean the middle fold?—I knew she wouldn't. I don't want my back to be best—I don't walk backward."
"Yes," Mrs. Brook resignedly mused; "you dress for yourself."

"Oh how can you say that," the girl asked, "when I never stick in a pin but what I think of YOU!"

"Well," Mrs. Brook moralised, "one must always, I consider, think, as a sort of point de repere, of some one good person. Only it's best if it's a person one's afraid of. You do very well, but I'm not enough. What one really requires is a kind of salutary terror. I never stick in a pin without thinking of your Cousin Jane. What is it that some one quotes somewhere about some one's having said that 'Our antagonist is our helper—he prevents our being superficial'? The extent to which with my poor clothes the Duchess prevents ME—!" It was a measure Mrs. Brook could give only by the general soft wail of her submission to fate.

"Yes, the Duchess isn't a woman, is she? She's a standard."

The speech had for Nanda's companion, however, no effect of pleasantry or irony, and it was a mark of the special intercourse of these good friends that though they showed each other, in manner and tone, such sustained consideration as might almost have given it the stamp of diplomacy, there was yet in it also something of that economy of expression which is the result of a common experience. The recurrence of opportunity to observe them together would have taught a spectator that—on Mrs. Brook's side doubtless more particularly—their relation was governed by two or three remarkably established and, as might have been said, refined laws, the spirit of which was to guard against the vulgarity so often coming to the surface between parent and child. That they WERE as good friends as if Nanda had not been her daughter was a truth that no passage between them might fail in one way or another to illustrate. Nanda had gathered up, for that matter, early in life, a flower of maternal wisdom: "People talk about conscience, but it seems to me one must just bring it up to a certain point and leave it there. You can let your conscience alone if you're nice to the second housemaid." Mrs. Brook was as "nice" to Nanda as she was to Sarah Curd—which involved, as may easily be imagined, the happiest conditions for Sarah. "Well," she resumed, reverting to the Duchess on a final appraisement of the girl's air, "I really think I do well by you and that Jane wouldn't have anything to say to-day. You look awfully like mamma," she then threw off as if for the first time of mentioning it.

"Oh Cousin Jane doesn't care for that," Nanda returned. "What I don't look like is Aggie, for all I try."

"Ah you shouldn't try—you can do nothing with it. One must be what one is."

Mrs. Brook was almost sententious, but Nanda, with civility, let it pass. "No one in London touches her. She's quite by herself. When one sees her one feels her to be the real thing."

Mrs. Brook, without harshness, wondered. "What do you mean by the real thing?"

Even Nanda, however, had to think a moment.

"Well, the real young one. That's what Lord Petherton calls her," she mildly joked—"'the young 'un'"

Her mother's echo was not for the joke, but for something else. "I know what you mean. What's the use of being good?"

"Oh I didn't mean that," said Nanda. "Besides, isn't Aggie of a goodness—?"

"I wasn't talking of her. I was asking myself what's the use of MY being."

"Well, you can't help it any more than the Duchess can help—!"

"Ah but she could if she would!" Mrs. Brook broke in with a sharper ring than she had yet given. "We can't help being good perhaps, if that burden's laid on us—but there are lengths in other directions we're not absolutely obliged to go. And what I think of when I stick in the pins," she went on, "is that Jane seems to me really never to have had to pay." She appeared for a minute to brood on this till she could no longer bear it; after which she jerked out: "Why she has never had to pay for ANYthing!"

Nanda had by this time seated herself, taking her place, under the interest of their talk, on her mother's sofa, where, except for the removal of her long soft gloves, which one of her hands again and again drew caressingly through the other, she remained very much as if she were some friendly yet circumspect young visitor to whom Mrs. Brook had on some occasion dropped "DO come." But there was something perhaps more expressly conciliatory in the way she had kept everything on: as if, in particular serenity and to confirm kindly Mrs. Brook's sense of what had been done for her, she had neither taken off her great feathered hat nor laid down her parasol of pale green silk, the "match" of hat and ribbons and which had an expensive precious knob. Our spectator would possibly have found too much earnestness in her face to be sure if there was also candour. "And do you mean that YOU have had to pay—?"

"Oh yes—all the while." With this Mrs. Brook was a little short, and also as she added as if to banish a slight awkwardness: "But don't let it discourage you."

Nanda seemed an instant to weigh the advice, and the whole thing would have been striking as another touch in the picture of the odd want, on the part of each, of any sense of levity in the other. Whatever escape, face to face, mother or daughter might ever seek would never be the humorous one—a circumstance, notwithstanding, that would not in every case have failed to make their interviews droll for a third person. It would always indeed for such a person have produced an impression of tension beneath the surface. "I could have done much better at the start and have lost less time," the girl at last said, "if I hadn't had the drawback of not really remembering Granny."

"Oh well, I remember her!" Mrs. Brook moaned with an accent that evidently struck her the next moment as so much out of place that she slightly deflected. She took Nanda's parasol and held it as if—a more delicate thing much than any one of hers—she simply liked to have it. "Her clothes—at your age at least—must have been hideous. Was it at the place he took you to that he gave you tea?" she then went on.
"Yes, at the Museum. We had an orgy in the refreshment-room. But he took me afterwards to Tishy's, where we had another."
"He went IN with you?" Mrs. Brook had suddenly flashed into eagerness.

"Oh yes—I made him."

"He didn't want to?"

"On the contrary—very much. But he doesn't do everything he wants," said Nanda.

Mrs. Brook seemed to wonder. "You mean you've also to want it?"

"Oh no—THAT isn't enough. What I suppose I mean," Nanda continued, "is that he doesn't do anything he doesn't want. But he does quite enough," she added.

"And who then was at Tishy's?"

"Oh poor old Tish herself, naturally, and Carrie Donner."

"And no one else?"

The girl just waited. "Yes, Mr. Cashmore came in."

Her mother gave a groan of impatience. "Ah AGAIN?"

Nanda thought an instant. "How do you mean, 'again'? He just lives there as much as he ever did, and Tishy can't prevent him."

"I was thinking of Mr. Longdon—of THEIR meeting. When he met him here that time he liked it so little. Did he like it any more to-day?" Mrs. Brook quavered.

"Oh no, he hated it."

"But hadn't he—if he should go in—known he WOULD?"

"Yes, perfectly. But he wanted to see."

"To see—?" Mrs. Brook just threw out.

"Well, where I go so much. And he knew I wished it."

"I don't quite see why," Mrs. Brook mildly observed. And then as her daughter said nothing to help her: "At any rate he did loathe it?"
Nanda, for a reply, simply after an instant put a question. "Well, how can he understand?"

"You mean, like me, why you do go there so much? How can he indeed?"

"I don't mean that," the girl returned—"it's just that he understands perfectly, because he saw them all, in such an extraordinary way—well, what can I ever call it?—clutch me and cling to me."

Mrs. Brook, with full gravity, considered this picture. "And was Mr. Cashmore to-day so ridiculous?"

"Ah he's not ridiculous, mamma—he's very unhappy. He thinks now Lady Fanny probably won't go, but he feels that may be after all only the worse for him."

"She WILL go," Mrs. Brook answered with one of her roundabout approaches to decision. "He IS too great an idiot. She was here an hour ago, and if ever a woman was packed—!"

"Well," Nanda objected, "but doesn't she spend her time in packing and unpacking?"

This enquiry, however, scarce pulled up her mother. "No—though she HAS, no doubt, hitherto wasted plenty of labour. She has now a dozen boxes—I could see them there in her wonderful eyes—just waiting to be called for. So if you're counting on her not going, my dear—!" Mrs. Brook gave a head-shake that was the warning of wisdom.

"Oh I don't care what she does!" Nanda replied. "What I meant just now was that Mr. Longdon couldn't understand why, with so much to make them so, they couldn't be decently happy."

"And did he wish you to explain?"

"I tried to, but I didn't make it any better. He doesn't like them. He doesn't even care for Tish."

"He told you so—right out?"

"Oh," Nanda said, "of course I asked him. I didn't press him, because I never do—!"

"You never do?" Mrs. Brook broke in as with the glimpse of a new light.

The girl showed an indulgence for this interest that was for a moment almost elderly. "I enjoy awfully with him seeing just how to take him."
Her tone and her face evidently put forth for her companion at this juncture something freshly, even quite supremely suggestive; and yet the effect of them on Mrs. Brook's part was only a question so off-hand that it might already often have been asked. The mother's eyes, to ask it, we may none the less add, attached themselves closely to the daughter's, and her face just glowed. "You like him so very awfully?"
It was as if the next instant Nanda felt herself on her guard. Yet she spoke with a certain surrender. "Well, it's rather intoxicating to be one's self—!" She had only a drop over the choice of her term.

"So tremendously made up to, you mean—even by a little fussy ancient man? But DOESN'T he, my dear," Mrs. Brook continued with encouragement, "make up to you?"

A supposititious spectator would certainly on this have imagined in the girl's face the delicate dawn of a sense that her mother had suddenly become vulgar, together with a general consciousness that the way to meet vulgarity was always to be frank and simple and above all to ignore. "He makes one enjoy being liked so much—liked better, I do think, than I've ever been liked by any one."

If Mrs. Brook hesitated it was, however, clearly not because she had noticed. "Not better surely than by dear Mitchy? Or even if you come to that by Tishy herself."

Nanda's simplicity maintained itself. "Oh Mr. Longdon's different from Tishy."

Her mother again hesitated. "You mean of course he knows more?"

The girl considered it. "He doesn't know MORE. But he knows other things. And he's pleasanter than Mitchy."

"You mean because he doesn't want to marry you?"

It was as if she had not heard that Nanda continued: "Well, he's more beautiful."

"O-oh!" cried Mrs. Brook, with a drawn-out extravagance of comment that amounted to an impugnment of her taste even by herself.
It contributed to Nanda's quietness. "He's one of the most beautiful people in the world."

Her companion at this, with a quick wonder, fixed her. "DOES he, my dear, want to marry you?"

"Yes—to all sorts of ridiculous people."

"But I mean—would you take HIM?"

Nanda, rising, met the question with a short ironic "Yes!" that showed her first impatience. "It's so charming being liked without being approved."

But Mrs. Brook only wanted to know. "He doesn't approve—?"

"No, but it makes no difference. It's all exactly right—it doesn't matter."

Mrs. Brook seemed to wonder, however, exactly how these things could be. "He doesn't want you to give up anything?" She looked as if swiftly thinking what Nanda MIGHT give up.

"Oh yes, everything."

It was as if for an instant she found her daughter inscrutable; then she had a strange smile. "Me?"

The girl was perfectly prompt. "Everything. But he wouldn't like me nearly so much if I really did."

Her mother had a further pause. "Does he want to ADOPT you?" Then more quickly and sadly, though also a little as if lacking nerve to push the research: "We couldn't give you up, Nanda."

"Thank you so much, mamma. But we shan't be very much tried," Nanda said, "because what it comes to seems to be that I'm really what you may call adopting HIM. I mean I'm little by little changing him—gradually showing him that, as I couldn't possibly have been different, and as also of course one can't keep giving up, the only way is for him not to mind, and to take me just as I am. That, don't you see? is what he would never have expected to do."

Mrs. Brook recognised in a manner the explanation, but still had her wistfulness. "But—a—to take you, 'as you are,' WHERE?"

"Well, to the South Kensington Museum."

"Oh!" said Mrs. Brook. Then, however, in a more exemplary tone: "Do you enjoy so very much your long hours with him?"

Nanda appeared for an instant to think how to express it. "Well, we're great friends."

"And always talking about Granny?"

"Oh no—really almost never now."

"He doesn't think so awfully much of her?" There was an oddity of eagerness in the question—a hope, a kind of dash, for something that might have been in Nanda's interest.

The girl met these things only with obliging gravity. "I think he's losing any sense of my likeness. He's too used to it—or too many things that are too different now cover it up."

"Well," said Mrs. Brook as she took this in, "I think it's awfully clever of you to get only the good of him and have none of the worry."
Nanda wondered. "The worry?"

"You leave that all to ME," her mother went on, but quite forgivingly. "I hope at any rate that the good, for you, will be real."
"Real?" the girl, remaining vague, again echoed.

Mrs. Brook showed for this not perhaps an irritation, but a flicker of austerity. "You must remember we've a great many things to think about. There are things we must take for granted in each other—we must all help in our way to pull the coach. That's what I mean by worry, and if you don't have any so much the better for you. For me it's in the day's work. Your father and I have most to think about always at this time, as you perfectly know—when we have to turn things round and manage somehow or other to get out of town, have to provide and pinch, to meet all the necessities, with money, money, money at every turn running away like water. The children this year seem to fit into nothing, into nowhere, and Harold's more dreadful than he has ever been, doing nothing at all for himself and requiring everything to be done for him. He talks about his American girl, with millions, who's so awfully taken with him, but I can't find out anything about her: the only one, just now, that people seem to have heard of is the one Booby Manger's engaged to. The Mangers literally snap up everything," Mrs. Brook quite wailingly now continued: "the Jew man, so gigantically rich—who is he? Baron Schack or Schmack—who has just taken Cumberland House and who has the awful stammer—or what is it? no roof to his mouth—is to give that horrid little Algie, to do his conversation for him, four hundred a year, which Harold pretended to me that, of all the rush of young men—dozens!—HE was most in the running for. Your father's settled gloom is terrible, and I bear all the brunt of it; we get literally nothing this year for the Hovel, yet have to spend on it heaven knows what; and everybody, for the next three months, in Scotland and everywhere, has asked us for the wrong time and nobody for the right: so that I assure you I don't know where to turn—which doesn't however in the least prevent every one coming to me with their own selfish troubles." It was as if Mrs. Brook had found the cup of her secret sorrows suddenly jostled by some touch of which the perversity, though not completely noted at the moment, proved, as she a little let herself go, sufficient to make it flow over; but she drew, the next thing, from her daughter's stillness a reflexion of the vanity of such heat and speedily recovered herself as if in order with more dignity to point the moral. "I can carry my burden and shall do so to the end; but we must each remember that we shall fall to pieces if we don't manage to keep hold of some little idea of responsibility. I positively can't arrange without knowing when it is you go to him."

"To Mr. Longdon? Oh whenever I like," Nanda replied very gently and simply.

"And when shall you be so good as to like?"

"Well, he goes himself on Saturday, and if I want I can go a few days later."

"And what day can you go if I want?" Mrs. Brook spoke as with a small sharpness—just softened indeed in time—produced by the sight of a freedom in her daughter's life that suddenly loomed larger than any freedom of her own. It was still a part of the unsteadiness of the vessel of her anxieties; but she never after all remained publicly long subject to the influence she often comprehensively designated to others as well as to herself as "nastiness." "What I mean is that you might go the same day, mightn't you?"

"With him—in the train? I should think so if you wish it."

"But would HE wish it? I mean would he hate it?"

"I don't think so at all, but I can easily ask him."

Mrs. Brook's head inclined to the chimney and her eyes to the window. "Easily?"

Nanda looked for a moment mystified by her mother's insistence. "I can at any rate perfectly try it."

"Remembering even that mamma would never have pushed so?"

Nanda's face seemed to concede even that condition. "Well," she at all events serenely replied, "I really think we're good friends enough for anything."

It might have been, for the light it quickly produced, exactly what her mother had been working to make her say. "What do you call that then, I should like to know, but his adopting you?"

"Ah I don't know that it matters much what it's called."

"So long as it brings with it, you mean," Mrs. Brook asked, "all the advantages?"

"Well yes," said Nanda, who had now begun dimly to smile—"call them advantages."

Mrs. Brook had a pause. "One would be quite ready to do that if one only knew a little more exactly what they're to consist of."

"Oh the great advantage, I feel, is doing something for HIM."

Nanda's companion, at this, hesitated afresh. "But doesn't that, my dear, put the extravagance of your surrender to him on rather an odd footing? Charity, love, begins at home, and if it's a question of merely GIVING, you've objects enough for your bounty without going so far."
The girl, as her stare showed, was held a moment by her surprise, which presently broke out. "Why, I thought you wanted me so to be nice to him!"

"Well, I hope you won't think me very vulgar," said Mrs. Brook, "if I tell you that I want you still more to have some idea of what you'll get by it. I've no wish," she added, "to keep on boring you with Mitchy—"

"Don't, don't!" Nanda pleaded.

Her mother stopped as short as if there had been something in her tone to set the limit the more utterly for being unstudied. Yet poor Mrs. Brook couldn't leave it there. "Then what do you get instead?"

"Instead of Mitchy? Oh," said Nanda, "I shall never marry."

Mrs. Brook at this turned away, moving over to the window with quickened weariness. Nanda, on her side, as if their talk had ended, went across to the sofa to take up her parasol before leaving the room, an impulse rather favoured than arrested by the arrival of her brother Harold, who came in at the moment both his relatives had turned a back to the door and who gave his sister, as she faced him, a greeting that made their mother look round. "Hallo, Nan—you ARE lovely! Ain't she lovely, mother?"

"No!" Mrs. Brook answered, not, however, otherwise noticing him. Her domestic despair centred at this instant all in her daughter. "Well then, we shall consider—your father and I—that he must take the consequence."

Nanda had now her hand on the door, while Harold had dropped on the sofa. "'He'?" she just sounded.

"I mean Mr. Longdon."

"And what do you mean by the consequence?"

"Well, it will do for the beginning of it that you'll please go down WITH him."

"On Saturday then? Thanks, mamma," the girl returned.

She was instantly gone, on which Mrs. Brook had more attention for her son. This, after an instant, as she approached the sofa and raised her eyes from the little table beside it, came straight out. "Where in the world is that five-pound note?"

Harold looked vacantly about him. "What five-pound note?"