[Verse 1]
Haven't seen you at the Angel Cafe
Or any other place where the crowd hangs out
Sorry crazy just yesterday
Seems like you're the one they're all talking about
Tried to call a hundred times
Did you drown in all those tears?
Just gotta get you on the line
'Cause there's something you need to hear, oh

[Chorus]
You're not alone
I've been there too
You think that nobody cares for you
But don't forget
In the darkest night
When your heart is sinking like a stone
I know what you're going through
You're not alone

[Verse 2]
Oh, this morning I've been picturing you
In your lonely room with the shades pulled down
Caught up in all the bitter thoughts
I know the last thing you want is someone coming around
When it's time to talk it out
I'll be standing at your door
Whatever you need you can count on me
'Cause that's what friends are for

[Chorus]
You're not alone
I've been there too
You think that nobody cares for you
But don't forget
In the darkest night
When your heart is sinking like a stone
I know what you're going through
You're not alone

[Bridge]
I've been down that road you're on
When I swore it will never end
It's gonna take some time
But before you know it
You'll be back on your feet again

[Chorus]
You're not alone
I've been there too
You think that nobody cares for you
But don't forget
In the darkest night
When your heart is sinking like a stone
I know what you're going through
You're not alone
You're not alone
Dananananananadadanana
You're not alone
Nanananananadadanana
You're not alone
Nanananananadadanana
You're not alone
Nanananananadadanana
Nanananananadadanana
Nanananananadadanana