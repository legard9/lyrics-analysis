시계바늘 의해 세븐어클락 (Seven O'Clock) 가사
Hangul

[Verse 1]
그냥 가만히 있어
아무 말도 말아
이제 눈을 감아
잡어 내 손을 (yeah yeah)

[Verse 2]
시선회피 대체 뭘 숨기는지
아무것도 아니라며 웃어넘기기
모든 질문에 대답은 no no
눈물은 점점 흘러 너의 얼굴을 타고

[Verse 3]
No 전부 거짓말이야
고인 눈물이 말하잖아
상처만 받기엔 우린 너무 여려
바라보고만 있어도 마음이 너무 아려

[Pre-Chorus]
돌아가지 않는 시계바늘 속
너가 받은 상처 모두 품에 담을 나

지금 내 목소리 들려?
이제 너만의 비밀은 숨겨 (it’s ok)
아무 말도 하지마
난 너 만의 것

[Chorus]
그냥 가만히 있어 oh oh oh oh
찢겨 무너지는 널
지킬게 나란 사람

그냥 가만히 있어 oh oh oh oh
너라면 상관없어
너의 비밀로만 채워줘

[Verse 4]
괜찮은거 맞아?
젖은 눈은 아니라 하잖아
너의 몸에 퍼진 독기는
내 신경을 자극하는 향기들 ah

[Verse 5]
숨기려 해도 숨길 수가 없어
오직 나만이 알 수가 있어
텅 빈 날 너 로만 채워줘

[Pre-Chorus]
돌아가지 않는 시계바늘 속
너가 받은 상처 모두 품에 담을 나

지금 내 목소리 들려?
이제 너만의 비밀은 숨겨 (it’s ok)
아무 말도 하지마
난 너 만의 것

[Chorus]
그냥 가만히 있어 oh oh oh oh
찢겨 무너지는 널
지킬게 나란 사람

그냥 가만히 있어 oh oh oh oh
너라면 상관없어
너의 비밀로만 채워줘

[Bridge]
여기에 stay
오직 너만 보겠어
원하는 뭐든 되줄게 내가

Take on me 잘 들어봐
Take on me 감추지마
Take on me 너의 모든 걸 다 내게 맡겨봐

[Outro]
그냥 가만히 있어 oh oh oh oh
찢겨 무너지는 널
지킬게 나란 사람

그냥 가만히 있어 oh oh oh oh
너라면 상관없어
너의 비밀로만 채워줘

그냥 가만히 있어 oh oh oh oh
찢겨 무너지는 널
지킬게 나란 사람

그냥 가만히 있어 oh oh oh oh
너라면 상관없어
너의 비밀로만 채워줘
그냥 가만히 있어

Romanization

[Verse 1]
Geunyang gamanhi isseo
Amu maldo mara
Ije nuneul gama
Jabeo nae soneul (yeah yeah)

[Verse 2]
Shiseonhoepi daeche mwol sumgineunji
Amugeotto aniramyeo useoneomgigi
Modeun jilmune daedabeun no no
Nunmureun jeomjeom heulleo neoye eolgureul tago

[Verse 3]
No jeonbu geojitmariya
Goin nunmuri malhajana
Sangcheoman batgien urin neomu yeoryeo
Barabogoman isseodo maeumi neomu aryeo

[Pre-Chorus]
Doragaji anneun shigyebaneul sok
Neoga badeun sangcheo modu pume dameul na

Jigeum nae moksori deullyeo?
Ije neomane bimireun sumgyeo (it’s ok)
Amu maldo hajima
Nan neo mane geot

[Chorus]
Geunyang gamanhi isseo oh oh oh oh
Jjitgyeo muneojineun neol
Jikilge naran saram

Geunyang gamanhi isseo oh oh oh oh
Neoramyeon sanggwaneopseo
Neoye bimilloman chaewojwo

[Verse 4]
Gwaenchaneungeo maja?
Jeojeun nuneun anira hajana
Neoye mome peojin dokgineun
Nae shingyeongeul jageukhaneun hyanggideul ah

[Verse 5]
Sumgiryeo haedo sumgil suga eopseo
Ojik namani al suga isseo
Teong bin nal neo roman chaewojwo

[Pre-Chorus]
Doragaji anneun shigyebaneul sok
Neoga badeun sangcheo modu pume dameul na

Jigeum nae moksori deullyeo?
Ije neomane bimireun sumgyeo (it’s ok)
Amu maldo hajima
Nan neo mane geot

[Chorus]
Geunyang gamanhi isseo oh oh oh oh
Jjitgyeo muneojineun neol
Jikilge naran saram

Geunyang gamanhi isseo oh oh oh oh
Neoramyeon sanggwaneopseo
Neoye bimilloman chaewojwo

[Bridge]
Yeogie stay
Ojik neoman bogesseo
Wonhaneun mwodeun doejulge naega

Take on me jal deureobwa
Take on me gamchujima
Take on me neoye modeun geol da naege matgyeobwa

[Outro]
Geunyang gamanhi isseo oh oh oh oh
Jjitgyeo muneojineun neol
Jikilge naran saram

Geunyang gamanhi isseo oh oh oh oh
Neoramyeon sanggwaneopseo
Neoye bimilloman chaewojwo

Geunyang gamanhi isseo oh oh oh oh
Jjitgyeo muneojineun neol
Jikilge naran saram

Geunyang gamanhi isseo oh oh oh oh
Neoramyeon sanggwaneopseo
Neoye bimilloman chaewojwo
Geunyang gamanhi isseo

English

[Verse 1]
Just stay as you are
Don’t say a word
Now, close your eyes
Hold my hand (yeah yeah)

[Verse 2]
You shift your eyes, what are you hiding?
You laugh and say it’s nothing
Your answer is no no to every question
The tears rolling down your eyes

[Verse 3]
No it’s all a lie
The tears brimming in your eyes tell me
We’re too young to only get hurt
It hurts to even look at you

[Pre-Chorus]
Stuck in the hands of time that won’t go back
I’ll embrace all of your pain

Can you hear my voice right now?
You can hide your secrets (it’s ok)
Don’t say anything anymore
I am yours

[Chorus]
Just stay as you are oh oh oh oh
You’re tearing apart and wearing down
I’ll be the one to protect you

Just stay as you are oh oh oh oh
I don’t care as long it’s for you
Just fill me with your secrets

[Verse 4]
Are you sure you’re okay?
Your wet eyes are betraying you
The poison spreading in your body
The scents waking up my senses ah

[Verse 5]
I can’t hide even if I want to
Only I can know
Fill me up with you

[Pre-Chorus]
Stuck in the hands of time that won’t go back
I’ll embrace all of your pain

Can you hear my voice right now?
You can hide your secrets (it’s ok)
Don’t say anything anymore
I am yours

[Chorus]
Just stay as you are oh oh oh oh
You’re tearing apart and wearing down
I’ll be the one to protect you

Just stay as you are oh oh oh oh
I don’t care as long it’s for you
Just fill me with your secrets

[Bridge]
Stay here
I’ll only have eyes for you
I’ll be whatever you need me to be

Take on me listen closely
Take on me don’t hide it
Take on me leave everything on me

[Outro]
Just stay as you are oh oh oh oh
You’re tearing apart and wearing down
I’ll be the one to protect you

Just stay as you are oh oh oh oh
I don’t care as long it’s for you
Just fill me with your secrets

Just stay as you are oh oh oh oh
You’re tearing apart and wearing down
I’ll be the one to protect you

Just stay as you are oh oh oh oh
I don’t care as long it’s for you
Just fill me with your secrets
Just stay as you are