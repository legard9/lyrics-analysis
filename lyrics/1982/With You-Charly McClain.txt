I love a starry night
Warm red wine and candle light
With you, with you
I love a summer rain
Snowflakes on the window pane
With you, with you
With you, all of my skies are blue
With you, all of my dreams come true
I've got it all when I'm with you
Never felt this way before
I found all I was lookin' for
With you, with you
Everything we do feels right
Can't wait to spend the rest of my life
With you, with you
With you, all of my skies are blue
With you, all of my dreams come true
I've got it all when I'm with you
With you, all of my skies are blue
With you, all of my dreams come true
I've got it all when I'm with you