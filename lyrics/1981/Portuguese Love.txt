Even if you send me a letter with our names in a cutie heart
I wouldn’t fall in love
And if you send me a lot of flowers with hershey’s
I wouldn’t mind the chocolates
But I’m so alergic of flowers I could fall to the ground
Fall to the ground
You can’t blame me for not being romantic
If I don’t know what is sentimental
I cannot go wrong with an off-key
Any boy would think that it’s gentle
Anyway, my heart isn’t a crowd you choose someone to dance
You’d turn to be my friend
It’s more to a book you don’t get the end
I can’t fall in love
I can’t fall in love
Although you’re irresistible
I can’t fall in love
When I said that I loved you, you didn’t move a muscle
Surprised enough to say something
And then you said that you loved me
I don’t know, but I felt you were afraid that I could hurt you
Instead of silly break-ups
We should kiss and make-up
Call me up when you wake up and maybe I shut up
That I can’t fall in love
Cause I can’t fall in love
Although you’re irresistible
I can’t fall in love
Instead of silly break-ups
We should kiss and make-up
Call me up when you wake up and maybe I shut up