2075
The Freeways were still alive
See the People for Miles and Miles
2075
We were searching for a place to hide
Easy rider meets the end of time
You were young and i was wild
2075
And they blew down the buildings
And we stood in lines for bread
Just like Mother Russia back when she was still Red

And they said
Ding Dong The Witch is Dead
From the shores of Nantucket to Los Angeles
She was swallowed up in petty greed

Verse 2

In New York City
The sun doesn't shine
And Hollywood
Is covered in vines
They dug up treasure from the Mayan Times
2079
But the fire in their breast
Burned like a funeral Pyre

The museums are closing
And the animals all fled
Down deep in the City
Liberty's lost her head

And they said
Ding Dong The Witch is dead
From the bars of Milwaukee to the Forests of Bend
She was driven out of the city on her own Stench

Bridge-

So Tell me a story
And make it sweet
Like a tale from a dream
There is a city of gold up in the sky
A place where we never die

2099