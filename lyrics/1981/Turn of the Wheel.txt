Hunter
We're here to praise your name

Blazing sun and bitter death
You're the guardian cross the gate

Wake up!
It is a new dawning
Wake up!
The witches wait
For you remember the mirror
When she looked in
You were born
In the void
In the middle of none
There was nobody else
There was nothing at all

Well sons and daughters
Joy's in the air
The horned one dies
Renewal everywhere

Turn the wheel again!

A new beginning
Another end
Dried out
The land needs blood
Inside the ring we're waiting
Give up yourself
Enter life

It's set up for you
Come turn the wheel
There is nothing to feel
Come turn the wheel
Come turn the page
Come turn the page

(Solo 1)

We do not believe in lies
Do not believe in lies

There's someone coming
There's someone coming
We hear a voice from the underworld
Now everything should move
When the new day begins

God of wind and god of rain
Turn the wheel
Oh you better be aware
The mermaids
They will sing for you
The nymphs will bless the child

Reunite them
Sacrifice him
We will be with you until the end
Come move our hearts
We know there is light beyond the dark

Dried out
We cry for blood
Conquer your fear and join us
Come guide us now
Time is right

It's set up for you
Come turn the wheel
There is nothing to feel
Come turn the wheel
Come turn the page
Come turn the page

(Solo 2)

A new beginning
Another end
The myth of life
You hold in your hand
We cry out
The land needs blood
The change of season
Praise to the newborn king!

Na, na, na, nanananana!
Na, na, na, nanananana!
Na, na, na, nanananana!
Na, na, na, nanananana!

It's set up for you
Come turn the wheel
There is nothing to feel
Come turn the wheel
Come turn the page
Come turn the page