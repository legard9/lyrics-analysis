She remained alone ten minutes, at the end of which her reflexions would have been seen to be deep—were interrupted by the entrance of her husband. The interruption was indeed not so great as if the couple had not met, as they almost invariably met, in silence: she took at all events, to begin with, no more account of his presence than to hand him a cup of tea accompanied with nothing but cream and sugar. Her having no word for him, however, committed her no more to implying that he had come in only for his refreshment than it would have committed her to say: "Here it is, Edward dear—just as you like it; so take it and sit down and be quiet." No spectator worth his salt could have seen them more than a little together without feeling how everything that, under his eyes or not, she either did or omitted, rested on a profound acquaintance with his ways. They formed, Edward's ways, a chapter by themselves, of which Mrs. Brook was completely mistress and in respect to which the only drawback was that a part of her credit was by the nature of the case predestined to remain obscure. So many of them were so queer that no one but she COULD know them, and know thereby into what crannies her reckoning had to penetrate. It was one of them for instance that if he was often most silent when most primed with matter, so when he had nothing to say he was always silent too—a peculiarity misleading, until mastered, for a lady who could have allowed in the latter case for almost any variety of remark. "What do you think," he said at last, "of his turning up to-day?"
<<<<<<< HEAD

"Of old Van's?"

"Oh has HE turned up?"

"Half an hour ago, and asking almost in his first breath for Nanda. I sent him up to her and he's with her now." If Edward had his ways she had also some of her own; one of which, in talk with him, if talk it could be called, was never to produce anything till the need was marked. She had thus a card or two always in reserve, for it was her theory that she never knew what might happen. It nevertheless did occur that he sometimes went, as she would have called it, one better.

"He's not with her now. I've just been with her."

"Then he didn't go up?" Mrs. Brook was immensely interested. "He left me, you know, to do so."

"Know—how should I know? I left her five minutes ago."

"Then he went out without seeing her." Mrs. Brook took it in. "He changed his mind out there on the stairs."

"Well," said Edward, "it won't be the first mind that has been changed there. It's about the only thing a man can change."

"Do you refer particularly to MY stairs?" she asked with her whimsical woe. But meanwhile she had taken it in. "Then whom were you speaking of?"

"Mr. Longdon's coming to tea with her. She has had a note."

"But when did he come to town?"

"Last night, I believe. The note, an hour or two ago, announced him—brought by hand and hoping she'd be at home."

Mrs. Brook thought again. "I'm glad she is. He's too sweet. By hand!—it must have been so he sent them to mamma. He wouldn't for the world wire."

"Oh Nanda has often wired to HIM," her father returned.

"Then she ought to be ashamed of herself. But how," said Mrs. Brook, "do you know?"

"Oh I know when we're in a thing like this."

"Yet you complain of her want of intimacy with you! It turns out that you're as thick as thieves."

Edward looked at this charge as he looked at all old friends, without a sign—to call a sign—of recognition. "I don't know of whose want of intimacy with me I've ever complained. There isn't much more of it, that I can see, that any of them could put on. What do you suppose I'd have them do? If I on my side don't get very far I may have alluded to THAT."

"Oh but you do," Mrs. Brook declared. "You think you don't, but you get very far indeed. You're always, as I said just now, bringing out something that you've got somewhere."

"Yes, and seeing you flare up at it. What I bring out is only what they tell me."

This limitation offered, however, for Mrs. Brook no difficulty. "Ah but it seems to me that with the things people nowadays tell one—! What more do you want?"

"Well"—and Edward from his chair regarded the fire a while—"the difference must be in what they tell YOU."

"Things that are better?"

"Yes—worse. I dare say," he went on, "what I give them—"

"Isn't as bad as what I do? Oh we must each do our best. But when I hear from you," Mrs. Brook pursued, "that Nanda had ever permitted herself anything so dreadful as to wire to him, it comes over me afresh that I would have been the perfect one to deal with him if his detestation of me hadn't prevented." She was by this time also—but on her feet—before the fire, into which, like her husband, she gazed. "I would never have wired. I'd have gone in for little delicacies and odd things she has never thought of."

"Oh she doesn't go in for what you do," Edward assented.

"She's as bleak as a chimney-top when the fire's out, and if it hadn't been after all for mamma—!" And she lost herself again in the reasons of things.

Her husband's silence seemed to mark for an instant a deference to her allusion, but there was a limit even to this combination. "You make your mother, I think, keep it up pretty well. But if she HADN'T as you say, done so—?"

"Why we shouldn't have been anywhere."

"Well, where are we now? That's what I want to know."

Following her own train she had at first no heed for his question. "Without his hatred he would have liked me." But she came back with a sigh to the actual. "No matter. We must deal with what we've got."

"What HAVE we got?" Edward continued.

Again with no ear for his question his wife turned away, only however, after taking a few vague steps, to approach him with new decision. "If Mr. Longdon's due will you do me a favour? Will you go back to Nanda—before he arrives—and let her know, though not of course as from ME, that Van has been here half an hour, has had it put well before him that she's up there and at liberty, and has left the house without seeing her?"
Edward Brookenham made no motion. "You don't like better to do it yourself?"

"If I liked better," said Mrs. Brook, "I'd have already done it. The way to make it not come from me is surely not for me to give it to her. Besides, I want to be here to receive him first."

"Then can't she know it afterwards?"

"After Mr. Longdon has gone? The whole point is that she should know it in time to let HIM know it."

Edward still communed with the fire. "And what's the point of THAT?" Her impatience, which visibly increased, carried her away again, and by the time she reached the window he had launched another question. "Are you in such a hurry she should know that Van doesn't want her?"

"What do you call a hurry when I've waited nearly a year? Nanda may know or not as she likes—may know whenever: if she doesn't know pretty well by this time she's too stupid for it to matter. My only pressure's for Mr. Longdon. She'll have it there for him when he arrives."

"You mean she'll make haste to tell him?"

Mrs. Brook raised her eyes a moment to some upper immensity. "She'll mention it."

Her husband on the other hand, his legs outstretched, looked straight at the toes of his boots. "Are you very sure?" Then as he remained without an answer: "Why should she if he hasn't told HER?"

"Of the way I so long ago let you know that he had put the matter to Van? It's not out between them in words, no doubt; but I fancy that for things to pass they've not to dot their i's quite so much, my dear, as we two. Without a syllable said to her she's yet aware in every fibre of her little being of what has taken place."

Edward gave a still longer space to taking this in. "Poor little thing!"

"Does she strike you as so poor," Mrs. Brook asked, "with so awfully much done for her?"

"Done by whom?"

It was as if she had not heard the question that she spoke again. "She has got what every woman, young or old, wants."

"Really?"

Edward's tone was of wonder, but she simply went on: "She has got a man of her own."

"Well, but if he's the wrong one?"

"Do you call Mr. Longdon so very wrong? I wish," she declared with a strange sigh, "that I had had a Mr. Longdon!"

"I wish very much you had. I wouldn't have taken it like Van."

"Oh it took Van," Mrs. Brook replied, "to put THEM where they are."

"But where ARE they? That's exactly it. In these three months, for instance," Edward demanded, "how has their connexion profited?"

Mrs. Brook turned it over. "Profited which?"

"Well, one cares most for one's child."

"Then she has become for him what we've most hoped her to be—an object of compassion still more marked."

"Is that what you've hoped her to be?" Mrs. Brook was obviously so lucid for herself that her renewed expression of impatience had plenty of point. "How can you ask after seeing what I did—"

"That night at Mrs. Grendon's? Well, it's the first time I HAVE asked it."

Mrs. Brook had a silence more pregnant. "It's for being with US that he pities her."

Edward thought. "With me too?"

"Not so much—but still you help."

"I thought you thought I didn't—that night."

"At Tishy's? Oh you didn't matter," said Mrs. Brook. "Everything, every one helps. Harold distinctly"—she seemed to figure it all out—"and even the poor children, I dare say, a little. Oh but every one"—she warmed to the vision—"it's perfect. Jane immensely, par example. Almost all the others who come to the house. Cashmore, Carrie, Tishy, Fanny—bless their hearts all!—each in their degree."

Edward Brookenham had under the influence of this demonstration gradually risen from his seat, and as his wife approached that part of her process which might be expected to furnish the proof he placed himself before her with his back to the fire. "And Mitchy, I suppose?"
But he was out. "No. Mitchy's different."

He wondered. "Different?"

"Not a help. Quite a drawback." Then as his face told how these WERE involutions, "You needn't understand, but you can believe me," she added. "The one who does most is of course Van himself." It was a statement by which his failure to apprehend was not diminished, and she completed her operation. "By not liking her."

Edward's gloom, on this, was not quite blankness, yet it was dense. "Do you like his not liking her?"

"Dear no. No better than HE does."

"And he doesn't—?"

"Oh he hates it."

"Of course I haven't asked him," Edward appeared to say more to himself than to his wife.

"And of course I haven't," she returned—not at all in this case, plainly, for herself. "But I know it. He'd like her if he could, but he can't. That," Mrs. Brook wound up, "is what makes it sure."

There was at last in Edward's gravity a positive pathos. "Sure he won't propose?"

"Sure Mr. Longdon won't now throw her over."

"Of course if it IS sure—"

"Well?"

"Why, it is. But of course if it isn't—"

"Well?"

"Why, she won't have anything. Anything but US," he continued to reflect. "Unless, you know, you're working it on a certainty—!"

"That's just what I AM working it on. I did nothing till I knew I was safe."

"'Safe'?" he ambiguously echoed while on this their eyes met longer.

"Safe. I knew he'd stick."

"But how did you know Van wouldn't?"

"No matter 'how'—but better still. He hasn't stuck." She said it very simply, but she turned away from him.

His eyes for a little followed her. "We don't KNOW, after all, the old boy's means."

"I don't know what you mean by 'we' don't. Nanda does."

"But where's the support if she doesn't tell us?"

Mrs. Brook, who had faced about, again turned from him. "I hope you don't forget," she remarked with superiority, "that we don't ask her."

"YOU don't?" Edward gloomed.

"Never. But I trust her."

"Yes," he mused afresh, "one must trust one's child. Does Van?" he then enquired.

"Does he trust her?"

"Does he know anything of the general figure?"

She hesitated. "Everything. It's high."

"He has told you so?"

Mrs. Brook, supremely impatient now, seemed to demur even to the question. "We ask HIM even less."

"Then how do we know?"

She was weary of explaining. "Because that's just why he hates it."

There was no end however, apparently, to what Edward could take. "But hates what?"

"Why, not liking her."

Edward kept his back to the fire and his dead eyes on the cornice and the ceiling. "I shouldn't think it would be so difficult."

"Well, you see it isn't. Mr. Longdon can manage it."

"I don't see what the devil's the matter with her," he coldly continued.

"Ah that may not prevent—! It's fortunately the source at any rate of half Mr. Longdon's interest."

"But what the hell IS it?" he drearily demanded.

She faltered a little, but she brought it out. "It's ME."

"And what's the matter with 'you'?"

She made, at this, a movement that drew his eyes to her own, and for a moment she dimly smiled at him. "That's the nicest thing you ever said to me. But ever, EVER, you know."

"Is it?" She had her hand on his sleeve, and he looked almost awkward.

"Quite the very nicest. Consider that fact well and even if you only said it by accident don't be funny—as you know you sometimes CAN be—and take it back. It's all right. It's charming, isn't it? when our troubles bring us more together. Now go up to her."

Edward kept a queer face, into which this succession of remarks introduced no light, but he finally moved, and it was only when he had almost reached the door that he stopped again. "Of course you know he has sent her no end of books."

"Mr. Longdon—of late? Oh yes, a deluge, so that her room looks like a bookseller's back shop; and all, in the loveliest bindings, the most standard English works. I not only know it, naturally, but I know—what you don't—why."

"'Why'?" Edward echoed. "Why but that—unless he should send her money—it's about the only kindness he can show her at a distance?"

Mrs. Brook hesitated; then with a little suppressed sigh: "That's it!"

But it still held him. "And perhaps he does send her money."

"No. Not now."

Edward lingered. "Then is he taking it out—?"

"In books only?" It was wonderful—with its effect on him now visible—how she possessed her subject. "Yes, that's his delicacy—for the present."

"And you're not afraid for the future—?"

"Of his considering that the books will have worked it off? No. They're thrown in."

Just perceptibly cheered he reached the door, where, however, he had another pause. "You don't think I had better see Van?"
She stared. "What for?"

"Why, to ask what the devil he means."

"If you should do anything so hideously vulgar," she instantly replied, "I'd leave your house the next hour. Do you expect," she asked, "to be able to force your child down his throat?"

He was clearly not prepared with an account of his expectations, but he had a general memory that imposed itself. "Then why in the world did he make up to us?"

"He didn't. We made up to HIM."

"But why in the world—?"

"Well," said Mrs. Brook, really to finish, "we were in love with him."

=======
"Of old Van's?"
"Oh has HE turned up?"
"Half an hour ago, and asking almost in his first breath for Nanda. I sent him up to her and he's with her now." If Edward had his ways she had also some of her own; one of which, in talk with him, if talk it could be called, was never to produce anything till the need was marked. She had thus a card or two always in reserve, for it was her theory that she never knew what might happen. It nevertheless did occur that he sometimes went, as she would have called it, one better.
"He's not with her now. I've just been with her."
"Then he didn't go up?" Mrs. Brook was immensely interested. "He left me, you know, to do so."
"Know—how should I know? I left her five minutes ago."
"Then he went out without seeing her." Mrs. Brook took it in. "He changed his mind out there on the stairs."
"Well," said Edward, "it won't be the first mind that has been changed there. It's about the only thing a man can change."
"Do you refer particularly to MY stairs?" she asked with her whimsical woe. But meanwhile she had taken it in. "Then whom were you speaking of?"
"Mr. Longdon's coming to tea with her. She has had a note."
"But when did he come to town?"
"Last night, I believe. The note, an hour or two ago, announced him—brought by hand and hoping she'd be at home."
Mrs. Brook thought again. "I'm glad she is. He's too sweet. By hand!—it must have been so he sent them to mamma. He wouldn't for the world wire."
"Oh Nanda has often wired to HIM," her father returned.
"Then she ought to be ashamed of herself. But how," said Mrs. Brook, "do you know?"
"Oh I know when we're in a thing like this."
"Yet you complain of her want of intimacy with you! It turns out that you're as thick as thieves."
Edward looked at this charge as he looked at all old friends, without a sign—to call a sign—of recognition. "I don't know of whose want of intimacy with me I've ever complained. There isn't much more of it, that I can see, that any of them could put on. What do you suppose I'd have them do? If I on my side don't get very far I may have alluded to THAT."
"Oh but you do," Mrs. Brook declared. "You think you don't, but you get very far indeed. You're always, as I said just now, bringing out something that you've got somewhere."
"Yes, and seeing you flare up at it. What I bring out is only what they tell me."
This limitation offered, however, for Mrs. Brook no difficulty. "Ah but it seems to me that with the things people nowadays tell one—! What more do you want?"
"Well"—and Edward from his chair regarded the fire a while—"the difference must be in what they tell YOU."
"Things that are better?"
"Yes—worse. I dare say," he went on, "what I give them—"
"Isn't as bad as what I do? Oh we must each do our best. But when I hear from you," Mrs. Brook pursued, "that Nanda had ever permitted herself anything so dreadful as to wire to him, it comes over me afresh that I would have been the perfect one to deal with him if his detestation of me hadn't prevented." She was by this time also—but on her feet—before the fire, into which, like her husband, she gazed. "I would never have wired. I'd have gone in for little delicacies and odd things she has never thought of."
"Oh she doesn't go in for what you do," Edward assented.
"She's as bleak as a chimney-top when the fire's out, and if it hadn't been after all for mamma—!" And she lost herself again in the reasons of things.
Her husband's silence seemed to mark for an instant a deference to her allusion, but there was a limit even to this combination. "You make your mother, I think, keep it up pretty well. But if she HADN'T as you say, done so—?"
"Why we shouldn't have been anywhere."
"Well, where are we now? That's what I want to know."
Following her own train she had at first no heed for his question. "Without his hatred he would have liked me." But she came back with a sigh to the actual. "No matter. We must deal with what we've got."
"What HAVE we got?" Edward continued.
Again with no ear for his question his wife turned away, only however, after taking a few vague steps, to approach him with new decision. "If Mr. Longdon's due will you do me a favour? Will you go back to Nanda—before he arrives—and let her know, though not of course as from ME, that Van has been here half an hour, has had it put well before him that she's up there and at liberty, and has left the house without seeing her?"
Edward Brookenham made no motion. "You don't like better to do it yourself?"
"If I liked better," said Mrs. Brook, "I'd have already done it. The way to make it not come from me is surely not for me to give it to her. Besides, I want to be here to receive him first."
"Then can't she know it afterwards?"
"After Mr. Longdon has gone? The whole point is that she should know it in time to let HIM know it."
Edward still communed with the fire. "And what's the point of THAT?" Her impatience, which visibly increased, carried her away again, and by the time she reached the window he had launched another question. "Are you in such a hurry she should know that Van doesn't want her?"
"What do you call a hurry when I've waited nearly a year? Nanda may know or not as she likes—may know whenever: if she doesn't know pretty well by this time she's too stupid for it to matter. My only pressure's for Mr. Longdon. She'll have it there for him when he arrives."
"You mean she'll make haste to tell him?"
Mrs. Brook raised her eyes a moment to some upper immensity. "She'll mention it."
Her husband on the other hand, his legs outstretched, looked straight at the toes of his boots. "Are you very sure?" Then as he remained without an answer: "Why should she if he hasn't told HER?"
"Of the way I so long ago let you know that he had put the matter to Van? It's not out between them in words, no doubt; but I fancy that for things to pass they've not to dot their i's quite so much, my dear, as we two. Without a syllable said to her she's yet aware in every fibre of her little being of what has taken place."
Edward gave a still longer space to taking this in. "Poor little thing!"
"Does she strike you as so poor," Mrs. Brook asked, "with so awfully much done for her?"
"Done by whom?"
It was as if she had not heard the question that she spoke again. "She has got what every woman, young or old, wants."
"Really?"
Edward's tone was of wonder, but she simply went on: "She has got a man of her own."
"Well, but if he's the wrong one?"
"Do you call Mr. Longdon so very wrong? I wish," she declared with a strange sigh, "that I had had a Mr. Longdon!"
"I wish very much you had. I wouldn't have taken it like Van."
"Oh it took Van," Mrs. Brook replied, "to put THEM where they are."
"But where ARE they? That's exactly it. In these three months, for instance," Edward demanded, "how has their connexion profited?"
Mrs. Brook turned it over. "Profited which?"
"Well, one cares most for one's child."
"Then she has become for him what we've most hoped her to be—an object of compassion still more marked."
"Is that what you've hoped her to be?" Mrs. Brook was obviously so lucid for herself that her renewed expression of impatience had plenty of point. "How can you ask after seeing what I did—"
"That night at Mrs. Grendon's? Well, it's the first time I HAVE asked it."
Mrs. Brook had a silence more pregnant. "It's for being with US that he pities her."
Edward thought. "With me too?"
"Not so much—but still you help."
"I thought you thought I didn't—that night."
"At Tishy's? Oh you didn't matter," said Mrs. Brook. "Everything, every one helps. Harold distinctly"—she seemed to figure it all out—"and even the poor children, I dare say, a little. Oh but every one"—she warmed to the vision—"it's perfect. Jane immensely, par example. Almost all the others who come to the house. Cashmore, Carrie, Tishy, Fanny—bless their hearts all!—each in their degree."
Edward Brookenham had under the influence of this demonstration gradually risen from his seat, and as his wife approached that part of her process which might be expected to furnish the proof he placed himself before her with his back to the fire. "And Mitchy, I suppose?"
But he was out. "No. Mitchy's different."
He wondered. "Different?"
"Not a help. Quite a drawback." Then as his face told how these WERE involutions, "You needn't understand, but you can believe me," she added. "The one who does most is of course Van himself." It was a statement by which his failure to apprehend was not diminished, and she completed her operation. "By not liking her."
Edward's gloom, on this, was not quite blankness, yet it was dense. "Do you like his not liking her?"
"Dear no. No better than HE does."
"And he doesn't—?"
"Oh he hates it."
"Of course I haven't asked him," Edward appeared to say more to himself than to his wife.
"And of course I haven't," she returned—not at all in this case, plainly, for herself. "But I know it. He'd like her if he could, but he can't. That," Mrs. Brook wound up, "is what makes it sure."
There was at last in Edward's gravity a positive pathos. "Sure he won't propose?"
"Sure Mr. Longdon won't now throw her over."
"Of course if it IS sure—"
"Well?"
"Why, it is. But of course if it isn't—"
"Well?"
"Why, she won't have anything. Anything but US," he continued to reflect. "Unless, you know, you're working it on a certainty—!"
"That's just what I AM working it on. I did nothing till I knew I was safe."
"'Safe'?" he ambiguously echoed while on this their eyes met longer.
"Safe. I knew he'd stick."
"But how did you know Van wouldn't?"
"No matter 'how'—but better still. He hasn't stuck." She said it very simply, but she turned away from him.
His eyes for a little followed her. "We don't KNOW, after all, the old boy's means."
"I don't know what you mean by 'we' don't. Nanda does."
"But where's the support if she doesn't tell us?"
Mrs. Brook, who had faced about, again turned from him. "I hope you don't forget," she remarked with superiority, "that we don't ask her."
"YOU don't?" Edward gloomed.
"Never. But I trust her."
"Yes," he mused afresh, "one must trust one's child. Does Van?" he then enquired.
"Does he trust her?"
"Does he know anything of the general figure?"
She hesitated. "Everything. It's high."
"He has told you so?"
Mrs. Brook, supremely impatient now, seemed to demur even to the question. "We ask HIM even less."
"Then how do we know?"
She was weary of explaining. "Because that's just why he hates it."
There was no end however, apparently, to what Edward could take. "But hates what?"
"Why, not liking her."
Edward kept his back to the fire and his dead eyes on the cornice and the ceiling. "I shouldn't think it would be so difficult."
"Well, you see it isn't. Mr. Longdon can manage it."
"I don't see what the devil's the matter with her," he coldly continued.
"Ah that may not prevent—! It's fortunately the source at any rate of half Mr. Longdon's interest."
"But what the hell IS it?" he drearily demanded.
She faltered a little, but she brought it out. "It's ME."
"And what's the matter with 'you'?"
She made, at this, a movement that drew his eyes to her own, and for a moment she dimly smiled at him. "That's the nicest thing you ever said to me. But ever, EVER, you know."
"Is it?" She had her hand on his sleeve, and he looked almost awkward.
"Quite the very nicest. Consider that fact well and even if you only said it by accident don't be funny—as you know you sometimes CAN be—and take it back. It's all right. It's charming, isn't it? when our troubles bring us more together. Now go up to her."
Edward kept a queer face, into which this succession of remarks introduced no light, but he finally moved, and it was only when he had almost reached the door that he stopped again. "Of course you know he has sent her no end of books."
"Mr. Longdon—of late? Oh yes, a deluge, so that her room looks like a bookseller's back shop; and all, in the loveliest bindings, the most standard English works. I not only know it, naturally, but I know—what you don't—why."
"'Why'?" Edward echoed. "Why but that—unless he should send her money—it's about the only kindness he can show her at a distance?"
Mrs. Brook hesitated; then with a little suppressed sigh: "That's it!"
But it still held him. "And perhaps he does send her money."
"No. Not now."
Edward lingered. "Then is he taking it out—?"
"In books only?" It was wonderful—with its effect on him now visible—how she possessed her subject. "Yes, that's his delicacy—for the present."
"And you're not afraid for the future—?"
"Of his considering that the books will have worked it off? No. They're thrown in."
Just perceptibly cheered he reached the door, where, however, he had another pause. "You don't think I had better see Van?"
She stared. "What for?"
"Why, to ask what the devil he means."
"If you should do anything so hideously vulgar," she instantly replied, "I'd leave your house the next hour. Do you expect," she asked, "to be able to force your child down his throat?"
He was clearly not prepared with an account of his expectations, but he had a general memory that imposed itself. "Then why in the world did he make up to us?"
"He didn't. We made up to HIM."
"But why in the world—?"
"Well," said Mrs. Brook, really to finish, "we were in love with him."
>>>>>>> master
"Oh!" Edward jerked. He had by this time opened the door, and the sound was partly the effect of the disclosure of a servant preceding a visitor. His greeting of the visitor before edging past and away was, however, of the briefest; it might have implied that they had met but yesterday. "How d'ye do, Mitchy?—At home? Oh rather!"