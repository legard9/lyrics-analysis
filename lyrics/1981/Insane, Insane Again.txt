Hangul

난 매일 밤을 기도해 (꿈속에)
너를 만날 수 있게 해 (달라고)
내 곁에 없는 니 품에
하루 종일 뜨겁게 안겨 있을 수 있게 baby

너 하나면 난 충분해
세상에 유일한 빛이 돼가는
너의 존재에 감사해
너의 세상에 유일한 여자가 될 수 있게 baby

네 꿈속에 내가 먼저 찾아가
너와 눈을 맞추고
영원한 사랑을 속삭이고
아침이 되면 나는 다시 돌아가

Oh baby, I hope you know I love you insane, sane, sane
I love you insane, sane, yeah
I'll love you the same, same, same
좀 더 내 곁에, hey, yeah

Oh 있기 싫어 나 홀로, 나 홀로
필요 없어 wake up call, wake up call
Hmm, 눈을 떠도 love you insane

남겨진 너의 향기에 나는 기억해, whoa oh
이런 일이 난 익숙해, ooh déjà vu, yeah baby
또다시 눈을 감으며 너를 만나길 기다려 (널 만나길 기다려)
오늘도 깨지 않는 잠에 취해 너와 함께
So we can love again, no whoa

네 꿈속에 내가 먼저 찾아가
너와 입을 맞추고 아쉬운 이별을 또 나누고
시간이 되면 나는 다시 돌아가
Oh baby, I hope you know I love you insane yeah

I love you insane, sane, sane
I love you insane, sane, yeah
I'll love you the same, same, same
좀 더 내 곁에, hey, yeah

눈을 떠도 love you insane

Said 있기 싫어 나 홀로, ah ooh
Oh 필요 없어 wake up call
잊을 수 없는 너의 미소가
아른거려 머릿속에
나 슬프지 않게 눈물 흘리지 않게
'Cause baby I'm insane for you!

Insane, sane, sane
I love you insane, sane, yeah (baby I'm insane, oh oh)
I'll love you the same, same, same (I'll love you the same, yeah)
좀 더 내 곁에, hey, yeah

Oh 있기 싫어 나 홀로, 나 홀로 (나 홀로, 나 홀로)
필요 없어 wake up call, wake up call
Hmm, 눈을 떠도 love you insane

Romanization

Nan maeil bameul gidohae (kkumsoge)
Neoreul mannal su itge hae (dallago)
Nae gyeote eomneun ni pume
Haru jongil tteugeopge angyeo isseul su itge baby

Neo hanamyeon nan chungbunhae
Sesange yuilhan bichi dwaeganeun
Neoui jonjaee gamsahae
Neoui sesange yuilhan yeojaga doel su itge baby

Ne kkumsoge naega meonjeo chajaga
Neowa nuneul matchugo
Yeongwonhan sarangeul soksagigo
Achimi doemyeon naneun dasi doraga

Oh baby, I hope you know I love you insane, sane, sane
I love you insane, sane, yeah
I'll love you the same, same, same
Jom deo nae gyeote, hey, yeah

Oh itgi sileo na hollo, na hollo
Pillyo eopseo wake up call, wake up call
Hmm, nuneul tteodo love you insane

Namgyeojin neoui hyanggie naneun gieokae, whoa oh
Ireon iri nan iksukae, ooh déjà vu, yeah baby
Ttodasi nuneul gameumyeo neoreul mannagil gidaryeo  (neol mannagil gidaryeo)
Oneuldo kkaeji anneun jame chwihae neowa hamkke
So we can love again, no whoa

Ne kkumsoge naega meonjeo chajaga
Neowa ibeul matchugo aswiun ibyeoreul tto nanugo
Sigani doemyeon naneun dasi doraga
Oh baby, I hope you know I love you insane yeah

I love you insane, sane, sane
I love you insane, sane, yeah
I'll love you the same, same, same
Jom deo nae gyeote, hey, yeah

Nuneul tteodo love you insane

Said itgi sileo na hollo, ah ooh
Oh pillyo eopseo wake up call
Ijeul su eomneun neoui misoga
Areungeoryeo meoritsoge
Na seulpeuji anke nunmul heulliji anke
'Cause baby I'm insane for you!

Insane, sane, sane
I love you insane, sane, yeah (baby I'm insane, oh oh)
I'll love you the same, same, same (I'll love you the same, yeah)
Jom deo nae gyeote, hey, yeah

Oh itgi sileo na hollo, na hollo (na hollo, na hollo)
Pillyo eopseo wake up call, wake up call
Hmm, nuneul tteodo love you insane