A ROOM IN VOLPONE'S HOUSE.

ENTER VOLPONE.

VOLP
Mosca stays long, methinks. Bring forth your sports,
And help to make the wretched time more sweet.

[ENTER NANO, ANDROGYNO, AND CASTRONE.]

NAN
Dwarf, fool, and eunuch, well met here we be.
A question it were now, whether of us three,
Being all the known delicates of a rich man,
In pleasing him, claim the precedency can?

CAS
I claim for myself.

AND
And so doth the fool.

NAN
'Tis foolish indeed: let me set you both to school.
First for your dwarf, he's little and witty,
And every thing, as it is little, is pretty;
Else why do men say to a creature of my shape,
So soon as they see him, It's a pretty little ape?
And why a pretty ape, but for pleasing imitation
Of greater men's actions, in a ridiculous fashion?
Beside, this feat body of mine doth not crave
Half the meat, drink, and cloth, one of your bulks will have.
Admit your fool's face be the mother of laughter,
Yet, for his brain, it must always come after:
And though that do feed him, 'tis a pitiful case,
His body is beholding to such a bad face.

[KNOCKING WITHIN.]

VOLP
Who's there? my couch; away! look! Nano, see:
[EXE. AND. AND CAS.]
Give me my caps, first—go, enquire.
[EXIT NANO.]
—Now, Cupid
Send it be Mosca, and with fair return!

NAN
[WITHIN.]: It is the beauteous madam—

VOLP
Would-be?—is it?

NAN
The same.

VOLP
Now torment on me! Squire her in;
For she will enter, or dwell here for ever:
Nay, quickly.
[RETIRES TO HIS COUCH.]
—That my fit were past! I fear
A second hell too, that my lothing this
Will quite expel my appetite to the other:
Would she were taking now her tedious leave.
Lord, how it threats me what I am to suffer!

[RE-ENTER NANO, WITH LADY POLITICK WOULD-BE.]

LADY P
I thank you, good sir. 'Pray you signify
Unto your patron, I am here.—This band
Shews not my neck enough.—I trouble you, sir;
Let me request you, bid one of my women
Come hither to me.—In good faith, I, am drest
Most favorably, to-day! It is no matter:
'Tis well enough.—
[ENTER 1 WAITING-WOMAN.]
Look, see, these petulant things,
How they have done this!

VOLP
[ASIDE.]: I do feel the fever
Entering in at mine ears; O, for a charm,
To fright it hence.

LADY P
Come nearer: Is this curl
In his right place, or this? Why is this higher
Then all the rest? You have not wash'd your eyes, yet!
Or do they not stand even in your head?
Where is your fellow? call her.

[EXIT 1 WOMAN.]

NAN
Now, St. Mark
Deliver us! anon, she will beat her women,
Because her nose is red.

[RE-ENTER 1 WITH 2 WOMAN.]

LADY P
I pray you, view
This tire, forsooth; are all things apt, or no?

1 WOM
One hair a little, here, sticks out, forsooth.

LADY P
Does't so, forsooth? and where was your dear sight,
When it did so, forsooth! What now! bird-eyed?
And you too? 'Pray you, both approach and mend it.
Now, by that light, I muse you are not ashamed!
I, that have preach'd these things so oft unto you,
Read you the principles, argued all the grounds,
Disputed every fitness, every grace,
Call'd you to counsel of so frequent dressings—

NAN
[ASIDE.]: More carefully than of your fame or honour.

LADY P
Made you acquainted, what an ample dowry
The knowledge of these things would be unto you,
Able, alone, to get you noble husbands
At your return: and you thus to neglect it!
Besides you seeing what a curious nation
The Italians are, what will they say of me?
"The English lady cannot dress herself."
Here's a fine imputation to our country:
Well, go your ways, and stay, in the next room.
This fucus was too course too, it's no matter.—
Good-sir, you will give them entertainment?

[EXEUNT NANO AND WAITING-WOMEN.]

VOLP
The storm comes toward me.

LADY P
[GOES TO THE COUCH.]: How does my Volpone?

VOLP
Troubled with noise, I cannot sleep; I dreamt
That a strange fury enter'd, now, my house,
And, with the dreadful tempest of her breath,
Did cleave my roof asunder.

LADY P
Believe me, and I
Had the most fearful dream, could I remember't—

VOLP
[ASIDE.]: Out on my fate! I have given her the occasion
How to torment me: she will tell me hers.

LADY P
Me thought, the golden mediocrity,
Polite and delicate—

VOLP
O, if you do love me,
No more; I sweat, and suffer, at the mention
Of any dream: feel, how I tremble yet.

LADY P
Alas, good soul! the passion of the heart.
Seed-pearl were good now, boil'd with syrup of apples,
Tincture of gold, and coral, citron-pills,
Your elicampane root, myrobalanes—

VOLP
[ASIDE.]: Ah me, I have ta'en a grass-hopper by the wing!

LADY P
Burnt silk, and amber: you have muscadel
Good in the house—

VOLP
You will not drink, and part?

LADY P
No, fear not that. I doubt, we shall not get
Some English saffron, half a dram would serve;
Your sixteen cloves, a little musk, dried mints,
Bugloss, and barley-meal—

VOLP
[ASIDE.]: She's in again!
Before I fain'd diseases, now I have one.

LADY P
And these applied with a right scarlet cloth.

VOLP
[ASIDE.]: Another flood of words! a very torrent!

LADY P
Shall I, sir, make you a poultice?

VOLP
No, no, no;
I am very well: you need prescribe no more.

LADY P
I have a little studied physic; but now,
I'm all for music, save, in the forenoons,
An hour or two for painting. I would have
A lady, indeed, to have all, letters, and arts,
Be able to discourse, to write, to paint,
But principal, as Plato holds, your music,
And, so does wise Pythagoras, I take it,
Is your true rapture: when there is concent
In face, in voice, and clothes: and is, indeed,
Our sex's chiefest ornament.

VOLP
The poet
As old in time as Plato, and as knowing,
Says that your highest female grace is silence.

LADY P
Which of your poets? Petrarch, or Tasso, or Dante?
Guarini? Ariosto? Aretine?
Cieco di Hadria? I have read them all.

VOLP
[ASIDE.]: Is every thing a cause to my distruction?

LADY P
I think I have two or three of them about me.

VOLP
[ASIDE.]: The sun, the sea will sooner both stand still,
Then her eternal tongue; nothing can 'scape it.

LADY P
Here's pastor Fido—

VOLP
[ASIDE.]: Profess obstinate silence,
That's now my safest.

LADY P
All our English writers,
I mean such as are happy in the Italian,
Will deign to steal out of this author, mainly:
Almost as much, as from Montagnie;
He has so modern and facile a vein,
Fitting the time, and catching the court-ear!
Your Petrarch is more passionate, yet he,
In days of sonetting, trusted them with much:
Dante is hard, and few can understand him.
But, for a desperate wit, there's Aretine;
Only, his pictures are a little obscene—
You mark me not.

VOLP
Alas, my mind is perturb'd.

LADY P
Why, in such cases, we must cure ourselves,
Make use of our philosophy—

VOLP
Oh me!

LADY P
And as we find our passions do rebel,
Encounter them with reason, or divert them,
By giving scope unto some other humour
Of lesser danger: as, in politic bodies,
There's nothing more doth overwhelm the judgment,
And cloud the understanding, than too much
Settling and fixing, and, as 'twere, subsiding
Upon one object. For the incorporating
Of these same outward things, into that part,
Which we call mental, leaves some certain faeces
That stop the organs, and as Plato says,
Assassinate our Knowledge.

VOLP
[ASIDE.]: Now, the spirit
Of patience help me!

LADY P
Come, in faith, I must
Visit you more a days; and make you well:
Laugh and be lusty.

VOLP
[ASIDE.]: My good angel save me!

LADY P
There was but one sole man in all the world,
With whom I e'er could sympathise; and he
Would lie you, often, three, four hours together
To hear me speak; and be sometimes so rapt,
As he would answer me quite from the purpose,
Like you, and you are like him, just. I'll discourse,
An't be but only, sir, to bring you asleep,
How we did spend our time and loves together,
For some six years.

VOLP
Oh, oh, oh, oh, oh, oh!

LADY P
For we were coaetanei, and brought up—

VOLP
Some power, some fate, some fortune rescue me!

[ENTER MOSCA.]

MOS
God save you, madam!

LADY P
Good sir.

VOLP
Mosca? welcome,
Welcome to my redemption.

MOS
Why, sir?

VOLP
Oh,
Rid me of this my torture, quickly, there;
My madam, with the everlasting voice:
The bells, in time of pestilence, ne'er made
Like noise, or were in that perpetual motion!
The Cock-pit comes not near it. All my house,
But now, steam'd like a bath with her thick breath.
A lawyer could not have been heard; nor scarce
Another woman, such a hail of words
She has let fall. For hell's sake, rid her hence.

MOS
Has she presented?

VOLP
O, I do not care;
I'll take her absence, upon any price,
With any loss.

MOS
Madam—

LADY P
I have brought your patron
A toy, a cap here, of mine own work.

MOS
'Tis well.
I had forgot to tell you, I saw your knight,
Where you would little think it.—

LADY P
Where?

MOS
Marry,
Where yet, if you make haste, you may apprehend,
Rowing upon the water in a gondole,
With the most cunning courtezan of Venice.

LADY P
Is't true?

MOS
Pursue them, and believe your eyes;
Leave me, to make your gift.
[EXIT LADY P. HASTILY.]
—I knew 'twould take:
For, lightly, they, that use themselves most license,
Are still most jealous.

VOLP
Mosca, hearty thanks,
For thy quick fiction, and delivery of me.
Now to my hopes, what say'st thou?

[RE-ENTER LADY P. WOULD-BE.]

LADY P
But do you hear, sir?—

VOLP
Again! I fear a paroxysm.

LADY P
Which way
Row'd they together?

MOS
Toward the Rialto.

LADY P
I pray you lend me your dwarf.

MOS
I pray you, take him.—
[EXIT LADY P.]
Your hopes, sir, are like happy blossoms, fair,
And promise timely fruit, if you will stay
But the maturing; keep you at your couch,
Corbaccio will arrive straight, with the Will;
When he is gone, I'll tell you more.

[EXIT.]

VOLP
My blood,
My spirits are return'd; I am alive:
And like your wanton gamester, at primero,
Whose thought had whisper'd to him, not go less,
Methinks I lie, and draw—for an encounter.

[THE SCENE CLOSES UPON VOLPONE.]