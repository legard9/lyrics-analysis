[Verse 1]
So many times you steal, my pride
Every time I write a wrong

[Chorus]
I'm falling in doubts
Breathing the sound
Nothing to say
You're lightning the town
Waving the crowd
You're the mess I made

[Verse 2]
I'm in the palm of your hands
So we will, sign my face with your red stain
I hope you still
We'll take it back I swear
If I have to fly miles
I'll reach you before your white dress

[Chorus]
I'm falling in doubts
Breathing the sound
Nothing to say
You're lightning the town
Waving the crowd
You're the wish I made
And the city lights
Playing the night
No where to hide
(Nana nana nana)

[Verse 3]
And I keep catching the same mistake
Coz no one ever made me feel the way
It feels to tell them about you
How we were

[Chorus]
I'm falling in doubts
Breathing the sound
Nothing to say
You're lightning the town
Waving the crowd
You're the wish I made