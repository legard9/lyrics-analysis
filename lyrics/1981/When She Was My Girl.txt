[Hook: Bru Castellano & (aTomic)]
There's some dark stories of men came before me
Some that is ten and those that is forty
Spirits don't bend and hearts get to roaring
Even when it's raining, even when it's pouring down (x2)
Even when it's pouring down (x2)
Even when it's raining, even when it's pouring
Even when it's pouring down
(Call it the Trish Triumph)
I am trying to overcome
(Call it the Trish Triumph)
Endure rain til come the sun
(Call it the Trish Triumph)
Wage war til we have won
(Call it the Trish Triumph)

[Verse 1: Bru Castellano]
I'll be damned if I don't speak about it
Mothers to daughters to brothers to fathers
To aunties to cousins to lovers to nanas to papas to toddlers
Summing dollars, in return we obtain some knowledge
Running boots polished, we know our mission
No one enlisting, so I call it conscription
Babyface or wrinkled face forced to be warriors
While I'm in my own world, lost in euphoria
I can rock a crowd and I can get they hands up
But WE can get loud and WE can find the answers
This is real and my girl Mama couldn't avoid it
Whether lungs or the liver, cancer tryna destroy it
It's up for debate, is it spirit or the power of the mind
No matter the cause, I'm in awe, the way her eyes shine
In the light, delighted I could join her in her fight
Something in her character excites me for life
Tryna put a finger on it, figure what it all mean
Right now I'm trucking through the tender age of seventeen
Year older than Trish when she was diagnosed
That struck me in a funny way, looking like I'd seen a ghost
Shoutout to Al Sovey, that's the big homie
Give the shirt off his back if you asked for it
Even if you didn't, good deeds, he ain't ask, he just did it
Life of a Samaritan, yeah he lived it
Until he started coughing lots, cancerous esophagus
Though it battered me, his faith had him pass happily
A real role model, overcame and endured
Clouds emerging, and when it rains, it pours
But we ain't running for no cover, yo we running for the cure
Some youngins spitting food for thought, stick it on a skewer
Light the bar-b, get it bumpin' like we kickin' off a tour
Battle with the cancer, help us win the war boi
Yeah, help us win the war

[Hook]

[Verse 2: aTomic]
I've seen too many people affected by this
Disease, it's taking lives, this evil virus
That we call cancer, I'm praying that my prayers get answered
But for now we just need to advance the
Search for an antidote, feeling choked up like a hand to throat
Cause people I know have had to handle those
Very same treatments, where surviving is achievement
Just don't forget it's you that we believe in
You and chemo standing in the ring for twelve rounds
You won this fight but still wearing that 'stal gown
You overthink it and eventually meltdown
Just keep your head and your hopes high that you well now
To all the brave kids, I don't know what you scared bout
Cause cutting it off is the reason I grew my hair out
Although you're beautiful with no hair on your head
I bet mine as a wig would fit better instead
And that's the least I could do
Keep you in my prayers as I reach out to Zeus
My nana still living but she affected too
And how long does she have? Man I wish I knew
My cousin Rachel passed away a dozen years ago
I remember being too young to even know
But I didn't need to meet her to see
That she's a hero, a soldier, and an angel with wings
Cause after that her parents plus her peers
Held a three pitch tournament every following year
And in that time all these little baseballers
Have collected and raised almost eight hundred thousand dollars
All towards finding a cure
When I talk about Trish it reminds me of her
She had it in her brain, Trish had it in her breast
Nonetheless I still feel it in my chest
We lost another one just this past Tuesday
When I got the news I did not know what to say
So I just picked up the pen and got to writing
This a tribute to those whose still fighting
So Trish if you listening, we standing behind you
We wearing all this pink just as a reminder
That the war has yet to be won, but we hopeful
Cause every Trish day we take another step closer to finding a cure

[Hook]