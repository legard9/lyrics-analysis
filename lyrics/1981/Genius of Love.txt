There's a funny thing about our love
That sets my heart in motion
Whenever I tell you what I'm thinking of
I kind of get the notion

I'm off on ride
Far within your mind
I'm out of control
I hope you will treat me kind

Cause I'm riding (in your car of love)
I'm in love and I'm going (in your car above)
I'm just a passenger (in your car of love)
Just don't slam on your breaks
My heart is at stake

You say your love for me is almost here
Just around the corner
But then when you arrive, you shift into gear
Try to run me over

And wreck the piece of my mind
The part will be so hard to find
To replace a very strong love
That I had for you for all of the time

I'm riding along (in your car of love)
I'm in love and I'm going (in your car above)
I'm just a passenger (in your car of love)
So please don't slam on your breaks
Cause if you stop, you know my poor heart will break