[Verse 1]
There is no end to this
I have seen your face
But I don't recognize all these things
You must have left behind
It's a problem, you know
That's been there all your life
I've tried to make you see the world without you
That just turned black and white
At night it gets cold
And you'd dearly like to turn away
Escape that fails
And makes the wounds that time won't heal

[Chorus 1]
Alone, alone, alone, alone

[Verse 2]
There is no end to this
I can't turn away another picture
But the scene
Is still the same
There is no room to move
Or try to look away
Remember, life is strange
And life keeps getting stranger every day
A mass of harmless attitudes
An attack that won’t subside
No matter what they say, you knew
Your heart beats you late at night

[Chorus 2]
Your heart beats you late at night
Your heart beats you late at night
Your heart beats you late at night