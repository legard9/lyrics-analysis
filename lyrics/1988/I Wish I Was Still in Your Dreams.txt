When I was young I used to want to be a polar bear ninja
Master of the shuriken, nunchucks and sabre
Now I'm older, I'm more comfortable with danger
Sometimes I still wish I was a polar bear ninja

They will not carry me in pine, they will scatter me
On the front doorstep of the first kid back at school who ever battered me
I take a pinch of salt with flattery
Cause who's line is it anyway?
Tony Slattery

Something had to be done, so I did it
Now I wish there was sixty-one seconds in every minute
To do what is never finished
In this mind all ideas rise to the top of the bubbles inside the Guinness
I will not know my limits

Cause it might be the difference between simply existing and really living
There is no prison like opinion given if two ears won't listen
You don't need eyes for vision, see, I can write five tunes decent
Release them every night to a crowd of unknown people
And if one of those people is seen to carry weight
And speaks in a magazine, if more people are going to believe them
Please, who are you dealing with?

I am not that creature
I'd rather cut my own left ear off than suffer these eijits
So much made with no reason
I want to leave a shape in the snow like a paw before I'm leaving

The wheel of fortune's spun
The needle's stuck between humdrum and one more tune done soon
I pass people in the street
I wonder how many feel completely controlled by a beat
We think we win once so we're told to repeat
But no matter who you know, we're all alone in defeat
And I take stock, like an Oxo thief

When I was young, I used to want to be a polar bear ninja
Master of the shuriken, nunchucks and sabre
Now I'm older, I'm more comfortable with danger
Sometimes I still wish I was a polar bear ninja

My eyes opened, thinking, not too tough
I guess I still don't know when I've had enough
Some things don't change
My old skull feels too small for my own brain
No pain, no gain, doesn't really apply to this one

? , get a tea
Have I got milk?  I'd better see
It's the habitual routine of a boy/man living his days
I blew one fifty last night
Now I'm digging for change

The Prince, the Pauper cliché, stuck in a loop
Either I,ve got dough and I blow it
Or I don't, and I fucking know it

That's the price you pay for living in the moment, you learn hard
When old friends are taking turns to buy their own yards
It kind of leaves you looking at your own cards
But I coughed this morning and some words came out of my mouth
Reminded me of what it is makes my world turn around

And so anyone who writes or plays
For nights and days
And fights to pay a rent
Might relate to the position of a visionary wishing to be given a wage
To exist as well as scribble on pages

The stage is set to make something out of nothing
Like The A Team
That's the reason why I never really sleep
I daydream

You see, I used to run with John
Back when Big Bird was yellow
He was Raphael, I was Donatello
Then when we moved to the big school I walked with Steven Strong
And every male friend of the family seemed to be an uncle
We chased the same girls, tamed the same curls
And our first shave in the very same sink, back at my Mum's

John would stay at mine, I'd stay at John's
We made beats on the Playstation for our MC alter egos to speak on
You're checking, this is the Polarbear, and I'm live!
I don't want four fish fingers with my beans and chips; I want five!
Know that, Nan!

The clarity of mentality shared
All we cared about was dreams and what we could do
We knew ?
Last time I saw John that "could" had become "should"
And he had a three haircut, ripped jeans and overly clean shoes

We went out, kicked a couple of rums
Just an educated couple of bums, sat at a bar
Me with no money, him with a fast car
He in a nine to five, and me living to write
And as I walked home that night
I thought hard
About how I had a new name for my long list of safe, but different, mates

See, people move in enough different ways and different rays
And all I could think was, broken up, I wouldn't go complaining
Cause see, the grass is always greener
But I'm happy with my own grass
What I'm after is a little bit of that green to buy my own gas
Started in my own class
Polarbear's "How to be Not Shit in Three Steps"
Now don't laugh

Step one:
Become extremely good at something
Step two:
Keep on getting better
And Step three, if you're wondering, is a warning:
If you're ever completely happy with what you've done
You're going to sleep thinking you're good
And wake up shit in the morning
I promise

Lesson over
Mr Polar
Go to the front of the class and show these so-called pros how they're supposed to do it
You know something's good when it seems like you already knew it
Now stop talking about that idea and do it
Get shit done, mate

Imagine writing something that reaches people
On a level, and equal
Leaves them needing a sequel
Getting straight to the point like the brand new needle
Like the guy who stepped over the street to get straight to the bar that's full of the right people

So there's a reason that I call it "The Scene"
It's cause it's not real
It's just a rôle play with the same characters, just change their hairstyle
We're used to getting vexed as I'm making this bear smile
And whatever blows your hair back
Just give me my free drink

If I was here for the money I'd be rich by now
If I listened to all the talk I'd be a bitch by now
And by "bitch" I don't mean a derogatory term for girls
I refer to the worms
Only concerned with what they hope is cool

Cause people, let's face it
If lyrical ability and brilliant delivery made you famous
Then every one of us here would be wearing
Limited Edition David Jay trainers

Why say in ten lines what you can say in just two
And why say in two lines what you can shut the fuck up and do