[Verse 1]
I can't believe I get to wake up next to you
Sometimes it's still hard for me to do
Feels like we've been on this wave, yeah
And to me it feels amazing
But, my biggest fear's in the years tearing us apart

[Chorus]
So I just gotta know
Do you still feel?
Do you still feel?
Do you still feel?
My love...
Do you still feel?
Do you still feel?
Do you still feel?
My love...

[Verse 2]
I don't wanna tell you what I'm thinking of
'Cause these thoughts are driving me insane
Mmm....
Yeah, I know I'm getting older
But I won't let our hearts get colder
No, my biggest fear's in the years tearing us apart

[Chorus]
So I just gotta know
Do you still feel?
Do you still feel?
Do you still feel?
My love...
Do you still feel?
Do you still feel?
Do you still feel?
My love...

[Bridge]
(Nanana...)
You've got my heart (nanana...)
You've got my soul
Do you still feel?
(Don't ever let me go)
Do you still feel my love?!

[Chorus]
Do you still feel?
Do you still feel?
Do you still feel?
My love...
Do you still feel?
Do you still feel?
Do you still feel?
My love