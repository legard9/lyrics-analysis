I'm drowning in the sea of love
I never could control you
I only wait around for you to land
I have no say in what you decide
From that first day I saw you
All sense of reason left me
You'll never know how you cut me inside
All the days I've ever known
I've watched you come and go
But when the tide runs high
You wonder what the world is coming to
I'm drowning in the sea of love
I've fallen from the stars above
Now I'm drowning in the sea of love
Feels like the longest winter
Feels like it's never ever gonna end
Feels like I've lost my will to defend
I can't remember summer
I don't believe in rainbows anymore
Since you cut the love on which I depend
All the days I've ever known
I've fought to stem the flow
But when the tide runs high
You wonder what your world is coming to
All my trials have left my soul addicted to you
It's not my style to give up
But what more can I do?