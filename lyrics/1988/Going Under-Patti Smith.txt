[Verse:]
Sun is rising on the water
Light is dancing again
Let's go under where the sun beams
Let's go under my friend
Are we sleeping
Are we dreaming
Are we dancing again
Is it heaven crack it open
And we'll slide down its stream
We can hold on (I'm sure)
To the sea's foaming mane
It will serve us
We'll surface
And we'll plunge back again
Sun is rising on the water
Light is dancing like a flame
There's no burning where the sun beams
Oh it's such a lovely game
Does the sea dream (I'm sure)
We are here, we attend
We are bells on the shore
As the tolling suspends
Who will decide the shape of things
The shift of being
Who will perceive when life is new
Shall we divide and become another
Who is due for gift upon gift
Who will decide
Shall we swim over and over
The curve of a wing
Its destination ever hanging
Sun is rising on the water
Light is dancing like a flame
Let's go waltzing on the water
Let's go under again
Let's go under
Going under