Hangul

어릴 적에 한 번쯤은 들어봤던
사랑 속에 미친다는 흔한 얘기

영화나 드라마에서만 나올 줄 알았던
이런 일이 나에게도 Hey
Woo-woo-huh-yeah

너 때문에 많이도 울었다는 그 가사
이해 못 했다가 이젠 이해할 수 있어

한줄기로 시작해 올라가 솟아난 감정 폭죽 Blah-lah-lah
주체 못 해 네 앞에서 난 아무 말이나 Blah-lah-lah

When I’m talking about love woo
When you’re talking about love woo
떨어져 있어도 믿어 우린
Thinking about us woo

When I’m talking about love woo
When you’re talking about love woo
나는 정말 네게 미친 걸까
Woo-woo-huh-yeah

우리 둘이라 괜찮아 Crazy in love crazy in love
때 묻지 않은 너와 나 Crazy in love crazy in love
나는 너를 믿어 의심하지 않아
과거 지금 미래도 Crazy in love crazy in love

Crazy in love crazy in love crazy in love
Crazy in love crazy in love crazy in love

시간이 지나도 의심하지 않아
과거 지금 미래도 Crazy in love crazy in love

얼음 얼음 얼렸다가
날 녹이는 너는 내게 유일한 불

사랑은 돌아온다는 영화 속 대사가
이해 안 갔다가 이젠 이해 가는 중이야

Zig zag 지금 내가 감지한 이 기운은
원초적인 기분은 맥박의 북을 쳐
내적 춤을 춰 너의 꿈을 꿔
어떡할래 나의 흥분을

When I’m talking about love woo
When you’re talking about love woo
떨어져 있어도 믿어 우린
Thinking about us woo

When I’m talking about love woo
When you’re talking about love woo
나는 정말 네게 미친 걸까
Woo-woo-huh-yeah

우리 둘이라 괜찮아 Crazy in love crazy in love
때 묻지 않은 너와 나 Crazy in love crazy in love
나는 너를 믿어 의심하지 않아
과거 지금 미래도 Crazy in love crazy in love

Crazy in love crazy in love crazy in love
Crazy in love crazy in love crazy in love

I fall in 바다같이 푸른 눈 속
아, 내 머리, 너란 파도에 벅차올라
Can’t stop it 느낌 woo
Can’t stop it 너를 woo
Can’t stop it crazy woo

Woo-woo-huh-yeah
Woo-woo-huh-yeah

우리 둘이라 괜찮아 Crazy in love crazy in love
때 묻지 않은 너와 나 Crazy in love crazy in love
나는 너를 믿어 의심하지 않아
과거 지금 미래도 Crazy in love crazy in love

Crazy in love crazy in love crazy in love
Crazy in love crazy in love crazy in love
시간이 지나도 의심하지 않아
과거 지금 미래도 Crazy in love crazy in love

Romanization

Eoril jeoge han beonjjeumeun deureobwassdeon
Sarang soge michindaneun heunhan yaegi

Yeonghwana deuramaeseoman naol jul arassdeon
Ireon iri naegedo Hey
Woo-woo-huh-yeah

Neo ttaemune manhido ureossdaneun geu gasa
Ihae mot haessdaga ijen ihaehal su isseo

Hanjulgiro sijakhae
Ollaga sosanan gamjeong pokjuk
Blah-lah-lah
Juche mot hae ne apeseo nan amu marina
Blah-lah-lah

When I’m talking about love woo
When you’re talking about love woo
Tteoreojyeo isseodo mideo urin
Thinking about us woo

When I’m talking about love woo
When you’re talking about love woo
Naneun jeongmal nege michin geolkka
Woo-woo-huh-yeah

Uri durira gwaenchanha
Crazy in love crazy in love
Ttae mutji anheun neowa na
Crazy in love crazy in love
Naneun neoreul mideo uisimhaji anha
Gwageo jigeum miraedo
Crazy in love crazy in love

Crazy in love crazy in love
Crazy in love
Crazy in love crazy in love
Crazy in love

Sigani jinado uisimhaji anha
Gwageo jigeum miraedo
Crazy in love crazy in love

Eoreum eoreum eollyeossdaga
Nal nogineun neoneun naege yuilhan bul

Sarangeun doraondaneun yeonghwa sok daesaga
Ihae an gassdaga ijen ihae ganeun jungiya

Zig zag jigeum naega gamjihan i giuneun
Wonchojeogin gibuneun maekbagui bugeul chyeo
Naejeok chumeul chwo neoui kkumeul kkwo
Eotteokhallae naui heungbuneul

When I’m talking about love woo
When you’re talking about love woo
Tteoreojyeo isseodo mideo urin
Thinking about us woo

When I’m talking about love woo
When you’re talking about love woo
나는 정말 네게 미친 걸까
Woo-woo-huh-yeah

Uri durira gwaenchanha
Crazy in love crazy in love
Ttae mutji anheun neowa na
Crazy in love crazy in love
Naneun neoreul mideo uisimhaji anha
Gwageo jigeum miraedo
Crazy in love crazy in love

Crazy in love crazy in love
Crazy in love
Crazy in love crazy in love
Crazy in love

I fall in badagati pureun nun sok
A nae meori neoran padoe beokchaolla
Can’t stop it neukkim woo
Can’t stop it neoreul woo
Can’t stop it crazy woo

Woo-woo-huh-yeah
Woo-woo-huh-yeah

Uri durira gwaenchanha
Crazy in love crazy in love
Ttae mutji anheun neowa na
Crazy in love crazy in love
Naneun neoreul mideo uisimhaji anha
Gwageo jigeum miraedo
Crazy in love crazy in love

Crazy in love crazy in love
Crazy in love
Crazy in love crazy in love
Crazy in love
Sigani jinado uisimhaji anha
Gwageo jigeum miraedo
Crazy in love crazy in love

English Translation

It’s what you hear at least once when you’re young
The same old story that you get crazy in love

What I thought only happened in movies or TV shows
Something like that is happening to me too Hey
Woo-woo-huh-yeah

The lyrics that say they cried a lot because of you
I didn’t understand before but I understand now

It started as one strand and it sprouted into an emotional fireworks Blah-lah-lah
I can’t resist, I’ll say anything in front of you Blah-lah-lah

When I’m talking about love woo
When you’re talking about love woo
We may be apart but we have faith
Thinking about us woo

When I’m talking about love woo
When you’re talking about love woo
Am I really crazy for you
Woo-woo-huh-yeah

It’s okay because it’s the two of us, Crazy in love crazy in love
You and I are untainted Crazy in love crazy in love
I believe you, I don’t doubt you
Past, present and future, Crazy in love crazy in love

Crazy in love crazy in love crazy in love
Crazy in love crazy in love crazy in love

I won’t doubt even when time passes
Past, present and future, Crazy in love crazy in love

Ice, ice I’m frozen
You are the only fire that melts me

There’s a line in a movie that says that love comes around
I didn’t understand before but I understand now

Zig zag this energy that I feel now
It’s a primal feeling that beats the drums of the pulse
I dance internally, I dream of you
What are you gonna do about my excitement

When I’m talking about love woo
When you’re talking about love woo
We may be apart but we have faith
Thinking about us woo

When I’m talking about love woo
When you’re talking about love woo
Am I really crazy for you
Woo-woo-huh-yeah

It’s okay because it’s the two of us, Crazy in love crazy in love
You and I are untainted Crazy in love crazy in love
I believe you, I don’t doubt you
Past, present and future, Crazy in love crazy in love

Crazy in love crazy in love crazy in love
Crazy in love crazy in love crazy in love

I fall in your eyes blue like the sea
Ah, my head, is overflowing with the waves that are you
Can’t stop it this feeling woo
Can’t stop it you woo
Can’t stop it crazy woo

Woo-woo-huh-yeah
Woo-woo-huh-yeah

It’s okay because it’s the two of us, Crazy in love crazy in love
You and I are untainted Crazy in love crazy in love
I believe you, I don’t doubt you
Past, present and future, Crazy in love crazy in love

Crazy in love crazy in love crazy in love
Crazy in love crazy in love crazy in love
I won’t doubt even when time passes
Past, present and future, Crazy in love crazy in love