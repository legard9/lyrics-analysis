The time is coming to do some soul searchin'
No more running, no more bridges burnin'
The time is comin' for you and me
To take a look inside, no matter what we see
Let's do some soul searchin', just you and me
Our lives are changin', this old world keeps turnin'
And I sit here wondering what we're really learnin'
I know you're just a woman, I'm just a man
Let's be true to each other, do the best we can
Let's do some soul searchin', soul searchin'
Some soul searchin', soul searchin'
Let's do some soul searchin', just you and me
There's a big train rollin', big train rollin'
I can hear it hummin', hear it hummin'
But the river is risin', river is risin'
And the rain keeps comin', rain keeps comin'
It's time to get onboard, before we're washed away
It's time to leave behind some things from yesterday
Let's do some soul searchin', soul searchin'
Little bit of soul searchin', soul searchin'
Let's do some soul searchin', just you and me
Come and talk to me
The time is comin', you know the time is comin'
To do some soul searchin', do some soul searchin'
No more runnin', they'll be no more runnin'
No more bridges burnin', no more bridges burnin'
It's time for every woman and every man
Build a new world together, workin' hand in hand
It's time to stop pretendin' that you're somebody else
You know you can't change this world, but you can change yourself
Let's do a little soul searchin', soul searchin'
Little bit of soul searchin', soul searchin'
Let's do some soul searchin', just you and me
We're gonna take a look inside, forget our foolish pride
It's time to search our soul yeah, search our souls
And make a good thing whole
Soul searchin', soul searchin', soul searchin'