In a world and in it's sounds
In any street in any town I go
There's a wreckage of desire
Of feelings never hired or sold
Where cellophane and thunder always come
And the factories of fantasies belong

I can only find the murder machine
I can only find the murder machine

From a notion of the times
To the hoodlum kitchen minds that die
From every mutant hour
To those who have the power who don't try
Where the surplus sounds of sanity grow hoarse
And an abject slave mentality gets worse

I can only find the murder machine
I can only find the murder machine