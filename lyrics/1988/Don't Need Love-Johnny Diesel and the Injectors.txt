John Axon was a railway man to steam trains born and bred
He was an engine driver at Edgeley loco shed
For forty years he followed and served the iron way
He lost his life upon the track one February day
The year was 1957, the morning bright and gay
On the 9th of February John Axon drove away
In a class 8 locomotive from Buxton he did go:
On the road to Chapel–en-le Frith his steam brake pipe did blow
It’s a seven-mile drop from Bibbington Top, oh Johnny
It’s 1 in 58 and you’ve no steam brake, oh Johnny
She’s picking up speed and the power is freed; it’s a prayer you’ll need
But you’ll never make it, Johnny
It’s hell on a plate, it’s a funeral freight, oh Johnny
It’s the end of a dream in steel and steam, oh Johnny
There’s a world in your head and you’re due at the shed and there’s life ahead
But you’ll never see it, Johnny

[CRASH]
It was 4 AM that Saturday John Axon left his bed
At five he drew his time-card at Edgeley loco shed
Just after six Ron Scanlon his fireman cried ‘Away’ -
It was a day no different from any other day
The iron road is a hard road and the work is never ending
Working night and day on the iron way -
We’re the boys who keep the engines rolling
You sign on at the loco shed, they put you through the cleaning
In your dungarees, cleaning Super D’s, you’re a –
Sweeper-upper, brewer-upper, shovel-slinger, spanner-bringer, steam-raiser
Fire-dropper, general-cook and bottle-washer, learning how to keep ‘em rolling
Hey lad will you fetch me a bucket of red oil for a red tail lamp
Charlie!
Hey Charlie!
On your toes – clean that muck out of number 5
Look alive there
Get weaving –
Where’ve you been for that oil, Arabia?
See the job on number 3 they’ve got to strip her
Ginger!
Leave the job you’re working on - help the fitter –
Hold the light, pass the wrench, a one-inch spanner up the bench, a one-inch reamer
Hey cleaner
Do this, do that, get me this, get me that, rush job on number 8, working late, got a date, I’ll
Never make it
You’ll have to break it
Just a bloody skivvy, that’s me
Two years, five years, ten years, fifteen years a cleaner!
When you’ve done your time at the loco shed and had your share of trouble
On the open plate you’re the driver’s mate and you’re married to a lousy shovel
It’s check the water, check the tools and chuck the blooming coal in
Give the gauge a wipe, check injector pipe
Now it’s swing your shovel at the double, give her rock, watch the clock, steam-raising
Sweat-running, back-aching, bone-shaking – Fireman, fireman, keep her rolling
You bend your back almost double
Feed that coal-hungry fire, swing that shovel, that’s a fireman’s trade
You’ve got your long-handled shovel
Three and a half feet of sweat-polished wood and a narrow steel blade
Swing your long-handled shovel
Hear that shovel ring
Swing your steel-bladed shovel
Hear the fire sing
Give us some rock, a round at a time, party your signal along the line…
Put your weight behind your shovel
From your middle, swing
Swing your steel-bladed shovel
From your shoulders, swing
One at the front, one at the back
One at each side, and that’s the knack
Sweat on your back, sweat in your eyes
Feed the fire, the steam’ll rise
Bend and thrust, lift and turn
There’s nine tons of coal to burn
Breathing steam, swallowing coal
Brace your legs to take the roll
Now fireman, come on, she’s lagging, get some rock on
You’ve got your long-handled shovel –
Then make it ring
Take your steel-bladed shovel –
On the tender ring
Sweating it out, eight hours a day. Earning your keep, on the iron way
When you’ve shovelled a million tons of coal
Some ten or twelve years later
And your only dream is of raising steam
Then they hand to you your driver’s papers
Home more tired than you did as a fireman’. And I never used to believe him, but it’s true
You’re on your own, mate
King of the footplate
You’ve got a load, mate, watch the road, mate
Get her through mate, it’s up to you, mate
She’s a class eight engine
She’s as tough as they come
Weighs well over 100 tons
She’s a puller, an iron horse
You’ve got nine tons of coal
You’ve got four thousand gallons of water
You’ve got a measure
Her boiler pressure is
Two hundred and twenty five pounds an inch
You’ve got a snorter
You give her water, you give her coal
Hand on the regulator, watch her roll
Mama, I swear as long as I live, going to serve me steam locomotive
Dirty tunnels, blinding smoke
Cover your head, mate, or else you’ll choke
Mama me heart and me soul I give, going to serve the steam locomotive
Got me paddle iron, that’s a ten foot spoon
Got me pricker and me dart like a long harpoon
Mama, I tell you positive, going to serve me steam locomotive
You’ve got to watch the line
And get her there in time
And keep her rolling
Keep your hand on the brake
She’s a monster, mate, that you’re controlling
You can sing the praises of the aviators
Rocket pilots and ocean navigators
Arctic explorers and deep sea divers, but me, I sing of the engine drivers
Mama, listen to me narrative, going to serve me steam locomotive
The iron road is a hard road and the work is never-ending
Working night and day on the iron way
With our
Loco drivers, early-risers, lodging-turners, Pile-burners, eleven-quid-a-week-earners -
We’re the boys who keep ‘em rolling
The rain was gently falling when they started down the line
And on the way to Buxton the sun began to shine
But the steam brake pipe was leaking and a wisp of steam did rise -
The fireman he reported this when in Buxton they arrived
Come all you British loco men who travel the Iron Way
There’s a long weekend and money to spend, it’s time to draw your pay
You’ve done your 80-hour fortnight and now it’s time for play
So off with your dirty dungarees, your time is yours today
We’ll give her some rock and we’ll beat the clock and send her on her way
For every train is an express train upon a Saturday
The missus is standing at the door, your dinner is on the hob
So bung your driver’s ticket in, forget all about the job -
Get dolled up in your Saturday best, the match’ll be starting soon
So hurry up mate and don’t be late - it’s Saturday afternoon
We’ll give her some rock and we’ll beat the clock and send her on her way
For every train is an express train upon a Saturday
There’s some that’s fond of gardening and some that like a gill
And some of the lads, they play the pools and sometimes make a kill
Some like a potato pie supper and an extra hour in bed
But everyone likes the moment when he signs off at the shed
We’ll give her some rock and we’ll beat the clock and send her on her way
For every train is an express train upon a Saturday
So come all you gallant loco men, steam and diesel too
You lads that serve the Iron Road, let’s drink a glass or two
And join me in the chorus, all you who like a tune -
The railwayman’s friend is the long weekend - and Saturday afternoon
So long to the driver’s lobby, so long the controller’s room
For while we’re here we can’t be there – on Saturday afternoon
The repair was done and the train made up
When they left in Buxton siding
And the time was just eleven-five
And the sun it was a-shining
Four eight one double eight was her number
Scanlon was the fireman
And the guard in the van was Alfred Ball
And the driver was John Axon
Her wagons numbered thirty-three
And a twenty-ton rear brake van
She was carrying coke, woodpulp and coal
And fire bricks and pig iron
The down line out of Buxton climbed
She was pulling nice and steady
And the bank engine was pushing behind
And the guard’s brake stick was ready
John Axon looked at the rolling hills
And he found them to his liking
And he thought of his early courting days
The days when he went hiking
I may be a wage slave on Monday
But I am a free man on Sunday
I’ve been over Snowdon, I’ve slept upon Crowdon
I’ve camped by the Wain Stones as well
I’ve sunbathed on Kinder, been burnt to a cinder, and many more things I can tell
Me rucksack has oft been my pillow
The heather has oft been my bed
And sooner than part from the mountains
I think I would sooner be dead
I’m a rambler, I’m a rambler from Manchester way
I get all me pleasure the hard moorland way -
I may be a wage slave on Monday
But I am a free man on Sunday
I once loved a maid, a spot welder by trade
She was fair as the rowan in bloom
And the blue in her eyes matched the blue moorland skies
And I wooed her from April till June
On the day that we should have been married
I went for a ramble instead
For sooner than part from the mountains
I think I would rather be dead
I’m a rambler, I’m a rambler from Manchester way
I get all my pleasure the hard moorland way -
I may be a wage slave on Monday
But I am a free man on Sunday
John Axon smiled at the thought that
Later he’d be celebrating
And he smiled when he thought of the Stockport pub
Where a pint of mild was waiting
John Axon was a dancing man
On his pins he was light and nimble
And often he’d stand on the old footplate
Whistling an old-time jingle
Come all you young maidens, take a warning from me -
Shun all engine drivers and their company
They’ll tell you they love you and all kinds of lies
But the one that he loves is the train that he drives
I once loved a fireman, he said he loved me
He took me out walking into the country
He hugged me and kissed me and gazed in my eyes
And said, You’re as nice as the eight forty-five
He said ‘My dear Molly, Oh won’t you be mine
Just give me the signal and let’s clear the line
My fires are all burning, me steam it is high
If you don’t take the brake off I think I shall die.’
I told him,Young fellow now don’t make so free
For no loco fireman shall ever have me -
He’ll hug you and kiss you and when you’re in need
He races away at the top of his speed
A sailor comes home when the voyage is done
A soldier gets weary of following the drum
A collier will cleave to his loved one for life
But the fireman’s one love is the engine, his wife
John Axon kept a little book
And in it there was written
The class, the type and the number of
Every engine he had driven
Steam train steam train
What’s your number what’s your name
Collecting trains, a fine game
I’ve got more than you
Waiting for the Arpley train
On the road to Cheadle
Saw the local passing by
Pop goes the diesel
I’ve got a Cornish
I’ve got a Lancastrian
I’ve got the Prince of Wales
My Dad’s a railwayman
Steam train, steam train
There goes a Super D
Write the name and number down
Four oh one ninety-three
Steam train, steam train, racing down the line
Thirty wagons full of coal and lime
Class eight, never late, she’ll arrive on time
Four eight one double eight
Pulling a lot of freight
Steam train on the line
Rattle rattle crash
Watch the Corny pass
Like a rocket flash
That’s the train for London
Chuggy chuggy chug
Like a little bug
Crawling on a rug -
That’s the shunting bogey
Class A’s an express passenger train
B’s an ordinary passenger train
C’s an express once again
Just for carrying parcels
E and F are freights we know
H is freight that travels slow
K’s a local train that goes
Between the local stations
Steam train, steam train
Carry me away with you
Steam train, steam train
Going to be a driver
Under the large injector steam valve
There’s a length of one and one-eighth piping
It connects with a driver’s brake valve
The connecting point is a joint of brass
A one and one-eighth steam pipe
Fixed in a threaded joint
Rests on asbestos packing
And is sealed –
Sealed with brazing metal
A hundred and twenty-five tons of engine
Six hundred and fifty tons behind
And the boiler pressure -
Two twenty-five pounds per square inch
And the men? Two fragile bodies
Flesh and blood and brittle bone
Carbon and water, nerves and dreams
Power from coal. Power from water
Power imprisoned in a one and one-eighth pipe
The restless steam
Watches the tired metal
Explores the worn thread
Watching, watching
Every turn of the four-foot wheels
Every lunge of the smooth-armed piston
Every thrust in the two great cylinders
Weakens the joint’s resistance
And the brazed flange crumbles
The pipe is parted -
IT BLOWS!

[Intense gush of steam]
The engine had reached the distant signal
When the broken steam pipe began to scream -
John Axon and his mate couldn’t reach the driver’s brake
For the cab was full of scalding steam, poor boys
The cab was full of scalding steam
John Axon he knew that his regulator
Was still wide open and on full power
He couldn’t turn it off for the way that it was blocked
And the cab was full of scalding steam, poor boys
The cab was full of scalding steam
They hung on the side and they both took turns
At shifting the regulator from afar
They prodded at the bar with the pricker and the dart
But they couldn’t move the iron bar, brave boys
They couldn’t move the iron bar
John Axon, he got to the fireman’s side
And over the scream of the steam did say -
We’ll have to get outside if we want to stay alive
Or this’ll be our dying day, poor boys
Or this’ll be our dying day
The guard, he was waiting to pin down the brakes
The train it didn’t slow down that day -
He stood in the van with the brake stick in his hand
And he knew she was a runaway, poor boy
He knew she was a runaway
John Axon, he cried to his fireman: Jump!
It is the only thing you can do
While I hang on the side and I’ll take a little ride
For I’ve got to see the journey through, brave boy
I’ve got to see the journey through
John Axon, he was all alone, there on the engine side
The train it reached the hilltop and began the downhill ride
The sun it was still shining, the sky was still as blue
He gambled with his life that day, and this John Axon knew
Ooooh…
You’re on your own, mate
Ooooh…
King of the footplate
Oooh Johnny, oooh Johnny
What makes you do the things you do, Johnny?
Oh why do you have to see it through, Johnny?
Oooh, oooh, oooh, Johnny
It’s a seven-mile drop from Bibbington Top, oh Johnny
It’s one in fifty-eight and you’ve no steam brake, oh Johnny
She’s picking up speed and the power is freed; it’s a prayer you’ll need
But you’ll never make it, Johnny
Every yard of the track says you won’t come back, Oh Johnny
She’s a fist of steel, every turn of the wheels cry Johnny
There isn’t a chance
You’ll get to your dance
You can see at a glance
That you’ll never make it Johnny
There’s a tunnel ahead, you can’t cover your head, oh Johnny
Doing sixty an hour and she’s gaining power, oh Johnny
Watch out for the wall -
Bunch yourself up small
In the smoky pall
Or you’ll never make it, Johnny
It’s hell on a plate, it’s a funeral freight, oh Johnny
It’s the end of a dream in steel and steam, oh Johnny
There’s a world in your head
And you’re due at the shed
And there’s life ahead
But you’ll never see it, Johnny
All alone now. Ron’s gone. On my own now, all the way, all the way
Never make it. How far’s all the way? There’s a gradient all the way into Whaley
Seven-mile gradient. One in seventy. One in sixty. One in fifty-eight
Wait!
Dove Hole’s passed
Going too fast to see if they saw me hanging outside the cab
Down the curving line, through the hill of limestone, Eaves tunnel
Every turn of the four foot wheels
Every lunge of the smooth-armed piston
Every thrust of the two great cylinders
Sings of a man’s destruction
Was I born for this?
To hang like a fly on an iron ball
Helpless, on a moving wall
To die, to end
In a welter of blood and oil
Twisted metal, splintered bone
What was it that Jim said, one day in the shed
Jim said, or was it in the pub
What was it that Jim said about steam, about power -
Curse the power
Curse the boiler pressure. The burning coal that made it. the fire and the air which fed it
Curse the water which boiled and turned to steam
Curse the steam brake and the nut which connects with the steam brake pipe
Curse the brass of the steam brake valve
Curse the nut of the steam brake valve
Curse the steam
The run it is finished, the shift’s nearly ended
So long, mates, so long, remember -
A man is a man, he must do what he can
For his brothers
By his deeds you shall know him
By the work of his hand
By the friends who will mourn him
By the love that he bore
By the gift of his courage
And the life that he gave

[Crash]
John Axon was a railway man to steam trains born and bred
He was an engine driver at Edgeley loco shed
He was a man of courage and served the iron way -
He gave his life upon the track one February day