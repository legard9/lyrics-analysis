ST. MARK'S PLACE; A RETIRED CORNER BEFORE CORVINO'S HOUSE.

ENTER SIR POLITICK WOULD-BE, AND PEREGRINE.

SIR P
Sir, to a wise man, all the world's his soil:
It is not Italy, nor France, nor Europe,
That must bound me, if my fates call me forth.
Yet, I protest, it is no salt desire
Of seeing countries, shifting a religion,
Nor any disaffection to the state
Where I was bred, and unto which I owe
My dearest plots, hath brought me out; much less,
That idle, antique, stale, gray-headed project
Of knowing men's minds, and manners, with Ulysses!
But a peculiar humour of my wife's
Laid for this height of Venice, to observe,
To quote, to learn the language, and so forth—
I hope you travel, sir, with license?

PER
Yes.

SIR P
I dare the safelier converse—How long, sir,
Since you left England?

PER
Seven weeks.

SIR P
So lately!
You have not been with my lord ambassador?

PER
Not yet, sir.

SIR P
Pray you, what news, sir, vents our climate?
I heard last night a most strange thing reported
By some of my lord's followers, and I long
To hear how 'twill be seconded.

PER
What was't, sir?

SIR P
Marry, sir, of a raven that should build
In a ship royal of the king's.

PER
[ASIDE.]: This fellow,
Does he gull me, trow? or is gull'd?
—Your name, sir.

SIR P
My name is Politick Would-be.

PER
[ASIDE.]: O, that speaks him.
—A knight, sir?

SIR P
A poor knight, sir.

PER
Your lady
Lies here in Venice, for intelligence
Of tires, and fashions, and behaviour,
Among the courtezans? the fine lady Would-be?

SIR P
Yes, sir; the spider and the bee, ofttimes,
Suck from one flower.

PER
Good Sir Politick,
I cry you mercy; I have heard much of you:
'Tis true, sir, of your raven.

SIR P
On your knowledge?

PER
Yes, and your lion's whelping, in the Tower.

SIR P
Another whelp!

PER
Another, sir.

SIR P
Now heaven!
What prodigies be these? The fires at Berwick!
And the new star! these things concurring, strange,
And full of omen! Saw you those meteors?

PER
I did, sir.

SIR P
Fearful! Pray you, sir, confirm me,
Were there three porpoises seen above the bridge,
As they give out?

PER
Six, and a sturgeon, sir.

SIR P
I am astonish'd.

PER
Nay, sir, be not so;
I'll tell you a greater prodigy than these.

SIR P
What should these things portend?

PER
The very day
(Let me be sure) that I put forth from London,
There was a whale discover'd in the river,
As high as Woolwich, that had waited there,
Few know how many months, for the subversion
Of the Stode fleet.

SIR P
Is't possible? believe it,
'Twas either sent from Spain, or the archdukes:
Spinola's whale, upon my life, my credit!
Will they not leave these projects? Worthy sir,
Some other news.

PER
Faith, Stone the fool is dead;
And they do lack a tavern fool extremely.

SIR P
Is Mass Stone dead?

PER
He's dead sir; why, I hope
You thought him not immortal?
[ASIDE.]
—O, this knight,
Were he well known, would be a precious thing
To fit our English stage: he that should write
But such a fellow, should be thought to feign
Extremely, if not maliciously.

SIR P
Stone dead!

PER
Dead.—Lord! how deeply sir, you apprehend it?
He was no kinsman to you?

SIR P
That I know of.
Well! that same fellow was an unknown fool.

PER
And yet you knew him, it seems?

SIR P
I did so. Sir,
I knew him one of the most dangerous heads
Living within the state, and so I held him.

PER
Indeed, sir?

SIR P
While he lived, in action.
He has received weekly intelligence,
Upon my knowledge, out of the Low Countries,
For all parts of the world, in cabbages;
And those dispensed again to ambassadors,
In oranges, musk-melons, apricocks,
Lemons, pome-citrons, and such-like: sometimes
In Colchester oysters, and your Selsey cockles.

PER
You make me wonder.

SIR P
Sir, upon my knowledge.
Nay, I've observed him, at your public ordinary,
Take his advertisement from a traveller
A conceal'd statesman, in a trencher of meat;
And instantly, before the meal was done,
Convey an answer in a tooth-pick.

PER
Strange!
How could this be, sir?

SIR P
Why, the meat was cut
So like his character, and so laid, as he
Must easily read the cipher.

PER
I have heard,
He could not read, sir.

SIR P
So 'twas given out,
In policy, by those that did employ him:
But he could read, and had your languages,
And to't, as sound a noddle—

PER
I have heard, sir,
That your baboons were spies, and that they were
A kind of subtle nation near to China:

SIR P
Ay, ay, your Mamuluchi. Faith, they had
Their hand in a French plot or two; but they
Were so extremely given to women, as
They made discovery of all: yet I
Had my advices here, on Wednesday last.
From one of their own coat, they were return'd,
Made their relations, as the fashion is,
And now stand fair for fresh employment.

PER
'Heart!
[ASIDE.]
This sir Pol will be ignorant of nothing.
—It seems, sir, you know all?

SIR P
Not all sir, but
I have some general notions. I do love
To note and to observe: though I live out,
Free from the active torrent, yet I'd mark
The currents and the passages of things,
For mine own private use; and know the ebbs,
And flows of state.

PER
Believe it, sir, I hold
Myself in no small tie unto my fortunes,
For casting me thus luckily upon you,
Whose knowledge, if your bounty equal it,
May do me great assistance, in instruction
For my behaviour, and my bearing, which
Is yet so rude and raw.

SIR P
Why, came you forth
Empty of rules, for travel?

PER
Faith, I had
Some common ones, from out that vulgar grammar,
Which he that cried Italian to me, taught me.

SIR P
Why this it is, that spoils all our brave bloods,
Trusting our hopeful gentry unto pedants,
Fellows of outside, and mere bark. You seem
To be a gentleman, of ingenuous race:—
I not profess it, but my fate hath been
To be, where I have been consulted with,
In this high kind, touching some great men's sons,
Persons of blood, and honour.—

[ENTER MOSCA AND NANO DISGUISED, FOLLOWED BY PERSONS WITH
MATERIALS FOR ERECTING A STAGE.]

PER
Who be these, sir?

MOS
Under that window, there 't must be. The same.

SIR P
Fellows, to mount a bank. Did your instructor
In the dear tongues, never discourse to you
Of the Italian mountebanks?

PER
Yes, sir.

SIR P
Why,
Here shall you see one.

PER
They are quacksalvers;
Fellows, that live by venting oils and drugs.

SIR P
Was that the character he gave you of them?

PER
As I remember.

SIR P
Pity his ignorance.
They are the only knowing men of Europe!
Great general scholars, excellent physicians,
Most admired statesmen, profest favourites,
And cabinet counsellors to the greatest princes;
The only languaged men of all the world!

PER
And, I have heard, they are most lewd impostors;
Made all of terms and shreds; no less beliers
Of great men's favours, than their own vile med'cines;
Which they will utter upon monstrous oaths:
Selling that drug for two-pence, ere they part,
Which they have valued at twelve crowns before.

SIR P
Sir, calumnies are answer'd best with silence.
Yourself shall judge.—Who is it mounts, my friends?

MOS
Scoto of Mantua, sir.

SIR P
Is't he? Nay, then
I'll proudly promise, sir, you shall behold
Another man than has been phant'sied to you.
I wonder yet, that he should mount his bank,
Here in this nook, that has been won't t'appear
In face of the Piazza!—Here, he comes.

[ENTER VOLPONE, DISGUISED AS A MOUNTEBANK DOCTOR, AND
FOLLOWED BY A CROWD OF PEOPLE.]

VOLP
[TO NANO.]: Mount zany.

MOB
Follow, follow, follow, follow!

SIR P
See how the people follow him! he's a man
May write ten thousand crowns in bank here. Note,
[VOLPONE MOUNTS THE STAGE.]
Mark but his gesture:—I do use to observe
The state he keeps in getting up.

PER
'Tis worth it, sir.

VOLP
Most noble gentlemen, and my worthy patrons! It may seem
strange, that I, your Scoto Mantuano, who was ever won't to fix
my bank in face of the public Piazza, near the shelter of the
Portico to the Procuratia, should now, after eight months'
absence from this illustrious city of Venice, humbly retire
myself into an obscure nook of the Piazza.

SIR P
Did not I now object the same?

PER
Peace, sir.

VOLP
Let me tell you: I am not, as your Lombard proverb saith,
cold on my feet; or content to part with my commodities at a
cheaper rate, than I accustomed: look not for it. Nor that the
calumnious reports of that impudent detractor, and shame to our
profession, (Alessandro Buttone, I mean,) who gave out, in
public, I was condemn'd a sforzato to the galleys, for
poisoning the cardinal Bembo's—cook, hath at all attached,
much less dejected me. No, no, worthy gentlemen; to tell you
true, I cannot endure to see the rabble of these ground
ciarlitani, that spread their cloaks on the pavement, as if
they meant to do feats of activity, and then come in lamely,
with their mouldy tales out of Boccacio, like stale Tabarine,
the fabulist: some of them discoursing their travels, and of
their tedious captivity in the Turks' galleys, when, indeed,
were the truth known, they were the Christians' galleys, where
very temperately they eat bread, and drunk water, as a
wholesome penance, enjoined them by their confessors, for base
pilferies.

SIR P
Note but his bearing, and contempt of these.

VOLP
These turdy-facy-nasty-paty-lousy-fartical rogues, with
one poor groat's-worth of unprepared antimony, finely wrapt up
in several scartoccios, are able, very well, to kill their
twenty a week, and play; yet, these meagre, starved spirits,
who have half stopt the organs of their minds with earthy
oppilations, want not their favourers among your shrivell'd
sallad-eating artizans, who are overjoyed that they may have
their half-pe'rth of physic; though it purge them into another
world, it makes no matter.

SIR P
Excellent! have you heard better language, sir?

VOLP
Well, let them go. And, gentlemen, honourable gentlemen,
know, that for this time, our bank, being thus removed from the
clamours of the canaglia, shall be the scene of pleasure and
delight; for I have nothing to sell, little or nothing to sell.

SIR P
I told you, sir, his end.

PER
You did so, sir.

VOLP
I protest, I, and my six servants, are not able to make
of this precious liquor, so fast as it is fetch'd away from my
lodging by gentlemen of your city; strangers of the Terra-firma;
worshipful merchants; ay, and senators too: who, ever since my
arrival, have detained me to their uses, by their splendidous
liberalities. And worthily; for, what avails your rich man to
have his magazines stuft with moscadelli, or of the purest
grape, when his physicians prescribe him, on pain of death,
to drink nothing but water cocted with aniseeds? O health!
health! the blessing of the rich, the riches of the poor! who
can buy thee at too dear a rate, since there is no enjoying
this world without thee? Be not then so sparing of your purses,
honourable gentlemen, as to abridge the natural course of life—

PER
You see his end.

SIR P
Ay, is't not good?

VOLP
For, when a humid flux, or catarrh, by the mutability of
air, falls from your head into an arm or shoulder, or any other
part; take you a ducat, or your chequin of gold, and apply to
the place affected: see what good effect it can work. No, no,
'tis this blessed unguento, this rare extraction, that hath
only power to disperse all malignant humours, that proceed
either of hot, cold, moist, or windy causes—

PER
I would he had put in dry too.

SIR P
'Pray you, observe.

VOLP
To fortify the most indigest and crude stomach, ay, were
it of one, that, through extreme weakness, vomited blood,
applying only a warm napkin to the place, after the unction
and fricace;—for the vertigine in the head, putting but a drop
into your nostrils, likewise behind the ears; a most sovereign
and approved remedy. The mal caduco, cramps, convulsions,
paralysies, epilepsies, tremor-cordia, retired nerves, ill
vapours of the spleen, stopping of the liver, the stone, the
strangury, hernia ventosa, iliaca passio; stops a disenteria
immediately; easeth the torsion of the small guts: and cures
melancholia hypocondriaca, being taken and applied according to
my printed receipt.
[POINTING TO HIS BILL AND HIS VIAL.]
For, this is the physician, this the medicine; this counsels,
this cures; this gives the direction, this works the effect;
and, in sum, both together may be termed an abstract of the
theorick and practick in the Aesculapian art. 'Twill cost you
eight crowns. And,—Zan Fritada, prithee sing a verse extempore
in honour of it.

SIR P
How do you like him, sir?

PER
Most strangely, I!

SIR P
Is not his language rare?

PER
But alchemy,
I never heard the like: or Broughton's books.

NANO
[SINGS.]: Had old Hippocrates, or Galen,
That to their books put med'cines all in,
But known this secret, they had never
(Of which they will be guilty ever)
Been murderers of so much paper,
Or wasted many a hurtless taper;
No Indian drug had e'er been famed,
Tabacco, sassafras not named;
Ne yet, of guacum one small stick, sir,
Nor Raymund Lully's great elixir.
Ne had been known the Danish Gonswart,
Or Paracelsus, with his long-sword.

PER
All this, yet, will not do, eight crowns is high.

VOLP
No more.—Gentlemen, if I had but time to discourse to you
the miraculous effects of this my oil, surnamed Oglio del Scoto;
with the countless catalogue of those I have cured of the
aforesaid, and many more diseases; the pattents and privileges of
all the princes and commonwealths of Christendom; or but the
depositions of those that appeared on my part, before the signiory
of the Sanita and most learned College of Physicians; where I was
authorised, upon notice taken of the admirable virtues of my
medicaments, and mine own excellency in matter of rare and unknown
secrets, not only to disperse them publicly in this famous city,
but in all the territories, that happily joy under the government
of the most pious and magnificent states of Italy. But may some
other gallant fellow say, O, there be divers that make profession
to have as good, and as experimented receipts as yours: indeed,
very many have assayed, like apes, in imitation of that, which is
really and essentially in me, to make of this oil; bestowed great
cost in furnaces, stills, alembecks, continual fires, and
preparation of the ingredients, (as indeed there goes to it six
hundred several simples, besides some quantity of human fat, for
the conglutination, which we buy of the anatomists,) but, when
these practitioners come to the last decoction, blow, blow, puff,
puff, and all flies in fumo: ha, ha, ha! Poor wretches! I rather
pity their folly and indiscretion, than their loss of time and
money; for these may be recovered by industry: but to be a fool
born, is a disease incurable.
For myself, I always from my youth have endeavoured to get the
rarest secrets, and book them, either in exchange, or for money;
I spared nor cost nor labour, where any thing was worthy to be
learned. And gentlemen, honourable gentlemen, I will undertake,
by virtue of chemical art, out of the honourable hat that covers
your head, to extract the four elements; that is to say, the
fire, air, water, and earth, and return you your felt without
burn or stain. For, whilst others have been at the Balloo, I
have been at my book; and am now past the craggy paths of study,
and come to the flowery plains of honour and reputation.

SIR P
I do assure you, sir, that is his aim.

VOLP
But, to our price—

PER
And that withal, sir Pol.

VOLP
You all know, honourable gentlemen, I never valued this
ampulla, or vial, at less than eight crowns, but for this time,
I am content, to be deprived of it for six; six crowns is the
price; and less, in courtesy I know you cannot offer me; take it,
or leave it, howsoever, both it and I am at your service. I ask
you not as the value of the thing, for then I should demand of
you a thousand crowns, so the cardinals Montalto, Fernese, the
great Duke of Tuscany, my gossip, with divers other princes, have
given me; but I despise money. Only to shew my affection to you,
honourable gentlemen, and your illustrious State here, I have
neglected the messages of these princes, mine own offices,
framed my journey hither, only to present you with the fruits of
my travels.—Tune your voices once more to the touch of your
instruments, and give the honourable assembly some delightful
recreation.

PER
What monstrous and most painful circumstance
Is here, to get some three or four gazettes,
Some three-pence in the whole! for that 'twill come to.

NANO
[SINGS.]: You that would last long, list to my song,
Make no more coil, but buy of this oil.
Would you be ever fair and young?
Stout of teeth, and strong of tongue?
Tart of palate? quick of ear?
Sharp of sight? of nostril clear?
Moist of hand? and light of foot?
Or, I will come nearer to't,
Would you live free from all diseases?
Do the act your mistress pleases;
Yet fright all aches from your bones?
Here's a med'cine, for the nones.

VOLP
Well, I am in a humour at this time to make a present of
the small quantity my coffer contains; to the rich, in
courtesy, and to the poor for God's sake. Wherefore now mark:
I ask'd you six crowns, and six crowns, at other times, you
have paid me; you shall not give me six crowns, nor five, nor
four, nor three, nor two, nor one; nor half a ducat; no, nor a
moccinigo. Sixpence it will cost you, or six hundred pound—
expect no lower price, for, by the banner of my front, I will
not bate a bagatine, that I will have, only, a pledge of your
loves, to carry something from amongst you, to shew I am not
contemn'd by you. Therefore, now, toss your handkerchiefs,
cheerfully, cheerfully; and be advertised, that the first
heroic spirit that deignes to grace me with a handkerchief, I
will give it a little remembrance of something, beside, shall
please it better, than if I had presented it with a double
pistolet.

PER
Will you be that heroic spark, sir Pol?
[CELIA AT A WINDOW ABOVE, THROWS DOWN HER HANDKERCHIEF.]
O see! the window has prevented you.

VOLP
Lady, I kiss your bounty; and for this timely grace you
have done your poor Scoto of Mantua, I will return you, over and
above my oil, a secret of that high and inestimable nature,
shall make you for ever enamour'd on that minute, wherein your
eye first descended on so mean, yet not altogether to be
despised, an object. Here is a powder conceal'd in this paper,
of which, if I should speak to the worth, nine thousand volumes
were but as one page, that page as a line, that line as a word;
so short is this pilgrimage of man (which some call life) to the
expressing of it. Would I reflect on the price? why, the whole
world is but as an empire, that empire as a province, that
province as a bank, that bank as a private purse to the purchase
of it. I will only tell you; it is the powder that made Venus a
goddess (given her by Apollo,) that kept her perpetually young,
clear'd her wrinkles, firm'd her gums, fill'd her skin, colour'd
her hair; from her deriv'd to Helen, and at the sack of Troy
unfortunately lost: till now, in this our age, it was as happily
recovered, by a studious antiquary, out of some ruins of Asia,
who sent a moiety of it to the court of France, (but much
sophisticated,) wherewith the ladies there, now, colour their
hair. The rest, at this present, remains with me; extracted to a
quintessence: so that, whereever it but touches, in youth it
perpetually preserves, in age restores the complexion; seats your
teeth, did they dance like virginal jacks, firm as a wall; makes
them white as ivory, that were black, as—

[ENTER CORVINO.]

COR
Spight o' the devil, and my shame! come down here;
Come down;—No house but mine to make your scene?
Signior Flaminio, will you down, sir? down?
What, is my wife your Franciscina, sir?
No windows on the whole Piazza, here,
To make your properties, but mine? but mine?
[BEATS AWAY VOLPONE, NANO, ETC.]
Heart! ere to-morrow, I shall be new-christen'd,
And call'd the Pantalone di Besogniosi,
About the town.

PER
What should this mean, sir Pol?

SIR P
Some trick of state, believe it. I will home.

PER
It may be some design on you:

SIR P
I know not.
I'll stand upon my guard.

PER
It is your best, sir.

SIR P
This three weeks, all my advices, all my letters,
They have been intercepted.

PER
Indeed, sir!
Best have a care.

SIR P
Nay, so I will.

PER
This knight,
I may not lose him, for my mirth, till night.

[EXEUNT.]