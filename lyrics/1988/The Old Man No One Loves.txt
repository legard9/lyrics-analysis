The scene represents the village street. To the left the outside of Peter's hut, built of logs, with a porch in the middle; to the right of the hut the gates and a corner of the yard buildings. Anísya is beating hemp in the street near the corner of the yard. Six months have elapsed since the First Act.

ANÍSYA
[stops and listens] Mumbling something again. He's probably got off the stove.

Akoulína enters, carrying two pails on a yoke.

ANÍSYA.
He's calling. You go and see what he wants, kicking up such a row.

AKOULÍNA.
Why don't you go?

ANÍSYA.
Go, I tell you! [Exit Akoulína into hut] He's bothering me to death. Won't let out where the money is, and that's all about it. He was out in the passage the other day. He must have been hiding it there. Now, I don't know myself where it is. Thank goodness he's afraid of parting with it, so that at least it will stay in the house. If only I could manage to find it. He hadn't it on him yesterday. Now I don't know where it can be. He has quite worn the life out of me.

Enter Akoulína, tying her kerchief over her head.

ANÍSYA.
Where are you off to?

AKOULÍNA.
Where? Why, he's told me to go for Aunt Martha. “Fetch my sister,” he says. “I am going to die,” he says. “I have a word to say to her.”

ANÍSYA
[aside] Asking for his sister? Oh my poor head! Sure he wants to give it her. What shall I do? Oh! [To Akoulína] Don't go! Where are you off to?

AKOULÍNA.
To call Aunt.

ANÍSYA.
Don't go I tell you, I'll go myself. You go and take the clothes to the river to rinse. Else you'll not have finished by the evening.

AKOULÍNA.
But he told me to go.

ANÍSYA.
You go and do as you're bid. I tell you I'll fetch Martha myself. Take the shirts off the fence.

AKOULÍNA.
The shirts? But maybe you'll not go. He's given the order.

ANÍSYA.
Didn't I say I'd go? Where's Nan?

AKOULÍNA.
Nan? Minding the calves.

ANÍSYA.
Send her here. I dare say they'll not run away. [Akoulína collects the clothes, and exit].

ANÍSYA.
If one doesn't go he'll scold. If one goes he'll give the money to his sister. All my trouble will be wasted. I don't myself know what I'm to do. My poor head's splitting. [Continues to work].

Enter Matryóna, with a stick and a bundle, in outdoor clothes.

MATRYÓNA.
May the Lord help you, honey.

ANÍSYA
[looks round, stops working, and claps her hands with joy] Well, I never expected this! Mother Matryóna, God has sent the right guest at the right time.

MATRYÓNA.
Well, how are things?

ANÍSYA.
Ah, I'm driven well-nigh crazy. It's awful!

MATRYÓNA.
Well, still alive, I hear?

ANÍSYA.
Oh, don't talk about it. He doesn't live and doesn't die!

MATRYÓNA.
But the money—has he given it to anybody?

ANÍSYA.
He's just sending for his sister Martha—probably about the money.

MATRYÓNA.
Well, naturally! But hasn't he given it to any one else?

ANÍSYA.
To no one. I watch like a hawk.

MATRYÓNA.
And where is it?

ANÍSYA.
He doesn't let out. And I can't find out in any way. He hides it now here, now there, and I can't do anything because of Akoulína. Idiot though she is, she keeps watch, and is always about. Oh my poor head! I'm bothered to death.

MATRYÓNA.
Oh, my jewel, if he gives the money to any one but you, you'll never cease regretting it as long as you live! They'll turn you out of house and home without anything. You've been worriting, and worriting all your life with one you don't love, and will have to go a-begging when you are a widow.

ANÍSYA.
No need to tell me, mother. My heart's that weary, and I don't know what to do. No one to get a bit of advice from. I told Nikíta, but he's frightened of the job. The only thing he did was to tell me yesterday it was hidden under the floor.

MATRYÓNA.
Well, and did you look there?

ANÍSYA.
I couldn't. The old man himself was in the room. I notice that sometimes he carries it about on him, and sometimes he hides it.

MATRYÓNA.
But you, my lass, must remember that if once he gives you the slip there's no getting it right again! [Whispering] Well, and did you give him the strong tea?

ANÍSYA.
Oh! oh!… [About to answer, but sees neighbour and stops].

The neighbour (a woman) passes the hut, and listens to a call from within.

NEIGHBOUR
[to Anísya] I say, Anísya! Eh, Anísya! There's your old man calling, I think.

ANÍSYA.
That's the way he always coughs,—just as if he were screaming. He's getting very bad.

NEIGHBOUR
[approaches Matryóna] How do you do, granny? Have you come far?

MATRYÓNA.
Straight from home, dear. Come to see my son. Brought him some shirts—can't help thinking of these things, you see, when it's one's own child.

NEIGHBOUR.
Yes, that's always so. [To Anísya] And I was thinking of beginning to bleach the linen, but it is a bit early, no one has begun yet.

ANÍSYA.
Where's the hurry?

MATRYÓNA.
Well, and has he had communion?

ANÍSYA.
Oh dear yes, the priest was here yesterday.

NEIGHBOUR.
I had a look at him yesterday. Dearie me! one wonders his body and soul keep together. And, O Lord, the other day he seemed just at his last gasp, so that they laid him under the holy icóns.[1] They started lamenting and got ready to lay him out.

ANÍSYA.
He came to, and creeps about again.

MATRYÓNA.
Well, and is he to have extreme unction?

ANÍSYA.
The neighbours advise it. If he lives till to-morrow we'll send for the priest.

NEIGHBOUR.
Oh, Anísya dear, I should think your heart must be heavy. As the saying goes, “Not he is sick that's ill in bed, but he that sits and waits in dread.”

ANÍSYA.
Yes, if it were only over one way or other!

NEIGHBOUR.
Yes, that's true, dying for a year, it's no joke. You're bound hand and foot like that.

MATRYÓNA.
Ah, but a widow's lot is also bitter. It's all right as long as one's young, but who'll care for you when you're old? Oh yes, old age is not pleasure. Just look at me. I've not walked very far, and yet am so footsore I don't know how to stand. Where's my son?

ANÍSYA.
Ploughing. But you come in and we'll get the samovár ready; the tea'll set you up again.

MATRYÓNA
[sitting down] Yes, it's true, I'm quite done up, my dears. As to extreme unction, that's absolutely necessary. Besides, they say it's good for the soul.

ANÍSYA.
Yes, we'll send to-morrow.

MATRYÓNA.
Yes, you had better. And we've had a wedding down in our parts.

NEIGHBOUR.
What, in spring?[2]

MATRYÓNA.
Ah, now if it were a poor man, then, as the saying is, it's always unseasonable for a poor man to marry. But it's Simon Matvéyitch, he's married that Marína.

ANÍSYA.
What luck for her!

NEIGHBOUR.
He's a widower. I suppose there are children?

MATRYÓNA.
Four of 'em. What decent girl would have him! Well, so he's taken her, and she's glad. You see, the vessel was not sound, so the wine trickled out.

NEIGHBOUR.
Oh my! And what do people say to it? And he, a rich peasant!

MATRYÓNA.
They are living well enough so far.

NEIGHBOUR.
Yes, it's true enough. Who wants to marry where there are children? There now, there's our Michael. He's such a fellow, dear me …

PEASANT'S VOICE.
Hullo, Mávra. Where the devil are you? Go and drive the cow in.

Exit Neighbour.

MATRYÓNA
[while the Neighbour is within hearing speaks in her ordinary voice] Yes, lass, thank goodness, she's married. At any rate my old fool won't go bothering about Nikíta. Now [suddenly changing her tone], she's gone! [Whispers] I say, did you give him the tea?

ANÍSYA.
Don't speak about it. He'd better die of himself. It's no use—he doesn't die, and I have only taken a sin on my soul. O-oh, my head, my head! Oh, why did you give me those powders?

MATRYÓNA.
What of the powders? The sleeping powders, lass,—why not give them? No evil can come of them.

ANÍSYA.
I am not talking of the sleeping ones, but the others, the white ones.

MATRYÓNA.
Well, honey, those powders are medicinal.

ANÍSYA
[sighs] I know, yet it's frightening. Though he's worried me to death.

MATRYÓNA.
Well, and did you use many?

ANÍSYA.
I gave two doses.

MATRYÓNA.
Was anything noticeable?

ANÍSYA.
I had a taste of the tea myself—just a little bitter. And he drank them with the tea and says, “Even tea disgusts me,” and I say, “Everything tastes bitter when one's sick.” But I felt that scared, mother.

MATRYÓNA.
Don't go thinking about it. The more one thinks the worse it is.

ANÍSYA.
I wish you'd never given them to me and led me into sin. When I think of it something seems to tear my heart. Oh dear, why did you give them to me?

MATRYÓNA.
What do you mean, honey? Lord help you! Why are you turning it on to me? Mind, lass, don't go twisting matters from the sick on to the healthy. If anything were to happen, I stand aside! I know nothing! I'm aware of nothing! I'll kiss the cross on it; I never gave you any kind of powders, never saw any, never heard of any, and never knew there were such powders. You think about yourself, lass. Why, we were talking about you the other day. “Poor thing, what torture she endures. The step-daughter an idiot; the old man rotten, sucking her life-blood. What wouldn't one be ready to do in such a case!”

ANÍSYA.
I'm not going to deny it. A life such as mine could make one do worse than that. It could make you hang yourself or throttle him. Is this a life?

MATRYÓNA.
That's just it. There's no time to stand gaping; the money must be found one way or other, and then he must have his tea.

ANÍSYA.
O-oh, my head, my head! I can't think what to do. I am so frightened; he'd better die of himself. I don't want to have it on my soul.

MATRYÓNA
[viciously] And why doesn't he show the money? Does he mean to take it along with him? Is no one to have it? Is that right? God forbid such a sum should be lost all for nothing. Isn't that a sin? What's he doing? Is he worth considering?

ANÍSYA.
I don't know anything. He's worried me to death.

MATRYÓNA.
What is it you don't know? The business is clear. If you make a slip now, you'll repent it all your life. He'll give the money to his sister and you'll be left without.

ANÍSYA.
O-oh dear! Yes, and he did send for her—I must go.

MATRYÓNA.
You wait a bit and light the samovár first. We'll give him some tea and search him together—we'll find it, no fear.

ANÍSYA.
Oh dear, oh dear; supposing something were to happen.

MATRYÓNA.
What now? What's the good of waiting? Do you want the money to slip from your hand when it's just in sight? You go and do as I say.

ANÍSYA.
Well, I'll go and light the samovár.

MATRYÓNA.
Go, honey, do the business so as not to regret it afterwards. That's right! [Anísya turns to go. Matryóna calls her back].

MATRYÓNA.
Just a word. Don't tell Nikíta about the business. He's silly. God forbid he should find out about the powders. The Lord only knows what he would do. He's so tender-hearted. D'you know, he usen't to be able to kill a chicken. Don't tell him. 'Twould be a fine go, he wouldn't understand things. [Stops horror-struck as Peter appears in the doorway].

PETER
[holding on to the wall, creeps out into the porch and calls with a faint voice] How's it one can't make you hear? Oh, oh, Anísya! Who's there? [Drops on the bench].

ANÍSYA
[steps from behind the corner] Why have you come out? You should have stayed where you were lying.

PETER.
Has the girl gone for Martha? It's very hard.… Oh, if only death would come quicker!

ANÍSYA.
She had no time. I sent her to the river. Wait a bit, I'll go myself when I'm ready.

PETER.
Send Nan. Where's she? Oh, I'm that bad! Oh, death's at hand!

ANÍSYA.
I've sent for her already.

PETER.
Oh dear! Then where is she?

ANÍSYA.
Where's she got to, the plague seize her!

PETER.
Oh, dear! I can't bear it. All my inside's on fire. It's as if a gimlet were boring me. Why have you left me as if I were a dog?… no one to give me a drink.… Oh … send Nan to me.

ANÍSYA.
Here she is. Nan, go to father.

Nan runs in. Anísya goes behind the corner of the house.

PETER.
Go you. Oh … to Aunt Martha, tell her father wants her; say she's to come, I want her.

NAN.
All right.

PETER.
Wait a bit. Tell her she's to come quick. Tell her I'm dying. O-oh!

NAN.
I'll just get my shawl and be off. [Runs off].

MATRYÓNA
[winking] Now then, mind and look sharp, lass. Go into the hut, hunt about everywhere, like a dog that's hunting for fleas: look under everything, and I'll search him.

ANÍSYA
[to Matryóna] I feel a bit bolder, somehow, now you're here. [Goes up to porch. To Peter] Hadn't I better light the samovár? Here's Mother Matryóna come to see her son; you'll have a cup of tea with her?

PETER.
Well then, light it. [Anísya goes into the house. Matryóna comes up to the porch].

PETER.
How do you do?

MATRYÓNA
[bowing] How d'you do, my benefactor; how d'you do, my precious … still ill, I see. And my old man, he's that sorry! “Go,” says he, “see how he's getting on.” He sends his respects to you. [Bows again].

PETER.
I'm dying.

MATRYÓNA.
Ah yes, Peter Ignátitch, now I look at you I see, as the saying has it, “Sickness lives where men live.” You've shrivelled, shrivelled, all to nothing, poor dear, now I come to look at you. Seems illness does not add to good looks.

PETER.
My last hour has come.

MATRYÓNA.
Oh well, Peter Ignátitch, it's God's will you know, you've had communion, and you'll have unction, God willing. Your missus is a wise woman, the Lord be thanked; she'll give you a good burial, and have prayers said for your soul, all most respectable! And my son, he'll look after things meanwhile.

PETER.
There'll be no one to manage things! She's not steady. Has her head full of folly—why, I know all about it, I know. And my girl is silly and young. I've got the homestead together, and there's no one to attend to things. One can't help feeling it. [Whimpers].

MATRYÓNA.
Why, if it's money, or something, you can leave orders.

PETER
[to Anísya inside the house] Has Nan gone?

MATRYÓNA
[aside] There now, he's remembered!

ANÍSYA
[from inside] She went then and there. Come inside, won't you? I'll help you in.

PETER.
Let me sit here a bit for the last time. The air's so stuffy inside. Oh, how bad I feel! Oh, my heart's burning.… Oh, if death would only come.

MATRYÓNA.
If God don't take a soul, the soul can't go out. Death and life are in God's will, Peter Ignátitch. You can't be sure of death either. Maybe you'll recover yet. There was a man in our village just like that, at the very point of death …

PETER.
No, I feel I shall die to-day, I feel it. [Leans back and shuts his eyes].

ANÍSYA
[enters] Well now, are you coming in or not? You do keep one waiting. Peter! eh, Peter!

MATRYÓNA
[steps aside and beckons to Anísya with her finger] Well?

ANÍSYA
[comes down the porch steps] Not there.

MATRYÓNA.
But have you searched everywhere? Under the floor?

ANÍSYA.
No, it's not there either. In the shed perhaps; he was rummaging there yesterday.

MATRYÓNA.
Go, search, search for all you're worth. Go all over everywhere, as if you licked with your tongue! But I see he'll die this very day, his nails are turning blue and his face looks earthy. Is the samovár ready?

ANÍSYA.
Just on the boil.

NIKÍTA
[comes from the other side, if possible on horseback, up to the gate, and does not see Peter. To Matryóna] How d'you do, mother, is all well at home?

MATRYÓNA.
The Lord be thanked, we're all alive and have a crust to bite.

NIKÍTA.
Well, and how's master?

MATRYÓNA.
Hush, there he sits. [Points to porch].

NIKÍTA.
Well, let him sit. What's it to me?

PETER
[opens his eyes] Nikíta, I say, Nikíta, come here! [Nikíta approaches. Anísya and Matryóna whisper together].

PETER.
Why have you come back so early?

NIKÍTA.
I've finished ploughing.

PETER.
Have you done the strip beyond the bridge?

NIKÍTA.
It's too far to go there.

PETER.
Too far? From here it's still farther. You'll have to go on purpose now. You might have made one job of it. [Anísya, without showing herself, stands and listens].

MATRYÓNA
[approaches] Oh, sonnie, why don't you take more pains for your master? Your master is ill and depends on you; you should serve him as you would your own father, straining every muscle just as I always tell you to.

PETER.
Well then—o-oh!… Get out the seed potatoes, and the women will go and sort them.

ANÍSYA
[aside] No fear, I'm not going. He's again sending every one away; he must have the money on him now, and wants to hide it somewhere.

PETER.
Else … o-oh! when the time comes for planting, they'll all be rotten. Oh, I can't stand it! [Rises].

MATRYÓNA
[runs up into the porch and holds Peter up] Shall I help you into the hut?

PETER.
Help me in. [Stops] Nikíta!

NIKÍTA
[angrily] What now?

PETER.
I shan't see you again … I'll die to-day.… Forgive me,[3] for Christ's sake, forgive me if I have ever sinned against you … If I have sinned in word or deed … There's been all sorts of things. Forgive me!

NIKÍTA.
What's there to forgive? I'm a sinner myself.

MATRYÓNA.
Ah, sonnie, have some feeling.

PETER.
Forgive me, for Christ's sake. [Weeps].

NIKÍTA
[snivels] God will forgive you, Daddy Peter. I have no cause to complain of you. You've never done me any wrong. You forgive me; maybe I've sinned worse against you. [Weeps].

Peter goes in whimpering, Matryóna supporting him.

ANÍSYA.
Oh, my poor head! It's not without some reason he's hit on that. [Approaches Nikíta] Why did you say the money was under the floor? It's not there.

NIKÍTA
[does not answer, but cries] I have never had anything bad from him, nothing but good, and what have I gone and done!

ANÍSYA.
Enough now! Where's the money?

NIKÍTA
[angrily] How should I know? Go and look for it yourself!

ANÍSYA.
What's made you so tender?

NIKÍTA.
I am sorry for him,—that sorry. How he cried! Oh dear!

ANÍSYA.
Look at him,—seized with pity! He has found someone to pity too! He's been treating you like a dog, and even just now was giving orders to have you turned out of the house. You'd better show me some pity!

NIKÍTA.
What are you to be pitied for?

ANÍSYA.
If he dies, and the money's been hidden away …

NIKÍTA.
No fear, he'll not hide it …

ANÍSYA.
Oh, Nikíta darling! he's sent for his sister, and wants to give it to her. It will be a bad lookout for us. How are we going to live, if he gives her the money? They'll turn me out of the house! You try and manage somehow! You said he went to the shed last night.

NIKÍTA.
I saw him coming from there, but where he's shoved it to, who can tell?

ANÍSYA.
Oh, my poor head! I'll go and have a look there. [Nikíta steps aside].

MATRYÓNA
[comes out of the hut and down the steps of the porch to Anísya and Nikíta] Don't go anywhere. He's got the money on him. I felt it on a string round his neck.

ANÍSYA.
Oh my head, my head!

MATRYÓNA.
If you don't keep wide awake now, then you may whistle for it. If his sister comes—then good-bye to it!

ANÍSYA.
That's true. She'll come and he'll give it her. What's to be done? Oh my poor head!

MATRYÓNA.
What is to be done? Why, look here; the samovár is boiling, go and make the tea and pour him out a cup, and then [whispers] put in all that's left in the paper. When he's drunk the cup, then just take it. He'll not tell, no fear.

ANÍSYA.
Oh! I'm afeared!

MATRYÓNA.
Don't be talking now, but look alive, and I'll keep his sister off if need be. Mind, don't make a blunder! Get hold of the money and bring it here, and Nikíta will hide it.

ANÍSYA.
Oh my head, my head! I don't know how I'm going to …

MATRYÓNA.
Don't talk about it I tell you, do as I bid you. Nikíta!

NIKÍTA.
What is it?

MATRYÓNA.
You stay here—sit down—in case something is wanted.

NIKÍTA
[waves his hand] Oh these women, what won't they be up to? Muddle one up completely. Bother them! I'll really go and fetch out the potatoes.

MATRYÓNA
[catches him by the arm] Stay here, I tell you.

Nan enters.

ANÍSYA.
Well?

NAN.
She was down in her daughter's vegetable plot—she's coming.

ANÍSYA.
Coming! What shall we do?

MATRYÓNA.
There's plenty of time if you do as I tell you.

ANÍSYA.
I don't know what to do; I know nothing, my brain's all in a whirl. Nan! Go, daughter, and see to the calves, they'll have run away, I'm afraid.… Oh dear, I haven't the courage.

MATRYÓNA.
Go on! I should think the samovár's boiling over.

ANÍSYA.
Oh my head, my poor head! [Exit].

MATRYÓNA
[approaches Nikíta] Now then, sonnie. [Sits down beside him] Your affairs must also be thought about, and not left anyhow.

NIKÍTA.
What affairs?

MATRYÓNA.
Why, this affair—how you're to live your life.

NIKÍTA.
How to live my life? Others live, and I shall live!

MATRYÓNA.
The old man will probably die to-day.

NIKÍTA.
Well, if he dies, God give him rest! What's that to me?

MATRYÓNA
[keeps looking towards the porch while she speaks] Eh, sonnie! Those that are alive have to think about living. One needs plenty of sense in these matters, honey. What do you think? I've tramped all over the place after your affairs, I've got quite footsore bothering about matters. And you must not forget me when the time comes.

NIKÍTA.
And what's it you've been bothering about?

MATRYÓNA.
About your affairs, about your future. If you don't take trouble in good time you'll get nothing. You know Iván Moséitch? Well, I've been to him too. I went there the other day. I had something else to settle, you know. Well, so I sat and chatted awhile and then came to the point. “Tell me, Iván Moséitch,” says I, “how's one to manage an affair of this kind? Supposing,” says I, “a peasant as is a widower married a second wife, and supposing all the children he has is a daughter by the first wife, and a daughter by the second. Then,” says I, “when that peasant dies, could an outsider get hold of the homestead by marrying the widow? Could he,” says I, “give both the daughters in marriage and remain master of the house himself?” “Yes, he could,” says he, “but,” says he, “it would mean a deal of trouble; still the thing could be managed by means of money, but if there's no money it's no good trying.”

NIKÍTA
[laughs] That goes without saying, only fork out the money. Who does not want money?

MATRYÓNA.
Well then, honey, so I spoke out plainly about the affair. And he says, “First and foremost, your son will have to get himself on the register of that village—that will cost something. The elders will have to be treated. And they, you see, they'll sign. Everything,” says he, “must be done sensibly.” Look, [unwraps her kerchief and takes out a paper] he's written out this paper; just read it, you're a scholar, you know. [Nikíta reads].

NIKÍTA.
This paper's only a decision for the elders to sign. There's no great wisdom needed for that.

MATRYÓNA.
But you just hear what Iván Moséitch bids us do. “Above all,” he says, “mind and don't let the money slip away, dame. If she don't get hold of the money,” he says, “they'll not let her do it. Money's the great thing!” So look out, sonnie, things are coming to a head.

NIKÍTA.
What's that to me? The money's hers—so let her look out.

MATRYÓNA.
Ah, sonnie, how you look at it! How can a woman manage such affairs? Even if she does get the money, is she capable of arranging it all? One knows what a woman is! You're a man anyhow. You can hide it, and all that. You see, you've after all got more sense, in case of anything happening.

NIKÍTA.
Oh, your woman's notions are all so inexpedient!

MATRYÓNA.
Why inexpedient? You just collar the money, and the woman's in your hands. And then should she ever turn snappish you'd be able to tighten the reins!

NIKÍTA.
Bother you all,—I'm going.

ANÍSYA
[quite pale, runs out of the hut and round the corner to Matryóna] So it was, it was on him! Here it is! [Shows that she has something under her apron].

MATRYÓNA.
Give it to Nikíta, he'll hide it. Nikíta, take it and hide it somewhere.

NIKÍTA.
All right, give here!

ANÍSYA.
O-oh, my poor head! No, I'd better do it myself. [Goes towards the gate].

MATRYÓNA
[seizing her by the arm] Where are you going to? You'll be missed. There's the sister coming; give it him; he knows what to do. Eh, you blockhead!

ANÍSYA
[stops irresolutely] Oh, my head, my head!

NIKÍTA.
Well, give it here. I'll shove it away somewhere.

ANÍSYA.
Where will you shove it to?

NIKÍTA
[laughing] Why, are you afraid?

Enter Akoulína, carrying clothes from the wash.

ANÍSYA.
O-oh, my poor head! [Gives the money] Mind, Nikíta.

NIKÍTA.
What are you afraid of? I'll hide it so that I'll not be able to find it myself. [Exit].

ANÍSYA
[stands in terror] Oh dear, and supposing he …

MATRYÓNA.
Well, is he dead?

ANÍSYA.
Yes, he seems dead. He did not move when I took it.

MATRYÓNA.
Go in, there's Akoulína.

ANÍSYA.
Well there, I've done the sin and he has the money.…

MATRYÓNA.
Have done and go in! There's Martha coming!

ANÍSYA.
There now, I've trusted him. What's going to happen now? [Exit].

MARTHA
[enters from one side, Akoulína enters from the other. To Akoulína] I should have come before, but I was at my daughter's. Well, how's the old man? Is he dying?

AKOULÍNA
[puts down the clothes] Don't know, I've been to the river.

MARTHA
[pointing to Matryóna] Who's that?

MATRYÓNA.
I'm from Zoúevo. I'm Nikíta's mother from Zoúevo, my dearie. Good afternoon to you. He's withering, withering away, poor dear—your brother, I mean. He came out himself. “Send for my sister,” he said, “because,” said he … Dear me, why, I do believe, he's dead!

ANÍSYA
[runs out screaming. Clings to a post, and begins wailing][4] Oh, oh, ah! who-o-o-m have you left me to, why-y-y have you dese-e-e-e-rted me—a miserable widow … to live my life alone … Why have you closed your bright eyes …

Enter Neighbour. Matryóna and Neighbour catch hold of Anísya under the arms to support her. Akoulína and Martha go into the hut. A crowd assembles.

A VOICE IN THE CROWD.
Send for the old women to lay out the body.

MATRYÓNA
[rolls up her sleeves] Is there any water in the copper? But I daresay the samovár is still hot. I'll also go and help a bit.

Footnotes:

[1] It is customary to place a dying person under the icón. One or more icóns hang in the hut of each Orthodox peasant.

[2] Peasant weddings are usually in autumn. They are forbidden in Lent, and soon after Easter the peasants become too busy to marry till harvest is over.

[3] A formal request for forgiveness is customary among Russians, but it is often no mere formality. Nikíta's first reply is evasive; his second reply, “God will forgive you,” is the correct one sanctioned by custom.

[4] Loud public wailing of this kind is customary, and considered indispensable, among the peasants.