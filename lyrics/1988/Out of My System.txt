[Intro: Copywrite talking]
Yeah, I got some shit on my mind man. I know I'm not the only one. Highs and lows, we all go through 'em. There's some highs right now but I'm definitely feeling the lows. Stress off my chest
"When you got things on your mind"

[Verse One: Copywrite]
I burn blunts back to back
One after that, one after that, one more
Who am I kidding?
Prolly burn a bunch more
On the front porch tryin' to reach a zone to each his own
Rather be alone with a blunt torch than speak to foes
I got problems layered on top of problems player
I'm at the bottom with not a prayer
My cake's not in layers
And ain't no chef to complain to
So I'm adjusting my aim to top you haters
I'm pop
But they don't wanna see it, told 'em to close their eyes
Been years in the making, told 'em it's no surprise
Spit tears, blood and sweat
So those who close your eyes, open 'em
You ain't seen nothin' yet
O.H. 10, put your ?? prints in the cement
We're going nowhere that we leavin' but we came to leave with
I got shit on my mind, it's tellin' me to get on my grind
And get in where you fit in, ain't no ditchin' in line
Now...

[Hook: Tage Pronto]
Slow down, you won't get the best of me
Fulfilling the destiny, shuffle through the low down
Reppin' the O-Town
I know there's a lot to do
I do what I got to do
Shakin' off the haters
Made it on my own now
Predicting a victory, you ain't got shit for me
It's nothin'
Ain't my complexion I hold down
There ain't no tamin' him cause we ain't afraid of 'em
God damn it hard for a hater to sleep
"When you've got things on your mind"

[Verse Two: Copywrite]
Gotta, write it, spit it out and get it out of my system and into yours
Paper and ink, my shrink hit record
My landlords knockin', wantin' rent
All I got is pocket lint so I changed the locks on him
Knock again
Got not a cent, gotta vent, aim precise
Cause I gotta win, playin' in the game of life
I dropped out after a 9th grade 3 peat
Made it to the 10th and my school mates didn't see Pete
I was, hardheaded couldn't tell me nothin'
Now I ask my mom like, "Why you ain't tell me nothin'?"
She tried, see I
Make it harder to make me
God I was lazy
Used to have to throw water to wake me
She must've thought I was crazy
Now I want to repay her
With a seven level house and some lottery paper
For all the pain that I gave her
Early, comin' to court for me
We all got war stories, mine's a little more gory

[Hook]

[Verse Three: Copywrite]
I haven't done taxes in three years
Ain't seen a dentist or doctor in seven
You think I gotta be kidding, but I'm not I'm admittin'
I'mma keep wishin' and scratchin' till I'm lottery hittin'
Straighten my credit up then I'm on a property mission
My mom apologized for where I was raised
Told her I wouldn't change it for nothin', that's where I was made
Down the street from nanam and papa and cherish the days
And pray to God that I inherit their ways
In the name of the Father, Son and the only Ghost
Miss when they hold me close
Now I'm grown and reality can lead to an overdose
Gotta stay sober though
Cause when I got 'dro to blow I'm comatose
I wanna remember what I see coast to coast
I'm goin'
Regardless what this world leaves me
My mind state is every state and zip code needs me
It's hard to block once my target's locked
Artist's guard your props
I'm comin', 100 Miles and Running

[Hook]