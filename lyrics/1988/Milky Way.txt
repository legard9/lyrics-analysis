Far away above the skyline
You watched me without offering helps
In silence i climbed the mountain
But what i found there is just nothingness

You said this place would be our home
But you run away
The ground's shaking, you are leaving
Walk off the milky way

Won't hold this tongue any longer
You are the one i regret in life
Sally and me, we both can't wait
We're standing for too long
But you wisely waste the air tonight

You said this place would be our home
But you run away
The ground's shaking, you are leaving
Walk off the milky way

You said this place would be our home
But you run away
The ground's shaking, you are leaving
Walk off the milky way

Nananananananana
Nananananananana

The ground's shaking, you are leaving
Walk off the milky way