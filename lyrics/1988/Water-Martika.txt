Like water
You see into me
You reach into me
And I ripple round in circles
Reflections
Of my dreams in yours
Bring me to the shores of desire

I have been like a desert creature
Wandering on this earth so cracked and dry
Only to survive
But now your love is bringing me alive

Like water
How could I do without you
My body heat will not cool down
Like water
I could not live without you
I want to dive so deep into you I might drown

Tu eres como el agua

You know me
An invisible line
Connects us in time
And now there is no distance
Between us
You see what I feel
And all I am feeling is you

Tu eres como el agua
No se que haria sin ti mi amor
Yo no pudiera vivir sin ti

Quiero ahogarme en ti y nunca
Regresar

Como el agua me ves tan clara
Siempre puedes tu llegar a mi
Me siento tan feliz
Tu amor me da razon para vivir

Like water
Like water