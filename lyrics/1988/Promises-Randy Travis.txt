Cheap perfume and painted faces
Fallen angels fill the places
Where I go when my troubles pull me down
All the lies I know they'll tell me
And the time that they will sell me
For a while I'll be the biggest man in town
Back at home in bed she's crying
For her love for me is dying
But she'll pray I make it safely through the night
When the mornin' sun starts showing
To her bedside I'll be going
And she'll hold me while I face the morning light
And I'll make promises
Promises to change
I'll make her promises swear I'll rearrange
And I'll start giving all the love she needs
If only she will stay
Once again she'll reassure me and
I believe her love will cure me
And I'll fall asleep with tears on my face
Lord I know she's just a woman
And her love can't last forever
And someday soon I know she'll
Leave without a trace
For broken' promises
Will tear her dreams apart
Just token promises will someday break her heart
And for the last time she'll hold me when I cry
And while I'm sleeping
She'll quietly say goodbye