[Verse 1]
May the good Lord be with you
Down every road you roam
And may sunshine and happiness
Surround you when you're far from home
And may you grow to be proud
Dignified and true
And do unto others
As you'd have done to you

[Chorus]
Be courageous and be brave
And in my heart, you'll always stay
Forever young, forever young
Forever young, forever young

[Verse 2]
May good fortune be with you
May your guiding light be strong
Build a stairway to Heaven
With a prince or a vagabond

[Chorus]
And may you never love in vain
And in my heart you will remain
Forever young, forever young
Forever young, forever young
Forever young, forever young
Yeah

[Instrumental Break]

[Verse 3]
And when you finally fly away
I'll be hoping that I served you well
For all the wisdom of a lifetime
No one can ever tell

[Chorus]
But whatever road you choose
I'm right behind you, win or lose
Forever young, forever young
Forever young, forever young
Forever young, forever young
For-Forever young, forever young