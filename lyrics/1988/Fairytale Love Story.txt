Hangul
시작된 이야기 더욱 설레게 하지
동화 속 커플처럼 너와 나의 love story
꿈같은 이 시간이 계속될 거야
너가 원하면 앞으로 이어질 거야
우리 안에 blooming flower
너란 꽃잎은 계속 향기로워
어린아이 같은 꿈 꾸길 바래
내가 원하는 건 너 하나인데
우리 손 꼭 잡고 자 꼭 같은 꿈
꾸길 바래 푹 잘 수 있도록
어딜 가도 그대만 있다면
아무것도 두렵지 않아요
너와 나 둘만의 story
빛나는 나날들이
우리 앞엔 계속 있을 테니
그저 지금처럼만 내 곁에 있어줘요 baby
이대로 이대로 이대로 이대로
잠시 멀어진다 해도 그대만 사랑해요
이대로 이대로 언제까지 우린 이대로
이대로 이대로
그날은 눈이 참 많이 내렸을 거야
첫눈에 네게 반해버렸을 거야
줄 거야 단 너를 위한 사랑
유일무이한 순간을 나와 계속 잡아
거리마다 울려 퍼져
우리의 메아리가 커져
너가 내게로 한걸음 더 다가와 준다면
난 더 바랄게 없어 내 손을 더 꽉 잡아줘
날 보고 미소 지을 때면
하늘을 날아갈 것 같아요
걱정은 하지 말아요
나 변하지 않을게요
널 사랑하니까
그저 지금처럼만 내 곁에 있어줘요 baby
이대로 이대로 이대로 이대로
잠시 멀어진다 해도 그대만 사랑해요
이대로 이대로 언제까지 우린 이대로
수많은 사람 중에 너를 만난 건
행운이야 운명이야
난 매일 밤 기도해
다시 태어나도 너
내게 하나뿐인 너
우리 이대로 이대로 영원히
그저 지금처럼만 내 곁에 있어줘요 baby
이대로 이대로 이대로 이대로
잠시 멀어진다 해도 그대만 사랑해요
이대로 이대로 언제까지 우린 이대로
이대로 이대로 이대로 이대로
잠시 멀어진다 해도 그대만 사랑해요
이대로 이대로 언제까지 우린 이대로
Romanization
Sijagdoen iyagi deoug seollege haji
Donghwa sog keopeulcheoleom neowa naui love story
Kkumgat-eun i sigan-i gyesogdoel geoya
Neoga wonhamyeon ap-eulo ieojil geoya
Uli an-e blooming flower
Neolan kkoch-ip-eun gyesog hyang-gilowo
Eolin-ai gat-eun kkum kkugil balae
Naega wonhaneun geon neo hanainde
Uli son kkog jabgo ja kkog gat-eun kkum
Kkugil balae pug jal su issdolog
Eodil gado geudaeman issdamyeon
Amugeosdo dulyeobji anh-ayo
Neowa na dulman-ui story
Bichnaneun nanaldeul-i
Uli ap-en gyesog iss-eul teni
Geujeo jigeumcheoleomman nae gyeot-e iss-eojwoyo baby
Idaelo idaelo idaelo idaelo
Jamsi meol-eojinda haedo geudaeman salanghaeyo
Idaelo idaelo eonjekkaji ulin idaelo
Idaelo idaelo
Geunal-eun nun-i cham manh-i naelyeoss-eul geoya
Cheosnun-e nege banhaebeolyeoss-eul geoya
Jul geoya dan neoleul wihan salang
Yuilmu-ihan sungan-eul nawa gyesog jab-a
Geolimada ullyeo peojyeo
Uliui mealiga keojyeo
Neoga naegelo hangeol-eum deo dagawa jundamyeon
Nan deo balalge eobs-eo nae son-eul deo kkwag jab-ajwo
Nal bogo miso jieul ttaemyeon
Haneul-eul nal-agal geos gat-ayo
Geogjeong-eun haji mal-ayo
Na byeonhaji anh-eulgeyo
Neol salanghanikka
Geujeo jigeumcheoleomman nae gyeot-e iss-eojwoyo baby
Idaelo idaelo idaelo idaelo
Jamsi meol-eojinda haedo geudaeman salanghaeyo
Idaelo idaelo eonjekkaji ulin idaelo
Sumanh-eun salam jung-e neoleul mannan geon
Haeng-un-iya unmyeong-iya
Nan maeil bam gidohae
Dasi taeeonado neo
Naege hanappun-in neo
Uli idaelo idaelo yeong-wonhi
Geujeo jigeumcheoleomman nae gyeot-e iss-eojwoyo baby
Idaelo idaelo idaelo idaelo
Jamsi meol-eojinda haedo geudaeman salanghaeyo
Idaelo idaelo eonjekkaji ulin idaelo
Idaelo idaelo idaelo idaelo
Jamsi meol-eojinda haedo geudaeman salanghaeyo
Idaelo idaelo eonjekkaji ulin idaelo
English Translation
The story has started, making my heart flutter even more
Like a fairy tale couple, it's our love story
This dream-like times will continue
If you want, it'll keep going
Inside us is a blooming flower
Your flower smells so good
I hope we have child-like dreams
What I want is you alone
Let's hold hands and dream the same dream
So you can have a deep sleep
Wherever I go, if I have you
I'm not afraid of anything
You and me, it's our story
Our shining days
Will be in front of us
Stay by my side, just like now baby
Just like this, just like this
Even if we grow apart for a bit, I still love you
Just like this, just like this, till always
Just like this, just like this
It snowed a lot that day
I fell for you at first sight
I'm gonna give you love
Keep this one and only moment with me
Let it ring throughout each street
Our echo is getting louder
If you take a step toward me
I have nothing more to want
Hold my hand tighter
When you look at me and smile
I feel like I could fly
Don't worry
I won't change
Because I love you
Stay by my side, just like now baby
Just like this, just like this
Even if we grow apart for a bit, I still love you
Just like this, just like this, till always
Having met you out of all these people
It's destiny, it's fate
I pray every night
Even if I'm born again
It's only you
Let's stay like this forever
Stay by my side, just like now baby
Just like this, just like this
Even if we grow apart for a bit, I still love you
Just like this, just like this, till always
Just like this, just like this
Even if we grow apart for a bit, I still love you
Just like this, just like this, till always