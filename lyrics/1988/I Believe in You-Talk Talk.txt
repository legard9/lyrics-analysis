[Verse 1]
Hear it in my spirit
I've seen heroin for myself
On the street so young, laying wasted
Enough, ain't it enough, crippled world
I just can't bring myself to see it starting

[Verse 2]
Tell me how I fear it
I buy prejudice for my health
Is it worth so much when you taste it?
Enough, there ain't enough hidden hurt
A time to sell yourself, a time for passing

[Chorus]
Spirit
How long?
Spirit

[Instrumental]

[Chorus]
Spirit
Spirit
How long?
Spirit
How long?
Spirit