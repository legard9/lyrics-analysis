<<<<<<< HEAD
Have these predictions come true? Click on the highlights to read how the world has changed since 2010. If you'd like to add your own insights, comments, or questions, visit 
the article on Genius
, highlight the relevant text, a
nd click the button that pops up. Your annotation will appear both here and on Genius.
Pesky thing, tomorrow
. 
Day after day, it shows up and brings technological innovation that alters the best-laid plans of every business owner.
Managing for the future isn't easy. Emerging technologies and new developments can create a tumbling mash-up of hard-to-understand products and services, from web-connected printers to robots that represent you in meetings.
 And some innovations are even harder to get your head around--
like an internet that thinks on its own and actually does some of your work for you.
 (No kidding, that's coming.)
To keep you sane--and to try to help you get some sleep at night--here is our list of the top 25 tech tips, trends and megatrends: what's new now, what will be new tomorrow and what you can expect to grapple with even farther down the road
.
Master this list and, with a little luck, you can keep that next new day under control.
What's New Now
1. Web-based office software
The major software players are in a battle royal to sell you word processing
, spreadsheets and other web-based office software. 
There's Google with its Google Apps office suite, Microsoft with Microsoft Office software tools
 and Adobe with its 
Buzzword
 online word processor--not to mention new entrants like 
Zoho
 and 
ThinkFree
. 
Yes, cloud-based office tools really can save you money and make the tech aspect of your business more efficient.
 All your work can be backed up, accessed and--most important--shared in real time from anywhere that has a web connection.
The smart play:
 For optimum features, security and durability, install both a cloud-based tool and a PC-based tool side by side in your business. That way you have 
the sophisticated collaboration tools like Google Apps
 for content creation and sharing alongside a top-quality, on-premises tool like Microsoft Office or OpenOffice.org with bombproof security. 
Redundancy is always better when it comes to office software.
2. Mobile business apps
All that cool software on Apple's iPhone really does have some competition: 
Google's Android Market, Research In Motion's BlackBerry App World and Microsoft's Marketplace for mobile apps offer software that compete step for step with code coming out of Apple's App Store.
 Troll the app markets for tools you can use in your business. (See related story, "
The Smartphone Gets Smarter
.")
Insider tip: 
Follow the app development communities on Facebook and Twitter. They offer the best developer gossip on what's new, what's necessary and what's coming.
3. The touch kiosk
Perhaps the iPhone's greatest gift to business society is that it got us in touch with touch.
 Devices 
like desktop
 
HP TouchSmart 600t
 ($1,030) and 
Acer
 Aspire Z5600 ($950), as well as printers from 
Epson
 like the Artisan 810 ($299) now come with powerful touch-activated options.
Do this now: 
These touch-activated tools can be tricked into becoming powerful business kiosks.
 Put one in the office for your customers to use and save a fortune over dedicated professional, walk-up interactive systems.
4. Office in a box
Ever wonder why you need all those different phone servers, e-mail servers, routers and document servers stuffed into your tech room? The truth is, you don't. Products like the 
Sutus BC 200
 (price starts at $2,000) and 
edgeBOX office SOHO
 (price varies by configuration) are combining phone servers, e-mail servers, routers, document servers and firewalls into a single low-cost device.
Cut your costs: 
Fed up with that crazy expensive telephony reseller you're using? Test drive one of these alternatives.
5. Business-class text messaging
Turns out the plain vanilla short messaging service (also known as SMS, or text messaging) on your cell phone can come in handy. Fast and robust, texting really can be a business-grade communications tool. The trick? Bulk short messaging firms like 
Clickatell
 (price varies by country) and 
RedOxygen
 (U.S. messaging starts at 8 cents) 
help turn SMS into a mass marketing message or a way to get groups to communicate smarter.
Be warned: 
SMS can get pricey, so make sure you are doing it for the right reasons and can show the ROI on each text you ship
--whether it's more customers, larger incremental sales or improved internal productivity.
6. The interactive sales pitch
That static, premade sales presentation that you project on a wall with a projector is oh so 1999. Interactive imaging systems let you control not only a PowerPoint, but your entire computer as well just by touching the projected image on the surface. If you're looking for that Wizard of Oz effect on your next sales trip, try these out.
Top interactive projectors:
Boxlight ProjectoWrite2
 ($2,200)
Epson BrightLink 450Wi
 ($2,199)
Smart Technologies Smart Board Interactive Whiteboard 685ix 
($6,000)
7. Bigger, faster and better office networks
Two new networking standards are getting set to turbocharge how data moves around. The geek-speak to know here is: "Dual-band 8.02.11n class" wireless routers, and "USB 3.0." Dual N routers, like the 
D-Link Xtreme N 450 Gigabit Router
 (DIR-665) ($199), were built to move big HD media files, but they make killer small-business networks, too. And USB 3.0 speeds up locally connected devices like the 
LaCie Rugged 1TB USB 3.0 drive
 ($185) so they run at awfully close-to-wired Ethernet speeds.
New technology bonus: 
USB 3.0 drives are also engineered to use less power. The power savings won't be much, unless you deploy the technology on a massive scale, but it still counts. And drives generate less heat.
8. The smarter ledger
Online financial software is one of the sleeper pockets of value in web-based business tools. Intuit gets most of the media buzz for its QuickBooks offerings, but several other web-based options can upgrade how you keep your books. These tools do require you to be even more security conscious (important tip: change passwords often), but they allow you to import expenses, invoice clients and share data with your accountant pretty much as your fortunes change. Maintained carefully, they also make tax time a breeze.
Our online bean-counting tool picks:
Xero
 (starts at $19 per month for five users)
LessAccounting
 (starts at $12 per month)
Outright
 (free)
9. The wall outlet that pays for itself
Electricity is dear for some businesses, particularly those in power-starved California. But developments in smart grid technology are creating clever ways to save juice: 
The ThinkEco Modlet
 (price to be determined), the 
Tenrehte Technologies PICOwatt
 (price $7,999) and the 
Ted 5000-G
 ($199) let you track and manage power remotely.
Tech tip: 
Use a remote plug to turn the lights off when you're not at the office and watch that monthly bill go down.
10. Translation software goes mainstream
Though long considered a nonstarter, translation software is finally becoming reliable enough for businesses to use. Several cool tools offer quick and easy means to port content internationally. They won't replace the live editor entirely, but they can be useful for basic tasks.
Our translator picks:
Google Translate
 (free)
Babylon
 (free to start then $9.70 per month per user)
Systran Premium Translator
 ($799)
 
What Will be Hot Tomorrow
11. The super-smart browser
Sparked by (who else?) Google and its relentless drive for market share, all browsers from Internet Explorer to Mozilla Firefox to Safari now host ambitious third-party software tools that bolt quickly onto existing browser software. Now once-unimaginable services run natively on web tools. Check out design tools like 
Aviary
 (free), complex collaboration systems like 
UseKit
 (free) and communication tools such as 
Follow-Up Robot
 ($25 per month per user).
Be warned:
 
All this new browser software will make it tougher for businesses to develop code that works across all browsers and the fast-moving extension market.
 Expect the debugging of web-based products and services to get really, really complex.
12. The appliance app
Get ready for it: 
Apps, like the ones you find on mobile phones, are spreading to TVs and, before long, to washers and fridges, too.
 To get a feel for the opportunity, check out the TV apps Vizio offers on its sets that enable Netflix, Flickr and Twitter.
Coolest appliance app:
 Have a look at what 
Touch Revolution
 is doing with its Google Android OS -based modules that turn a microwave oven into an app ready, web-ready device. Pretty slick.
13. Talking to your computer
Everyone from Ford to Microsoft now supports some sort of voice-activated software. Voice recognition is not perfect yet, but it can ease the burden on tired tendons.
Do this now: 
To determine whether talking to your PC is for you, test-drive 
Nuance's Dragon NaturallySpeaking
 (starts at $99).
14. PC-less desktop imaging
With printers now a commodity ($35 buys a unit that once cost thousands), business desktop imaging eventually will free itself from being tethered to a PC. The big mover here is, yet again, Google. The giant is rolling out cloud-based printing that will not need a connected computer. You can expect everything from smartphones to positional devices to be able to easily communicate with web-connected printers.
Peek at the future:
 
HP recently announced a deal with FedEx to test the cloud-printing waters. Track this deal closely.
15. 
The 3-D peripheral
Heard all the buzz about 
3-D FIFA World Cup
 or the NBA All-Star game? Products like 
Space Controller
 ($299) and SpacePilot Pro from 
3Dconnexion
 ($59) are offering computer controllers that put depth access in the hands of CAD artists and engineers.
Why it matters: 
These systems will lower the barrier to entry for creating and developing 3-D content.
16. The high-tech ceiling fan
For businesses facing brutal HVAC costs, several outfits are reinventing the standard ceiling fan to be a high-performance air circulation tool. Several firms are using engineering from the new generation of wind-powered technologies to make large ceiling fans more efficient. Considering the cost of heating and cooling, these units really can affect the bottom line--and let you have the fun of writing the phrase "Big Ass Fans" on a check.
Big fans:
 
Gossamer Wind
, 
Big Ass Fans
 and 
Envira-North
17. The ultraportable office
There's nothing like a cutting-edge technology that combines another cutting-edge technology to give you that buy-one-get-one-free sort of feel. Portable hot spots that let small groups collaborate quickly are now available in units like the MiFi 2200 from 
Verizon Wireless
 (free with a two-year service plan). In addition to giving you a nice productivity boost on the road, portable Wi-Fi is creating an ad hoc network of mobile Wi-Fi coverage.
Tomorrow's tech tip:
 
Next time you need to log in, look around in your Wi-Fi software--a local portable hot spot might be nearby.
18. Videoconferencing for everyone
If Skype has a bonus, it's that it has banished the videoconferencing taboo. Now many vendors are making low-cost video appliances that even tiny firms can use, with development in this area only to increase. Although these units won't rival those cute Cisco commercials for quality, videoconferencing can be helpful in your firm. And you can't beat the price.
Our pick for low-cost video phone:
 The 
Vialta Beamer FX video phone
 ($179).
 
Get Ready for the Day After Tomorrow
19. Everything goes automatic
As crazy as it sounds, web-delivered, automatic decision-making will quietly creep into the basic fabric of your business. Expect smarter versions of everything from spell checkers to complex decision engines to show up in web office tools and business software. 
Try out Google's recently acquired Aardvark answers product, which uses Google Chat to automatically match a person who has a question with a person who has an answer.
20. The projectable PC interface
Projectable computer interfaces have long been stubbornly "just around the corner." Projectable keyboards and other controllers aren't yet up to full business-class capabilities, but when a unit like the one from 
Light Blue Optics
 finds its way into smartphones, it will offer a new, simple way for road warriors to communicate on the go.
21. The flexible display
It will be years before flexible screens make it into your office
, but not so for getting them into your point-of-sale displays. Flexible, high-quality displays from companies like 
Atlanta's NanoLumens
 are making big, bright screens that can be mounted on rounded surfaces. That means any old column can work as a pricing display or marketing surface. Slick.
22. The solar-powered office
With tax breaks, advances in core solar technologies and a new generation of gadgets, fairly soon you will be able to power your entire SMB with the sun. 
State-funded renewable energy programs mix tax breaks and direct grants to drive solar deployments.
 Outfits like 
PowerFilm Inc.
 are developing custom-made flexible solar panels that can power literally anything.
23. A smarter delivery van
Technologies like 
Ford's Sync Traffic Direction and Information
 and 
Rand McNally's IntelliRoute
 make the idea of sending a man and a van to wander around town unsupervised about as smart as using carrier pigeons. And commercial vehicles are set to get smarter still: FedEx is preparing to deploy electric delivery vans. And startups like 
Boulder Electric Vehicles
 will make such technology available to most any small business. That means using a dumb old delivery van is just plain dumb.
24. Making your inventory talk
If it's good enough for Wal-Mart, it's good enough for you. Low-cost RFID (radio frequency identifier) devices from companies like 
RF*IDI
  will offer tagging and tracking solutions even your small business can afford. Wondering where your stuff went is like worrying about Y2K.
25. The virtual you
Finally, be at that meeting without actually having to attend.
 
Anybot the robot avatar 
($15,000) will use telepresence technology to let you attend meetings virtually by rolling into conferences and transmitting information so that you don't have to be there to get the job done.
=======
Have these predictions come true? Click on the highlights to read how the world has changed since 2010. If you'd like to add your own insights, comments, or questions, visit the article on Genius, highlight the relevant text, and click the button that pops up. Your annotation will appear both here and on Genius.
Pesky thing, tomorrow. Day after day, it shows up and brings technological innovation that alters the best-laid plans of every business owner.

Managing for the future isn't easy. Emerging technologies and new developments can create a tumbling mash-up of hard-to-understand products and services, from web-connected printers to robots that represent you in meetings. And some innovations are even harder to get your head around--like an internet that thinks on its own and actually does some of your work for you. (No kidding, that's coming.)

To keep you sane--and to try to help you get some sleep at night--here is our list of the top 25 tech tips, trends and megatrends: what's new now, what will be new tomorrow and what you can expect to grapple with even farther down the road.
Master this list and, with a little luck, you can keep that next new day under control.

What's New Now

1. Web-based office software

The major software players are in a battle royal to sell you word processing, spreadsheets and other web-based office software. There's Google with its Google Apps office suite, Microsoft with Microsoft Office software tools and Adobe with its Buzzword online word processor--not to mention new entrants like Zoho and ThinkFree. Yes, cloud-based office tools really can save you money and make the tech aspect of your business more efficient. All your work can be backed up, accessed and--most important--shared in real time from anywhere that has a web connection.

The smart play: For optimum features, security and durability, install both a cloud-based tool and a PC-based tool side by side in your business. That way you have the sophisticated collaboration tools like Google Apps for content creation and sharing alongside a top-quality, on-premises tool like Microsoft Office or OpenOffice.org with bombproof security. Redundancy is always better when it comes to office software.

2. Mobile business apps

All that cool software on Apple's iPhone really does have some competition: Google's Android Market, Research In Motion's BlackBerry App World and Microsoft's Marketplace for mobile apps offer software that compete step for step with code coming out of Apple's App Store. Troll the app markets for tools you can use in your business. (See related story, "The Smartphone Gets Smarter.")

Insider tip: Follow the app development communities on Facebook and Twitter. They offer the best developer gossip on what's new, what's necessary and what's coming.

3. The touch kiosk

Perhaps the iPhone's greatest gift to business society is that it got us in touch with touch. Devices like desktop HP TouchSmart 600t ($1,030) and Acer Aspire Z5600 ($950), as well as printers from Epson like the Artisan 810 ($299) now come with powerful touch-activated options.

Do this now: These touch-activated tools can be tricked into becoming powerful business kiosks. Put one in the office for your customers to use and save a fortune over dedicated professional, walk-up interactive systems.

4. Office in a box

Ever wonder why you need all those different phone servers, e-mail servers, routers and document servers stuffed into your tech room? The truth is, you don't. Products like the Sutus BC 200 (price starts at $2,000) and edgeBOX office SOHO (price varies by configuration) are combining phone servers, e-mail servers, routers, document servers and firewalls into a single low-cost device.

Cut your costs: Fed up with that crazy expensive telephony reseller you're using? Test drive one of these alternatives.

5. Business-class text messaging

Turns out the plain vanilla short messaging service (also known as SMS, or text messaging) on your cell phone can come in handy. Fast and robust, texting really can be a business-grade communications tool. The trick? Bulk short messaging firms like Clickatell (price varies by country) and RedOxygen (U.S. messaging starts at 8 cents) help turn SMS into a mass marketing message or a way to get groups to communicate smarter.

Be warned: SMS can get pricey, so make sure you are doing it for the right reasons and can show the ROI on each text you ship--whether it's more customers, larger incremental sales or improved internal productivity.

6. The interactive sales pitch

That static, premade sales presentation that you project on a wall with a projector is oh so 1999. Interactive imaging systems let you control not only a PowerPoint, but your entire computer as well just by touching the projected image on the surface. If you're looking for that Wizard of Oz effect on your next sales trip, try these out.

Top interactive projectors:


Boxlight ProjectoWrite2 ($2,200)

Epson BrightLink 450Wi ($2,199)

Smart Technologies Smart Board Interactive Whiteboard 685ix ($6,000)
7. Bigger, faster and better office networks

Two new networking standards are getting set to turbocharge how data moves around. The geek-speak to know here is: "Dual-band 8.02.11n class" wireless routers, and "USB 3.0." Dual N routers, like the D-Link Xtreme N 450 Gigabit Router (DIR-665) ($199), were built to move big HD media files, but they make killer small-business networks, too. And USB 3.0 speeds up locally connected devices like the LaCie Rugged 1TB USB 3.0 drive ($185) so they run at awfully close-to-wired Ethernet speeds.

New technology bonus: USB 3.0 drives are also engineered to use less power. The power savings won't be much, unless you deploy the technology on a massive scale, but it still counts. And drives generate less heat.

8. The smarter ledger

Online financial software is one of the sleeper pockets of value in web-based business tools. Intuit gets most of the media buzz for its QuickBooks offerings, but several other web-based options can upgrade how you keep your books. These tools do require you to be even more security conscious (important tip: change passwords often), but they allow you to import expenses, invoice clients and share data with your accountant pretty much as your fortunes change. Maintained carefully, they also make tax time a breeze.

Our online bean-counting tool picks:


Xero (starts at $19 per month for five users)

LessAccounting (starts at $12 per month)

Outright (free)
9. The wall outlet that pays for itself

Electricity is dear for some businesses, particularly those in power-starved California. But developments in smart grid technology are creating clever ways to save juice: The ThinkEco Modlet (price to be determined), the Tenrehte Technologies PICOwatt (price $7,999) and the Ted 5000-G ($199) let you track and manage power remotely.

Tech tip: Use a remote plug to turn the lights off when you're not at the office and watch that monthly bill go down.

10. Translation software goes mainstream

Though long considered a nonstarter, translation software is finally becoming reliable enough for businesses to use. Several cool tools offer quick and easy means to port content internationally. They won't replace the live editor entirely, but they can be useful for basic tasks.

Our translator picks:


Google Translate (free)

Babylon (free to start then $9.70 per month per user)

Systran Premium Translator ($799)

 
What Will be Hot Tomorrow

11. The super-smart browser

Sparked by (who else?) Google and its relentless drive for market share, all browsers from Internet Explorer to Mozilla Firefox to Safari now host ambitious third-party software tools that bolt quickly onto existing browser software. Now once-unimaginable services run natively on web tools. Check out design tools like Aviary (free), complex collaboration systems like UseKit (free) and communication tools such as Follow-Up Robot ($25 per month per user).

Be warned: All this new browser software will make it tougher for businesses to develop code that works across all browsers and the fast-moving extension market. Expect the debugging of web-based products and services to get really, really complex.

12. The appliance app

Get ready for it: Apps, like the ones you find on mobile phones, are spreading to TVs and, before long, to washers and fridges, too. To get a feel for the opportunity, check out the TV apps Vizio offers on its sets that enable Netflix, Flickr and Twitter.
Coolest appliance app: Have a look at what Touch Revolution is doing with its Google Android OS -based modules that turn a microwave oven into an app ready, web-ready device. Pretty slick.

13. Talking to your computer

Everyone from Ford to Microsoft now supports some sort of voice-activated software. Voice recognition is not perfect yet, but it can ease the burden on tired tendons.

Do this now: To determine whether talking to your PC is for you, test-drive Nuance's Dragon NaturallySpeaking (starts at $99).

14. PC-less desktop imaging

With printers now a commodity ($35 buys a unit that once cost thousands), business desktop imaging eventually will free itself from being tethered to a PC. The big mover here is, yet again, Google. The giant is rolling out cloud-based printing that will not need a connected computer. You can expect everything from smartphones to positional devices to be able to easily communicate with web-connected printers.

Peek at the future: HP recently announced a deal with FedEx to test the cloud-printing waters. Track this deal closely.

15. The 3-D peripheral

Heard all the buzz about 3-D FIFA World Cup or the NBA All-Star game? Products like Space Controller ($299) and SpacePilot Pro from 3Dconnexion ($59) are offering computer controllers that put depth access in the hands of CAD artists and engineers.

Why it matters: These systems will lower the barrier to entry for creating and developing 3-D content.

16. The high-tech ceiling fan

For businesses facing brutal HVAC costs, several outfits are reinventing the standard ceiling fan to be a high-performance air circulation tool. Several firms are using engineering from the new generation of wind-powered technologies to make large ceiling fans more efficient. Considering the cost of heating and cooling, these units really can affect the bottom line--and let you have the fun of writing the phrase "Big Ass Fans" on a check.

Big fans: Gossamer Wind, Big Ass Fans and Envira-North

17. The ultraportable office

There's nothing like a cutting-edge technology that combines another cutting-edge technology to give you that buy-one-get-one-free sort of feel. Portable hot spots that let small groups collaborate quickly are now available in units like the MiFi 2200 from Verizon Wireless (free with a two-year service plan). In addition to giving you a nice productivity boost on the road, portable Wi-Fi is creating an ad hoc network of mobile Wi-Fi coverage.

Tomorrow's tech tip: Next time you need to log in, look around in your Wi-Fi software--a local portable hot spot might be nearby.

18. Videoconferencing for everyone

If Skype has a bonus, it's that it has banished the videoconferencing taboo. Now many vendors are making low-cost video appliances that even tiny firms can use, with development in this area only to increase. Although these units won't rival those cute Cisco commercials for quality, videoconferencing can be helpful in your firm. And you can't beat the price.

Our pick for low-cost video phone: The Vialta Beamer FX video phone ($179).

 
Get Ready for the Day After Tomorrow

19. Everything goes automatic

As crazy as it sounds, web-delivered, automatic decision-making will quietly creep into the basic fabric of your business. Expect smarter versions of everything from spell checkers to complex decision engines to show up in web office tools and business software. Try out Google's recently acquired Aardvark answers product, which uses Google Chat to automatically match a person who has a question with a person who has an answer.

20. The projectable PC interface

Projectable computer interfaces have long been stubbornly "just around the corner." Projectable keyboards and other controllers aren't yet up to full business-class capabilities, but when a unit like the one from Light Blue Optics finds its way into smartphones, it will offer a new, simple way for road warriors to communicate on the go.

21. The flexible display

It will be years before flexible screens make it into your office, but not so for getting them into your point-of-sale displays. Flexible, high-quality displays from companies like Atlanta's NanoLumens are making big, bright screens that can be mounted on rounded surfaces. That means any old column can work as a pricing display or marketing surface. Slick.

22. The solar-powered office

With tax breaks, advances in core solar technologies and a new generation of gadgets, fairly soon you will be able to power your entire SMB with the sun. State-funded renewable energy programs mix tax breaks and direct grants to drive solar deployments. Outfits like PowerFilm Inc. are developing custom-made flexible solar panels that can power literally anything.

23. A smarter delivery van

Technologies like Ford's Sync Traffic Direction and Information and Rand McNally's IntelliRoute make the idea of sending a man and a van to wander around town unsupervised about as smart as using carrier pigeons. And commercial vehicles are set to get smarter still: FedEx is preparing to deploy electric delivery vans. And startups like Boulder Electric Vehicles will make such technology available to most any small business. That means using a dumb old delivery van is just plain dumb.

24. Making your inventory talk

If it's good enough for Wal-Mart, it's good enough for you. Low-cost RFID (radio frequency identifier) devices from companies like RF*IDI  will offer tagging and tracking solutions even your small business can afford. Wondering where your stuff went is like worrying about Y2K.

25. The virtual you

Finally, be at that meeting without actually having to attend. Anybot the robot avatar ($15,000) will use telepresence technology to let you attend meetings virtually by rolling into conferences and transmitting information so that you don't have to be there to get the job done.
>>>>>>> master
 