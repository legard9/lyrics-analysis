[Chorus]
Count the days till we're together (One, two, three)
Though it feels like forever (We will be)
Holding on to each other (You and me)
You will be my only lover (Eternally)

[Verse 1]
You made my day
When a dozen red roses came my way
Warmed up my heart
Because I know you're thinking of me from afar

[Pre-Chorus]
Press a petal to my lips
Feels like your kiss
Don't you know that's what I miss?
To smell the sweet perfume
Reminds me so much of you
There's nothing else that I can do

[Chorus]
Count the days till we're together (One, two, three)
Though it feels like forever (We will be)
Holding on to each other (You and me)
You will be my only lover (Eternally)

[Verse 2]
It's been so long
I know our two hearts are strong
There's not a moment
That my spirit isn't in it, don't you know it

[Pre-Chorus]
Try to occupy my time
Remember you're mine
Talking to you on the line
Try to get used to this
But long distance loving is
Leaving me to dream of your kiss

[Chorus]
Count the days till we're together (One, two, three)
Though it feels like forever (We will be)
Holding on to each other (You and me)
You will be my only lover (Eternally)
Count the days till we're together (One, two, three)
Though it feels like forever (We will be)
Holding on to each other (You and me)
You will be my only lover (Eternally)

[Bridge]
When I'm feeling scared and lonely
Wonder if I'm still your only
Tell me now, soft and slowly
It's going to be all right
You'll be by my side

[Pre-Chorus]
Press a petal to my lips
Feels like your kiss
Don't you know that's what I miss?
To smell the sweet perfume
Reminds me so much of you
There's nothing else that I can do

[Chorus]
Count the days till we're together (One, two, three)
Though it feels like forever (We will be)
Holding on to each other (You and me)
You will be my only lover (Eternally)
Count the days till we're together (One, two, three)
Though it feels like forever (We will be)
Holding on to each other (You and me)
You will be my only lover (Eternally)
Count the days till we're together (One, two, three)
(I count the days)
Though it feels like forever (We will be)
Holding on to each other (You and me)
You will be my only lover (Eternally)