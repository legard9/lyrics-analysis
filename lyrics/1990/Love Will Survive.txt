[Intro: Canibus]
Tranquility to infinity (yeah!)
Tranquility to infinity

[Verse 1]
Canibus is an animal with the mechanical mandible
Coming to damage you, spitting understandable slang at you
I'm the all-seeing lyrical, with infinite bars of visuals
And a sideways eight peripheral
I told you I'd spit a rhyme that would melt the earth
Then ask you in the afterlife if you felt the verse
On planet Earth, I searched for my tranquillity first
I said I was the illest, but it didn't help me, it hurt!
And whenever I said "Can-I" the crowd said "Bus!"
Ten years later, who am I? I still got a passionate love
To be the man who I was, never give up
Irrational rush to crush every mic I clutch
When I erupt, you duck or eggs clash, flash solar blast from Bus
Then sweep you off the stage like crumbs
Grab your tongue, shout, rip it out, then shove it back in your mouth
Now, ten tell you to spit it out
I spit about them lyrics my people can't live without
Been around since '97, I've been ripping it down
Spit track after track after Beast From the East, I'm back
Before Lil' Weezy knew how to rap
When T.I. was still hustling crack, I put the muscle in rap
A 100 Bars, who fucking with that?
A thousand bars later, I ain't heard nothing from Pap
Where you was at when I was giving Big Punisher dap?
On stage with a him at the Palladium
You was in a gymnasium, I was putting chainsaws to craniums
Blazing Homo Sapiens in the atrium, ripping jaws off aliens
Performing Channel Zero in stadiums
Up at Hot 97 disgracing them
Any radio station they place me in I broke the break-beats in
I beat her, I beat him, the beat blend, I beat them
Spit a verse to beat Barack Obama if he win
I'm the Beast From the East, picking meat out my teeth
And as soon as the beat stop, I forget how to speak
I release a better rhyme seven times a week
To beat me you gotta be better than my last release
The bars rip ya face off, spit bars, spitshine ya skull
Till every rhyme you memorize is gone
Battle you for the respect in a battle to the death
Dial zero, call the operator ask for Bis
411 ask for RIP
555-1212, I rip the mic to shit
Before the Federal Communication Commission started a new division
With the intention to cripple our children
Mentally deficient from television
This radio programming we listen to got too many elements missing
Lyricism and wisdom got overshadowed by the singing and blinging
Deceived by a system thats media driven
I made a vow that I would get them and bit them, then injected my venom
And for that I was never forgiven my nigga
I let the rhythm hit 'em with a chemical algorithm
Liable to kill them if I ever get with them, I rip them
The infinite monk, All Hail Can-I-Bus
Then wake up to this Pure Uncut
How Many Emcees do I have to bust?
I'm a Patriot with No Airplay but How Come?
My Block is your block, I Throw It Up with Doo Wop
I'm the Enemy of the State of Hip Hop
Indibisible, Indestructible, Canibustible
The Adversarial Theater of Justice, judging you
Tired of you posers, I'm the rap superstar soldier on a poster
Captain Cold Crush
Tuck the heat before I brush the teeth
The athlete at the track meet with rusty cleats
Artillery like lawn mowers with four motors and four rotors
Look like mom with four strollers
Counterstrike like Black Kobra
With gasoline in the Super Soaker, walk over, I'll roast ya!

[Extended Version Verse]
White phosphorus clouds eject through the net
Of silver mesh that protects my mouth, face, and neck
The first to introduce full-body proximity suits
The emcees who become recruits
I knew a submarine spook that accidentally armed the nuke
All he could do was say 'oops'
Knowin' the truth is moot
We got troops in a deployment loop under Title 32
The constitution been reconstituted
The contract that we signed was a con, stupid
Biofields collect energy from my flesh
Multiplicity effects take me higher yet
Happy hour empowers the mind
Red wine from divine co-combines with my neuropeptides
Let's Ride, Sumerian summary summarized
I implied some will die, some will survive
Nanochemical composures absorb the aural exposures
The stars begin to move closer
I reach out and touch novas that respond with explosions
The eye of the wormhole opens
Step out the other smokin', worshiped as the chosen
Behind me the Stargate closes
I sit down and make poetry in motion, the table and chair where I sit start floatin'
Hip-hop cops black ops take notice
Recorded, then show this, but still couldn't hold this
I work for CONUS, codename: Moses verbosus
Classified throat poet component
Serotonin overflowin', I keep growin' like the Samoans
Tsunami approachin', lower me into the ocean
Steady, telepathy stabilize levies
It save lives but I don't know how many
The flood was not an obstacle
I made a raft out of empty milk gallon bottles, for survival
The line(?)  is symbolic how they worship the phallus
Of the male chesedeck(?) true path(?) the symphonic
At the surface, what's the topic?
Aliens? Drop it
According to them, we do not exist
This is my race, I dictate the pace
But the law stipulates I spit to save face
Dead in one sense, far more alive than other
Since my female vampire lover
She asked if I love her, then bit my chest
The last time I had sex with a dragon princess
I feed(?) the keys to Da Vinci's antiquities
Beneath the patch of little ittle(?) trees
Extraordinary men, that meet to review and recommend the gubernatorial that never ends
Learn to give, the collective lives
Otherwise the global mind will affect your kids
That's how it is, you can ask his(?)
The NWO lives, nigga good riddance

[DJ scratched sample]
"Cold Crush get it crackin!" (8x)