[Verse 1]
Ha! So the Joker's locked away in the asylum
Me thinks things are gonna become quite violent
I believe, he slipped a trick inside his sleeve
And his desire to leave is nonexistant 'til he's played a game malicious hide and seek
With sticks and dynamite and tnt
But what's that silhouette that you can see?
In the half-light to the dark night you might just find the dark knight
Savage as a shark bite, to the other rappers who can't just quite do this
You see the truth is I leap from roof with a really ruthless steez
I leave you toothless please you silly doofus
I'll give you a Chinese burn, and I don't mean confusius dead inside a little urn
I'll twist your arm until the skin is rather irritated
My methods of torture are really unsophisticated, to the most complicated machinations
To mash your face in, with patterned lacerations that are fascinating
And as my grapple hangs my batarang'll batter gangs
I brang the thang to make you say this can't be happenstance
The blackest avalanche, and as Dan's an anagram of "and"
I'll crick your spine and leave twisted like an ampersand
Feeling squiffy like you're getting high in Amsterdam, the neon city light'll emphasise the fact your damned

[Hook]
NANANANANANANANA
BATMAN BATMAN!

[Verse 2]
If commissioner Gordon didn't ignore the dissonant warnings
Than it'd have surely been a different story all together for the better
The prisoners all lead a sinister life a criminal orgy
Living inside a wicked and vile despicable isle, sicker than bile
But it's evident I'm a vigilant militant
Willing to kill them with brilliant diligent skill and the guile of a killer with infinite style
Jack White is quite insane, and while I'm fighting Bane, I find a titan-like strain flowing rife inside his veins
It's the same the Joker's trying to synthesise in doses high as planes
I'm tired of games, It's time for change, to time to take is smile away, Hey
How long has Killer Croc been a cock?
My trap'll leave him reeling like a spinning top and I willn't stop until his locked up in a box
Sit and watch me fighting pound for pound with Poison Ivy, a vigilante plant antidote without my boys behind me
Horticulture's not the sort of culture to be taken lightly
I take an eighth of titan now I'm feeling slightly hyphy
I might be quite that as high that I can see inside me psyche
Me, I'm so fly that I can fly, crikey

[Hook]
NANANANANANANANA
BATMAN BATMAN!