[Verse 1]
If there was a chance to save our broken romance
I'd let it go because now I know
That life doesn't end, you can start again
All isn't gone, just carry on
Be your own best friend

[Pre-Chorus]
It's easy to survive
Now that I finally realize that

[Chorus]
The world still turns, the stars still shine
The way they did when you were mine
A broken heart is a lesson learned
Though we're apart, baby, the world still turns

[Verse 2]
I couldn't believe that you were going to leave without a care
Heading somewhere elsewhere
My aching heart never thought that we would part
What could I do - my love for you
I was honest from the start

[Pre-Chorus]
Time takes away the pain
And the sun will shine again, because

[Chorus]
The world still turns, the stars still shine
The way they did when you were mine
A broken heart is a lesson learned
Though we're apart, baby, the world still turns

[Saxophone Solo]

[Pre-Chorus]
It's easy to survive
Now that I finally realize that

[Chorus]
The world still turns, the stars still shine
The way they did when you were mine
A broken heart is a lesson learned
Though we're apart, baby, the world still turns
The world still turns, the stars still shine
The way they did when you were mine
A broken heart is a lesson learned
Though we're apart, baby, the world still turns
The world still turns, the stars still shine
The way they did when you were mine
A broken heart is a lesson learned
Though we're apart, baby, the world still turns