[Verse 1]
He drives into the city, works extra hard all day
He finishes up early so he can get away
'Cause there's a blue-eyed kid on second base
Wants Dad to watch him play
And Daddy knows he's waiting so he hurries on his way

[Chorus]
'Cause you know heroes come in every shape and size
Making special sacrifices for others in their lives
No one gives 'em medals, the world don't know their names
But in someone's eyes, they're heroes just the same

[Verse 2]
She rocks her crying baby in the hours before dawn;
She whispers words of hope to help her husband to hold on;
She takes time with the children making sure they know she cares
She's more than just a momma, she's the answer to their prayers

[Chorus]
'Cause you know heroes come in every shape and size
Making special sacrifices for others in their lives
No one gives 'em medals, the world don't know their names
But in someone's eyes, they're heroes just the same

[Verse 3]
Now I don't pretend to know you but I'll bet it's safe to say
There's someone out there somewhere looking up to you today
And they see everything you do except for your mistakes
You may not think you measure up but you got all it takes

[Chorus]
'Cause you know heroes come in every shape and size
Making special sacrifices for others in their lives
No one gives 'em medals, the world don't know their names
But in someone's eyes, they're heroes just the same
No one gives 'em medals, the world don't know their names
But in someone's eyes, they're heroes just the same

[Tag]
Yeah, in someone's eyes they're heroes just the same
Talkin' 'bout heroes
Special kind of heroes
No ordinary heroes
Talkin' 'bout a hero
Special kind of hero