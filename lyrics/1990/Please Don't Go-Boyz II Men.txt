[Intro: Mike]
Hey, baby
I'm sorry
I never meant to hurt you
Please don't go

[Verse 1: Shawn]
Slowly, my eyes began to see
That I need you near right with me at all times
My feelings are so deep for you
That I won't let go of you

[Bridge: Nate]
I can't let this love (slip away)
And I can't turn it around (can't you see)
And only then

[Chorus: Shawn]
Please don't go away from me
(I'll be there for you)
When you call my name
(Call my name)
I'll reach out my hand to you
(And I Welcome you)
To my heart
(My future, my heart)
Please don't go away from me
We can work it out
Whatever it may be, girl
Please don't go
(No, no, no, no, no, please don't go)

[Interlude: Mike]
You know, It's not often that we see each other
That's why I cherish every moment that we spent together

[Verse 2: Shawn]
When you're away, temptation may come
Above of when I get caught up (by you)
When I get caught up (by you)
When I get caught up
I let go
Thinking about you each day
I feel (I love you and I never let go)
Your love makes my head spin 'round and round in a day

[Bridge: Nate]
I can't let this love (slip away)
And I can't turn it around (can't you see)
And only then

[Chorus: Shawn]
Please don't go away from me
(I'll be there for you)
When you call my name
(Call my name)
I'll reach out my hand to you
(And I Welcome you)
To my heart
(My future, my heart)
Please don't go away from me
We can work it out
Whatever it may be, girl
Please don't go
(No, no, no, no, no, please don't go)
Please don't go away from me
(I'll be there for you)
When you call my name
(Call my name)
I'll reach out my hand to you
(And I Welcome you)
To my heart
(My future, my heart)
Please don't go away from me
We can work it out
Whatever it may be, girl
Please don't go
(No, no, no, no, no, please don't go)

[Outro]
I'll be there, I'll be right there
I'll be there, I'll be right there
I'll be there, I'll be right there
I'll be there, I'll be right there
I'll be there, I'll be right there
I'll be there, I'll be right there
I'll be there, I'll be right there
I'll be there, I'll be right there