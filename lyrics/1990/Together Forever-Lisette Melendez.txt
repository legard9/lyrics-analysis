[Verse 1:]
It's been so long since
We've been friends in need
And how the love we share in our hearts
To try to keep us apart

It took sometime to build this Trust in me
But now you're all I have too inside
You want the best of my love

[Hook:]
To keep away from the thoughts that we share
Is to feel the pain that
I feel when you're there

See what you want to see
But please have (some) faith in me

[Chorus:]
Together forever yours
Together forever mine
Facing what we feel inside
Ready to stand the test of time

[Verse 2:]
I've seen us come together in my dreams
And felt the kiss that
Makes me love you
Enough to see us through

It took sometime to feel
The way I feel
That's why I know I'm here forever
And you will always be there

[Hook:]
To keep away from the thoughts
That we share
Is to feel the pain that I feel
When you're there

See what you want to see
But please have (some) faith in me

[Repeat Chorus x2:]

[Bridge:]
(Come on)
Together
(I can't wait)
Forever
(Come on)
Together
(Yeah yeah)

Forever
(Come on)
Together
(I can't wait)
Forever
(Come on)
Together
(Yeah yeah)
Forever

(Come on)
Together
(I can't wait)
Forever
(Come on)
Together
(Yeah yeah)

Forever
(Come on)
Together
(I can't wait for love)

[Chorus x4:]