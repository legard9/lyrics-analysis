[Verse 1]
I won't tell you how it is
She won't tell you how it goes
I will bring you up to tears
She won't hit you with the blows
Life is better now this way
Truth by night, life by the day
Hello modern superman
I got millions of your fans
No one tells me how to live
I got places now to be
Secret service of a man
Take no prisoners you see
Jumping straight right into crowds
Telling people how to be
Holding nothing little back
From the big black cherry tree
Yeah, yeah, yeah

[Chorus]
Friends all call me negative
Maybe I'm a pessimist
'Cause of you I'm always late
Gonna miss my nail appointment
Daddy called me Nancy boy
Never let me play with toys
'Cause of him I'm always late
Gonna miss my hair appointment

[Verse 2]
She will tell you where to go
Looking straight right through the lie
She will fall into the trap
Taking shots with all the guys
Screaming hard, her tongue it stings
Waiting for her phone to ring
Positive all days are ruined
Stealing tracks to weave and glue-in
Everyone 'round her sucks
Table top for just two bucks
Washing through the ugly stains
Wishing that her trick would stay
Falling into all the crowds
Hearing everything they say
Zipping up, her mouth is shut
Feeling like the end of days
Yeah, yeah, yeah

[Chorus]
Friends all call me negative
Maybe I'm a pessimist
'Cause of you I'm always late
Gonna miss my nail appointment
Daddy called me Nancy boy
Never let me play with toys
'Cause of him I'm always late
Gonna miss my hair appointment

[Bridge]
Everything's because of you
Thanks I'm late because
All my problems came from you
Thanks I'm late because
My hair's shit because of you
Thanks I'm late because
My nail's shit because of you
Thanks I'm late because
Yeah, yeah, yeah

[Chorus]
Friends all call me negative
Maybe I'm a pessimist
'Cause of you I'm always late
Gonna miss my nail appointment
Daddy called me Nancy boy
Never let me play with toys
'Cause of him I'm always late
Gonna miss my hair appointment
Gonna miss my hair appointment

[Spoken outro]
One more, one more, one more