What I did for love

Everybody's heart is torn and tattered
Mine was not designed for giving up
'cos all I've ever done that really matters
It's what I did for love

So count me with the wound and the shaking
Even when I'm barely standing up
I've tried to offer more than I have taken
It's what I did for love

What I did for love, what I did for love
And I hope that you can see
All that love they've done for me

And when they sit and list the reasons I've existed
I'm afraid it won't add up to very much
'cos I've gathered all I had and gladly rested
That's what I did for love

What I did for love, ...

Some day I'll make silently surrender
This old heart has finally had enough
But I hope that I will always be remembered
For what I did for love