[Verse 1]
Was an ordinary day, sky was stormy very gray
Then I got a call from 
Mary
, sounded worried, told me hurry, "On my away”
Was my response, didn’t wait for her to even try and launch
Into her explanation, walked up she was crying in her palms
Sitting on the porch steps, all her belongings on the lawn
I'm like Nancy Drew Dick Tracy, got a nasty crew, spit crazy
Rhythm addict that’s cinematic like Scorsese
Either adore or hate me but I do it for the babies and
She was without a doubt mad at more than bout her house
Told me to look around, more evictions than you could try to count
Every house on the block in a cataclysmic event
Read the notice from the 
locusts
, my battle instinct intense
It read, “Your street needs to be moved to be improved to make it safe”
In other words, they've been displaced and privatized
Put me on the case, a private eye, no time to waste
I’m ride or die
My motto, “Nowhere to run, nowhere to hide out
Tell the truth, or the world will find out”

[Hook] x2
I’m the drunken sleuth, always on the hunt for truth
Make the evidence ever clear, as a hundred proof
A hundred proof

[Verse 2]
I’m hot on the trail, binoculars spotted betrayal
Superintendent intended to put the lot up for sale
The block chipped in to stop the shoplifting
Now the plot thickens, shocked thinking about what I got in email
The meeting notes from a conglomerate
Who planned to take the city over, someone leaked their top secret document
It read, “Dear conquistadors
We have to shut the soup kitchens down 'cause they enable and feed the poor
And then went on to call the homeless undesirable
And planned to take the benches out the park so they could keep it pure
Also conspired to create security patrols
Assuredly the goal is to jail and police them more
A year later one of the patrol men started beatin’
On an old man who was sleepin’ on the street and almost killed him
Got off ‘cause his lawyer cost a quarter million
, but he got
Nowhere to run, nowhere to hide out
Tell the truth, or the world will find out

[Hook] x2

[Verse 3]
Marchin' the city street named after Martin
, sparking a Swisher Sweet
Wish in Michigan we had a Detroit WikiLeaks
Then it clicked like a Bic lighter
I need an insider for governor Rick Snyder
Got us living in a state of 
emergency, financial managers
Watch it slow motion like time-lapses on cameras
The mayor is Dave Bing
, thinks the city’s his plaything
Acts a fool to make this capital like it’s Beijing
Profits over people but the people are the prophets
Never getting heard by the people that’s in office
Get to the bottom of it like swallowing bottles of booze
Tracing the money and follow the clues
‘Cause 
the advance was given by the foundation funding grants
They don’t believe me, they just think I’m just on one of my drunken rants
It’s a prequel to a land grab but you can’t have
Got nowhere to run, nowhere to hide out
Tell the truth or the world will find out

[Hook] x2