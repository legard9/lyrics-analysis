[Verse 1]
There's no time to give at all
I cause you grief and blow my hatred
Further in your mind
You reach, I run, you fall
On skinned knees you crawl

[Chorus]
I wanna set you free, ah
Yeah, recognize my disease, ah
Love, sex, pain, confusion, suffering
You're there crying, I feel not a thing
Drilling my way deeper in your head
Sinking, draining, drowning, bleeding, dead

[Verse 2]
So you sit and think of love
I wait, hate all the more, I fall
On skinned knees I crawl

[Chorus]
I wanna set you free, ah
Yeah, recognize my disease, ah
Love, sex, pain, confusion, suffering
You're there crying, I feel not a thing
Drilling my way deeper in your head
Sinking, draining, drowning, bleeding, dead

[Verse 3]
Now there's time to give it all
I put my fears behind again
On skinned knees we'll crawl

[Outro]
I wanna set you free, ah
Yeah, recognize my disease, ah
Love, sex, pain, confusion, suffering
You're there crying, I feel not a thing
Drilling my way deeper in your head
Sinking, draining, drowning, bleeding, dead
Love, sex, pain, confusion, suffering