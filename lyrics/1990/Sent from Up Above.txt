[Intro]
Matthaios be wonderin'

[Hook]
When I see you I get dazed (dazed, dazed)
Runnin' in my brain like a race (race, race)
An angel sent from up above (yeah)
I'm gonna give you all my love (ooh)
When I see you I get dazed (dazed, dazed)
Runnin' in my brain like a race (race, race)
An angel sent from up above (yeah)
I'm gonna give you all my love (ooh)

[Verse 1]
Losin' myself when I'm with you
Slidin' left and right (swerve)
I just wish you were my boo
My badly needed light (yuh)
You' always runnin' places that you shouldn't be in
Denying all the facts that you look good, you' always trippin
You look like that you made it
Chinita  wearin' braces
Outgoing and outlandish
You' simple, but you' lavish (yes sir)
B, you drippin' with the Louis V, always classy (drip)
Even when at home you' never really lookin' trashy

[Hook]
When I see you I get dazed (dazed, dazed)
Runnin' in my brain like a race (race, race)
An angel sent from up above (yeah)
I'm gonna give you all my love (ooh)
When I see you I get dazed (dazed, dazed)
Runnin' in my brain like a race (race, race)
An angel sent from up above (yeah)
I'm gonna give you all my love (ooh)

[Verse 2]
I saw you outside smokin' Juul
Stunnin' like a model that ain't no bull
Fancy, fancy, fancy, playin' it like Nancy
Flexin', you' a baddie, fucking vigilante (yuh-yeet)
Maybe you, maybe you're my missing link
I don't see nothin' wrong if you and I get a drink
You got them brown eyes, beauty in and outside
Remove my solo rule if you wanna go for a ride (ride, ride)

[Hook]
When I see you I get dazed (dazed, dazed)
Runnin' in my brain like a race (race, race)
An angel sent from up above (yeah)
I'm gonna give you all my love (ooh)
When I see you I get dazed (dazed, dazed)
Runnin' in my brain like a race (race, race)
An angel sent from up above (yeah)
I'm gonna give you all my love (ooh)