When I came home this morning my clothes were on the floor
You'd have a drink or two or three and maybe even more
Yeah, you had my old dog bite me in a place that I can't mention
There's something that I want to ask you now that I have your attention

Is making a little love out of the question?
Or is it just cause you're not in the mood?
If making a little love out of the question
Then maybe you can fix me up some food?

You have not been home since Tuesday and that's a week ago
You said you car won't start because it's covered up with snow
Well, I wasn't born yesterday, Barton, and I know when you lie
It wasn't snow that made you late, there's no snow in July

Is making a little love out of the question?
Or is it just cause you're not in the mood?
If making a little love out of the question
Then maybe you can fix me up some food? (Yeah, right)

Making a little love is out of the question
And it's not because I'm not in the mood
(I'm always on the mood)
Making a little love is out of the question
And you can fix your own f*cking food

— You know what I think currently? — What? — I think Paula was right. I want a man in my life and not in my house. — Aw — Oh, okay. Come on, take me to bed. — Talk to me, I'm listening