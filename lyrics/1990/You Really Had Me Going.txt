[Verse 1]
G-g-got a funny way of showing it
I can't even lie, you had me going, yeah
Call me up, how you doing? Yeah
Then I get all caught up in the moment
Ay, I like the way you're talking
Whoopsies, almost caught myself falling
Oh nanana, now I can't stop smiling
I got your number but I shouldn't be dialing

[Pre-Chorus]
Ooh ooh, ooh ooh
Usually more outspoken (yeah)
Ooh ooh, ooh ooh
If you love me, then why you ghosting?

[Chorus]
You got a funny way of showing it (yeah)
Okay, I see just how you're doing it, yeah yeah
You go M.I.A., now you're right back in my face
Baby, if you really want me
You got a funny way of showing it (ooh yeah yeah yeah yeah)
Woah (ooh yeah yeah yeah yeah)
You got a funny way of showing it (ooh yeah yeah yeah yeah)
Woah (ooh yeah yeah yeah yeah)

[Verse 2]
It's kinda crazy, here we go again
Get so many chances and you're blowing them
Just be honest, you're gon' play that role again
Used to be the Romeo and Juliet
Ay, I like the way you're talking
Whoopsies, almost caught myself falling
Oh nanana, now I can't stop smiling
I got your number but I shouldn't be dialing

[Pre-Chorus]
Ooh ooh, ooh ooh
Don't get that my heart's broken (yeah)
Ooh ooh, ooh ooh
If you love me, then why you ghosting?

[Chorus]
You got a funny way of showing it (yeah)
Okay, I see just how you're doing it, yeah yeah
You go M.I.A., now you're right back in my face
Baby, if you really want me
You got a funny way of showing it (ooh yeah yeah yeah yeah)
Woah (ooh yeah yeah yeah yeah)
You got a funny way of showing it (ooh yeah yeah yeah yeah)
Woah (ooh yeah yeah yeah yeah)

[Bridge]
Why does this feel wrong? (Why does this feel wrong?)
Even when you're here, you're gone (When you're here, you're gone)
Waited way too long (Waited way too long)
Maybe I should just move on

[Chorus]
You got a funny way of showing it
Okay, I see just how you're doing it, yeah yeah
You go M.I.A., now you're right back in my face
Baby, if you really want me
You got a funny way of showing it
Okay, I see just how you're doing it, yeah yeah
You go M.I.A., now you're right back in my face
Baby, if you really want me
You got a funny way of showing it (ooh yeah yeah yeah yeah)
Woah (ooh yeah yeah yeah yeah)
You got a funny way of showing it (ooh yeah yeah yeah yeah)
Woah (ooh yeah yeah yeah yeah)

[Outro]
Ooh, ooh
Yeah, yeah, yeah
Funny way of showing it