[Verse 1]
It’s unbelievable, it’s strange but true
It’s inconceivable it could happen to you
You go north and you go south
Just like bait in the fish’s mouth
You must be living’ in the shadow of some kind of evil star
It’s unbelievable it would get this far

[Verse 2]
It’s undeniable what they’d have you to think
It’s indescribable, it can drive you to drink
They said it was the land of milk and honey
Now they say it’s the land of money
Whoever thought they could ever make that stick
It’s unbelievable you can get this rich this quick

[Verse 3]
Every head is so dignified
Every moon is so sanctified
Every urge is so satisfied as long as you’re with me
All the silver, all the gold
All the sweethearts you can hold
That doesn’t come back with stories untold
Are hanging on a tree

[Verse 4]
It’s unbelievable like a lead balloon
It’s so impossible to even learn the tune
Kill that beast and feed that swine
Scale that wall and smoke that vine
Feed that horse and saddle up the drum
It’s unbelievable, the day would finally come

[Verse 5]
Once there was a man who had no eyes
Every lady in the land told him lies
He stood beneath the silver sky and his heart began to bleed
Every brain is civilized
Every nerve is analyzed
Everything is criticized when you are in need

[Verse 6]
It’s unbelievable, it’s fancy-free
So interchangeable, so delightful to see
Turn your back, wash your hands
There’s always someone who understands
It don’t matter no more what you got to say
It’s unbelievable it would go down this way