[Verse 1: Jiyoon, Zoa, Jihan]
Maeilmaeil doraganeun (mujigae)
Merry Merry dallyeoganeun (hwansange)
Hangsang ttokgateun jangmyeonin geol (binggeulbinggeul)
Sigani meomchun deut kung nae mami kung (moreuge)
Tteollyeowayo keojyeowayo (ppareuge)
Dasiwado seolleineun geol (Tingle Tingle)

[Pre-Chorus: Jaehee, Soojin, Zoa]
Simjangbakdongi ppallajigo
(Ijeneun meomchul sudo eopsge dwaesseo)
Gakgi dareun moseubui naega
(Ttokgati ne juwireul maemdora)
Sangsangeuro geuryeobon galaxy
Kkumsogeseo deureobon Melody
Deo ullyeo peojine
Ssodajineun byeolbicckkaji wanbyeokhae

[Chorus: Monday, Soeun]
Merry go around
Hangsang gateun goseul doragajyo
Naui mameun binggeulbinggeul Tingle Tingle
Teukbyeolhan Merry go around
Jogeum neurijiman
Gyeolguk neoreul hyanghae dallyeogaji
Jaemissjanha binggeulbinggeul Tingle Tingle
Singihan Merry go around

[Post-Chorus: Soojin]
Ije Round and Round and Round and Round
Run and Run and Run and Run
Turn and Turn and Turn and Turn
Binggeulbinggeul Tingle Tingle ppallajyeoyo

[Verse 2: Jaehee, Jiyoon, Soeun]
Tteollyeoone deo dalkomhage
Nal tto seolleige heundeune
Norigongwon pungseoncheoreom nae mameul
Dugeundugeun jakkuman Shake it Shake it
Oreuraknaerirak nal deureossda nwassda
Ppaljunochopanambo rike a Paradise
Ingongwiseongcheoreom ne juwil maemdolda
Jom deo dagaga neol barabwa Round about

[Pre-Chorus: Jaehee, Soojin, Zoa, Jiyoon]
Jungryeokcheoreom neol kkeureodanggyeo
(Ppallajin nae maeumi jeonhaejilkka)
Galsurok niga johajyeoseo
(Oneuldo neoui gwedoreul maemdora)
Sangsangeuro geuryeobon galaxy
Kkumsogeseo deureobon Melody
Deo ullyeo peojine
Ssodajineun byeolbicckkaji wanbyeokhae
Ijeneun neoege gallae

[Chorus: Monday, Soeun]
Merry go around
Hangsang gateun goseul doragajyo
Naui mameun binggeulbinggeul Tingle Tingle
Teukbyeolhan Merry go around
Jogeum neurijiman
Gyeolguk neoreul hyanghae dallyeogaji
Jaemissjanha binggeulbinggeul Tingle Tingle
Singihan Merry go around

[Bridge: Soeun, Soojin, Jiyoon, Monday, (Zoa), (Jihan)]
Ajigeun josimseureopge
Ttaeroneun jom gwagamhage
Animyeon deo saekdareuge
Neoui mameul ppaeseo bollae jigeumbuteo Countdown
(Mayday Mayday you’re my universe
Mayday Mayday you’re my universe)
(Love)
(Meomchuji malgo dasi han beon deo)

[Chorus: Jiyoon, Monday, Jihan, Soeun]
Merry go around (go around)
Hangsang gateun goseul doragajyo (doragajyeo)
Naui mameun binggeulbinggeul Tingle Tingle (oh, woah)
Teukbyeolhan Merry go around (around)
Eonjenga meomchwodo (woah, woah)
Dasi dol ttaekkaji nan gidaryeo
Sinnajanha binggeulbinggeul Tingle Tingle (woah, no)
Yeongwonhan Merry go around (merry go around)

[Post-Chorus: Soojin]
Ije Round and Round and Round and Round (woah)
Run and Run and Run and Run (yeah)
Turn and Turn and Turn and Turn
Binggeulbinggeul Tingle Tingle ppallajyeoyo