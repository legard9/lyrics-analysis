I'm much too headstrong for my own good
I'm too involved to realize
That your uncertainty will be the death of me
But I lose my will and if looks could kill
I've been struck down dead by your beautiful eyes

Time after time you challenge me for a reason
I know that I'm impossible and deceiving
I'v seen the signs
You're telling me that you're leaving
Time after time...
Time after time...

These games of politics and romance
They're quite impossible to win
I can't unfathom you;
One minute you love me or you say you do
And then I turn my back, I'm under attack
I turn to hold you and you stick the knife in

Time after time you challenge me for a reason
I know that I'm impossible and deceiving
I'v seen the signs
You're telling me that you're leaving
Time after time...
Time after time...

And if I change to be the way you say you want me
I'll only be the way I've always wanted to
You know as well as I do...
You know as well as I do...

Time after time you challenge me for a reason
I know that I'm impossible and deceiving
I'v seen the signs
You're telling me that you're leaving
Time after time...
Time after time...

Time after time you challenge me for a reason
I know that I'm impossible and deceiving
I'v seen the signs
You're telling me that you're leaving
Time after time...

Oh, well I still love you
Time after time...
Oh, I love you
Time after time...
You know, you know, I still love you
Time after time...
Oh, I love you
Time after time after time after time after time...