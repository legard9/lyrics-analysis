Let's go! Na, na, na, na, na, na, nana!
Let's go! Na, na, na, na, na, na, nana!
Let's go! Na, na, na, na, na, na, nana!
Let's go! Na, na, na, na!

I declare a holiday, I won't be answering my cell
No emails or texts, no messages or telly
I leave the front door open for the people that I'm close to
(Life's short!) We're tryin'a get down like we're supposed to
Scratch out all your plans, you got nowhere else to be tonight
Give it just a minute and I promise you that you'll see I'm right
Tell the people outside, if they don't know, they're going to find out
Forget about the past because here there's only right now
We don't need the speakers on your stereo, to dance to
We'll just stomp our feet and clap our hands if we have to!
And if you fall girl, I'll be the one to catch you
The beat is in your bones, you've just got to let it pass through
We don't need no choruses, or words to write a song
I'll just tell you what I'm thinking and we won't be singing long
And when you're lost all you gotta do is find your way to my house, they won't be able to see us with the lights out
(come on!)

There's nothing left to do (Turn the lights out)
Baby, what's wrong with you? (Turn the lights out)
All you gotta do is want a little bit more!
Girl, just shut the door, and turn the lights out!

There's nothing left to do (Turn the lights out)
Baby, what's wrong with you? (Turn the lights out)
All we gotta do is find a place on the floor!
Girl, just shut the door, and turn the lights out!

We're gonna make sure everybody gets the message
Plug this sucker in, we're gonna make this night electric
It's ending and we're finishing and we're starting
When we're starting, leave your problems back at home
'Cause yo, it ain't that kinda party. I wanna feel your skin
I wanna hear you laugh, I wanna touch your hips with no
Chit-chat distraction! Grab my shirt, you can pull me real close
Telling me there ain't a time you've gotta be home
'Cause tonight we're gonna act like every minute lasts a day
When you're this close to me girl
The world is gonna have to wait
And everything you ever need is in this room, I got you
You don't ever have to leave, you can stay here if you want to
Dance until you sweat, scream until you see spots
Everybody's gotta have a piece of what we've got
If you want it, all you gotta do is find your way to my house
They won't be able to see us with the lights out, (come on!)

There's nothing left to do (Turn the lights out)
Baby, what's wrong with you? (Turn the lights out)
All you gotta do is want a little bit more!
Girl just shut the door, and turn the lights out!

There's nothing left to do (Turn the lights out)
Baby, what's wrong with you? (Turn the lights out)
All we gotta do is find a place on the floor!
Girl just shut the door, and turn the lights out!

Let's go! Na, na, na, na, na, na, nana!
Let's go! Na, na, na, na, na, na, nana!
Let's go! Na, na, na, na, na, na, nana!
Let's go! Na, na, na, na!

Let's go! Na, na, na, na, na, na, nana!
Let's go! Na, na, na, na, na, na, nana!
Let's go! Na, na, na, na, na, na, nana!
Let's go! Na, na, na, na!

And turn the lights out!
There's nothing left to do (Turn the lights out)
Baby, what's wrong with you? (Turn the lights out)
All you gotta do is want a little bit more!
Girl just shut the door, and turn the lights out!

There's nothing left to do (Turn the lights out)
Baby, what's wrong with you? (Turn the lights out)
All we gotta do is find a place on the floor!
Girl just shut the door, and turn the lights out!