[Chorus]
Along her vain parade
Along her veins

[Verse 1]
All of seventeen
Eyes a purple green
Treated like a queen
She was on borrowed self-esteem
She would do her dance
A painful masquerade
Spinning you into her web
Along her vain parade
In her uniform
Studded brass and steel
Kissing napkin lipstick stains
And smearing sincerity

[Chorus]
Along her vain parade
Along her veins

[Verse 2]
Time crept up on her
She's in early gray
Her reflection looks concerned
And flowers hit her grave