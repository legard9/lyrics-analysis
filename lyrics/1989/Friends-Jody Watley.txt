[Verse 1: Jody Watley]
Have you ever been stabbed in the back
By someone you thought was really cool
Did they steal your lover? Or was it money?
Or was it lies they told?
Strangers just disguised as your friends
Never again, 'cause now you know, that...

[Chorus: Jody Watley]
Friends will let you down
Friends won't be around
When you need them most
Where are your friends?
Oh, friends are hard to find
Friends yours and mine
I'm talking 'bout your friends

[Verse 2: Jody Watley]
Smiles they hide behind
Never know what's on their mind
Could be true deception
Jealousy and envy reign
Never want to see you get ahead
They just hold you back

[Chorus: Jody Watley]
Friends will let you down
Friends won't be around
When you need them most
Where are your friends?
Oh, friends are hard to find
Friends yours and mine
I'm talking 'bout your friends

[Verse 3: Rakim]
Friends are hard to find so be careful
You'll get fried in the end to remind and prepare you
That some ain't that bad, but one might back-stab
To get their fingertips on what one might have
Bite the hand that feeds you, lead the people who need you
From those who hold you back and mislead you
To be a leader, don't get lead on or lead in
The wrong direction, a dead-ends next then
Use your detour, life's like a seesaw
Ups and downs and I bet there'll be more
Potholes and obstacles, in a path that's righteous
At times you need a hand to fight this
Way of life, straighten up, take the thought and replace it
And don't you act two-faceded
Cause jealousy and envy and you still act friendly
You'll be fried in the end when you pretend to be

[Chorus: Jody Watley]
Friends will let you down
Friends won't be around
When you need them most
Where are your friends?
Oh, friends are hard to find
Friends yours and mine
I'm talking 'bout your friends

[Verse 4: Rakim]
You used to kiss me, tell me you missed me
But now you try glaze me, play me, and diss me
I'm wide awake, ready to break so we'd argue
What happened to the kisses and "Ra, how are you?"
Forgot about the times when I rhymed when I bathed you
Rings was the only little things that I gave you
Still ain't thankful, you're still complaining
Used to be a quiet storm but now it's raining
Harder than ever, I'm thinking whether
If we should be friends, let it end, is it better
To forget or remember, your body's tender
The vibes that I send her makes her surrender
The feelings I capture, caught in a rapture
No woman can't match her, so when I'm looking at ya
Paint a perfect picture so you can remember me
But you'll get fried in the end if you pretend to be

[Chorus: Jody Watley]
Friends will let you down
Friends won't be around
When you need them most
Where are your friends?
Oh, friends are hard to find
Friends yours and mine
I'm talking 'bout your friends

[Outro: Jody Watley]
Friends will let you down
Friends won't be around
When you need them most
Where are your friends?
Yeah, friends are hard to find
Friends yours and mine
I'm talking 'bout your friends