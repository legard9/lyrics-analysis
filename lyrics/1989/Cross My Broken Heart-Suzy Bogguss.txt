Maybe it's true we're not together
But when I close my eyes
You're still here
The sweet taste of love can last forever
Sometimes it makes me cry
But I still find a smile for every tear
Maybe I am blue
Maybe I'm crazy too
But I'll always love you
Cross my broken heart
When I hear your name in conversation
I melt into a dream
I can't forget
Don't get me wrong, I'm not complaining
It hurts me now and then
But I'd do it all again
With no regrets
Maybe I am blue
Maybe I'm crazy too
But I'll always love you
Cross my broken heart
I could say love wasn't worth
All the tears that it took
But I wouldn't lie to you now
You know I never could
Maybe I am blue
Maybe I'm crazy too
But I'll always love you
Cross my broken heart
Maybe I'm crazy too
But I'll always love you
Cross my broken heart...