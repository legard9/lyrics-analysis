[Intro: Roger Taylor & 
Freddie Mercury
]
I'm the invisible man
I'm the invisible man
Incredible how you can
See right through me

[Interlude 1: Roger Taylor]
Freddie Mercury!

[Verse 1: Freddie Mercury]
When you hear a sound
That you just can't place
Feel something move
That you just can't trace
When something sits
On the end of your bed
Don't turn around
When you hear me tread

[Chorus: Roger Taylor & 
Freddie Mercury
]
I'm the invisible man, I'm the invisible man
Incredible how you can
See right through me
I'm the invisible man, I'm the invisible man
It's criminal how I can
See right through you

[Interlude 2: Freddie Mercury]
John Deacon!

[Verse 2: Freddie Mercury]
Now I'm in your room and I'm in your bed
And I'm in your life and I'm in your head
Like the CIA or the FBI
You'll never get close
Never take me alive

[Chorus: Roger Taylor & 
Freddie Mercury
]
I'm the invisible man, I'm the invisible man
Incredible how you can
See right through me
I'm the invisible man, I'm the invisible man
It's criminal how I can
See right through you

[Bridge: Freddie Mercury]
Hah, hah, hah, hello
Hah, hah, hah, okay
Hah, hah, hah, hello, hello, hello, hello
Never had a real good friend
Not a boy or a girl
No one knows what I've been through
Let my flag unfurl
So I make my mark
From the edge of the world
From the edge of the world
From the edge of the world

[Interlude 3: Freddie Mercury]
Brian May, Brian May!

[Verse 3: Freddie Mercury]
Now I'm on your track
And I'm in your mind
And I'm on your back
But don't look behind
I'm your meanest thought
I'm your darkest fear
But I'll never get caught
You can't shake me, shake me, dear

[Chorus: Roger Taylor & 
Freddie Mercury
]
I'm the invisible man, I'm the invisible man
Incredible how you can
See right through me
I'm the invisible man, I'm the invisible man
It's criminal how I can
See right through you

[Interlude 4: Roger Taylor & 
Freddie Mercury
]
Look at me, look at me
Rrrroger Taylor!

[Outro: Freddie Mercury]
Shake you, shake you, dear