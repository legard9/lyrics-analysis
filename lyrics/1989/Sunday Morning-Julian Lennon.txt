Through the window the plane goes by
Watch my lips as I say good-bye
Can't you see I don't want to cry
On this Sunday morning

Drop my change as I move away
Can't you see I just want you to stay
Please don't hate me for being this way
On this Sunday morning

Oh, please don't keep me hanging on
Oh, oh, oh won't you hold me in your arms

Through the window the plane goes by
Waiting for you to call and cry
Please don't do this or I shall die
On this Sunday morning

Oh, please don't keep me hanging on
Oh, oh, oh won't you hold me in your arms
Oh, please don't keep me hanging on
Oh, oh, oh won't you hold me in your arms

On this Sunday morning