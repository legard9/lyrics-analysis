<<<<<<< HEAD
Stanching the free flow of blood from an injury remains a holy grail of clinical medicine. Controlling blood flow is a primary concern and first line of defense for patients and medical staff in many situations, from traumatic injury to illness to surgery. If control is not established within the first few minutes of a hemorrhage, further treatment and healing are impossible.

At UC Santa Barbara, researchers in the Department of Chemical Engineering and at Center for Bioengineering (CBE) have turned to the human body's own mechanisms for inspiration in dealing with the necessary and complicated process of coagulation. By creating nanoparticles that mimic the shape, flexibility and surface biology of the body's own platelets, they are able to accelerate natural healing processes while opening the door to therapies and treatments that can be customized to specific patient needs.

"This is a significant milestone in the development of synthetic platelets, as well as in targeted drug delivery," said Samir Mitragotri, CBE director, who specializes in targeted therapy technologies. Results of the researchers' findings appear in the current issue of the journal ACS Nano.
The process of coagulation is familiar to anyone who has suffered even the most minor of injuries, such as a scrape or paper cut. Blood rushes to the site of the injury, and within minutes the flow stops as a plug forms at the site. The tissue beneath and around the plug works to knit itself back together and eventually the plug disappears.

But what we don't see is the coagulation cascade, the series of signals and other factors that promote the clotting of blood and enable the transition between a free-flowing fluid at the site and a viscous substance that brings healing factors to the injury. Coagulation is actually a choreography of various substances, among the most important of which are platelets, the blood component that accumulates at the site of the wound to form the initial plug.
"While these platelets flow in our blood, they're relatively inert," said graduate student researcher Aaron Anselmo, lead author of the paper. As soon as an injury occurs, however, the platelets, because of the physics of their shape and their response to chemical stimuli, move from the main flow to the side of the blood vessel wall and congregate, binding to the site of the injury and to each other. As they do so, the platelets release chemicals that "call" other platelets to the site, eventually plugging the wound.
But what happens when the injury is too severe, or the patient is on anti-coagulation medication, or is otherwise impaired in his or her ability to form a clot, even for a modest or minor injury?

That's where platelet-like nanoparticles (PLNs) come in. These tiny, platelet-shaped particles that behave just like their human counterparts can be added to the blood flow to supply or augment the patient's own natural platelet supply, stemming the flow of blood and initiating the healing process, while allowing physicians and other caregivers to begin or continue the necessary treatment. Emergency situations can be brought under control faster, injuries can heal more quickly and patients can recover with fewer complications.

"We were actually able to render a 65 percent decrease in bleeding time compared to no treatment," said Anselmo.
According to Mitragotri, the key lies in the PLNs' mimicry of the real thing. By imitating the shape and flexibility of natural platelets, PLNs can also flow to the injury site and congregate there. With surfaces functionalized with the same biochemical motifs found in their human counterparts, these PLNs also can summon other platelets to the site and bind to them, increasing the chances of forming that essential plug. In addition, and very importantly, these platelets are engineered to dissolve into the blood after their usefulness has run out. This minimizes complications that can arise from emergency hemostatic procedures.

"The thing about hemostatic agents is that you have to intervene to the right extent," said Mitragotri. "If you do too much, you cause problems. If you do too little, you cause problems.
"
These synthetic platelets also let the researchers improve on nature. According to Anselmo's investigations, for the same surface properties and shape, nanoscale particles can perform even better than micron-size platelets. Additionally, this technology allows for customization of the particles with other therapeutic substances -- medications, therapies and such -- that patients with specific conditions might need.

"This technology could address a plethora of clinical challenges," said Dr. Scott Hammond, director of UCSB's Translational Medicine Research Laboratories. "One of the biggest challenges in clinical medicine right now -- which also costs a lot of money -- is that we're living longer and people are more likely to end up on blood thinners. When an elderly patient presents at a clinic, it's a huge challenge because you have no idea what their history is and you might need an intervention."

With optimizable PLNs, physicians would be able to strike a finer balance between anticoagulant therapy and wound healing in older patients, by using nanoparticles that can target where clots are forming without triggering unwanted bleeding. In other applications, bloodborne pathogens and other infectious agents could be minimized with antibiotic-carrying nanoparticles. Particles could be made to fulfill certain requirements to travel to certain parts of the body -- across the blood-brain barrier, for instance -- for better diagnostics and truly targeted therapies.

Additionally, according to the researchers, these synthetic platelets cost relatively less, and have a longer shelf life than do human platelets -- a benefit in times of widespread emergency or disaster, when the need for these blood components is at its highest and the ability to store them onsite is essential.
Further research into PLNs will involve investigations to see how well the technology and synthesis can scale up, as well as assessments into the more practical matters involved in translating the technology from the lab to the clinic, such as manufacturing, storage, sterility and stability as well as pre-clinical and clinical testing.
=======
Stanching the free flow of blood from an injury remains a holy grail of clinical medicine. Controlling blood flow is a primary concern and first line of defense for patients and medical staff in many situations, from traumatic injury to illness to surgery. If control is not established within the first few minutes of a 
hemorrhage,
 further treatment and healing are impossible.
At UC Santa Barbara, researchers in the Department of Chemical Engineering and at Center for Bioengineering (CBE) have turned to the human body's own mechanisms for inspiration in dealing with the necessary and complicated process of 
coagulation
. By creating 
nanoparticles
 that mimic the shape, flexibility and surface biology of the body's own 
platelets
, they are able to accelerate natural healing processes while opening the door to 
therapies and treatments that can be customized to specific patient needs
.
"This is a 
significant milestone
 in the development of synthetic platelets, as well as in targeted drug delivery," said Samir Mitragotri, CBE director, who specializes in targeted therapy technologies. Results of the researchers' findings appear in the current issue of the journal ACS Nano.
The process of coagulation is familiar to anyone who has suffered even the most minor of injuries, such as a scrape or paper cut. 
Blood rushes to the site of the injury, and within minutes the flow stops as a plug forms at the site. The tissue beneath and around the plug works to knit itself back together and eventually the plug disappears.
But what we don't see is the 
coagulation cascade
, the series of signals and other factors that promote the clotting of blood and enable the transition between a free-flowing fluid at the site and a viscous substance that brings healing factors to the injury. Coagulation is actually a choreography of various substances, among the most important of which are platelets, the blood component that accumulates at the site of the wound to form the 
initial plug.
"While these platelets flow in our blood, they're relatively inert," said graduate student researcher Aaron Anselmo, lead author of the paper. As soon as an injury occurs, however, the platelets, because of the physics of their shape and their response to chemical stimuli, move from the main flow to the side of the blood vessel wall and congregate, binding to the site of the injury and to each other. As they do so, the platelets release chemicals that "call" other platelets to the site, eventually plugging the wound.
But what happens when the injury is too severe, or the patient is on anti-coagulation medication, or is otherwise impaired in his or her ability to form a clot, even for a modest or minor injury?
That's where platelet-like nanoparticles (PLNs) come in. These tiny, platelet-shaped particles that 
behave just like their human counterparts
 can be added to the blood flow to supply or augment the patient's own natural platelet supply, stemming the flow of blood and initiating the healing process, while allowing physicians and other caregivers to begin or continue the necessary treatment. Emergency situations can be brought under control faster, injuries can heal more quickly and patients can recover with fewer complications.
"We were actually able to render a 65 percent decrease in bleeding time 
compared to no treatment
," said Anselmo.
According to Mitragotri, the key lies in the PLNs' mimicry of the real thing. By imitating the shape and flexibility of natural platelets, PLNs can also flow to the injury site and congregate there. With surfaces functionalized with the same biochemical motifs found in their human counterparts, these PLNs also can 
summon other platelets to the site and bind to them
, increasing the chances of forming that essential plug. In addition, and very importantly, these platelets are engineered to dissolve into the blood after their usefulness has run out. This minimizes complications that can arise from emergency hemostatic procedures.
"The thing about 
hemostatic agents
 is that you have to intervene to the right extent," said Mitragotri. "If you do too much, you cause problems. If you do too little, you cause problems.
"
These synthetic platelets also let the researchers improve on nature. According to Anselmo's investigations, for the same surface properties and shape, nanoscale particles can perform even better than micron-size platelets. Additionally, this technology allows for customization of the particles with other therapeutic substances -- medications, therapies and such -- that patients with specific conditions might need.
"This technology could address a plethora of 
clinical challenges,
" said Dr. Scott Hammond, director of UCSB's Translational Medicine Research Laboratories. "One of the biggest challenges in clinical medicine right now -- which also costs a lot of money -- is that we're living longer and people are more likely to end up on blood thinners. When an elderly patient presents at a clinic, it's a huge challenge because you have no idea what their history is and you might 
need an intervention
."
With optimizable PLNs, physicians would be able to strike a finer balance between anticoagulant therapy and wound healing in older patients, by using nanoparticles that can target where clots are forming without triggering unwanted bleeding. In other applications, 
bloodborne pathogens
 and other infectious agents could be minimized with antibiotic-carrying nanoparticles. Particles could be made to fulfill certain requirements to travel to certain parts of the body -- across the blood-brain barrier, for instance -- for better diagnostics and truly targeted therapies.
Additionally, according to the researchers, these synthetic platelets 
cost relatively less
, and 
have a longer shelf life than do human platelets
 -- a benefit in times of widespread emergency or disaster, when the need for these blood components is at its highest and the ability to store them onsite is essential.
Further research into PLNs will 
involve investigations
 to see how well the technology and synthesis can scale up, as well as assessments into the more practical matters involved in translating the technology from the lab to the clinic, such as manufacturing, storage, sterility and stability as well as pre-clinical and clinical testing.
>>>>>>> master
________________________________________
Story Source:
The above story is based on materials provided by University of California - Santa Barbara. The original article was written by Sonia Fernandez. Note: Materials may be edited for content and length.