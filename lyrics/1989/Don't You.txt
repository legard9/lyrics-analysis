You said that things were gonna change
This time around
But I’ve heard that before
My mind is in a haze
And I haven’t seen straight for days
Will this ever change
'cause I know that I’ve
I've made some mistakes before
And I won’t be around to let you settle the score

[Chorus]
Listen to me once
Listen to me twice
I can leave you faster than a Saturday night
Don’t you forget it
Driving down the road
And missing all the shows
I can quit you harder than you’ll ever want to know
Don’t you forget it
You always said you weren’t like the others
But I never saw that
I saw that in you
You spent years trying to change me
But what did that prove?
It’s just that time went by
I know that change
Change can be rough
But all that it proves is that
It never mattered enough

[Chorus]
Listen to me once
Listen to me twice
I can leave you faster than a Saturday night
Don’t you forget it
Driving down the road
And missing all the shows
I can quit you harder than you’ll ever want to know
Don’t you forget it
'cause I don’t know
If I can turn this thing around
It didn’t matter
Like I knew it never would

[Chorus]
Listen to me once
Listen to me twice
I can leave you faster than a Saturday night
Don’t you forget it
Driving down the road
And missing all the shows
I can quit you harder than you’ll ever want to know
Don’t you forget it