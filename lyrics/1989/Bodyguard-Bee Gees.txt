[Verse 1: Robin Gibb]
It hurts me when you cry
When you feel the love is gone
Don't give up on us when you got so much to believe in
And so I beg you please stay yeah
Danger in the night
A flame that never dies
In a world of lonely faces
You need someone to survive
A love like yours should never live alone

[Chorus: Barry Gibb and Robin Gibb]
Let me be your bodyguard, (I can be useful to you)
We can just get excited, stop before we go too far
Let me be your bodyguard (Living in my protection)
We can just get ignited
Lost inside
It's only you and me

[Verse 2: Robin Gibb]
There's things you shouldn't do
You lose your self-control
Should the eyes of a perfect stranger
Take you down to a new sensation
Just turn back to me
Don't wait till it's gone
Don't go where you don't belong
In a world of pain and sorrow
Every wish is your command
A heart like yours
Should always find a home

[Pre-Chorus: Barry Gibb]
I know the signs and I'm telling you you're living on borrowed time
But a heart needs love like a flower needs rain
Like I'm making you mine

[Chorus - Variation: Barry Gibb and Robin Gibb]
Let me be your bodyguard, (I can be useful to you)
We can just get excited, stop before we go too far
Let me be your bodyguard. (Living in my protection)
We can just get ignited
What we had never known all our lives
I pray it's not too late
I would die by your side my love
I'm the life that you save

[Bridge: Robin Gibb and Barry Gibb]
In this world of pain and sorrow
Every wish is your command
A heart like yours should never live alone
I used to dream of a special one
And now I feel it's coming true
But I couldn't let go, I'm a jealous guy
What you're putting me through

[Chorus] [x2]