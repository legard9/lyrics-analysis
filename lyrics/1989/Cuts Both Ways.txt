Cut off your head
Shut up your face
Here is the time
Now is the place
Cut off your head
Why should you wait
Don't think about it
It's much too late
Well, it's been a lot of work but now I'm
Right back where I used to be
Surrounded by the stupid things I had before I started out
The Dixie cups, the Tonka trucks, the Action Jackson figurines
The motorcycle wouldn't go - not even when you pulled the string
The comic books I didn't read - too stupid for me even then
The light bulb oven wouldn't cook the pasty stuff in little tins
The woolen shorts, the clamp on skates
The duck, the duck, the duck, the goose
The busted nose, the broken eardrum
Falling off the skateboard hands
The plasticine, the orange aspirin, play-doh crud in little cans
Keep coming back in different combinations
Cut off your head
Don't hesitate
Do it today
It's great
Throw out your brains
They're a disease
Cut off your head
Think with your knees
Nananananananana
Nananananananana
Nananananananana
Nananananananana
You've tattooed all your body parts and pierced your lip
And pierced your nose
And bolted this and branded that and done the drugs
And done your friends
And done the friends of other friends
So why not try some amputation
You can feel so stupid when it's over
Nananananananana
Nananananananana
Nananananananana
Nananananananana
The ratna, padma, karma, vajra, Buddha are all in all o.k
There is a pain, there is a cause, there is a goal, there is a way -
YOU KNOW THE WAY!
Cut off your head
Do it yourself
Don't ask a friend
That's gross
Use a big knife
That cuts both ways
Open your heart
Cut off your head
Nananananananana
Nananananananana
Nananananananana
Nananananananana