[Instrumental]

[Pre-Chorus]
What you need's a little change of heart
(Change of heart)
Forget this fear and frustration
Love will always play the greater part
(Greater part)
When your battles wear you down
Here's my advice

[Chorus]
When you're feeling down and out
And you've got troubles on your mind
Love will save the day
When you're feeling full of doubt
And fear has got you in a bind
Love will save the day
When your world's falling apart
All you have to do is say a prayer
And love will save the day
There's an answer in your heart
So let your light shine on, my dear
And love will save the day
Love will save the day
Love will save the day
Love will save the day
Love will save the day

[Outro]
Oh, love will save the
Oh, love will save the
Oh, love will save the day, hahaha
Oh, love will save the
Oh, love will save the
Oh, love will save the day, hahaha
Nananana, nanananananana, love will save the day
Nananana, nanananananana, love will save the day
Save the day, save the day
Save the day, love will save the day
Oh, oh, oh, oh
Nananananananana, love will save the day
Nananananananana, love will save the, oh
Love will save the day
Love will save the
Love will save the
Love will save the, save the, save the
Save the, save the
Save the, save the, love will save the day