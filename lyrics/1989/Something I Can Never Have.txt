(Harry, Chris)
Haz, I heard this vicious rumour just the other day (what?)
I don't know if this is the right time or place to say (I mean you have brought it up, so)
But I really hope it's false, cos it really is a scary thing
I heard through the grapevine that you've turned vegetarian (I mean) [Yeah, yeah]

Is it true? I'll never have another Nando's with you?
You gotta help me out cos I am confused
Is this forever or just a phase?
Is it real? No more ironic but not ironic happy meals? (we've never even had a happy meal)
It doesn't matter that we've never had a happy meal
You're missing the point, I'm trying to express the way I feel
(you could've texted me or something Chris)

{Chorus}
So I can't cook us a nice romantic roast when it's cold and damp?
Do you remember the way we shared spaghetti bolognese like in Lady and the Tramp? (that never happened)
Life was different back then and I loved it
Now I have to think up ways to make aubergine not rubbish (I like aubergine)
I've stuffed it, with chestnut stuffing
Please tell me something

Look Chris, I thought you might overreact
That's why I wrote a note, stuck it on the fridge and then left the flat
Then came back, destroyed the note and just texted your fiancée
Asked her to break it softly, when I knew I was out of the country
On my birthday. It's the only way that I knew I'd be safe
I think that this has evoked a disproportionate amount of rage
I know you feel betrayed, but I thought that I should try it
I'm not changing as a person, I'm just changing my diet
See the thing is, I was talking to some poetry friends recently
Who said that I should think about living my life less meatily
And I don't know if you've heard about this film called 'Cowspiracy'
I've not watched it but I've heard it's got some powerful imagery
And in answer to your question about if it's just a phase
I said I'd give up meat for lent so yes, that is just 40 days
But if you compare this to that original temptation
Then right now that makes me Jesus, and right now that makes you Satan

{Chorus}
But I can't cook us a nice romantic roast when it's cold and damp? (that only happened once)
Do you remember the way we shared spaghetti bolognese like in Lady and the Tramp? (think that might've been a dream)
Life was different back then and I loved it
Now I have to think up ways to make kale not rubbish
(I mean I don't like kale either. No one likes it. It's just a- like a smoothie ingredient) [Alright fair enough]
Please tell me something
(Okay Chris)

We can still go to Nando's, I can guarantee that
It's just next time I'll go for the mushroom and halloumi wrap
I know you're going to miss the eating meat we do
But the most important thing for me is that I'm eating it with you
So it may not be a meat ting, but it's still a you and me ting
Sometimes a halloumi ting, it's not like I'm a vegan
I still care about your feelings, it just comes with a warning
Next time we have a curry, I'll just chuck a bit of quorn in

You say "Quorn's great, yeah, it tastes just like chicken!"
You know what else tastes like chicken? Chicken
I see the veggie bug but it's just not bitten me yet
I'm just a little upset

I'll be honest Chris, I did not expect this either
I had thought for ever more that I would be a meat eater
So I know that you might not be a believer
But at least now you can have the pepperoni from my pizza

{Chorus}
Yeah but I still can't cook us a nice romantic roast when it's cold and damp? (that was really nice to be honest)
Remember the way we shared spaghetti bolognese like in Lady and the Tramp? (that might've been you and Sam?)
Life was different back then and I loved it (it was like a month ago)
Now I have to think up ways to make- [gimme a vegetable] {mushrooms!} mushrooms not rubbish
(I like mushrooms) [I like mushrooms]
Please tell me something

(Let me lay it out for you Chris) [Yeah go on, go on]
Would you eat a human? [No!] Well this is literally the same
I think you've got the meaning of literally mixed up again (how dare you)
That is literally the most offensive thing that you could say
What about- Stop! There's literally thousands of kids in here today
Well I literally want my omnivore mate back
You say it's just a phase but I	have seen that before
What if one day we meet, and I haven't seen it
It's like- I've forgotten the lyrics
[But that's okay, we're gonna go again!]
(I really like this bit, I think we should do it again)
[Yeah okay, d'you reckon? I don't think anyone noticed]

I literally just miss my favourite omnivore
You say it's just a phase but I	know where that's gone before
What if one day we meet and I'm left wanting more?
Like I know your name but I don't know why like Gabby Agbonlahor (shoutout Gabby Agbonlahor! Villa fans in the house!)

So it may be after lent I'll keep on going with the lentils
There might not be a meat feast on Easter day
And I know it drives you mental, but it's just environmental
If you hate beetroot you can still be true to your mate
I'm just doing what I can to be a vaguely better person
But if even after this you're still insisting that I worsen
I guess that I could have some secret meat in times I spend with you
'Cause that's the kind of thing a veggie bessie friend'll do
(That's for you) [Seriously?] (That's for you) [Yeah?] (Yeah)

{Chorus}
So I can cook us a nice romantic roast when it's cold and damp? (as long as you don't tell anyone Chris!)
We can share bolognese on those long summer days like in Lady and the Tramp? (still never gonna happen)
Life can be different, I'll still love it
I don't have to think up ways to make vegetables not rubbish
(I mean you like most vegetables) [I do, I do like most vegetables] (It's just the kale thing wasn't it)
I guess I've learnt something