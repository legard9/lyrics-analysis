[Xue Xing, spoken]
David, we have a saying:
"Dà nàn bù sǐ, bì yǒu hòu fú"
"If somehow you survive when you should have died, good fortune will follow."
I hope this proves true for you all

[Ensemble]
Oh
Oooh

I was taught

[Female soloist]
In the land of the free

{Ensemble]
I was taught

[Male soloist]
Where they screamed at me, "Life"

[Ensemble]
"Life!"

[Male soloist]
"Liberty!"

[Ensemble]
"Liberty!"

[Female soloist]
How can I turn my back

[Ensemble]
On democracy?

And I forget what I really should know
I forget that it's such a big, big show

[Female Ensemble]
Still I dream

[Male soloist]
Still I dream

[Female Ensemble]
Still I dream

[Female soloist]
Still I dream

[Ensemble]
That our people can be

[Male soloist]
They can be wise enough

[Ensemble]
Wise enough

[Male soloist]
Just enough

[Ensemble]
Just enough
Worthy of trust enough, kind enough, smart enough

[Female soloist]
With a big heart enough

[Female soloist]
Good and grown-up enough

[Male soloist]
To lift us up

[Ensemble]
Lift us up!

[Female soloist]
Lift us up!

[Ensemble]
Lift us up!

[Male soloist]
Lift us up!

[Ensemble]
Lift us up!

[Male soloist]
Lift us up!

[Ensemble]
Lift us up!

[Female soloist]
And I dream

[Ensemble, simultaneously]
Welcome to America, Welcome to America

This is, this is
America

[Male soloist]
Look, our country's a disaster

[Ensemble]
This is America

[Male soloist]
In so many ways
But we have the power

[Ensemble]
We have
We have
We have

[Male soloist, simultaneously]
We have the power
We have the power
We have

[DHH, simultaneously]
This is America
This is America
This is America

[Ensemble]
I believe, I believe
I believe, I believe
I believe, I believe
I believe, I believe
I believe, I believe
I believe, I believe
I believe, I believe
I believe, I believe

[DHH, spoken]
"Dà nàn bù sǐ, bì yǒu hòu fú"
"Good fortune will follow, if we somehow survive."

[Ensemble]
In America