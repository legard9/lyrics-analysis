Even for hardcore B-Boys fans, little was known of Felicia Villarreal, except that she had been Mike D's personal assistant and worked for the Beastie Boys' management company.  
According to 9th Beastie, she has steadfastly declined her spot in Beastiemania…
  I tracked down the elusive Ms. Villarreal, and she agreed to participate in this interview.  So, get ready, because this is the first time Felicia has EVER gone on record about her time with the Boys…
BEN LIFLAND: To get started, can you bring us back a memory of 
when you first started working with the Beastie Boys?
FELICIA VILLARREAL: Yeah, I was thinking about this the other day.  It was right when I first started with them.  
I mean, we had worked together on Lollapalooza
, but this was my first tour with the Beastie Boys, specifically.  So there was some pressure, and we were somewhere in 
Australia
 or Japan, I don’t know…  We were all crammed in an elevator, and I was holding all my bags, and my stuff, because you know—a workaholic has to carry everything with her.  Anyway, the doors opened, and I stepped…  and the elevator had stopped, and was halfway on the floor, and halfway not on the floor.  And I fell backwards, and dropped everything.  I was SO embarrassed — I mean, I didn’t really know them yet.  
Until I live my last days, I’ll never forget Cane saying, “Aiight, see ‘ya later… Rookie.”
  And I just thought that was a perfect initiation for what was to come, in truly being on the road with the entourage.  They were funny like that.  We had a great time.  Everybody was family
BL: No doubt.  That’s always the impression I’ve gotten.  That the Boys go out of their way to be kind to the members of their team, and I would imagine that Adam Yauch helped set that precedent?
FV: Definitely, he did
BL: Of course, everyone who knew Adam or the band will always feel that loss.  As their manager for so many years, can you tell us what he meant to you?
FV: For all those years, I was the team leader, constantly looking out for everyone.  I was always a little bit different than typical managers or label people who went out on the road.  I’ve had a deep love, empathy, and understanding for everyone I’ve worked with, and definitely, that was true with Adam.  It was like being a mama bear, like having all these kids, who you love more than anything in the world.  You want to encourage, support, and protect them.  You see them as the unbelievable, wonderful individuals they are.  For me, that is always how I felt about Adam and the Boys.  
After he passed away, a lot of us got together in LA, and everybody was connecting and expressing their true emotions.
  It honestly felt like I had lost a child.  Because he was somebody that I loved so deeply — who I cared about so much, and I always wanted to see him happy and successful.  It was really hard
BL: I hear you…  Like so many, I felt it, too.  Are you still in touch with Adam H. and Mike?
FV:  We check in occasionally.  Absolutely.  
I had been in touch with Mike a little more frequently because he had come out to LA.  He was curating an exhibit at the at the MOCA in downtown Los Angeles.
 It was amazing for us all to get back together.  It ended up being a moment that everyone could reconnect and it was so nice that Mike was here.  When we all realized that Adam had gone, we were able to spend time together, and talk about memories of Adam, reminiscing on the things we loved about him and all the times we'd had together
BL: Those memories must have been intense
FV: We all supported each other and we had a nice little memorial.  Somebody put together a really nice video.  It was amazing because many of us were there together and we watched 15-20 years of our lives on that screen. It was truly bittersweet.  It hurt so bad to realize it would never be the same because Adam wouldn’t be there.  But we all felt lucky to have had all those moments together as a “Beastie Boys Family…”
BL: That makes sense…  It’s nice to hear how the extended Beastie family came together when everyone was hurting.  So let’s rewind, and talk a little bit about your decision to give up that spot as their manager.  About ten years ago, you resigned from Silva Artist Management.  A lot of our readers and the good people out there — they know so little about you.  Could you explain a little bit more about why you made that decision?
FV: I was always really committed to The Boys, and the family at 
Silva Artist Management
.  Or when I 
worked on Lollapalooza, I knew everyone, from the artists, to the crew and the volunteers.  And I always saw everyone as being equal across the board.
  I had that love for others and I made a lot of really amazing friends.  Though I cherished those relationships, they took a lot.  One day, I just kind of looked up, and noticed how everyone around me was having children.  I started thinking…
BL: I know that feeling, where all of sudden, family takes precedence…  Where are you from originally?
FV: I grew up in a small town called Las Cruces, New Mexico.  I married my high school sweetheart.  Two days after my 16th birthday, we had our first date.  Paul and I have been married 25 years now
BL: That is amazing
FV: I know, right?  In the music business and still trying to balance being married
BL: So, how is it that you were able to manage the Beastie Boys for a decade or so, and yet, as the fan base goes, most people have been unaware of how significant your role was?
FV: It’s kind of funny.  I recently took my kids to the 
California Science Center, and there’s a stealth bomber
 there on exhibit.  We were walking underneath it, and I was laughing to myself.  My kids were like, What’s funny?  I told them that I had my own little secret identity—I used to call myself “stealth.”  I was always there, watching out.  I knew what was going on and had to make sure everything was ok.  But like a stealth bomber, nobody ever saw me.  I like to lay low.  I think it’s my mama bear instinct.  I was there, but I wanted to stay low-key and fit in, so I could really observe what was going on
BL: Was there a self-preservation dynamic to that?  By staying in the cut, did that help prevent burnout?
FV: Well, yes.  
The work was always challenging.  As a woman and with the things that I've accomplished, I have had to work so hard.
 My husband and I always used to say, We’re not going to have kids.  We are going to accomplish big things and break cycles of dysfunction.  And then one day, my thinking changed a little bit.  I was trying to schedule dinner with my husband on a Friday night, but usually I would be about three hours late…
BL: Haha
FV: 
Yes, Paul is an incredibly patient man.  But I started feeling like I was missing something in life.  We went to the beach to talk and I said, “I am almost 35, and I feel like I'm missing out on you.  I wish we could spend more time together.”
  So I told Paul I would give him one week to think about this—because the Boys are about to start another record—so if you want to have kids, we have a week to make that decision
BL: No pressure
FV: My husband is like, What?!  A week went by, and he didn’t say anything.  I thought, I must have blown his mind.  But a couple of weeks later, we were finally talking about it, and I thought, I'm thirty-five; it’s going to take forever…  Then we were on a jet on our way to San Francisco, we were going to play BFD at Shoreline Amphitheatre.  I got super-sick on take off and everyone was cracking up and laughing at me because I got super nauseous and ran for the bathroom as soon as the seat belt light went off.  It was like I was on the worst flight of my life.  And I thought, This is so weird, it’s so unlike me to get so sick.  Everyone was laughing at me, and teasing me, because they never saw me get sick.  When I got home, I took a pregnancy test…
BL: Ah-ha!
FV: 
The next morning, we were going to Irvine Meadows for the KROQ Weenie Roast.
  It all happened so fast—just a month later.  I’ll never forget, because Diane (
Mix Master Mike’s
 wife) was sitting next to me in the car on the way to Irvine.  She noticed that I looked really tired and sick today, and she asked, “What’s wrong, because that’s so not like you.  You’re always so happy.”  And I just really felt sick again, and she’s like, “Oh my gosh, you’re pregnant.”  At that point, I wasn’t ready to say anything!  Anyhow, I’ll never forget, it was one of the most heartbreaking moments of my life.  Adam Horovitz turned around, looking at me, with this sincere look of shock and sadness, and he’s like, “What!?”
BL: For real?
FV: Yah, 
Adam Horovitz goes, “You wouldn’t do that me…  You’re not…  You’re going to have a baby?  What??”  And I could see the wheels turning in his brain.  Then he asks, “But Felicia, who is going to love and take care of us?”
  It made me smile and broke my heart in the same moment.  I was so excited I just found out I was pregnant, but at the same time, I don’t think I realized what a pivotal moment this would be in my life.  I loved these three gentlemen and my Beastie Boys/SAM family so much.   I had no idea my life had just completely changed and I was about to begin a completely new journey. It was a big day in my life
BL: I hear you
FV: After the baby was due, I was planning to go back to work with John Silva, you know, like many people do before they know what they are getting themselves into.  And he promised me he would build this area in my office, so I could have a nanny on one side, and work on the other.  That way, I could be with my child at work
BL: Sounds perfect
FV: It felt like it would be the best of both worlds, and I was so thrilled.  I thought, “I can do this.”  But then, I was working too hard, and I got put on bed rest.  I had a difficult labor
BL: Oh, I understand
FV: Then when my son, Jayden, was three months old, I was supposed to go back to work.  He hadn’t changed and he hadn’t made any progress at all.  My pediatrician was like, that’s not normal…  So, I talked with John.  And I told him that I was scared, and that I needed to take a hiatus.  And being the person that I was, a Mama Bear, I was 100% devoted to making sure Jayden was ok.  I noticed that he struggled with things where other kids didn’t struggle.  And he was having a hard time, and at two, my pediatrician told me to go to The Regional Center, in Los Angeles, where they help identify children with special needs or difficulties.  At that time, The Regional Center did a lot of evaluations.  And they told us that our son had autistic like qualities, but we would have to reassess when he turned three because they do not like to label before that
BL: Got it.  So, were the Boys supportive?
FV: Right about this time, they had come to LA to play at the Greek Theatre and I had gone out to see them.  This was one of the first times I had been out of the house at night since Jayden was born almost 2 years earlier.  
I remember talking to Adam Yauch and telling him about how I was struggling with missing all of them and wanting to come back, but that I was also trying to figure out how to help Jayden.  It still makes me cry today… Adam just told me: "Felicia, you are doing the right thing…  Be a mom and he’s going to be ok…  They are very broad with Autism diagnoses and many times it really turns out to be ok.  We are all ok…  Be a mom."  This is an example of just how amazing Adam Yauch was.
  When I was on bed rest, SO stressed because I was a workaholic and I couldn’t work, and Paul was working with Universal in Florida for a couple of months… Adam stopped by my house on his way to the airport and his message was very similar.  “We will be ok…  Take care of yourself…  Be a mom.”  Adam was always the greatest dad.  I remember being on tour and always seeing him and 
Dechen
 and 
Losel
 at a park near whatever hotel we were at that day.  I loved that about him…
BL: Ok.  So what did you do to help Jayden?
FV: I researched everything, and found all kinds of things you could do with your child—occupational therapy, speech therapy and 
floortime therapy
, so I was on the floor playing with him every day all day long.  It was an amazing and extremely scary time in my life.  Jayden really struggled socially, so during this time, I hosted playgroups at my house twice per week for 7-12 kids on the Spectrum and their moms (and sometimes dads and therapists).  All of our children went to a school in LA called 
Smart Start
 that used a Floortime Philosophy and they supported all of our children
BL: Nice... You created an opportunity for mutual aid
FV: Right.  It was an opportunity for these moms to feel like they had a safe place to go, they weren’t alone and we were all here together to support one another.  Also, the kids were able to just be themselves without anyone judging them.  It’s hard when you have a child with Autism or a child on the Spectrum and you are at the park trying to let them be themselves, and onlookers may not understand what is going on; they can be very judgemental.  Kids were just themselves, I always had tons of sensory things for them to get into, toys that were appropriate for each of them, and their favorite snacks.  There was on little girl I adored that it took a while just for her to get comfortable coming into my house… Eventually we learned she loved frozen pizza so I always had a frozen pizza waiting for her.  We would make that and then have vanilla ice cream.  This is what made her happy so this is what we were going to do.  It was a great experience and I learned a lot about being a mom.  I also learned a lot about families who are dealing with this sort of diagnosis.  And I developed a lot of empathy because I understood the emotions that they were experiencing
BL: Do you feel like your efforts made a difference?
FV: Absolutely, I am a HUGE believer in early intervention…  It can totally change a child’s life.  Children are so incredibly resilient.  It turns out Jayden had severe developmental delays.  He really has no idea that all the fun we had when he was a kid was therapy.  He just thought we loved him, played with him and we were always there for him.  We didn’t feel like it was important for him to feel or know he was “different” in any way as a child.  NO ONE is “normal” or “perfect”.  I am around 350 kids everyday and every single one of them are different and awesome, just as we all are
BL: Sounds so rewarding, to be giving back to others in the same spot you had been in.  Was it hard to stay away from the music business?
FV: I missed it immensely.  Diane (MMM’s wife) had asked me to start a 
DJ management company with her
.  So, for about a year I was going through all this with my kids and I worked most nights from 9pm until about 2am.  I was helping her build this company.  After a year of doing that and waking up at 5 am in the morning with my kids, who were going through a very difficult time at that point, I decided I still needed to be a fulltime mom.  But when the time is right, I will be back
BL: So by devoting yourself so completely to your kids, you were able to make sure they stayed on the right track
FV: Exactly
BL: And your son is doing better?
FV: Now he is doing amazingly well. He is funny, smart, kind and just all of the things that any parent could ever wish for.  That doesn’t mean that it hasn’t been a very difficult journey getting here…  I really learned a lot.  When I was looking for kindergartens in LA, I looked at public and private schools and charter schools.  I had just about given up and I was about to start my own private school for kids of all abilities, ethnicities, and all income levels.  Then I heard about a new Charter School they were about to open in Los Angeles, called 
Westside Innovative School House or WISH Charter
 and it was supposed to be a fully inclusive school.  Meaning ALL kids of ALL abilities are taught in a class together, building one community.  So, I rushed over and got my lottery application in 5 days before the deadline and we got in.  I was volunteering to help hire the founding teachers and was asked to be a Co-President of the parents association…  But then I heard they were also looking to hire an Office Manager.  I had never worked in a school before and honestly hated school…  But based on the new journey I was on, I wanted to not only become an advocate for children and education, but I wanted to be there EVERY DAY…  To help build the great school that I and millions of parents had dreamed of
BL: So, you were able to take your skills from the music industry and apply them to this school?
FV: Exactly.  It’s funny.  People laugh at what I do because now the kids call me Ms. Felicia and they have no idea where I came from.  With Lollapalooza and Grand Royal, we had many kids wanting to be successful, working hard to achieve their goals, and they were so dedicated.  It’s kind of the same thing.  They all need someone to believe in them.  Everyone needs someone to support them, to teach them.  And at the end of the day, it’s nice, because those kids love you back
BL: Gotcha.  Since you brought up Grand Royal, can you tell us about that?
FV: 
Grand Royal was this amazing new label that the Boys had.  It was in Atwater.  It was another huge part of my life, and I put a great deal of love into it.  It was one of the first independent artist labels and I got to see many amazing musicians come out of it.  There was a lot of passion and love, and lots of dynamic people involved
BL: I was a huge fan.  But it’s always been kind of a mystery to me, as to why it ended up closing.  Did you see the end coming?
FV: The industry started changing, with Napster getting huge around 1999-2000.  That completely changed everything for everyone.  Labels were downsizing, and buying other labels.  The industry was in turmoil.  Instead of embracing the new technology, the industry was trying to control it or move away from it.  John Silva and Gary Gersh started a new company called G.A.S. (Gersh and Silva) and since John managed Beastie Boys, and Gersh had been the President of Capitol Records, I’m assuming it made sense to bring Grand Royal along for this new partnership with Digital Entertainment Network.  It was kind of a joint kind of venture, I'm not exactly sure, but GR got folded into this digital entertainment network label.  I think there were two different philosophies at work at the new Grand Royal: an indie label philosophy and a big label philosophy.  Digital Entertainment Network was a brilliant idea.  It was going to be the on-line MTV of the time.  And there were some innovative, amazing people involved.  But I think the technology just wasn’t quite there yet
BL: I see
FV: Yeah, I think in the midst of all that downsizing and splitting, it just became a bad era for the music industry.  It was a shame, because not only was GR a label, but it felt like a family.  For anything related to the Boys, everyone was always so committed and it was always a big part of our lives.  It was hard when the decision came down to close GR.  The new authorities started letting everyone know on the day I was driving home to see my family in New Mexico.  I was on the road, and I started getting all these calls from the kids on the label, scared and not sure what to do.  So, after driving twelve hours to New Mexico, I got on the next flight headed back to LA, so that I could be here, and make sure that everyone was ok.  One way or another, we would work it out.  It was a tough time for everybody involved
BL: Was there any temptation on Mike’s part to put up a cash infusion, to try preventing Grand Royal from going under?
FV: 
I think Mike was very dedicated to Grand Royal.  He loved GR, and put his heart and soul, and lots of hard work into it.  And he probably did some of that…
  But it was just a crazy time in the world of record labels, around 2000.  There were so many people losing jobs.  There was a sense of, wow, we have to figure out what’s going on before we invest so much more in this.  We saw labels going under because people were suddenly choosing this new way of finding music online. I think we saw that it was time to take a step back and search for a new frontier or develop this into something else.  To start fresh, you know?  That might have been the thinking on Mike’s part
BL: Crazy how the industry got transformed so quickly.  What about the other members of the entourage?  I mean, early on you had certain individuals, like like Ricky Powell and 
The Captain
, Rest In Peace…
FV: Right, right
BL: I’ve always wondered…  Some of those attitudes toward women, drugs, and going buck wild weren’t exactly congruent with where the Boys were going.  Did that ever create tension?
FV: I wasn’t with them in their early years.  I met them during Lollapalooza in probably 1994.  Ricky was on the road with us that year. He was a lot of fun.  He was a nice guy.  The Boys would just find these people in their lives, 
like Ian Rogers
, or Mix Master, or 
Fredo
.  We talked about this when we had Yauch’s memorial in LA.  They would see something special in a person, and they would try to support them or encourage them.  And give them opportunities to be who they wanted to be.  In time, I think we all started growing up.  Mike was in LA and Adam and Adam were in New York, 
and I think that Ricky probably had other stuff going on.
  Everyone was starting to live grown-up lives, you know?  There was no real bad stuff.  You grow up with people and then they start living their own lives—and you’re still friends, absolutely, but they’re not going to take off to hit the road for 6-8 months anymore…  Many great people transitioned through—being part of their lives for a certain period
BL: Is that the way you would describe their relationship with Hurricane?  What can you tell us about that, when they went separate ways?
FV: That was something that they worked on.  Hurra and the Boys are still good friends I saw him at the memorial in New York.  I still see Hurra on Facebook all the time.  I wasn’t there, so I don’t know exactly what those conversations entailed.  But I can tell you that they absolutely love Hurra and Hurra loves them.  I don’t know what else to tell you except that everyone goes through transitions and ends up in a different place in their life
BL: Fair enough…
FV: Yeah, I don’t know exactly how to answer that question
BL: 
What about Mario Caldato?  Why didn’t he produce “To The Five Boroughs”?
FV: Again, I think those were conversations that John probably had with them.  I don’t know exactly.  They worked together a long time.  I think Mario was doing great stuff…  He’s done a bunch of great records, and he was out here in LA, but I don’t know why they chose…  why they made that decision.  But I can tell you that Mario is still a very important person in their lives, he had a huge impact on their music.  That will always be the way it is.  They still have a deep respect, compassion, and love for each other.  In LA, that’s where we went for part of Adam’s Memorial… we went to Mario’s new studio in Eagle Rock
BL: I understand.  Tell me, was it difficult trying to rein them in?  I mean, they had all this passion for all these different projects.  
At the drop of a hat, MCA would be going out to Utah, snowboarding
, and then it was the 
Tibetan Freedom Concerts
.  Was it hard getting them to focus?
FV: I met them while I was working on Lollapalooza, and I think that’s one of the qualities that drew me to them.  I’m always multitasking.  I loved and respected that not only were they the Beastie Boys, but they had a record label that they were trying to run.  I loved that they created Milarepa.  They used to be very involved in X-Large clothes.  For me, music is the way to reach out to the world, and communicate and come together. I loved how they found so many different ways to channel their creativity into so many different things.  Was it crazy sometimes?  Of course…
BL: Crazy how?
FV: 
I remember when I met Adam at Lollapalooza.  They were headliners, and Adam was like, I'm going to bring these Tibetan monks on tour.  And I was like, “What?”  The monks would come and do the blessing every day, and it was an amazing introduction to a different culture and other things that were happening in the world.
  And I think Lollapalooza was the way that our generation learned about politics, and becoming themselves, and that the world is a bigger place.  It was our way of growing up.  I spent so much time with the monks.  But I'm going to be honest.  At first, they didn’t particularly embrace the monks and they were a little bit disrespectful.  It was frustrating
BL: Who was disrespectful toward the monks?
FV: Well, see, this was when Adam was just starting with the monks.  And nobody really understood.  Adam hadn’t set up Milarepa yet, and nobody really knew about Tibet.  I mean, you have a bunch of 20 year-old kids that came to see rock and roll, and then you have these monks on the stage, chanting…
BL: I see
FV: Adam always inspired me—the way he would bring out issues and put them in front of people.  He didn’t preach.  He understood that you have to let people make their own choices.  I mean, the way he blended Milarepa with the Tibetan Freedom Festivals was just beautiful.  People came for the show, but he also inspired them to take notice of what was happening in Tibet, and to appreciate the lives that they were leading now
BL: To be present in the moment and to see how we are interconnected
FV: Exactly.  He showed people the importance of being grateful for their opportunities, while having empathy for the less fortunate.  People could relate to that and they'd say, Hey, let's show some compassion and together, let’s learn how to have respect for each other.  I loved Adam, because I saw how he was in a position, as a Beastie Boy, to make a difference for people through a cause he was so passionate about.  He was such a kind man.  He showed the world that so many of us are lucky, and that we need to be kind and appreciative, and compassionate every day
BL: How did they use that energy during 9/11?
FV: I remember them being so completely shaken and upset.  But they were like, Felicia, John, and 
JC
: 
“We want to do a benefit in New York, and we want it to be for New Yorkers Against Violence.  Because we feel like we have to stop for a minute, and figure out what is going on.  Violence is not the answer.”  And within a month, we were in New York, doing the benefit, supporting those peaceful causes so that people would keep an open mind.  And we had a lot of support.  U2 came out, REM came out, and a lot of artists came out to support the ideas that these brilliant people were sharing.  And it created a platform for these people to inspire each other to make change.
  I think that’s why I was so drawn to them, and really, it’s why I got into the music business.  I’m a kid from New Mexico that grew up in an impoverished and abusive home and became an emancipated minor when I was 16.   I remember that music was my focus and always gave me drive when I felt like I couldn’t take it anymore.  Anyhow, the B-Boys seized the moment when they had the opportunity.  They really did their best to make a change in the world.  And for me, that was worth working the 20-hour days for 15 years
BL: That’s a lot of hours
FV: Yes, again, I'm fortunate to have such an understanding husband
BL: Noted.  What about the future?  Do you see Adam Horovitz and Mike going on and recording as the Beastie Boys?
FV: That’s an interesting question…  I think the Beastie Boys will always be the three.  They grew up together.  They were brothers.  It would just never be the same.  It’s been about 8 or 9 years since I stopped spending every day of my life with them, so I could not say for sure, but in my heart, I would have to say no
BL: Do your children...  Does your family understand the role that you played?  That you were an integral part of the coolest band that ever was?
FV: I don’t know.  I don’t think so exactly.  I always try to put others first, and not focus on myself.  It goes back to remaining stealth.  I wanted to embrace my kids as being who they were, and not imposing too much of myself on them.  I think within the last couple of years or so, Jayden just turned 8 and Jake is 7.  It’s funny how they’ve evolved.  Now they are really starting to understand music.  I’ve wanted them to grow up being who they were meant to be.  Jayden is always dancing, and the way he moves his hands to express himself reminds me so much of the way Yauch used his hands.  Jake is my little breakdancer b-boy, and that’s just all coming from him.  Jayden…  He just loves to rock.  I’ve told them a little bit about it...
BL: What else are you up to?
FV: I have become a strong advocate for children and education.  I have been helping build our amazing school and community at WISH Charter and I’m learning so much every day.  I am so thankful WISH has become such an important part of our lives.  In our 3rd year were selected as one of six schools across the country (and the only charter school) by the University of Kansas to become a 
SWIFT Knowledge Development Site
 to help reform education across the country.  District Representatives, educators, administrators and community members come from all over the world to tour our school, to observe our community and see how exciting it is to develop an inclusive environment for all children.  We are continuing to develop our educational and community parterships, and our dream within the next couple of years, is 
to build our own site.  That way, we would have a permanent home and become a true Development Site for educators and administrators from all over the world.
  What we do is not magic, but is from the heart for ALL kids.  This school has not only changed my family, but also the lives of 400 other families.  I used to think the ONLY way I could change the world was through a HUGE festival to get people to listen.  But, now I see us changing lives everyday — one child at a time. But I am not giving up my dream of producing an event, festival or new digital idea — to bring my music and educational worlds together.  NO WAY.  I won’t give up.  I WILL make it happen
BL: 
I can see how your dream of improving education connects with the way the Beastie Boys advocated for social change.  I believe that Adrock’s sister is on the Board of the Ghetto Film School…
FV: Absolutely.  People can do amazing things, but sometimes everyone needs to be reminded that they have greatness inside them.  It’s important to have people in your life who have the ability to inspire others and remind them of their potential
BL: Word.  
Can you tell us what it is Ad-Rock is saying at the end of “Rhyme the Rhyme Well”?
FV: Haha, he’s saying, “You know I give a fuck ‘cause I'm Felicia.”
BL:  Ohhh.  How did that come about?
FV: They called me and said, “Hey…  We did something special for you.  Because we always like to hear your voice.  You’re always there for us.  And we just always wanted to do something nice for you.”  I think they were letting me know that they appreciated how much I cared.  And I think more than anything in the world, that was the greatest thing they could have said to me
BL: Anything else you’d like to say?
FV: My most powerful and amazing memories were from the side of the stage watching tens of thousands of fans in audiences all over the world singing along, bouncing, and just having the time of their lives.  My favorite concerts were the festivals… watching a sea of people moving and singing together so perfectly……THAT is what it is all about in the end.  THAT was truly magical and there is nothing like it in the world.  I remember feeling like my heart was going to explode because I was so proud of the three gentlemen on the stage and what they meant to their fans.  Thanks for letting me reminisce about a very special time in my life with some truly exceptional people…