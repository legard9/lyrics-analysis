MEREDITH: To make it... really make it as a surgeon, it takes major commitment. We have to be willing to pick up that scalpel that may or may not do more damage than good. It's all about being committed. Cause if we're not, we have no business picking up that scalpel in the first place.
----------
CHRISTINA: We'll book OR 2 tomorrow at 10. Usual scrub team. And get rid of his valve replacement at 2, that's no good.
INTERN: Yang. Brought you a mocha latte.
CHRISTINA: You're not scrubbing in.
INTERN: But Burke always lets me scrub it.
OTHER INTERN: Hey, Yang can I get in on...
CHRISTINA: No, no residents. Too crowded. Go away. (To a nurse) Make sure you put the instrument tray on his left hand side. He needs the elbow room. And OR 2, no gallery. Burke was specific.
NURSE: Dr. Burke has become very specific since coming back to work, hasn't he?
CHRISTINA: Do you want me to tell him you said that?
GEORGE (who is looking on from above with Meredith and Alex): Burke's back and suddenly Christina's running his board. Who does she think she is, Bailey?
MEREDITH: She's helping him.
ALEX: She's taking advantage. She gets out of rounds, she scrubs in on every surgery Burke does, she's ordered around 3'rd year residents...
MEREDITH: You're overreacting...
(Christina grabs the pen from the nurse's hand and starts writing on the board.)
GEORGE: She's writing on the board!
ALEX: Maybe I should sleep with Burke...
----------
MEREDITH: You've been busy
CHRISTINA: Yeah
MEREDITH: Derek hast called yet. I told him I broke up with Finn a week ago, he still hasn't called. Hey, do you wanna go get a drink, catch up?
CHRISTINA: No, I can't. I have to study up for Burke's surgeries tomorrow.
MEREDITH: She's busy.
----------
GEORGE: Callie. Hey, talk to me. And how about talking to me instead of ignoring me?
CALLIE: George!
GEORGE: Well that's talking That's a good start.
----------
(Derek walks out of the shower, and screams upon seeing a woman on his bed.)
NANCY: Well hey there stranger.
DEREK: Nancy you don't call first?
(Meredith enters.)
MEREDITH: Hey, I...
DEREK: Meredith... Meredith, let me explain!
(But of course she takes off.)
NANCY: I take it that was the slutty intern you cheated on Addison with?
DEREK: She's not slutty. What are you doing here?
NANCY: You bailed on Thanksgiving, then Christmas, you're living in a trailor, and you're getting a divorce,and then there's the slutty intern.
DEREK: I really don't like you.
NANCY: You love me.
----------
IZZIE: I'm feeling really good.
GEORGE: Good.
MEREDITH: Good.
IZZIE: First day back, it feels good. Big. Kind of nervous. I feel good. Do I look alright?
MEREDITH and GEORGE: Great.
IZZIE: I want to get on the right foot, get my ID renewed, new time card, sign some papers with legal then I'm back...really back.
MEREDITH and GEORGE: Yeah.
(Izzie leaves the elevator.)
IZZIE: This is me. Human resources.. See you guys on the floor.
BOTH: Yeah.
GEORGE: I'll be Christina for you if you'll be Izzie for me.
MEREDITH: Deal.
(George stops the elevator.)
MEREDITH: Derek had a woman in his trailer last night. She was ugly, very ugly. Except she was tall and beautiful...and he was naked.
GEORGE: Oh uh, McDreamy was doing the McNasty with a McHottie? That McBastard. How was that?
MEREDITH: That was good! Channelling Izzie... go.
GEORGE: OK... Callie... she won't forgive me, she won't talk to me. She dumped me...which I dont...care about...at all
MEREDITH: Good because you deserve better, 'cause you are George. I mean seriously. Seriously. Was that Izzie optimistic?
GEORGE: That was very Izzie...
MEREDITH: What has happened to us?
GEORGE: We are now the people the people we want to be with avoid.
MEREDITH: We have careers to think about. We don't need attention from men with perfect hair.
GEORGE: We should make a pact. No more dating.
MEREDITH: No more Derek. No more Callie.
GEORGE: Just 100% focused on our work.
MEREDITH: They are dead to us.
GEORGE: They are freaking corpses to us.
(They butt knuckles.)
----------
MRS. BYRD: Preston Burke, what a pleasure. I have read so much about you.
BURKE: Well thank you.
CHRISTINA: Pruitt Byrd. Medivac brought him in this morning. He presents with a primary cardiac tumor.
MRS. BYRD: We've been told Pruitt needs a cardio autotransplantation.
BURKE: A cardio autotransplantation?
CHRISTINA: A humpty dumpty surgery.
BURKE: Yes I know what it is. Mrs. Byrd, this is a very complicated surgery. You'll need thorough testing.
CHRISTINA: Already done.
BURKE: You understand what the surgery intails? We have to..
CHRISTINA: We have to remove his heart, cut it open, scrape out all the tumor, then put his heart back together again.
BURKE: Which is risky.
CHRISTINA: But possible.
MRS. BYRD: Which is why we're here I want the very best for my husband.
BURKE: The best.
CHRISTINA: She means you.
BURKE: I know that.
CHRISTINA: Oh I scheduled Mr. Byrd for surgery this afternoon. Just trying to be as efficient as possible.
----------
CHIEF: Legal cleared Stephens. Explain her perameters and make she understands.
BAILEY: Stephens is being assigned to me again?
CHIEF: She's your intern.
BAILEY: Well wouldn't it be better for her to start fresh with another resident?
CHIEF: You don't want her.
BAILEY: I want her to succeed.
CHIEF: There's still a good deal of fallout after the Duquet M&M. There's talk. I don't need to tell you that there are doctors in this hospital who have concerns about your judgement.
BAILEY: Concerns about my judgment.
CHIEF: Stephens was your mistake.
BAILEY: Do you, sir, do you have concerns about my judgement?
CHIEF: Do you? You're not going to put those concerns to rest by avoiding taking responsibility for Stephens. She's your intern... again.
----------
GEORGE: Transfer from Mercy West. Noelle LeBatt. 32 weeks along, pregnant with twins. One appears to be developing more slowly than the other.
GREG: Hi.
ADDISON: Hi there.
GREG: Greg Stanton. This is my lovely fiancé. The future Mrs. Stanton. Noelle.
NOELLE: Greg, I promise you you're the only person who cares if we're married or not.
GREG: They said we had to come here because her condition could be serious. Is it serious?
ADDISON: I gotta say, you don't seem emergent enough for Mercy to send you over here in a ambulance. You're not in labor, you're not bleeding, your vitals are fine.
NOELLE: They said I had to come here because of my thing.
GEORGE: Two uteruses!? Ms. Lebatt was born with two uteruses. Uh, Uteri
ADDISON: Uteruses.
GEORGE: Uteruses. That's very rare.
IZZIE: I'm here. Hi. Hello. That paperwork took forever, but I'm here now. And can I just say how greatful I am to be back?
BAILEY: You understand there are rules to your probation. Rules to follow?
IZZIE: Oh yeah, legal took me through all that stuff. The mandatory counseling, the extra lab hours. The extra work with the transplant patients.
BAILEY: That's the stuff you're doing with the chief. I'm talking about what you're doing for me.
IZZIE: Okay. What am I doing for you?
BAILEY: OK, lets start with what you don't get to do. You will not interact with any patients, you will not be alone with any patients. You will be seen and not heard. No procedures. The OR is off limits. No pre-op. No post-op. Anything having to do with an op. You have no authority. You have no opinions. You have no choice in this matter. Am I understood?
IZZIE: Is there anything I can do? I want to be useful.
BAILEY: I cant use you. You've got to earn back the right for any of us to trust you again. Until then, you will be shadowing a differend Dr. every day.
IZZIE: Okay, who am I with today? Dr. Montgomery? You?
MEREDITH: Dr. Bailey, you paged?
BAILEY: Dr. Grey.
IZZIE: Meredith...
BAILEY: You are to make sure that Stephens observes only.
MEREDITH: You want me to?
IZZIE: Wait Meredith? Meredith is the boss of me.
----------
MEREDITH: I'm sorry about this.
IZZIE: Hey you want me to get you some coffee? Rub your feet maybe?
MEREDITH: I did not ask for this assignment.
IZZIE: It's fine...I'm fine with it. Oh, I'm so sorry (She's bumped into people who are talking to Derek.)
DEREK: Dr. Stephens. Welcome back. Meredith? Meredith...
IZZIE: Sorry it's not like I can leave. I gotta stay by my bosses side.
NANCY: Okay, the trailer sucks, but in the light of day the land is nice. Seattle's pretty in the daylight. Plus you have your thing for ferries.
MEREDITH and DEREK: Ferry boats.
NANCY: Right, whatever.
DEREK: Meredith, this is my sister Nancy.
MEREDITH: Sister? You're one of Derek's sisters?
NANCY: Well I knew you didn't think I was the wife, seeing as how you already ran her off.
DEREK: Nancy is visiting from Connecticut. She's on her way home... now. Straight back home.
MEREDITH: Well it was nice to meet you.
NANCY: Okay...
(Meredith and Izzie walk off as Derek grabs Nancy's arm scornfully.)
IZZIE: McDreamy's sister is McBitchy...
----------
CHRISTINA: You put me in charge of your schedule.
BURKE: To make sure that I didn't get a surgery like this one.
CHRISTINA: Only a handful of people do humpties Burke. And you're the best one on the west coast.
BURKE: Then he can go to Houston.
CHRISTINA: You want to tell the Chief that? Pruitt needs the surgery, he'll die without it.
BURKE: You didn't tell me. You told the patient we were doing the surgery. but you didn't tell me.
CHRISTINA: I have been doing that all week.
BURKE: Are you up on a humpty? Do you realize what this entails?
CHRISTINA: I have done my research. Stop worrying. We are a well-oiled machine.
----------
BAILEY: Uh Dr. Burke.
BURKE: Yeah.
BAILEY: I understand there are some doctors in this hospital that have doubts about my judgement since Denny
BURKE: Everyone loves a scandal.
BAILEY: So you think it's just gossip? It's not actual concern about me being a doctor?
BURKE: Sure, sure.
----------
(Meredith pulls gauze off a young girl's burnt hand.)
MEREDITH: Ow. Iz, set me up for debriedment, and dressing.
GRETCHEN: It's stupid. I know better. When I'm setting up for the bar... I'm a mess. I can't focus on anything for weeks, but tourts and real property and constitutional law.
MEREDITH: Uh the Biosynthetic ones.
IZZIE: We never use those.
MEREDITH: We do now. Sloan changed the burn protocol last week.
GRETCHEN: I was about to start a practice test. I wanted something to drink. So I put on some tea. So I put on a pot of water and forgot. Half an hour into my section on contracts and the smoke alarm was blaring...and well you know the rest.
MEREDITH: So you're going to be a lawyer?
GRETCHEN: All I have to do is pass the bar exam. I failed last time...but this time I'm ready. I'm going to pass.
IZZIE: Good for you.
----------
DEREK: That was just mean.
NANCY: Kathleen called and asked me to find out why the slutty interns panties were hanging on the bulletin board.
DEREK: Four sisters... four sisters and not one brother. And you wonder why I don't call more.
NANCY: You can answer about the panties at any time.
DEREK: Nance, it's good to see you. Really great to have you here. But I have a job you know? I have patients.
NANCY: I have a mother with twins at 35 weeks...where is her OB?
DEREK: Well her OB should be on a plane back to Connecticut where she belongs.
NANCY: Okay, so we've covered the trailer, we still have to cover the slutty intern and the divorce.
DEREK: You know what? You sound more and more like mom every day.
NANCY: Take it back.
----------
GEORGE: So two uteruses. That's pretty cool. And you're engaged...also cool.
NOELLE: Greg talked me into it
GREG: From the first moment I saw noel I knew I was ham.
GEORGE: Did you say ham?
NOELLE: Oh here we go again.
GREG: You're either ham or eggs. You gotta ask yourself in every situation, are you the pig of chicken?
GEORGE: Sir it's just a pig or chicken.
GREG: Look you got to play the ham and the eggs. You see the chicken is involved in the meal. But the pig is committed. So the question is...are you committed? Or are you involved?
GEORGE: Ham or eggs?
GREG: Ham or eggs.
----------
MARK: Karev you free?
ALEX: Absolutely sir.
MARK: Great. Take this. I'm on hold with the DMV. Some mess up about switching my license and registration to Seattle. Take care of this for me?
ALEX: Thanks for thinking of me sir. O'Malley...how's it feel to be the new gynie grunt?
GEORGE: I've got a patient who was born with and is pregnant in two uteruses. Two uteruses...jealous?
ALEX: No, I'm on hold. Important business. For Sloan.
GEORGE: Right... Super important, I'm sure. Noelle LeBatt...room 2413.
ALEX: Hang on, can I see that? (referring to the chart)
(Nancy walks up.)
NANCY: Did I just hear him say two uteruses?
ALEX: You must be...
MARK: Nancy pants?
NANCY: Hey loser.
MARK: I wish Derek had told me you were visiting.
NANCY: Oh like he tells you anything these days?
MARK: Well I'm working on that.
NANCY: What are you doing here Mark? Are you trying to torture him?
MARK: He's my family Nancy. Plus I needed a change of pace. Plus I slept with my tennis partners wief and he went out and bought a gun.
NANCY: There it is!
ADDISON: Oh! Nancy! Let me guess, did mom send you out?
NANCY: Let me guess, he's trying to ban you from Seattle?
ADDISON: Did he also tell you he's living in a trailer?
(Derek walks up and sees them talking, and walks away.)
NANCY: Derek. Derek? (She goes after him.) I want to see the two uteruses.
ADDISON: Yeah, find me later.
MARK: I miss her.
ADDISON: Yeah me too.
----------
GREG: The Diesel pushes past quarterback Don McNeil. He's at the 30. He's at the 20. He's at the 10. Touchdown Washington!
NOELLE: Sorry, one of the twins gets rowdy, and Greg gets carried away with talking to the baby. He's decided that the baby is into sports of all things.
GREG: Well she's daddy's little girl. See when I talk to her...
ALEX: Yeah, your voice, it helps calm her down. Yeah, I read a couple aricles on that recently.
ADDISON: Dr. Karev...I didn't know you secretly missed my service.
ALEX: Right.
ADDISON: Greg, Noelle, I have good news for you. According to our tests, the babies are both perfectly healthy. But they are substantially different sizes for a very unique reason. The babies have two different due dates.
GEORGE: According to our calculations, the larger baby, your son was conceived a whole six weeks before your daughter.
NOELLE: Oh God.
GREG: I don't understand. That's impossible.
ADDISON: Actually out tests are extremely accurate at predicting due dates.
GREG: But we broke up. Yeah, see she got pregnant. I wanted to get married, she didn't. I gave her an ultimatum, and she left me.
NOELLE: Greg I am so sorry.
GREG: We were barely speaking, six weeks after the conception, let alone having s*x. Or at least I wasn't having s*x.
NOELLE: It was one night, Greg, it was one night. I didn't think this could happen. It was nothing.
GREG: I can't believe you did this. I can't believe you didn't tell me. I can't believe this.
(He storms out.)
GEORGE: He's not the father of both babies.
----------
ADDISON: I just accidentally broke the news of my patient's infidelity to her husband.
BAILEY: And yet no one is questioning your competence as a surgeon.
ADDISON: No, what?
CHIEF: I need a button. I'm on my last clean shirt and I'm missing a button. Adele always handled my buttons
CHIEF: Adele...no. I wouldn't want to bother her with anything as trivial as a button. I don't suppose either of you would want to...
ADDISON: I'm sorry, I have two uteruses I need to deal with.
BAILEY: I have many skills. Surgical skills. Your button ruptures an oesophagus, I'm your woman... otherwise...
MARK: I hear you burned your hand because you were studying?
GRETCHEN: Can't be allowed to warm soup within five days of taking the bar.
MARK: So you burned your hand...
GRETCHEN: While I was burning the soup.
MARK: You grabbed the pot
GRETCHEN: Dropped it, obviously it was red hot...
MARK: Must have held onto it for a while though. This burn's extremely deep.
GRETCHEN: Oh yeah, that's right. I held onto it then I dropped it.
MARK: There are some deep partial thickness burns here. Give her a gram of Cephazol and a tetanus.
GRETCHEN: It's too bad really. I did this to my hand. I'm supposed to be taking the bar again on Friday.
MARK: This burn is bad, but we'll get you fixed up. You'll be fine to take the test on Friday.
GRETCHEN: I will?
IZZIE: She's lying. Something's off about Gretchen's story.
MEREDITH: Izzie, you're only supposed to be observing.
IZZIE: I am observing. I'm observing closely. Did you see her reaction when Sloan told her she'd be okay to take the test again. That was not relief. That was panic.
MEREDITH: It's okay to be nervous...about being back here at work, about failing. But you haven't been here a whole day yet and already you're pushing to bend the rules.
IZZIE: MEREDITH: This is so not about me. This is about our patient. I'm worried about our patient. And get off your high freaking horse.
MEREDITH: This is my patient. Try to remember that.
IZZIE: Fine, whatever.
BAILEY: A humpty dumpty surgery. Burke's doing a humpty today? I've never seen one of those first hand before.
CHRISTINA: Yeah, me neither. What are you doing?
BAILEY: Scrubbing in. You'll let Burke know?
CHRISTINA: Sure. (She doesn't want Bailey in there because sit would give the game away.)
----------
GEORGE: I know you're not talking to me any more, but there's something I'd like to explain. All you have to do is listen. You and me, we're like ham and eggs. I was the chicken and I just want you to know that I know I was the chicken. You put yourself out there and you were committed. I just put the eggs on the plate. Not the ham 'cause you were the pig. I was involved but now I'm committed.
CALLIE: Did you just call me a pig?
GEORGE: It's a metaphor.
CALLIE: Calling me a pig.
GEORGE: The point is you're not the pig any more. I am the pig. Now...I am the pig. (She walks away.)
(Izzie walks past a room where the gang is sitting eating their lunch.)
IZZIE: I've been waiting for you guys for half an hour in the cafeteria. Since when do we eat in a patient room?
GEORGE: Izzie, meet really old guy. We found him a couple of days ago. He sleeps all the time.
CHRISTINA: It's quiet. No one bothers us. What are you doing.
IZZIE: I'm not doing anything. Turns out I'm literally not allowed to do anything. I knew coming back would be an adjustment. But this...
CHRISTINA: No, what are you doing now?
IZZIE: Eating my lunch.
CHRISTINA: Okay, if you want to socialize, or talk about your day or get in a quick therapy session, go do it over there.
IZZIE: Seriously?
CHRISTINA: Goodbye Izzie.
IZZIE: Okay...
ALEX: So O'Malley, your two uteruses...
GEORGE: You're on scut. Glorified plastics scut. You're in no position to mock me or my uteruses.
ALEX: Any cervical changes on the ultrasound?
GEORGE: Why do you care?
ALEX: I don't.
IZZIE: I am so glad to be back. Yay! I'm not complaining about you...you and I are fine, boss.
(The old man groans in his bed and moves slightly and everyone looks around at one another.)
GEORGE: I called Callie a pig.
ALEX: To her face?
MEREDITH: What happened to the whole corpse thing?
IZZIE: I can't get over how much everything around here has changed.
MEREDITH: How many times?
GEORGE: Just one time.
MEREDITH: I get one too then, Gin
(They're playing cards. Meredith gets up and leaves.)
IZZIE: What was that... with Meredith?
GEORGE: We have a thing.
IZZIE: You have a thing with Meredith and I don't now about it? I live with you, I see you every day.
GEORGE: It's work thing.
CHRISTINA: You know the point of hanging out with really old guy is that he doesn't talk. The man is sedated. This is supposed to be a quiet place. (She storms out.)
IZZIE: See...change. She's changed. Everything's change.
----------
MRS. BYRD: I gave the other nurse a list of his medications this morning. My husband needs his pills, he needed them hours ago!
CHRISTINA: Okay, why don't you go check on his meds?
NURSE: I have checked on them.
CHRISTINA: Meds. Pharmacy. Now. Just...
MRS. BYRD: I'm sorry. I'm sorry.
CHRISTINA: It's fine.
MRS. BYRD: I was never a yeller before. I've become this horrible person who yells at people who are just trying to do their job.
CHRISTINA: You are just looking out for your husband. You do exactly what it takes to protect him. That does not make you horrible. That makes you smart.
----------
(Christina erases Bailey's name off the board when no one is looking.)
----------
GEORGE: She's hyperventilating. And the baby's had some brachicardia.
ADDISON: Noelle I need you to get back into bed.
NOELLE: Is Greg out there? Did he leave? Like really leave?
GEORGE: Noelle, take deep breaths.
NOELLE: I always give him such a hard time about everything. I mean, he likes weddings, I hate them He loves kids, they scare me to death.
ADDISON: Have you had any cramping, any contractions, anything at all?
NOELLE: I can't do this without him. We'll get past this right. I mean, we'll get past this. AH!
ADDISON: All right, Noelle, I need you to get into bed.
NOELLE: What's happening?
ADDISON: Stay on your left side. I need slow deep breaths.
----------
NANCY (at dinner with Derek): So tell me about the slutty girl. Fine, the slutty intern.
DEREK: It's the slutty part I had a problem with.
NANCY: Kathleen said she's not even single.
DEREK: She is single. She's wonderful, she's smart, she's alot of things, but she's none of your business Nancy.
NANCY: Wow...I've never seen you like this over a girl. Not even Addison.
DEREK: I've never been like this over a girl, especially Addison.
NANCY: Oh don't be bitter. I mean shame on Addison and all that but he's Mark, Derek. What do you expect? He's Mark. And who hasn't gone there once or twice, right?
DEREK: What'd you say?
NANCY: Oh come on, everyone sleeps with Mark, it's practically a right of passage. Oh I get it. I get that they made a mistake.
DEREK: It wasn't a mistake. It was months. They were together for months, did you know that?
NANCY: No, I had no idea.
DEREK: Then shut up about it and eat your lunch.
----------
MEREDITH: Derek's sister is in town. Christina, are you mad at me or something, 'cause I've been trying to talk to you...
CHRISTINA: Not everything is about you, Meredith.
MEREDITH: What's going on? 'Cause George and Alex are saying...
CHRISTINA: I...don't care
MEREDITH: What is your problem?
CHRISTINA: I have bigger things in my life right now.
(Bailey notices her name missing from the humpty dumpty.)
BURKE: What?
CHRISTINA: Nothing.
BURKE: 10 blade.
GREG: Dr. O'Malley? Can you tell me what's going on in there?
GEORGE: You left... she thinks you left!
GREG: Look man, this is all about to digest. My little girl... she isn't even my little girl any more.
GEORGE: Noelle is in there freaking out because she thinks you left.
GREG: Just tell me if she is okay... please.
GEORGE: She's gone into premature labor. Dr. Montgomery... is about to do an emergency c-section to stop labor on the other.
GREG: So you're delivering one of the babies right now? Today?
GEORGE: In just a few minutes.
GREG: The boy or the girl?
GEORGE: Ham or eggs?
----------
MARK: You say you want a career in plastics, but you can't tear yourself away from the baby catchers long enough to show me you want it.
ALEX: Being on hold with the DMV doesn't have anything to do with a career in plastics Dr. Sloan
MARK: Only it does. Cause I have everything to do with your career in plastics, and I have everything to do with your career in plastics.
NURSE: Tyler, Addison Montgomery needs two units of B-Positive blood in L&D STAT.
(Alex hangs up with the DMV and goes to help)
----------
MEREDITH: So the biosynthetic dressings should ensure that you won't need skin grafts. And that's good.
GRETCHEN: Would that take long? I mean, how long would something like that take? If I had needed them?
MEREDITH: Oh that would depend on the severity of the burn.
GRETCHEN: So you mean more severe than this one? It would have to be more severe.
MEREDITH: Just to make sure that I have everything accurate, you burned your hand...
GRETCHEN: We've been over this.
MEREDITH: I'm sorry..
GRETCHEN: Look I have a test Friday and I have work to do. I want to get out of here. I'm ready to go home.
IZZIE: Gretchen did you burn your hand on purpose?
MEREDITH: Izzie you heard what Dr. Bailey said.
IZZIE: It's okay if you did. It's just... did you... burn your hand to get out of taking your test?
GRETCHEN: I can't fail that damn test one more time. I just can't. It's all anyone if my family...anyone in my life talks about. It's all I'm known for. Oh Gretchen the failure. Can you imagine failing the bar exam five times? Five times! I mean that's absurd. It's just...that's pathetic. I cannot sit for 2 1/2 solid days of testing...again...just to prove to everyone again...how pathetic I am. Now...when.
IZZIE: You feel that pathetic all by yourself.
----------
ADDISON: Okay, I've completed the incision. I'm going to ask you to move the top uterus to the side so I can reach the one underneath.
GEORGE: Okay.
ADDISON: I don't want to alarm you or make you nervous in any way, 'cause you seem like a decent person, O'Malley, but I've got about 120 seconds to get baby 1 out of uterus 1 while you're holding uterus 2. And if you so much as hiccup you could rupture uterus 2 and kill this woman's child. Just try and be careful okay?
GEORGE: Okay.
NURSE: Wow. Look at that.
ADDISON: Amazing huh?
CHRISTINA: Applying the final hemastasis suture.
BURKE: Don't be afraid to.
CHRISTINA: Grab a bigger piece? Got it.
BURKE: Very nice Dr. Yang.
CHRISTINA: Applying pressure.
BURKE: Gently.
(They pull a mass out of the guy's heart.)
CHRISTINA: It was attached at the intraventricular septum. are you going to use a graft to repair?
BURKE: Someone's been doing their homework.
CHRISTINA: 4-0 Prolene please. Thank you very much Boki.
ADDISON: Ready to perforate uterus 2.
GEORGE: Wait, Dr. Montgomery, my baby's moving. It's really moving, I can't hold it.
ADDISON: I need you to hold her still if I'm going to do the c-section.
GEORGE: I know. I'm trying. What should I do? It's really moving.
ADDISON: I need you to keep her still George.
NURSE: You're sending her into distress. You have to get that baby to stop moving.
GEORGE: How do I do that?
ALEX: Talk O'Malley.
(Alex moves towards the table.)
GEORGE: Alex...
ADDISON: Dr. Karev move away from the table.
ALEX: Talk to it, to the baby to calm it down.
ADDISON: Karev.
GEORGE: Talk about what?
ALEX: October 30, 1974, it's the fight known as Rumble in the Jungle. World heavyweight champ George Foreman is pitted against Muhammed Ali. It's his first fight in 3 1/2 years out of the ring.
GEORGE: It's working
ALEX: Foreman is his favorite to win. He's younger, stronger.
ADDISON: Scalpel.
ALEX: But he's not prepared for what Ali later called the rope-a-dope. It all starts in the second round. He comes out swinging. Ali's backed up against the ropes.
----------
DEREK: That's not going to stay on. Giving it too much slack
CHIEF: You're blocking my light.
(He's trying to sew a button onto his shirt.)
DEREK: Think of it as a basic corner stitch.
CHIEF: I can figure out how to sew on my own buttons, thank you. I am a surgeon.
DEREK: Right.
CHIEF: Oh for God's sakes. You sew this on for me, I'll get rid of Addison and Sloan.
DEREK: Really?
CHIEF: No.
DEREK: Well, I'll do it anyway.
CHIEF: So I heard you've got a sister wandering the halls She planning on moving in too?
DEREK: I hope not.
CHIEF: Derek I know it's been hard for you.
DEREK: He was like my brother. I have four sisters. Four very annoying sisters. Mark was my brother. It's hard.
CHIEF: Divorce isn't all it's cracked up to be is it?
DEREK: I just want it to be easy. Move on.
CHIEF: But you're in a surprising amount of pain.
DEREK: You and Adele?
CHIEF: I'm sewing on a button for the first time in my life. What does that tell you?
DEREK: Technically, I'm sewing. I'm just saying.
----------
ALEX: Dr. Sloan
MARK: Just so we're clear, you knew when you stepped into that ER that you were forfeiting your career in plastics, right?
ALEX: But, Dr. Sloan...
MARK: I need my phone back.
----------
BURKE: Well done. You were very prepared.
CHRISTINA: Yes, I'm always prepared.
BURKE: I couldn't have done it without you.
CHRISTINA: Thank you.
BAILEY: Dr. Burke, could we have a moment alone? I just I didn't realize that you were one of them. One of the doctors who have doubts about my abilities.
BURKE: Miranda, I'm not.
BAILEY: My name was erased from the board. I have to assume that was you.
BURKE: Dr. Bailey.
BAILEY: I just...I just need to know why. I need you to tell me why you didn't want me in on your surgery.
BURKE: I'm afraid I just couldn't use you.
BAILEY: I understand.
----------
NANCY: You should have seen the two uteruses. Unbelievable. And a cute baby to boot.
DEREK: I'm glad you're enjoying your trip.
NANCY: Oh Derek, I'm going. I'm on a plane back in 2 hours.
DEREK: So, you're going to report back to mom that...
NANCY: That you're you. Still running circles around all the women in your life. But that's to be expected with 4 sisters and a dead dad.
DEREK: I'm not running circles.
NANCY: Can you even remember the last time you were alone? You've never been single. I mean you're fine, but you're not happy. And you're not going to get happy until you get some space. You just need to get away. Away from Addie, away from the intern, just away. Think about what you want.
DEREK: Kathleen's the shrink Nancy, not you.
NANCY: I gotta go.
DEREK: Nancy, thanks for flying out here. It was... thanks
----------
GRETCHEN: Where are we going again.
MEREDITH: We need to go upstairs.
GRETCHEN: OK. Wait. Wait. Where are we?
MEREDITH: We're on the psychiatric floor Gretchen. We need to place you on a 72 hour hold.
GRETCHEN: No I'm not crazy! I'm going home
MEREDITH: Gretchen.
GRETCHEN: No I just need to go home, Please?
IZZIE: You know you're just going to hurt yourself again. Isn't that your plan? So that you won't have to take the bar exam? You need some help Gretchen. If the idea of taking an exam makes you hold the palm of your hand to a burner... you need some help. Everyone needs help from time to time. To make sure they're okay, they're ready. I have that. And you need that right now.
(She consents.)
GRETCHEN: I'm not crazy.
IZZIE: I know.
GRETCHEN: Just didn't want to fail.
IZZIE: I know.
----------
NOELLE: What happened? Did everything go okay? Are my babies okay?
ADDISON: The surgery went very well. You have a healthy baby boy. And the labor stopped on our little girl. She's going to be just fine.
NOELLE: And Greg? Has Greg come back yet?
ADDISON: No, Noelle. I'm sorry. Greg isn't here.
(She passes back out.)
ADDISON: I want an update every half hour.
NOELLE: What happened? My babies?
ADDISON: Are just fine Noelle. You're just coming out of anaesthesia.
NOELLE: Is Greg back yet.
ADDISON: I'm sorry noel he's not here. (She slips away again.) (To George) And be sure to alert me to any fetal distress.
NOELLE: What happened?
ADDISON: Your babies are fine Noelle, everything is okay.
NOELLE: Is Greg back yet?
GREG (at the door): I'm right here.
NOELLE: Hi.
GREG: Hi. I saw our son. He's amazing. But how's our little girl?
----------
MARK: You look like you could use a little cheering up.
CALLIE: Not from you.
MARK: If I recall I was pretty good at cheering you up. One... two... yep three cheerful times.
CALLIE: Dirty. It was not cheerful, it was dirty. And like I said, no.
(Alex is listening in.)
----------
MEREDITH: So your sister really doesn't like me.
DEREK: Sorry. It's just... she's from the East Coast.
MEREDITH: Well I...
DEREK: I should have called.
MEREDITH: But you didn't.
DEREK: I want us to work it's just...It's complicated. I think I need a little time to..
MEREDITH: Take some space.
DEREK: Yeah. To clear my head.
MEREDITH: Yeah. Okay. Okay.
DEREK: Okay.
MEREDITH: (VO) There are times when even the best of us have trouble with commitment, and we may be surprised at the commitments we're willing to let slip out of our grasp. Commitments are complicated. We may surprise ourselves by the commitments we're willing to make. True commitment, takes effort, and sacrifice. Which is why sometimes, we have to learn the hard way, to choose our commitments very carefully.