[Intro: Yoo Young Jin & D.O.]
Oh yeah, don’t leave me baby
Just turn around
Play another slow jam
(Play another slow jam)
This is my property
Tell me, what is love?

[Verse 1: D.O.]
나를 사랑하지 않는 너를 잊은 채
하고 싶었던 모든걸 하고 살아도
머릿속에 넌 절대 지워지지가 않아
도대체

[Verse 2: Yoo Young Jin]
니까짓게 뭔데 날 비참하게 만들어?
욕이라도 해줬으면 속이 후련할 텐데
왜? 어떤말조차 넌 없이
내 곁을 떠나가 버렸니?

[Verse 3: D.O.]
헤어질 준비가 안됐어 Wait a minute
기다림은 너무 길은데 Has no limit
대답 없는 널 자꾸 불러도
메아리만 돌아와

[Chorus: Yoo Young Jin & D.O.]
너를 잃고도
살아가는 내가 너무 싫지만
언젠가는… 이라고 바랜다
Oh, tell me, what is love?
Tell me, what is love?
내 눈을 보며 말했던
두 글자가 낙인처럼 아파 와
내 맘이 그걸 알게 됐는데
Oh, tell me, what is love?
Tell me, what is love?

[Verse 4: Yoo Young Jin & D.O.]
Hey, baby please stop
돌아서던 발걸음을 멈춰줘
이제라도 제발… No, no, no
거짓말도 상관없어
난 이유라도 알고 싶은걸

[Verse 5: D.O. & Yoo Young Jin]
(왜 넌 날 자꾸)
밀어내야만 했나?
(그럴수록 다가선 내가)
이기적인 마음이었나?
이런 생각들로 도망치지도 못해
이딴게 정말 사랑인게 맞을까?

[Verse 6: Yoo Young Jin & D.O.]
헤어질 준비가 안됐어 Wait a minute
(Wait a minute for me, baby)
기다림은 너무 길은데 Has no limit
(Has no limit)
대답없는 널 자꾸 불러도 (불러도)
메아리만 돌아와

[Chorus: Yoo Young Jin & D.O.]
너를 잃고도 (잃고도)
살아가는 내가 너무 싫지만
언젠가는… 이라고 바랜다 (널 바래본다)
Oh, tell me, what is love?
Tell me, what is love?
내 눈을 보며 말했던
두 글자가 낙인처럼 아파 와
내 맘이 그걸 알게 됐는데
Oh, tell me, what is love?
Tell me, what is love?

[Verse 7]
긴 시간들이 지나도
그 자리에 난 멈춰있어
행복했던 순간이
사진처럼 날 멈춰버렸어 yeah

[Verse 8]
묻지만, 사람들은 괜찮냐고 묻지만 (괜찮냐고 묻지만)
조금씩 부서져 (Yeah)
베인 맘이 아픈걸
이렇게 So bad
내게서 멀리 가지마 Oh- yeah
No baby you oh no no no no no
I’m not a kind of steel yeah (yeah)

[Chorus: Yoo Young Jin & D.O.]
너를 잃고도
살아가는 내가 너무 싫지만
언젠가는… 이라고 바랜다
(언젠간 내 곁에)
Oh, tell me, what is love?
Tell me, what is love?
(Tell me, what is love?)
내 눈을 보며 말했던
(Come back to me, baby)
두 글자가 낙인처럼 아파 와
내 맘이 그걸 알게 됐는데…
Oh, tell me, what is love?
Tell me, what is love?
(Tell me, what is love?)

Romanization

Oh yeah, don’t leave me baby
Just turn around
Yeah- Play another slow jam
(Oh- Play another slow jam)
This is my property (Oh-)
Tell me, what is love?

Nareul saranghaji anneun neoreul ijeun chae
Hago sipeotdeon modeungeol hago sarado
Meorissoge neon jeoldae jiwojijiga anha
Dodaeche

Nikkajitge mwonde nal bichamhage mandeureo?
Yogirado haejwosseumyeon sogi huryeonhal tende
Wae? eotteonmaljocha neon eopsi
Nae gyeoteul tteonaga beoryeonni?

Heeojil junbiga andwaesseo, Wait a minute
Gidarimeun neomu gireunde Has no limit
Daedap eomneun neol jakku bulleodo
Meariman dorawa… (Oh-)

Neoreul ilkodo
Saraganeun naega neomu silchiman
Eonjenganeun… irago baraenda
Oh, tell me, what is love?
Tell me, what is love?
Nae nuneul bomyeo malhaetdeon
Du geuljaga nagincheoreom apa wa
Nae mami geugeol alge dwaenneunde
Oh, tell me, what is love?
Tell me, what is love?

Hey, baby please stop!
Doraseodeon balgeoreumeul meomchwojwo
Ijerado jebal… No no no
Geojitmaldo sanggwaneopseo
Nan iyurado algo sipeungeol

(wae neon nal jakku)
Mireonaeyaman haenna?
(geureolsurok dagaseon naega)
Igijeogin maeumieonna?
Ireon saenggakdeullo domangchijido mothae
Ittange jeongmal saranginge majeulkka?

Heeojil junbiga andwaesseo, Wait a minute
(Wait a minute for me baby)
Gidarimeun neomu gireunde Has no limit
(Has no limit)
Daedapeomneun neol jakku bulleodo (bulleodo)
Meariman dorawa… (Oh-)

Neoreul ilkodo (ilkodo)
Saraganeun naega neomu silchiman
Eonjenganeun… irago baraenda (neol baraebonda)
Oh, tell me, what is love?
Tell me, what is love?
Nae nuneul bomyeo malhaetdeon
Du geuljaga nagincheoreom apa wa (Yeah)
Nae mami geugeol alge dwaenneunde
Oh, tell me, what is love?
Tell me, what is love?

Gin sigandeuri jinado
Geu jarie nan meomchwoisseo
Haengbokhaetdeon sungani
Sajincheoreom nal meomchwobeoryeosseo yeah

Mutjiman, saramdeureun
Gwaenchannyago mutjiman
(gwaenchannyago mutjiman)
Jogeumssik buseojyeo (Yeah)
Bein mami apeungeol
Ireoke So bad
Naegeseo meolli gajima Oh- yeah
No baby you oh- no no no no no
I’m not a kind of steel yeah (yeah)

Neoreul ilkodo
Saraganeun naega neomu silchiman
Eonjenganeun… irago baraenda
(eonjengan nae gyeote Yeah)
Oh, tell me, what is love?
Tell me, what is love?
(Tell me, what is love?)
Nae nuneul bomyeo malhaetdeon
(Come back to me babe)
Du geuljaga nagincheoreom apa wa (Yeah-)
Nae mami geugeol alge dwaenneunde…
Oh, tell me, what is love?
Tell me, what is love?
(Tell me, what is love)

English Translation

Oh yeah, don’t leave me baby
Just turn around
Yeah- Play another slow jam
(Oh- Play another slow jam)
This is my property (Oh-)
Tell me, what is love?

As I forgot about you who doesn’t love me
I live doing everything that I wanted to do
I can’t ever erase you from my head
Why?

Who are you to make me pitiful?
If I cursed you out I would have felt better at least
Why? Why did you leave me
Without saying anything?

I’m not ready to break up Wait a minute
Waiting is too long it Has no limit
I keep calling for you but you’re answerless
And only echoes keep coming back to me…

I hate myself
For living on after losing you
But I hope that some day…
Oh, tell me, what is love?
Tell me, what is love?
Like you told me while gazing into my eyes
The two letters hurt like humiliation
My heart finally knows
Oh, tell me, what is love?
Tell me, what is love?

Hey, baby please stop!
You stop in your tracks walking away from me
Please even now… NO NO NO
It doesn’t matter if it’s a lie
I want to know at least the truth

(Why did you keep)
Pushing me away?
(How I kept approaching you,)
Was that selfish of me?
I can’t escape from these thoughts
Is such a thing love?

I’m not ready to break up Wait a minute
(Wait a minute for me baby)
Waiting is too long, it Has no limit
(Has no limit)
I keep calling for you but you’re answerless
And only echoes keep coming back to me..

I hate myself
For living on after losing you
But I hope that some day… (I hope for you)
Oh, tell me, what is love?
Tell me, what is love?
Like you told me while gazing into my eyes (YEAH-)
The two letters hurt like humiliation
My heart finally knows
Oh, tell me, what is love?
Tell me, what is love?

I’m frozen in the same place
Even after time passes
Happy moments stopped me
Like a photo. YEAH

They ask, people ask
If I’m alright
(if I’m alright)
I break a little (YEAH)
My slashed heart hurts
Like this, SO BAD…
Don’t go far from me Oh- yeah
No baby you oh- no no no no no
I’m not a kind of steel yeah (yeah)

I hate myself
For living on after losing you
But I hope that some day… I hope for
(Some day next to me Yeah)
Oh, tell me, what is love?
Tell me, what is love?
(Tell me, what is love?)
Like you told me while gazing into my eyes
(Come back to me babe)
The two letters hurt like humiliation. (Yeah)
My heart finally knows..
Oh, tell me, what is love?
Tell me, what is love?
(Tell me, what is love)