Hangul

[Verse 1]
사랑하고 싶었던 거야
불행히 난 아니었지만
해 잃은 밤인 듯
나를 앞에 두고도
알아보지 못하는 그대

[Pre-Chorus]
오랜 시간
난 여기 있었죠
그대 기억 속에서

[Chorus]
아련한 눈빛으로
그대 마음을 채워
애타게 부르면
언젠가는 볼 수 있나요
그대 싫은 모습
내 맘에 담아도
몰래 흘린 눈물
가슴에 새겨도
내 맘을 알 수 없어

[Pre-Chorus]
바람처럼
거기 있었군요
사랑했던 그대여

[Chorus]
아련한 눈빛으로
그대 마음을 채워
애타게 부르면
언젠가는 볼 수 있나요
그대 싫은 모습
내 맘에 담아도
몰래 흘린 눈물
가슴에 새겨도
좋은 기억 모두
내 맘에 지워도
함께 약속했던
기억들 버려도
내 맘을 알 수 없어

[Bridge]
난 부서질 것 같아
힘 없이 무너지는
모래성처럼

[Refrain]
푸르른 하늘 위에
고운 얼굴 그리다
다시 또 불러봐요
돌아 오지 않는 그대를

[Chorus]
좋은 기억 모두
내 맘에 지워도
함께 약속했던
기억들 버려도
거울 속에 비친
내 모습 미워도
아직 사랑하나 봐
<<<<<<< HEAD
=======

>>>>>>> master
Romanization

[Verse 1]
Saranghago sipeossdeon geoya
Bulhaenghi nan anieossjiman
Hae ilheun bamin deus
Nareul ape dugodo
Araboji moshaneun geudae

[Pre-Chorus]
Oraen sigan
Nan yeogi isseossjyo
Geudae gieok sogeseo

[Chorus]
Aryeonhan nunbicceuro
Geudae maeumeul chaewo
Aetage bureumyeon
Eonjenganeun bol su issnayo
Geudae silheun moseup
Nae mame damado
Mollae heullin nunmul
Gaseume saegyeodo
Nae mameul al su eopseo

[Pre-Chorus]
Baramcheoreom
Geogi isseossgunyo
Saranghaessdeon geudaeyeo

[Chorus]
Aryeonhan nunbicceuro
Geudae maeumeul chaewo
Aetage bureumyeon
Eonjenganeun bol su issnayo
Geudae silheun moseup
Nae mame damado
Mollae heullin nunmul
Gaseume saegyeodo
Joheun gieok modu
Nae mame jiwodo
Hamkke yaksokhaessdeon
Gieokdeul beoryeodo
Nae mameul al su eopseo

[Bridge]
Nan buseojil geot gata
Him eopsi muneojineun
Moraeseongcheoreom

[Refrain]
Pureureun haneul wie
Goun eolgul geurida
Dasi tto bulleobwayo
Dora oji anhneun geudaereul

[Chorus]
Joheun gieok modu
Nae mame jiwodo
Hamkke yaksokhaessdeon
Gieokdeul beoryeodo
Geoul soge bichin
Nae moseup miwodo
Ajik saranghana bwa
<<<<<<< HEAD
=======

>>>>>>> master
English Translation

[Verse 1]
I wanted to love you
Although unfortunately, I wasn’t the one
Like a night that lost the sun
Even though I’m right in front of you
You don’t recognize me

[Pre-Chorus]
For a long time
I stood here
In your memory

[Chorus]
With a sad look
I’m filling up your heart
If I anxiously call out
Will I be able to see you some day?
Even if I put bad images of you in my heart
Even if I engrave my secret tears in my heart
You don’t know my heart
Like the wind
You were right there

[Pre-Chorus]
You, who I used to love
With a sad look
I’m filling up your heart

[Chorus]
If I anxiously call out
Will I be able to see you some day?
Even if I put bad images of you in my heart
Even if I engrave my secret tears in my heart
Even if I erase all good memories in my heart
Even if I throw away the promised memories

[Bridge]
You don’t know my heart
I feel like I will break down
Like a sand castle that crumbles down

[Refrain]
On the blue sky
I was drawing your fair face
Then I called out to you again
To you, who won’t come back

[Chorus]
Even if I erase all good memories in my heart
Even if I throw away the promised memories
Even if I hate my reflection in the mirror
I think I still love you