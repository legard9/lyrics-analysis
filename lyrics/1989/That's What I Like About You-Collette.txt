[Verse 1: Andrew]
I know you never seen nothing like this
Blow up, show up, everybody right here where the mic is
I just light that fuse and I run with it
Had a guy try and buy rights to my likeness
Go ahead, make a movie
Cut the checks, get that gravy to me
Get that bread, that paper to me
I'll have a whole kitchen on pastry duty
That's what I'm trying to do
I'll have a shark and a lion or two
Head up to the rooftop, see my zebra
Remember take a minute and admire the view
Living like Xanadu
That's exactly what I plan to do
Please believe I'm gonna make it happen
At the minimum, I hope I made a fan of you

[Chorus: BK]
Yeah I got
Yeah I got it all
And I ain't really worried about nothing
Nothing anymore

[Verse 2: Travis]
Making it up as we go, like Calvinball
Been a while since you heard us here spitting raw
But lay the shit down on these cyphers, might heard
We always try and kill it like our name is Squall
Alright, alright, that's what I'm talking about
Yo, like some bees in a hive, yeah we're buzzing now
We know you're wowed now, no need to bow down
Cause on the count of three, man we're getting loud
One, two--come on, I ain't really gonna do it
Do you think college graduates would act so stupid?
True, it's through this music you could probably misconstrue it
Drawn to -clusions, add the con-, on our songs we--screw it
I ain't gotta explain
Beat's Will Graham season one, it's going insane
Killing college parties, fratricide, Abel and Cain
Don't deny the groove, you've got nothing to gain

[Chorus]

[Outro: Andrew]
I feel it
Oh, oh, I feel it
Oh, you know I'm floating away
And if you feel it
Don't try to conceal it
Own it, you're the realest
Oh, you know what real is