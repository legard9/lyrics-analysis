[PERCY]
<<<<<<< HEAD
So many nights I have stood in the moonlight,
=======
So many nights I have stood in the 
moonlight
,
>>>>>>> master
watching it fade into dawn,
wanting her back with me, warm in the moonlight,
knowing that moment was gone -
out of mind, out of sight,
'til the moon rose tonight ...
<<<<<<< HEAD

=======
>>>>>>> master
All at once I felt a chill.
In a spill of moonlight she was there.
Though we both held very still,
there was something pulling in the air.
<<<<<<< HEAD

=======
>>>>>>> master
When she whispered through the dark,
I tried hard to hold my ground.
I believed I had a choice
'til the music in her voice
turned my whole world around.
<<<<<<< HEAD

I would like to understand,
but the stars and I begin to blur ...
=======
I would like to understand,
but 
the stars and I begin to blur
 ...
>>>>>>> master
If she never touched my hand,
then what filled me with the feel of her?
In between us stood a wall.
In a flash it fell apart!
Is it possible she heard
ev'ry last unspoken word
racing out of my heart?
<<<<<<< HEAD

=======
>>>>>>> master
She never turned to me,
but suddenly
we had so much to share!
I never took her in my arms,
but she was there!
Oh, she was there!
<<<<<<< HEAD

=======
>>>>>>> master
No, I never pulled her in!
Still her tenderness was ev'rywhere!
Oh, she slipped beneath my skin,
just as if she'd always been right there!
Has she been there all along?
Was I too far gone to know?
What a fool I must have been,
for how could I pull her in
when I've never let her go!