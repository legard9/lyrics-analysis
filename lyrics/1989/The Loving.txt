So I sing you to sleep after the lovin'
With a song I just wrote yesterday
And I hope you can hear
What the words and the music have to say

It's so hard to explain everything that I'm feelin'
Face to face, I just seem to go dry
But I love you so much that the sound
Of your voice can get me high

Thanks for taking me on a one way trip to the sun
And thanks for turning me into a someone

So I sing you to sleep after the lovin'
I brush back the hair from your eyes
And the love on your face is so real
That it makes me want to cry

And I know that my song isn't saying anything new
But after the lovin' I'm still in love with you

And I know that my song isn't saying anything new
Oh, but after the lovin' I'm still in love with you

— I love you too, Barton
— Uhmm