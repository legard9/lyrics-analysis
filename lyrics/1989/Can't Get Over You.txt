[Intro]
Over me-e-e-e-e-e-e

[Verse 1]
Over me? There's not one over
I'm overweight, yeah, one tonne over
Every beat, you talk about gun over
Stop MC'ing and don't come over
When I MC, the crowd get won over
Yep, you and your crew get spun over

[Refrain]
Over me-e-e-e-e-e-e

[Verse 2]
Like Hot Wheels, you'll get run over
Any little thing, man get bun over
Trust me fam, that's your fun over
You can't wait for the day that I'm over
On this beat, you'll get flung over
Not me, that bit get sung over

[Refrain]
Over me-e-e-e-e-e-e
You can't get over me-e-e-e-e-e-e

[Verse 3]
I get bookings from here to Hanover
You MCs don't get brang over
I bring Skepta, Gritz and Jamm over
You can't afford to fly man over
Over budget? Yeah, one grand over
I take sand to the beach, bring Anne over

[Refrain]
Over me-e-e-e-e-e-e

[Verse 4]
I can't be ignored like a hangover
'Cause every beat, I sound nang over
Half o' dem, I don't get mad over
This bass will knock your gran over
On Pro Evo, you'll get bang over
I'll make man have to pass that pad over

[Refrain]
Over me-e-e-e-e-e-e
You can't get over me-e-e-e-e-e-e

[Verse 5]
I'm quick, like Denilson step-over
On the next level, you can't get over
You MCs, I don't fret over
I got bars you get upset over
You wanna bet grime won't get let over?
I'm the guy you'll lose your bet over

[Refrain]
Over me-e-e-e-e-e-e

[Verse 6]
I got a show in France, jet over
Or the Eurostar, wet over
Promoter can't pass no debt over
They're the tings I'll break your neck over
Most little things I don't pet over
But I want cash, don't post no cheque over

[Refrain]
Over me-e-e-e-e-e-e
You can't get over me-e-e-e-e-e-e

[Verse 7]
Any qualms, I'll bust your chin over
You can't sing on the tune, man sing over
You're moving slow, I wing over
You're not ready for the vibe that I bring over
Trust me, I've done shows in over 20 countries you ain't been over

[Refrain]
Over me-e-e-e-e-e-e

[Verse 8]
When I'm here, don't bring your ting over
'Cause your girlfriend's, heart I'll win over
She's deep in love, I'll swim over
She'll go head-over-heels and spin over
Come to mine tonight, you're staying over
Checkmate, I took your king over

[Refrain]
Over me-e-e-e-e-e-e
You can't get over me-e-e-e-e-e-e
Over me-e-e-e-e-e-e
You can't get over me-e-e-e-e-e-e