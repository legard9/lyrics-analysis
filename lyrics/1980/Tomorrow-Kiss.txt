[Verse 1]
I didn't know just what to say
When you turned and you looked my way
It doesn't happen to me every day
Can we talk a while?

I didn't know just what to do
I couldn't seem to take my eyes off you
You know I wanted just to take you home
But that's not your style

[Chorus]
And tomorrow, we're gonna fall in love, fall in love
Tomorrow, we're going to fall in love, fall in love, tomorrow, tomorrow

[Verse 2]
I didn't even know your name
Like a moth I was in your flame
I knew you wanted me to feel real good
By the way you smiled
You didn't have to say a word
I tried to tell you, but I lost my nerve
You know I wanted just to slip away for a little while

[Chorus]
And tomorrow, we're gonna fall in love, fall in love
Tomorrow, we're gonna fall in love, fall in love, tomorrow, tomorrow

We're gonna fall in love, fall in love, fall in love, fall in love with me
(But tonight, when the feeling feels so right, we got all the love we need)

[Solo]

[Bridge]
I didn't know just what to say
This doesn't happen to me every day
And that's not my style

[Chorus]
And tomorrow, we're gonna fall in love, fall in love
Tomorrow, we're gonna fall in love, fall in love
Tomorrow, tomorrow, tomorrow, tomorrow
(Tomorrow) we're gonna fall in love, fall in love
(Tomorrow) fall in love, fall in love
(Tomorrow) fall in love, fall in love
(Tomorrow) fall in love, fall in love

We're gonna fall in love, fall in love, fall in love, fall in love with me
(But tonight, when the feeling feels so right, we got all the love we need)