[Chorus]
Here come the girls!
Girls, girls, girls-girls!
Here come the girls!
Girls, girls, girls-girls!

[Verse 1]
You're lookin' so good, it's a doggone shame
That they all couldn't be mine
You're lookin' so pretty, it's a doggone pity
Oh, you're lookin' so fine
I said look out brother, let me get further
Little closer to the one I love
Anything better than the opposite sex
They must have kep' it up above

[Chorus]
Here come the girls!
(
Girls, girls, girls-girls!
)
Here come the girls!
(
Girls, girls, girls-girls!
)

[Verse 2]
I can live without coffee, I can live without tea
But I'm livid 'bout the honeybee
I'm not a fillet steak, a leave or take
But the girls are part of me
Ohhhh, water
I don't need no lemonade
But to live without girls, can't live without girls
Like a man with a hole in his head

[Chorus]
Here come the girls! Oh
(
Girls, girls, girls-girls!
)
Here come the girls! Yeah
(
Girls, girls, girls-girls!
)
Here come the girls! Ohh oh
(
Girls, girls, girls-girls!
)
Here come the girls!
(
Girls, girls, girls-girls!
)

[Post-Chorus]
Bored outta my head
Oh, while the foxes do their thing
You make me feel so good inside
I gotta jump up and sing
Naa, naa, naa, naa
Naa, naa, naa naa
Naa, naa, naa, naa
Naa-naa-nana-na
Naa, naa, naa, naa
Naa, naa-nana, na
Naa, naa, naa, naa
Naa-naa-nana-na

[Bridge]
Ohh-ohh
I'm not sayin' I can live on love alone
Ohh-ohh
That's the only thing that turns me on
Ohh-ohh
I was born to be a freak just once
Freedom of choice, ring that bell
Give all the girls to me

[Chorus]
Here come the girls! Ha ooh
(
Girls, girls, girls-girls!
)
Here come the girls! Yeah
(
Girls, girls, girls-girls!
)
Here come the girls! Yeah yeah
(
Girls, girls, girls-girls!
)
Here come the girls! Oh oh
(
Girls, girls, girls-girls!
)
Here come the girls!
(
Girls, girls, girls-girls!
)
Here come the girls!
(
Girls, girls, girls-girls!
)
Here come the girls! Yeah
(
Girls, girls, girls-girls!
)
Here come the girls!
(
Girls, girls, girls-girls!
)