[Verse 1]
I'm growing ever so impatient
At times I feel like my kindness is on vacation
What is it you want from me?
Want money? Want time?
Barely finding it myself
I can not contain the rage these days
Mental destruction we power through it
Running away is how cowards do it
You should be careful, I'm bout to lose it
Oh my God, save me please
How can I stop from hating me
Too many put there faith in me
Don't trust in me, I'll combust, you'll see

[Chorus]
Stick with me and you will see
This is more than just me
If you take my hand and trust me
You will become more like me

[Verse 2]
Walk on this Earth
I'm a man on this land with no grandiose plan
I stress over time like the sands in my hands
No reverse for precursor
Still I'm a fan like MJ, Selena, Lisa
Too much damage, Nancy, Katrina
Slamming my hands on the table like Bino
Line up my problems then black out like bingo
Black on black crime and this side is so geno
You just keep bugging me, back away Shino
Why do you test me so?
Why won't you let me go?
I need a sec to grow
Kakrot, I need my vegetables
Coming up short
Still so compact like the Capsule Corp
It seems that my soul resembles a Puck
Whenever I slash, I feel like I'm Guts

[Chorus]
Stick with me and you will see
This is more than just me
If you take my hand and trust me
You will become more like me

[Bridge]
Broken down
I can't keep count
You put your faith in me
Your faith in me
If you realize you'll see
I'm not what I used to be
I used to be okay
But now my heart is stuck in the flames

[Chorus]
Stick with me and you will see
This is more than just me
If you take my hand and trust me
You will become more like me

[Verse 3]
The results are not optimal
I've been crashing into obstacles
They keep laughing, never audible
Got a passion, what's it costing you?
Hard for me to sleep, part of me is weak
Other part is the same thing
Shackled up like a chain gang
I'm overloading my mainframe
No legend messes with the blame game
They just carve a path, giving it all they have
Scars from heart to back
Never bogard, but guard the squad
When times are hard, overworking the calves
Do not irk with a gaffe
Carry two teams so they carry two staffs
Sip from the stream and prepare for the bath
People will leave it's a part of the math
Subtract
Wrath