[Verse 1]
People look to me and say
Is the end near, when is the final day
What's the future of mankind
How do I know, I got left behind

[Chorus]
Everyone goes through changes
Looking to find the truth
Don't look to me for answers
Don't ask me, I don't know (Know...)

[Verse 2]
How am I supposed to know
Hidden meanings that will never show
Fools and prophets from the past
Life's a stage and we're all in the cast

[Chorus]
Ya gotta believe in someone
Asking me who is right
Asking me who to follow
Don't ask me, I don't know (Know)
I don't know, I don't know (Know), I don't know

[Bridge]
Nobody ever told me I found out for myself
Ya gotta believe in foolish miracles
It's not how you play the game
It's if you win or lose you can choose
Don't confuse win or lose, it's up to you
It's up to you, it's up to you, it's up to you
Go, go, go

[Guitar Solo]

[Verse 1]
People look to me and say
Is the end near, when is the final day
What's the future of mankind
How do I know, I got left behind, I'm lost

[Chorus]
Everyone goes through changes
Looking to find the truth
Don't look to me for answers
Don't ask me, I don't know