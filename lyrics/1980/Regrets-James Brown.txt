You know, when... a person does another person wrong
And there's prob'ly a lot of ways to say "I'm sorry", but...
And after that, hey, all you have to live with is your memories
But don't let it get you down, 'cause it happens to the best of us

Oh, I wish that I could take back
All the things I've done to hurt you
I didn't mean to be so cruel
And yet, I know I've been selfish
And at times, a jealous fool

And every thoughtless thing that I've done
Keeps coming back to haunt me
Because you took each one
To your little somber heart
And I realize, that it's my fault
But we've grown worlds apart...

Oh, I have regrets
(I have regrets), regrets
For my mistaken thinkin' only of myself
Now that all I have left are these mean regrets
(I have regrets) regrets
For the things I did, somehow you just can't forgive
I guess I'll always have to live with these mean regrets
Mmhm...

If I could right the wrong that I've done
Maybe I could stop believing
There's so much I'm guilty of, oh yeah
Yet it's too late to say I'm sorry
'Cause being sorry just ain't enough

'Cause with each angry word that I've said
Your love for me was dyin'
And each word left a lasting scar
And now I feel so helpless
Wishin' I could change the way things are

I have regrets
(I have regrets) I have regrets
For my mistaken thinkin' only of myself
And now that all I have left are these mean regrets
(I have regrets) regrets
For the things I did, somehow you just can't forgive
And I guess I'll always have to live with these regrets

Oh baby, oh baby
My, my, my, my, my, these...
Oh Lord, oh Lord
Help me please

If I could right the wrong that I've done
Maybe I could stop believing
There's so much I'm guilty of, help me
Yet it's too late to say I'm sorry
'Cause being sorry just ain't enough

I have regrets
(I have regrets) Oh, regrets
For my mistaken thinkin' only, so selfish, of myself
And now that all I have left are these regrets
(I have regrets) Ahh, yeah baby
I guess I just can't forgive
And I'll always have to live with these mean regrets
(I have regrets) Oh, regrets, yea-ah, ahhh

Oh baby, don't let me suffer so long
Come on over to my place
(I have regrets) Come on over to my place
Or let me know where you are
I can please, if I can get your phone number
And when I call...
Then when she comes on, and says (I have regrets)
"You're having a message - you're leaving with me"
But I know they don't understand, you and I
And oh, I don't want the world to know about my regrets
(I have regrets) AAhhh! No no, no no, no...
Oh, can I, can I walk home?
Can I walk home, can I walk on home? (I have regrets)
Ahh, no, now now now...
I'm a thousand miles away, and all I need
Is your tender voice to lead me on
'Cause I lost someone, yeah (I have regrets)
Yeah yeah, I lost someone, yeah
I lost someone
I don't want no stranger to come in my home, and enjoy
(I have regrets) Enjoy what we've built, baby
Ah, ah, oh no no...