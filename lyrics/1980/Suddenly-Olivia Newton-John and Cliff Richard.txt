[VERSE 1]
[CLIFF]
She walks in and I'm suddenly a hero
I'm taken in
My hopes begin to rise

[OLIVIA]
Look at me
Can't you tell
I'd be so thrilled to see
The message in your eyes?

[CLIFF]
You make it seem I'm so close to my dream
And then suddenly it's all there

[Chorus]
[OLIVIA]
Suddenly
[BOTH]
The wheels are in motion
And I
I'm ready to sail any ocean
Suddenly, I don't need the answers
'Cause I
I'm ready to take all my chances with you

[VERSE 2]
[CLIFF]
And how can I
Feel you're all that matters?
I'd rely on anything you say

[OLIVIA]
I'll take care that no illusions shatter
If you dare to say what you should say

[CLIFF]
You make it seem I'm so close to my dream
And then suddenly it's all there

[Chorus]
[OLIVIA]
Suddenly
[BOTH]
The wheels are in motion
And I
I'm ready to sail any ocean
Suddenly, I don't need the answers
'Cause I
I'm ready to take all my chances with you

[BRIDGE]
[CLIFF]
Why do I feel so alive when you're near?
There's no way any hurt can get through
[BOTH]
Longing to spend every moment of the day with you
With you

[Chorus]
[OLIVIA]
Suddenly
[BOTH]
The wheels are in motion
And I
I'm ready to sail any ocean
Suddenly, I don't need the answers
'Cause I
I'm ready to take all my chances with you