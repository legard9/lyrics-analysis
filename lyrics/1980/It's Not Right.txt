[Intro]
Ooh, ooh, ooh
Ooh, ooh, ooh-ooh

[Verse 1: Nayeon & Mina]
Allami ullyeodae ring, ring-a-ling
Seoroui nungiri daheul ttaemada
Almyeonseo bingbing doneunde
Jeomjeom dagagajanha I know it's too late

[Pre-Chorus: Sana, Chaeyoung & Jeongyeon]
Maeumsogeuroneun da algo itjanha
Gyeolgugeneun seoneul neomge doel georaneun geol
I’m warning to myself neo geureomyeon an dwae
Maebun, maecho, nae mami nae mameul chuwolhae
Out of control (Hey)

[Chorus: Jihyo, Momo, Nayeon, Dahyun & (All)]
Nareul gamsihaneun jeo spot, spot, spotlight
Bichulsurok еodum sogeuro ppallyeo deurеo
Kkeutchi boineunde I know it's not right
I can't stop me, can’t stop me
(No, woah, woah, hey)
Nae ape nohyeojin i red, red, red line
Geonneopyeonui neowa nan imi nuneul majchwo
Neukkigo sipeo jjarit han highlight
I can't stop me, can't stop me
(No, woah, woah)

[Post-Chorus: Tzuyu]
Ooh, ooh, ooh
Ooh, ooh, ooh-ooh
Ooh, ooh, ooh
Ooh, ooh, ooh-ooh
Ooh, ooh, ooh
Ooh, ooh, ooh-ooh
I can't stop me, can't stop me

[Verse 2: Momo, Dahyun, Jeongyeon, Jihyo, (Chaeyoung)]
Nungama dallagu (Yah, yah)
Ttak han beonman no rules (Ah, ah)
Moreun cheokhae jwo lights off tonight
Na chameul su eopseul geo gata losing myself (Yeah, yeah)

[Pre-Chorus: Tzuyu, Sana, Chaeyoung & Jihyo]
Ijeneun turning back bulganeunghae
Nan jeomjeom deo gipeun eodume (Gipeun eodume)
Neomuna jjarit hae na nuneul gameullae (Hey)
Dasineun doragal su eopseul geosman gatae
Out of control (Ooh, yeah; Hey)

[Chorus: Nayeon, Dahyun, Jihyo, Mina & (All)]
Nareul gamsihaneun jeo spot, spot, spotlight
Bichulsurok eodum sogeuro ppallyeo deureo (Ooh, ooh, ooh)
Kkeutchi boineunde I know it's not right
I can't stop me, can't stop me
(No, woah, woah, hey)
Nae ape nohyeojin i red, red, red line (Red line)
Geonneopyeonui neowa nan imi nuneul majchwo
Neukkigo sipeo jjarit han highlight
I can’t stop me, can’t stop me (I can't stop me, yeah)
(No, woah, woah)

[Verse 3: Chaeyoung, Dahyun & Momo]
Risky, risky, wiggy wigi
This is an emergency
Help me, help me, somebody stop me
’Cause I know I can't stop me
Dabeun algo itjanha
Geunde gago itjanha
Ireogo sipji anha
Nae ane naega tto itna bwa

[Bridge: Mina, Sana, Tzuyu & Nayeon]
Naneun wonhaneunde (Ah, ah)
Wonhaneun ge andwae (Ooh, ooh)
Guilty nan silheunde
I can't stop me, can't stop me, can’t stop me (Oh)

[Chorus: Jeongyeon, Chaeyoung, Nayeon, Sana, (All) & (Jihyo)]
Nareul gamsihaneun jeo spot, spot, spotlight
Bichulsurok eodum sogeuro ppallyeo deureo
Kkeutchi boineunde I know it's not right
I can't stop me, can't stop me
(No, woah, woah, hey)
Nae ape nohyeojin i red, red, red line
Geonneopyeonui neowa nan imi nuneul majchwo
Neukkigo sipeo jjarit han highlight (Ooh, ooh, ooh, ooh, ooh)
I can't stop me, can't stop me (Yeah, yeah, yeah)
(No, woah, woah)

[Post-Chorus: Tzuyu, Nayeon]
Ooh, ooh, ooh
Ooh, ooh, ooh-ooh (Ah, ah)
Ooh, ooh, ooh
Ooh, ooh, ooh-ooh (Ooh, ooh)
Ooh, ooh, ooh
Ooh, ooh, ooh-ooh
I can't stop me, can't stop me
No, woah, woah

[Outro]
Ooh, ooh, ooh
Ooh, ooh, ooh-ooh
Ooh, ooh, ooh
Ooh, ooh, ooh-ooh
Ooh, ooh, ooh
Ooh, ooh, ooh-ooh