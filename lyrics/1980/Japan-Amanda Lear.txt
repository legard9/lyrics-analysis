Oriental people are a mystery
Strange little ladies make origami
Walking a street is like a beautiful dream
Open my eyes and I am in Japan
Hear children feet worm on a catwalk
Sweet little faces my last we talk
Run tip-tap on a catwalk
Dance all around
Open my eyes and I am in Japan
Japan, Japan, Japan
Japan, Japan, Japan
I like dancing around
Tokyo, my kind of town
I can't forget naif sukijaki
Yokohama and Nagasaki
Japan, Japan, Japan
I like dancing around
Tokyo, my kind of town
They got traditions and electronics
Imagination and kimono chic
Goldorak and geishas, Fujiyama
I feel so happy to be in Japan
Japan, Japan, Japan
I like dancing around
Tokyo, my kind of town
Japan, Japan, Japan
I'll always miss Japan
Tokyo, my kind of town
Japan, Japan, Japan
Oh, how I love Japan
Japan, Japan, Japan