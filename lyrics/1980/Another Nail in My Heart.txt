One punch!
(Three! Two! One! Kill shot!)

[Verse 1]
Sanjou! Hisshou! Shijou saikyou!!
(Nan da tten da? Furasutoreeshon ore wa tomaranai)
ONE PUNCH! Kanryou! Rensen renshou!
(Ore wa katsu!! Tsune ni katsu!! Asshou!!)
Power! Get the power! Girigiri genkai made

[English]
Appearing! Certain victory! The absolute strongest!
(What're you saying? Frustration! Nobody can stop me)
ONE PUNCH! It's over! One victory after another!
(I shout out! I'm always victorious! Totally victory!)
Power! Get the power! Right up to the limit

[Chorus]
HERO! Ore o tataeru koe ya kassai nante hoshiku wa nai sa
HERO! Dakara hitoshirezu aku to tatakau
(Nobody knows who he is.)
Sora ooi oshiyoseru teki ore wa se o muki wa shinai
HERO! Naraba yuruginaki kakugo shita tame kuridase tekken

[English]
HERO! I don't want voices praising me or an ovation
HERO! So I fight against evil in secret
(Nobody knows who he is.)
Foes are closing in and covering the sky. I won't turn my back on them
If I am a hero, then I'm prepared with unwavering resolve, unleashing my fist
(Three! Two! One! Fight back!)

[Verse 2]
Sanjou! Go on! Seiseidoudou!!
(Dou natten da? Nani mo kanjinee mohaya teki inee!)
Justice! Shikkou! Mondou muyou!
(Ore ga tatsu!! Aku o tatsu!! Gasshou!!)
Power! Get the power! Adorenarin afuredasu ze!
Power! Get the power! Kitaeta waza o buchikamase!

[English]
Appearing! Go on! Fair and square!
(What's going on? I can't feel a thing, my opponents are all gone!)
Justice! Enforcement! No point arguing it!
(I'll eradicate! Eradicate evil! Say your prayers!)
Power! Get the power! Adrenaline's overflowing!
Power! Get the power! Strike with the force of my disciplined technique!

[Chorus]
HERO! Donna ni tsuyoi yatsu mo chippoke na gaki dattan da
HERO! Yowaki onore norikoe tsuyoku naru
(Nobody knows who he is.)
Kami yadoru kobushi kakagete ore wa tsukisusumu dake sa
HERO! Itsuka haiboku ni odei nameru made takakau HERO!

[English]
HERO! Even the strongest guys used to be tiny brats
HERO! I overcome my weakness and become stronger
(Nobody knows who he is.)
I just raise my fists with the gods dwelling in them, and push onward
HERO! Until I taste the dirt of defeat some day, a fighting HERO!

[Bridge]
Ore wa akiramenai sono mune ni asu o egaki
Mezame yuku sekai e ima maiagare tsuyoku takaku
Donna toki de mo nani ga atte mo

[English]
I won't give up; I picture the future in my heart
I awaken and go to the world now, soaring high and strong
No matter when, no matter what happens

[Chorus]
HERO! Ore o tataeru koe ya kassai nante hoshiku wa nai sa
HERO! Dakara hitoshirezu aku to tatakau
(Nobody knows who he is.)
Kami yadoru kobushi kakagete ore wa tsukisusumu dake sa
HERO! Itsuka haiboku ni odei nameru made takakau HERO!
Kodoku na HERO!
I wanna be the saikyou HERO!

[English]
HERO! I don't want voices praising me or an ovation
HERO! So I fight against evil in secret
I just raise my fists with the gods dwelling in them, and push onward
HERO! Until I taste the dirt of defeat some day, a fighting HERO!
A lonely HERO!
I wanna be the strongest HERO!