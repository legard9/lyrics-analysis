Thirstin Howl III - “The Polorican”
[Emcee(s): Thirstin Howl III]
[Producer(s): PF Cuttin]
[Scratches: PF Cuttin]
[Sample (Melody): Los Terricolas - “Cenizas”]

[Intro: Thirstin Howl III]
Yo, this is for the live Puerto Rocks with official style.  Thoroughbred status.  Yo, what’s my nationality?

[Hook: Thirstin Howl III and (Samples with Scratches by PF Cuttin)]
Polorican! (“Polo cologne” - Sample from Slick Rick on Doug E. Fresh & The Get Fresh Crew ft. Slick Rick - “La Di Da Di”)
Polorican! (“Same damn Lo sweater” - Sample from Raekwon on Wu-Tang Clan (ft. Raekwon, Method Man, and Inspectah Deck) - “C.R.E.A.M.”)
Polorican! ([?])
Polorican! (“I sport Polo” - Sample from Heavy D on Marley Marl ft. Heavy D and Biz Markie - “We Write the Songs”)

[Verse 1: Thirstin Howl III]
Don’t call me dunn ‘til I’m finished
Marcus Garvey Village, Lo items vintage
Me wear FUBU?  Man, listen!
That’s like the Marlboro man smoking a Winston
(Polorican!) From the cradle to the precincts
Holding noise, mug shots, Poloroids
You not Thirsty as me—you neither
With one sip, I could kill a two-liter!
Snatch carteras, funk cualquiera
Left-hook izquierda with tijeras
Verano, hot.  Perra, know
The streets is watching—vélalo
Drop science, speak true facts
Jail activist, made my own doo-rags

[Hook: Thirstin Howl III and (Samples with Scratches by PF Cuttin)]
Polorican! (“Polo cologne” - Sample from Slick Rick on Doug E. Fresh & The Get Fresh Crew ft. Slick Rick - “La Di Da Di”)
Polorican! (“Same damn Lo sweater” - Sample from Raekwon on Wu-Tang Clan (ft. Raekwon, Method Man, and Inspectah Deck) - “C.R.E.A.M.”)
Polorican! ([?])
Polorican! (“I sport Polo” - Sample from Heavy D on Marley Marl ft. Heavy D and Biz Markie - “We Write the Songs”)

[Verse 2: Thirstin Howl III]
(Polorican!) Ralph Lauren pueblo
Tommy ain’t my gear—I will never rock a bear coat
Eckō?  No puedo!
Tight por este cuello, Polo be way low
Mossimo’s for facilones
The lights are on, but nobody’s home
Who could stop me?  A nana de mami
Illuminati, lodging
Polo golf shoes, curry goat soup
It’s a thin line between jakes and thugs
He ain’t a Lo-Life if he’s wearing Lugz
Polo shaving, never salute Old Navy
Condemnado, yo soy el quemando
El clavo, blew them out the cuadro
Turn beef to caldo, papa majada
Hará bajada, off-the-hook agancha!
Knowledge this: profit lives
Still got my ‘86 Gucci moccasins

[Hook: Thirstin Howl III and (Samples with Scratches by PF Cuttin)] (x2)
Polorican! (“Polo cologne” - Sample from Slick Rick on Doug E. Fresh & The Get Fresh Crew ft. Slick Rick - “La Di Da Di”)
Polorican! (“Same damn Lo sweater” - Sample from Raekwon on Wu-Tang Clan (ft. Raekwon, Method Man, and Inspectah Deck) - “C.R.E.A.M.”)
Polorican! ([?])
Polorican! (“I sport Polo” - Sample from Heavy D on Marley Marl ft. Heavy D and Biz Markie - “We Write the Songs”)

[Verse 3: Thirstin Howl III]
P-O-L-O on my hat
Vio bueno—never wore shit from Gap
Nos diga todo.  Illegalo—mira, bro
Heart of San Dulces, jibaros!
No ice—fishing at the Shark Bar
True b-boys don’t wear Sean John
Nautica’s not for all of us
North Face official style sculptured
Stenciled.  Heads flown as scheduled
Feet tearé—no me piss el pie
I’m subtitled, speak in French
Spanish version of “Impeach the Prez”
“Brownsville Bullet Goldcard Membership”
My mind’s the weapon, my heart’s the extra clip

[Hook: Thirstin Howl III and (Samples with Scratches by PF Cuttin)]
Polorican! (“Polo cologne” - Sample from Slick Rick on Doug E. Fresh & The Get Fresh Crew ft. Slick Rick - “La Di Da Di”)
(“Same damn Lo sweater” - Sample from Raekwon on Wu-Tang Clan (ft. Raekwon, Method Man, and Inspectah Deck) - “C.R.E.A.M.”)

[Bridge: Thirstin Howl III]
When I die, bury me with the Lo on
Official to the death, all eternity and so on
When I die, bury me with the Lo on
Official to the death, all eternity and so on
When I die, bury me with the Lo on
Official to the death, all eternity and so on
And so on, and so on
And so on, and so on

[Hook: Thirstin Howl III and (Samples with Scratches by PF Cuttin)]
Polorican! (“Polo cologne” - Sample from Slick Rick on Doug E. Fresh & The Get Fresh Crew ft. Slick Rick - “La Di Da Di”)
Polorican! (“Same damn Lo sweater” - Sample from Raekwon on Wu-Tang Clan (ft. Raekwon, Method Man, and Inspectah Deck) - “C.R.E.A.M.”)
Polorican! ([?])
Polorican! (“I sport Polo” - Sample from Heavy D on Marley Marl ft. Heavy D and Biz Markie - “We Write the Songs”)
(“I sport Polo” - Sample from Heavy D on Marley Marl ft. Heavy D and Biz Markie - “We Write the Songs”)
(“Polo cologne” - Sample from Slick Rick on Doug E. Fresh & The Get Fresh Crew ft. Slick Rick - “La Di Da Di”)
(“Same damn Lo sweater” - Sample from Raekwon on Wu-Tang Clan (ft. Raekwon, Method Man, and Inspectah Deck) - “C.R.E.A.M.”)
([?])
(“I sport Polo” - Sample from Heavy D on Marley Marl ft. Heavy D and Biz Markie - “We Write the Songs”)