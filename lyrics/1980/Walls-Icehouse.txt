Listen
If you listen
You will hear an endless heartbeat
On the inside
Through the walls
The sound is crawling
Down the corridors and halls
It cracks the ceiling
The windows and the doors
All the rest won't listen
Though the walls have ears
But they never really look
They just stand and stare
They're all standing
Staring at the walls

Who put the writing on the walls?
Will no-one ever know?
Oh, well I don't mind the walls

Tell me
Why don't you tell me?
Have you got nothing to say?
Tell me where in hell this place is
Tell me why I get no answers
Am I talking, taking to the walls?
So it's running, jumping, standing still
Well I think I've had enough of it all
Get your hands up
Up against the wall
(well, I'm never going back to the factory
No I'll never be a part of the machinery)

Who put the writing on the wall?
Who fights and runs away?
The minutes and the hours pass him
Tracing out the days
The days and nights drag into years
And no-one ever knows
Oh, well I don't mind the walls
Oh, well I don't mind the walls
I don't mind the walls