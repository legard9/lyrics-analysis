[Verse 1: Batto]
I just need a little progress
I just need a little movement
Stuck on her
Cause she showed promise, yeah
Stuck on scenes
I can't play back
I lost those options
How you look and how you move I should have been cautioned
Somebody should have said something
You're top, top to the bottom
God knows how you blossomed
Stuck still
But that's time's will
You see time's still when you watch it
Time's still when I' close to you
But I'm a ghost to you, I got it
Stuck still
On a few words, cause I can't live with this silence
But it's all good I guess
Take what you need
Dont mind the mess
Once I believed
Now I guess I'm on the wrong side of this..
But we ain't gotta rush
And it can never be enough
So it is
And due to the place that I'm in...

[Chorus: Jozi]
Well, I been thinking, I been thinking, I been thinking
We don't need to rush
And all these feelings in my heart that I been keeping
They might be too much
So what's another one
Just another one
Coz we never get enough
Roll another one
Sip another one
Coz we never get enough
Coz we ain't never get enough

[Verse 2: UniQ]
Ah
I mind my bi'ness
And you the only one with shares
My little Mrs
But baby don't you ever scare
And if I'm missing
Just trust in God I'm always there
And all these feelings
It's prolly coz it's something rare
Yeah, wouldn't you rather feel?
Yeah, how else you know it's real?
Ah, going out?
Making out?
Or posting each other on IG?
There might be something in this drink
That got me talking like them real Gs
And I don't even really talk much
I just handle you with a soft touch
Ummh, 'Ey
God damn make your blood rush
Ummh, stay
For all we know it's just another night
So light up the Dunhill, yeah
Put on that Netflix and chill, yeah
And you could just lay on my chest, yeah
I swear there is treasure under neath

[Chorus: Jozi]
Well, I been thinking, I been thinking, I been thinking
We don't need to rush
And all these feelings in my heart that I been keeping
They might be too much
So what's another one
Just another one
Coz we never get enough
Roll another one
Sip another one
Coz we never get enough
Coz we ain't never get enough

[Outro: Jozi]
Nananana, nananana
Nananana, nananana
Baby I've been thinking
That we dont need to rush
Baby all these feelings...
Can they just be too much?
Baby I've been thinking
That we dont need to rush
Baby all these feelings...
Can they just be too much?