Pretty Nancy of London from that fair inland stream
She was courted by William he belongs to the sea
She was courted by William a long time ago
Now he's on the sea sailing where the stormy winds do blow

Oh the stormy winds do blow my boys and they make my heart ache
They make my room window for to shiver and shake
God knows where my love lies so far from the shore
I will pray for his welfare tell me what can I do more

When the sailors are sailing they drink a health to thеir wives
For they love thеir own sweethearts as they love their own lives
Here's a punch going round my boys, here's a full glass in hand
Here's a health to loving Nancy that I leave upon dry land

Oh, it's Nancy my jewel, my joy and heart's delight
Here is one lovely letter I a m going for to write
Here is one lovely letter to let you well know
That I'm on the seas sailing where the stormy winds do blow