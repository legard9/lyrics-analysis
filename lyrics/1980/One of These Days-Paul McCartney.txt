[Verse 1]
One of these days, when my feet are on the ground
I'm gonna look around and see
See what's right, see what's there
And breathe fresh air ever after

[Verse 2]
One of these days, when a job just takes too long
I'm gonna sing my song and see
See what's right, see what's there
And breathe fresh air ever after

[Bridge]
It's there, it's round
It's to be found
By you, by me
It's all we ever wanted to be

[Verse 3]
One of these days, when we both are at our ease
When you've got time to please yourself
See what's right and see what's there
And breathe fresh air ever after

[Bridge]
It's there, it's round
It's to be found
By you, by me
It's all we ever wanted to see

[Verse 1]
One of these days, when my feet are on the ground
I'm gonna look around and see
See what's right and see what's there
And breathe fresh air ever after

[Outro]
Ever after
Breathe fresh air ever after