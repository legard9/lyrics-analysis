[Verse 1]
Jeep tires burn my driveway black
Got my John Deere hat turned back
Brother says there's going to be a fight
Sumter County Friday Night
One black eye and two teeth later
Sumter High and the Lakewood gators
Brandy May, Nancy Bean
Waiting at the Dairy Queen
Jump in the back, take down the top
28 minutes to Sparkle Berry Swamp
Come over here give me a kiss
Lord, don't make it no better than this

[Chorus]
Red dirt roads and big tire toys
Country girls and redneck boys
Carolina moon is big and bright
Sumter County Friday Night

[Verse 2]
Fred Johnson just opened up his fields
Change of plans ain't no big deal
Hundred fifty cell phones ring
Everybody's talking about the same thing
Wedgefield Road south of town
Go 13 miles then slow it down
Radar trap, Barney Fife
Don't hold that brown bag up to high

[Chorus]
Red dirt roads and big tire toys
Country girls and redneck boys
Carolina moon is big and bright
Sumter County Friday Night

[Verse 3]
Tailgates down around the fire
New shotgun can't wait to try her
Beer can targets in the air
Duck boots are the thing to wear
Cow tipping, skinny dipping
Bring your own cause you can't have mine
Don't start out looking for trouble
But oh, the trouble we can find

[Chorus]
Red dirt roads and big tire toys
Country girls and redneck boys
Carolina moon is big and bright
Sumter County Friday Night