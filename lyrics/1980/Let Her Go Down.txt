[Verse 1 - Chester P]
It's nice to be here, and first of all my name is Jim
Tonight I came along to try express what kind of pain I'm in
I started talking to the dead when I was 9 at best
And naturally my parents felt obliged to run some minor tests
The doctor said my mind's a mess that I was mad as Hell
The next thing that I knew I was sitting in some padded cell
And after years of being tortured by same crazy quacks
They told me I was better, then re-housed me in a dainty flat
And everywhеre I look the dead arе tryna ask for help
They follow me eternally insisting that their presence felt
I'm not insane but for most my life I felt I was
And yea I lost my job again 'cus somehow I had to tell the boss
That me I talk to dead folk and his nana says to tell him 'hi'
Not to think his wife is not aware that all he does is lie
I guess it's fair to say that I am quite the loner
And I possess the sight to reach beyond the greys persona
And just to say it loud is lifting off a heavy cloud
So thank you all for listening your decency has done me proud
You're like the warmest group of people that I've ever known
And Tom, your mother says it's cool, just settle down and let her go

[Hook]
No matter where you're from
Lets take a minute to reflect on all the things that you've done
I guess I'm all alone
No you'll never be alone, we're always here so you can phone
Will I ever be okay?
Well its doubtful you'll be normal, but it's boring being plain
Am I mad?
It's easy to believe, but you are special in my eyes
This is ESP

[Verse 2 - Farma G]
Hello my name is Hugo and I'm very glad to be here
My problems overwhelming, and it's something that I've never shared
I warn you from the start, you should all just keep your distance
'Cus every time I touch a living thing I have a premonition
And this is nothing like a gift, I hear you asking why?
'Cus all I ever see in all of my visions is how you die
The time and date and how it happens yea I see it all
And I am never wrong predicting when the grim reaper calls
I was 5 and I knew my daddy's time would come
I begged him not to go to work and tried my best to warn my mum
But mother sent me to my room and told me I was dumb
I wish she had believed me, now she thinks I'm like the devils son
So just imagine what it's like for me to hug my kids
And kiss my lovely wife, when I know how long they've got to live
It's hard to look them in the eye without a tear to shed
'Cus every time I hold their little hands I have to see them dead
My life is morbid, I'm sick and wanna end it here
But when I touch myself I know I won't be gone for many years
I'm thankful that you've listened, now I'm feeling better man
And please don't take offense if I refuse to shake your hand

[Hook]
No matter where you're from
Lets take a minute to reflect on all the things that you've done
I guess I'm all alone
No you'll never be alone, we're always here so you can phone
Will I ever be okay?
Well its doubtful you'll be normal, but it's boring being plain
Am I mad?
It's easy to believe, but you are special in my eyes
This is ESP