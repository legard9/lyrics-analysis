[Intro]
It's Degs
Ebenezer Freestyle
Shouts to my DnB family

[Chorus 1]
All around the world
I never wanted to conform
I used to beat myself about it
But now preparing for the encore (ayy)
I'm sailing but you're staying onshore
You need to fight when the drought hits
I'm soaring high like a Concorde

[Interlude]
Yeah, let's do this
Shouts to my RH squad

[Verse 1]
No scrooge, Ebenezer
Getting in the cans like a geezer
I'm ill on the mic no fever
Live in the dance man I'm tearing the speaker
All I wanna do is get high with the team
Spit fire with the team and make a life with the team
Pour drinks to the future
I'm an atheist but with love I'm a believer
Yeah, and that’s the truth of it
Write syllables and words get in booths with it
I'm seeing all this hate I'm reducing it
Too many of our souls man are losing it
'Cause the world’s so cold that's a fact of life
And this girl's so cold 'cause she's acting like
That I never held it down in the club tonight
So it's arguments and it's jealous eyes
'Cause we've, lost trust for the people
Write bars for the book
All I wanna do is go live for the sequel
All I wanna do is get high man it's lethal
And what I mean by that is
I'm neglecting shit responsibilities
And I'm a faking a smile
Spread wings like an eagle, yeah
But fuck this
I preach vibes not hatred
And confidence in your consciousness
Have pride when you say shit
Have pride when you make shit
And seek help when you're breaking
That's important bro you ain't an island
We should all give strength for the weigh-ins, yeah
You gotta go down fighting
Let's do this
12th round in the ring and we're tiring
But your corner's here to make your game plan clear
So listen up we're uniting
Power in numbers and we've got abundance
Tear down walls that we're climbing
And tear down fools that are sniping
I'm bored of it this negativity
It's a mystery to me
Dark times start smiling
In dark times start smiling
'Cause we're wiling out tonight, listen
I never thought that we'd get to this point
The poison burns on aching joints
I'm hearing what you're saying
In a shaking voice and feeling like I've made mistakes
Deluded that I've made it rain
Our lives have been so up and down
Stability like paper planes
Sick of each other like the company's got stale
All I'm trying to do is get away
Fly down the wing like Gareth Bale
But now it's time to rectify
So we can tell compelling tales
About the good things in our lives
Without us wearing veils

[Pre-Chorus]
'Cause I’m holding onto this and that's the truth, trying to get away
Ohh

[Chorus 2]
Swing back to me 'cause I don't mind
I'll crush it down and bill it so we're both high
But you don't wanna chill and I know why
'Cause your mind is goin' in circles
You always falling at the hurdles, yeah
But what's the point if you don't try?

[Verse 2]
And I know you're mine
'Cause I put my soul in the fire and lights
And I got you girl for the colder nights man I roll the dice
I don't wanna feel like I'm holding time, woo
With you now everything is not lost
When I'm talking about my history girl
I put it in a mental loft never counting the cost
Yes yes, I'm never counting the cost
Past experience rounding it off
If anybody wanted to install change
The old me would argue the toss
Should I get packed up jump in the whip
And come to your house on your night off?
I'll never grow tired if you show me the way
Whenever my ice cold heart defrosts, yeah
When I'm sleeping girl I'm moving a lot
My mental strings get tied in a knot
3AM man I stare at the clock
But there's no time it's your time when you're mine
No war crimes no sore rhymes in your prime
Cream rises right to the top
And I no longer feel deserted
But still I put the work in
Wanna be leading on this circuit
This longevity ain't certain
It's not that I misinterpret
It's the coldness of the world
As I'm sitting here hypothermic

[Outro]
Na na na na na na na na na na nana
Ohh
(It's Degs)
(Yeah)
(Ebenezer)