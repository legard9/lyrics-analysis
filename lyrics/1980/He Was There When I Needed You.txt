They'd ask me if I'm okay if they saw me now
They'd ask what's wrong if I began to break down
But they'd understand if they lost their grandfather
They'd understand why my life is getting darker
Granddad, you were always there when I needed you
Through life's troubles, you'd motivate me to push through
Always moving 'round, looking for a place to rest
'Till you found a neighbourhood so nice, maybe the best

I knew I'd experience this at one point during my life
But my-my, now 'Rain' hurts more in hindsight
On the outside, I'm emotionless, that's no lie
But inside, deep down, I know I want to cry
Let my tears show, why don't I? I don't know
I guess my mourning process is rather slow
You’re one of many grandfathers that I’ve got
I should be crying over this, but I’m honestly not

And I really have no clue why
But now that you’re gone, a piece of me may also die
And your passing is understandable, it’s sad but true
I can only imagine how much it hurts my mum too
Especially since her own dad passed away years back
I guess she’s as sad as me, her joy being reduced to black
I guess it’s only natural to be a sad grandson
Granddad’s now out of our lives, Michael Jackson

Don’t know if I should be happy his suffering’s over
Or sad that his life’s ended and I wasn’t closer
To him than I could have been during his life
Now it seems his has become lost in the night
It’s also saddening that he had survived cancer
The fact a brain tumour killed him fills me with anger
But I guess a good thing to come out this tonight
Is that we can all agree that he lived a good life

Of course I give condolences to my nan
Still single in my life, but I can understand
Why it would hurt her to lose her husband like this
I know out of my family, he’ll always be missed
Like a candle that’s been blown out in the wind
Or like a bulb in a light that’s seriously dimmed
I know that a grandparent is never expendable
But I guess this type of moment was inevitable

So I’m not shedding tears nor having shaky breaths
I’d rather celebrate your life than mourn your death
Though it weighs heavy on my chest, regardless I guess
It’s for the best that you can now finally rest
At the risk of sounding like there’s no care in my brain
I’d rather you be dead than alive and suffering in pain
In the end, you’ve helped me find my self-worth
So rest in peace, William Montgomery-Verth