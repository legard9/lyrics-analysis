Burden
my mother doesn’t want
to burden me
her bad hip’s shaky grumble
imagined as some weighty burlap sack
i must carry up a mountain
i want to tell her:
you would not make a child
apologize for asking for a juice box
Mama, there is nothing wrong
with needing a little help sometimes
i want to tell her:
loving you isn’t my burden
i want to ask her:
did you ever resent me
in my youth’s inadequacy?
i want to ask her:
is it naive
to think i can be of any use
at all?
Reciprocity
The girl on the internet says
She doesn’t owe anyone
	Anything.
The girl on the internet says
No one deserves my time /
No one deserves my labor /
No one deserves my energy /
No one deserves
The girl on the internet says
I am allowed to take and take and keep on taking
The girl on the internet says
I have given all that I can to this world
And she spit it back out
And what do you expect me to do now?
The girl on the internet is very, very angry
She says
Living out of spite
is still living,
right?
right?
Notes on Staying
By HIEU MINH NGUYEN  |  21 SEP 2016
All my life I watched my mother contemplate an exit, hovering between a conversation & a doorway. Her sleep, medicated & rich. I imagine, in her dreams she is tall with laughter. I feel most like her son when I am lonely—a child again, dragged by her to a party I enjoyed, but then stopped enjoying. In our future, there are two cabs idling in the driveway, which is a cowardly way of saying, I cannot kill myself until my mother dies. If joy is what tethers us to this life then most days, my mother & I float above the pavement, tied together by the fraying threads of her nightgown. All my life I’ve bitten at the knots of my solitude. No one wants to be alive when they’re forgotten. When she is gone, who will call my name?
& I should mention hope, since hope is what disarms the bomb when the city clutches their children goodnight, the red wire blue wire optimism of my mother’s voice, when she says, ​I don’t need friends, just you & in me still a child refusing to accept the terms of her mercy & how many times have I been told you’ll understand when you’re older, or how many times have I heard, ​we’re all gonna die one day—boring hopelessness, clearing the table before we eat, which is fine. Who needs a last meal? Who needs a good reason to leave the party before things get weird? So maybe that’s hope. Maybe hope is stopping the story before it’s over, before the inevitable messy end. O monger of the broken records. O monger of the early birthday present. Push me from the highway overpass—let’s leave the story there, let’s leave the body whole in mid-air illuminated by oncoming headlights, a tiny song, a pixel in the pixelated mouth of hope, or whatever it is that propels us through the door of tomorrow & since there was no key, I guess I’ll swallow the door.
I read the poem above during the height of my mother’s first hospital stay, between my brother’s first suicide attempt and his second. I cried outside my academic advisor’s door and he peered out timidly, offered me a tissues like a child might offer seeds to a squirrel. Like he was a little afraid. After reading this poem I thought I finally understood what it must be like to be living at home right now--my brother suffering in silence, feeling trapped into his illness by my mother’s different and immediate pain. I thought I knew what it was like to be him without even asking. This is one of the first mistakes I made.
Notes on Language
It was very important throughout the month of January that we name my mother’s healing in her terms. Everything became regal, a kind of fantasy game in which everything metallic and clinical became weapons of magic.  The portable toilet, which I carried gingerly between living room, bathroom, and bedroom, became her Throne. The extendable plastic tongs, used to reach remote control or bag of pistachios, her Claw. My mother renamed her healing Training. She was never broken so there was nothing to heal from. She was just practicing her body stronger. And it hurt. And it was hard. And we all pretended that it wasn’t. And she wrote the rules.
When she got the staples out, she sweaty-squeezed my hand so tight her wedding ring slipped off. She’d lost weight in preparation for the surgery, and her fingers were smaller than I could recall in recent memory. She swore SHIT! when it happened, and we asked the nurse Mai to take a break for a second. He didn’t want to-- said it would be easier for all of us if we finished in one fell swoop-- but my mother persisted. As she watched me crawl around on hands and knees looking for the ring amongst all the medical waste from the checkup, I felt like an infant again, discovering a new world from an angle reserved for the smallest of us. I found it pretty quickly, old gold hidden in the plastic lip of a urine collection jar. She slipped it back on with shaking fingers and we didn’t speak about it again. Later, she asked to see the staples. Mai put them in a little dish and Mama jangled them together in the still afternoon light. She wanted to keep them but Mai said it was a hazard. Everything felt so hazardous that day it was a wonder we didn’t wear masks.
Balance
After the doctors inserted the rods, my mother’s right leg extended an inch past her left. Once she was allowed back onto her feet, she found it difficult to walk evenly, the newly long side of her dizzying in its frustration. She used the crutch for longer than she needed, I think. It was easier to blame her limping on the worn down rubber of the crutch’s foot than to admit there was something else changed within her own body.
When her energy was up, she’d joke that she was a pegleg, that she’d lost her real leg at sea and her hobbling was rational given the melodrama of the situation. I’m tempted to describe her laughter as hollow, but really rather than being empty it just contained much more than I was used to. Perhaps what I mean is that all her joking during this period seemed imbued with something morbid and foreign to our household, drops of saltwater stinging every echo of speech. I wonder who she was trying to comfort when she talked like that.
In late winter, only a few days before I was set to depart back to school, I woke to her slowly looping the backyard in the middle of the night. I wasn’t sure why’d she be out there; it didn’t make sense for her not to be tucked into bed. I knew she was having trouble sleeping, that the sutures in her side made her unable to position herself comfortably, that the rod in her hip still felt alien and forced. My bedroom is upstairs, at the back of the house; we converted it from a sunroom when I was younger and desperate for privacy. I looked out its window and watched. The minutes melted into each other.What was she doing out there? She didn’t seem like my mother, but someone separate; I stared at her body swinging itself so slowly around the apple trees, the familiar limp and drag of her right foot now rooting itself into the wet dirt. I wanted to call out, but was afraid I would embarrass her, or startle her off course. She wasn’t even supposed to be walking up and down stairs by herself at this point- to make it all the way down from her room must have been a wild feat of patience. Eventually I drifted back into sleep, left her slowly tracing under the sky lit a muted orange by foggy streetlights.
I asked her about it in the morning:
What were you doing out there?
Practicing.
Why did you go alone? You’re not supposed to go alone.
	I knew what I was doing.
Well, can you let me know next time? I was worried.
	..
I’m sorry for bugging you all the time.
	I never meant to worry anyone.
Just let me help next time, alright?
	Okay. Okay.
Therapy
The first time my family speaks openly
About my brother wanting to slit his wrists
Is in plastic white chairs
In a therapist’s office on Geary Street.
Afterward,
We bring home Banh Mi
And let the vinegar dribble down our chins
As we watch television and do not speak.
As my mother bends over the dishwasher,
Her aging back shakes.
Her tears mix with the detergent
And form tiny blue bubbles on the good china.
She explains:
We have to throw the knives away.
Together, we unload the silverware trap
Pack the garbage can like cucumber inside a dumpling wrapper
Or the plastic seats we sat in when we cried with our open mouths.
List of Demands/My Family is Sick But You Don’t Get to Say So
Thou shalt not call my mother weak.
Thou shalt not call my mother patient.
Thou shalt not call my mother anything/but her name.
Nancy.
Thou shalt not offer to bring us a casserole/Casseroles are for the dead/My brother is not dead yet.
Thou shalt not drop in unannounced/Thou shalt not pretend we want to schedule you in either.
Thou shalt not tell me I am being strong/I am being my mother’s daughter/brother’s sister/family.
Thou shalt not turn away from the x-ray/You asked to see/so you will see.
Thou shalt not demand any more of me than I of you/Call that cooperation/or just/giving a shit.
Give a shit/Please/Just stop taking.
Support Work
September 2016-February 2017 were some of the hardest months of my life. Within the span of 8 or so weeks, Trump won the election, my brother attempted suicide, my mother was admitted to the hospital for surgery but had to stay much longer because of complications, and the Ghost Ship Fire took the lives of multiple friends from the Bay Area. I felt like I was walking through my days inside a fever. I was completely and totally overwhelmed by grief, and with the new election came some of the most difficult sexual assault flashbacks of my life. I found myself incapable of contributing politically to Oberlin’s campus threats. I felt useless; there were days where I didn’t leave my bed or eat. I felt guilt over everything--If I did participate in things outside my dorm’s walls, I felt I wasn’t adequately taking care of myself; if I stayed put, I felt I was letting my community down. I broke down in public multiple times. I wondered if I needed to go home and not come back. I had repeated dreams of walking into a field and suffocating in the grass.
When  I got home, things changed. I was forced by circumstance to stop thinking about myself. I doubt that I allowed myself to fully process my grief, but at the time what I needed was to move forward. My mother needed round the clock care and assistance, my brother needed to feel seen and heard, and my best friend Sarah needed help being admitted into a psychiatric ward. My days became consumed with support work; it’s what I care about and am good at, but this was the first time the work had felt so much like physical labor. My mom needed help using the restroom, feeding herself, showering, going up and down stairs; I routinely filled ice packs, made soup, made tea, made the bed, took care of the garden, helped with her sutures and medications and physical therapy exercises. I knew it was hard for her to rely on me so much, but in a way I was selfishly happy to focus on only the tasks directly ahead of me.
I was spread thin. I know this. My relationship with my brother is fraught, and there were times when I didn’t want to hear about his struggles, when I felt like any attention he needed should have been focused on helping my mother. He was the most depressed he had ever been, and still I resented him for not helping out around the house. I was angry with my father too, although I knew he was doing the best he could given his nature. By the end of break, the smallest things would bring me nearly to tears: piled up dishes on the counter, the cat crying to be let outside, the mailbox overflowing with junk mail and issues of National Geographic. I valued housework  for the explicit nature of its completion, the obvious steps I could take to make our living space more habitable. But it hurt that I had to do it all alone.  I found myself overwhelmed in a new way.
It’s interesting that I refer so much to this time period as me “finding myself” in emotion. More than anything I think I was drifting away. Doing support work when you don’t yourself feel supported is a new kind of identity formation. When your days are spent lifting someone else up, how does that relationship inform your livelihood? This winter, I wasn’t queer, I wasn’t an Oberlin student, I wasn’t femme in the radical ways I’ve strived to be. I was just tough. And looking back, it’s influenced many of the ways in which I imagine myself now; doing that heavy lifting  gave me new insight into my queer identity, into the labor behind femininity, into the forms of love I will put time and sweat into that aren’t romantic or sexual. Most of all, though, I find it fascinating that I was so compartmentalized for so long. When I got back to school and wasn’t spending 100% of my energy on other people, it was like all my intersections meeting up at once. It felt like walking into a field and breathing in so deep my chest turned blue.
Shower
Have you ever helped your mother shower? I hadn’t until December 16th, 2016. She was shy when asking me, and I could see by the oil in her hair and the dark sweat smell coming from her armpits that it had been a while since she’d last been really clean. I found out later that the men in my house were so uncomfortable about the nakedness that they just dumped her in the  tub and waited for her to figure it out- never mind the fact that she couldn’t reach the faucet.
	So when I helped her, it was different. It had to be. We pretended we were at the spa, and would put a foux-foux French lilt into our voices throughout the period. “Ahhh, oui oui madame, the water eez warm enuf, oui?” I’d joke. “Merci, merci, monsieur. The water eez fine. Would you, monsieur, scrub my leetle toes?” For the interaction to work the way it needed to, we had to pretend to be people other than ourselves.
I hadn’t seen my mom naked since I was a little kid, and it was fascinating discovering her in this way. We bumbled, both trying to spare the other of any awkwardness. I’d hold her skull in my hands, massage Trader Joe’s tea tree shampoo into the roots of her greying hair and watch as the water passed the bubbles over her freckled back and down the drain. I’d massage her tender calves with an old pink loofah- I had to be careful, because if I lifted the left one incorrectly, she’d stifle groans of pain.
The worst part was after, when I had to help her get dressed. Dressing her wound came first, and I had to inspect the infection site at her newly removed sutures. At first I would clench my thumb under my fist, because I knew that would kill my gag reflex. It was hard to clean it because it hurt her, and because I was uncomfortable seeing a body open that way. There is a tenderness to flesh- the way it gives and pulls under tension- and I had to be so careful with her as I spread the sticky bandaid over her hip. I needed the precision of a surgeon but the heart of a friend. I did my best.
After she was bandaged up, she’d instruct me about the clothing that she wanted to wear that day. All that she could usually stomach to pull on was elastic-banded pajamas, but they made her feel childish and too casual to be seen by anyone other than her family. Once we tried for forty minutes to guide her into a pair of soft blue jeans. When we had to give up, she left a frustrated tear on my shoulder and tersely told me to leave the room. I stayed right outside the door, so afraid that she wouldn’t be able to get up off the bed by herself and that she’d fall and break the hip we’d spent so long healing. I’m sure that she heard me, though--I was crying too.