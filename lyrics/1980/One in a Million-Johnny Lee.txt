Sometimes it seems the world's a jungle
Through it all I was born to stumble
Searchin' for somethin' I never would find
I take your love in bits and pieces
Comin' to you only when it eases that lonely feelin'
That gets in my mind
And you treat me so kind
With those hard times come a dime a dozen
But girls with your kind of lovin'
Are one in a million
They're so hard to find
And those hard times come a dime a dozen
But girls with your kind of lovin'
Are one in a million
I'm lucky you're mine
I know your friends don't think much of me
They say you're a fool to love me
But girl, I know they'll never turn you around
You're bound to love, you hard luck poet
When I let you down
You don't show it
You know it don't matter
They can't touch what we found when the sun goes down
And those hard times come a dime a dozen
But girls with your kind of lovin'
Are one in a million
They're so hard to find
And those hard times come a dime a dozen
But girls, with your kind of lovin'
Are one in a million
I'm lucky you're mine
And those hard times come a dime a dozen
But girls, with your kind of lovin'
Are one in a million
They're so hard to find
And those hard times come a dime a dozen