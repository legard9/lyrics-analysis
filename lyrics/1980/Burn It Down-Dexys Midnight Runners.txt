I'll only ask you once more
You only want to believe
This man is looking for someone to hold him down
He doesn't quite ever understand the meaning

Never heard about, can't think about
Oscar Wilde and Brendan Behan
Sean O'Casey, George Bernard Shaw
Samuel Beckett, Eugene O'Neill, Edna O'Brien and Lawrence Stern

I'll only ask you once more
It must be so hard to see
This man is waiting for someone to hold him down
He doesn't quite fully understand the meaning

Never heard about, won't think about
Oscar Wilde and Brendan Behan
Sean O'Casey, George Bernard Shaw
Samuel Beckett, Eugene O'Neill, Edna O'Brien and Lawrence Stern
Sean Kavanaugh and Sean McCann
Benedict Keilly, Jimmy Hiney
Frank O'Connor and Catherine Rhine

Shut it You don't understand it
Shut it That's not the way I planned it
Shut your fucking mouth 'til you know the truth