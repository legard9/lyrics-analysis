2017: Self-driving cars
Google self-driving cars have gone half a million miles without human drivers on highways and city streets, with no incidents.
 
Within ten years they will be ubiquitous.
 
Humans have a fairly narrow field of view, these
 
cars have sensors, both visual and laser
, and 
artificial intelligence to be able to assess what’s going on in their environment
. 
Ultimately these cars will communicate with each other and co-ordinate their movements.
 
You also won’t need to own a car, there’ll be a pool of them circulating, and you’ll just call one from your phone when you need it.
2018: Personal assistant search engines
Right now, search is based mostly on looking for key words
. 
What I’m working on is creating a search engine that understands the meaning of these billion of documents.
 
It will be more like a human assistant that you can talk things over with,
 
that you can express complicated, even personal concerns to
. 
If you’re wearing something like Google Glass,
 
it could annotate reality
; 
it could even listen in to a conversation, giving helpful hints. It might suggest an anecdote that would fit into your conversation in real time.
2020: Switch off our fat cells
It was in our interest a thousand years ago to store every calorie.
 
There were no refrigerators, so you stored them in the fat cells of your body, which now means
 
we have an epidemic of obesity and type 2 diabetes
. 
Thanks to the Human Genome Project, medicine is now information technology, and we’re learning how to reprogram this outdated software of our bodies exponentially.
 
In animals with diabetes, scientists have now successfully turned off the fat insulin receptor gene.
 
So these animals ate ravenously, remained slim, didn’t get diabetes, and lived 20 per cent longer.
 I would say that this will be a human intervention in five to ten years, and 
we will have the means of really controlling our weight independent of our eating.
2020: Click and print designer clothes at home
Currently there is a lot of overenthusiasm about 3-D printing. Typically where people are prematurely very excited it leads to disillusionment and a bust, like the dot.com crash. I think we’re about five years away from the really important applications. 
By the early 2020s we’ll be replacing a significant part of manufacturing with 3-D printing. We’ll be able to print out clothing and there’ll be an open source market of free designs.
 
There will be personal 3-D printers, but also shared ones in your local Starbucks, for example.
2023: Full-immersion virtual realities
Computer games have pioneered virtual reality, and within ten years — but probably more like five — these will be totally convincing, full-immersion virtual realities, at least for the visual and auditory senses, and there will be some simulation of the tactile sense.
 To fully master the tactile sense we have to actually tap into the nervous system. That will be a scenario within 20 years. 
We’ll be able to send little devices, nanobots, into the brain and capillaries
, and they’ll provide additional sensory signals, as if they were coming from your real senses. 
You could for example get together with a friend, even though you were hundreds of thousands of miles apart, and take a virtual walk on a virtual Mediterranean beach and hold their hand and feel the warm spray of the moist air in your face.
2030: Vertical meat and vegetable farms
There will be a new vertical agriculture revolution, because right now we use up a third of the usable land of the world to produce food, which is very inefficient. 
Instead we will grow food in a computerised vertical factory building (which is a more efficient use of real estate) controlled by artificial intelligence, which recycles all of the nutrients so there’s no environmental impact at all.
 This would include 
hydroponic plants, fruits and vegetables
, and 
in vitro cloning of meat
. This could also be very healthy — we could have meat with Omega-3 fats instead of saturated fats, this sort of thing.
2033: 
100 per cent of our energy from solar
We are applying new nanotechnologies to the design of solar panels, and the costs are coming down dramatically. A recent report by Deutsche Bank said that ‘the cost of unsubsidised solar power is about the same as the cost of electricity from the grid in India and Italy. By 2014 even more countries will achieve solar grid parity’. So I do believe that within 20 years we could get all our energy from solar energy. I presented this not so long ago to the Prime Minister of Israel, Benjamin Netanyahu, who was actually 
my classmate at MIT’s Sloan School of Management
, and he said: “Ray, 
do we have enough sunlight to do this with?
” and I said: “
Yes, we’ve got 10,000 times more than we need
.
2040: Stay young 
for ever
Twenty years from now, we will be adding more time than is going by to your remaining life expectancy
. 
We’ve quadrupled life expectancy in the past 1,000 years and doubled it in the past 200 years.
 We’re now able to reprogram health and medicine as software, and so that pace is only going to continue to accelerate. There are three bridges to life extension. 
Bridge 1 is taking aggressive steps to stay healthy today, with today’s knowledge.
 The goal is to get to 
bridge 2: the biotechnology revolution
, where we can reprogram biology away from disease. Bridge 3 is the nanotechnology revolution. The quintessential application of that is nanobots — 
little robots in the bloodstream that augment your immune system
. We can create an immune system that recognises all disease, and could be reprogrammed to deal with new pathogens.