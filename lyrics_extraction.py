import os, re
import numpy as np
import pandas as pd
import lyricsgenius
import multiprocessing
import concurrent.futures
from langdetect import detect
from numpy.core.numeric import NaN
from numpy.lib.function_base import extract

def extract(df):

  genius = lyricsgenius.Genius("WaMsiD8LRv7GABgsXPDYcsohk3pABnM9OI-1xoZjn_CIQqkpcJCZCP6M95cF4ZHI")
  genius.verbose = False

  for _, row in df.iterrows():
    folder = f"lyrics/{row['year']}"
    if not os.path.isdir(folder): os.makedirs(folder)

    title = row['title']
    artist = '' if row['artist'] is NaN else row['artist']
    print(f'[*] Searching: {title} of {artist}...')

    try:
      song = genius.search_song(row['title'], artist=artist)
      print('[*] Writing...')

      title = str(title).replace('/', '-')
      artist = str(artist).replace('/', '-')

      if song is not None:
        if detect(song.lyrics) == 'en':
          fileName = title if artist == 'nan' else f'{title.strip()}-{artist.strip()}'
          with open(f'{folder}/{fileName.strip()}.txt', 'w') as f: f.write(song.lyrics)
      else: print('[*] Not found')

    except Exception as e:
      print(f'[*] Exception: {e.message}')
      continue

if __name__ == '__main__':
  df = pd.read_csv('dataset.csv')
  df.drop(df.columns[0], axis=1, inplace=True)

  cpu_count = multiprocessing.cpu_count()
  split = np.array_split(df, cpu_count)

  with concurrent.futures.ProcessPoolExecutor(max_workers=cpu_count) as executor:
    list_edges_partial = executor.map(extract, split)